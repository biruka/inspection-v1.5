﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Globalization;
using System.Web.Security;
using System.Web.Profile;
using CProfile;

namespace CUSTOR.Commen
{
    public class BasePage : Page
    {
        
        protected override void InitializeCulture()
        {

            if (Session["CurrentUICulture"] != null && Session["CurrentCulture"] != null)
            {
                Thread.CurrentThread.CurrentUICulture = (CultureInfo)Session["CurrentUICulture"];
                Thread.CurrentThread.CurrentCulture = (CultureInfo)Session["CurrentCulture"];
            }

            base.InitializeCulture();
        }

    }
}
