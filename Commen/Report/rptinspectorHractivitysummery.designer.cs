﻿namespace CUSTOR.Commen
{
    partial class rptinspectorHractivitysummery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptinspectorHractivitysummery));
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel1 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.lblOrganizationNameEng = new DevExpress.XtraReports.UI.XRLabel();
            this.lblOrganizationNameAmh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox3 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrFrom = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDateFrom = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDateTo = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPivotGrid1 = new DevExpress.XtraReports.UI.XRPivotGrid();
            this.xrPivotGridField1 = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.xrPivotGridField2 = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.xrPivotGridField3 = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.xrPivotGridField4 = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.xrChart1 = new DevExpress.XtraReports.UI.XRChart();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrControlStyle2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.lbldate = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPage = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.Label36 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            resources.ApplyResources(this.Detail, "Detail");
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblOrganizationNameEng,
            this.lblOrganizationNameAmh,
            this.xrPictureBox3,
            this.xrLine1});
            resources.ApplyResources(this.TopMargin, "TopMargin");
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // lblOrganizationNameEng
            // 
            resources.ApplyResources(this.lblOrganizationNameEng, "lblOrganizationNameEng");
            this.lblOrganizationNameEng.CanGrow = false;
            this.lblOrganizationNameEng.Multiline = true;
            this.lblOrganizationNameEng.Name = "lblOrganizationNameEng";
            this.lblOrganizationNameEng.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblOrganizationNameEng.StylePriority.UseFont = false;
            this.lblOrganizationNameEng.Tag = "Human Resource Managment System / የሰው ሃይል መዋቅር አስተዳደር ሲስተም";
            // 
            // lblOrganizationNameAmh
            // 
            resources.ApplyResources(this.lblOrganizationNameAmh, "lblOrganizationNameAmh");
            this.lblOrganizationNameAmh.CanGrow = false;
            this.lblOrganizationNameAmh.Multiline = true;
            this.lblOrganizationNameAmh.Name = "lblOrganizationNameAmh";
            this.lblOrganizationNameAmh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblOrganizationNameAmh.StylePriority.UseFont = false;
            this.lblOrganizationNameAmh.Tag = "Human Resource Managment System / የሰው ሃይል መዋቅር አስተዳደር ሲስተም";
            // 
            // xrPictureBox3
            // 
            this.xrPictureBox3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBox3.ImageUrl = "~\\Logo\\Logo.gif";
            resources.ApplyResources(this.xrPictureBox3, "xrPictureBox3");
            this.xrPictureBox3.Name = "xrPictureBox3";
            this.xrPictureBox3.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            this.xrPictureBox3.StylePriority.UseBorders = false;
            // 
            // xrLine1
            // 
            resources.ApplyResources(this.xrLine1, "xrLine1");
            this.xrLine1.LineWidth = 2;
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.StylePriority.UseBackColor = false;
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // xrFrom
            // 
            resources.ApplyResources(this.xrFrom, "xrFrom");
            this.xrFrom.Name = "xrFrom";
            this.xrFrom.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrFrom.StylePriority.UseBackColor = false;
            this.xrFrom.StylePriority.UseFont = false;
            this.xrFrom.StylePriority.UseTextAlignment = false;
            // 
            // xrDateFrom
            // 
            resources.ApplyResources(this.xrDateFrom, "xrDateFrom");
            this.xrDateFrom.Name = "xrDateFrom";
            this.xrDateFrom.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDateFrom.StylePriority.UseBackColor = false;
            this.xrDateFrom.StylePriority.UseFont = false;
            this.xrDateFrom.StylePriority.UseTextAlignment = false;
            // 
            // xrTo
            // 
            resources.ApplyResources(this.xrTo, "xrTo");
            this.xrTo.Name = "xrTo";
            this.xrTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTo.StylePriority.UseBackColor = false;
            this.xrTo.StylePriority.UseFont = false;
            this.xrTo.StylePriority.UseTextAlignment = false;
            // 
            // xrDateTo
            // 
            resources.ApplyResources(this.xrDateTo, "xrDateTo");
            this.xrDateTo.Name = "xrDateTo";
            this.xrDateTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDateTo.StylePriority.UseBackColor = false;
            this.xrDateTo.StylePriority.UseBorderColor = false;
            this.xrDateTo.StylePriority.UseFont = false;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            resources.ApplyResources(this.BottomMargin, "BottomMargin");
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrDateFrom,
            this.xrFrom,
            this.xrTo,
            this.xrDateTo});
            resources.ApplyResources(this.PageHeader, "PageHeader");
            this.PageHeader.Name = "PageHeader";
            // 
            // xrLabel1
            // 
            resources.ApplyResources(this.xrLabel1, "xrLabel1");
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            // 
            // xrPivotGrid1
            // 
            this.xrPivotGrid1.CellStyleName = "xrControlStyle1";
            this.xrPivotGrid1.FieldHeaderStyleName = "xrControlStyle2";
            this.xrPivotGrid1.Fields.AddRange(new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField[] {
            this.xrPivotGridField1,
            this.xrPivotGridField2,
            this.xrPivotGridField3,
            this.xrPivotGridField4});
            this.xrPivotGrid1.FieldValueStyleName = "xrControlStyle2";
            resources.ApplyResources(this.xrPivotGrid1, "xrPivotGrid1");
            this.xrPivotGrid1.Name = "xrPivotGrid1";
            this.xrPivotGrid1.OptionsPrint.FilterSeparatorBarPadding = 3;
            this.xrPivotGrid1.OptionsView.ShowColumnHeaders = false;
            this.xrPivotGrid1.OptionsView.ShowDataHeaders = false;
            // 
            // xrPivotGridField1
            // 
            this.xrPivotGridField1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.xrPivotGridField1.AreaIndex = 0;
            resources.ApplyResources(this.xrPivotGridField1, "xrPivotGridField1");
            this.xrPivotGridField1.Name = "xrPivotGridField1";
            // 
            // xrPivotGridField2
            // 
            this.xrPivotGridField2.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.xrPivotGridField2.AreaIndex = 0;
            resources.ApplyResources(this.xrPivotGridField2, "xrPivotGridField2");
            this.xrPivotGridField2.Name = "xrPivotGridField2";
            // 
            // xrPivotGridField3
            // 
            this.xrPivotGridField3.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.xrPivotGridField3.AreaIndex = 0;
            resources.ApplyResources(this.xrPivotGridField3, "xrPivotGridField3");
            this.xrPivotGridField3.Name = "xrPivotGridField3";
            // 
            // xrPivotGridField4
            // 
            this.xrPivotGridField4.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.xrPivotGridField4.AreaIndex = 1;
            resources.ApplyResources(this.xrPivotGridField4, "xrPivotGridField4");
            this.xrPivotGridField4.Name = "xrPivotGridField4";
            // 
            // xrChart1
            // 
            resources.ApplyResources(this.xrChart1, "xrChart1");
            this.xrChart1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrChart1.DataSource = this.xrPivotGrid1;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisX.WholeRange.AutoSideMargins = true;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.WholeRange.AutoSideMargins = true;
            xyDiagram1.DefaultPane.EnableAxisXScrolling = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram1.DefaultPane.EnableAxisXZooming = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram1.DefaultPane.EnableAxisYScrolling = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram1.DefaultPane.EnableAxisYZooming = DevExpress.Utils.DefaultBoolean.False;
            this.xrChart1.Diagram = xyDiagram1;
            this.xrChart1.Legend.MaxHorizontalPercentage = 30D;
            this.xrChart1.Name = "xrChart1";
            this.xrChart1.SeriesDataMember = "Series";
            this.xrChart1.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.xrChart1.SeriesTemplate.ArgumentDataMember = "Arguments";
            this.xrChart1.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative;
            sideBySideBarSeriesLabel1.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.Default;
            this.xrChart1.SeriesTemplate.Label = sideBySideBarSeriesLabel1;
            this.xrChart1.SeriesTemplate.ValueDataMembersSerializable = "Values";
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(245)))), ((int)(((byte)(216)))));
            this.xrControlStyle1.Name = "xrControlStyle1";
            this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // xrControlStyle2
            // 
            this.xrControlStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.xrControlStyle2.Name = "xrControlStyle2";
            this.xrControlStyle2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPivotGrid1,
            this.xrChart1});
            resources.ApplyResources(this.GroupHeader1, "GroupHeader1");
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lbldate,
            this.lblPage,
            this.xrPageInfo1,
            this.xrLine2,
            this.Label36});
            this.PageFooter.Name = "PageFooter";
            // 
            // lbldate
            // 
            resources.ApplyResources(this.lbldate, "lbldate");
            this.lbldate.Name = "lbldate";
            this.lbldate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbldate.StylePriority.UseForeColor = false;
            this.lbldate.StylePriority.UseTextAlignment = false;
            // 
            // lblPage
            // 
            resources.ApplyResources(this.lblPage, "lblPage");
            this.lblPage.CanGrow = false;
            this.lblPage.Multiline = true;
            this.lblPage.Name = "lblPage";
            this.lblPage.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPage.StylePriority.UseFont = false;
            this.lblPage.Tag = "Page/gI";
            // 
            // xrPageInfo1
            // 
            resources.ApplyResources(this.xrPageInfo1, "xrPageInfo1");
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            // 
            // xrLine2
            // 
            resources.ApplyResources(this.xrLine2, "xrLine2");
            this.xrLine2.LineWidth = 2;
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.StylePriority.UseBackColor = false;
            this.xrLine2.StylePriority.UseForeColor = false;
            // 
            // Label36
            // 
            resources.ApplyResources(this.Label36, "Label36");
            this.Label36.CanGrow = false;
            this.Label36.Multiline = true;
            this.Label36.Name = "Label36";
            this.Label36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Label36.StylePriority.UseFont = false;
            this.Label36.Tag = "Print Date/yHTmT qN";
            // 
            // rptinspectorHractivitysummery
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.GroupHeader1,
            this.PageFooter});
            resources.ApplyResources(this, "$this");
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1,
            this.xrControlStyle2});
            this.Version = "13.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.rptinspectorHractivitysummery_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRPivotGrid xrPivotGrid1;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField xrPivotGridField1;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField xrPivotGridField2;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField xrPivotGridField3;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField xrPivotGridField4;
        private DevExpress.XtraReports.UI.XRLabel xrFrom;
        private DevExpress.XtraReports.UI.XRLabel xrDateFrom;
        private DevExpress.XtraReports.UI.XRLabel xrTo;
        private DevExpress.XtraReports.UI.XRLabel xrDateTo;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle2;
        private DevExpress.XtraReports.UI.XRChart xrChart1;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox3;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        public DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRLabel lbldate;
        private DevExpress.XtraReports.UI.XRLabel lblPage;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel Label36;
        private DevExpress.XtraReports.UI.XRLabel lblOrganizationNameEng;
        private DevExpress.XtraReports.UI.XRLabel lblOrganizationNameAmh;
    }
}
