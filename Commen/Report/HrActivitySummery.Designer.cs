﻿using CUSTOR.Commen;
namespace Commen.Report
{
    partial class HrActivitySummery 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel1 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.lblOrganizationNameEng = new DevExpress.XtraReports.UI.XRLabel();
            this.lblOrganizationNameAmh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox3 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrChart1 = new DevExpress.XtraReports.UI.XRChart();
            this.xrPivotGrid1 = new DevExpress.XtraReports.UI.XRPivotGrid();
            this.xrPivotGridField1 = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.xrPivotGridField3 = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.xrPivotGridField2 = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.xrPivotGridField4 = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.xrPivotGridField5 = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.Label36 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPage = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.lbldate = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 7.291667F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1,
            this.lblOrganizationNameEng,
            this.lblOrganizationNameAmh,
            this.xrPictureBox3});
            this.TopMargin.HeightF = 142.75F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine1
            // 
            this.xrLine1.BackColor = System.Drawing.Color.Transparent;
            this.xrLine1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine1.LineWidth = 2;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(24.00018F, 126.0417F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(958F, 10.5F);
            this.xrLine1.StylePriority.UseBackColor = false;
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // lblOrganizationNameEng
            // 
            this.lblOrganizationNameEng.BackColor = System.Drawing.Color.Transparent;
            this.lblOrganizationNameEng.CanGrow = false;
            this.lblOrganizationNameEng.Font = new System.Drawing.Font("Nyala", 14.5F, System.Drawing.FontStyle.Bold);
            this.lblOrganizationNameEng.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblOrganizationNameEng.LocationFloat = new DevExpress.Utils.PointFloat(151.5416F, 52.10416F);
            this.lblOrganizationNameEng.Multiline = true;
            this.lblOrganizationNameEng.Name = "lblOrganizationNameEng";
            this.lblOrganizationNameEng.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblOrganizationNameEng.SizeF = new System.Drawing.SizeF(724.3334F, 24.00001F);
            this.lblOrganizationNameEng.StylePriority.UseFont = false;
            this.lblOrganizationNameEng.Tag = "Human Resource Managment System / የሰው ሃይል መዋቅር አስተዳደር ሲስተም";
            this.lblOrganizationNameEng.Text = "lblOrganizationNameEng";
            this.lblOrganizationNameEng.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblOrganizationNameAmh
            // 
            this.lblOrganizationNameAmh.BackColor = System.Drawing.Color.Transparent;
            this.lblOrganizationNameAmh.CanGrow = false;
            this.lblOrganizationNameAmh.Font = new System.Drawing.Font("Nyala", 14.5F, System.Drawing.FontStyle.Bold);
            this.lblOrganizationNameAmh.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblOrganizationNameAmh.LocationFloat = new DevExpress.Utils.PointFloat(151.5416F, 28.10415F);
            this.lblOrganizationNameAmh.Multiline = true;
            this.lblOrganizationNameAmh.Name = "lblOrganizationNameAmh";
            this.lblOrganizationNameAmh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblOrganizationNameAmh.SizeF = new System.Drawing.SizeF(724.3334F, 24F);
            this.lblOrganizationNameAmh.StylePriority.UseFont = false;
            this.lblOrganizationNameAmh.Tag = "Human Resource Managment System / የሰው ሃይል መዋቅር አስተዳደር ሲስተም";
            this.lblOrganizationNameAmh.Text = "lblOrganizationNameAmh";
            this.lblOrganizationNameAmh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrPictureBox3
            // 
            this.xrPictureBox3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBox3.ImageUrl = "~\\Logo\\Logo.gif";
            this.xrPictureBox3.LocationFloat = new DevExpress.Utils.PointFloat(23.95833F, 10.00001F);
            this.xrPictureBox3.Name = "xrPictureBox3";
            this.xrPictureBox3.SizeF = new System.Drawing.SizeF(114.33F, 101.08F);
            this.xrPictureBox3.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            this.xrPictureBox3.StylePriority.UseBorders = false;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 20.83333F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.Empty;
            this.xrLabel1.Font = new System.Drawing.Font("Nyala", 14.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10.00001F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(982.0002F, 39.58333F);
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = global::Commen.Resource1.txtStastic;
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1});
            this.PageHeader.HeightF = 49.58334F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrChart1
            // 
            this.xrChart1.BorderColor = System.Drawing.Color.Black;
            this.xrChart1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrChart1.DataSource = this.xrPivotGrid1;
            xyDiagram1.AxisX.Title.Text = "ተቋም የውሳኔ አይነት";
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisX.WholeRange.AutoSideMargins = true;
            xyDiagram1.AxisY.Title.Text = global::Commen.Resource1.txtHRMServiceType;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.WholeRange.AutoSideMargins = true;
            xyDiagram1.DefaultPane.EnableAxisXScrolling = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram1.DefaultPane.EnableAxisXZooming = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram1.DefaultPane.EnableAxisYScrolling = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram1.DefaultPane.EnableAxisYZooming = DevExpress.Utils.DefaultBoolean.False;
            this.xrChart1.Diagram = xyDiagram1;
            this.xrChart1.Legend.MaxHorizontalPercentage = 30D;
            this.xrChart1.LocationFloat = new DevExpress.Utils.PointFloat(9.99999F, 167.7083F);
            this.xrChart1.Name = "xrChart1";
            this.xrChart1.SeriesDataMember = "Series";
            this.xrChart1.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.xrChart1.SeriesTemplate.ArgumentDataMember = "Arguments";
            this.xrChart1.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative;
            sideBySideBarSeriesLabel1.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.Default;
            this.xrChart1.SeriesTemplate.Label = sideBySideBarSeriesLabel1;
            this.xrChart1.SeriesTemplate.ValueDataMembersSerializable = "Values";
            this.xrChart1.SizeF = new System.Drawing.SizeF(969.5F, 159.7916F);
            // 
            // xrPivotGrid1
            // 
            this.xrPivotGrid1.Appearance.FieldHeader.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.xrPivotGrid1.Appearance.FieldValue.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.xrPivotGrid1.Appearance.FieldValueGrandTotal.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.xrPivotGrid1.Fields.AddRange(new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField[] {
            this.xrPivotGridField1,
            this.xrPivotGridField3,
            this.xrPivotGridField2,
            this.xrPivotGridField4,
            this.xrPivotGridField5});
            this.xrPivotGrid1.LocationFloat = new DevExpress.Utils.PointFloat(41.125F, 10.00001F);
            this.xrPivotGrid1.Name = "xrPivotGrid1";
            this.xrPivotGrid1.OptionsDataField.AreaIndex = 0;
            this.xrPivotGrid1.OptionsPrint.FilterSeparatorBarPadding = 3;
            this.xrPivotGrid1.OptionsView.ShowColumnHeaders = false;
            this.xrPivotGrid1.OptionsView.ShowDataHeaders = false;
            this.xrPivotGrid1.SizeF = new System.Drawing.SizeF(756.113F, 92.71F);
            // 
            // xrPivotGridField1
            // 
            this.xrPivotGridField1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.xrPivotGridField1.AreaIndex = 0;
            this.xrPivotGridField1.Caption = "ተቋም";
            this.xrPivotGridField1.FieldName = "DescriptionAm";
            this.xrPivotGridField1.Name = "xrPivotGridField1";
            this.xrPivotGridField1.UnboundFieldName = "xrPivotGridField1";
            this.xrPivotGridField1.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.xrPivotGridField1.Width = 300;
            // 
            // xrPivotGridField3
            // 
            this.xrPivotGridField3.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.xrPivotGridField3.AreaIndex = 0;
            this.xrPivotGridField3.Caption = "የውሳኔ አይነት";
            this.xrPivotGridField3.FieldName = "HRMServiceType";
            this.xrPivotGridField3.Name = "xrPivotGridField3";
            this.xrPivotGridField3.Width = 150;
            // 
            // xrPivotGridField2
            // 
            this.xrPivotGridField2.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.xrPivotGridField2.AreaIndex = 0;
            this.xrPivotGridField2.FieldName = "HRActivity";
            this.xrPivotGridField2.Name = "xrPivotGridField2";
            this.xrPivotGridField2.Width = 150;
            // 
            // xrPivotGridField4
            // 
            this.xrPivotGridField4.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.xrPivotGridField4.AreaIndex = 1;
            this.xrPivotGridField4.Caption = "የውሳኔ አይነት";
            this.xrPivotGridField4.FieldName = "HRActivity1";
            this.xrPivotGridField4.Name = "xrPivotGridField4";
            this.xrPivotGridField4.Visible = false;
            this.xrPivotGridField4.Width = 150;
            // 
            // xrPivotGridField5
            // 
            this.xrPivotGridField5.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.xrPivotGridField5.AreaIndex = 1;
            this.xrPivotGridField5.Caption = "የውሳኔ አይነት";
            this.xrPivotGridField5.FieldName = "InspectionDecision";
            this.xrPivotGridField5.Name = "xrPivotGridField5";
            this.xrPivotGridField5.Width = 150;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrChart1,
            this.xrPivotGrid1});
            this.GroupHeader1.HeightF = 361.4582F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.Label36,
            this.lblPage,
            this.xrPageInfo1,
            this.xrLine2,
            this.lbldate});
            this.PageFooter.Name = "PageFooter";
            // 
            // Label36
            // 
            this.Label36.BackColor = System.Drawing.Color.Transparent;
            this.Label36.CanGrow = false;
            this.Label36.Font = new System.Drawing.Font("Nyala", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label36.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Label36.LocationFloat = new DevExpress.Utils.PointFloat(776.6669F, 27.37497F);
            this.Label36.Multiline = true;
            this.Label36.Name = "Label36";
            this.Label36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Label36.SizeF = new System.Drawing.SizeF(100F, 19F);
            this.Label36.StylePriority.UseFont = false;
            this.Label36.Tag = "Print Date/yHTmT qN";
            this.Label36.Text = "የህትመት ቀን :";
            this.Label36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblPage
            // 
            this.lblPage.BackColor = System.Drawing.Color.Transparent;
            this.lblPage.CanGrow = false;
            this.lblPage.Font = new System.Drawing.Font("Nyala", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblPage.LocationFloat = new DevExpress.Utils.PointFloat(3.125F, 20.91667F);
            this.lblPage.Multiline = true;
            this.lblPage.Name = "lblPage";
            this.lblPage.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPage.SizeF = new System.Drawing.SizeF(38F, 19F);
            this.lblPage.StylePriority.UseFont = false;
            this.lblPage.Tag = "Page/gI";
            this.lblPage.Text = "ገፅ";
            this.lblPage.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(41.125F, 20.91667F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(75F, 25F);
            // 
            // xrLine2
            // 
            this.xrLine2.BackColor = System.Drawing.Color.Transparent;
            this.xrLine2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine2.LineWidth = 2;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(21.5F, 10.41667F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(958F, 10.5F);
            this.xrLine2.StylePriority.UseBackColor = false;
            this.xrLine2.StylePriority.UseForeColor = false;
            // 
            // lbldate
            // 
            this.lbldate.ForeColor = System.Drawing.Color.Black;
            this.lbldate.LocationFloat = new DevExpress.Utils.PointFloat(876.6669F, 27.37497F);
            this.lbldate.Name = "lbldate";
            this.lbldate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbldate.SizeF = new System.Drawing.SizeF(105.3333F, 18.04169F);
            this.lbldate.StylePriority.UseForeColor = false;
            this.lbldate.StylePriority.UseTextAlignment = false;
            this.lbldate.Text = "lbldate";
            this.lbldate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // HrActivitySummery
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.GroupHeader1,
            this.PageFooter});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(53, 55, 143, 21);
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.Version = "13.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.HrActivitySummery_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        public DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        public DevExpress.XtraReports.UI.XRPivotGrid xrPivotGrid1;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField xrPivotGridField1;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField xrPivotGridField3;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField xrPivotGridField2;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField xrPivotGridField4;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField xrPivotGridField5;
        private DevExpress.XtraReports.UI.XRChart xrChart1;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox3;
        private DevExpress.XtraReports.UI.XRLabel lblOrganizationNameEng;
        private DevExpress.XtraReports.UI.XRLabel lblOrganizationNameAmh;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRLabel Label36;
        private DevExpress.XtraReports.UI.XRLabel lblPage;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel lbldate;
    }
}
