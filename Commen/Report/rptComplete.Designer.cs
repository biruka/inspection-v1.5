﻿namespace Commen.Report
{
    partial class rptComplete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.InspectionDate = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.UnnecessaryCost = new DevExpress.XtraReports.Parameters.Parameter();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.lblOrganizationNameAmh = new DevExpress.XtraReports.UI.XRLabel();
            this.lblOrganizationNameEng = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox3 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrReportedBy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrOrganization = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrComplainType = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrFrom = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDateFrom = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDateTo = new DevExpress.XtraReports.UI.XRLabel();
            this.FullName = new DevExpress.XtraReports.Parameters.Parameter();
            this.job_title = new DevExpress.XtraReports.Parameters.Parameter();
            this.AmGrade = new DevExpress.XtraReports.Parameters.Parameter();
            this.salary = new DevExpress.XtraReports.Parameters.Parameter();
            this.InspectionFinding = new DevExpress.XtraReports.Parameters.Parameter();
            this.row = new DevExpress.XtraReports.Parameters.Parameter();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.lbldate = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPage = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.Label36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail.HeightF = 24.00001F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.BorderColor = System.Drawing.Color.Black;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(964.5209F, 24F);
            this.xrTable2.StylePriority.UseBorderColor = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell11});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "row")});
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.Text = "xrTableCell4";
            this.xrTableCell4.Weight = 0.34732216406498218D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "FullName")});
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "xrTableCell5";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell5.Weight = 1.86330568202626D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "job_title")});
            this.xrTableCell7.Multiline = true;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "xrTableCell7";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell7.Weight = 1.7125818871009035D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AmGrade")});
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.Text = "xrTableCell8";
            this.xrTableCell8.Weight = 0.72361786548060447D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "salary")});
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.Text = "xrTableCell9";
            this.xrTableCell9.Weight = 0.93004230628873252D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.InspectionDate, "Text", "")});
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.Text = "xrTableCell10";
            this.xrTableCell10.Weight = 1.1724903563268823D;
            this.xrTableCell10.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell10_BeforePrint);
            // 
            // InspectionDate
            // 
            this.InspectionDate.Description = "InspectionDate";
            this.InspectionDate.Name = "InspectionDate";
            this.InspectionDate.Type = typeof(System.DateTime);
            this.InspectionDate.ValueInfo = "10/09/2013 16:54:42";
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "InspectionFinding")});
            this.xrTableCell11.Multiline = true;
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.Text = "xrTableCell11";
            this.xrTableCell11.Weight = 2.0663991857348627D;
            // 
            // UnnecessaryCost
            // 
            this.UnnecessaryCost.Description = "UnnecessaryCost";
            this.UnnecessaryCost.Name = "UnnecessaryCost";
            this.UnnecessaryCost.Type = typeof(double);
            this.UnnecessaryCost.ValueInfo = "0";
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1,
            this.lblOrganizationNameAmh,
            this.lblOrganizationNameEng,
            this.xrPictureBox3});
            this.TopMargin.HeightF = 151.0001F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine1
            // 
            this.xrLine1.BackColor = System.Drawing.Color.Transparent;
            this.xrLine1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine1.LineWidth = 2;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 137.7917F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(958F, 10.5F);
            this.xrLine1.StylePriority.UseBackColor = false;
            this.xrLine1.StylePriority.UseForeColor = false;
            // 
            // lblOrganizationNameAmh
            // 
            this.lblOrganizationNameAmh.BackColor = System.Drawing.Color.Transparent;
            this.lblOrganizationNameAmh.CanGrow = false;
            this.lblOrganizationNameAmh.Font = new System.Drawing.Font("Nyala", 14.5F, System.Drawing.FontStyle.Bold);
            this.lblOrganizationNameAmh.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblOrganizationNameAmh.LocationFloat = new DevExpress.Utils.PointFloat(134.0624F, 51.16665F);
            this.lblOrganizationNameAmh.Multiline = true;
            this.lblOrganizationNameAmh.Name = "lblOrganizationNameAmh";
            this.lblOrganizationNameAmh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblOrganizationNameAmh.SizeF = new System.Drawing.SizeF(724.3334F, 24F);
            this.lblOrganizationNameAmh.StylePriority.UseFont = false;
            this.lblOrganizationNameAmh.Tag = "Human Resource Managment System / የሰው ሃይል መዋቅር አስተዳደር ሲስተም";
            this.lblOrganizationNameAmh.Text = "lblOrganizationNameAmh";
            this.lblOrganizationNameAmh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblOrganizationNameEng
            // 
            this.lblOrganizationNameEng.BackColor = System.Drawing.Color.Transparent;
            this.lblOrganizationNameEng.CanGrow = false;
            this.lblOrganizationNameEng.Font = new System.Drawing.Font("Nyala", 14.5F, System.Drawing.FontStyle.Bold);
            this.lblOrganizationNameEng.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblOrganizationNameEng.LocationFloat = new DevExpress.Utils.PointFloat(134.0624F, 75.16666F);
            this.lblOrganizationNameEng.Multiline = true;
            this.lblOrganizationNameEng.Name = "lblOrganizationNameEng";
            this.lblOrganizationNameEng.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblOrganizationNameEng.SizeF = new System.Drawing.SizeF(724.3334F, 24.00001F);
            this.lblOrganizationNameEng.StylePriority.UseFont = false;
            this.lblOrganizationNameEng.Tag = "Human Resource Managment System / የሰው ሃይል መዋቅር አስተዳደር ሲስተም";
            this.lblOrganizationNameEng.Text = "lblOrganizationNameEng";
            this.lblOrganizationNameEng.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrPictureBox3
            // 
            this.xrPictureBox3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBox3.ImageUrl = "~\\Logo\\Logo.gif";
            this.xrPictureBox3.LocationFloat = new DevExpress.Utils.PointFloat(6.479065F, 33.06251F);
            this.xrPictureBox3.Name = "xrPictureBox3";
            this.xrPictureBox3.SizeF = new System.Drawing.SizeF(114.33F, 101.08F);
            this.xrPictureBox3.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            this.xrPictureBox3.StylePriority.UseBorders = false;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.BackColor = System.Drawing.Color.Empty;
            this.xrTable1.BorderColor = System.Drawing.Color.Black;
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Font = new System.Drawing.Font("Nyala", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.ForeColor = System.Drawing.Color.Black;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 22.49998F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(964.5209F, 25.21F);
            this.xrTable1.StylePriority.UseBackColor = false;
            this.xrTable1.StylePriority.UseBorderColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseForeColor = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell20,
            this.xrTableCell22,
            this.xrReportedBy,
            this.xrOrganization,
            this.xrTableCell3,
            this.xrComplainType});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.StylePriority.UseBackColor = false;
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Font = new System.Drawing.Font("Nyala", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.Text = global::Commen.Resource1.txtNo;
            this.xrTableCell6.Weight = 0.24024076145699921D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Font = new System.Drawing.Font("Nyala", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.Text = global::Commen.Resource1.txtEmployee;
            this.xrTableCell20.Weight = 1.2888379050128527D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Font = new System.Drawing.Font("Nyala", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.Text = global::Commen.Resource1.txtDescription;
            this.xrTableCell22.Weight = 1.1845830517975278D;
            // 
            // xrReportedBy
            // 
            this.xrReportedBy.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.xrReportedBy.Font = new System.Drawing.Font("Nyala", 12F, System.Drawing.FontStyle.Bold);
            this.xrReportedBy.ForeColor = System.Drawing.Color.Black;
            this.xrReportedBy.Name = "xrReportedBy";
            this.xrReportedBy.StylePriority.UseBackColor = false;
            this.xrReportedBy.StylePriority.UseFont = false;
            this.xrReportedBy.StylePriority.UseForeColor = false;
            this.xrReportedBy.StylePriority.UseTextAlignment = false;
            this.xrReportedBy.Text = global::Commen.Resource1.txtgrade;
            this.xrReportedBy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrReportedBy.Weight = 0.50052270716643821D;
            // 
            // xrOrganization
            // 
            this.xrOrganization.Font = new System.Drawing.Font("Nyala", 12F, System.Drawing.FontStyle.Bold);
            this.xrOrganization.Name = "xrOrganization";
            this.xrOrganization.StylePriority.UseFont = false;
            this.xrOrganization.StylePriority.UseTextAlignment = false;
            this.xrOrganization.Text = global::Commen.Resource1.txtsalary;
            this.xrOrganization.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrOrganization.Weight = 0.64330487046873686D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Font = new System.Drawing.Font("Nyala", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = global::Commen.Resource1.txtEmploymentdate;
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 0.89661671154105516D;
            // 
            // xrComplainType
            // 
            this.xrComplainType.Font = new System.Drawing.Font("Nyala", 12F, System.Drawing.FontStyle.Bold);
            this.xrComplainType.Name = "xrComplainType";
            this.xrComplainType.StylePriority.UseFont = false;
            this.xrComplainType.StylePriority.UseTextAlignment = false;
            this.xrComplainType.Text = global::Commen.Resource1.txtDecisionRemark;
            this.xrComplainType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrComplainType.Weight = 1.3437037158204133D;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.Empty;
            this.xrLabel1.Font = new System.Drawing.Font("Nyala", 14.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(111.3804F, 10.00001F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(724.33F, 22.58F);
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = global::Commen.Resource1.txtRHeader;
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrFrom
            // 
            this.xrFrom.BackColor = System.Drawing.Color.Transparent;
            this.xrFrom.Font = new System.Drawing.Font("Nyala", 12F);
            this.xrFrom.LocationFloat = new DevExpress.Utils.PointFloat(318.075F, 52.79166F);
            this.xrFrom.Name = "xrFrom";
            this.xrFrom.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrFrom.SizeF = new System.Drawing.SizeF(17.00006F, 20.125F);
            this.xrFrom.StylePriority.UseBackColor = false;
            this.xrFrom.StylePriority.UseFont = false;
            this.xrFrom.StylePriority.UseTextAlignment = false;
            this.xrFrom.Text = global::Commen.Resource1.txtFrom;
            this.xrFrom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrDateFrom
            // 
            this.xrDateFrom.BackColor = System.Drawing.Color.Transparent;
            this.xrDateFrom.Font = new System.Drawing.Font("Nyala", 14F, System.Drawing.FontStyle.Underline);
            this.xrDateFrom.LocationFloat = new DevExpress.Utils.PointFloat(335.0751F, 52.79166F);
            this.xrDateFrom.Name = "xrDateFrom";
            this.xrDateFrom.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDateFrom.SizeF = new System.Drawing.SizeF(86.6F, 20.13F);
            this.xrDateFrom.StylePriority.UseBackColor = false;
            this.xrDateFrom.StylePriority.UseFont = false;
            this.xrDateFrom.StylePriority.UseTextAlignment = false;
            this.xrDateFrom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTo
            // 
            this.xrTo.BackColor = System.Drawing.Color.Transparent;
            this.xrTo.Font = new System.Drawing.Font("Nyala", 12F);
            this.xrTo.LocationFloat = new DevExpress.Utils.PointFloat(421.675F, 52.79665F);
            this.xrTo.Name = "xrTo";
            this.xrTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTo.SizeF = new System.Drawing.SizeF(34.70828F, 20.12502F);
            this.xrTo.StylePriority.UseBackColor = false;
            this.xrTo.StylePriority.UseFont = false;
            this.xrTo.StylePriority.UseTextAlignment = false;
            this.xrTo.Text = global::Commen.Resource1.txtTo;
            this.xrTo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrDateTo
            // 
            this.xrDateTo.BackColor = System.Drawing.Color.Transparent;
            this.xrDateTo.BorderColor = System.Drawing.Color.Transparent;
            this.xrDateTo.Font = new System.Drawing.Font("Nyala", 14F, System.Drawing.FontStyle.Underline);
            this.xrDateTo.LocationFloat = new DevExpress.Utils.PointFloat(456.3833F, 52.79665F);
            this.xrDateTo.Name = "xrDateTo";
            this.xrDateTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDateTo.SizeF = new System.Drawing.SizeF(86.6F, 20.13F);
            this.xrDateTo.StylePriority.UseBackColor = false;
            this.xrDateTo.StylePriority.UseBorderColor = false;
            this.xrDateTo.StylePriority.UseFont = false;
            // 
            // FullName
            // 
            this.FullName.Description = "FullName";
            this.FullName.Name = "FullName";
            // 
            // job_title
            // 
            this.job_title.Description = "job_title";
            this.job_title.Name = "job_title";
            // 
            // AmGrade
            // 
            this.AmGrade.Description = "AmGrade";
            this.AmGrade.Name = "AmGrade";
            // 
            // salary
            // 
            this.salary.Description = "salary";
            this.salary.Name = "salary";
            this.salary.Type = typeof(decimal);
            this.salary.ValueInfo = "0";
            // 
            // InspectionFinding
            // 
            this.InspectionFinding.Description = "InspectionFinding";
            this.InspectionFinding.Name = "InspectionFinding";
            // 
            // row
            // 
            this.row.Description = "row";
            this.row.Name = "row";
            this.row.Type = typeof(int);
            this.row.ValueInfo = "0";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.GroupHeader1.HeightF = 47.70998F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrTo,
            this.xrDateFrom,
            this.xrDateTo,
            this.xrFrom});
            this.PageHeader.HeightF = 77.08334F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lbldate,
            this.lblPage,
            this.xrLine2,
            this.Label36,
            this.xrPageInfo1});
            this.PageFooter.HeightF = 61.45833F;
            this.PageFooter.Name = "PageFooter";
            // 
            // lbldate
            // 
            this.lbldate.ForeColor = System.Drawing.Color.Black;
            this.lbldate.LocationFloat = new DevExpress.Utils.PointFloat(851.1458F, 26.50001F);
            this.lbldate.Name = "lbldate";
            this.lbldate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbldate.SizeF = new System.Drawing.SizeF(105.3333F, 18.04169F);
            this.lbldate.StylePriority.UseForeColor = false;
            this.lbldate.StylePriority.UseTextAlignment = false;
            this.lbldate.Text = "lbldate";
            this.lbldate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblPage
            // 
            this.lblPage.BackColor = System.Drawing.Color.Transparent;
            this.lblPage.CanGrow = false;
            this.lblPage.Font = new System.Drawing.Font("Nyala", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblPage.LocationFloat = new DevExpress.Utils.PointFloat(6.479064F, 26.50001F);
            this.lblPage.Multiline = true;
            this.lblPage.Name = "lblPage";
            this.lblPage.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPage.SizeF = new System.Drawing.SizeF(38F, 19F);
            this.lblPage.StylePriority.UseFont = false;
            this.lblPage.Tag = "Page/gI";
            this.lblPage.Text = "ገፅ";
            this.lblPage.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLine2
            // 
            this.xrLine2.BackColor = System.Drawing.Color.Transparent;
            this.xrLine2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrLine2.LineWidth = 2;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(6.479064F, 10.00001F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(958F, 10.5F);
            this.xrLine2.StylePriority.UseBackColor = false;
            this.xrLine2.StylePriority.UseForeColor = false;
            // 
            // Label36
            // 
            this.Label36.BackColor = System.Drawing.Color.Transparent;
            this.Label36.CanGrow = false;
            this.Label36.Font = new System.Drawing.Font("Nyala", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label36.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Label36.LocationFloat = new DevExpress.Utils.PointFloat(751.1458F, 26.50001F);
            this.Label36.Multiline = true;
            this.Label36.Name = "Label36";
            this.Label36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Label36.SizeF = new System.Drawing.SizeF(100F, 19F);
            this.Label36.StylePriority.UseFont = false;
            this.Label36.Tag = "Print Date/yHTmT qN";
            this.Label36.Text = "የህትመት ቀን :";
            this.Label36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(45.80908F, 26.50001F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(75F, 25F);
            // 
            // rptComplete
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.GroupHeader1,
            this.PageHeader,
            this.PageFooter});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(74, 55, 151, 100);
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.FullName,
            this.job_title,
            this.AmGrade,
            this.salary,
            this.InspectionDate,
            this.InspectionFinding,
            this.row,
            this.UnnecessaryCost});
            this.Version = "13.2";
            this.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.rptComplete_BeforePrint);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        public DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrFrom;
        private DevExpress.XtraReports.UI.XRLabel xrDateFrom;
        private DevExpress.XtraReports.UI.XRLabel xrTo;
        private DevExpress.XtraReports.UI.XRLabel xrDateTo;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrReportedBy;
        private DevExpress.XtraReports.UI.XRTableCell xrOrganization;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrComplainType;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.Parameters.Parameter FullName;
        private DevExpress.XtraReports.Parameters.Parameter job_title;
        private DevExpress.XtraReports.Parameters.Parameter AmGrade;
        private DevExpress.XtraReports.Parameters.Parameter salary;
        private DevExpress.XtraReports.Parameters.Parameter InspectionDate;
        private DevExpress.XtraReports.Parameters.Parameter InspectionFinding;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.Parameters.Parameter row;
        private DevExpress.XtraReports.Parameters.Parameter UnnecessaryCost;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel lblOrganizationNameAmh;
        private DevExpress.XtraReports.UI.XRLabel lblOrganizationNameEng;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox3;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRLabel lbldate;
        private DevExpress.XtraReports.UI.XRLabel lblPage;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel Label36;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
    }
}
