﻿ 



using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Configuration;
 
using CUSTOR.Commen;
namespace Commen.Report
{
    public partial class rptPromotionComplete : DevExpress.XtraReports.UI.XtraReport
    {
        public rptPromotionComplete()
        {
            InitializeComponent();
        }
     
        private void rptComplete_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            CGeneralSetting.ReadSetting();
            
            lblOrganizationNameAmh.Text = ConfigurationManager.AppSettings.Get("OrganizationNameAmh").ToString();
            lblOrganizationNameEng.Text = ConfigurationManager.AppSettings.Get("OrganizationName").ToString();
             
            lbldate.Text = EthiopicDateTime.GetEthiopicDate(DateTime.Today.Day, DateTime.Today.Month, DateTime.Today.Year);
 

        //    if (xrTableCell23.Text.ToString() != string.Empty)
        //{
        //    DateTime dtt = Convert.ToDateTime(xrTableCell23.Text.ToString());
        //    string date = Convert.ToString(EthiopicDateTime.GetEthiopicDate(dtt.Day, dtt.Month, dtt.Year));
        //    xrTableCell23.Text = date;
        //}
        }

        private void xrTableCell23_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (xrTableCell23.Text.ToString() != string.Empty)
            {
                //DateTime dtt = Convert.ToDateTime(xrTableCell23.Text.ToString());
                //string date = Convert.ToString(EthiopicDateTime.GetEthiopicDate(dtt.Day, dtt.Month, dtt.Year));
                //xrTableCell23.Text = date;
            }
        }
    }
}
