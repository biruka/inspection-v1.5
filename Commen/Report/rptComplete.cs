﻿
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Configuration;
using CUSTOR.Commen;

namespace Commen.Report
{
    public partial class rptComplete : DevExpress.XtraReports.UI.XtraReport
    {
        public rptComplete()
        {
            InitializeComponent();
        }
     
        private void rptComplete_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            CGeneralSetting.ReadSetting();
            lblOrganizationNameAmh.Text = ConfigurationManager.AppSettings.Get("OrganizationNameAmh").ToString();
            lblOrganizationNameEng.Text = ConfigurationManager.AppSettings.Get("OrganizationName").ToString();

            lbldate.Text = EthiopicDateTime.GetEthiopicDate(DateTime.Today.Day, DateTime.Today.Month, DateTime.Today.Year);
            //xrTableCell10.Text
        }

        private void xrTableCell10_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (xrTableCell10.Text.ToString() != string.Empty)
            {
                DateTime dtt = Convert.ToDateTime(xrTableCell10.Text.ToString());
                string date = Convert.ToString(EthiopicDateTime.GetEthiopicDate(dtt.Day, dtt.Month, dtt.Year));
                xrTableCell10.Text = date;
            }
        }
    }
}
