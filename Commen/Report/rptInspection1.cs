﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using CUSTOR.Commen;

namespace Report
{
    public partial class rptInspection : DevExpress.XtraReports.UI.XtraReport
    {
        public rptInspection()
        {
            InitializeComponent();
        }

        public void ShowDateRange()
        {
            DateTime minDate = new DateTime();
            if (!minDate.ToString().Equals(dateFrom.ToString()))
            {
                xrDateFrom.Text = EthiopicDateTime.TranslateDateMonth(dateFrom);
            }
            else
            {
                xrFrom.Visible = false;
                xrDateFrom.Visible = false;
            }
            if (!minDate.ToString().Equals(dateTo.ToString()))
            {
                xrDateTo.Text = EthiopicDateTime.TranslateDateMonth(dateTo);
            }
            else
            {
                xrTo.Visible = false;
                xrDateTo.Visible = false;
            }
       }

        private void ReportHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            CGeneralSetting.ReadSetting();

            txtReportTitleAmh.Text = CGeneralSetting.ReportTitleAmh;
            txtReportTitle.Text = CGeneralSetting.ReportTitle;
            txtReportSubTitle.Text = CGeneralSetting.SubReportTitle;
            txtReportSubTitleAmh.Text = CGeneralSetting.SubReportTitleAmh;
        }
    }
}
