﻿ 
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Configuration;
using CUSTOR.Commen;

namespace Commen.Report 
{
    public partial class HrActivitySummery : DevExpress.XtraReports.UI.XtraReport
    {
        public HrActivitySummery()
        {
            InitializeComponent();
        }

        private void HrActivitySummery_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            CGeneralSetting.ReadSetting();
            lblOrganizationNameAmh.Text = ConfigurationManager.AppSettings.Get("OrganizationNameAmh").ToString();
            lblOrganizationNameEng.Text = ConfigurationManager.AppSettings.Get("OrganizationName").ToString();
            //lbldate.Text = "የህትመት ቀን: " + (CUSTOR.EthiopicDateTime.GetEthiopicDate(Convert.ToInt32(DateTime.Now.Day), Convert.ToInt32(DateTime.Now.Month), Convert.ToInt32(DateTime.Now.Year))).ToString();
            lbldate.Text = EthiopicDateTime.GetEthiopicDate(DateTime.Today.Day, DateTime.Today.Month, DateTime.Today.Year);
        }

    }
}
