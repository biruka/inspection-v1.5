﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Configuration;

using CUSTOR.Commen;
namespace Commen.Report
{
    public partial class rptTransferComplete : DevExpress.XtraReports.UI.XtraReport
    {
        public rptTransferComplete()
        {
            InitializeComponent();
        }
     
        private void rptComplete_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            CGeneralSetting.ReadSetting();
            lblOrganizationNameAmh.Text = ConfigurationManager.AppSettings.Get("OrganizationNameAmh").ToString();
            lblOrganizationNameEng.Text = ConfigurationManager.AppSettings.Get("OrganizationName").ToString();

            lbldate.Text = EthiopicDateTime.GetEthiopicDate(DateTime.Today.Day, DateTime.Today.Month, DateTime.Today.Year);


    
      
    

        }
    }
}
