﻿using System;
namespace Report
{
    partial class rptInspection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptInspection));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrRichText1 = new DevExpress.XtraReports.UI.XRRichText();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDateTo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDateFrom = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrFrom = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.FullName = new DevExpress.XtraReports.Parameters.Parameter();
            this.InspectionDecision = new DevExpress.XtraReports.Parameters.Parameter();
            this.InspectionFinding = new DevExpress.XtraReports.Parameters.Parameter();
            this.DescriptionAm = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.txtReportTitleAmh = new DevExpress.XtraReports.UI.XRLabel();
            this.txtReportSubTitleAmh = new DevExpress.XtraReports.UI.XRLabel();
            this.txtReportTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.txtReportSubTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrRichText2 = new DevExpress.XtraReports.UI.XRRichText();
            this.Answer = new DevExpress.XtraReports.Parameters.Parameter();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrRichText2,
            this.xrLabel1,
            this.xrLine4,
            this.xrRichText1,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel4});
            this.Detail.HeightF = 125F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine4
            // 
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.041667F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(1169F, 2F);
            // 
            // xrRichText1
            // 
            this.xrRichText1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrRichText1.BackColor = System.Drawing.Color.White;
            this.xrRichText1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Rtf", null, "InspectionFinding")});
            this.xrRichText1.Font = new System.Drawing.Font("Nyala", 9.75F);
            this.xrRichText1.LocationFloat = new DevExpress.Utils.PointFloat(412.5413F, 5.125014F);
            this.xrRichText1.Name = "xrRichText1";
            this.xrRichText1.SerializableRtfString = resources.GetString("xrRichText1.SerializableRtfString");
            this.xrRichText1.SizeF = new System.Drawing.SizeF(392.1041F, 82.37497F);
            this.xrRichText1.StylePriority.UseBackColor = false;
            this.xrRichText1.StylePriority.UseFont = false;
            // 
            // xrLabel9
            // 
            this.xrLabel9.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel9.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrLabel9.Font = new System.Drawing.Font("Nyala", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(304.1666F, 3.041666F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel9.StylePriority.UseBackColor = false;
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.Text = "የውሳኔው ቃለ ጉባኤ:";
            // 
            // xrLabel8
            // 
            this.xrLabel8.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel8.BackColor = System.Drawing.Color.White;
            this.xrLabel8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "InspectionDecision")});
            this.xrLabel8.Font = new System.Drawing.Font("Nyala", 9.75F);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(114.2292F, 37.75002F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(141.6666F, 23F);
            this.xrLabel8.StylePriority.UseBackColor = false;
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.Text = " ዠ፡";
            // 
            // xrLabel7
            // 
            this.xrLabel7.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrLabel7.Font = new System.Drawing.Font("Nyala", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(4.166666F, 37.75002F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel7.StylePriority.UseBackColor = false;
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.Text = "የውሳኔው አይነት:";
            // 
            // xrLabel6
            // 
            this.xrLabel6.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel6.BorderColor = System.Drawing.Color.White;
            this.xrLabel6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "FullName")});
            this.xrLabel6.Font = new System.Drawing.Font("Nyala", 9.75F);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(114.2291F, 3.041666F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(141.6667F, 23F);
            this.xrLabel6.StylePriority.UseBorderColor = false;
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.Text = "xrLabel6";
            // 
            // xrLabel4
            // 
            this.xrLabel4.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top;
            this.xrLabel4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrLabel4.Font = new System.Drawing.Font("Nyala", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(4.166664F, 3.041667F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel4.StylePriority.UseBackColor = false;
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.Text = "ሙሉ ስም:";
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(4.166664F, 0F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(1169F, 4.583336F);
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox1,
            this.txtReportTitleAmh,
            this.txtReportSubTitleAmh,
            this.txtReportTitle,
            this.txtReportSubTitle});
            this.TopMargin.HeightF = 192.6667F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1});
            this.BottomMargin.HeightF = 83.20834F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.BackColor = System.Drawing.Color.BurlyWood;
            this.xrLabel5.Font = new System.Drawing.Font("Nyala", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(1159F, 33F);
            this.xrLabel5.StylePriority.UseBackColor = false;
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "የሰው ሀብት ክንውን ግምገማ ግኝት";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrDateTo
            // 
            this.xrDateTo.BackColor = System.Drawing.Color.Transparent;
            this.xrDateTo.BorderColor = System.Drawing.Color.Transparent;
            this.xrDateTo.Font = new System.Drawing.Font("Nyala", 14F);
            this.xrDateTo.LocationFloat = new DevExpress.Utils.PointFloat(589.6665F, 33F);
            this.xrDateTo.Name = "xrDateTo";
            this.xrDateTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDateTo.SizeF = new System.Drawing.SizeF(162.4582F, 20.12502F);
            this.xrDateTo.StylePriority.UseBackColor = false;
            this.xrDateTo.StylePriority.UseBorderColor = false;
            this.xrDateTo.StylePriority.UseFont = false;
            // 
            // xrDateFrom
            // 
            this.xrDateFrom.BackColor = System.Drawing.Color.Transparent;
            this.xrDateFrom.Font = new System.Drawing.Font("Nyala", 14F);
            this.xrDateFrom.LocationFloat = new DevExpress.Utils.PointFloat(448.9579F, 33F);
            this.xrDateFrom.Name = "xrDateFrom";
            this.xrDateFrom.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDateFrom.SizeF = new System.Drawing.SizeF(103.1668F, 20.12502F);
            this.xrDateFrom.StylePriority.UseBackColor = false;
            this.xrDateFrom.StylePriority.UseFont = false;
            this.xrDateFrom.StylePriority.UseTextAlignment = false;
            this.xrDateFrom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTo
            // 
            this.xrTo.BackColor = System.Drawing.Color.Transparent;
            this.xrTo.Font = new System.Drawing.Font("Nyala", 12F);
            this.xrTo.LocationFloat = new DevExpress.Utils.PointFloat(552.1246F, 33F);
            this.xrTo.Name = "xrTo";
            this.xrTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTo.SizeF = new System.Drawing.SizeF(37.54184F, 20.12502F);
            this.xrTo.StylePriority.UseBackColor = false;
            this.xrTo.StylePriority.UseFont = false;
            this.xrTo.StylePriority.UseTextAlignment = false;
            this.xrTo.Text = "እስከ:";
            this.xrTo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrFrom
            // 
            this.xrFrom.BackColor = System.Drawing.Color.Transparent;
            this.xrFrom.Font = new System.Drawing.Font("Nyala", 12F);
            this.xrFrom.LocationFloat = new DevExpress.Utils.PointFloat(412.5413F, 33F);
            this.xrFrom.Name = "xrFrom";
            this.xrFrom.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrFrom.SizeF = new System.Drawing.SizeF(36.4166F, 20.125F);
            this.xrFrom.StylePriority.UseBackColor = false;
            this.xrFrom.StylePriority.UseFont = false;
            this.xrFrom.StylePriority.UseTextAlignment = false;
            this.xrFrom.Text = "ከ:";
            this.xrFrom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2,
            this.xrLabel3});
            this.GroupHeader1.HeightF = 25F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrLabel2
            // 
            this.xrLabel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DescriptionAm")});
            this.xrLabel2.Font = new System.Drawing.Font("Nyala", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(104.1667F, 0F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(201.0417F, 23F);
            this.xrLabel2.StylePriority.UseBackColor = false;
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "xrLabel2";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrLabel3.Font = new System.Drawing.Font("Nyala", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(4.166664F, 0F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel3.StylePriority.UseBackColor = false;
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "ድርጅት";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel5,
            this.xrDateTo,
            this.xrDateFrom,
            this.xrTo,
            this.xrFrom});
            this.ReportHeader.HeightF = 53.20832F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ReportHeader_BeforePrint);
            // 
            // FullName
            // 
            this.FullName.Description = "FullName";
            this.FullName.Name = "FullName";
            this.FullName.Value = "";
            // 
            // InspectionDecision
            // 
            this.InspectionDecision.Description = "InspectionDecision";
            this.InspectionDecision.Name = "InspectionDecision";
            this.InspectionDecision.Value = "";
            // 
            // InspectionFinding
            // 
            this.InspectionFinding.Description = "InspectionFinding";
            this.InspectionFinding.Name = "InspectionFinding";
            this.InspectionFinding.Value = "";
            // 
            // DescriptionAm
            // 
            this.DescriptionAm.Description = "DescriptionAm";
            this.DescriptionAm.Name = "DescriptionAm";
            this.DescriptionAm.Value = "";
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.xrPictureBox1.ImageUrl = "~\\Images\\EthLogo.gif";
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(499.6665F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(90F, 76F);
            this.xrPictureBox1.StylePriority.UseBackColor = false;
            // 
            // txtReportTitleAmh
            // 
            this.txtReportTitleAmh.BackColor = System.Drawing.Color.Transparent;
            this.txtReportTitleAmh.Font = new System.Drawing.Font("Nyala", 15F);
            this.txtReportTitleAmh.ForeColor = System.Drawing.Color.Black;
            this.txtReportTitleAmh.LocationFloat = new DevExpress.Utils.PointFloat(86.64169F, 75.99999F);
            this.txtReportTitleAmh.Name = "txtReportTitleAmh";
            this.txtReportTitleAmh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtReportTitleAmh.SizeF = new System.Drawing.SizeF(925F, 29.16667F);
            this.txtReportTitleAmh.StylePriority.UseBackColor = false;
            this.txtReportTitleAmh.StylePriority.UseFont = false;
            this.txtReportTitleAmh.StylePriority.UseForeColor = false;
            this.txtReportTitleAmh.StylePriority.UseTextAlignment = false;
            this.txtReportTitleAmh.Text = "የኢትዮጵያ ፌዴራዊ ዲሞክራሲያዊ ሪፐብሊክ";
            this.txtReportTitleAmh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txtReportSubTitleAmh
            // 
            this.txtReportSubTitleAmh.BackColor = System.Drawing.Color.Transparent;
            this.txtReportSubTitleAmh.Font = new System.Drawing.Font("Nyala", 15F);
            this.txtReportSubTitleAmh.ForeColor = System.Drawing.Color.Black;
            this.txtReportSubTitleAmh.LocationFloat = new DevExpress.Utils.PointFloat(86.64169F, 105.1667F);
            this.txtReportSubTitleAmh.Name = "txtReportSubTitleAmh";
            this.txtReportSubTitleAmh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtReportSubTitleAmh.SizeF = new System.Drawing.SizeF(925F, 29.16667F);
            this.txtReportSubTitleAmh.StylePriority.UseBackColor = false;
            this.txtReportSubTitleAmh.StylePriority.UseFont = false;
            this.txtReportSubTitleAmh.StylePriority.UseForeColor = false;
            this.txtReportSubTitleAmh.StylePriority.UseTextAlignment = false;
            this.txtReportSubTitleAmh.Text = "የኢትዮጵያ ፌዴራዊ ዲሞክራሲያዊ ሪፐብሊክ";
            this.txtReportSubTitleAmh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txtReportTitle
            // 
            this.txtReportTitle.BackColor = System.Drawing.Color.Transparent;
            this.txtReportTitle.Font = new System.Drawing.Font("Nyala", 15F);
            this.txtReportTitle.ForeColor = System.Drawing.Color.Black;
            this.txtReportTitle.LocationFloat = new DevExpress.Utils.PointFloat(86.64169F, 134.3334F);
            this.txtReportTitle.Name = "txtReportTitle";
            this.txtReportTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtReportTitle.SizeF = new System.Drawing.SizeF(925F, 29.16667F);
            this.txtReportTitle.StylePriority.UseBackColor = false;
            this.txtReportTitle.StylePriority.UseFont = false;
            this.txtReportTitle.StylePriority.UseForeColor = false;
            this.txtReportTitle.StylePriority.UseTextAlignment = false;
            this.txtReportTitle.Text = "የኢትዮጵያ ፌዴራዊ ዲሞክራሲያዊ ሪፐብሊክ";
            this.txtReportTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txtReportSubTitle
            // 
            this.txtReportSubTitle.BackColor = System.Drawing.Color.Transparent;
            this.txtReportSubTitle.Font = new System.Drawing.Font("Nyala", 15F);
            this.txtReportSubTitle.ForeColor = System.Drawing.Color.Black;
            this.txtReportSubTitle.LocationFloat = new DevExpress.Utils.PointFloat(86.64169F, 163.5001F);
            this.txtReportSubTitle.Name = "txtReportSubTitle";
            this.txtReportSubTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtReportSubTitle.SizeF = new System.Drawing.SizeF(925F, 29.16667F);
            this.txtReportSubTitle.StylePriority.UseBackColor = false;
            this.txtReportSubTitle.StylePriority.UseFont = false;
            this.txtReportSubTitle.StylePriority.UseForeColor = false;
            this.txtReportSubTitle.StylePriority.UseTextAlignment = false;
            this.txtReportSubTitle.Text = "የኢትዮጵያ ፌዴራዊ ዲሞክራሲያዊ ሪፐብሊክ";
            this.txtReportSubTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.xrLabel1.BorderColor = System.Drawing.Color.SaddleBrown;
            this.xrLabel1.Font = new System.Drawing.Font("Nyala", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(304.1666F, 99.91668F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseBorderColor = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "መልስ :";
            // 
            // xrRichText2
            // 
            this.xrRichText2.BorderColor = System.Drawing.Color.White;
            this.xrRichText2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.Answer, "Rtf", "")});
            this.xrRichText2.Font = new System.Drawing.Font("Nyala", 9.75F);
            this.xrRichText2.LocationFloat = new DevExpress.Utils.PointFloat(412.5413F, 99.91668F);
            this.xrRichText2.Name = "xrRichText2";
            this.xrRichText2.SerializableRtfString = resources.GetString("xrRichText2.SerializableRtfString");
            this.xrRichText2.SizeF = new System.Drawing.SizeF(392.1041F, 23F);
            this.xrRichText2.StylePriority.UseBorderColor = false;
            this.xrRichText2.StylePriority.UseFont = false;
            // 
            // Answer
            // 
            this.Answer.Description = "Answer";
            this.Answer.Name = "Answer";
            this.Answer.Value = "";
            // 
            // rptInspection
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.GroupHeader1,
            this.ReportHeader});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 193, 83);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.FullName,
            this.InspectionDecision,
            this.InspectionFinding,
            this.DescriptionAm,
            this.Answer});
            this.Version = "12.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        public DevExpress.XtraReports.UI.XRLabel xrDateTo;
        public DevExpress.XtraReports.UI.XRLabel xrDateFrom;
        private DevExpress.XtraReports.UI.XRLabel xrTo;
        private DevExpress.XtraReports.UI.XRLabel xrFrom;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRRichText xrRichText1;
        private DevExpress.XtraReports.Parameters.Parameter FullName;
        private DevExpress.XtraReports.Parameters.Parameter InspectionDecision;
        private DevExpress.XtraReports.Parameters.Parameter InspectionFinding;
        private DevExpress.XtraReports.Parameters.Parameter DescriptionAm;
        public DateTime dateFrom;
        public DateTime dateTo;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel txtReportTitleAmh;
        private DevExpress.XtraReports.UI.XRLabel txtReportSubTitleAmh;
        private DevExpress.XtraReports.UI.XRLabel txtReportTitle;
        private DevExpress.XtraReports.UI.XRLabel txtReportSubTitle;
        private DevExpress.XtraReports.UI.XRRichText xrRichText2;
        private DevExpress.XtraReports.Parameters.Parameter Answer;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
    }
}
