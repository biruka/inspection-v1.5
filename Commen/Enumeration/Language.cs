﻿using System;
using System.Collections.Generic;
using System.Web;
namespace CUSTOR.Commen
{
    public static class Language
    {
        public enum eLanguage
        {
            eEnglish = 0,
            eAmharic = 1,
            eAfanOromo = 2,
            eTigrigna = 3,
            eAfar = 4,
            eSomali = 5,
            eArabic = 6,
            eFrench = 7
        }
        public  static string GetVisibleDescriptionColumn(int language)
        {
            switch (language)
            {
                case (int)eLanguage.eAmharic:
                    return "AmDescription";
                case (int)eLanguage.eEnglish:
                    return "Description";
                case (int)eLanguage.eTigrigna:
                    return "Tigrigna";
                case (int)eLanguage.eAfanOromo:
                    return "AfanOromo";
                case (int)eLanguage.eAfar:
                    return "Afar";
                case (int)eLanguage.eSomali:
                    return "Somali";
                default:
                    return "AmDescription";
            } 

        }
        public static string GetVisibleDescriptionColumnOther(int language)
        {
            switch (language)
            {
                case (int)eLanguage.eAmharic:
                    return "DescriptionAm";
                case (int)eLanguage.eEnglish:
                    return "Description";
                case (int)eLanguage.eTigrigna:
                    return "DescriptionTig";
                case (int)eLanguage.eAfanOromo:
                    return "DescriptionOr";
                case (int)eLanguage.eAfar:
                    return "DescriptionAf";
                case (int)eLanguage.eSomali:
                    return "DescriptionSom";
                default:
                    return "AmDescription";
            }

        }
    }
}