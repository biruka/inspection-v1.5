﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

/// <summary>
/// Summary description for Enumeration
/// </summary>
namespace CUSTOR.Commen
{
    public class Enumeration
    {
        public enum eOrgType
        {
            [Description("Fedral Civil Service|ፌዴራል ሲቪል ሰርቪስ|Fedral Civil Service|ፌዴራል ሲቪል ሰርቪስ|Fedral Civil Service|Fedral Civil Service")]
            FCivilService=0,

            [Description("Federal Organization|ፌዴራል መ/ቤት|Federal Organization|ፌዴራል መ/ቤት|Federal Organization|Federal Organization")]
            FOrganization,

            [Description("Regional Civil Service|ክልላዊ ሲቪል ሰርቪስ|Regional Civil Service|ክልላዊ ሲቪል ሰርቪስ|Regional Civil Service|Regional Civil Service")]
            RCivilService,

            [Description("Zonal Civil Service|ዞን ሲቪል ሰርቪስ|Zonal Civil Service|ዞን ሲቪል ሰርቪስ|Zonal Civil Service|Zonal Civil Service")]
            ZCivilService,

            [Description("Wereda Civil Service|ወረዳ ሲቪል ሰርቪስ|Wereda Civil Service|ወረዳ ሲቪል ሰርቪስ|Wereda Civil Service|Wereda Civil Service")]
            WCivilService,
            [Description("Regional Organization|ክልላዊ መ/ቤት|Regional Organization|ክልላዊ መ/ቤት|Regional Organization|Regional Organization")]
            ROrganization,
            [Description("Zonal Organization|ዞን መ/ቤት|Zonal Organization|ዞን መ/ቤት|Zonal Organization|Zonal Organization")]
            ZOrganization,
            [Description("Wereda Organization|ወረዳ መ/ቤት|Wereda Organization|ወረዳ መ/ቤት|Wereda Organization|Wereda Organization")]
            WOrganization
        }
        public enum MeansOfReport
        {
            [Description("Individual|በአካል|Qaaman")]
            InPerson = 0,
            [Description("Web|በድረ ገልፅ|Sayitti/Web-Site")]
            Web,
            [Description("Email|በኢሜል|E-Mail dhan")]
            Email,
            [Description("Pobox|በፖስታ|Poostaadhan")]
            Pobox,
            [Description("Telephone|በስልክ|Bilbilaan")]
            Telephone
        }
        public enum ComplainTypes
        {
            [Description("Recruitment|ሰራተኛ ቅጥር|Qacaarri Hojeeta")]
            Recruitment = 311,
            [Description("Promotion|ዕድገት|Guddina")]
            Promotion,
            [Description("Transfer|ዝውውር|Jijjiira")]
            Transfer,
            [Description("ServiceExtension|አገልግሎት ስለመቀጠል|Hojii itti Fuffuu")]
            ServiceExtension,
            [Description("ServiceTermination|አገልግሎት ስለሟቋረጥ|Hojjii Dhabu")]
            ServiceTermination,
            [Description("DisciplinaryMeasuresAndGrievanceHandling|ስለደንብ መተላለፍ ውሳኔ እና ቅሬታ ሰሚ|Dhaga`aa Muurte fi Heyyanoo Seera fi Heera Darbu ")]
            DisciplinaryMeasuresAndGrievanceHandling,
            [Description("HealthAndSafety|ጤና እና ደህንነት|fayyaa fi Iyyuuma")]
            HealthAndSafety,
            [Description("OvertimeWork|የትርፍ ስራ ሰዓት|Hojii Sa`aa  alaa")]
            OvertimeWork,
            [Description("ScholarshipTraining|የትምህርት ዕድል|Addemsa Barnoota")]
            ScholarshipTraining,
            [Description("AuthorityDelegation|የስልጣን ውክልና|Bakka Bu`aa Abba Tayyitaa")]
            AuthorityDelegation
        }
        public enum FindingSummary
        {
            [Description("NotInspected|ያልታየ|Kan Hin Argamnee")]
            NotInspected = 0,
            [Description("Accepted|ተቀባይነት ያገኘ|Fudhatamaa Kan Argattee")]
            Accepted,
            [Description("Incomplete|ያልተሟላ|Kan hin Gutaamne")]
            Incomplete,
            [Description("Invalid|ውድቅ የሆነ|Kan Kuufe")]
            Invalid
        }
        public enum InspectionType
        {
            [Description("RegularInspection|መደበኛ|Dhabata")]
            RegularInspection = 1,
            [Description("complain|ጥቆማ|Seexallu")]
            complain,
            [Description("complain1|ቅሬታ|Heyyanoo")]
            complain1,

        }
        public enum InspectionDecisionType
        {
            [Description("Complete|የተሟላ|Kan Guttee")]
            Complete = 1,
            [Description("INComplete|ያልተሟላ|Kan hin Gutaamne")]
            INComplete,
            [Description("Invalid|የተሳሳተ|Kan Dogoogore")]
            Invalid,
        }
        public enum MessageExchangStatus
        {
            [Description("Requested|መልክት ቀርቧል|Mallattoon Dhiyateera")]
            Requested = 1,
            [Description("RequestSent|መልክት ተልኳል|Mallattoon Ergameera")]
            RequestSent,
            [Description("SentForDecision|ለውሳኔ ተላልፋል|Murteef kan Dhiyatee")]
            SentForDecision,
            [Description("Decided|ውሳኔ ተሰቷል|Muurten itti kenameera")]
            Decided,
        }
        public enum ReportCategorization
        {
            DescisionStatus = 1,
            ComplainType
        }
        public enum sex
        {
            [Description("Male|ወንድ|Dhiira")]
            Male = 0,
            [Description("Female|ሴት|Dubartii")]
            Female = 1
        }
    }
}