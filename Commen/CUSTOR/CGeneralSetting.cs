﻿using System.Web;
using System.Data;
using System.Data.SqlClient;
using CUSTOR;
using System.Configuration;


/// <summary>
/// Summary description for CGeneralSetting
/// </summary>
public static class CGeneralSetting
{
    public static int ExpiryYear = 0;
    public static bool UseOldTariff = false;
    public static bool CanRenewWithPenality = false;
    public static bool CanRenewWithInjunction = false;
    public static int YearTo = 0;
    public static int MaxNoDaysForLicense = 0;
    public static int ValidYear = 0;
    public static int Location = 0;
    public static string ReportTitle = "";
    public static string ReportTitleAmh = "";
    public static string SubReportTitle = "";
    public static string SubReportTitleAmh = "";
    public static bool UseNewPolicyDriver = false;
    public static byte[] CompanyLogo = null;
    public static void ReadSetting()
    {
        DBHelper dataAccess = new DBHelper();
        DataTable table = new DataTable();
        string strSQL = "";

        strSQL = "SELECT * FROM tblReportHeaderSetting where id=1";

        table = dataAccess.GetRecord(strSQL);

        if (table.Rows.Count > 0)
        {

            ReportTitle = (string)table.Rows[0]["ReportTitle"];
            ReportTitleAmh = (string)table.Rows[0]["ReportTitleAmh"];
            SubReportTitle = (string)table.Rows[0]["ReportSubTitle"];
            SubReportTitleAmh = (string)table.Rows[0]["ReportSubTitleAmh"];
            //CompanyLogo = (byte[])table.Rows[0]["CompanyLogo"];

        }
    }


}
