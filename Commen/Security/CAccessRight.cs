﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CUSTOR.Commen
{
   public class CAccessRight
    {
        public static bool CanAccess(CResource.eResource enumResourceType)
        {
            switch (enumResourceType)
            {
                case CResource.eResource.einspectorsupervisor:
                    return CCurrentUser.CurrentUser.IsInRole("Inspection Supervisor");
                case CResource.eResource.einspectordirector:
                    return CCurrentUser.CurrentUser.IsInRole("Inspection Director");
                case CResource.eResource.einspectordataencoder:
                    return CCurrentUser.CurrentUser.IsInRole("Inspection Data Encoder");
                case CResource.eResource.einspectorofficer:
                    return CCurrentUser.CurrentUser.IsInRole("Inspection Officer");
                case CResource.eResource.ehrofficer:
                    return CCurrentUser.CurrentUser.IsInRole("HR Officer");
                case CResource.eResource.eHRSupervisor:
                    return CCurrentUser.CurrentUser.IsInRole("HR Supervisor");

                default:
                    return false;

            }

        }
        public static bool CanEditComplain(CResource.eResource enumResourceType)
        {
            switch (enumResourceType)
            {
                case CResource.eResource.eEditDataEntry:
                    return CCurrentUser.CurrentUser.IsInRole("ComplainDataEntry");
                default:
                    return false;
            }
        }
        public static bool CanEdit(CResource.eResource enumResourceType)
        {
            switch (enumResourceType)
            {
                case CResource.eResource.eDataEntry:
                    return CCurrentUser.CurrentUser.IsInRole("ComplainDataEntry");
                case CResource.eResource.einspector:
                    return CCurrentUser.CurrentUser.IsInRole("Inspector");
                case CResource.eResource.ehigherinspector:
                    return CCurrentUser.CurrentUser.IsInRole("HigherInspector");
                case CResource.eResource.eDirectorInspection:
                    return CCurrentUser.CurrentUser.IsInRole("DirectorInspection");
                default:
                    return false; ;
            }
        }
        public static bool CanViewReport(CResource.eResource enumResourceType)
        {
            switch (enumResourceType)
            {
                case CResource.eResource.eAdmin:
                    return CCurrentUser.CurrentUser.IsInRole("Administrator");
                case CResource.eResource.eDataEntry:
                    return CCurrentUser.CurrentUser.IsInRole("DataEntry");
                case CResource.eResource.einspector:
                    return CCurrentUser.CurrentUser.IsInRole("Inspector");
                case CResource.eResource.ehigherinspector:
                    return CCurrentUser.CurrentUser.IsInRole("HigherInspector");
                case CResource.eResource.eDirectorInspection:
                    return CCurrentUser.CurrentUser.IsInRole("DirectorInspection");
             
                default:
                    return false;

            }
        }
        public static bool CanViewComplaint(CResource.eResource enumResourceType)
        {
            switch (enumResourceType)
            {
                case CResource.eResource.eViewComplian:
                    return CCurrentUser.CurrentUser.IsInRole("Administrator");
                default:
                    return false;
            }
        }

        public static bool CanRegisterComplain(CResource.eResource enumResourceType)
        {
            switch (enumResourceType)
            {
                case CResource.eResource.eComplainDataEntry:
                    return CCurrentUser.CurrentUser.IsInRole("GuestRole") || CCurrentUser.CurrentUser.IsInRole("DataEntryRole");
                default:
                    return false;
            }
        }

        public static bool CanDelete(CResource.eResource enumResourceType)
        {
            switch (enumResourceType)
            {
                case CResource.eResource.eDeleteCompliain:
                    return CCurrentUser.CurrentUser.IsInRole("InspectorRole");
                default:
                    return false;
            }
        }

        public static bool CanDownload(CResource.eResource enumResourceType)
        {
            switch (enumResourceType)
            {
                case CResource.eResource.eDownLoad:
                    return CCurrentUser.CurrentUser.IsInRole("InspectorRole");
                default:
                    return false;
            }
        }

        public static bool CanPrintReport(CResource.eResource enumResourceType)
        {
            switch (enumResourceType)
            {
                case CResource.eResource.ePrintReport:
                    return CCurrentUser.CurrentUser.IsInRole("InspectorRole");
                default:
                    return false;
            }
        }

        public static bool CanMakeComplianDecision(CResource.eResource enumResourceType)
        {
            switch (enumResourceType)
            {
                case CResource.eResource.eMakeComplianDecision:
                    return CCurrentUser.CurrentUser.IsInRole("InspectorRole");
                default:
                    return false;
            }
        }
    }
}
