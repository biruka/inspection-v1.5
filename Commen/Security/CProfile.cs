﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;


namespace CProfile
{
    public class ProfileGroupOrganization : System.Web.Profile.ProfileGroupBase
    {

        public virtual string Name
        {
            get
            {
                return ((string)(this.GetPropertyValue("Name")));
            }
            set
            {
                this.SetPropertyValue("Name", value);
            }
        }

        public virtual string GUID
        {
            get
            {
                return ((string)(this.GetPropertyValue("GUID")));
            }
            set
            {
                this.SetPropertyValue("GUID", value);
            }
        }

        public virtual string Administration
        {
            get
            {
                return ((string)(this.GetPropertyValue("Administration")));
            }
            set
            {
                this.SetPropertyValue("Administration", value);
            }
        }

        public virtual string Category
        {
            get
            {
                return ((string)(this.GetPropertyValue("Category")));
            }
            set
            {
                this.SetPropertyValue("Category", value);
            }
        }
        public virtual string Language
        {
            get
            {
                return ((string)(this.GetPropertyValue("Language")));
            }
            set
            {
                this.SetPropertyValue("Language", value);
            }
        }
        public virtual string Zone
        {
            get
            {
                return ((string)(this.GetPropertyValue("Zone")));
            }
            set
            {
                this.SetPropertyValue("Zone", value);
            }
        }
        public virtual string Wereda
        {
            get
            {
                return ((string)(this.GetPropertyValue("Wereda")));
            }
            set
            {
                this.SetPropertyValue("Wereda", value);
            }
        }
    }

    public class ProfileGroupStaff : System.Web.Profile.ProfileGroupBase
    {

        public virtual string FullName
        {
            get
            {
                return ((string)(this.GetPropertyValue("FullName")));
            }
            set
            {
                this.SetPropertyValue("FullName", value);
            }
        }

        public virtual string GUID
        {
            get
            {
                return ((string)(this.GetPropertyValue("GUID")));
            }
            set
            {
                this.SetPropertyValue("GUID", value);
            }
        }

        public virtual string Units
        {
            get
            {
                return ((string)(this.GetPropertyValue("Units")));
            }
            set
            {
                this.SetPropertyValue("Units", value);
            }
        }

    }
    public class ProfileCommon : System.Web.Profile.ProfileBase
    {


        public virtual ProfileGroupOrganization Organization
        {
            get
            {
                return ((ProfileGroupOrganization)(this.GetProfileGroup("Organization")));
            }
        }

        public virtual ProfileGroupStaff Staff
        {
            get
            {
                return ((ProfileGroupStaff)(this.GetProfileGroup("Staff")));
            }
        }

        public virtual ProfileCommon GetProfile(string username)
        {
            return ((ProfileCommon)(ProfileBase.Create(username)));
        }
    }
}