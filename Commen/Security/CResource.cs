﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CResource
/// </summary>
namespace CUSTOR.Commen
{
    public static class CResource
    {
        public enum eResource
        {
            eComplainDataEntry,
            eMakeComplianDecision,
            eDownLoad,
            eDeleteCompliain,
            ePrintReport,
            eViewComplian,
            eDataEntry,
            eAdmin,
            eCashCollection,
            einspector,
            ehigherinspector,
            eEditDataEntry,
            eDirectorInspection,
            einspectorofficer,
            einspectorsupervisor,
            einspectordirector,
            einspectordataencoder,
            ehrofficer,
            eHRSupervisor

        }
    }
}