﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Profile;

namespace Service.Security
{
    public class UserProfile : ProfileBase
    {
        #region Properties

        [SettingsAllowAnonymous(false)]
        public string FirstName
        {
            get { return base["FirstName"] as string; }
            set { base["FirstName"] = value; }
        }

        [SettingsAllowAnonymous(false)]
        public string SecondName
        {
            get { return base["SecondName"] as string; }
            set { base["SecondName"] = value; }
        }

        [SettingsAllowAnonymous(false)]
        public string LastName
        {
            get { return base["LastName"] as string; }
            set { base["LastName"] = value; }
        }

        #endregion

        public static UserProfile GetUserProfile(string username)
        {
            return Create(username) as UserProfile;
        }
    }
}
