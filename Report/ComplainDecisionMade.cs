﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel.Design.Serialization;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Design;
using System.Data;

//using CUSTOR.Commen;
//using CUSTOR.Business;
/// <summary>
/// Summary description for rptComplainDecisionMade
/// </summary>
namespace CUSTOR.Report
{
    public class ComplainDecisionMade : DevExpress.XtraReports.UI.XtraReport
    {
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private XRLabel xrLabel19;
        private XRLabel xrLabel17;
        private XRLine xrLine2;
        private XRLabel xrLabel18;
        private DevExpress.XtraReports.Parameters.Parameter ReportedBy;
        private DevExpress.XtraReports.Parameters.Parameter Organization;
        private DevExpress.XtraReports.Parameters.Parameter ReportedDate;
        private DevExpress.XtraReports.Parameters.Parameter ComplainType;
        private DevExpress.XtraReports.Parameters.Parameter No;
        private XRControlStyle xrControlStyle1;
        private XRControlStyle xrControlStyle2;
        private XRControlStyle xrControlStyle3;
        private XRTable xrTable3;
        private XRTableRow xrTableRow3;
        private XRTableCell xrTableCell13;
        private XRTableCell xrTableCell8;
        private XRTableCell xrTableCell27;
        private XRTableCell xrTableCell28;
        private XRTableCell xrTableCell29;
        public DateTime dateFrom;
        public DateTime dateTo;
        public string reportTitle;
        public string headerTitle2;
        public string headerTitle4;
        public string headerTitle5;
        private XRPageInfo xrPageInfo1;
        private PageFooterBand PageFooter;
        private XRTableCell xrTableCell2;
        private DevExpress.XtraReports.Parameters.Parameter ScreenedBy;
        private XRControlStyle xrControlStyle4;
        private XRLabel xrLabel1;
        private XRTable xrTable1;
        private XRTableRow xrTableRow1;
        private XRTableCell xrTableCell6;
        private XRTableCell xrReportedBy;
        private XRTableCell xrOrganization;
        private XRTableCell xrTableCell3;
        private XRTableCell xrComplainType;
        private XRTableCell xrTableCell1;
        private XRLine xrLine1;
        private XRLabel xrDateTo;
        private XRLabel xrTo;
        private XRLabel xrDateFrom;
        private XRLabel xrFrom;
        private XRPictureBox xrPictureBox1;
        private XRLabel xrLabel2;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public ComplainDecisionMade()
        {
            InitializeComponent();
            //
            // TODO: Add constructor logic here
            //
            Today();
            LoadReportHeaderTitle();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComplainDecisionMade));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrReportedBy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrOrganization = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrComplainType = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrDateTo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDateFrom = new DevExpress.XtraReports.UI.XRLabel();
            this.xrFrom = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportedBy = new DevExpress.XtraReports.Parameters.Parameter();
            this.Organization = new DevExpress.XtraReports.Parameters.Parameter();
            this.ReportedDate = new DevExpress.XtraReports.Parameters.Parameter();
            this.ComplainType = new DevExpress.XtraReports.Parameters.Parameter();
            this.No = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrControlStyle2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrControlStyle3 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.ScreenedBy = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrControlStyle4 = new DevExpress.XtraReports.UI.XRControlStyle();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.Detail.HeightF = 25.00002F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseTextAlignment = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTable3
            // 
            this.xrTable3.BackColor = System.Drawing.Color.Transparent;
            this.xrTable3.BorderColor = System.Drawing.Color.Black;
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.EvenStyleName = "xrControlStyle1";
            this.xrTable3.Font = new System.Drawing.Font("Nyala", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable3.ForeColor = System.Drawing.Color.Black;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(29.794F, 1.000023F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.OddStyleName = "xrControlStyle2";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(783.8331F, 24F);
            this.xrTable3.StylePriority.UseBorderColor = false;
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.StylePriority.UseForeColor = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell8,
            this.xrTableCell27,
            this.xrTableCell28,
            this.xrTableCell29,
            this.xrTableCell2});
            this.xrTableRow3.EvenStyleName = "xrControlStyle1";
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.OddStyleName = "xrControlStyle2";
            this.xrTableRow3.Weight = 0.76399548125525918D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(177)))), ((int)(((byte)(183)))));
            this.xrTableCell13.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "No")});
            this.xrTableCell13.Font = new System.Drawing.Font("Nyala", 12F, System.Drawing.FontStyle.Bold);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell13.StylePriority.UseBorderColor = false;
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.StylePriority.UseFont = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "xrTableCell13";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell13.Weight = 0.042713337900170407D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ReportedBy")});
            this.xrTableCell8.Font = new System.Drawing.Font("Nyala", 12F);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "[ReportedBy]";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell8.Weight = 0.20589494990320525D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Organization")});
            this.xrTableCell27.Font = new System.Drawing.Font("Nyala", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell27.ForeColor = System.Drawing.SystemColors.ControlText;
            this.xrTableCell27.Multiline = true;
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.StylePriority.UseForeColor = false;
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.Text = "xrTableCell27";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell27.Weight = 0.20699172694118964D;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ReportedDate")});
            this.xrTableCell28.Font = new System.Drawing.Font("Nyala", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.StylePriority.UseTextAlignment = false;
            this.xrTableCell28.Text = "xrTableCell28";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell28.Weight = 0.20305183665652724D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ComplainType")});
            this.xrTableCell29.Font = new System.Drawing.Font("Nyala", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.StylePriority.UseTextAlignment = false;
            this.xrTableCell29.Text = "[ComplainType]";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell29.Weight = 0.17742156894500871D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ScreenedBy")});
            this.xrTableCell2.Font = new System.Drawing.Font("Nyala", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "[ScreenedBy]";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell2.Weight = 0.17443873899501394D;
            // 
            // TopMargin
            // 
            this.TopMargin.BackColor = System.Drawing.Color.SlateGray;
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrTable1,
            this.xrDateTo,
            this.xrTo,
            this.xrDateFrom,
            this.xrFrom,
            this.xrPictureBox1,
            this.xrLabel2,
            this.xrLabel19,
            this.xrLabel17,
            this.xrLine2,
            this.xrLabel18,
            this.xrLine1});
            this.TopMargin.HeightF = 296.5001F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.StylePriority.UseBackColor = false;
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.BurlyWood;
            this.xrLabel1.Font = new System.Drawing.Font("Nyala", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(152.8354F, 195.1667F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(552.7917F, 33F);
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "ውድቅ የሆኑ አቤቱታዎች ";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTable1
            // 
            this.xrTable1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrTable1.BorderColor = System.Drawing.Color.LightGray;
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Font = new System.Drawing.Font("Nyala", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.ForeColor = System.Drawing.Color.White;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(29.79396F, 271.2917F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(783.8331F, 25.20842F);
            this.xrTable1.StylePriority.UseBackColor = false;
            this.xrTable1.StylePriority.UseBorderColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseForeColor = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrReportedBy,
            this.xrOrganization,
            this.xrTableCell3,
            this.xrComplainType,
            this.xrTableCell1});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "ተ.ቁ";
            this.xrTableCell6.Weight = 0.31447852705825119D;
            // 
            // xrReportedBy
            // 
            this.xrReportedBy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrReportedBy.Name = "xrReportedBy";
            this.xrReportedBy.StylePriority.UseBackColor = false;
            this.xrReportedBy.StylePriority.UseTextAlignment = false;
            this.xrReportedBy.Text = "ጥቆማ አቅራቢ";
            this.xrReportedBy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrReportedBy.Weight = 1.5159099531865721D;
            // 
            // xrOrganization
            // 
            this.xrOrganization.Name = "xrOrganization";
            this.xrOrganization.StylePriority.UseTextAlignment = false;
            this.xrOrganization.Text = "ተቋም";
            this.xrOrganization.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrOrganization.Weight = 1.5239848815686621D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "ጥቆማ የቀረበበት ቀን";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell3.Weight = 1.5128307801278811D;
            // 
            // xrComplainType
            // 
            this.xrComplainType.Name = "xrComplainType";
            this.xrComplainType.StylePriority.UseTextAlignment = false;
            this.xrComplainType.Text = "የአቤቱታ አይነት";
            this.xrComplainType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrComplainType.Weight = 1.288421075851758D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "ያፀደቀው";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell1.Weight = 1.2843116965511994D;
            // 
            // xrDateTo
            // 
            this.xrDateTo.BackColor = System.Drawing.Color.Transparent;
            this.xrDateTo.BorderColor = System.Drawing.Color.Transparent;
            this.xrDateTo.Font = new System.Drawing.Font("Nyala", 14F);
            this.xrDateTo.LocationFloat = new DevExpress.Utils.PointFloat(464.0685F, 228.1666F);
            this.xrDateTo.Name = "xrDateTo";
            this.xrDateTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDateTo.SizeF = new System.Drawing.SizeF(185.7344F, 20.12502F);
            this.xrDateTo.StylePriority.UseBackColor = false;
            this.xrDateTo.StylePriority.UseBorderColor = false;
            this.xrDateTo.StylePriority.UseFont = false;
            // 
            // xrTo
            // 
            this.xrTo.BackColor = System.Drawing.Color.Transparent;
            this.xrTo.Font = new System.Drawing.Font("Nyala", 12F);
            this.xrTo.LocationFloat = new DevExpress.Utils.PointFloat(429.3602F, 228.1667F);
            this.xrTo.Name = "xrTo";
            this.xrTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTo.SizeF = new System.Drawing.SizeF(34.70828F, 20.12502F);
            this.xrTo.StylePriority.UseBackColor = false;
            this.xrTo.StylePriority.UseFont = false;
            this.xrTo.StylePriority.UseTextAlignment = false;
            this.xrTo.Text = "እስከ:";
            this.xrTo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrDateFrom
            // 
            this.xrDateFrom.BackColor = System.Drawing.Color.Transparent;
            this.xrDateFrom.Font = new System.Drawing.Font("Nyala", 14F);
            this.xrDateFrom.LocationFloat = new DevExpress.Utils.PointFloat(268.8009F, 228.1667F);
            this.xrDateFrom.Name = "xrDateFrom";
            this.xrDateFrom.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDateFrom.SizeF = new System.Drawing.SizeF(160.5592F, 20.12502F);
            this.xrDateFrom.StylePriority.UseBackColor = false;
            this.xrDateFrom.StylePriority.UseFont = false;
            this.xrDateFrom.StylePriority.UseTextAlignment = false;
            this.xrDateFrom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrFrom
            // 
            this.xrFrom.BackColor = System.Drawing.Color.Transparent;
            this.xrFrom.Font = new System.Drawing.Font("Nyala", 12F);
            this.xrFrom.LocationFloat = new DevExpress.Utils.PointFloat(251.8009F, 228.1667F);
            this.xrFrom.Name = "xrFrom";
            this.xrFrom.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrFrom.SizeF = new System.Drawing.SizeF(17.00006F, 20.125F);
            this.xrFrom.StylePriority.UseBackColor = false;
            this.xrFrom.StylePriority.UseFont = false;
            this.xrFrom.StylePriority.UseTextAlignment = false;
            this.xrFrom.Text = "ከ:";
            this.xrFrom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(383.1934F, 76.41684F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(87.49994F, 81.24989F);
            // 
            // xrLabel2
            // 
            this.xrLabel2.BackColor = System.Drawing.Color.LightGray;
            this.xrLabel2.Font = new System.Drawing.Font("Nyala", 18F);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(29.79393F, 157.6667F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(783.8331F, 37.50002F);
            this.xrLabel2.StylePriority.UseBackColor = false;
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "ኢ.ፌ.ድ.ሪ ሲቪል ሰርቪስ ሚኒስቴር               E.F.D.R Civil Service Minstry";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel19
            // 
            this.xrLabel19.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel19.Font = new System.Drawing.Font("Nyala", 9.75F);
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(663.6271F, 52.70834F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(42F, 17F);
            this.xrLabel19.StylePriority.UseBackColor = false;
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.Text = "ቁጥር :";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel17
            // 
            this.xrLabel17.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel17.Font = new System.Drawing.Font("Nyala", 9.75F);
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(713.6271F, 27.70834F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(100F, 17F);
            this.xrLabel17.StylePriority.UseBackColor = false;
            this.xrLabel17.StylePriority.UseBorders = false;
            this.xrLabel17.StylePriority.UseFont = false;
            // 
            // xrLine2
            // 
            this.xrLine2.BackColor = System.Drawing.Color.Transparent;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(713.6271F, 52.70834F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLine2.SizeF = new System.Drawing.SizeF(100F, 29.5F);
            this.xrLine2.StylePriority.UseBackColor = false;
            this.xrLine2.StylePriority.UsePadding = false;
            // 
            // xrLabel18
            // 
            this.xrLabel18.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel18.Font = new System.Drawing.Font("Nyala", 9.75F);
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(663.6271F, 27.70834F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(42F, 17F);
            this.xrLabel18.StylePriority.UseBackColor = false;
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.Text = "ቀን :";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLine1
            // 
            this.xrLine1.BackColor = System.Drawing.Color.Transparent;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(21.79398F, 248.2917F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(783.8331F, 23F);
            this.xrLine1.StylePriority.UseBackColor = false;
            // 
            // BottomMargin
            // 
            this.BottomMargin.BackColor = System.Drawing.Color.Empty;
            this.BottomMargin.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.BottomMargin.BorderWidth = 0;
            this.BottomMargin.HeightF = 3F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.StyleName = "xrControlStyle1";
            this.BottomMargin.StylePriority.UseBackColor = false;
            this.BottomMargin.StylePriority.UseBorders = false;
            this.BottomMargin.StylePriority.UseBorderWidth = false;
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportedBy
            // 
            this.ReportedBy.Name = "ReportedBy";
            this.ReportedBy.Value = "";
            // 
            // Organization
            // 
            this.Organization.Name = "Organization";
            this.Organization.Value = "";
            // 
            // ReportedDate
            // 
            this.ReportedDate.Name = "ReportedDate";
            this.ReportedDate.Value = "";
            // 
            // ComplainType
            // 
            this.ComplainType.Name = "ComplainType";
            this.ComplainType.Value = "";
            // 
            // No
            // 
            this.No.Name = "No";
            this.No.Value = "";
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.xrControlStyle1.BorderColor = System.Drawing.SystemColors.ControlLight;
            this.xrControlStyle1.ForeColor = System.Drawing.Color.Black;
            this.xrControlStyle1.Name = "xrControlStyle1";
            // 
            // xrControlStyle2
            // 
            this.xrControlStyle2.BackColor = System.Drawing.Color.White;
            this.xrControlStyle2.BorderColor = System.Drawing.Color.Empty;
            this.xrControlStyle2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrControlStyle2.ForeColor = System.Drawing.Color.Black;
            this.xrControlStyle2.Name = "xrControlStyle2";
            this.xrControlStyle2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // xrControlStyle3
            // 
            this.xrControlStyle3.Name = "xrControlStyle3";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Font = new System.Drawing.Font("Nyala", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo1.Format = "Page : {0 } / {1}{0}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(710.4165F, 76.99998F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.HeightF = 99.99998F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.StylePriority.UseBorderColor = false;
            // 
            // ScreenedBy
            // 
            this.ScreenedBy.Name = "ScreenedBy";
            this.ScreenedBy.Value = "";
            // 
            // xrControlStyle4
            // 
            this.xrControlStyle4.Name = "xrControlStyle4";
            this.xrControlStyle4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 2, 100F);
            // 
            // ComplainDecisionMade
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageFooter});
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 297, 3);
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.ReportedBy,
            this.Organization,
            this.ReportedDate,
            this.ComplainType,
            this.No,
            this.ScreenedBy});
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1,
            this.xrControlStyle2,
            this.xrControlStyle3,
            this.xrControlStyle4});
            this.Version = "12.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        public void ShowDateRange()
        {
            DateTime minDate = new DateTime();
            if (!minDate.ToString().Equals(dateFrom.ToString()))
            {
                xrDateFrom.Text = EthiopicDateTime.TranslateDateMonth(dateFrom);
            }
            else
            {
                xrFrom.Visible = false;
                xrDateFrom.Visible = false;
            }
            if (!minDate.ToString().Equals(dateTo.ToString()))
            {
                xrDateTo.Text = EthiopicDateTime.TranslateDateMonth(dateTo);
            }
            else
            {
                xrTo.Visible = false;
                xrDateTo.Visible = false;
            }
        }

        protected void Today()
        {
            xrLabel17.Text = EthiopicDateTime.TranslateDateMonth(DateTime.Today);
        }

        public void LoadReportHeaderTitle()
        {
            xrLabel1.Text = reportTitle;
            xrReportedBy.Text = headerTitle2;
            xrTableCell3.Text = headerTitle4;
            xrComplainType.Text = headerTitle5;
        }
    }
}