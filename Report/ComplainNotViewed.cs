﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraReports.UI;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Text;
/// <summary>
/// Summary description for XtraReport1
/// </summary>
/// 
using Service.Commen;
using CUSTOR.DataAccess;

namespace CUSTOR.Report
{
    public class ComplainNotViewed : DevExpress.XtraReports.UI.XtraReport
    {
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private XRLabel xrLabel18;
        private XRLabel xrLabel17;
        private XRLabel xrLabel19;
        private XRLine xrLine2;
        private XRTable xrTable2;
        private XRTableRow xrTableRow2;
        private XRTableCell xrTableCell1;
        private XRTableCell xrTableCell2;
        private XRTableCell xrTableCell4;
        private XRTableCell xrTableCell5;
        private DevExpress.XtraReports.Parameters.Parameter ReportedBy;
        private DevExpress.XtraReports.Parameters.Parameter Organization;
        private DevExpress.XtraReports.Parameters.Parameter ReportedDate;
        private DevExpress.XtraReports.Parameters.Parameter ComplainType;
        private DevExpress.XtraReports.Parameters.Parameter No;
        private XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle2;
        public DateTime dateFrom;
        public DateTime dateTo;
        public string reportTitle;
        public string headerTitle2;
        public string headerTitle4;
        public string headerTitle5;
        private DevExpress.XtraReports.Parameters.Parameter ScreenedBy;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrReportedBy;
        private DevExpress.XtraReports.UI.XRTableCell xrOrganization;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrComplainType;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrDateTo;
        private DevExpress.XtraReports.UI.XRLabel xrTo;
        private DevExpress.XtraReports.UI.XRLabel xrDateFrom;
        private DevExpress.XtraReports.UI.XRLabel xrFrom;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public ComplainNotViewed()
        {
            InitializeComponent();   
            Today();
        }
        
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComplainNotViewed));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportedBy = new DevExpress.XtraReports.Parameters.Parameter();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrReportedBy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrOrganization = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrComplainType = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrDateTo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDateFrom = new DevExpress.XtraReports.UI.XRLabel();
            this.xrFrom = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.Organization = new DevExpress.XtraReports.Parameters.Parameter();
            this.ReportedDate = new DevExpress.XtraReports.Parameters.Parameter();
            this.ComplainType = new DevExpress.XtraReports.Parameters.Parameter();
            this.No = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrControlStyle2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.ScreenedBy = new DevExpress.XtraReports.Parameters.Parameter();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.BackColor = System.Drawing.Color.Empty;
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail.HeightF = 25.62498F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseBackColor = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.EvenStyleName = "xrControlStyle1";
            this.xrTable2.Font = new System.Drawing.Font("Nyala", 11F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(32.04177F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.OddStyleName = "xrControlStyle2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(783.8331F, 25F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell4,
            this.xrTableCell5});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "No")});
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Text = "xrTableCell7";
            this.xrTableCell7.Weight = 0.14249991134562948D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ReportedBy")});
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "ReportedBy";
            this.xrTableCell1.Weight = 0.50030080810751887D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Organization")});
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "Organization";
            this.xrTableCell2.Weight = 0.5351968956696701D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ReportedDate")});
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "ReportedDate";
            this.xrTableCell4.Weight = 0.53128007083282347D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ComplainType")});
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "ComplainType";
            this.xrTableCell5.Weight = 0.4524710733710211D;
            // 
            // ReportedBy
            // 
            this.ReportedBy.Name = "ReportedBy";
            this.ReportedBy.Value = "";
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrTable1,
            this.xrLine1,
            this.xrDateTo,
            this.xrTo,
            this.xrDateFrom,
            this.xrFrom,
            this.xrPictureBox1,
            this.xrLabel2,
            this.xrLabel18,
            this.xrLabel17,
            this.xrLabel19,
            this.xrLine2});
            this.TopMargin.HeightF = 317.0834F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.BurlyWood;
            this.xrLabel1.Font = new System.Drawing.Font("Nyala", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(139.2331F, 214.7083F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(552.7917F, 33F);
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "ውድቅ የሆኑ አቤቱታዎች ";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTable1
            // 
            this.xrTable1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Font = new System.Drawing.Font("Nyala", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.ForeColor = System.Drawing.Color.White;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(32.04177F, 290.8333F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(783.8331F, 25.20842F);
            this.xrTable1.StylePriority.UseBackColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseForeColor = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrReportedBy,
            this.xrOrganization,
            this.xrTableCell3,
            this.xrComplainType});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "ተ.ቁ";
            this.xrTableCell6.Weight = 0.31447852705825119D;
            // 
            // xrReportedBy
            // 
            this.xrReportedBy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrReportedBy.Name = "xrReportedBy";
            this.xrReportedBy.StylePriority.UseBackColor = false;
            this.xrReportedBy.StylePriority.UseTextAlignment = false;
            this.xrReportedBy.Text = "ጥቆማ አቅራቢ";
            this.xrReportedBy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrReportedBy.Weight = 1.5159099531865721D;
            // 
            // xrOrganization
            // 
            this.xrOrganization.Name = "xrOrganization";
            this.xrOrganization.StylePriority.UseTextAlignment = false;
            this.xrOrganization.Text = "ተቋም";
            this.xrOrganization.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrOrganization.Weight = 1.5239848815686621D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "ጥቆማ የቀረበበት ቀን";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell3.Weight = 1.5128307801278811D;
            // 
            // xrComplainType
            // 
            this.xrComplainType.Name = "xrComplainType";
            this.xrComplainType.StylePriority.UseTextAlignment = false;
            this.xrComplainType.Text = "የአቤቱታ አይነት";
            this.xrComplainType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrComplainType.Weight = 1.288421075851758D;
            // 
            // xrLine1
            // 
            this.xrLine1.BackColor = System.Drawing.Color.Transparent;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(32.04177F, 267.8333F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(783.8331F, 23F);
            this.xrLine1.StylePriority.UseBackColor = false;
            // 
            // xrDateTo
            // 
            this.xrDateTo.BackColor = System.Drawing.Color.Transparent;
            this.xrDateTo.BorderColor = System.Drawing.Color.Transparent;
            this.xrDateTo.Font = new System.Drawing.Font("Nyala", 14F);
            this.xrDateTo.LocationFloat = new DevExpress.Utils.PointFloat(453.8162F, 247.7084F);
            this.xrDateTo.Name = "xrDateTo";
            this.xrDateTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDateTo.SizeF = new System.Drawing.SizeF(152.7343F, 20.12498F);
            this.xrDateTo.StylePriority.UseBackColor = false;
            this.xrDateTo.StylePriority.UseBorderColor = false;
            this.xrDateTo.StylePriority.UseFont = false;
            // 
            // xrTo
            // 
            this.xrTo.BackColor = System.Drawing.Color.Transparent;
            this.xrTo.Font = new System.Drawing.Font("Nyala", 12F);
            this.xrTo.LocationFloat = new DevExpress.Utils.PointFloat(419.1079F, 247.7083F);
            this.xrTo.Name = "xrTo";
            this.xrTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTo.SizeF = new System.Drawing.SizeF(34.70828F, 20.12502F);
            this.xrTo.StylePriority.UseBackColor = false;
            this.xrTo.StylePriority.UseFont = false;
            this.xrTo.StylePriority.UseTextAlignment = false;
            this.xrTo.Text = "እስከ:";
            this.xrTo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrDateFrom
            // 
            this.xrDateFrom.BackColor = System.Drawing.Color.Transparent;
            this.xrDateFrom.Font = new System.Drawing.Font("Nyala", 14F);
            this.xrDateFrom.LocationFloat = new DevExpress.Utils.PointFloat(289.7987F, 247.7083F);
            this.xrDateFrom.Name = "xrDateFrom";
            this.xrDateFrom.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDateFrom.SizeF = new System.Drawing.SizeF(129.3092F, 20.12498F);
            this.xrDateFrom.StylePriority.UseBackColor = false;
            this.xrDateFrom.StylePriority.UseFont = false;
            this.xrDateFrom.StylePriority.UseTextAlignment = false;
            this.xrDateFrom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrFrom
            // 
            this.xrFrom.BackColor = System.Drawing.Color.Transparent;
            this.xrFrom.Font = new System.Drawing.Font("Nyala", 12F);
            this.xrFrom.LocationFloat = new DevExpress.Utils.PointFloat(272.7987F, 247.7083F);
            this.xrFrom.Name = "xrFrom";
            this.xrFrom.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrFrom.SizeF = new System.Drawing.SizeF(17.00006F, 20.125F);
            this.xrFrom.StylePriority.UseBackColor = false;
            this.xrFrom.StylePriority.UseFont = false;
            this.xrFrom.StylePriority.UseTextAlignment = false;
            this.xrFrom.Text = "ከ:";
            this.xrFrom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(385.4411F, 95.95842F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(87.49994F, 81.24989F);
            // 
            // xrLabel2
            // 
            this.xrLabel2.BackColor = System.Drawing.Color.LightGray;
            this.xrLabel2.Font = new System.Drawing.Font("Nyala", 18F);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(32.04177F, 177.2083F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(783.8331F, 37.50002F);
            this.xrLabel2.StylePriority.UseBackColor = false;
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "ኢ.ፌ.ድ.ሪ ሲቪል ሰርቪስ ሚኒስቴር               E.F.D.R Civil Service Minstry";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel18
            // 
            this.xrLabel18.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel18.Font = new System.Drawing.Font("Nyala", 9.75F);
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(665.875F, 22.91667F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(42F, 17F);
            this.xrLabel18.StylePriority.UseBackColor = false;
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.Text = "ቀን :";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel17
            // 
            this.xrLabel17.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel17.Font = new System.Drawing.Font("Nyala", 9.75F);
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(707.875F, 22.91667F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(107.9999F, 17F);
            this.xrLabel17.StylePriority.UseBackColor = false;
            this.xrLabel17.StylePriority.UseBorders = false;
            this.xrLabel17.StylePriority.UseFont = false;
            // 
            // xrLabel19
            // 
            this.xrLabel19.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel19.Font = new System.Drawing.Font("Nyala", 9.75F);
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(665.875F, 47.91667F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(42F, 17F);
            this.xrLabel19.StylePriority.UseBackColor = false;
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.Text = "ቁጥር :";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLine2
            // 
            this.xrLine2.BackColor = System.Drawing.Color.Transparent;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(707.875F, 47.91667F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(107.9999F, 33F);
            this.xrLine2.StylePriority.UseBackColor = false;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // Organization
            // 
            this.Organization.Name = "Organization";
            this.Organization.Value = "";
            // 
            // ReportedDate
            // 
            this.ReportedDate.Name = "ReportedDate";
            this.ReportedDate.Value = "";
            // 
            // ComplainType
            // 
            this.ComplainType.Name = "ComplainType";
            this.ComplainType.Value = "";
            // 
            // No
            // 
            this.No.Name = "No";
            this.No.Value = "";
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.xrControlStyle1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(220)))), ((int)(((byte)(255)))));
            this.xrControlStyle1.Name = "xrControlStyle1";
            this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // xrControlStyle2
            // 
            this.xrControlStyle2.BackColor = System.Drawing.Color.White;
            this.xrControlStyle2.BorderColor = System.Drawing.Color.White;
            this.xrControlStyle2.Name = "xrControlStyle2";
            this.xrControlStyle2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // ScreenedBy
            // 
            this.ScreenedBy.Description = "Parameter1";
            this.ScreenedBy.Name = "ScreenedBy";
            this.ScreenedBy.Value = "";
            // 
            // ComplainNotViewed
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 317, 100);
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.ReportedBy,
            this.Organization,
            this.ReportedDate,
            this.ComplainType,
            this.No,
            this.ScreenedBy});
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1,
            this.xrControlStyle2});
            this.Version = "12.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        public void ShowDateRange()
        {
            DateTime minDate = new DateTime();
            if (!minDate.ToString().Equals(dateFrom.ToString()))
            {
                xrDateFrom.Text = EthiopicDateTime.TranslateDateMonth(dateFrom);
            }
            else
            {
                xrFrom.Visible = false;
                xrDateFrom.Visible = false;
            }
            if (!minDate.ToString().Equals(dateTo.ToString()))
            {
                xrDateTo.Text = EthiopicDateTime.TranslateDateMonth(dateTo);
            }
            else
            {
                xrTo.Visible = false;
                xrDateTo.Visible = false;
            }
            xrLabel1.Text = reportTitle;
            xrReportedBy.Text = headerTitle2;
            xrTableCell3.Text = headerTitle4;
            xrComplainType.Text = headerTitle5;
        }

        protected void Today()
        {
            xrLabel17.Text = EthiopicDateTime.TranslateDateMonth(DateTime.Today);
        }

        public void LoadReportHeaderTitle()
        {
            xrLabel1.Text = reportTitle;
            xrReportedBy.Text = headerTitle2;
            xrTableCell3.Text = headerTitle4;
            xrComplainType.Text = headerTitle5;
        }
    }
}