﻿namespace Report.Inspection
{
    partial class rptinspectorHractivitysummery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel1 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.txtReportSubTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.txtReportTitleAmh = new DevExpress.XtraReports.UI.XRLabel();
            this.txtReportSubTitleAmh = new DevExpress.XtraReports.UI.XRLabel();
            this.txtReportTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrFrom = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDateFrom = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDateTo = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrPivotGrid1 = new DevExpress.XtraReports.UI.XRPivotGrid();
            this.xrPivotGridField1 = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.xrPivotGridField2 = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.xrPivotGridField3 = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.xrPivotGridField4 = new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField();
            this.xrChart1 = new DevExpress.XtraReports.UI.XRChart();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrControlStyle2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 200F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txtReportSubTitle,
            this.txtReportTitleAmh,
            this.txtReportSubTitleAmh,
            this.txtReportTitle,
            this.xrPictureBox1});
            this.TopMargin.HeightF = 208.875F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // txtReportSubTitle
            // 
            this.txtReportSubTitle.BackColor = System.Drawing.Color.Transparent;
            this.txtReportSubTitle.Font = new System.Drawing.Font("Nyala", 15F);
            this.txtReportSubTitle.ForeColor = System.Drawing.Color.Black;
            this.txtReportSubTitle.LocationFloat = new DevExpress.Utils.PointFloat(87.5F, 173.5F);
            this.txtReportSubTitle.Name = "txtReportSubTitle";
            this.txtReportSubTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtReportSubTitle.SizeF = new System.Drawing.SizeF(925F, 29.16667F);
            this.txtReportSubTitle.StylePriority.UseBackColor = false;
            this.txtReportSubTitle.StylePriority.UseFont = false;
            this.txtReportSubTitle.StylePriority.UseForeColor = false;
            this.txtReportSubTitle.StylePriority.UseTextAlignment = false;
            this.txtReportSubTitle.Text = "የኢትዮጵያ ፌዴራዊ ዲሞክራሲያዊ ሪፐብሊክ";
            this.txtReportSubTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txtReportTitleAmh
            // 
            this.txtReportTitleAmh.BackColor = System.Drawing.Color.Transparent;
            this.txtReportTitleAmh.Font = new System.Drawing.Font("Nyala", 15F);
            this.txtReportTitleAmh.ForeColor = System.Drawing.Color.Black;
            this.txtReportTitleAmh.LocationFloat = new DevExpress.Utils.PointFloat(87.50002F, 86F);
            this.txtReportTitleAmh.Name = "txtReportTitleAmh";
            this.txtReportTitleAmh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtReportTitleAmh.SizeF = new System.Drawing.SizeF(925F, 29.16667F);
            this.txtReportTitleAmh.StylePriority.UseBackColor = false;
            this.txtReportTitleAmh.StylePriority.UseFont = false;
            this.txtReportTitleAmh.StylePriority.UseForeColor = false;
            this.txtReportTitleAmh.StylePriority.UseTextAlignment = false;
            this.txtReportTitleAmh.Text = "የኢትዮጵያ ፌዴራዊ ዲሞክራሲያዊ ሪፐብሊክ";
            this.txtReportTitleAmh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txtReportSubTitleAmh
            // 
            this.txtReportSubTitleAmh.BackColor = System.Drawing.Color.Transparent;
            this.txtReportSubTitleAmh.Font = new System.Drawing.Font("Nyala", 15F);
            this.txtReportSubTitleAmh.ForeColor = System.Drawing.Color.Black;
            this.txtReportSubTitleAmh.LocationFloat = new DevExpress.Utils.PointFloat(87.5F, 115.1667F);
            this.txtReportSubTitleAmh.Name = "txtReportSubTitleAmh";
            this.txtReportSubTitleAmh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtReportSubTitleAmh.SizeF = new System.Drawing.SizeF(925F, 29.16667F);
            this.txtReportSubTitleAmh.StylePriority.UseBackColor = false;
            this.txtReportSubTitleAmh.StylePriority.UseFont = false;
            this.txtReportSubTitleAmh.StylePriority.UseForeColor = false;
            this.txtReportSubTitleAmh.StylePriority.UseTextAlignment = false;
            this.txtReportSubTitleAmh.Text = "የኢትዮጵያ ፌዴራዊ ዲሞክራሲያዊ ሪፐብሊክ";
            this.txtReportSubTitleAmh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txtReportTitle
            // 
            this.txtReportTitle.BackColor = System.Drawing.Color.Transparent;
            this.txtReportTitle.Font = new System.Drawing.Font("Nyala", 15F);
            this.txtReportTitle.ForeColor = System.Drawing.Color.Black;
            this.txtReportTitle.LocationFloat = new DevExpress.Utils.PointFloat(87.5F, 144.3333F);
            this.txtReportTitle.Name = "txtReportTitle";
            this.txtReportTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtReportTitle.SizeF = new System.Drawing.SizeF(925F, 29.16667F);
            this.txtReportTitle.StylePriority.UseBackColor = false;
            this.txtReportTitle.StylePriority.UseFont = false;
            this.txtReportTitle.StylePriority.UseForeColor = false;
            this.txtReportTitle.StylePriority.UseTextAlignment = false;
            this.txtReportTitle.Text = "የኢትዮጵያ ፌዴራዊ ዲሞክራሲያዊ ሪፐብሊክ";
            this.txtReportTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.xrPictureBox1.ImageUrl = "~\\Images\\EthLogo.gif";
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(499.0458F, 10F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(90F, 76F);
            this.xrPictureBox1.StylePriority.UseBackColor = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.BurlyWood;
            this.xrLabel1.Font = new System.Drawing.Font("Nyala", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(7.947286E-06F, 0F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(1100F, 33F);
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = " ";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrFrom
            // 
            this.xrFrom.BackColor = System.Drawing.Color.Transparent;
            this.xrFrom.Font = new System.Drawing.Font("Nyala", 12F);
            this.xrFrom.LocationFloat = new DevExpress.Utils.PointFloat(330.0904F, 33F);
            this.xrFrom.Name = "xrFrom";
            this.xrFrom.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrFrom.SizeF = new System.Drawing.SizeF(17.00006F, 20.125F);
            this.xrFrom.StylePriority.UseBackColor = false;
            this.xrFrom.StylePriority.UseFont = false;
            this.xrFrom.StylePriority.UseTextAlignment = false;
            this.xrFrom.Text = "ከ:";
            this.xrFrom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrDateFrom
            // 
            this.xrDateFrom.BackColor = System.Drawing.Color.Transparent;
            this.xrDateFrom.Font = new System.Drawing.Font("Nyala", 14F);
            this.xrDateFrom.LocationFloat = new DevExpress.Utils.PointFloat(347.0904F, 33F);
            this.xrDateFrom.Name = "xrDateFrom";
            this.xrDateFrom.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDateFrom.SizeF = new System.Drawing.SizeF(160.5592F, 20.12502F);
            this.xrDateFrom.StylePriority.UseBackColor = false;
            this.xrDateFrom.StylePriority.UseFont = false;
            this.xrDateFrom.StylePriority.UseTextAlignment = false;
            this.xrDateFrom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTo
            // 
            this.xrTo.BackColor = System.Drawing.Color.Transparent;
            this.xrTo.Font = new System.Drawing.Font("Nyala", 12F);
            this.xrTo.LocationFloat = new DevExpress.Utils.PointFloat(507.6497F, 33F);
            this.xrTo.Name = "xrTo";
            this.xrTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTo.SizeF = new System.Drawing.SizeF(34.70828F, 20.12502F);
            this.xrTo.StylePriority.UseBackColor = false;
            this.xrTo.StylePriority.UseFont = false;
            this.xrTo.StylePriority.UseTextAlignment = false;
            this.xrTo.Text = "እስከ:";
            this.xrTo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrDateTo
            // 
            this.xrDateTo.BackColor = System.Drawing.Color.Transparent;
            this.xrDateTo.BorderColor = System.Drawing.Color.Transparent;
            this.xrDateTo.Font = new System.Drawing.Font("Nyala", 14F);
            this.xrDateTo.LocationFloat = new DevExpress.Utils.PointFloat(542.358F, 32.9999F);
            this.xrDateTo.Name = "xrDateTo";
            this.xrDateTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDateTo.SizeF = new System.Drawing.SizeF(185.7344F, 20.12502F);
            this.xrDateTo.StylePriority.UseBackColor = false;
            this.xrDateTo.StylePriority.UseBorderColor = false;
            this.xrDateTo.StylePriority.UseFont = false;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 3F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPivotGrid1,
            this.xrChart1});
            this.PageHeader.HeightF = 246.875F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrPivotGrid1
            // 
            this.xrPivotGrid1.CellStyleName = "xrControlStyle1";
            this.xrPivotGrid1.FieldHeaderStyleName = "xrControlStyle2";
            this.xrPivotGrid1.Fields.AddRange(new DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField[] {
            this.xrPivotGridField1,
            this.xrPivotGridField2,
            this.xrPivotGridField3,
            this.xrPivotGridField4});
            this.xrPivotGrid1.FieldValueStyleName = "xrControlStyle2";
            this.xrPivotGrid1.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
            this.xrPivotGrid1.Name = "xrPivotGrid1";
            this.xrPivotGrid1.OptionsChartDataSource.UpdateDelay = 300;
            this.xrPivotGrid1.OptionsView.ShowColumnHeaders = false;
            this.xrPivotGrid1.OptionsView.ShowDataHeaders = false;
            this.xrPivotGrid1.SizeF = new System.Drawing.SizeF(1020.792F, 46.87499F);
            // 
            // xrPivotGridField1
            // 
            this.xrPivotGridField1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.xrPivotGridField1.AreaIndex = 0;
            this.xrPivotGridField1.Caption = " ";
            this.xrPivotGridField1.FieldName = "HRMServiceType";
            this.xrPivotGridField1.Name = "xrPivotGridField1";
            // 
            // xrPivotGridField2
            // 
            this.xrPivotGridField2.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.xrPivotGridField2.AreaIndex = 0;
            this.xrPivotGridField2.Caption = "ተቋም";
            this.xrPivotGridField2.FieldName = "DescriptionAm";
            this.xrPivotGridField2.Name = "xrPivotGridField2";
            // 
            // xrPivotGridField3
            // 
            this.xrPivotGridField3.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.xrPivotGridField3.AreaIndex = 0;
            this.xrPivotGridField3.FieldName = "HRActivity";
            this.xrPivotGridField3.Name = "xrPivotGridField3";
            // 
            // xrPivotGridField4
            // 
            this.xrPivotGridField4.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.xrPivotGridField4.AreaIndex = 1;
            this.xrPivotGridField4.Caption = "ገምጋሚ";
            this.xrPivotGridField4.FieldName = "UpdatedUsername";
            this.xrPivotGridField4.Name = "xrPivotGridField4";
            // 
            // xrChart1
            // 
            this.xrChart1.BorderColor = System.Drawing.Color.Black;
            this.xrChart1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrChart1.DataSource = this.xrPivotGrid1;
            xyDiagram1.AxisX.Range.SideMarginsEnabled = true;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.Range.SideMarginsEnabled = true;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.DefaultPane.EnableAxisXScrolling = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram1.DefaultPane.EnableAxisXZooming = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram1.DefaultPane.EnableAxisYScrolling = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram1.DefaultPane.EnableAxisYZooming = DevExpress.Utils.DefaultBoolean.False;
            this.xrChart1.Diagram = xyDiagram1;
            this.xrChart1.Legend.MaxHorizontalPercentage = 30D;
            this.xrChart1.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 46.875F);
            this.xrChart1.Name = "xrChart1";
            this.xrChart1.SeriesDataMember = "Series";
            this.xrChart1.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.xrChart1.SeriesTemplate.ArgumentDataMember = "Arguments";
            this.xrChart1.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative;
            sideBySideBarSeriesLabel1.LineVisible = true;
            sideBySideBarSeriesLabel1.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.Default;
            this.xrChart1.SeriesTemplate.Label = sideBySideBarSeriesLabel1;
            this.xrChart1.SeriesTemplate.ValueDataMembersSerializable = "Values";
            this.xrChart1.SizeF = new System.Drawing.SizeF(1020.792F, 200F);
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(245)))), ((int)(((byte)(216)))));
            this.xrControlStyle1.Name = "xrControlStyle1";
            this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // xrControlStyle2
            // 
            this.xrControlStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(223)))), ((int)(((byte)(201)))));
            this.xrControlStyle2.Name = "xrControlStyle2";
            this.xrControlStyle2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrTo,
            this.xrDateFrom,
            this.xrFrom,
            this.xrDateTo});
            this.ReportHeader.HeightF = 55.20833F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ReportHeader_BeforePrint);
            // 
            // rptinspectorHractivitysummery
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.ReportHeader});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 209, 3);
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1,
            this.xrControlStyle2});
            this.Version = "12.1";
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrChart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRPivotGrid xrPivotGrid1;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField xrPivotGridField1;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField xrPivotGridField2;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField xrPivotGridField3;
        private DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField xrPivotGridField4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrFrom;
        private DevExpress.XtraReports.UI.XRLabel xrDateFrom;
        private DevExpress.XtraReports.UI.XRLabel xrTo;
        private DevExpress.XtraReports.UI.XRLabel xrDateTo;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle2;
        private DevExpress.XtraReports.UI.XRChart xrChart1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel txtReportSubTitle;
        private DevExpress.XtraReports.UI.XRLabel txtReportTitleAmh;
        private DevExpress.XtraReports.UI.XRLabel txtReportSubTitleAmh;
        private DevExpress.XtraReports.UI.XRLabel txtReportTitle;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
    }
}
