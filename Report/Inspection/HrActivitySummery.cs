﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Report
{
    public partial class HrActivitySummery : DevExpress.XtraReports.UI.XtraReport
    {
        public HrActivitySummery()
        {
            InitializeComponent();
        }

        private void ReportHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            CGeneralSetting.ReadSetting();

            txtReportTitleAmh.Text = CGeneralSetting.ReportTitleAmh;
            txtReportTitle.Text = CGeneralSetting.ReportTitle;
            txtReportSubTitle.Text = CGeneralSetting.SubReportTitle;
            txtReportSubTitleAmh.Text = CGeneralSetting.SubReportTitleAmh;
        }
      

    }
}
