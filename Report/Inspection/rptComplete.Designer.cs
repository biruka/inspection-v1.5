﻿namespace Report
{
    partial class rptComplete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrReportedBy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrOrganization = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrComplainType = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrFrom = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDateFrom = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDateTo = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.InspectionDate = new DevExpress.XtraReports.Parameters.Parameter();
            this.Org = new DevExpress.XtraReports.Parameters.Parameter();
            this.Activitytype = new DevExpress.XtraReports.Parameters.Parameter();
            this.InspectionDecision = new DevExpress.XtraReports.Parameters.Parameter();
            this.UpdatedUsername = new DevExpress.XtraReports.Parameters.Parameter();
            this.FullName = new DevExpress.XtraReports.Parameters.Parameter();
            this.Unitdesc = new DevExpress.XtraReports.Parameters.Parameter();
            this.inspectstatus = new DevExpress.XtraReports.Parameters.Parameter();
            this.row = new DevExpress.XtraReports.Parameters.Parameter();
            this.DescriptionAm = new DevExpress.XtraReports.Parameters.Parameter();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.txtReportTitleAmh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.txtReportSubTitleAmh = new DevExpress.XtraReports.UI.XRLabel();
            this.txtReportTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.txtReportSubTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail.HeightF = 36.45833F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(189)))), ((int)(((byte)(177)))), ((int)(((byte)(183)))));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1149F, 24F);
            this.xrTable2.StylePriority.UseBorderColor = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell11});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "row")});
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "xrTableCell4";
            this.xrTableCell4.Weight = 0.743457872858579D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "InspectionDate")});
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "xrTableCell5";
            this.xrTableCell5.Weight = 1.446878232540973D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DescriptionAm")});
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Text = "xrTableCell7";
            this.xrTableCell7.Weight = 1.7362128523517564D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Activitytype")});
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Text = "xrTableCell8";
            this.xrTableCell8.Weight = 1.2539883604530131D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "FullName")});
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Text = "xrTableCell9";
            this.xrTableCell9.Weight = 1.4989201395548861D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "InspectionDecision")});
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Text = "xrTableCell10";
            this.xrTableCell10.Weight = 0.65260595876793781D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "UpdatedUsername")});
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Text = "xrTableCell11";
            this.xrTableCell11.Weight = 0.50493666947692128D;
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txtReportTitleAmh,
            this.xrPictureBox1,
            this.txtReportSubTitleAmh,
            this.txtReportTitle,
            this.txtReportSubTitle});
            this.TopMargin.HeightF = 226F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine1
            // 
            this.xrLine1.BackColor = System.Drawing.Color.Transparent;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 53.125F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(1149F, 23F);
            this.xrLine1.StylePriority.UseBackColor = false;
            // 
            // xrTable1
            // 
            this.xrTable1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrTable1.BorderColor = System.Drawing.Color.LightGray;
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Font = new System.Drawing.Font("Nyala", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.ForeColor = System.Drawing.Color.White;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 76.12502F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1149F, 25.20842F);
            this.xrTable1.StylePriority.UseBackColor = false;
            this.xrTable1.StylePriority.UseBorderColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseForeColor = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrReportedBy,
            this.xrOrganization,
            this.xrTableCell3,
            this.xrComplainType,
            this.xrTableCell1,
            this.xrTableCell2});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "ተ.ቁ";
            this.xrTableCell6.Weight = 0.67729977959368792D;
            // 
            // xrReportedBy
            // 
            this.xrReportedBy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(132)))), ((int)(((byte)(213)))));
            this.xrReportedBy.Name = "xrReportedBy";
            this.xrReportedBy.StylePriority.UseBackColor = false;
            this.xrReportedBy.StylePriority.UseTextAlignment = false;
            this.xrReportedBy.Text = "ቀን";
            this.xrReportedBy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrReportedBy.Weight = 1.4237603430585988D;
            // 
            // xrOrganization
            // 
            this.xrOrganization.Name = "xrOrganization";
            this.xrOrganization.StylePriority.UseTextAlignment = false;
            this.xrOrganization.Text = "ተቋም";
            this.xrOrganization.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrOrganization.Weight = 1.7085123576761792D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "የግምገማ ክንውን";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 1.2339255923565027D;
            // 
            // xrComplainType
            // 
            this.xrComplainType.Name = "xrComplainType";
            this.xrComplainType.StylePriority.UseTextAlignment = false;
            this.xrComplainType.Text = "የተገምጋሚ ስም";
            this.xrComplainType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrComplainType.Weight = 1.4749483976435927D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "ውሳኔ";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 0.64215584827559968D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "ገምጋሚ";
            this.xrTableCell2.Weight = 0.55122196356432218D;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.BurlyWood;
            this.xrLabel1.Font = new System.Drawing.Font("Nyala", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(1169F, 33F);
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "የተሟላ ግምገማ";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrFrom
            // 
            this.xrFrom.BackColor = System.Drawing.Color.Transparent;
            this.xrFrom.Font = new System.Drawing.Font("Nyala", 12F);
            this.xrFrom.LocationFloat = new DevExpress.Utils.PointFloat(422.2388F, 32.99999F);
            this.xrFrom.Name = "xrFrom";
            this.xrFrom.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrFrom.SizeF = new System.Drawing.SizeF(17.00006F, 20.125F);
            this.xrFrom.StylePriority.UseBackColor = false;
            this.xrFrom.StylePriority.UseFont = false;
            this.xrFrom.StylePriority.UseTextAlignment = false;
            this.xrFrom.Text = "ከ:";
            this.xrFrom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrDateFrom
            // 
            this.xrDateFrom.BackColor = System.Drawing.Color.Transparent;
            this.xrDateFrom.Font = new System.Drawing.Font("Nyala", 14F);
            this.xrDateFrom.LocationFloat = new DevExpress.Utils.PointFloat(439.2388F, 32.99999F);
            this.xrDateFrom.Name = "xrDateFrom";
            this.xrDateFrom.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDateFrom.SizeF = new System.Drawing.SizeF(160.5592F, 20.12502F);
            this.xrDateFrom.StylePriority.UseBackColor = false;
            this.xrDateFrom.StylePriority.UseFont = false;
            this.xrDateFrom.StylePriority.UseTextAlignment = false;
            this.xrDateFrom.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTo
            // 
            this.xrTo.BackColor = System.Drawing.Color.Transparent;
            this.xrTo.Font = new System.Drawing.Font("Nyala", 12F);
            this.xrTo.LocationFloat = new DevExpress.Utils.PointFloat(599.7988F, 32.99999F);
            this.xrTo.Name = "xrTo";
            this.xrTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTo.SizeF = new System.Drawing.SizeF(34.70828F, 20.12502F);
            this.xrTo.StylePriority.UseBackColor = false;
            this.xrTo.StylePriority.UseFont = false;
            this.xrTo.StylePriority.UseTextAlignment = false;
            this.xrTo.Text = "እስከ:";
            this.xrTo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrDateTo
            // 
            this.xrDateTo.BackColor = System.Drawing.Color.Transparent;
            this.xrDateTo.BorderColor = System.Drawing.Color.Transparent;
            this.xrDateTo.Font = new System.Drawing.Font("Nyala", 14F);
            this.xrDateTo.LocationFloat = new DevExpress.Utils.PointFloat(634.5089F, 32.9999F);
            this.xrDateTo.Name = "xrDateTo";
            this.xrDateTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDateTo.SizeF = new System.Drawing.SizeF(185.7344F, 20.12502F);
            this.xrDateTo.StylePriority.UseBackColor = false;
            this.xrDateTo.StylePriority.UseBorderColor = false;
            this.xrDateTo.StylePriority.UseFont = false;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 1.041667F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // InspectionDate
            // 
            this.InspectionDate.Description = "InspectionDate";
            this.InspectionDate.Name = "InspectionDate";
            this.InspectionDate.Type = typeof(System.DateTime);
            this.InspectionDate.Value = new System.DateTime(2013, 5, 31, 4, 15, 53, 271);
            // 
            // Org
            // 
            this.Org.Description = "Org";
            this.Org.Name = "Org";
            this.Org.Value = "";
            // 
            // Activitytype
            // 
            this.Activitytype.Description = "Activitytype";
            this.Activitytype.Name = "Activitytype";
            this.Activitytype.Value = "";
            // 
            // InspectionDecision
            // 
            this.InspectionDecision.Description = "InspectionDecision";
            this.InspectionDecision.Name = "InspectionDecision";
            this.InspectionDecision.Value = "";
            // 
            // UpdatedUsername
            // 
            this.UpdatedUsername.Description = "UpdatedUsername";
            this.UpdatedUsername.Name = "UpdatedUsername";
            this.UpdatedUsername.Value = "";
            // 
            // FullName
            // 
            this.FullName.Description = "FullName";
            this.FullName.Name = "FullName";
            this.FullName.Value = "";
            // 
            // Unitdesc
            // 
            this.Unitdesc.Description = "Unitdesc";
            this.Unitdesc.Name = "Unitdesc";
            this.Unitdesc.Value = "";
            // 
            // inspectstatus
            // 
            this.inspectstatus.Description = "inspectstatus";
            this.inspectstatus.Name = "inspectstatus";
            this.inspectstatus.Value = "";
            // 
            // row
            // 
            this.row.Description = "row";
            this.row.Name = "row";
            this.row.Type = typeof(short);
            this.row.Value = ((short)(0));
            // 
            // DescriptionAm
            // 
            this.DescriptionAm.Description = "DescriptionAm";
            this.DescriptionAm.Name = "DescriptionAm";
            this.DescriptionAm.Value = "";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrFrom,
            this.xrDateFrom,
            this.xrTo,
            this.xrDateTo,
            this.xrLine1,
            this.xrTable1});
            this.ReportHeader.HeightF = 107.2917F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ReportHeader_BeforePrint);
            // 
            // txtReportTitleAmh
            // 
            this.txtReportTitleAmh.BackColor = System.Drawing.Color.Transparent;
            this.txtReportTitleAmh.Font = new System.Drawing.Font("Nyala", 15F);
            this.txtReportTitleAmh.ForeColor = System.Drawing.Color.Black;
            this.txtReportTitleAmh.LocationFloat = new DevExpress.Utils.PointFloat(91.35994F, 91.54166F);
            this.txtReportTitleAmh.Name = "txtReportTitleAmh";
            this.txtReportTitleAmh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtReportTitleAmh.SizeF = new System.Drawing.SizeF(925F, 29.16667F);
            this.txtReportTitleAmh.StylePriority.UseBackColor = false;
            this.txtReportTitleAmh.StylePriority.UseFont = false;
            this.txtReportTitleAmh.StylePriority.UseForeColor = false;
            this.txtReportTitleAmh.StylePriority.UseTextAlignment = false;
            this.txtReportTitleAmh.Text = "የኢትዮጵያ ፌዴራዊ ዲሞክራሲያዊ ሪፐብሊክ";
            this.txtReportTitleAmh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.xrPictureBox1.ImageUrl = "~\\Images\\EthLogo.gif";
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(502.9058F, 15.54167F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(90F, 76F);
            this.xrPictureBox1.StylePriority.UseBackColor = false;
            // 
            // txtReportSubTitleAmh
            // 
            this.txtReportSubTitleAmh.BackColor = System.Drawing.Color.Transparent;
            this.txtReportSubTitleAmh.Font = new System.Drawing.Font("Nyala", 15F);
            this.txtReportSubTitleAmh.ForeColor = System.Drawing.Color.Black;
            this.txtReportSubTitleAmh.LocationFloat = new DevExpress.Utils.PointFloat(91.35992F, 120.7084F);
            this.txtReportSubTitleAmh.Name = "txtReportSubTitleAmh";
            this.txtReportSubTitleAmh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtReportSubTitleAmh.SizeF = new System.Drawing.SizeF(925F, 29.16667F);
            this.txtReportSubTitleAmh.StylePriority.UseBackColor = false;
            this.txtReportSubTitleAmh.StylePriority.UseFont = false;
            this.txtReportSubTitleAmh.StylePriority.UseForeColor = false;
            this.txtReportSubTitleAmh.StylePriority.UseTextAlignment = false;
            this.txtReportSubTitleAmh.Text = "የኢትዮጵያ ፌዴራዊ ዲሞክራሲያዊ ሪፐብሊክ";
            this.txtReportSubTitleAmh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txtReportTitle
            // 
            this.txtReportTitle.BackColor = System.Drawing.Color.Transparent;
            this.txtReportTitle.Font = new System.Drawing.Font("Nyala", 15F);
            this.txtReportTitle.ForeColor = System.Drawing.Color.Black;
            this.txtReportTitle.LocationFloat = new DevExpress.Utils.PointFloat(91.35992F, 149.8751F);
            this.txtReportTitle.Name = "txtReportTitle";
            this.txtReportTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtReportTitle.SizeF = new System.Drawing.SizeF(925F, 29.16667F);
            this.txtReportTitle.StylePriority.UseBackColor = false;
            this.txtReportTitle.StylePriority.UseFont = false;
            this.txtReportTitle.StylePriority.UseForeColor = false;
            this.txtReportTitle.StylePriority.UseTextAlignment = false;
            this.txtReportTitle.Text = "የኢትዮጵያ ፌዴራዊ ዲሞክራሲያዊ ሪፐብሊክ";
            this.txtReportTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txtReportSubTitle
            // 
            this.txtReportSubTitle.BackColor = System.Drawing.Color.Transparent;
            this.txtReportSubTitle.Font = new System.Drawing.Font("Nyala", 15F);
            this.txtReportSubTitle.ForeColor = System.Drawing.Color.Black;
            this.txtReportSubTitle.LocationFloat = new DevExpress.Utils.PointFloat(91.35992F, 179.0417F);
            this.txtReportSubTitle.Name = "txtReportSubTitle";
            this.txtReportSubTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtReportSubTitle.SizeF = new System.Drawing.SizeF(925F, 29.16667F);
            this.txtReportSubTitle.StylePriority.UseBackColor = false;
            this.txtReportSubTitle.StylePriority.UseFont = false;
            this.txtReportSubTitle.StylePriority.UseForeColor = false;
            this.txtReportSubTitle.StylePriority.UseTextAlignment = false;
            this.txtReportSubTitle.Text = "የኢትዮጵያ ፌዴራዊ ዲሞክራሲያዊ ሪፐብሊክ";
            this.txtReportSubTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // PageHeader
            // 
            this.PageHeader.HeightF = 1.041667F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.HeightF = 10.41667F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rptComplete
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageHeader,
            this.PageFooter});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 226, 1);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.InspectionDate,
            this.Org,
            this.Activitytype,
            this.InspectionDecision,
            this.UpdatedUsername,
            this.FullName,
            this.Unitdesc,
            this.inspectstatus,
            this.row,
            this.DescriptionAm});
            this.Version = "12.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        public DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrFrom;
        private DevExpress.XtraReports.UI.XRLabel xrDateFrom;
        private DevExpress.XtraReports.UI.XRLabel xrTo;
        private DevExpress.XtraReports.UI.XRLabel xrDateTo;
        private DevExpress.XtraReports.Parameters.Parameter InspectionDate;
        private DevExpress.XtraReports.Parameters.Parameter Org;
        private DevExpress.XtraReports.Parameters.Parameter Activitytype;
        private DevExpress.XtraReports.Parameters.Parameter InspectionDecision;
        private DevExpress.XtraReports.Parameters.Parameter UpdatedUsername;
        private DevExpress.XtraReports.Parameters.Parameter FullName;
        private DevExpress.XtraReports.Parameters.Parameter Unitdesc;
        private DevExpress.XtraReports.Parameters.Parameter inspectstatus;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrReportedBy;
        private DevExpress.XtraReports.UI.XRTableCell xrOrganization;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrComplainType;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.Parameters.Parameter row;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.Parameters.Parameter DescriptionAm;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
       public string headerTitlecomplete;
       private DevExpress.XtraReports.UI.XRLabel txtReportTitleAmh;
       private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
       private DevExpress.XtraReports.UI.XRLabel txtReportSubTitleAmh;
       private DevExpress.XtraReports.UI.XRLabel txtReportTitle;
       private DevExpress.XtraReports.UI.XRLabel txtReportSubTitle;
       private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
       private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
       private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
    }
}
