﻿using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

using CUSTOR;
using CUSTOR.Commen;

namespace CUSTOR.Report
{
    public partial class FindingSummaryStatisticalReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DateTime dateFrom;
        public DateTime dateTo;
        public int totalSum;
        public string reportTitle;
        public FindingSummaryStatisticalReport()
        {
            InitializeComponent();
            Today();
        }

        public void ShowDateRange()
        {
            DateTime minDate = new DateTime();
            if (!minDate.ToString().Equals(dateFrom.ToString()))
            {
                xrDateFrom.Text = EthiopicDateTime.TranslateDateMonth(dateFrom);
            }
            else
            {
                xrFrom.Visible = false;
                xrDateFrom.Visible = false;
            }
            if (!minDate.ToString().Equals(dateTo.ToString()))
            {
                xrDateTo.Text = EthiopicDateTime.TranslateDateMonth(dateTo);
            }
            else
            {
                xrTo.Visible = false;
                xrDateTo.Visible = false;
            }
        }

        protected void Today()
        {
            xrLabel17.Text = EthiopicDateTime.TranslateDateMonth(DateTime.Today);
        }

        protected void LoadReportHeaderTitle()
        {
            xrLabel1.Text = reportTitle;
        }
    }
}
