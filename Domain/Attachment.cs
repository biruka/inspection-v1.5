﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{

    public class CompliantAttachment
    {
       

        public Guid AttachmentGuid { get; set; }

        public Guid CompliantGuid { get; set; }

        public string FileNumber { get; set; }

        public string AttachmentTitle { get; set; }

        public string DocumentTitle { get; set; }

        public string FileType { get; set; }

        public double FileSize { get; set; }

        public string FileSizeCaption { get; set; }

        public string FileUrl { get; set; }

    }
}