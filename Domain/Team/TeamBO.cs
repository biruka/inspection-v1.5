﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CUSTOR.Domain
{
    public class tblTeam
    {
        public Guid TeamGuid { get; set; }

        public string TeamNameAm { get; set; }

        public string TeamName { get; set; }

        public string UserName { get; set; }

        public DateTime EventDatetime { get; set; }

        public string UpdatedUsername { get; set; }

        public DateTime UpdatedEventDatetime { get; set; }


    }
}
