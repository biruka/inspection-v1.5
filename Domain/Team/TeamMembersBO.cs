﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CUSTOR.Domain
{
    public class tblTeamMembers
    {
        public int SNo { get; set; }
        public Guid TeamMembersGuid { get; set; }

        public Guid TeamGuid { get; set; }

        public Guid PersonGuid { get; set; }

        public string PositionInTeam { get; set; }

        public string UserName { get; set; }

        public string FullName { get; set; }

        public DateTime EventDatetime { get; set; }

        public string UpdatedUsername { get; set; }

        public DateTime UpdatedEventDatetime { get; set; }

        public string Comment { get; set; }

    }
}
