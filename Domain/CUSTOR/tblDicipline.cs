﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class tblDicipline
    {

        public Guid MainGuid { get; set; }

        public int Seq_no { get; set; }

        public Guid ParentGuid { get; set; }

        public DateTime Date_action_taken { get; set; }

        public string Break_type { get; set; }

        public string Break_description { get; set; }

        public string Action_taken { get; set; }

        public string Remark { get; set; }

        public DateTime Date_lifted { get; set; }

    }
}