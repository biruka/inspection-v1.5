﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class CandidateEntityView
    {

        public string FullName { get; set; }

        public string Filedofstudy { get; set; }

        public string Expyear { get; set; }

        public Guid MainGuid { get; set; }

        public Guid Announcement_Guid { get; set; }

        public double TheoryExamResult { get; set; }

        public double PracticalExamResult { get; set; }

        public double InterviewExamResult { get; set; }

        public string Selection_Criteria { get; set; }

        public double Result { get; set; }

        public bool Is_Hired { get; set; }

    }
}
