﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class InspReportEntityview
    {

        public string FullName { get; set; }

        public Guid MainGuid { get; set; }

        public string Gender { get; set; }

        public string Typeofemployment { get; set; }

        public int Person_id { get; set; }

        public string Staff_code { get; set; }

        public string Staff_code_sort { get; set; }

        public string Cost_center_code { get; set; }

        public string Inspectstatus { get; set; }

        public string Job_title { get; set; }

        public int Id { get; set; }

        public int Org_Code { get; set; }

        public string Org_name { get; set; }

        public int Grade_id { get; set; }

        public string AmGrade { get; set; }

        public string Description { get; set; }

        public string InspectionDecision { get; set; }

        public string InspectionFinding { get; set; }

    }
}