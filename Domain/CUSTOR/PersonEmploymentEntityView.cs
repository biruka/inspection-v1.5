﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class PersonEmploymentEntityView
    {

        public string First_am_name { get; set; }

        public string Father_am_name { get; set; }

        public string Grand_am_name { get; set; }

        public string Gender { get; set; }

        public DateTime Birth_date { get; set; }

        public string Typeofemployment { get; set; }

        public int Person_id { get; set; }

        public string Staff_code { get; set; }

        public string Staff_code_sort { get; set; }

        public string Cost_center_code { get; set; }

        public string First_name { get; set; }

        public string Father_name { get; set; }

        public double Allowance { get; set; }

        public double Salary { get; set; }

        public double Transport_allowance { get; set; }

        public double Hardship_allowance { get; set; }

        public double Housing_allowance { get; set; }

        public double Over_time { get; set; }

        public double Pension_employee_contribution { get; set; }

        public double Pension_company_contribution { get; set; }

        public bool Is_ca_member { get; set; }

        public DateTime Ca_membership_date { get; set; }

        public DateTime Ca_termination_date { get; set; }

        public double Thrift_contribution { get; set; }

        public double Extra_contribution { get; set; }

        public double Card_fee { get; set; }

        public double Membership_fee { get; set; }

        public bool Is_membership_fee_returned { get; set; }

        public bool Is_lu_member { get; set; }

        public double Labour_union { get; set; }

        public double Leave_without_pay { get; set; }

        public double Absentism { get; set; }

        public double Fine { get; set; }

        public int Sex { get; set; }

        public int Religion { get; set; }

        public string Birth_place { get; set; }

        public int Marital_status { get; set; }

        public int Family_size { get; set; }

        public int Nationality { get; set; }

        public int Town { get; set; }

        public int Region { get; set; }

        public string P_o_box { get; set; }

        public string Person_address { get; set; }

        public string Tel_home { get; set; }

        public string Tel_off_direct1 { get; set; }

        public string Tel_off_direct2 { get; set; }

        public string Tel_off_ext1 { get; set; }

        public string Room_no { get; set; }

        public string Passport_no { get; set; }

        public int License_grade { get; set; }

        public int Education_group { get; set; }

        public int Education_level { get; set; }

        public string Education_description { get; set; }

        public string Job_title { get; set; }

        public string Job_title_Sort { get; set; }

        public int Occupation { get; set; }

        public string Occupation_step { get; set; }

        public DateTime Emp_date { get; set; }

        public string Emp_letter { get; set; }

        public int Current_status { get; set; }

        public DateTime Date_termination { get; set; }

        public string Termination_reason { get; set; }

        public int Mother_tongue { get; set; }

        public int Ethnicity { get; set; }

        public int Location { get; set; }

        public int Id { get; set; }

        public Guid Org_Code { get; set; }

        public string Org_name { get; set; }

        public string First_name_sort { get; set; }

        public string Father_name_sort { get; set; }

        public string Grand_name_sort { get; set; }

        public string First_name_soundeX { get; set; }

        public string Father_name_soundeX { get; set; }

        public string Grand_name_soundeX { get; set; }

        public int Leave_balance { get; set; }

        public string Pension_no { get; set; }

        public bool Is_teacher { get; set; }

        public int Is_police { get; set; }

        public int Working_days { get; set; }

        public double Cost_sharing_total_amount { get; set; }

        public double Cost_sharing_total_paid { get; set; }

        public double Cost_sharing_monthly_payment { get; set; }

        public int Health_Status { get; set; }

    }
}