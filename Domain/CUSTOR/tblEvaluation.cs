﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class tblEvaluation
    {

        public Guid MainGuid { get; set; }

        public int Seq_no { get; set; }

        public Guid ParentGuid { get; set; }

        public DateTime Fiscal_year { get; set; }

        public string strFiscal_year { get; set; }

        public int Eval_year { get; set; }

        public int Period { get; set; }

        public string Periods { get; set; }

        public int EvaluationLevel { get; set; }

        public float Eval_point { get; set; }

        public string strEval_point { get; set; }

        public decimal Eval_pointD { get; set; }

        public string Strong_side { get; set; }

        public string Weak_side { get; set; }

        public string Remark { get; set; }

        public string Is_Rejected { get; set; }

        public string Rejected_by { get; set; }

        public DateTime Rejected_Date { get; set; }

        public string Appear_Remark { get; set; }

        public string Is_Final_Decision { get; set; }

        public string Final_Decision_Given_By { get; set; }

        public string Final_Decision { get; set; }

        public DateTime Final_Decision_Date { get; set; }

    }
}