﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class ApplicantRequisitionExamresult
    {



        public string Full_Name { get; set; }

        public Guid Employment_Requisition_Guid { get; set; }

        public double TheoryExamResult { get; set; }

        public double PracticalExamResult { get; set; }

        public double InterviewExamResult { get; set; }

        public double Result { get; set; }

        public Guid ApplicantExamGuid { get; set; }

        public Guid ApplicantGuid { get; set; }

    }
}