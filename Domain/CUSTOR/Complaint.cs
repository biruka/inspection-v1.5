﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class Complaint
    {
        private string _ComplaintRemark;

        public Guid ComplaintGUID { get; set; }

        public Guid OrgGuid { get; set; }

        public Guid EmployeeGUID { get; set; }

        public string ReportedBy { get; set; }

        public string ReporterEmail { get; set; }

        public string Referenceno { get; set; }

        public string ReporterTel { get; set; }

        public int MeansOfReport { get; set; }

        public string ComplaintDescription { get; set; }

        public string ComplaintRemark
        {
            get { return _ComplaintRemark; }
            set
            {
                if (value != null && value.Length > 3999)
                    throw new ArgumentOutOfRangeException("Invalid value for Remark", value, value.ToString());
                _ComplaintRemark = value;
            }
        }

        public DateTime DateReported { get; set; }

        public DateTime DateScreened { get; set; }

        public string ScreenedBy { get; set; }

        public int ScreeningDecision { get; set; }

        public int FindingSummary { get; set; }

        public string FindingSummaryDecision { get; set; }

        public bool IsGrievance { get; set; }

        public bool Isselfservice { get; set; }

    }
}