﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class tblOrganization
    {

        public int Id { get; set; }

        public int Org_Code { get; set; }

        public string Org_name { get; set; }

        public string Kifle_ketema { get; set; }

        public string Kebele { get; set; }

        public string House_no { get; set; }

        public string Tel_no1 { get; set; }

        public string Tel_no2 { get; set; }

        public string Org_category { get; set; }

    }
}