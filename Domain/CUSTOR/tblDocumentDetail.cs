﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class tblDocumentDetail
    {
        public Guid MainGuid { get; set; }

        public Guid DocMainGuid { get; set; }

        public int Id { get; set; }

    }
}