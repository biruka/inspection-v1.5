﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class tblTermination
    {

        public Guid MainGuid { get; set; }

        public Guid ParentGuid { get; set; }

        public DateTime Terminationdate { get; set; }

        public string Termination_reason { get; set; }

        public string Reason { get; set; }

        public string Remark { get; set; }

        public string NewJobTitle { get; set; }

        public string NewRank { get; set; }

        public string Punishment { get; set; }

        public DateTime Returndate { get; set; }

    }
}