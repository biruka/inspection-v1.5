﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class Organization
    {
        public Guid OrgGuid { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public string DescriptionAm { get; set; }

        public string DescriptionAmSort { get; set; }

        public string DescriptionTig { get; set; }

        public string DescriptionTigSort { get; set; }

        public string DescriptionOr { get; set; }

        public string DescriptionAf { get; set; }

        public string DescriptionSom { get; set; }

        public int YearEstablished { get; set; }

        public string ParentAdministrationCode { get; set; }

        public string OrgType { get; set; }

        public string OrgHistory { get; set; }

        public bool IsActive { get; set; }

        public string FileNumber { get; set; }

        public string Telephone { get; set; }

        public string POBox { get; set; }

        public string EMail { get; set; }

        public string WebURL { get; set; }

        public string SiteName { get; set; }

        public string InspectedBy { get; set; }

        public byte[] Logo { get; set; }

        public string UserName { get; set; }

        public DateTime EventDatetime { get; set; }

        public string UpdatedUsername { get; set; }

        public DateTime UpdatedEventDatetime { get; set; }

    }
}