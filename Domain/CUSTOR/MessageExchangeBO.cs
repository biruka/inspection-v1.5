﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace CUSTOR.Domain
{

    [Serializable]
    public class MessageExchangeBO
    {
        public int ParentMessageID { get; set; }
        public int ID { get; set; }
        public int ParentID { get; set; }

        public Guid OrgGuid { get; set; }
        public string OrgName { get; set; }
        public string Case { get; set; }

        public string Subject { get; set; }
        public DateTime Date { get; set; }

        public string EmployeeName { get; set; }

        public bool Status { get; set; }

        public string Text { get; set; }
        public string FStatus { get; set; }
        public string Url { get; set; }

        public string Decission { get; set; }

        public string UserName { get; set; }

        public DateTime EventDatetime { get; set; }

        public string UpdatedUsername { get; set; }

        public DateTime UpdatedEventDatetime { get; set; }

        public String FinalRevisedBy { get; set; }

        public String FinalAnswerdBy { get; set; }

        public Int32 FlowStatus1 { get; set; }

        public string FlowStatus { get; set; }

        public DateTime SentDate { get; set; }

        public DateTime AnsweredDate { get; set; }

        public int MessageStatus { get; set; }

        public void Assign(MessageExchangeBO source)
        {
            ParentID = source.ParentID;
            Url = source.Url;
            Case = source.Case;
            OrgGuid = source.OrgGuid;
            Subject = source.Subject;
            Text = source.Text;
            Status = source.Status;
            EmployeeName = source.EmployeeName;
        }
    }

    public class AttachmentMessageExchangeBO
    {
        public Guid MainGuid { get; set; }

        public string Url { get; set; }

        public int ID { get; set; }

        public string txtFileDescription { get; set; }

        public string UserName { get; set; }

        public DateTime EventDatetime { get; set; }

        public string UpdatedUsername { get; set; }

        public DateTime UpdatedEventDatetime { get; set; }
    }
}