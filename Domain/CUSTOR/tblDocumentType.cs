﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class tblDocumentType
    {



        public Guid MainGuid { get; set; }

        public string Description { get; set; }

        public string DescriptionAm { get; set; }

        public string DescriptionSoudx { get; set; }

        public string DescriptionSortValue { get; set; }

        public string strReulatoryBody { get; set; }

        public int ReulatoryBody { get; set; }

        public string Code { get; set; }
        

    }
}