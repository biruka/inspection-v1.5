﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class tblTransferHistory
    {
        public Guid MainGuid { get; set; }

        public int Seq_no { get; set; }

        public Guid ParentGuid { get; set; }

        public DateTime Transfer_date { get; set; }

        public int Transferred_from { get; set; }

        public int Transferred_to { get; set; }

        public string Remark { get; set; }

        public double From_salary { get; set; }

        public double To_salary { get; set; }

        public string From_job_title { get; set; }

        public string To_job_title { get; set; }

        public string From_rank { get; set; }

        public string To_rank { get; set; }

        public string TransferType { get; set; }

    }
}