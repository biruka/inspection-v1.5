﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class tblEmploymentHistory
    {
         public Guid MainGuid { get; set; }

        public int Seq_no { get; set; }

        public Guid ParentGuid { get; set; }

        public string Institution { get; set; }

        public string Location { get; set; }

        public string Tax_payment_letter_no { get; set; }

        public string Job_title { get; set; }

        public DateTime Date_from { get; set; }

        public DateTime Date_to { get; set; }

        public double Salary { get; set; }

        public DateTime Date_termination { get; set; }

        public string Termination_reason { get; set; }

        public int Emp_status { get; set; }

        public string Net_duration { get; set; }

        public string Remark { get; set; }

    }
}