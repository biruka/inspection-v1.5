﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class InspectionFollowupEntiryview
    {


        public Guid EmployeeGUID { get; set; }

        public Guid ComplaintGUID { get; set; }

        public Guid FollowupGUID { get; set; }

        public Guid InspectionGUID { get; set; }

        public DateTime FollowupDate { get; set; }

        public string UserName { get; set; }

        public DateTime EventDatetime { get; set; }

        public string UpdatedUsername { get; set; }

        public DateTime UpdatedEventDatetime { get; set; }

        public bool Isvalid { get; set; }

        public string Filename { get; set; }

        public string Reanswer { get; set; }

        public string InspectionDecision { get; set; }

        public int FollowupStatus { get; set; }

        public int ActionTaken { get; set; }

        public string Remark { get; set; }

    }
}