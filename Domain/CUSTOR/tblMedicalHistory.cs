﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class tblMedicalHistory
    {

        public int Seq_no { get; set; }

        public Guid Staff_id { get; set; }

        public int Associate_id { get; set; }

        public DateTime Diagnostic_date { get; set; }

        public string Infection_code { get; set; }

        public string Treatment_given { get; set; }

        public double Visiting_fee { get; set; }

        public double Medicine_expense { get; set; }

        public double Laboratory_expense { get; set; }

        public double Xray_expense { get; set; }

        public double Other_expense { get; set; }

        public int Is_paying { get; set; }

        public DateTime Birth_date { get; set; }

        public int Sex { get; set; }

        public int Patient_type { get; set; }

        public string Remark { get; set; }

    }
}