﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class tblTeamInOrganization
    {
        
        public Guid MainGuid { get; set; }

        public Guid TeamGuid { get; set; }

        public Guid OrgGuid { get; set; }

        public DateTime BudgetYear { get; set; }

    }
}