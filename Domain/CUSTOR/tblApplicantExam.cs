﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class tblApplicantExam
    {

        public Guid ApplicantExamGuid { get; set; }

        public Guid ApplicantGuid { get; set; }

        public bool IsTheoryTaken { get; set; }

        public bool IsPracticalTaken { get; set; }

        public bool IsInterviewTaken { get; set; }

        public int TheoryPercentage { get; set; }

        public int PracticalPercentage { get; set; }

        public int InterviewPercentage { get; set; }

        public double TheoryExamResult { get; set; }

        public double PracticalExamResult { get; set; }

        public double InterviewExamResult { get; set; }

        public double Result { get; set; }

    }
}