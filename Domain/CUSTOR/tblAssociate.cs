﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class tblAssociate
    {
        public Guid MainGuid { get; set; }

        public int Seq_no { get; set; }

        public Guid ParentGuid { get; set; }

        public string Full_name { get; set; }

        public string Region { get; set; }

        public string Town { get; set; }

        public string Address { get; set; }

        public string P_o_box { get; set; }

        public string Tel_res { get; set; }

        public string Tel_off { get; set; }

        public int Sex { get; set; }

        public DateTime Date_of_birth { get; set; }

        public int Nationality { get; set; }

        public string Occupation { get; set; }

        public string Birth_place { get; set; }

        public int Familiy_type { get; set; }

        public string Remark { get; set; }

        public bool Is_contact { get; set; }

        public bool Is_reference { get; set; }

        public bool Is_beneficiary { get; set; }

        public int Three_month_salary_order { get; set; }

        public string Zone { get; set; }

        public string Worede { get; set; }

        public string Kebele { get; set; }

        public string GenderDisplay { get; set; }

        public string Familytext { get; set; }
    }
}