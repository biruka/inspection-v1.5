﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class tblInspection
    {

        public Guid InspectionGUID { get; set; }

        public Guid EmployeeGUID { get; set; }

        public Guid ComplaintGUID { get; set; }

        public DateTime InspectionDate { get; set; }

        public int HRMServiceType { get; set; }

        public string InspectionDecision { get; set; }

        public string InspectionFinding { get; set; }

        public string UserName { get; set; }

        public DateTime EventDatetime { get; set; }

        public string UpdatedUsername { get; set; }

        public DateTime UpdatedEventDatetime { get; set; }

        public int Activitytype { get; set; }

        public int inspectiontype { get; set; }

        public string Answer { get; set; }

        public string Subject { get; set; }

        public DateTime AnswerDate { get; set; }

        public string approvalname { get; set; }

        public DateTime approvaldate { get; set; }

        public Boolean Approved { get; set; }

    }
}