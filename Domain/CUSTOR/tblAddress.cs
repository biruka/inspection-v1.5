﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class tblAddress
    {
        public Guid RegionID { get; set; }

        public string Region { get; set; }

        public Guid ParentID { get; set; }

        public int LevelNo { get; set; }

        public string Description { get; set; }

    }
}