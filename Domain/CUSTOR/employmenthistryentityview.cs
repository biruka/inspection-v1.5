﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class employmenthistryentityview
    {

        public Guid Applicant_Guid { get; set; }

        public string Institution { get; set; }

        public string Job_title { get; set; }

        public DateTime Date_from { get; set; }

        public DateTime Date_to { get; set; }

        public DateTime Date_termination { get; set; }

        public string Termination_reason { get; set; }

        public string Net_duration { get; set; }

    }
}