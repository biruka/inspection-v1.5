﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class tblTrainingHistory
    {

        public Guid MainGuid { get; set; }

        public int Seq_no { get; set; }

        public Guid ParentGuid { get; set; }

        public string Institution { get; set; }

        public string Location { get; set; }

        public DateTime Period_from { get; set; }

        public DateTime Period_to { get; set; }

        public string Duration { get; set; }

        public string Course_name { get; set; }

        public int Training_type { get; set; }

        public double Expense { get; set; }

        public string Sponsor { get; set; }

        public string Remark { get; set; }

        public double Total_cost { get; set; }

        public int Percent_covered { get; set; }

        public string Address { get; set; }

        public string Contact_person { get; set; }

        public int Training_nature { get; set; }

        public double Other_cost { get; set; }

        public double Budget { get; set; }

        public double Points_scored { get; set; }

        public string Country { get; set; }

        public bool Attachment { get; set; }

        public string Certificat_Type { get; set; }

        public string NotAttachedReson { get; set; }

        public bool AffirmativeAactionGiven { get; set; }

        public bool AffirmativeDoc_Attached { get; set; }

        public string Affir_Doc_NotAttachedReason { get; set; }

        public bool Aff_AactionWomen { get; set; }

        public bool Aff_AactionforHandicap { get; set; }

        public bool Aff_AactionforNationsNationalities { get; set; }

        public string Aff_Act_NotGivenReason { get; set; }

        public bool Is_Trainig_Related_with_Job { get; set; }

        public bool Given_by_the_Org { get; set; }

    }
}