﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class tblPromotionHistory
    {

        public Guid MainGuid { get; set; }

        public int Seq_no { get; set; }

        public Guid ParentGuid { get; set; }

        public double From_salary { get; set; }

        public double To_salary { get; set; }

        public string From_job_title { get; set; }

        public string To_job_title { get; set; }

        public DateTime Date_promoted { get; set; }

        public int Promotion_type { get; set; }

        public string Remark { get; set; }

        public string From_rank { get; set; }

        public string To_rank { get; set; }

        public double From_allowance { get; set; }

        public double To_allowance { get; set; }

        public int From_grade { get; set; }

        public int To_grade { get; set; }

        public Guid RankGuid { get; set; }

        public Guid RankGuidFrom { get; set; }

    }
}