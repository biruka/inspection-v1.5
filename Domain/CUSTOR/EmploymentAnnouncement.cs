﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class EmploymentAnnouncement
    {


        public Guid Employment_Announcement_Guid { get; set; }

        public DateTime Announcement_Date { get; set; }

        public DateTime Deadline_Date { get; set; }

        public DateTime Deadline_Time { get; set; }

        public int Announcement_Type { get; set; }

        public string Registration_Place { get; set; }

        public string Remark { get; set; }

        public Guid Employment_Requisition_Guid { get; set; }

        public string Documents_Required { get; set; }

    }
}