﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class Attachment
    {

        public Guid MainGuid { get; set; }

        public Guid ParentGuid { get; set; }

        public string UserName { get; set; }

        public string AttachmentContent { get; set; }

        public string AttachmentContent_Sort { get; set; }

        public string AttachmentContent_Soundx { get; set; }

        public string SelectedWord { get; set; }

        public string SelectedWord_Sort { get; set; }

        public string SelectedWord_Soundx { get; set; }

        public string Url { get; set; }

        public string Remark { get; set; }

        public string FileType { get; set; }

        public DateTime SentDate { get; set; }

        public string FileLocation { get; set; }

        public string Reference { get; set; }

    }
}