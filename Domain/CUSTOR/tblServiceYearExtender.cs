﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class tblServiceYearExtender
    {

        public Guid SYEMainGuid { get; set; }

        public int Seq_no { get; set; }

        public int SYEYear { get; set; }

        public int SYEHowMuch { get; set; }

        public bool Reason { get; set; }

        public string AnotherReason { get; set; }

        public string CreatedBy { get; set; }

        public bool Approved { get; set; }

        public string ApprovedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ApprovedDate { get; set; }

        public Guid PersonGuid { get; set; }

        public string Remark { get; set; }

    }
}