﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class tblDocument
    {

        public Guid MainGuid { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public string DescriptionAm { get; set; }

        public Guid ParentGuid { get; set; }

        public string ParentGuids { get; set; }

        public bool Isoptional { get; set; }

        public string strIsoptional { get; set; }

    }
}