﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class tblOverTime
    {

        public Guid MainGuid { get; set; }

        public int Overtime_id { get; set; }

        public string Staff_code { get; set; }

        public Guid ParentGuid { get; set; }

        public int Overtime_month { get; set; }

        public int Overtime_year { get; set; }

        public int Overtime_type { get; set; }

        public string Overtime_reason { get; set; }

        public double Worked_hours { get; set; }

        public double Overtime_amount { get; set; }

        public DateTime Overtime_date { get; set; }

    }
}