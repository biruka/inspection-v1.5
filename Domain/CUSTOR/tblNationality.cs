﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class tblNationality
    {

        public string Code { get; set; }

        public string Description { get; set; }

        public string Amdescription { get; set; }

        public string Parent { get; set; }

        public int Id { get; set; }

    }
}