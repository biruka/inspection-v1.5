﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace CUSTOR.Domain
{
    public class tblLateComer
    {

        public Guid MainGuid { get; set; }

        public int Seq_no { get; set; }

        public Guid ParentGuid { get; set; }

        public DateTime Late_date { get; set; }

        public int Period { get; set; }

        public int Duration { get; set; }

        public string Remark { get; set; }

    }
}