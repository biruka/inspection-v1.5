﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CUSTOR.Domain
{
    public class SpecialAssignment
    {
        public Guid MainGuid { get; set; }

        public int Seq_no { get; set; }

        public Guid ParentGuid { get; set; }

        public DateTime From_date { get; set; }

        public DateTime To_date { get; set; }

        public string Work_performed { get; set; }

        public string Remark { get; set; }

        public bool Benifit { get; set; }

        public bool Reserve { get; set; }

        public int ScaleType { get; set; }

        public string Latter_No { get; set; }

        public bool Isdeliget { get; set; }
    }
}
