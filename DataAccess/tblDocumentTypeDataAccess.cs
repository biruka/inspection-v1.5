﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblDocumentTypeDataAccess
    {

        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [MainGuid],[Description],[DescriptionAm],[ReulatoryBody],[Code] FROM [dbo].[tblDocumentType]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblDocumentType");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocumentType::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public DataTable SearchDocumentTypes(string documentTypeDescriptionEng, string documentTypeDescriptionRegional, bool isGeezFont)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [MainGuid],Code,[Description],[DescriptionAm],[ReulatoryBody],
                                    CASE ReulatoryBody WHEN 99 THEN N'ፌደራል' 
                                     WHEN 1 THEN N'ትግራይ' 
                                     WHEN 2 THEN N'አፋር' 
                                     WHEN 3 THEN N'አማራ' 
                                     WHEN 4 THEN N'ኦሮሚያ' 
                                     WHEN 5 THEN N'ቤኒሻንጉል ጉምዝ' 
                                     WHEN 6 THEN N'ደ/ኢ/ብ/ብ/ክ/መንግሥት' 
                                    END as strReulatoryBody,[Code] FROM [dbo].[tblDocumentType] where 1 = 1";
            if (documentTypeDescriptionEng != "")
            {
                strGetAllRecords += " AND Description like '%" + documentTypeDescriptionEng + "%' ";
            }
            if (documentTypeDescriptionRegional != "")
            {
                if(isGeezFont)
                {
                    strGetAllRecords += " AND DescriptionSortValue like '%" + documentTypeDescriptionRegional + "%'";
                }
                else
                {
                    strGetAllRecords += " AND DescriptionAm like '%" + documentTypeDescriptionRegional + "%'";
                }
            }
            strGetAllRecords += "ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblDocumentType");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocumentType::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public tblDocumentType GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblDocumentType objtblDocumentType = new tblDocumentType();
            string strGetRecord = @"SELECT [MainGuid],[Description],[DescriptionAm],[ReulatoryBody],[Code] FROM [dbo].[tblDocumentType] Where [MainGuid]=@MainGuid ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblDocumentType");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblDocumentType.MainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    if (dTable.Rows[0]["Description"].Equals(DBNull.Value))
                        objtblDocumentType.Description = string.Empty;
                    else
                        objtblDocumentType.Description = (string)dTable.Rows[0]["Description"];
                    if (dTable.Rows[0]["DescriptionAm"].Equals(DBNull.Value))
                        objtblDocumentType.DescriptionAm = string.Empty;
                    else
                        objtblDocumentType.DescriptionAm = (string)dTable.Rows[0]["DescriptionAm"];
                    if (dTable.Rows[0]["ReulatoryBody"].Equals(DBNull.Value))
                        objtblDocumentType.ReulatoryBody = 0;
                    else
                        objtblDocumentType.ReulatoryBody = (int)dTable.Rows[0]["ReulatoryBody"];
                    if (dTable.Rows[0]["Code"].Equals(DBNull.Value))
                        objtblDocumentType.Code = string.Empty;
                    else
                        objtblDocumentType.Code = (string)dTable.Rows[0]["Code"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblDocumentType::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblDocumentType;
        }

        public bool Insert(tblDocumentType objtblDocumentType)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"Declare @DocumentTypeCode  int;
                                 Set @DocumentTypeCode = NEXT VALUE FOR DocumentTypeCode;
                                 INSERT INTO [dbo].[tblDocumentType]
                                       ([MainGuid],[Description],[DescriptionAm],DescriptionSortValue,DescriptionSoudx,[ReulatoryBody],[Code])
                                 VALUES ( ISNULL(@MainGuid, (newid())),@Description,@DescriptionAm,@DescriptionSortValue,@DescriptionSoudx,@ReulatoryBody,@DocumentTypeCode)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", objtblDocumentType.MainGuid));
                command.Parameters.Add(new SqlParameter("@Description", objtblDocumentType.Description));
                command.Parameters.Add(new SqlParameter("@DescriptionAm", objtblDocumentType.DescriptionAm));
                command.Parameters.Add(new SqlParameter("@DescriptionSortValue", objtblDocumentType.DescriptionSortValue));
                command.Parameters.Add(new SqlParameter("@DescriptionSoudx", objtblDocumentType.DescriptionSoudx));
                command.Parameters.Add(new SqlParameter("@ReulatoryBody", objtblDocumentType.ReulatoryBody));
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return (_rowsAffected > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocumentType::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(tblDocumentType objtblDocumentType)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[tblDocumentType] SET [MainGuid]=@MainGuid,[Description]=@Description,[DescriptionAm]=@DescriptionAm,
                               DescriptionSortValue = @DescriptionSortValue,DescriptionSoudx = @DescriptionSoudx WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", objtblDocumentType.MainGuid));
                command.Parameters.Add(new SqlParameter("@Description", objtblDocumentType.Description));
                command.Parameters.Add(new SqlParameter("@DescriptionAm", objtblDocumentType.DescriptionAm));
                //command.Parameters.Add(new SqlParameter("@ReulatoryBody", objtblDocumentType.ReulatoryBody));
                //command.Parameters.Add(new SqlParameter("@Code", objtblDocumentType.Code));
                command.Parameters.Add(new SqlParameter("@DescriptionSortValue", objtblDocumentType.DescriptionSortValue));
                command.Parameters.Add(new SqlParameter("@DescriptionSoudx", objtblDocumentType.DescriptionSoudx));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocumentType::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblDocumentType] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocumentType::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public List<tblDocumentType> GetList()
        {
            List<tblDocumentType> RecordsList = new List<tblDocumentType>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [MainGuid],Code,[Description],[DescriptionAm],[ReulatoryBody],
                                    CASE ReulatoryBody WHEN 99 THEN N'ፌደራል' 
                                     WHEN 1 THEN N'ትግራይ' 
                                     WHEN 2 THEN N'አፋር' 
                                     WHEN 3 THEN N'አማራ' 
                                     WHEN 4 THEN N'ኦሮሚያ' 
                                     WHEN 5 THEN N'ቤኒሻንጉል ጉምዝ' 
                                     WHEN 6 THEN N'ደ/ኢ/ብ/ብ/ክ/መንግሥት' 
                                    END as strReulatoryBody,[Code] 
                                    FROM [dbo].[tblDocumentType]  ORDER BY  [tblDocumentType].Code ASC";
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblDocumentType objtblDocumentType = new tblDocumentType();
                    if (dr["MainGuid"].Equals(DBNull.Value))
                        objtblDocumentType.MainGuid = Guid.Empty;
                    else
                        objtblDocumentType.MainGuid = (Guid)dr["MainGuid"];
                    if (dr["Description"].Equals(DBNull.Value))
                        objtblDocumentType.Description = string.Empty;
                    else
                        objtblDocumentType.Description = (string)dr["Description"];
                    if (dr["DescriptionAm"].Equals(DBNull.Value))
                        objtblDocumentType.DescriptionAm = string.Empty;
                    else
                        objtblDocumentType.DescriptionAm = (string)dr["DescriptionAm"];
                    if (dr["strReulatoryBody"].Equals(DBNull.Value))
                        objtblDocumentType.strReulatoryBody = string.Empty;
                    else
                        objtblDocumentType.strReulatoryBody = (string)dr["strReulatoryBody"];
                    
                    if (dr["ReulatoryBody"].Equals(DBNull.Value))
                        objtblDocumentType.ReulatoryBody = 0;
                    else
                        objtblDocumentType.ReulatoryBody = (int)dr["ReulatoryBody"];
                    if (dr["Code"].Equals(DBNull.Value))
                        objtblDocumentType.Code = string.Empty;
                    else
                        objtblDocumentType.Code = (string)dr["Code"];
                    RecordsList.Add(objtblDocumentType);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblDocumentType::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public List<tblDocumentType> GetDocTypeList(int Language)
        {
            List<tblDocumentType> RecordsList = new List<tblDocumentType>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
            string strGetAllRecords;
            if (Language == 1)
            {
                strGetAllRecords = @"SELECT [MainGuid],[Description],[DescriptionAm],[ReulatoryBody],[Code] FROM [dbo].[tblDocumentType]  ORDER BY  [Code] ASC";
            }
            else 
            {
                strGetAllRecords = @"SELECT [MainGuid],[Description],[DescriptionAm] as DescriptionAm,[ReulatoryBody],[Code] FROM [dbo].[tblDocumentType]  ORDER BY  [Code] ASC";

            }
            

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblDocumentType objtblDocumentType = new tblDocumentType();
                    if (dr["MainGuid"].Equals(DBNull.Value))
                        objtblDocumentType.MainGuid = Guid.Empty;
                    else
                        objtblDocumentType.MainGuid = (Guid)dr["MainGuid"];
                    if (dr["Description"].Equals(DBNull.Value))
                        objtblDocumentType.Description = string.Empty;
                    else
                        objtblDocumentType.Description = (string)dr["Description"];
                    if (dr["DescriptionAm"].Equals(DBNull.Value))
                        objtblDocumentType.DescriptionAm = string.Empty;
                    else
                        objtblDocumentType.DescriptionAm = (string)dr["DescriptionAm"];
                    if (dr["ReulatoryBody"].Equals(DBNull.Value))
                        objtblDocumentType.ReulatoryBody = 0;
                    else
                        objtblDocumentType.ReulatoryBody = (int)dr["ReulatoryBody"];
                    if (dr["Code"].Equals(DBNull.Value))
                        objtblDocumentType.Code = string.Empty;
                    else
                        objtblDocumentType.Code = (string)dr["Code"];
                    RecordsList.Add(objtblDocumentType);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblDocumentType::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public bool Exists(string description, string descriptionRegionallName, bool isGeez)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            string strExists = @"SELECT * FROM [dbo].[tblDocumentType] WHERE [Description]='" + description + "'";
            if (descriptionRegionallName != "")
            {
                if (isGeez)
                {
                    strExists += " or DescriptionSortValue ='" + descriptionRegionallName + "'";
                }
                else
                {
                    strExists += " or DescriptionAm ='" + descriptionRegionallName + "'";
                }
            } 

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblDocumentType");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
                return dTable.Rows.Count > 0;
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocumentType::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
        }
    }
}