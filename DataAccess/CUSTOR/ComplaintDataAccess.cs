﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.Commen;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CProfile;

namespace CUSTOR.DataAccess
{
    public class ComplaintDataAccess
    {
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public bool Insert(Complaint objComplaint)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"Declare @CompliantCode  int;
                                 Set @CompliantCode = NEXT VALUE FOR CompliantCode;
                                 INSERT INTO [dbo].[tblComplaint]
                                        ([ComplaintGUID],[OrgGuid],[EmployeeGUID],[ReportedBy],[ReporterEmail],[Referenceno],[ReporterTel],[MeansOfReport],
                                         [ComplaintDescription],[ComplaintRemark],[DateReported],[DateScreened],[ScreenedBy],[ScreeningDecision],
                                         [FindingSummary],[FindingSummaryDecision],[IsGrievance],[Isselfservice])
                                 VALUES (@ComplaintGUID,@OrgGuid,@EmployeeGUID,@ReportedBy,@ReporterEmail,@CompliantCode,@ReporterTel,@MeansOfReport,
                                         @ComplaintDescription,@ComplaintRemark,@DateReported,@DateScreened,@ScreenedBy,@ScreeningDecision,@FindingSummary,
                                         @FindingSummaryDecision,@IsGrievance,@Isselfservice)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ComplaintGUID", objComplaint.ComplaintGUID));
                command.Parameters.Add(new SqlParameter("@OrgGuid", objComplaint.OrgGuid));
                command.Parameters.Add(new SqlParameter("@EmployeeGUID", objComplaint.EmployeeGUID));
                command.Parameters.Add(new SqlParameter("@ReportedBy", objComplaint.ReportedBy));
                command.Parameters.Add(new SqlParameter("@ReporterEmail", objComplaint.ReporterEmail));
                command.Parameters.Add(new SqlParameter("@ReporterTel", objComplaint.ReporterTel));
                command.Parameters.Add(new SqlParameter("@MeansOfReport", objComplaint.MeansOfReport));
                command.Parameters.Add(new SqlParameter("@ComplaintDescription", objComplaint.ComplaintDescription));
                command.Parameters.Add(new SqlParameter("@ComplaintRemark", objComplaint.ComplaintRemark));
                command.Parameters.Add(new SqlParameter("@DateReported", objComplaint.DateReported));
                command.Parameters.Add(new SqlParameter("@DateScreened", objComplaint.DateScreened));
                command.Parameters.Add(new SqlParameter("@ScreenedBy", objComplaint.ScreenedBy));
                command.Parameters.Add(new SqlParameter("@ScreeningDecision", objComplaint.ScreeningDecision));
                command.Parameters.Add(new SqlParameter("@FindingSummary", objComplaint.FindingSummary));
                command.Parameters.Add(new SqlParameter("@FindingSummaryDecision", objComplaint.FindingSummaryDecision));
                command.Parameters.Add(new SqlParameter("@IsGrievance", objComplaint.IsGrievance));
                command.Parameters.Add(new SqlParameter("@Isselfservice", objComplaint.Isselfservice));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Complaint::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public DataTable GetRecordDt(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            Complaint objComplaint = new Complaint();
            string strGetRecord = @"SELECT [Referenceno],[ComplaintGUID],[OrgGuid],[EmployeeGUID],[ReportedBy],[ReporterEmail],[ReporterTel],[MeansOfReport],[ComplaintDescription],[ComplaintRemark],[DateReported],[DateScreened],[ScreenedBy],[ScreeningDecision],[FindingSummary],[FindingSummaryDecision],[IsGrievance],[Isselfservice] FROM [dbo].[tblComplaint] WHERE [ComplaintGUID]=@ComplaintGUID";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Complaint");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@ComplaintGUID", ID));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Complaint::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }
        public bool MakeCompliantDecision(Complaint objComplaint)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[tblComplaint] SET [DateScreened]=@DateScreened,[ScreenedBy]=@ScreenedBy, [FindingSummary]=@FindingSummary,  [FindingSummaryDecision]=@FindingSummaryDecision WHERE [ComplaintGUID]=@ComplaintGUID";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@ComplaintGUID", objComplaint.ComplaintGUID));
                command.Parameters.Add(new SqlParameter("@DateScreened", objComplaint.DateScreened));
                command.Parameters.Add(new SqlParameter("@ScreenedBy", objComplaint.ScreenedBy));
                command.Parameters.Add(new SqlParameter("@FindingSummary", objComplaint.FindingSummary));
                command.Parameters.Add(new SqlParameter("@FindingSummaryDecision", objComplaint.FindingSummaryDecision));

                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Complaint::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool UpdateCompliant(Complaint objComplaint)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [tblComplaint] SET [OrgGuid]=@OrgGuid,[EmployeeGUID]=@EmployeeGUID,[ReportedBy]=@ReportedBy,[ReporterEmail]=@ReporterEmail,[ReporterTel]=@ReporterTel,[MeansOfReport]=@MeansOfReport,[ComplaintRemark]=@ComplaintRemark,[ComplaintDescription]=@ComplaintDescription,[DateReported]=@DateReported,[ScreeningDecision]=@ScreeningDecision WHERE [ComplaintGUID]=@ComplaintGUID";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@ComplaintGUID", objComplaint.ComplaintGUID));
                command.Parameters.Add(new SqlParameter("@OrgGuid", objComplaint.OrgGuid));
                command.Parameters.Add(new SqlParameter("@EmployeeGUID", objComplaint.EmployeeGUID));
                command.Parameters.Add(new SqlParameter("@ReportedBy", objComplaint.ReportedBy));
                command.Parameters.Add(new SqlParameter("@ReporterEmail", objComplaint.ReporterEmail));
                command.Parameters.Add(new SqlParameter("@ReporterTel", objComplaint.ReporterTel));
                command.Parameters.Add(new SqlParameter("@MeansOfReport", objComplaint.MeansOfReport));
                command.Parameters.Add(new SqlParameter("@ComplaintDescription", objComplaint.ComplaintDescription));
                command.Parameters.Add(new SqlParameter("@ComplaintRemark", objComplaint.ComplaintRemark));
                command.Parameters.Add(new SqlParameter("@DateReported", objComplaint.DateReported));
                command.Parameters.Add(new SqlParameter("@ScreeningDecision", objComplaint.ScreeningDecision));
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Complaint::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblComplaint] WHERE [ComplaintGUID]=@ComplaintGUID";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ComplaintGUID", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Complaint::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public DataTable GetComplianRecord(Guid ComptGuid, int language)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string columnDescription = Language.GetVisibleDescriptionColumnOther(language);
            string strGetAllRecords = @"SELECT [Referenceno],[ComplaintGUID],(select {0} from Organization where tblComplaint.[OrgGuid] =[OrgGuid]) as OrgDescription,[EmployeeGUID],[ReportedBy],[ReporterEmail], IsGrievance as HRACType,
                                      [ReporterTel], [MeansOfReport] as MeansOfReport ,[ComplaintDescription],[ComplaintRemark],[DateReported],[ScreeningDecision] as [ScreeningDecision], [FindingSummary] as FindingSummary ,[FindingSummaryDecision],ScreenedBy,DateScreened
                                      FROM [dbo].[tblComplaint] Where ComplaintGUID=@ComptGuid  ORDER BY  [ComplaintGUID] ASC";
            strGetAllRecords = string.Format(strGetAllRecords, columnDescription);
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Complaint");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@ComptGuid", ComptGuid));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Complaint::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }




        public DataTable GetRecordreference(Guid person, string FileType)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [Referenceno],[ComplaintGUID],(select DescriptionAm from Organization where tblComplaint.[OrgGuid] =[OrgGuid]) as OrgDescription,[EmployeeGUID],[ReportedBy],[ReporterEmail],case IsGrievance when 1 then N'ቅሬታ የቀረበበት መንገድ' when 0 then N'ጥቆማ የቀረበበት መንገድ' End as IsGrievance,case IsGrievance when 1 then N'ቅሬታ አይነት' when 0 then N'ጥቆማ አይነት' End as HRACType,
                                      [ReporterTel],case [MeansOfReport] when 0 then N'በአካል' when 1 then N'በድረ ገፅ' when 2 then N'በኢሜል' when 3  then N'በስልክ' when 4 then N'በፖስታ ' End as MeansOfReport ,[ComplaintDescription],[ComplaintRemark],[DateReported],[dbo].[fnGetLookupType] (ScreeningDecision) as [ScreeningDecision] 
                                      FROM [dbo].[tblComplaint] Where ComplaintGUID=@ParentGuid   ORDER BY  [ComplaintGUID] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Complaint");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ParentGuid", person));
                command.Parameters.Add(new SqlParameter("@FileType", FileType));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Complaint::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }
        //public DataTable GetRecord(Guid ID)
        //{
        //    SqlConnection connection = new SqlConnection(ConnectionString);

        //    Complaint objComplaint = new Complaint();
        //    string strGetRecord = @"SELECT [Referenceno],[ComplaintGUID],[OrgGuid],[EmployeeGUID],[ReportedBy],[ReporterEmail],[ReporterTel],[MeansOfReport],[ComplaintDescription],[ComplaintRemark],[DateReported],[DateScreened],[ScreenedBy],[ScreeningDecision],[FindingSummary],[FindingSummaryDecision],[IsGrievance],[Isselfservice] FROM [dbo].[tblComplaint] WHERE [ComplaintGUID]=@ComplaintGUID";

        //    SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
        //    DataTable dTable = new DataTable("Complaint");
        //    SqlDataAdapter adapter = new SqlDataAdapter(command);
        //    command.Connection = connection;
        //    try
        //    {
        //        command.Parameters.Add(new SqlParameter("@ComplaintGUID", ID));
        //        connection.Open();
        //        adapter.Fill(dTable);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Complaint::GetRecord::Error!" + ex.Message, ex);
        //    }
        //    finally
        //    {
        //        connection.Close();
        //        command.Dispose();
        //        adapter.Dispose();
        //    }
        //    return dTable;
        //}
        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [ComplaintGUID],[OrgGuid],[ReportedBy],[FindingSummaryDecision],[FindingSummaryDecision],[ReporterEmail],[Referenceno],[ReporterTel],[MeansOfReport],[ComplaintDescription],[ComplaintRemark],[DateReported],[DateScreened],[ScreenedBy],[ScreeningDecision],[FindingSummary] FROM [dbo].[tblComplaint]  ORDER BY  [ComplaintGUID] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Complaint");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Complaint::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }
        public Complaint GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

           Complaint objtblComplaint = new Complaint();
            string strGetRecord = @"SELECT Referenceno,[ComplaintGUID],[OrgGuid],[ReportedBy],[ReporterEmail],[ReporterTel],[MeansOfReport],[ComplaintRemark],[ComplaintDescription],[DateReported],[DateScreened],[ScreenedBy],[ScreeningDecision],[FindingSummary],[FindingSummaryDecision],[IsGrievance],[Referenceno],[EmployeeGUID],[Isselfservice] FROM [dbo].[tblComplaint] WHERE [ComplaintGUID]=@ComplaintGUID";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblComplaint");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ComplaintGUID", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblComplaint.ComplaintGUID = (Guid)dTable.Rows[0]["ComplaintGUID"];
                    objtblComplaint.OrgGuid = (Guid)dTable.Rows[0]["OrgGuid"];
                    if (dTable.Rows[0]["Referenceno"].Equals(DBNull.Value))
                        objtblComplaint.Referenceno = string.Empty;
                    else
                        objtblComplaint.Referenceno = (string)dTable.Rows[0]["Referenceno"];
                    if (dTable.Rows[0]["ReportedBy"].Equals(DBNull.Value))
                        objtblComplaint.ReportedBy = string.Empty;
                    else
                        objtblComplaint.ReportedBy = (string)dTable.Rows[0]["ReportedBy"];
                    if (dTable.Rows[0]["ReporterEmail"].Equals(DBNull.Value))
                        objtblComplaint.ReporterEmail = string.Empty;
                    else
                        objtblComplaint.ReporterEmail = (string)dTable.Rows[0]["ReporterEmail"];
                    if (dTable.Rows[0]["ReporterTel"].Equals(DBNull.Value))
                        objtblComplaint.ReporterTel = string.Empty;
                    else
                        objtblComplaint.ReporterTel = (string)dTable.Rows[0]["ReporterTel"];
                    if (dTable.Rows[0]["MeansOfReport"].Equals(DBNull.Value))
                        objtblComplaint.MeansOfReport = 0;
                    else
                        objtblComplaint.MeansOfReport = (int)dTable.Rows[0]["MeansOfReport"];
                    if (dTable.Rows[0]["ComplaintRemark"].Equals(DBNull.Value))
                        objtblComplaint.ComplaintRemark = string.Empty;
                    else
                        objtblComplaint.ComplaintRemark = (string)dTable.Rows[0]["ComplaintRemark"];
                    if (dTable.Rows[0]["ComplaintDescription"].Equals(DBNull.Value))
                        objtblComplaint.ComplaintDescription = string.Empty;
                    else
                        objtblComplaint.ComplaintDescription = (string)dTable.Rows[0]["ComplaintDescription"];
                    if (dTable.Rows[0]["DateReported"].Equals(DBNull.Value))
                        objtblComplaint.DateReported = DateTime.MinValue;
                    else
                        objtblComplaint.DateReported = (DateTime)dTable.Rows[0]["DateReported"];
                    if (dTable.Rows[0]["DateScreened"].Equals(DBNull.Value))
                        objtblComplaint.DateScreened = DateTime.MinValue;
                    else
                        objtblComplaint.DateScreened = (DateTime)dTable.Rows[0]["DateScreened"];
                    if (dTable.Rows[0]["ScreenedBy"].Equals(DBNull.Value))
                        objtblComplaint.ScreenedBy = string.Empty;
                    else
                        objtblComplaint.ScreenedBy = (string)dTable.Rows[0]["ScreenedBy"];
                    if (dTable.Rows[0]["ScreeningDecision"].Equals(DBNull.Value))
                        objtblComplaint.ScreeningDecision = 0;
                    else
                        objtblComplaint.ScreeningDecision = (int)dTable.Rows[0]["ScreeningDecision"];
                    if (dTable.Rows[0]["FindingSummary"].Equals(DBNull.Value))
                        objtblComplaint.FindingSummary = 0;
                    else
                        objtblComplaint.FindingSummary = (int)dTable.Rows[0]["FindingSummary"];
                    objtblComplaint.FindingSummaryDecision = (string)dTable.Rows[0]["FindingSummaryDecision"];
                    objtblComplaint.IsGrievance = (bool)dTable.Rows[0]["IsGrievance"];
                    if (dTable.Rows[0]["Referenceno"].Equals(DBNull.Value))
                        objtblComplaint.Referenceno = string.Empty;
                    else
                        objtblComplaint.Referenceno = (string)dTable.Rows[0]["Referenceno"];
                    if (dTable.Rows[0]["EmployeeGUID"].Equals(DBNull.Value))
                        objtblComplaint.EmployeeGUID = Guid.Empty;
                    else
                        objtblComplaint.EmployeeGUID = (Guid)dTable.Rows[0]["EmployeeGUID"];
                    if (dTable.Rows[0]["Isselfservice"].Equals(DBNull.Value))
                        objtblComplaint.Isselfservice = false;
                    else
                        objtblComplaint.Isselfservice = (bool)dTable.Rows[0]["Isselfservice"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblComplaint::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblComplaint;
        }

        //public Complaint GetRecord(string ID)
        //{
        //    SqlConnection connection = new SqlConnection(ConnectionString);

        //    Complaint objComplaint = new Complaint();
        //    string strGetRecord = @"SELECT [Referenceno],[ComplaintGUID],[OrgGuid],[EmployeeGUID],[ReportedBy],[ReporterEmail],[ReporterTel],[MeansOfReport],[ComplaintDescription],[ComplaintRemark],[DateReported],[DateScreened],[ScreenedBy],[ScreeningDecision],[FindingSummary],[FindingSummaryDecision],[IsGrievance],[Isselfservice] FROM [dbo].[tblComplaint] WHERE [ComplaintGUID]=@ComplaintGUID";

        //    SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
        //    DataTable dTable = new DataTable("Complaint");
        //    SqlDataAdapter adapter = new SqlDataAdapter(command);
        //    command.Connection = connection;
        //    try
        //    {
        //        command.Parameters.Add(new SqlParameter("@ComplaintGUID", ID));
        //        connection.Open();
        //        adapter.Fill(dTable);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Complaint::GetRecord::Error!" + ex.Message, ex);
        //    }
        //    finally
        //    {
        //        connection.Close();
        //        command.Dispose();
        //        adapter.Dispose();
        //    }
        //    return objComplaint;
        //}
        public DataTable GetRecordEmployee(Guid ID,bool isreg, int language)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            //string complainType = "";
            Complaint objComplaint = new Complaint();
            string strGetRecord = @"SELECT [Referenceno],[ComplaintGUID],[OrgGuid],[ReportedBy],[ReporterEmail],[ReporterTel],[MeansOfReport],[ComplaintDescription],[ComplaintRemark],[DateReported] as ReportedDate,[DateScreened],[ScreenedBy],[dbo].[fnGetLookupType] (tblComplaint.ScreeningDecision) AS ComplainType,case [FindingSummary] when 0 then N'ያልታየ' when 1 then N'ተቀባይነት ያገኘ' when 2 then N'ያልተሟላ' when 3 then N'ውድቅ የሆነ' End as [FindingSummary] ,[FindingSummaryDecision],[IsGrievance],[Isselfservice] 
                                    FROM [dbo].[tblComplaint] WHERE [EmployeeGUID]=@EmployeeGUID and [IsGrievance]=@isreg and FindingSummary=0";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Complaint");
            System.Type typeString = System.Type.GetType("System.String");

            dTable.Columns.Add("ReportedDate", typeString);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@EmployeeGUID", ID));
                command.Parameters.Add(new SqlParameter("@isreg", isreg));
                
                connection.Open();
                adapter.Fill(dTable);
                #region Translated Complain Types
                for (int index = 0; index < dTable.Rows.Count; index++)
                {
                    //To display complain type in a meaningfull way
                    string ethiodate = Convert.ToString(dTable.Rows[index]["ReportedDate"].ToString());
                    ethiodate = EthiopicDateTime.GetEthiopicDate((Convert.ToDateTime(ethiodate.ToString())).Day, Convert.ToDateTime(ethiodate.ToString()).Month, (Convert.ToDateTime(ethiodate.ToString())).Year);
                    
                    string tEthiodate = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate,language);
                    dTable.Rows[index]["ReportedDate"] = tEthiodate;


                }
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception("Complaint::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public DataTable GetRecordComplianReg(bool isreg,int language)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            //string complainType = "";
            Complaint objComplaint = new Complaint();
            string strGetRecord = @"SELECT [Referenceno],[ComplaintGUID],[OrgGuid],[ReportedBy],[ReporterEmail],[ReporterTel],[MeansOfReport],[ComplaintDescription],[ComplaintRemark],[DateReported] as ReportedDate,[DateScreened],[ScreenedBy],tblComplaint.ScreeningDecision AS ComplainType,[FindingSummary] as [FindingSummary],[FindingSummaryDecision],[IsGrievance], [Isselfservice]  as [Isselfservice]  FROM [dbo].[tblComplaint] WHERE [IsGrievance]=@isreg and [FindingSummary]=0";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Complaint");
            System.Type typeString = System.Type.GetType("System.String");

            dTable.Columns.Add("ReportedDate", typeString);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@isreg", isreg));

                connection.Open();
                adapter.Fill(dTable);
                #region Translated Complain Types
                for (int index = 0; index < dTable.Rows.Count; index++)
                {
                    //To display complain type in a meaningfull way
                    string ethiodate = Convert.ToString(dTable.Rows[index]["ReportedDate"].ToString());
                    ethiodate = EthiopicDateTime.GetEthiopicDate((Convert.ToDateTime(ethiodate.ToString())).Day, Convert.ToDateTime(ethiodate.ToString()).Month, (Convert.ToDateTime(ethiodate.ToString())).Year);
                    string tEthiodate = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate,language);
                    dTable.Rows[index]["ReportedDate"] = tEthiodate;
                }
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception("Complaint::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public DataTable GetRecordEmployeeAll(bool isreg)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            //string complainType = "";
            Complaint objComplaint = new Complaint();
            string strGetRecord = @"SELECT [Referenceno],[ComplaintGUID],[OrgGuid],[ReportedBy],[ReporterEmail],[ReporterTel],[MeansOfReport],[ComplaintDescription],[ComplaintRemark],[DateReported] as ReportedDate,[DateScreened],[ScreenedBy],[dbo].[fnGetLookupType] (tblComplaint.ScreeningDecision) AS ComplainType,case [FindingSummary] when 0 then  N'ያልታየ' when 1 then '' when 2 then '' when 3 then '' end as [FindingSummary]  ,[FindingSummaryDecision],[IsGrievance],[Isselfservice] FROM [dbo].[tblComplaint] WHERE [IsGrievance]=@isreg";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Complaint");
            System.Type typeString = System.Type.GetType("System.String");

            dTable.Columns.Add("ReportedDate", typeString);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@isreg", isreg));

                connection.Open();
                adapter.Fill(dTable);
                #region Translated Complain Types
                for (int index = 0; index < dTable.Rows.Count; index++)
                {
                    //To display complain type in a meaningfull way
                    string ethiodate = Convert.ToString(dTable.Rows[index]["ReportedDate"].ToString());
                    ethiodate = EthiopicDateTime.GetEthiopicDate((Convert.ToDateTime(ethiodate.ToString())).Day, Convert.ToDateTime(ethiodate.ToString()).Month, (Convert.ToDateTime(ethiodate.ToString())).Year);
                    string tEthiodate = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate);
                    dTable.Rows[index]["ReportedDate"] = tEthiodate;


                }
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception("Complaint::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public DataTable Search(Guid selectedOrgGuid, int selectedcomplainType, DateTime dateFrom, DateTime dateTo,  bool isGrievance, int language)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            Complaint objComplaint = new Complaint();
            DataTable dtComplain = new DataTable("TEMP_Complaint");
            System.Type typeString = System.Type.GetType("System.String");
            System.Type typeGuid = System.Type.GetType("System.Guid");
            System.Type typeInt = System.Type.GetType("System.Int");
            dtComplain.Columns.Add("ComplaintGuid", typeGuid);
            dtComplain.Columns.Add("ReportedBy", typeString);
            dtComplain.Columns.Add("ComplainType", typeString);
            dtComplain.Columns.Add("Organization", typeString);
            dtComplain.Columns.Add("ReportedDate", typeString);
            dtComplain.Columns.Add("FindingSummary", typeString);
            string requiredFields = "tblComplaint.ComplaintGUID AS ComplaintGuid, " +
                                    "tblComplaint.ReportedBy AS ReportedBy, " +
                                    "tblComplaint.DateReported AS ReportedDate, " +
                                    "tblComplaint.ScreeningDecision AS ComplainType, " +
                                    "Organization.DescriptionAm AS Organization," +
                                    "tblComplaint.FindingSummary AS FindingSummary";
            string joinStatement = "tblComplaint INNER JOIN Organization ON tblComplaint.OrgGuid = Organization.OrgGuid";
            string filteringCriterias = "tblComplaint.IsGrievance='" + isGrievance.ToString() + "'";
            //if (selecteddecisionType != -1)
            //{
            //    filteringCriterias += "And tblComplaint.FindingSummary='" + selecteddecisionType.ToString() + "'";
            //}
            if (selectedOrgGuid != Guid.Empty)
            {
                filteringCriterias += " AND Organization.OrgGuid='" + selectedOrgGuid.ToString() + "'";
            }
            if (selectedcomplainType != -1)
            {
                filteringCriterias += " AND tblComplaint.ScreeningDecision='" + selectedcomplainType.ToString() + "'";
            }
            DateTime minDate = new DateTime();
            if (dateFrom != minDate)
            {
                filteringCriterias += " AND tblComplaint.DateReported between @dateFrom and @dateTo";
            //}
            //if (dateTo != minDate)
            //{
                //filteringCriterias += " AND tblComplaint.DateReported<=@dateTo";
            }
            string complainType = "";
            string findingSummary = "";
            string strGetRecord = @"SELECT " + requiredFields + " FROM " + joinStatement + (!filteringCriterias.Equals("") ? " WHERE " + filteringCriterias : "") + " ORDER BY  DateReported Desc";
            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@dateFrom", dateFrom));
                command.Parameters.Add(new SqlParameter("@dateTo", dateTo));
                adapter.Fill(dtComplain);
                for (int index = 0; index < dtComplain.Rows.Count; index++)
                {
                    //To display date with the following format "month day, year"
                    //dtComplain.Rows[index]["ReportedDate"] = EthiopicDateTime.TranslateDateMonth(Convert.ToDateTime(dtComplain.Rows[index]["ReportedDate"]));
                    string ethiodate = Convert.ToString(dtComplain.Rows[index]["ReportedDate"].ToString());
                    ethiodate = EthiopicDateTime.GetEthiopicDate((Convert.ToDateTime(ethiodate.ToString())).Day, Convert.ToDateTime(ethiodate.ToString()).Month, (Convert.ToDateTime(ethiodate.ToString())).Year);
                    string tEthiodate = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate, language);
                    dtComplain.Rows[index]["ReportedDate"] = tEthiodate;
                    #region Translated Complain Types

                    //To display complain type in a meaningfull way
                    complainType = dtComplain.Rows[index]["ComplainType"].ToString();
                    switch (complainType)
                    {
                        case "311"://General 
                            if (language == 1)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.ComplainTypes), Language.eLanguage.eAmharic, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            if (language == 2)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.ComplainTypes), Language.eLanguage.eAfanOromo, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            break;
                        case "312"://Recuritment 
                            if (language == 1)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.ComplainTypes), Language.eLanguage.eAmharic, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            if (language == 2)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.ComplainTypes), Language.eLanguage.eAfanOromo, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            break;
                        case "313"://Promotion    
                            if (language == 1)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.ComplainTypes), Language.eLanguage.eAmharic, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            if (language == 2)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.ComplainTypes), Language.eLanguage.eAfanOromo, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            break;
                        case "314"://Transfer
                            if (language == 1)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.ComplainTypes), Language.eLanguage.eAmharic, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            if (language == 2)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.ComplainTypes), Language.eLanguage.eAfanOromo, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            break;
                        case "315"://Service Extension
                            if (language == 1)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.ComplainTypes), Language.eLanguage.eAmharic, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            if (language == 2)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.ComplainTypes), Language.eLanguage.eAfanOromo, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            break;
                        case "316"://Service Termination
                            if (language == 1)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.ComplainTypes), Language.eLanguage.eAmharic, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            if (language == 2)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.ComplainTypes), Language.eLanguage.eAfanOromo, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            break;
                        case "317"://Displanary and grevance handling 
                            if (language == 1)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.ComplainTypes), Language.eLanguage.eAmharic, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            if (language == 2)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.ComplainTypes), Language.eLanguage.eAfanOromo, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            break;
                        case "318"://Health and Safety
                            if (language == 1)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.ComplainTypes), Language.eLanguage.eAmharic, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            if (language == 2)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionType), Language.eLanguage.eAfanOromo, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            break;
                        case "319"://Overtime
                            if (language == 1)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.ComplainTypes), Language.eLanguage.eAmharic, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            if (language == 2)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.ComplainTypes), Language.eLanguage.eAfanOromo, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            break;
                        case "320"://Scholarship
                            if (language == 1)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.ComplainTypes), Language.eLanguage.eAmharic, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            if (language == 2)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.ComplainTypes), Language.eLanguage.eAfanOromo, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            break;
                        case "321"://Delegation
                            if (language == 1)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.ComplainTypes), Language.eLanguage.eAmharic, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            if (language == 2)
                            {
                                dtComplain.Rows[index]["ComplainType"] = EnumHelper.GetEnumDescription(typeof(Enumeration.ComplainTypes), Language.eLanguage.eAfanOromo, Convert.ToInt32(dtComplain.Rows[index]["ComplainType"].ToString()));
                            }
                            break;
                    }
                    #endregion
                    #region Translate FindingSummary

                    //To display complain type in a meaningfull way
                    findingSummary = dtComplain.Rows[index]["FindingSummary"].ToString();
                    switch (findingSummary)
                    {
                        case "0"://Not inspected
                                if (language == 1)
                                {
                                    dtComplain.Rows[index]["FindingSummary"] = EnumHelper.GetEnumDescription(typeof(Enumeration.FindingSummary), Language.eLanguage.eAmharic, Convert.ToInt32(dtComplain.Rows[index]["FindingSummary"].ToString()));
                                }
                                if (language == 2)
                                {
                                    dtComplain.Rows[index]["FindingSummary"] = EnumHelper.GetEnumDescription(typeof(Enumeration.FindingSummary), Language.eLanguage.eAfanOromo, Convert.ToInt32(dtComplain.Rows[index]["FindingSummary"].ToString()));
                                }
                           
                            break;
                        case "1"://Accepted
                            if (language == 1)
                            {
                                dtComplain.Rows[index]["FindingSummary"] = EnumHelper.GetEnumDescription(typeof(Enumeration.FindingSummary), Language.eLanguage.eAmharic, Convert.ToInt32(dtComplain.Rows[index]["FindingSummary"].ToString()));
                            }
                            if (language == 2)
                            {
                                dtComplain.Rows[index]["FindingSummary"] = EnumHelper.GetEnumDescription(typeof(Enumeration.FindingSummary), Language.eLanguage.eAfanOromo, Convert.ToInt32(dtComplain.Rows[index]["FindingSummary"].ToString()));
                            }
                            break;
                        case "2"://Need further document
                            if (language == 1)
                            {
                                dtComplain.Rows[index]["FindingSummary"] = EnumHelper.GetEnumDescription(typeof(Enumeration.FindingSummary), Language.eLanguage.eAmharic, Convert.ToInt32(dtComplain.Rows[index]["FindingSummary"].ToString()));
                            }
                            if (language == 2)
                            {
                                dtComplain.Rows[index]["FindingSummary"] = EnumHelper.GetEnumDescription(typeof(Enumeration.FindingSummary), Language.eLanguage.eAfanOromo, Convert.ToInt32(dtComplain.Rows[index]["FindingSummary"].ToString()));
                            }
                            break;
                        case "3"://Not Accpeted
                            if (language == 1)
                            {
                                dtComplain.Rows[index]["FindingSummary"] = EnumHelper.GetEnumDescription(typeof(Enumeration.FindingSummary), Language.eLanguage.eAmharic, Convert.ToInt32(dtComplain.Rows[index]["FindingSummary"].ToString()));
                            }
                            if (language == 2)
                            {
                                dtComplain.Rows[index]["FindingSummary"] = EnumHelper.GetEnumDescription(typeof(Enumeration.FindingSummary), Language.eLanguage.eAfanOromo, Convert.ToInt32(dtComplain.Rows[index]["FindingSummary"].ToString()));
                            }
                            break;
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Complaint::Search::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }

            return dtComplain;
        }
       
        public DataTable RegistrationSearch(Guid selectedOrgGuid, int selectedcomplainType, int selecteddecisionType, bool isGrievance)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            Complaint objComplaint = new Complaint();
            DataTable dtComplain = new DataTable("TEMP_Complaint");
            System.Type typeString = System.Type.GetType("System.String");
            System.Type typeGuid = System.Type.GetType("System.Guid");
            System.Type typeInt = System.Type.GetType("System.Int");
            dtComplain.Columns.Add("ComplaintGuid", typeGuid);
            dtComplain.Columns.Add("ReportedBy", typeString);
            dtComplain.Columns.Add("ComplainType", typeString);
            dtComplain.Columns.Add("Organization", typeString);
            dtComplain.Columns.Add("ReportedDate", typeString);
            dtComplain.Columns.Add("FindingSummary", typeString);
            string requiredFields = "Referenceno ,tblComplaint.ComplaintGUID AS ComplaintGuid, " +
                                    "tblComplaint.ReportedBy AS ReportedBy, " +
                                    "tblComplaint.DateReported AS ReportedDate, " +
                                    "tblComplaint.ScreeningDecision AS ComplainType, " +
                                    "Organization.DescriptionAm AS Organization," +
                                    "tblComplaint.FindingSummary AS FindingSummary";
            string joinStatement = "tblComplaint INNER JOIN Organization ON tblComplaint.OrgGuid = Organization.OrgGuid";
            string filteringCriterias = "tblComplaint.IsGrievance='" + isGrievance.ToString() + "'";
            if (selecteddecisionType != -1)
            {
                filteringCriterias += "And tblComplaint.FindingSummary='" + selecteddecisionType.ToString() + "'";
            }
            if (selectedOrgGuid != Guid.Empty)
            {
                filteringCriterias += " AND Organization.OrgGuid='" + selectedOrgGuid.ToString() + "'";
            }
            if (selectedcomplainType != -1)
            {
                filteringCriterias += " AND tblComplaint.ScreeningDecision='" + selectedcomplainType.ToString() + "'";
            }
            //DateTime minDate = new DateTime();
            //if (dateFrom != minDate)
            //{
            //    filteringCriterias += " AND tblComplaint.DateReported>='" + dateFrom + "'";
            //}
            //if (dateTo != minDate)
            //{
            //    filteringCriterias += " AND tblComplaint.DateReported<='" + dateTo + "'";
            //}
            string complainType = "";
            string findingSummary = "";
            string strGetRecord = @"SELECT " + requiredFields + " FROM " + joinStatement + (!filteringCriterias.Equals("") ? " WHERE " + filteringCriterias : "") + " ORDER BY  DateReported Desc";
            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                adapter.Fill(dtComplain);
                for (int index = 0; index < dtComplain.Rows.Count; index++)
                {
                    //To display date with the following format "month day, year"

                    string ethiodate = Convert.ToString(dtComplain.Rows[index]["ReportedDate"].ToString());
                    ethiodate = EthiopicDateTime.GetEthiopicDate((Convert.ToDateTime(ethiodate.ToString())).Day, Convert.ToDateTime(ethiodate.ToString()).Month, (Convert.ToDateTime(ethiodate.ToString())).Year);
                    string tEthiodate = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate);
                    dtComplain.Rows[index]["ReportedDate"] = tEthiodate;
                    #region Translated Complain Types

                    //To display complain type in a meaningfull way
                    complainType = dtComplain.Rows[index]["ComplainType"].ToString();
                    switch (complainType)
                    {
                        case "0"://General 
                            dtComplain.Rows[index]["ComplainType"] = "አጠቃላይ";
                            break;
                        case "311"://Recuritment 
                            dtComplain.Rows[index]["ComplainType"] = "ሰራተኛ ምልመላ";
                            break;
                        case "312"://Promotion    
                            dtComplain.Rows[index]["ComplainType"] = "ዕድገት";
                            break;
                        case "313"://Transfer
                            dtComplain.Rows[index]["ComplainType"] = "ዝውውር";
                            break;
                        case "314"://Service Extension
                            dtComplain.Rows[index]["ComplainType"] = "አገልግሎት ስለመቀጠል";
                            break;
                        case "315"://Service Termination
                            dtComplain.Rows[index]["ComplainType"] = "አገልግሎት ስለሟቋረጥ";
                            break;
                        case "316"://Displanary and grevance handling 
                            dtComplain.Rows[index]["ComplainType"] = "ስለደንብ መተላለፍ ውሳኔ እና ቅሬታ ሰሚ";
                            break;
                        case "317"://Health and Safety
                            dtComplain.Rows[index]["ComplainType"] = "ጤና እና ደህንነት";
                            break;
                        case "318"://Overtime
                            dtComplain.Rows[index]["ComplainType"] = "የትርፍ ስራ ሰዓት";
                            break;
                        case "319"://Scholarship
                            dtComplain.Rows[index]["ComplainType"] = "የትምህርት ዕድል";
                            break;
                        case "320"://Delegation
                            dtComplain.Rows[index]["ComplainType"] = "የስልጣን ውክልና";
                            break;
                    }
                    #endregion
                    #region Translate FindingSummary

                    //To display complain type in a meaningfull way
                    findingSummary = dtComplain.Rows[index]["FindingSummary"].ToString();
                    switch (findingSummary)
                    {
                        case "0"://Not inspected
                            if (isGrievance)
                                dtComplain.Rows[index]["FindingSummary"] = "ቅሬታው አልታየም";
                            else
                                dtComplain.Rows[index]["FindingSummary"] = "ጥቆማው አልታየም";
                            break;
                        case "1"://Accepted
                            dtComplain.Rows[index]["FindingSummary"] = "ተቀባይነት አግኝቷል";
                            break;
                        case "2"://Need further document
                            dtComplain.Rows[index]["FindingSummary"] = "አልተሟላም";
                            break;
                        case "3"://Not Accpeted
                            dtComplain.Rows[index]["FindingSummary"] = "ውድቅ ሆኗል";
                            break;
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Complaint::Search::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }

            return dtComplain;
        }

        public DataTable Report(Guid selectedOrgGuid, int selectedcomplainType, DateTime dateFrom, DateTime dateTo, int decisionType, bool isGrievance)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            Complaint objComplaint = new Complaint();
            DataTable dtComplain = new DataTable("TEMP_Complaint");
            System.Type typeString = System.Type.GetType("System.String");
            System.Type typeInt = System.Type.GetType("System.Int32");
            dtComplain.Columns.Add("No", typeInt);
            dtComplain.Columns["No"].AutoIncrementSeed = 1;
            dtComplain.Columns["No"].AutoIncrement = true;
            dtComplain.Columns.Add("ReportedBy", typeString);
            dtComplain.Columns.Add("ComplainType", typeString);
            dtComplain.Columns.Add("Organization", typeString);
            dtComplain.Columns.Add("ReportedDate", typeString);
            dtComplain.Columns.Add("ScreenedBy", typeString);
            string requiredFields = "tblComplaint.ReportedBy AS ReportedBy, " +
                                    "tblComplaint.DateReported AS ReportedDate, " +
                                    "tblComplaint.ScreeningDecision AS ComplainType, " +
                                    "Organization.DescriptionAm AS Organization," +
                                    "tblComplaint.ScreenedBy AS ScreenedBy";
            string joinStatement = "tblComplaint INNER JOIN Organization ON tblComplaint.OrgGuid = Organization.OrgGuid";
            string filteringCriterias = "tblComplaint.FindingSummary='" + decisionType.ToString() + "' AND tblComplaint.IsGrievance='" + isGrievance.ToString() + "'";
            if (selectedOrgGuid != Guid.Empty)
            {
                filteringCriterias += "AND Organization.OrgGuid='" + selectedOrgGuid.ToString() + "'";
            }
            if (selectedcomplainType != -1)
            {
                filteringCriterias += "AND tblComplaint.ScreeningDecision='" + selectedcomplainType.ToString() + "'";
            }
            DateTime minDate = new DateTime();
            if (dateFrom != minDate)
            {
                filteringCriterias += "AND tblComplaint.DateReported>='" + dateFrom + "'";
            }
            if (dateTo != minDate)
            {
                filteringCriterias += "AND tblComplaint.DateReported<='" + dateTo + "'";
            }
            string complainType = "";
            string strGetRecord = @"SELECT " + requiredFields + " FROM " + joinStatement + (!filteringCriterias.Equals("") ? " WHERE " + filteringCriterias : "") + " ORDER BY ComplaintGUID";
            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                adapter.Fill(dtComplain);
                for (int index = 0; index < dtComplain.Rows.Count; index++)
                {
                    //To display date with the following format "month day, year"
                    dtComplain.Rows[index]["ReportedDate"] = EthiopicDateTime.TranslateDateMonth(Convert.ToDateTime(dtComplain.Rows[index]["ReportedDate"]));
                    //To display complain type in a meaningfull way
                    complainType = dtComplain.Rows[index]["ComplainType"].ToString();
                    switch (complainType)
                    {
                        case "0"://General 
                            dtComplain.Rows[index]["ComplainType"] = "አጠቃላይ";
                            break;
                        case "1"://Recuritment 
                            dtComplain.Rows[index]["ComplainType"] = "ሰራተኛ ምልመላ";
                            break;
                        case "2"://Promotion    
                            dtComplain.Rows[index]["ComplainType"] = "ዕድገት";
                            break;
                        case "3"://Transfer
                            dtComplain.Rows[index]["ComplainType"] = "ዝውውር";
                            break;
                        case "4"://Service Extension
                            dtComplain.Rows[index]["ComplainType"] = "አገልግሎት ስለመቀጠል";
                            break;
                        case "5"://Service Termination
                            dtComplain.Rows[index]["ComplainType"] = "አገልግሎት ስለሟቋረጥ";
                            break;
                        case "6"://Displanary and grevance handling 
                            dtComplain.Rows[index]["ComplainType"] = "ስለደንብ መተላለፍ ውሳኔ እና ቅሬታ ሰሚ";
                            break;
                        case "7"://Health and Safety
                            dtComplain.Rows[index]["ComplainType"] = "ጤና እና ደህንነት";
                            break;
                        case "8"://Overtime
                            dtComplain.Rows[index]["ComplainType"] = "የትርፍ ስራ ሰዓት";
                            break;
                        case "9"://Scholarship
                            dtComplain.Rows[index]["ComplainType"] = "የትምህርት ዕድል";
                            break;
                        case "10"://Delegation
                            dtComplain.Rows[index]["ComplainType"] = "የስልጣን ውክልና";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Complaint::Search::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
            return dtComplain;
        }

        public DataTable StatisticalReport(Guid selectedOrgGuid, DateTime dateFrom, DateTime dateTo, bool isGrievance)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            Complaint objComplaint = new Complaint();
            DataTable dtStatistical = new DataTable("TEMP_StatisticalReport");
            System.Type typeInt = System.Type.GetType("System.Int32");
            dtStatistical.Columns.Add("No", typeInt);
            dtStatistical.Columns["No"].AutoIncrementSeed = 1;
            dtStatistical.Columns["No"].AutoIncrement = true;
            System.Type typeString = System.Type.GetType("System.String");
            dtStatistical.Columns.Add("Organization", typeString);
            dtStatistical.Columns.Add("Recuritment", typeInt);
            dtStatistical.Columns.Add("Promotion", typeInt);
            dtStatistical.Columns.Add("Transfer", typeInt);
            dtStatistical.Columns.Add("ServiceExtension", typeInt);
            dtStatistical.Columns.Add("ServiceTermination", typeInt);
            dtStatistical.Columns.Add("DisciplinaryMeasuresAndGrievanceHandling", typeInt);
            dtStatistical.Columns.Add("HealthAndSafety", typeInt);
            dtStatistical.Columns.Add("OvertimeWork", typeInt);
            dtStatistical.Columns.Add("ScholarshipTraining", typeInt);
            dtStatistical.Columns.Add("AuthorityDelegation", typeInt);
            dtStatistical.Columns.Add("Total", typeInt);

            string filteringCriterias = "tblComplaint.IsGrievance='" + isGrievance.ToString() + "'";
            DateTime minDate = new DateTime();
            if (selectedOrgGuid != Guid.Empty)
            {
                filteringCriterias = " tblComplaint.OrgGuid='" + selectedOrgGuid.ToString() + "'";
            }
            else
            {
                filteringCriterias = " tblComplaint.OrgGuid=Organization.OrgGuid ";
            }
            if (dateFrom != minDate)
            {
                filteringCriterias += " AND tblComplaint.DateReported>='" + dateFrom + "'";
            }
            if (dateTo != minDate)
            {
                filteringCriterias += " AND tblComplaint.DateReported<='" + dateTo + "'";
            }
            string sumOfRecuritment = "(select COUNT(tblComplaint.ComplaintGUID) FROM tblComplaint  WHERE tblComplaint.ScreeningDecision='" + Convert.ToInt32(Enumeration.ComplainTypes.Recruitment) + "' AND " + filteringCriterias + " ) AS 'Recuritment', ";
            string sumOfPromotion = "(select COUNT(tblComplaint.ComplaintGUID) FROM tblComplaint  WHERE  tblComplaint.ScreeningDecision='" + Convert.ToInt32(Enumeration.ComplainTypes.Promotion) + "' AND" + filteringCriterias + " ) AS 'Promotion', ";
            string sumOfTransfer = "(select COUNT(tblComplaint.ComplaintGUID) FROM tblComplaint  WHERE tblComplaint.ScreeningDecision='" + Convert.ToInt32(Enumeration.ComplainTypes.Transfer) + "' AND" + filteringCriterias + " ) AS 'Transfer', ";
            string sumOfServiceExtension = "(select COUNT(tblComplaint.ComplaintGUID) FROM tblComplaint  WHERE tblComplaint.ScreeningDecision='" + Convert.ToInt32(Enumeration.ComplainTypes.ServiceExtension) + "' AND  " + filteringCriterias + " ) AS 'ServiceExtension', ";
            string sumOfServiceTermination = "(select COUNT(tblComplaint.ComplaintGUID) FROM tblComplaint  WHERE tblComplaint.ScreeningDecision='" + Convert.ToInt32(Enumeration.ComplainTypes.ServiceTermination) + "' AND " + filteringCriterias + " ) AS 'ServiceTermination'  ,";
            string sumOfDisciplinaryMeasuresAndGrievanceHandling = "(select COUNT(tblComplaint.ComplaintGUID) FROM tblComplaint  WHERE tblComplaint.ScreeningDecision='" + Convert.ToInt32(Enumeration.ComplainTypes.DisciplinaryMeasuresAndGrievanceHandling) + "' AND " + filteringCriterias + ") AS 'DisciplinaryMeasuresAndGrievanceHandling',";
            string sumOfHealthAndSafety = "(select COUNT(tblComplaint.ComplaintGUID) FROM tblComplaint  WHERE tblComplaint.ScreeningDecision='" + Convert.ToInt32(Enumeration.ComplainTypes.HealthAndSafety) + "' AND" + filteringCriterias + ") AS 'HealthAndSafety', ";
            string sumOfOvertimeWork = "(select COUNT(tblComplaint.ComplaintGUID) FROM tblComplaint  WHERE tblComplaint.ScreeningDecision='" + Convert.ToInt32(Enumeration.ComplainTypes.OvertimeWork) + "' AND" + filteringCriterias + ") AS 'OvertimeWork', ";
            string sumOfScholarshipTraining = "(select COUNT(tblComplaint.ComplaintGUID) FROM tblComplaint  WHERE tblComplaint.ScreeningDecision='" + Convert.ToInt32(Enumeration.ComplainTypes.ScholarshipTraining) + "' AND" + filteringCriterias + ") AS 'ScholarshipTraining', ";
            string sumOfAuthorityDelegation = "(select COUNT(tblComplaint.ComplaintGUID) FROM tblComplaint  WHERE tblComplaint.ScreeningDecision='" + Convert.ToInt32(Enumeration.ComplainTypes.AuthorityDelegation) + "' AND" + filteringCriterias + ") AS 'AuthorityDelegation', ";
            string total = "(select COUNT(tblComplaint.ComplaintGUID) FROM tblComplaint  WHERE " + filteringCriterias + ") AS 'Total'";

            //string joinStatement = "tblComplaint INNER JOIN Organization ON tblComplaint.OrgGuid = Organization.OrgGuid";
            string filteringCriteria2 = "";
            //if (selectedOrgGuid != Guid.Empty)
            //{
            //     filteringCriteria2 = " WHERE tblComplaint.OrgGuid='" + selectedOrgGuid.ToString() + "'";
            //}
            string requiredFields = "Organization.DescriptionAm AS Organization ," + sumOfRecuritment + sumOfPromotion + sumOfTransfer + sumOfServiceExtension + sumOfServiceTermination +
                                    sumOfDisciplinaryMeasuresAndGrievanceHandling + sumOfHealthAndSafety + sumOfOvertimeWork + sumOfScholarshipTraining +
                                    sumOfAuthorityDelegation + total;
            string strGetRecord = @"SELECT Distinct " + requiredFields + " FROM tblComplaint INNER JOIN Organization ON tblComplaint.OrgGuid = Organization.OrgGuid ";
            //+ (!filteringCriteria2.Equals("") ? filteringCriteria2 : "");
            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                adapter.Fill(dtStatistical);
            }
            catch (Exception ex)
            {
                throw new Exception("Complaint::Search::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
            return dtStatistical;
        }

        public DataTable FindingSummaryStatisticalReport(Guid selectedOrgGuid, DateTime dateFrom, DateTime dateTo, bool isGrievance)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            Complaint objComplaint = new Complaint();
            DataTable dtStatistical = new DataTable("TEMP_FindingSummaryStatistical");
            System.Type typeInt = System.Type.GetType("System.Int32");
            System.Type typeString = System.Type.GetType("System.String");
            dtStatistical.Columns.Add("No", typeInt);
            dtStatistical.Columns["No"].AutoIncrementSeed = 1;
            dtStatistical.Columns["No"].AutoIncrement = true;
            dtStatistical.Columns.Add("Organization", typeString);
            dtStatistical.Columns.Add("Accepted", typeInt);
            dtStatistical.Columns.Add("Invalid", typeInt);
            dtStatistical.Columns.Add("Incomplete", typeInt);
            dtStatistical.Columns.Add("NotInspected", typeInt);
            dtStatistical.Columns.Add("Total", typeInt);

            string filteringCriterias = "tblComplaint.IsGrievance='" + isGrievance.ToString() + "'";
            DateTime minDate = new DateTime();
            if (selectedOrgGuid != Guid.Empty)
            {
                filteringCriterias += " AND tblComplaint.OrgGuid='" + selectedOrgGuid.ToString() + "'";
            }
            else
            {
                filteringCriterias += " AND tblComplaint.OrgGuid=Organization.OrgGuid ";
            }
            if (dateFrom != minDate)
            {
                filteringCriterias += "AND tblComplaint.DateReported>='" + dateFrom + "' ";
            }
            if (dateTo != minDate)
            {
                filteringCriterias += "AND tblComplaint.DateReported<='" + dateTo + "' ";
            }
            string sumOfAccepted = "(select COUNT(tblComplaint.FindingSummary) FROM tblComplaint WHERE " + filteringCriterias + " AND  tblComplaint.FindingSummary='" + Convert.ToInt32(Enumeration.FindingSummary.Accepted) + "' ) AS 'Accepted', ";
            string sumOfInvalid = "(select COUNT(tblComplaint.FindingSummary) FROM tblComplaint  WHERE " + filteringCriterias + " AND  tblComplaint.FindingSummary='" + Convert.ToInt32(Enumeration.FindingSummary.Invalid) + "' ) AS 'Invalid', ";
            string sumOfIncomplete = "(select COUNT(tblComplaint.FindingSummary) FROM tblComplaint  WHERE " + filteringCriterias + " AND  tblComplaint.FindingSummary='" + Convert.ToInt32(Enumeration.FindingSummary.Incomplete) + "' ) AS 'Incomplete', ";
            string sumOfNotInspected = "(select COUNT(tblComplaint.FindingSummary) FROM tblComplaint  WHERE " + filteringCriterias + " AND  tblComplaint.FindingSummary='" + Convert.ToInt32(Enumeration.FindingSummary.NotInspected) + "' ) AS 'NotInspected', ";
            string total = "(select COUNT(tblComplaint.ComplaintGUID) FROM tblComplaint  WHERE " + filteringCriterias + ") AS 'Total'";
            //string filteringCriteria2 = "";
            //if (selectedOrgGuid != Guid.Empty)
            //{
            //    filteringCriteria2 = " WHERE tblComplaint.OrgGuid='" + selectedOrgGuid.ToString() + "'";
            //}
            string requiredFields = "Organization.DescriptionAm AS Organization ," + sumOfAccepted + sumOfInvalid + sumOfIncomplete + sumOfNotInspected + total;

            string strGetRecord = @"SELECT Distinct " + requiredFields + " FROM tblComplaint  INNER JOIN Organization ON tblComplaint.OrgGuid = Organization.OrgGuid ";
            //+ (!filteringCriteria2.Equals("") ? filteringCriteria2 : "")
            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                adapter.Fill(dtStatistical);
            }
            catch (Exception ex)
            {
                throw new Exception("Complaint::Search::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
            return dtStatistical;

        }
       
        public bool Exists(string Referenceno)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Referenceno FROM [dbo].[tblComplaint] WHERE [Referenceno]=@Referenceno";

            SqlDataReader dr = null;
            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Referenceno", Referenceno));
                connection.Open();
                dr = command.ExecuteReader();

                if (dr.HasRows)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw new Exception("tblComplaint::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
    }
}