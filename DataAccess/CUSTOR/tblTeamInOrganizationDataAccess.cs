﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Security;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblTeamInOrganizationDataAccess
    {

        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public DataTable GetRecordsTeamOrg()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [MainGuid],[TeamGuid],[OrgGuid],[BudgetYear] FROM [dbo].[tblTeamInOrganization]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblTeamInOrganization");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblTeamInOrganization::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public DataTable GetRecords(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT     dbo.tblTeamInOrganization.MainGuid, dbo.tblTeamInOrganization.TeamGuid, dbo.tblTeamInOrganization.OrgGuid, dbo.tblTeamInOrganization.BudgetYear, dbo.Organization.DescriptionAm
                                       FROM       dbo.tblTeamInOrganization INNER JOIN dbo.Organization ON dbo.tblTeamInOrganization.OrgGuid = dbo.Organization.OrgGuid
                                       WHERE     (dbo.tblTeamInOrganization.TeamGuid = @TeamGuid)  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblTeamInOrganization");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@TeamGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblTeamInOrganization::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public tblTeamInOrganization GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblTeamInOrganization objtblTeamInOrganization = new tblTeamInOrganization();
            string strGetRecord = @"SELECT [MainGuid],[TeamGuid],[OrgGuid],[BudgetYear] FROM [dbo].[tblTeamInOrganization] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblTeamInOrganization");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblTeamInOrganization.MainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    if (dTable.Rows[0]["TeamGuid"].Equals(DBNull.Value))
                        objtblTeamInOrganization.TeamGuid = Guid.Empty;
                    else
                        objtblTeamInOrganization.TeamGuid = (Guid)dTable.Rows[0]["TeamGuid"];
                    if (dTable.Rows[0]["OrgGuid"].Equals(DBNull.Value))
                        objtblTeamInOrganization.OrgGuid = Guid.Empty;
                    else
                        objtblTeamInOrganization.OrgGuid = (Guid)dTable.Rows[0]["OrgGuid"];
                    if (dTable.Rows[0]["BudgetYear"].Equals(DBNull.Value))
                        objtblTeamInOrganization.BudgetYear = DateTime.MinValue;
                    else
                        objtblTeamInOrganization.BudgetYear = (DateTime)dTable.Rows[0]["BudgetYear"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblTeamInOrganization::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblTeamInOrganization;
        }

        public bool IsTeamBindToOrganization(Guid TeamGuid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            string strExists = @"SELECT MainGuid FROM [dbo].[tblTeamInOrganization] WHERE [TeamGuid]=@TeamGuid";
            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;
            SqlDataAdapter  adapter = new SqlDataAdapter(command);
            DataTable dtTeamOrganization = new DataTable();
            try
            {
                connection.Open();
                command.Parameters.Add(new SqlParameter("@TeamGuid", TeamGuid));
                adapter.Fill(dtTeamOrganization);
                return (dtTeamOrganization.Rows.Count > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("tblTeamInOrganization::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Insert(tblTeamInOrganization objtblTeamInOrganization)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[tblTeamInOrganization]
                                            ([TeamGuid],[OrgGuid])
                                     VALUES    (@TeamGuid,@OrgGuid)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                //command.Parameters.Add(new SqlParameter("@MainGuid", objtblTeamInOrganization.MainGuid));
                command.Parameters.Add(new SqlParameter("@TeamGuid", objtblTeamInOrganization.TeamGuid));
                command.Parameters.Add(new SqlParameter("@OrgGuid", objtblTeamInOrganization.OrgGuid));
                //command.Parameters.Add(new SqlParameter("@BudgetYear", objtblTeamInOrganization.BudgetYear));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblTeamInOrganization::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(tblTeamInOrganization objtblTeamInOrganization)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[tblTeamInOrganization] SET     [MainGuid]=@MainGuid,    [TeamGuid]=@TeamGuid,    [OrgGuid]=@OrgGuid,    [BudgetYear]=@BudgetYear WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", objtblTeamInOrganization.MainGuid));
                command.Parameters.Add(new SqlParameter("@TeamGuid", objtblTeamInOrganization.TeamGuid));
                command.Parameters.Add(new SqlParameter("@OrgGuid", objtblTeamInOrganization.OrgGuid));
                command.Parameters.Add(new SqlParameter("@BudgetYear", objtblTeamInOrganization.BudgetYear));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblTeamInOrganization::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool DeleteTeamOrgBuTeamAndOrgGuid(Guid ID, Guid OrgGuid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblTeamInOrganization] WHERE [TeamGuid]=@TeamGuid and [OrgGuid]=@OrgGuid";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@TeamGuid", ID));
                command.Parameters.Add(new SqlParameter("@OrgGuid", OrgGuid));

                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblTeamInOrganization::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool DeleteTeamOrg(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblTeamInOrganization] WHERE [TeamGuid]=@TeamGuid";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@TeamGuid", ID));

                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblTeamInOrganization::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblTeamInOrganization] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblTeamInOrganization::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        
        public List<tblTeamInOrganization> GetList()
        {
            List<tblTeamInOrganization> RecordsList = new List<tblTeamInOrganization>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [MainGuid],[TeamGuid],[OrgGuid],[BudgetYear] FROM [dbo].[tblTeamInOrganization]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblTeamInOrganization objtblTeamInOrganization = new tblTeamInOrganization();
                    if (dr["MainGuid"].Equals(DBNull.Value))
                        objtblTeamInOrganization.MainGuid = Guid.Empty;
                    else
                        objtblTeamInOrganization.MainGuid = (Guid)dr["MainGuid"];
                    if (dr["TeamGuid"].Equals(DBNull.Value))
                        objtblTeamInOrganization.TeamGuid = Guid.Empty;
                    else
                        objtblTeamInOrganization.TeamGuid = (Guid)dr["TeamGuid"];
                    if (dr["OrgGuid"].Equals(DBNull.Value))
                        objtblTeamInOrganization.OrgGuid = Guid.Empty;
                    else
                        objtblTeamInOrganization.OrgGuid = (Guid)dr["OrgGuid"];
                    if (dr["BudgetYear"].Equals(DBNull.Value))
                        objtblTeamInOrganization.BudgetYear = DateTime.MinValue;
                    else
                        objtblTeamInOrganization.BudgetYear = (DateTime)dr["BudgetYear"];
                    RecordsList.Add(objtblTeamInOrganization);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblTeamInOrganization::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public bool Exists(Guid OrgGuid, Guid TeamGuid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            string strExists =
                @"SELECT MainGuid FROM [dbo].[tblTeamInOrganization] WHERE [TeamGuid]=@TeamGuid and [OrgGuid]=@OrgGuid";
            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable dtTeamOrganization = new DataTable();
            try
            {
                connection.Open();
                command.Parameters.Add(new SqlParameter("@OrgGuid", OrgGuid));
                command.Parameters.Add(new SqlParameter("@TeamGuid", TeamGuid));
                adapter.Fill(dtTeamOrganization);
                return (dtTeamOrganization.Rows.Count > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("tblTeamInOrganization::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
    }
}