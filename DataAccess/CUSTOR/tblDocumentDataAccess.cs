﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblDocumentDataAccess
    {
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [MainGuid],[code],[Description],[DescriptionAm],[ParentGuid],[Isoptional] FROM [dbo].[tblDocument]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblDocument");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocument::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public DataTable GetRecordsByDocumentTypeGuid(Guid documentGuid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [MainGuid],[code],[Description],[DescriptionAm],[ParentGuid],[Isoptional] FROM [dbo].[tblDocument]  where ParentGuid='" + documentGuid + "' ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblDocument");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocument::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public DataTable SearchDocument(string documentDescription)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [tblDocument].[MainGuid],[tblDocument].[code],[tblDocument].[Description],[tblDocument].[DescriptionAm],tblDocumentType.DescriptionAm as HRActivity , [Isoptional] 
                                    FROM tblDocumentType inner join [dbo].[tblDocument]  on tblDocumentType.mainGuid =  tblDocument.ParentGuid where tblDocument.parentGuid='" + documentDescription + "'   ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblDocument");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocument::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public DataTable GetRecordCheckList(string parent, string HrActTypeDoc, int language)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            string strGetAllRecords = "[dbo].[GetDocumentDisplayValue]";
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.StoredProcedure };
            DataTable dTable = new DataTable("tblDocument");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;
           try
            {
                command.Parameters.Add("@parent", parent);
                command.Parameters.Add("@HrActTypeDoc", HrActTypeDoc);
                command.Parameters.Add("@language", language);
                
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocument::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public DataTable GetRecordCheckListNotOptional(string parent, string HrActTypeDoc, int language)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            string strGetAllRecords = "[dbo].[GetDocumentDisplayValueNotOptional]";
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.StoredProcedure };
            DataTable dTable = new DataTable("tblDocument");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;
            try
            {
                command.Parameters.Add("@parent", parent);
                command.Parameters.Add("@HrActTypeDoc", HrActTypeDoc);
                command.Parameters.Add("@language", language);

                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocument::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public DataTable GetRecordEditCheckList(string parent, string HrActTypeDoc, int language,Guid inspGuid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = "[dbo].[GetDocumentEditDisplayValue]";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.StoredProcedure };
            DataTable dTable = new DataTable("tblDocument");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;
           try
            {
                command.Parameters.Add("@parent", parent);
                command.Parameters.Add("@HrActTypeDoc", HrActTypeDoc);
                command.Parameters.Add("@language", language);
                command.Parameters.Add("@guid", inspGuid);
                
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocument::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public Guid GetRecordDocGuid(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            Guid DocGuid = Guid.Empty;
            tblDocument objtblDocument = new tblDocument();
            string strGetRecord = @"SELECT [MainGuid] FROM [dbo].[tblDocument] WHERE [code]=@code";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblDocument");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@code", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    DocGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblDocument::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return DocGuid;
        }

        public tblDocument GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblDocument objtblDocument = new tblDocument();
            string strGetRecord = @"SELECT [MainGuid],[code],[Description],[DescriptionAm],[ParentGuid],[Isoptional] FROM [dbo].[tblDocument] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblDocument");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblDocument.MainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    objtblDocument.Code = (string)dTable.Rows[0]["code"];
                    if (dTable.Rows[0]["Description"].Equals(DBNull.Value))
                        objtblDocument.Description = string.Empty;
                    else
                        objtblDocument.Description = (string)dTable.Rows[0]["Description"];
                    if (dTable.Rows[0]["DescriptionAm"].Equals(DBNull.Value))
                        objtblDocument.DescriptionAm = string.Empty;
                    else
                        objtblDocument.DescriptionAm = (string)dTable.Rows[0]["DescriptionAm"];
                    if (dTable.Rows[0]["ParentGuid"].Equals(DBNull.Value))
                        objtblDocument.ParentGuid = Guid.Empty;
                    else
                        objtblDocument.ParentGuid = (Guid)dTable.Rows[0]["ParentGuid"];
                    if (dTable.Rows[0]["Isoptional"].Equals(DBNull.Value))
                        objtblDocument.Isoptional = false;
                    else
                        objtblDocument.Isoptional = (bool)dTable.Rows[0]["Isoptional"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblDocument::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblDocument;
        }

        public tblDocument GetRecordDetail(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblDocument objtblDocument = new tblDocument();
            string strGetRecord = @"SELECT [MainGuid],[code],[Description],[DescriptionAm],[ParentGuid],[Isoptional] FROM [dbo].[tblDocument] WHERE [code]=@code";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblDocument");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@code", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblDocument.MainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    objtblDocument.Code = (string)dTable.Rows[0]["code"];
                    if (dTable.Rows[0]["Description"].Equals(DBNull.Value))
                        objtblDocument.Description = string.Empty;
                    else
                        objtblDocument.Description = (string)dTable.Rows[0]["Description"];
                    if (dTable.Rows[0]["DescriptionAm"].Equals(DBNull.Value))
                        objtblDocument.DescriptionAm = string.Empty;
                    else
                        objtblDocument.DescriptionAm = (string)dTable.Rows[0]["DescriptionAm"];
                    if (dTable.Rows[0]["ParentGuid"].Equals(DBNull.Value))
                        objtblDocument.ParentGuid = Guid.Empty;
                    else
                        objtblDocument.ParentGuid = (Guid)dTable.Rows[0]["ParentGuid"];
                    if (dTable.Rows[0]["Isoptional"].Equals(DBNull.Value))
                        objtblDocument.Isoptional = false;
                    else
                        objtblDocument.Isoptional = (bool)dTable.Rows[0]["Isoptional"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblDocument::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblDocument;
        }

        public bool Insert(tblDocument objtblDocument)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"Declare @DocumentCode  int;
                                 Set @DocumentCode = NEXT VALUE FOR DocumentCode;
                                 INSERT INTO [dbo].[tblDocument] ([code],[Description],[DescriptionAm],[ParentGuid],[Isoptional])
                                 VALUES (@DocumentCode,@Description,@DescriptionAm,@ParentGuid,@Isoptional)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Description", objtblDocument.Description));
                command.Parameters.Add(new SqlParameter("@DescriptionAm", objtblDocument.DescriptionAm));
                command.Parameters.Add(new SqlParameter("@ParentGuid", objtblDocument.ParentGuid));
                command.Parameters.Add(new SqlParameter("@Isoptional", objtblDocument.Isoptional));
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocument::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(tblDocument objtblDocument)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[tblDocument] SET     [MainGuid]=@MainGuid,    [code]=@code,    [Description]=@Description,    [DescriptionAm]=@DescriptionAm,    [ParentGuid]=@ParentGuid,    [Isoptional]=@Isoptional WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", objtblDocument.MainGuid));
                command.Parameters.Add(new SqlParameter("@code", objtblDocument.Code));
                command.Parameters.Add(new SqlParameter("@Description", objtblDocument.Description));
                command.Parameters.Add(new SqlParameter("@DescriptionAm", objtblDocument.DescriptionAm));
                command.Parameters.Add(new SqlParameter("@ParentGuid", objtblDocument.ParentGuid));
                command.Parameters.Add(new SqlParameter("@Isoptional", objtblDocument.Isoptional));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocument::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblDocument] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocument::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public DataTable GetList()
        {
            List<tblDocument> RecordsList = new List<tblDocument>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            string strGetAllRecords = @"SELECT [tblDocument].[MainGuid],[tblDocument].[code],[tblDocument].[Description],[tblDocument].[DescriptionAm],tblDocumentType.DescriptionAm as HRActivity , [Isoptional] 
                                        FROM  tblDocumentType inner join [dbo].[tblDocument]  on tblDocumentType.mainGuid =  tblDocument.ParentGuid ORDER BY  [MainGuid] ASC";
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable dtDocument = new DataTable();
            try
            {
                connection.Open(); 
                adapter.Fill(dtDocument);
                return dtDocument;
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocument::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Exists(Guid documentType , string code)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            string strExists = @"SELECT * FROM [dbo].[tblDocument] WHERE [ParentGuid]=@DocumentType and [Code]=@Code";
            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable dTable = new DataTable();
            try
            {
                command.Parameters.Add(new SqlParameter("@DocumentType", documentType));
                command.Parameters.Add(new SqlParameter("@Code", code));
                adapter.Fill(dTable);
                return (dTable.Rows.Count > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocument::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
    }
}