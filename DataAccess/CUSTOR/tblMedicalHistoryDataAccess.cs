﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblMedicalHistoryDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords(Guid person)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [seq_no],[staff_id],[associate_id],[diagnostic_date],[infection_code],[treatment_given],[visiting_fee],[medicine_expense],[laboratory_expense],[xray_expense],[other_expense],[is_paying],[birth_date],[sex],[patient_type],[remark] FROM [dbo].[tblMedicalHistory] where staff_id=@staff_id  ORDER BY  [seq_no] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblMedicalHistory");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@staff_id", person));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblMedicalHistory::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }
        

        public tblMedicalHistory GetRecord(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblMedicalHistory objtblMedicalHistory = new tblMedicalHistory();
            string strGetRecord = @"SELECT [seq_no],[staff_id],[associate_id],[diagnostic_date],[infection_code],[treatment_given],[visiting_fee],[medicine_expense],[laboratory_expense],[xray_expense],[other_expense],[is_paying],[birth_date],[sex],[patient_type],[remark] FROM [dbo].[tblMedicalHistory] WHERE [seq_no]=@seq_no";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblMedicalHistory");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@seq_no", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblMedicalHistory.Seq_no = (int)dTable.Rows[0]["seq_no"];
                    if (dTable.Rows[0]["staff_id"].Equals(DBNull.Value))
                        objtblMedicalHistory.Staff_id = Guid.Empty;
                    else
                        objtblMedicalHistory.Staff_id = (Guid)dTable.Rows[0]["staff_id"];
                    if (dTable.Rows[0]["associate_id"].Equals(DBNull.Value))
                        objtblMedicalHistory.Associate_id = 0;
                    else
                        objtblMedicalHistory.Associate_id = (int)dTable.Rows[0]["associate_id"];
                    if (dTable.Rows[0]["diagnostic_date"].Equals(DBNull.Value))
                        objtblMedicalHistory.Diagnostic_date = DateTime.MinValue;
                    else
                        objtblMedicalHistory.Diagnostic_date = (DateTime)dTable.Rows[0]["diagnostic_date"];
                    if (dTable.Rows[0]["infection_code"].Equals(DBNull.Value))
                        objtblMedicalHistory.Infection_code = string.Empty;
                    else
                        objtblMedicalHistory.Infection_code = (string)dTable.Rows[0]["infection_code"];
                    if (dTable.Rows[0]["treatment_given"].Equals(DBNull.Value))
                        objtblMedicalHistory.Treatment_given = string.Empty;
                    else
                        objtblMedicalHistory.Treatment_given = (string)dTable.Rows[0]["treatment_given"];
                    if (dTable.Rows[0]["visiting_fee"].Equals(DBNull.Value))
                        objtblMedicalHistory.Visiting_fee = 0;
                    else
                        objtblMedicalHistory.Visiting_fee = (double)dTable.Rows[0]["visiting_fee"];
                    if (dTable.Rows[0]["medicine_expense"].Equals(DBNull.Value))
                        objtblMedicalHistory.Medicine_expense = 0;
                    else
                        objtblMedicalHistory.Medicine_expense = (double)dTable.Rows[0]["medicine_expense"];
                    if (dTable.Rows[0]["laboratory_expense"].Equals(DBNull.Value))
                        objtblMedicalHistory.Laboratory_expense = 0;
                    else
                        objtblMedicalHistory.Laboratory_expense = (double)dTable.Rows[0]["laboratory_expense"];
                    if (dTable.Rows[0]["xray_expense"].Equals(DBNull.Value))
                        objtblMedicalHistory.Xray_expense = 0;
                    else
                        objtblMedicalHistory.Xray_expense = (double)dTable.Rows[0]["xray_expense"];
                    if (dTable.Rows[0]["other_expense"].Equals(DBNull.Value))
                        objtblMedicalHistory.Other_expense = 0;
                    else
                        objtblMedicalHistory.Other_expense = (double)dTable.Rows[0]["other_expense"];
                    if (dTable.Rows[0]["is_paying"].Equals(DBNull.Value))
                        objtblMedicalHistory.Is_paying = 0;
                    else
                        objtblMedicalHistory.Is_paying = (int)dTable.Rows[0]["is_paying"];
                    if (dTable.Rows[0]["birth_date"].Equals(DBNull.Value))
                        objtblMedicalHistory.Birth_date = DateTime.MinValue;
                    else
                        objtblMedicalHistory.Birth_date = (DateTime)dTable.Rows[0]["birth_date"];
                    if (dTable.Rows[0]["sex"].Equals(DBNull.Value))
                        objtblMedicalHistory.Sex = 0;
                    else
                        objtblMedicalHistory.Sex = (int)dTable.Rows[0]["sex"];
                    if (dTable.Rows[0]["patient_type"].Equals(DBNull.Value))
                        objtblMedicalHistory.Patient_type = 0;
                    else
                        objtblMedicalHistory.Patient_type = (int)dTable.Rows[0]["patient_type"];
                    if (dTable.Rows[0]["remark"].Equals(DBNull.Value))
                        objtblMedicalHistory.Remark = string.Empty;
                    else
                        objtblMedicalHistory.Remark = (string)dTable.Rows[0]["remark"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblMedicalHistory::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblMedicalHistory;
        }

       
        public List<tblMedicalHistory> GetList()
        {
            List<tblMedicalHistory> RecordsList = new List<tblMedicalHistory>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [seq_no],[staff_id],[associate_id],[diagnostic_date],[infection_code],[treatment_given],[visiting_fee],[medicine_expense],[laboratory_expense],[xray_expense],[other_expense],[is_paying],[birth_date],[sex],[patient_type],[remark] FROM [dbo].[tblMedicalHistory]  ORDER BY  [seq_no] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblMedicalHistory objtblMedicalHistory = new tblMedicalHistory();
                    if (dr["seq_no"].Equals(DBNull.Value))
                        objtblMedicalHistory.Seq_no = 0;
                    else
                        objtblMedicalHistory.Seq_no = (int)dr["seq_no"];
                    if (dr["staff_id"].Equals(DBNull.Value))
                        objtblMedicalHistory.Staff_id = Guid.Empty;
                    else
                        objtblMedicalHistory.Staff_id = (Guid)dr["staff_id"];
                    if (dr["associate_id"].Equals(DBNull.Value))
                        objtblMedicalHistory.Associate_id = 0;
                    else
                        objtblMedicalHistory.Associate_id = (int)dr["associate_id"];
                    if (dr["diagnostic_date"].Equals(DBNull.Value))
                        objtblMedicalHistory.Diagnostic_date = DateTime.MinValue;
                    else
                        objtblMedicalHistory.Diagnostic_date = (DateTime)dr["diagnostic_date"];
                    if (dr["infection_code"].Equals(DBNull.Value))
                        objtblMedicalHistory.Infection_code = string.Empty;
                    else
                        objtblMedicalHistory.Infection_code = (string)dr["infection_code"];
                    if (dr["treatment_given"].Equals(DBNull.Value))
                        objtblMedicalHistory.Treatment_given = string.Empty;
                    else
                        objtblMedicalHistory.Treatment_given = (string)dr["treatment_given"];
                    if (dr["visiting_fee"].Equals(DBNull.Value))
                        objtblMedicalHistory.Visiting_fee = 0;
                    else
                        objtblMedicalHistory.Visiting_fee = (double)dr["visiting_fee"];
                    if (dr["medicine_expense"].Equals(DBNull.Value))
                        objtblMedicalHistory.Medicine_expense = 0;
                    else
                        objtblMedicalHistory.Medicine_expense = (double)dr["medicine_expense"];
                    if (dr["laboratory_expense"].Equals(DBNull.Value))
                        objtblMedicalHistory.Laboratory_expense = 0;
                    else
                        objtblMedicalHistory.Laboratory_expense = (double)dr["laboratory_expense"];
                    if (dr["xray_expense"].Equals(DBNull.Value))
                        objtblMedicalHistory.Xray_expense = 0;
                    else
                        objtblMedicalHistory.Xray_expense = (double)dr["xray_expense"];
                    if (dr["other_expense"].Equals(DBNull.Value))
                        objtblMedicalHistory.Other_expense = 0;
                    else
                        objtblMedicalHistory.Other_expense = (double)dr["other_expense"];
                    if (dr["is_paying"].Equals(DBNull.Value))
                        objtblMedicalHistory.Is_paying = 0;
                    else
                        objtblMedicalHistory.Is_paying = (int)dr["is_paying"];
                    if (dr["birth_date"].Equals(DBNull.Value))
                        objtblMedicalHistory.Birth_date = DateTime.MinValue;
                    else
                        objtblMedicalHistory.Birth_date = (DateTime)dr["birth_date"];
                    if (dr["sex"].Equals(DBNull.Value))
                        objtblMedicalHistory.Sex = 0;
                    else
                        objtblMedicalHistory.Sex = (int)dr["sex"];
                    if (dr["patient_type"].Equals(DBNull.Value))
                        objtblMedicalHistory.Patient_type = 0;
                    else
                        objtblMedicalHistory.Patient_type = (int)dr["patient_type"];
                    if (dr["remark"].Equals(DBNull.Value))
                        objtblMedicalHistory.Remark = string.Empty;
                    else
                        objtblMedicalHistory.Remark = (string)dr["remark"];
                    RecordsList.Add(objtblMedicalHistory);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblMedicalHistory::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT seq_no FROM [dbo].[tblMedicalHistory] WHERE [seq_no]=@seq_no";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@seq_no", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("tblMedicalHistory::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}