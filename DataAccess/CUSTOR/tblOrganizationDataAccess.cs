﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblOrganizationDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [id],[org_Code],[org_name],[kifle_ketema],[kebele],[house_no],[tel_no1],[tel_no2],[org_category] FROM [dbo].[tblOrganization]  ORDER BY  [id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblOrganization");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblOrganization::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public tblOrganization GetRecord(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblOrganization objtblOrganization = new tblOrganization();
            string strGetRecord = @"SELECT [id],[org_Code],[org_name],[kifle_ketema],[kebele],[house_no],[tel_no1],[tel_no2],[org_category] FROM [dbo].[tblOrganization] WHERE [id]=@id";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblOrganization");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@id", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblOrganization.Id = (int)dTable.Rows[0]["id"];
                    objtblOrganization.Org_Code = (int)dTable.Rows[0]["org_Code"];
                    if (dTable.Rows[0]["org_name"].Equals(DBNull.Value))
                        objtblOrganization.Org_name = string.Empty;
                    else
                        objtblOrganization.Org_name = (string)dTable.Rows[0]["org_name"];
                    if (dTable.Rows[0]["kifle_ketema"].Equals(DBNull.Value))
                        objtblOrganization.Kifle_ketema = string.Empty;
                    else
                        objtblOrganization.Kifle_ketema = (string)dTable.Rows[0]["kifle_ketema"];
                    if (dTable.Rows[0]["kebele"].Equals(DBNull.Value))
                        objtblOrganization.Kebele = string.Empty;
                    else
                        objtblOrganization.Kebele = (string)dTable.Rows[0]["kebele"];
                    if (dTable.Rows[0]["house_no"].Equals(DBNull.Value))
                        objtblOrganization.House_no = string.Empty;
                    else
                        objtblOrganization.House_no = (string)dTable.Rows[0]["house_no"];
                    if (dTable.Rows[0]["tel_no1"].Equals(DBNull.Value))
                        objtblOrganization.Tel_no1 = string.Empty;
                    else
                        objtblOrganization.Tel_no1 = (string)dTable.Rows[0]["tel_no1"];
                    if (dTable.Rows[0]["tel_no2"].Equals(DBNull.Value))
                        objtblOrganization.Tel_no2 = string.Empty;
                    else
                        objtblOrganization.Tel_no2 = (string)dTable.Rows[0]["tel_no2"];
                    if (dTable.Rows[0]["org_category"].Equals(DBNull.Value))
                        objtblOrganization.Org_category = string.Empty;
                    else
                        objtblOrganization.Org_category = (string)dTable.Rows[0]["org_category"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblOrganization::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblOrganization;
        }

        public bool Insert(tblOrganization objtblOrganization)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[tblOrganization]
                                            ([org_Code],[org_name],[kifle_ketema],[kebele],[house_no],[tel_no1],[tel_no2],[org_category])
                                     VALUES    (@org_Code,@org_name,@kifle_ketema,@kebele,@house_no,@tel_no1,@tel_no2,@org_category)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@org_Code", objtblOrganization.Org_Code));
                command.Parameters.Add(new SqlParameter("@org_name", objtblOrganization.Org_name));
                command.Parameters.Add(new SqlParameter("@kifle_ketema", objtblOrganization.Kifle_ketema));
                command.Parameters.Add(new SqlParameter("@kebele", objtblOrganization.Kebele));
                command.Parameters.Add(new SqlParameter("@house_no", objtblOrganization.House_no));
                command.Parameters.Add(new SqlParameter("@tel_no1", objtblOrganization.Tel_no1));
                command.Parameters.Add(new SqlParameter("@tel_no2", objtblOrganization.Tel_no2));
                command.Parameters.Add(new SqlParameter("@org_category", objtblOrganization.Org_category));
                command.Parameters.Add(new SqlParameter("@id", objtblOrganization.Id));
                command.Parameters["@id"].Direction = ParameterDirection.Output;


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                objtblOrganization.Id = (int)command.Parameters["@id"].Value;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblOrganization::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(tblOrganization objtblOrganization)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[tblOrganization] SET     [org_Code]=@org_Code,    [org_name]=@org_name,    [kifle_ketema]=@kifle_ketema,    [kebele]=@kebele,    [house_no]=@house_no,    [tel_no1]=@tel_no1,    [tel_no2]=@tel_no2,    [org_category]=@org_category WHERE [id]=@id";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@id", objtblOrganization.Id));
                command.Parameters.Add(new SqlParameter("@org_Code", objtblOrganization.Org_Code));
                command.Parameters.Add(new SqlParameter("@org_name", objtblOrganization.Org_name));
                command.Parameters.Add(new SqlParameter("@kifle_ketema", objtblOrganization.Kifle_ketema));
                command.Parameters.Add(new SqlParameter("@kebele", objtblOrganization.Kebele));
                command.Parameters.Add(new SqlParameter("@house_no", objtblOrganization.House_no));
                command.Parameters.Add(new SqlParameter("@tel_no1", objtblOrganization.Tel_no1));
                command.Parameters.Add(new SqlParameter("@tel_no2", objtblOrganization.Tel_no2));
                command.Parameters.Add(new SqlParameter("@org_category", objtblOrganization.Org_category));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblOrganization::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblOrganization] WHERE [id]=@id";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@id", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblOrganization::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<tblOrganization> GetList()
        {
            List<tblOrganization> RecordsList = new List<tblOrganization>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [org_Code],[org_name],[kifle_ketema],[kebele],[house_no],[tel_no1],[tel_no2],[org_category] FROM [dbo].[tblOrganization]  ORDER BY  [id] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblOrganization objtblOrganization = new tblOrganization();
                    //if (dr["id"].Equals(DBNull.Value))
                    //    objtblOrganization.Id = 0;
                    //else
                    //    objtblOrganization.Id = (int)dr["id"];
                    if (dr["org_Code"].Equals(DBNull.Value))
                        objtblOrganization.Org_Code = 0;
                    else
                        objtblOrganization.Org_Code = (int)dr["org_Code"];
                    if (dr["org_name"].Equals(DBNull.Value))
                        objtblOrganization.Org_name = string.Empty;
                    else
                        objtblOrganization.Org_name = (string)dr["org_name"];
                    if (dr["kifle_ketema"].Equals(DBNull.Value))
                        objtblOrganization.Kifle_ketema = string.Empty;
                    else
                        objtblOrganization.Kifle_ketema = (string)dr["kifle_ketema"];
                    if (dr["kebele"].Equals(DBNull.Value))
                        objtblOrganization.Kebele = string.Empty;
                    else
                        objtblOrganization.Kebele = (string)dr["kebele"];
                    if (dr["house_no"].Equals(DBNull.Value))
                        objtblOrganization.House_no = string.Empty;
                    else
                        objtblOrganization.House_no = (string)dr["house_no"];
                    if (dr["tel_no1"].Equals(DBNull.Value))
                        objtblOrganization.Tel_no1 = string.Empty;
                    else
                        objtblOrganization.Tel_no1 = (string)dr["tel_no1"];
                    if (dr["tel_no2"].Equals(DBNull.Value))
                        objtblOrganization.Tel_no2 = string.Empty;
                    else
                        objtblOrganization.Tel_no2 = (string)dr["tel_no2"];
                    if (dr["org_category"].Equals(DBNull.Value))
                        objtblOrganization.Org_category = string.Empty;
                    else
                        objtblOrganization.Org_category = (string)dr["org_category"];
                    RecordsList.Add(objtblOrganization);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblOrganization::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT id FROM [dbo].[tblOrganization] WHERE [id]=@id";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@id", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("tblOrganization::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


        public DataTable GetOrganizationByOrgGuid(int orgGuid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [DescriptionAm] FROM [Organization] WHERE [OrgGuid]='" + orgGuid + "' ORDER BY [OrgGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Organization");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Organization::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public DataTable GetOrgnaizations()
        {
            DataTable dtOrganizations = new DataTable("Organization");
            SqlConnection connection = new SqlConnection(ConnectionString);
            string strGetAllRecords = @"SELECT [OrgGuid],[DescriptionAm] FROM [dbo].[Organization]  ORDER BY  [OrgGuid] ASC";
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;
            SqlDataAdapter daOrganizations = new SqlDataAdapter(command);
            try
            {
                connection.Open();
                daOrganizations.Fill(dtOrganizations);
            }
            catch (Exception ex)
            {
                throw new Exception("Organization::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }

            return dtOrganizations;
        }
    

    }
}