﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblNationalityDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [code],[description],[amdescription],[parent],[id] FROM [dbo].[tblNationality] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblNationality");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblNationality::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public tblNationality GetRecord(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblNationality objtblNationality = new tblNationality();
            string strGetRecord = @"SELECT [code],[description],[amdescription],[parent],[id] FROM [dbo].[tblNationality] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblNationality");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    if (dTable.Rows[0]["code"].Equals(DBNull.Value))
                        objtblNationality.Code = string.Empty;
                    else
                        objtblNationality.Code = (string)dTable.Rows[0]["code"];
                    if (dTable.Rows[0]["description"].Equals(DBNull.Value))
                        objtblNationality.Description = string.Empty;
                    else
                        objtblNationality.Description = (string)dTable.Rows[0]["description"];
                    if (dTable.Rows[0]["amdescription"].Equals(DBNull.Value))
                        objtblNationality.Amdescription = string.Empty;
                    else
                        objtblNationality.Amdescription = (string)dTable.Rows[0]["amdescription"];
                    if (dTable.Rows[0]["parent"].Equals(DBNull.Value))
                        objtblNationality.Parent = string.Empty;
                    else
                        objtblNationality.Parent = (string)dTable.Rows[0]["parent"];
                    objtblNationality.Id = (int)dTable.Rows[0]["id"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblNationality::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblNationality;
        }

       
        public List<tblNationality> GetList()
        {
            List<tblNationality> RecordsList = new List<tblNationality>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [code],[description],[amdescription],[parent],[id] FROM [dbo].[tblNationality] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblNationality objtblNationality = new tblNationality();
                    if (dr["code"].Equals(DBNull.Value))
                        objtblNationality.Code = string.Empty;
                    else
                        objtblNationality.Code = (string)dr["code"];
                    if (dr["description"].Equals(DBNull.Value))
                        objtblNationality.Description = string.Empty;
                    else
                        objtblNationality.Description = (string)dr["description"];
                    if (dr["amdescription"].Equals(DBNull.Value))
                        objtblNationality.Amdescription = string.Empty;
                    else
                        objtblNationality.Amdescription = (string)dr["amdescription"];
                    if (dr["parent"].Equals(DBNull.Value))
                        objtblNationality.Parent = string.Empty;
                    else
                        objtblNationality.Parent = (string)dr["parent"];
                    if (dr["id"].Equals(DBNull.Value))
                        objtblNationality.Id = 0;
                    else
                        objtblNationality.Id = (int)dr["id"];
                    RecordsList.Add(objtblNationality);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblNationality::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT  FROM [dbo].[tblNationality] ";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("tblNationality::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}