﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblAddressDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [RegionID],[Region],[ParentID],[LevelNo],[Description] FROM [dbo].[tblAddress] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblAddress");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblAddress::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public tblAddress GetRecord(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblAddress objtblAddress = new tblAddress();
            string strGetRecord = @"SELECT [RegionID],[Region],[ParentID],[LevelNo],[Description] FROM [dbo].[tblAddress] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblAddress");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblAddress.RegionID = (Guid)dTable.Rows[0]["RegionID"];
                    if (dTable.Rows[0]["Region"].Equals(DBNull.Value))
                        objtblAddress.Region = string.Empty;
                    else
                        objtblAddress.Region = (string)dTable.Rows[0]["Region"];
                    if (dTable.Rows[0]["ParentID"].Equals(DBNull.Value))
                        objtblAddress.ParentID = Guid.Empty;
                    else
                        objtblAddress.ParentID = (Guid)dTable.Rows[0]["ParentID"];
                    if (dTable.Rows[0]["LevelNo"].Equals(DBNull.Value))
                        objtblAddress.LevelNo = 0;
                    else
                        objtblAddress.LevelNo = (int)dTable.Rows[0]["LevelNo"];
                    objtblAddress.Description = (string)dTable.Rows[0]["Description"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblAddress::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblAddress;
        }

        public bool Insert(tblAddress objtblAddress)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[tblAddress]
                                            ([RegionID],[Region],[ParentID],[LevelNo],[Description])
                                     VALUES    ( ISNULL(@RegionID, (newid())),@Region,@ParentID,@LevelNo,@Description)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@RegionID", objtblAddress.RegionID));
                command.Parameters.Add(new SqlParameter("@Region", objtblAddress.Region));
                command.Parameters.Add(new SqlParameter("@ParentID", objtblAddress.ParentID));
                command.Parameters.Add(new SqlParameter("@LevelNo", objtblAddress.LevelNo));
                command.Parameters.Add(new SqlParameter("@Description", objtblAddress.Description));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblAddress::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(tblAddress objtblAddress)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[tblAddress] SET     [RegionID]=@RegionID,    [Region]=@Region,    [ParentID]=@ParentID,    [LevelNo]=@LevelNo,    [Description]=@Description ";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@RegionID", objtblAddress.RegionID));
                command.Parameters.Add(new SqlParameter("@Region", objtblAddress.Region));
                command.Parameters.Add(new SqlParameter("@ParentID", objtblAddress.ParentID));
                command.Parameters.Add(new SqlParameter("@LevelNo", objtblAddress.LevelNo));
                command.Parameters.Add(new SqlParameter("@Description", objtblAddress.Description));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblAddress::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblAddress] ";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblAddress::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<tblAddress> GetList()
        {
            List<tblAddress> RecordsList = new List<tblAddress>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [RegionID],[Region],[ParentID],[LevelNo],[Description] FROM [dbo].[tblAddress] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblAddress objtblAddress = new tblAddress();
                    if (dr["RegionID"].Equals(DBNull.Value))
                        objtblAddress.RegionID = Guid.Empty;
                    else
                        objtblAddress.RegionID = (Guid)dr["RegionID"];
                    if (dr["Region"].Equals(DBNull.Value))
                        objtblAddress.Region = string.Empty;
                    else
                        objtblAddress.Region = (string)dr["Region"];
                    if (dr["ParentID"].Equals(DBNull.Value))
                        objtblAddress.ParentID = Guid.Empty;
                    else
                        objtblAddress.ParentID = (Guid)dr["ParentID"];
                    if (dr["LevelNo"].Equals(DBNull.Value))
                        objtblAddress.LevelNo = 0;
                    else
                        objtblAddress.LevelNo = (int)dr["LevelNo"];
                    if (dr["Description"].Equals(DBNull.Value))
                        objtblAddress.Description = string.Empty;
                    else
                        objtblAddress.Description = (string)dr["Description"];
                    RecordsList.Add(objtblAddress);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblAddress::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT  FROM [dbo].[tblAddress] ";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("tblAddress::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}