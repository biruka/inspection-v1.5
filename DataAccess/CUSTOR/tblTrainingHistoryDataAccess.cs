﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Commen;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblTrainingHistoryDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords(Guid person, string language)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[institution],[location],[period_from],[period_to],[dbo].[fnGetDateDifferentNew] ([period_from],[period_to],0," + language + ") as [duration],[course_name],[dbo].fnGetLookupNew([training_type],105,"+ language +@") as [training_type],[expense],[sponsor],[remark],[total_cost],[percent_covered],[address],[contact_person],[dbo].fnGetLookupNew([training_nature],106,"+language+") as [training_nature],[other_cost],[budget],[points_scored],[Country],[Attachment],[Certificat_Type],[NotAttachedReson],[AffirmativeAactionGiven],[AffirmativeDoc_Attached],[Affir_Doc_NotAttachedReason],[Aff_AactionWomen],[Aff_AactionforHandicap],[Aff_AactionforNationsNationalities],[Aff_Act_NotGivenReason],[Is_Trainig_Related_with_Job],[Given_by_the_Org] FROM [dbo].[tblTrainingHistory] WHERE [ParentGuid]=@ParentGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblTrainingHistory");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ParentGuid", person));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblTrainingHistory::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public tblTrainingHistory GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblTrainingHistory objtblTrainingHistory = new tblTrainingHistory();
            string strGetRecord = @"SELECT [MainGuid],[seq_no],[ParentGuid],[institution],[location],[period_from],[period_to],[duration],[course_name],[training_type],[expense],[sponsor],[remark],[total_cost],[percent_covered],[address],[contact_person],[training_nature],[other_cost],[budget],[points_scored],[Country],[Attachment],[Certificat_Type],[NotAttachedReson],[AffirmativeAactionGiven],[AffirmativeDoc_Attached],[Affir_Doc_NotAttachedReason],[Aff_AactionWomen],[Aff_AactionforHandicap],[Aff_AactionforNationsNationalities],[Aff_Act_NotGivenReason],[Is_Trainig_Related_with_Job],[Given_by_the_Org] FROM [dbo].[tblTrainingHistory] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblTrainingHistory");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblTrainingHistory.MainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    if (dTable.Rows[0]["seq_no"].Equals(DBNull.Value))
                        objtblTrainingHistory.Seq_no = 0;
                    else
                        objtblTrainingHistory.Seq_no = (int)dTable.Rows[0]["seq_no"];
                    if (dTable.Rows[0]["ParentGuid"].Equals(DBNull.Value))
                        objtblTrainingHistory.ParentGuid = Guid.Empty;
                    else
                        objtblTrainingHistory.ParentGuid = (Guid)dTable.Rows[0]["ParentGuid"];
                    if (dTable.Rows[0]["institution"].Equals(DBNull.Value))
                        objtblTrainingHistory.Institution = string.Empty;
                    else
                        objtblTrainingHistory.Institution = (string)dTable.Rows[0]["institution"];
                    if (dTable.Rows[0]["location"].Equals(DBNull.Value))
                        objtblTrainingHistory.Location = string.Empty;
                    else
                        objtblTrainingHistory.Location = (string)dTable.Rows[0]["location"];
                    if (dTable.Rows[0]["period_from"].Equals(DBNull.Value))
                        objtblTrainingHistory.Period_from = DateTime.MinValue;
                    else
                        objtblTrainingHistory.Period_from = (DateTime)dTable.Rows[0]["period_from"];
                    if (dTable.Rows[0]["period_to"].Equals(DBNull.Value))
                        objtblTrainingHistory.Period_to = DateTime.MinValue;
                    else
                        objtblTrainingHistory.Period_to = (DateTime)dTable.Rows[0]["period_to"];
                    if (dTable.Rows[0]["duration"].Equals(DBNull.Value))
                        objtblTrainingHistory.Duration = string.Empty;
                    else
                        objtblTrainingHistory.Duration = (string)dTable.Rows[0]["duration"];
                    if (dTable.Rows[0]["course_name"].Equals(DBNull.Value))
                        objtblTrainingHistory.Course_name = string.Empty;
                    else
                        objtblTrainingHistory.Course_name = (string)dTable.Rows[0]["course_name"];
                    if (dTable.Rows[0]["training_type"].Equals(DBNull.Value))
                        objtblTrainingHistory.Training_type = 0;
                    else
                        objtblTrainingHistory.Training_type = (int)dTable.Rows[0]["training_type"];
                    if (dTable.Rows[0]["expense"].Equals(DBNull.Value))
                        objtblTrainingHistory.Expense = 0;
                    else
                        objtblTrainingHistory.Expense = (double)dTable.Rows[0]["expense"];
                    if (dTable.Rows[0]["sponsor"].Equals(DBNull.Value))
                        objtblTrainingHistory.Sponsor = string.Empty;
                    else
                        objtblTrainingHistory.Sponsor = (string)dTable.Rows[0]["sponsor"];
                    if (dTable.Rows[0]["remark"].Equals(DBNull.Value))
                        objtblTrainingHistory.Remark = string.Empty;
                    else
                        objtblTrainingHistory.Remark = (string)dTable.Rows[0]["remark"];
                    if (dTable.Rows[0]["total_cost"].Equals(DBNull.Value))
                        objtblTrainingHistory.Total_cost = 0;
                    else
                        objtblTrainingHistory.Total_cost = (double)dTable.Rows[0]["total_cost"];
                    if (dTable.Rows[0]["percent_covered"].Equals(DBNull.Value))
                        objtblTrainingHistory.Percent_covered = 0;
                    else
                        objtblTrainingHistory.Percent_covered = (int)dTable.Rows[0]["percent_covered"];
                    if (dTable.Rows[0]["address"].Equals(DBNull.Value))
                        objtblTrainingHistory.Address = string.Empty;
                    else
                        objtblTrainingHistory.Address = (string)dTable.Rows[0]["address"];
                    if (dTable.Rows[0]["contact_person"].Equals(DBNull.Value))
                        objtblTrainingHistory.Contact_person = string.Empty;
                    else
                        objtblTrainingHistory.Contact_person = (string)dTable.Rows[0]["contact_person"];
                    if (dTable.Rows[0]["training_nature"].Equals(DBNull.Value))
                        objtblTrainingHistory.Training_nature = 0;
                    else
                        objtblTrainingHistory.Training_nature = (int)dTable.Rows[0]["training_nature"];
                    if (dTable.Rows[0]["other_cost"].Equals(DBNull.Value))
                        objtblTrainingHistory.Other_cost = 0;
                    else
                        objtblTrainingHistory.Other_cost = (double)dTable.Rows[0]["other_cost"];
                    if (dTable.Rows[0]["budget"].Equals(DBNull.Value))
                        objtblTrainingHistory.Budget = 0;
                    else
                        objtblTrainingHistory.Budget = (double)dTable.Rows[0]["budget"];
                    if (dTable.Rows[0]["points_scored"].Equals(DBNull.Value))
                        objtblTrainingHistory.Points_scored = 0;
                    else
                        objtblTrainingHistory.Points_scored = (double)dTable.Rows[0]["points_scored"];
                    if (dTable.Rows[0]["Country"].Equals(DBNull.Value))
                        objtblTrainingHistory.Country = string.Empty;
                    else
                        objtblTrainingHistory.Country = (string)dTable.Rows[0]["Country"];
                    if (dTable.Rows[0]["Attachment"].Equals(DBNull.Value))
                        objtblTrainingHistory.Attachment = false;
                    else
                        objtblTrainingHistory.Attachment = (bool)dTable.Rows[0]["Attachment"];
                    if (dTable.Rows[0]["Certificat_Type"].Equals(DBNull.Value))
                        objtblTrainingHistory.Certificat_Type = string.Empty;
                    else
                        objtblTrainingHistory.Certificat_Type = (string)dTable.Rows[0]["Certificat_Type"];
                    if (dTable.Rows[0]["NotAttachedReson"].Equals(DBNull.Value))
                        objtblTrainingHistory.NotAttachedReson = string.Empty;
                    else
                        objtblTrainingHistory.NotAttachedReson = (string)dTable.Rows[0]["NotAttachedReson"];
                    if (dTable.Rows[0]["AffirmativeAactionGiven"].Equals(DBNull.Value))
                        objtblTrainingHistory.AffirmativeAactionGiven = false;
                    else
                        objtblTrainingHistory.AffirmativeAactionGiven = (bool)dTable.Rows[0]["AffirmativeAactionGiven"];
                    if (dTable.Rows[0]["AffirmativeDoc_Attached"].Equals(DBNull.Value))
                        objtblTrainingHistory.AffirmativeDoc_Attached = false;
                    else
                        objtblTrainingHistory.AffirmativeDoc_Attached = (bool)dTable.Rows[0]["AffirmativeDoc_Attached"];
                    if (dTable.Rows[0]["Affir_Doc_NotAttachedReason"].Equals(DBNull.Value))
                        objtblTrainingHistory.Affir_Doc_NotAttachedReason = string.Empty;
                    else
                        objtblTrainingHistory.Affir_Doc_NotAttachedReason = (string)dTable.Rows[0]["Affir_Doc_NotAttachedReason"];
                    if (dTable.Rows[0]["Aff_AactionWomen"].Equals(DBNull.Value))
                        objtblTrainingHistory.Aff_AactionWomen = false;
                    else
                        objtblTrainingHistory.Aff_AactionWomen = (bool)dTable.Rows[0]["Aff_AactionWomen"];
                    if (dTable.Rows[0]["Aff_AactionforHandicap"].Equals(DBNull.Value))
                        objtblTrainingHistory.Aff_AactionforHandicap = false;
                    else
                        objtblTrainingHistory.Aff_AactionforHandicap = (bool)dTable.Rows[0]["Aff_AactionforHandicap"];
                    if (dTable.Rows[0]["Aff_AactionforNationsNationalities"].Equals(DBNull.Value))
                        objtblTrainingHistory.Aff_AactionforNationsNationalities = false;
                    else
                        objtblTrainingHistory.Aff_AactionforNationsNationalities = (bool)dTable.Rows[0]["Aff_AactionforNationsNationalities"];
                    if (dTable.Rows[0]["Aff_Act_NotGivenReason"].Equals(DBNull.Value))
                        objtblTrainingHistory.Aff_Act_NotGivenReason = string.Empty;
                    else
                        objtblTrainingHistory.Aff_Act_NotGivenReason = (string)dTable.Rows[0]["Aff_Act_NotGivenReason"];
                    if (dTable.Rows[0]["Is_Trainig_Related_with_Job"].Equals(DBNull.Value))
                        objtblTrainingHistory.Is_Trainig_Related_with_Job = false;
                    else
                        objtblTrainingHistory.Is_Trainig_Related_with_Job = (bool)dTable.Rows[0]["Is_Trainig_Related_with_Job"];
                    if (dTable.Rows[0]["Given_by_the_Org"].Equals(DBNull.Value))
                        objtblTrainingHistory.Given_by_the_Org = false;
                    else
                        objtblTrainingHistory.Given_by_the_Org = (bool)dTable.Rows[0]["Given_by_the_Org"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblTrainingHistory::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblTrainingHistory;
        }

        public bool Insert(tblTrainingHistory objtblTrainingHistory)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[tblTrainingHistory]
                                            ([MainGuid],[seq_no],[ParentGuid],[institution],[location],[period_from],[period_to],[duration],[course_name],[training_type],[expense],[sponsor],[remark],[total_cost],[percent_covered],[address],[contact_person],[training_nature],[other_cost],[budget],[points_scored],[Country],[Attachment],[Certificat_Type],[NotAttachedReson],[AffirmativeAactionGiven],[AffirmativeDoc_Attached],[Affir_Doc_NotAttachedReason],[Aff_AactionWomen],[Aff_AactionforHandicap],[Aff_AactionforNationsNationalities],[Aff_Act_NotGivenReason],[Is_Trainig_Related_with_Job],[Given_by_the_Org])
                                     VALUES    ( ISNULL(@MainGuid, (newid())),@seq_no,@ParentGuid,@institution,@location,@period_from,@period_to,@duration,@course_name,@training_type,@expense,@sponsor,@remark,@total_cost,@percent_covered,@address,@contact_person,@training_nature,@other_cost,@budget,@points_scored,@Country,@Attachment,@Certificat_Type,@NotAttachedReson,@AffirmativeAactionGiven,@AffirmativeDoc_Attached,@Affir_Doc_NotAttachedReason,@Aff_AactionWomen,@Aff_AactionforHandicap,@Aff_AactionforNationsNationalities,@Aff_Act_NotGivenReason,@Is_Trainig_Related_with_Job,@Given_by_the_Org)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", objtblTrainingHistory.MainGuid));
                command.Parameters.Add(new SqlParameter("@seq_no", objtblTrainingHistory.Seq_no));
                command.Parameters.Add(new SqlParameter("@ParentGuid", objtblTrainingHistory.ParentGuid));
                command.Parameters.Add(new SqlParameter("@institution", objtblTrainingHistory.Institution));
                command.Parameters.Add(new SqlParameter("@location", objtblTrainingHistory.Location));
                command.Parameters.Add(new SqlParameter("@period_from", objtblTrainingHistory.Period_from));
                command.Parameters.Add(new SqlParameter("@period_to", objtblTrainingHistory.Period_to));
                command.Parameters.Add(new SqlParameter("@duration", objtblTrainingHistory.Duration));
                command.Parameters.Add(new SqlParameter("@course_name", objtblTrainingHistory.Course_name));
                command.Parameters.Add(new SqlParameter("@training_type", objtblTrainingHistory.Training_type));
                command.Parameters.Add(new SqlParameter("@expense", objtblTrainingHistory.Expense));
                command.Parameters.Add(new SqlParameter("@sponsor", objtblTrainingHistory.Sponsor));
                command.Parameters.Add(new SqlParameter("@remark", objtblTrainingHistory.Remark));
                command.Parameters.Add(new SqlParameter("@total_cost", objtblTrainingHistory.Total_cost));
                command.Parameters.Add(new SqlParameter("@percent_covered", objtblTrainingHistory.Percent_covered));
                command.Parameters.Add(new SqlParameter("@address", objtblTrainingHistory.Address));
                command.Parameters.Add(new SqlParameter("@contact_person", objtblTrainingHistory.Contact_person));
                command.Parameters.Add(new SqlParameter("@training_nature", objtblTrainingHistory.Training_nature));
                command.Parameters.Add(new SqlParameter("@other_cost", objtblTrainingHistory.Other_cost));
                command.Parameters.Add(new SqlParameter("@budget", objtblTrainingHistory.Budget));
                command.Parameters.Add(new SqlParameter("@points_scored", objtblTrainingHistory.Points_scored));
                command.Parameters.Add(new SqlParameter("@Country", objtblTrainingHistory.Country));
                command.Parameters.Add(new SqlParameter("@Attachment", objtblTrainingHistory.Attachment));
                command.Parameters.Add(new SqlParameter("@Certificat_Type", objtblTrainingHistory.Certificat_Type));
                command.Parameters.Add(new SqlParameter("@NotAttachedReson", objtblTrainingHistory.NotAttachedReson));
                command.Parameters.Add(new SqlParameter("@AffirmativeAactionGiven", objtblTrainingHistory.AffirmativeAactionGiven));
                command.Parameters.Add(new SqlParameter("@AffirmativeDoc_Attached", objtblTrainingHistory.AffirmativeDoc_Attached));
                command.Parameters.Add(new SqlParameter("@Affir_Doc_NotAttachedReason", objtblTrainingHistory.Affir_Doc_NotAttachedReason));
                command.Parameters.Add(new SqlParameter("@Aff_AactionWomen", objtblTrainingHistory.Aff_AactionWomen));
                command.Parameters.Add(new SqlParameter("@Aff_AactionforHandicap", objtblTrainingHistory.Aff_AactionforHandicap));
                command.Parameters.Add(new SqlParameter("@Aff_AactionforNationsNationalities", objtblTrainingHistory.Aff_AactionforNationsNationalities));
                command.Parameters.Add(new SqlParameter("@Aff_Act_NotGivenReason", objtblTrainingHistory.Aff_Act_NotGivenReason));
                command.Parameters.Add(new SqlParameter("@Is_Trainig_Related_with_Job", objtblTrainingHistory.Is_Trainig_Related_with_Job));
                command.Parameters.Add(new SqlParameter("@Given_by_the_Org", objtblTrainingHistory.Given_by_the_Org));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblTrainingHistory::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(tblTrainingHistory objtblTrainingHistory)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[tblTrainingHistory] SET     [MainGuid]=@MainGuid,    [seq_no]=@seq_no,    [ParentGuid]=@ParentGuid,    [institution]=@institution,    [location]=@location,    [period_from]=@period_from,    [period_to]=@period_to,    [duration]=@duration,    [course_name]=@course_name,    [training_type]=@training_type,    [expense]=@expense,    [sponsor]=@sponsor,    [remark]=@remark,    [total_cost]=@total_cost,    [percent_covered]=@percent_covered,    [address]=@address,    [contact_person]=@contact_person,    [training_nature]=@training_nature,    [other_cost]=@other_cost,    [budget]=@budget,    [points_scored]=@points_scored,    [Country]=@Country,    [Attachment]=@Attachment,    [Certificat_Type]=@Certificat_Type,    [NotAttachedReson]=@NotAttachedReson,    [AffirmativeAactionGiven]=@AffirmativeAactionGiven,    [AffirmativeDoc_Attached]=@AffirmativeDoc_Attached,    [Affir_Doc_NotAttachedReason]=@Affir_Doc_NotAttachedReason,    [Aff_AactionWomen]=@Aff_AactionWomen,    [Aff_AactionforHandicap]=@Aff_AactionforHandicap,    [Aff_AactionforNationsNationalities]=@Aff_AactionforNationsNationalities,    [Aff_Act_NotGivenReason]=@Aff_Act_NotGivenReason,    [Is_Trainig_Related_with_Job]=@Is_Trainig_Related_with_Job,    [Given_by_the_Org]=@Given_by_the_Org WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", objtblTrainingHistory.MainGuid));
                command.Parameters.Add(new SqlParameter("@seq_no", objtblTrainingHistory.Seq_no));
                command.Parameters.Add(new SqlParameter("@ParentGuid", objtblTrainingHistory.ParentGuid));
                command.Parameters.Add(new SqlParameter("@institution", objtblTrainingHistory.Institution));
                command.Parameters.Add(new SqlParameter("@location", objtblTrainingHistory.Location));
                command.Parameters.Add(new SqlParameter("@period_from", objtblTrainingHistory.Period_from));
                command.Parameters.Add(new SqlParameter("@period_to", objtblTrainingHistory.Period_to));
                command.Parameters.Add(new SqlParameter("@duration", objtblTrainingHistory.Duration));
                command.Parameters.Add(new SqlParameter("@course_name", objtblTrainingHistory.Course_name));
                command.Parameters.Add(new SqlParameter("@training_type", objtblTrainingHistory.Training_type));
                command.Parameters.Add(new SqlParameter("@expense", objtblTrainingHistory.Expense));
                command.Parameters.Add(new SqlParameter("@sponsor", objtblTrainingHistory.Sponsor));
                command.Parameters.Add(new SqlParameter("@remark", objtblTrainingHistory.Remark));
                command.Parameters.Add(new SqlParameter("@total_cost", objtblTrainingHistory.Total_cost));
                command.Parameters.Add(new SqlParameter("@percent_covered", objtblTrainingHistory.Percent_covered));
                command.Parameters.Add(new SqlParameter("@address", objtblTrainingHistory.Address));
                command.Parameters.Add(new SqlParameter("@contact_person", objtblTrainingHistory.Contact_person));
                command.Parameters.Add(new SqlParameter("@training_nature", objtblTrainingHistory.Training_nature));
                command.Parameters.Add(new SqlParameter("@other_cost", objtblTrainingHistory.Other_cost));
                command.Parameters.Add(new SqlParameter("@budget", objtblTrainingHistory.Budget));
                command.Parameters.Add(new SqlParameter("@points_scored", objtblTrainingHistory.Points_scored));
                command.Parameters.Add(new SqlParameter("@Country", objtblTrainingHistory.Country));
                command.Parameters.Add(new SqlParameter("@Attachment", objtblTrainingHistory.Attachment));
                command.Parameters.Add(new SqlParameter("@Certificat_Type", objtblTrainingHistory.Certificat_Type));
                command.Parameters.Add(new SqlParameter("@NotAttachedReson", objtblTrainingHistory.NotAttachedReson));
                command.Parameters.Add(new SqlParameter("@AffirmativeAactionGiven", objtblTrainingHistory.AffirmativeAactionGiven));
                command.Parameters.Add(new SqlParameter("@AffirmativeDoc_Attached", objtblTrainingHistory.AffirmativeDoc_Attached));
                command.Parameters.Add(new SqlParameter("@Affir_Doc_NotAttachedReason", objtblTrainingHistory.Affir_Doc_NotAttachedReason));
                command.Parameters.Add(new SqlParameter("@Aff_AactionWomen", objtblTrainingHistory.Aff_AactionWomen));
                command.Parameters.Add(new SqlParameter("@Aff_AactionforHandicap", objtblTrainingHistory.Aff_AactionforHandicap));
                command.Parameters.Add(new SqlParameter("@Aff_AactionforNationsNationalities", objtblTrainingHistory.Aff_AactionforNationsNationalities));
                command.Parameters.Add(new SqlParameter("@Aff_Act_NotGivenReason", objtblTrainingHistory.Aff_Act_NotGivenReason));
                command.Parameters.Add(new SqlParameter("@Is_Trainig_Related_with_Job", objtblTrainingHistory.Is_Trainig_Related_with_Job));
                command.Parameters.Add(new SqlParameter("@Given_by_the_Org", objtblTrainingHistory.Given_by_the_Org));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblTrainingHistory::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblTrainingHistory] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblTrainingHistory::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<tblTrainingHistory> GetList()
        {
            List<tblTrainingHistory> RecordsList = new List<tblTrainingHistory>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[institution],[location],[period_from],[period_to],[duration],[course_name],[training_type],[expense],[sponsor],[remark],[total_cost],[percent_covered],[address],[contact_person],[training_nature],[other_cost],[budget],[points_scored],[Country],[Attachment],[Certificat_Type],[NotAttachedReson],[AffirmativeAactionGiven],[AffirmativeDoc_Attached],[Affir_Doc_NotAttachedReason],[Aff_AactionWomen],[Aff_AactionforHandicap],[Aff_AactionforNationsNationalities],[Aff_Act_NotGivenReason],[Is_Trainig_Related_with_Job],[Given_by_the_Org] FROM [dbo].[tblTrainingHistory]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblTrainingHistory objtblTrainingHistory = new tblTrainingHistory();
                    if (dr["MainGuid"].Equals(DBNull.Value))
                        objtblTrainingHistory.MainGuid = Guid.Empty;
                    else
                        objtblTrainingHistory.MainGuid = (Guid)dr["MainGuid"];
                    if (dr["seq_no"].Equals(DBNull.Value))
                        objtblTrainingHistory.Seq_no = 0;
                    else
                        objtblTrainingHistory.Seq_no = (int)dr["seq_no"];
                    if (dr["ParentGuid"].Equals(DBNull.Value))
                        objtblTrainingHistory.ParentGuid = Guid.Empty;
                    else
                        objtblTrainingHistory.ParentGuid = (Guid)dr["ParentGuid"];
                    if (dr["institution"].Equals(DBNull.Value))
                        objtblTrainingHistory.Institution = string.Empty;
                    else
                        objtblTrainingHistory.Institution = (string)dr["institution"];
                    if (dr["location"].Equals(DBNull.Value))
                        objtblTrainingHistory.Location = string.Empty;
                    else
                        objtblTrainingHistory.Location = (string)dr["location"];
                    if (dr["period_from"].Equals(DBNull.Value))
                        objtblTrainingHistory.Period_from = DateTime.MinValue;
                    else
                        objtblTrainingHistory.Period_from = (DateTime)dr["period_from"];
                    if (dr["period_to"].Equals(DBNull.Value))
                        objtblTrainingHistory.Period_to = DateTime.MinValue;
                    else
                        objtblTrainingHistory.Period_to = (DateTime)dr["period_to"];
                    if (dr["duration"].Equals(DBNull.Value))
                        objtblTrainingHistory.Duration = string.Empty;
                    else
                        objtblTrainingHistory.Duration = (string)dr["duration"];
                    if (dr["course_name"].Equals(DBNull.Value))
                        objtblTrainingHistory.Course_name = string.Empty;
                    else
                        objtblTrainingHistory.Course_name = (string)dr["course_name"];
                    if (dr["training_type"].Equals(DBNull.Value))
                        objtblTrainingHistory.Training_type = 0;
                    else
                        objtblTrainingHistory.Training_type = (int)dr["training_type"];
                    if (dr["expense"].Equals(DBNull.Value))
                        objtblTrainingHistory.Expense = 0;
                    else
                        objtblTrainingHistory.Expense = (double)dr["expense"];
                    if (dr["sponsor"].Equals(DBNull.Value))
                        objtblTrainingHistory.Sponsor = string.Empty;
                    else
                        objtblTrainingHistory.Sponsor = (string)dr["sponsor"];
                    if (dr["remark"].Equals(DBNull.Value))
                        objtblTrainingHistory.Remark = string.Empty;
                    else
                        objtblTrainingHistory.Remark = (string)dr["remark"];
                    if (dr["total_cost"].Equals(DBNull.Value))
                        objtblTrainingHistory.Total_cost = 0;
                    else
                        objtblTrainingHistory.Total_cost = (double)dr["total_cost"];
                    if (dr["percent_covered"].Equals(DBNull.Value))
                        objtblTrainingHistory.Percent_covered = 0;
                    else
                        objtblTrainingHistory.Percent_covered = (int)dr["percent_covered"];
                    if (dr["address"].Equals(DBNull.Value))
                        objtblTrainingHistory.Address = string.Empty;
                    else
                        objtblTrainingHistory.Address = (string)dr["address"];
                    if (dr["contact_person"].Equals(DBNull.Value))
                        objtblTrainingHistory.Contact_person = string.Empty;
                    else
                        objtblTrainingHistory.Contact_person = (string)dr["contact_person"];
                    if (dr["training_nature"].Equals(DBNull.Value))
                        objtblTrainingHistory.Training_nature = 0;
                    else
                        objtblTrainingHistory.Training_nature = (int)dr["training_nature"];
                    if (dr["other_cost"].Equals(DBNull.Value))
                        objtblTrainingHistory.Other_cost = 0;
                    else
                        objtblTrainingHistory.Other_cost = (double)dr["other_cost"];
                    if (dr["budget"].Equals(DBNull.Value))
                        objtblTrainingHistory.Budget = 0;
                    else
                        objtblTrainingHistory.Budget = (double)dr["budget"];
                    if (dr["points_scored"].Equals(DBNull.Value))
                        objtblTrainingHistory.Points_scored = 0;
                    else
                        objtblTrainingHistory.Points_scored = (double)dr["points_scored"];
                    if (dr["Country"].Equals(DBNull.Value))
                        objtblTrainingHistory.Country = string.Empty;
                    else
                        objtblTrainingHistory.Country = (string)dr["Country"];
                    if (dr["Attachment"].Equals(DBNull.Value))
                        objtblTrainingHistory.Attachment = false;
                    else
                        objtblTrainingHistory.Attachment = (bool)dr["Attachment"];
                    if (dr["Certificat_Type"].Equals(DBNull.Value))
                        objtblTrainingHistory.Certificat_Type = string.Empty;
                    else
                        objtblTrainingHistory.Certificat_Type = (string)dr["Certificat_Type"];
                    if (dr["NotAttachedReson"].Equals(DBNull.Value))
                        objtblTrainingHistory.NotAttachedReson = string.Empty;
                    else
                        objtblTrainingHistory.NotAttachedReson = (string)dr["NotAttachedReson"];
                    if (dr["AffirmativeAactionGiven"].Equals(DBNull.Value))
                        objtblTrainingHistory.AffirmativeAactionGiven = false;
                    else
                        objtblTrainingHistory.AffirmativeAactionGiven = (bool)dr["AffirmativeAactionGiven"];
                    if (dr["AffirmativeDoc_Attached"].Equals(DBNull.Value))
                        objtblTrainingHistory.AffirmativeDoc_Attached = false;
                    else
                        objtblTrainingHistory.AffirmativeDoc_Attached = (bool)dr["AffirmativeDoc_Attached"];
                    if (dr["Affir_Doc_NotAttachedReason"].Equals(DBNull.Value))
                        objtblTrainingHistory.Affir_Doc_NotAttachedReason = string.Empty;
                    else
                        objtblTrainingHistory.Affir_Doc_NotAttachedReason = (string)dr["Affir_Doc_NotAttachedReason"];
                    if (dr["Aff_AactionWomen"].Equals(DBNull.Value))
                        objtblTrainingHistory.Aff_AactionWomen = false;
                    else
                        objtblTrainingHistory.Aff_AactionWomen = (bool)dr["Aff_AactionWomen"];
                    if (dr["Aff_AactionforHandicap"].Equals(DBNull.Value))
                        objtblTrainingHistory.Aff_AactionforHandicap = false;
                    else
                        objtblTrainingHistory.Aff_AactionforHandicap = (bool)dr["Aff_AactionforHandicap"];
                    if (dr["Aff_AactionforNationsNationalities"].Equals(DBNull.Value))
                        objtblTrainingHistory.Aff_AactionforNationsNationalities = false;
                    else
                        objtblTrainingHistory.Aff_AactionforNationsNationalities = (bool)dr["Aff_AactionforNationsNationalities"];
                    if (dr["Aff_Act_NotGivenReason"].Equals(DBNull.Value))
                        objtblTrainingHistory.Aff_Act_NotGivenReason = string.Empty;
                    else
                        objtblTrainingHistory.Aff_Act_NotGivenReason = (string)dr["Aff_Act_NotGivenReason"];
                    if (dr["Is_Trainig_Related_with_Job"].Equals(DBNull.Value))
                        objtblTrainingHistory.Is_Trainig_Related_with_Job = false;
                    else
                        objtblTrainingHistory.Is_Trainig_Related_with_Job = (bool)dr["Is_Trainig_Related_with_Job"];
                    if (dr["Given_by_the_Org"].Equals(DBNull.Value))
                        objtblTrainingHistory.Given_by_the_Org = false;
                    else
                        objtblTrainingHistory.Given_by_the_Org = (bool)dr["Given_by_the_Org"];
                    RecordsList.Add(objtblTrainingHistory);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblTrainingHistory::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT MainGuid FROM [dbo].[tblTrainingHistory] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("tblTrainingHistory::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}