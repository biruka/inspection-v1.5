﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblOverTimeDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        
        public DataTable GetRecords(Guid person)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [MainGuid],[overtime_id],[staff_code],[ParentGuid],[overtime_month],[overtime_year],[overtime_type],[overtime_reason],[worked_hours],[overtime_amount],[overtime_date] FROM [dbo].[tblOverTime] where ParentGuid=@ParentGuid   ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblOverTime");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ParentGuid", person));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblOverTime::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }
        

        public tblOverTime GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblOverTime objtblOverTime = new tblOverTime();
            string strGetRecord = @"SELECT [MainGuid],[overtime_id],[staff_code],[ParentGuid],[overtime_month],[overtime_year],[overtime_type],[overtime_reason],[worked_hours],[overtime_amount],[overtime_date] FROM [dbo].[tblOverTime] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblOverTime");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblOverTime.MainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    objtblOverTime.Overtime_id = (int)dTable.Rows[0]["overtime_id"];
                    if (dTable.Rows[0]["staff_code"].Equals(DBNull.Value))
                        objtblOverTime.Staff_code = string.Empty;
                    else
                        objtblOverTime.Staff_code = (string)dTable.Rows[0]["staff_code"];
                    objtblOverTime.ParentGuid = (Guid)dTable.Rows[0]["ParentGuid"];
                    if (dTable.Rows[0]["overtime_month"].Equals(DBNull.Value))
                        objtblOverTime.Overtime_month = 0;
                    else
                        objtblOverTime.Overtime_month = (int)dTable.Rows[0]["overtime_month"];
                    if (dTable.Rows[0]["overtime_year"].Equals(DBNull.Value))
                        objtblOverTime.Overtime_year = 0;
                    else
                        objtblOverTime.Overtime_year = (int)dTable.Rows[0]["overtime_year"];
                    if (dTable.Rows[0]["overtime_type"].Equals(DBNull.Value))
                        objtblOverTime.Overtime_type = 0;
                    else
                        objtblOverTime.Overtime_type = (int)dTable.Rows[0]["overtime_type"];
                    if (dTable.Rows[0]["overtime_reason"].Equals(DBNull.Value))
                        objtblOverTime.Overtime_reason = string.Empty;
                    else
                        objtblOverTime.Overtime_reason = (string)dTable.Rows[0]["overtime_reason"];
                    if (dTable.Rows[0]["worked_hours"].Equals(DBNull.Value))
                        objtblOverTime.Worked_hours = 0;
                    else
                        objtblOverTime.Worked_hours = (double)dTable.Rows[0]["worked_hours"];
                    if (dTable.Rows[0]["overtime_amount"].Equals(DBNull.Value))
                        objtblOverTime.Overtime_amount = 0;
                    else
                        objtblOverTime.Overtime_amount = (double)dTable.Rows[0]["overtime_amount"];
                    if (dTable.Rows[0]["overtime_date"].Equals(DBNull.Value))
                        objtblOverTime.Overtime_date = DateTime.MinValue;
                    else
                        objtblOverTime.Overtime_date = (DateTime)dTable.Rows[0]["overtime_date"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblOverTime::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblOverTime;
        }

        
        public List<tblOverTime> GetList()
        {
            List<tblOverTime> RecordsList = new List<tblOverTime>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [MainGuid],[overtime_id],[staff_code],[ParentGuid],[overtime_month],[overtime_year],[overtime_type],[overtime_reason],[worked_hours],[overtime_amount],[overtime_date] FROM [dbo].[tblOverTime]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblOverTime objtblOverTime = new tblOverTime();
                    if (dr["MainGuid"].Equals(DBNull.Value))
                        objtblOverTime.MainGuid = Guid.Empty;
                    else
                        objtblOverTime.MainGuid = (Guid)dr["MainGuid"];
                    if (dr["overtime_id"].Equals(DBNull.Value))
                        objtblOverTime.Overtime_id = 0;
                    else
                        objtblOverTime.Overtime_id = (int)dr["overtime_id"];
                    if (dr["staff_code"].Equals(DBNull.Value))
                        objtblOverTime.Staff_code = string.Empty;
                    else
                        objtblOverTime.Staff_code = (string)dr["staff_code"];
                    if (dr["ParentGuid"].Equals(DBNull.Value))
                        objtblOverTime.ParentGuid = Guid.Empty;
                    else
                        objtblOverTime.ParentGuid = (Guid)dr["ParentGuid"];
                    if (dr["overtime_month"].Equals(DBNull.Value))
                        objtblOverTime.Overtime_month = 0;
                    else
                        objtblOverTime.Overtime_month = (int)dr["overtime_month"];
                    if (dr["overtime_year"].Equals(DBNull.Value))
                        objtblOverTime.Overtime_year = 0;
                    else
                        objtblOverTime.Overtime_year = (int)dr["overtime_year"];
                    if (dr["overtime_type"].Equals(DBNull.Value))
                        objtblOverTime.Overtime_type = 0;
                    else
                        objtblOverTime.Overtime_type = (int)dr["overtime_type"];
                    if (dr["overtime_reason"].Equals(DBNull.Value))
                        objtblOverTime.Overtime_reason = string.Empty;
                    else
                        objtblOverTime.Overtime_reason = (string)dr["overtime_reason"];
                    if (dr["worked_hours"].Equals(DBNull.Value))
                        objtblOverTime.Worked_hours = 0;
                    else
                        objtblOverTime.Worked_hours = (double)dr["worked_hours"];
                    if (dr["overtime_amount"].Equals(DBNull.Value))
                        objtblOverTime.Overtime_amount = 0;
                    else
                        objtblOverTime.Overtime_amount = (double)dr["overtime_amount"];
                    if (dr["overtime_date"].Equals(DBNull.Value))
                        objtblOverTime.Overtime_date = DateTime.MinValue;
                    else
                        objtblOverTime.Overtime_date = (DateTime)dr["overtime_date"];
                    RecordsList.Add(objtblOverTime);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblOverTime::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        
    }
}