﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class OrganizationDataAccess
    {
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = "[dbo].[sp_GetOrganization]";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.StoredProcedure };
            DataTable dTable = new DataTable("Organization");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Organization::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public Organization GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            Organization objOrganization = new Organization();
            string strGetRecord = @"SELECT [OrgGuid],[Code],[Description],[DescriptionAm],[DescriptionAmSort],[DescriptionTig],[DescriptionTigSort],[DescriptionOr],[DescriptionAf],[DescriptionSom],[YearEstablished],[ParentAdministrationCode],[OrgType],[OrgHistory],[IsActive],[FileNumber],[Telephone],[POBox],[EMail],[WebURL],[SiteName],[Logo],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime] FROM [dbo].[Organization] WHERE [OrgGuid]=@OrgGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Organization");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@OrgGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objOrganization.OrgGuid = (Guid)dTable.Rows[0]["OrgGuid"];
                    if (dTable.Rows[0]["Code"].Equals(DBNull.Value))
                        objOrganization.Code = string.Empty;
                    else
                        objOrganization.Code = (string)dTable.Rows[0]["Code"];
                    objOrganization.Description = (string)dTable.Rows[0]["Description"];
                    objOrganization.DescriptionAm = (string)dTable.Rows[0]["DescriptionAm"];
                    if (dTable.Rows[0]["DescriptionAmSort"].Equals(DBNull.Value))
                        objOrganization.DescriptionAmSort = string.Empty;
                    else
                        objOrganization.DescriptionAmSort = (string)dTable.Rows[0]["DescriptionAmSort"];
                    if (dTable.Rows[0]["DescriptionTig"].Equals(DBNull.Value))
                        objOrganization.DescriptionTig = string.Empty;
                    else
                        objOrganization.DescriptionTig = (string)dTable.Rows[0]["DescriptionTig"];
                    if (dTable.Rows[0]["DescriptionTigSort"].Equals(DBNull.Value))
                        objOrganization.DescriptionTigSort = string.Empty;
                    else
                        objOrganization.DescriptionTigSort = (string)dTable.Rows[0]["DescriptionTigSort"];
                    if (dTable.Rows[0]["DescriptionOr"].Equals(DBNull.Value))
                        objOrganization.DescriptionOr = string.Empty;
                    else
                        objOrganization.DescriptionOr = (string)dTable.Rows[0]["DescriptionOr"];
                    if (dTable.Rows[0]["DescriptionAf"].Equals(DBNull.Value))
                        objOrganization.DescriptionAf = string.Empty;
                    else
                        objOrganization.DescriptionAf = (string)dTable.Rows[0]["DescriptionAf"];
                    if (dTable.Rows[0]["DescriptionSom"].Equals(DBNull.Value))
                        objOrganization.DescriptionSom = string.Empty;
                    else
                        objOrganization.DescriptionSom = (string)dTable.Rows[0]["DescriptionSom"];
                    objOrganization.YearEstablished = (int)dTable.Rows[0]["YearEstablished"];
                    objOrganization.ParentAdministrationCode = (string)dTable.Rows[0]["ParentAdministrationCode"];
                    objOrganization.OrgType = (string)dTable.Rows[0]["OrgType"];
                    objOrganization.OrgHistory = (string)dTable.Rows[0]["OrgHistory"];
                    objOrganization.IsActive = (bool)dTable.Rows[0]["IsActive"];
                    objOrganization.FileNumber = (string)dTable.Rows[0]["FileNumber"];
                    if (dTable.Rows[0]["Telephone"].Equals(DBNull.Value))
                        objOrganization.Telephone = string.Empty;
                    else
                        objOrganization.Telephone = (string)dTable.Rows[0]["Telephone"];
                    if (dTable.Rows[0]["POBox"].Equals(DBNull.Value))
                        objOrganization.POBox = string.Empty;
                    else
                        objOrganization.POBox = (string)dTable.Rows[0]["POBox"];
                    if (dTable.Rows[0]["EMail"].Equals(DBNull.Value))
                        objOrganization.EMail = string.Empty;
                    else
                        objOrganization.EMail = (string)dTable.Rows[0]["EMail"];
                    if (dTable.Rows[0]["WebURL"].Equals(DBNull.Value))
                        objOrganization.WebURL = string.Empty;
                    else
                        objOrganization.WebURL = (string)dTable.Rows[0]["WebURL"];
                    if (dTable.Rows[0]["SiteName"].Equals(DBNull.Value))
                        objOrganization.SiteName = string.Empty;
                    else
                        objOrganization.SiteName = (string)dTable.Rows[0]["SiteName"];
                    if (dTable.Rows[0]["Logo"].Equals(DBNull.Value))
                        objOrganization.Logo = null;
                    else
                        objOrganization.Logo = (byte[])dTable.Rows[0]["Logo"];
                    if (dTable.Rows[0]["UserName"].Equals(DBNull.Value))
                        objOrganization.UserName = string.Empty;
                    else
                        objOrganization.UserName = (string)dTable.Rows[0]["UserName"];
                    if (dTable.Rows[0]["EventDatetime"].Equals(DBNull.Value))
                        objOrganization.EventDatetime = DateTime.MinValue;
                    else
                        objOrganization.EventDatetime = (DateTime)dTable.Rows[0]["EventDatetime"];
                    if (dTable.Rows[0]["UpdatedUsername"].Equals(DBNull.Value))
                        objOrganization.UpdatedUsername = string.Empty;
                    else
                        objOrganization.UpdatedUsername = (string)dTable.Rows[0]["UpdatedUsername"];
                    if (dTable.Rows[0]["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objOrganization.UpdatedEventDatetime = DateTime.MinValue;
                    else
                        objOrganization.UpdatedEventDatetime = (DateTime)dTable.Rows[0]["UpdatedEventDatetime"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Organization::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objOrganization;
        }

        public bool Insert(Organization objOrganization)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[Organization]
                                            ([OrgGuid],[Code],[Description],[DescriptionAm],[DescriptionAmSort],[DescriptionTig],[DescriptionTigSort],[DescriptionOr],[DescriptionAf],[DescriptionSom],[YearEstablished],[ParentAdministrationCode],[OrgType],[OrgHistory],[IsActive],[FileNumber],[Telephone],[POBox],[EMail],[WebURL],[SiteName],[Logo],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime])
                                     VALUES    ( ISNULL(@OrgGuid, (newid())),@Code,@Description,@DescriptionAm,@DescriptionAmSort,@DescriptionTig,@DescriptionTigSort,@DescriptionOr,@DescriptionAf,@DescriptionSom, ISNULL(@YearEstablished, ((1900))), ISNULL(@ParentAdministrationCode, ((99))), ISNULL(@OrgType, ((0))), ISNULL(@OrgHistory, (N'-')), ISNULL(@IsActive, ((1))), ISNULL(@FileNumber, (N'-')),@Telephone,@POBox,@EMail,@WebURL,@SiteName,@Logo,@UserName,@EventDatetime,@UpdatedUsername,@UpdatedEventDatetime)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@OrgGuid", objOrganization.OrgGuid));
                command.Parameters.Add(new SqlParameter("@Code", objOrganization.Code));
                command.Parameters.Add(new SqlParameter("@Description", objOrganization.Description));
                command.Parameters.Add(new SqlParameter("@DescriptionAm", objOrganization.DescriptionAm));
                command.Parameters.Add(new SqlParameter("@DescriptionAmSort", objOrganization.DescriptionAmSort));
                command.Parameters.Add(new SqlParameter("@DescriptionTig", objOrganization.DescriptionTig));
                command.Parameters.Add(new SqlParameter("@DescriptionTigSort", objOrganization.DescriptionTigSort));
                command.Parameters.Add(new SqlParameter("@DescriptionOr", objOrganization.DescriptionOr));
                command.Parameters.Add(new SqlParameter("@DescriptionAf", objOrganization.DescriptionAf));
                command.Parameters.Add(new SqlParameter("@DescriptionSom", objOrganization.DescriptionSom));
                command.Parameters.Add(new SqlParameter("@YearEstablished", objOrganization.YearEstablished));
                command.Parameters.Add(new SqlParameter("@ParentAdministrationCode", objOrganization.ParentAdministrationCode));
                command.Parameters.Add(new SqlParameter("@OrgType", objOrganization.OrgType));
                command.Parameters.Add(new SqlParameter("@OrgHistory", objOrganization.OrgHistory));
                command.Parameters.Add(new SqlParameter("@IsActive", objOrganization.IsActive));
                command.Parameters.Add(new SqlParameter("@FileNumber", objOrganization.FileNumber));
                command.Parameters.Add(new SqlParameter("@Telephone", objOrganization.Telephone));
                command.Parameters.Add(new SqlParameter("@POBox", objOrganization.POBox));
                command.Parameters.Add(new SqlParameter("@EMail", objOrganization.EMail));
                command.Parameters.Add(new SqlParameter("@WebURL", objOrganization.WebURL));
                command.Parameters.Add(new SqlParameter("@SiteName", objOrganization.SiteName));
                command.Parameters.Add(new SqlParameter("@Logo", objOrganization.Logo));
                command.Parameters.Add(new SqlParameter("@UserName", objOrganization.UserName));
                command.Parameters.Add(new SqlParameter("@EventDatetime", objOrganization.EventDatetime));
                command.Parameters.Add(new SqlParameter("@UpdatedUsername", objOrganization.UpdatedUsername));
                command.Parameters.Add(new SqlParameter("@UpdatedEventDatetime", objOrganization.UpdatedEventDatetime));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Organization::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(Organization objOrganization)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[Organization] SET     [OrgGuid]=@OrgGuid,    [Code]=@Code,    [Description]=@Description,    [DescriptionAm]=@DescriptionAm,    [DescriptionAmSort]=@DescriptionAmSort,    [DescriptionTig]=@DescriptionTig,    [DescriptionTigSort]=@DescriptionTigSort,    [DescriptionOr]=@DescriptionOr,    [DescriptionAf]=@DescriptionAf,    [DescriptionSom]=@DescriptionSom,    [YearEstablished]=@YearEstablished,    [ParentAdministrationCode]=@ParentAdministrationCode,    [OrgType]=@OrgType,    [OrgHistory]=@OrgHistory,    [IsActive]=@IsActive,    [FileNumber]=@FileNumber,    [Telephone]=@Telephone,    [POBox]=@POBox,    [EMail]=@EMail,    [WebURL]=@WebURL,    [SiteName]=@SiteName,    [Logo]=@Logo,    [UserName]=@UserName,    [EventDatetime]=@EventDatetime,    [UpdatedUsername]=@UpdatedUsername,    [UpdatedEventDatetime]=@UpdatedEventDatetime WHERE [OrgGuid]=@OrgGuid";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@OrgGuid", objOrganization.OrgGuid));
                command.Parameters.Add(new SqlParameter("@Code", objOrganization.Code));
                command.Parameters.Add(new SqlParameter("@Description", objOrganization.Description));
                command.Parameters.Add(new SqlParameter("@DescriptionAm", objOrganization.DescriptionAm));
                command.Parameters.Add(new SqlParameter("@DescriptionAmSort", objOrganization.DescriptionAmSort));
                command.Parameters.Add(new SqlParameter("@DescriptionTig", objOrganization.DescriptionTig));
                command.Parameters.Add(new SqlParameter("@DescriptionTigSort", objOrganization.DescriptionTigSort));
                command.Parameters.Add(new SqlParameter("@DescriptionOr", objOrganization.DescriptionOr));
                command.Parameters.Add(new SqlParameter("@DescriptionAf", objOrganization.DescriptionAf));
                command.Parameters.Add(new SqlParameter("@DescriptionSom", objOrganization.DescriptionSom));
                command.Parameters.Add(new SqlParameter("@YearEstablished", objOrganization.YearEstablished));
                command.Parameters.Add(new SqlParameter("@ParentAdministrationCode", objOrganization.ParentAdministrationCode));
                command.Parameters.Add(new SqlParameter("@OrgType", objOrganization.OrgType));
                command.Parameters.Add(new SqlParameter("@OrgHistory", objOrganization.OrgHistory));
                command.Parameters.Add(new SqlParameter("@IsActive", objOrganization.IsActive));
                command.Parameters.Add(new SqlParameter("@FileNumber", objOrganization.FileNumber));
                command.Parameters.Add(new SqlParameter("@Telephone", objOrganization.Telephone));
                command.Parameters.Add(new SqlParameter("@POBox", objOrganization.POBox));
                command.Parameters.Add(new SqlParameter("@EMail", objOrganization.EMail));
                command.Parameters.Add(new SqlParameter("@WebURL", objOrganization.WebURL));
                command.Parameters.Add(new SqlParameter("@SiteName", objOrganization.SiteName));
                command.Parameters.Add(new SqlParameter("@Logo", objOrganization.Logo));
                command.Parameters.Add(new SqlParameter("@UserName", objOrganization.UserName));
                command.Parameters.Add(new SqlParameter("@EventDatetime", objOrganization.EventDatetime));
                command.Parameters.Add(new SqlParameter("@UpdatedUsername", objOrganization.UpdatedUsername));
                command.Parameters.Add(new SqlParameter("@UpdatedEventDatetime", objOrganization.UpdatedEventDatetime));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Organization::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[Organization] WHERE [OrgGuid]=@OrgGuid";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@OrgGuid", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Organization::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public List<Organization> GetActiveOrganizations()
        {
            List<Organization> RecordsList = new List<Organization>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [OrgGuid],[Code],[Description],[DescriptionAm],[DescriptionAmSort],[DescriptionTig],[DescriptionTigSort],[DescriptionOr],[DescriptionAf],[DescriptionSom],[YearEstablished],[ParentAdministrationCode],[OrgType],[OrgHistory],[IsActive],[FileNumber],[Telephone],[POBox],[EMail],[WebURL],[SiteName],[Logo],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime] FROM [dbo].[Organization]
                                       where IsActive=1  ORDER BY  [DescriptionAmSort] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Organization objOrganization = new Organization();
                    if (dr["OrgGuid"].Equals(DBNull.Value))
                        objOrganization.OrgGuid = Guid.Empty;
                    else
                        objOrganization.OrgGuid = (Guid)dr["OrgGuid"];
                    if (dr["Code"].Equals(DBNull.Value))
                        objOrganization.Code = string.Empty;
                    else
                        objOrganization.Code = (string)dr["Code"];
                    if (dr["Description"].Equals(DBNull.Value))
                        objOrganization.Description = string.Empty;
                    else
                        objOrganization.Description = (string)dr["Description"];
                    if (dr["DescriptionAm"].Equals(DBNull.Value))
                        objOrganization.DescriptionAm = string.Empty;
                    else
                        objOrganization.DescriptionAm = (string)dr["DescriptionAm"];
                    if (dr["DescriptionAmSort"].Equals(DBNull.Value))
                        objOrganization.DescriptionAmSort = string.Empty;
                    else
                        objOrganization.DescriptionAmSort = (string)dr["DescriptionAmSort"];
                    if (dr["DescriptionTig"].Equals(DBNull.Value))
                        objOrganization.DescriptionTig = string.Empty;
                    else
                        objOrganization.DescriptionTig = (string)dr["DescriptionTig"];
                    if (dr["DescriptionTigSort"].Equals(DBNull.Value))
                        objOrganization.DescriptionTigSort = string.Empty;
                    else
                        objOrganization.DescriptionTigSort = (string)dr["DescriptionTigSort"];
                    if (dr["DescriptionOr"].Equals(DBNull.Value))
                        objOrganization.DescriptionOr = string.Empty;
                    else
                        objOrganization.DescriptionOr = (string)dr["DescriptionOr"];
                    if (dr["DescriptionAf"].Equals(DBNull.Value))
                        objOrganization.DescriptionAf = string.Empty;
                    else
                        objOrganization.DescriptionAf = (string)dr["DescriptionAf"];
                    if (dr["DescriptionSom"].Equals(DBNull.Value))
                        objOrganization.DescriptionSom = string.Empty;
                    else
                        objOrganization.DescriptionSom = (string)dr["DescriptionSom"];
                    if (dr["YearEstablished"].Equals(DBNull.Value))
                        objOrganization.YearEstablished = 0;
                    else
                        objOrganization.YearEstablished = (int)dr["YearEstablished"];
                    if (dr["ParentAdministrationCode"].Equals(DBNull.Value))
                        objOrganization.ParentAdministrationCode = string.Empty;
                    else
                        objOrganization.ParentAdministrationCode = (string)dr["ParentAdministrationCode"];
                    if (dr["OrgType"].Equals(DBNull.Value))
                        objOrganization.OrgType = string.Empty;
                    else
                        objOrganization.OrgType = (string)dr["OrgType"];
                    if (dr["OrgHistory"].Equals(DBNull.Value))
                        objOrganization.OrgHistory = string.Empty;
                    else
                        objOrganization.OrgHistory = (string)dr["OrgHistory"];
                    if (dr["IsActive"].Equals(DBNull.Value))
                        objOrganization.IsActive = false;
                    else
                        objOrganization.IsActive = (bool)dr["IsActive"];
                    if (dr["FileNumber"].Equals(DBNull.Value))
                        objOrganization.FileNumber = string.Empty;
                    else
                        objOrganization.FileNumber = (string)dr["FileNumber"];
                    if (dr["Telephone"].Equals(DBNull.Value))
                        objOrganization.Telephone = string.Empty;
                    else
                        objOrganization.Telephone = (string)dr["Telephone"];
                    if (dr["POBox"].Equals(DBNull.Value))
                        objOrganization.POBox = string.Empty;
                    else
                        objOrganization.POBox = (string)dr["POBox"];
                    if (dr["EMail"].Equals(DBNull.Value))
                        objOrganization.EMail = string.Empty;
                    else
                        objOrganization.EMail = (string)dr["EMail"];
                    if (dr["WebURL"].Equals(DBNull.Value))
                        objOrganization.WebURL = string.Empty;
                    else
                        objOrganization.WebURL = (string)dr["WebURL"];
                    if (dr["SiteName"].Equals(DBNull.Value))
                        objOrganization.SiteName = string.Empty;
                    else
                        objOrganization.SiteName = (string)dr["SiteName"];
                    if (dr["Logo"].Equals(DBNull.Value))
                        objOrganization.Logo = null;
                    else
                        objOrganization.Logo = (byte[])dr["Logo"];
                    if (dr["UserName"].Equals(DBNull.Value))
                        objOrganization.UserName = string.Empty;
                    else
                        objOrganization.UserName = (string)dr["UserName"];
                    if (dr["EventDatetime"].Equals(DBNull.Value))
                        objOrganization.EventDatetime = DateTime.MinValue;
                    else
                        objOrganization.EventDatetime = (DateTime)dr["EventDatetime"];
                    if (dr["UpdatedUsername"].Equals(DBNull.Value))
                        objOrganization.UpdatedUsername = string.Empty;
                    else
                        objOrganization.UpdatedUsername = (string)dr["UpdatedUsername"];
                    if (dr["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objOrganization.UpdatedEventDatetime = DateTime.MinValue;
                    else
                        objOrganization.UpdatedEventDatetime = (DateTime)dr["UpdatedEventDatetime"];
                    RecordsList.Add(objOrganization);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Organization::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public List<Organization> GetActiveOrganizationsForInspectionTeam(Guid orgGuid)
        {
            List<Organization> RecordsList = new List<Organization>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"select * from (SELECT [OrgGuid],[Code],[Description],[DescriptionAm] FROM [dbo].[Organization]
                                       where IsActive=1 and InspectedBy='" + orgGuid + @"' 
                                       union 
                                       Select[OrgGuid],[Code],[Description],[DescriptionAm] FROM [dbo].[Organization]
                                       where [OrgGuid]='"+ orgGuid + @"')x  where x.[OrgGuid] 
                                       not in (select tblTeamInOrganization.OrgGuid from tblTeamInOrganization)" ;

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Organization objOrganization = new Organization();
                    if (dr["OrgGuid"].Equals(DBNull.Value))
                        objOrganization.OrgGuid = Guid.Empty;
                    else
                        objOrganization.OrgGuid = (Guid)dr["OrgGuid"];
                    if (dr["Code"].Equals(DBNull.Value))
                        objOrganization.Code = string.Empty;
                    else
                        objOrganization.Code = (string)dr["Code"];
                    if (dr["Description"].Equals(DBNull.Value))
                        objOrganization.Description = string.Empty;
                    else
                        objOrganization.Description = (string)dr["Description"];
                    if (dr["DescriptionAm"].Equals(DBNull.Value))
                        objOrganization.DescriptionAm = string.Empty;
                    else
                        objOrganization.DescriptionAm = (string)dr["DescriptionAm"];
                    RecordsList.Add(objOrganization);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Organization::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public List<Organization> GetActiveOrganizationsForInspection(Guid orgGuid)
        {
            List<Organization> RecordsList = new List<Organization>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [OrgGuid],[Code],[Description],[DescriptionAm] FROM [dbo].[Organization]
                                       where IsActive=1 and InspectedBy='" + orgGuid + @"' 
                                       union 
                                       Select[OrgGuid],[Code],[Description],[DescriptionAm] FROM [dbo].[Organization]
                                       where [OrgGuid]='" + orgGuid + "'";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Organization objOrganization = new Organization();
                    if (dr["OrgGuid"].Equals(DBNull.Value))
                        objOrganization.OrgGuid = Guid.Empty;
                    else
                        objOrganization.OrgGuid = (Guid)dr["OrgGuid"];
                    if (dr["Code"].Equals(DBNull.Value))
                        objOrganization.Code = string.Empty;
                    else
                        objOrganization.Code = (string)dr["Code"];
                    if (dr["Description"].Equals(DBNull.Value))
                        objOrganization.Description = string.Empty;
                    else
                        objOrganization.Description = (string)dr["Description"];
                    if (dr["DescriptionAm"].Equals(DBNull.Value))
                        objOrganization.DescriptionAm = string.Empty;
                    else
                        objOrganization.DescriptionAm = (string)dr["DescriptionAm"];
                    RecordsList.Add(objOrganization);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Organization::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public List<Organization> GetList()
        {
            List<Organization> RecordsList = new List<Organization>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

//            string strGetAllRecords = @"SELECT [OrgGuid],[Code],[Description],[DescriptionAm],[DescriptionAmSort],[DescriptionTig],[DescriptionTigSort],[DescriptionOr],[DescriptionAf],[DescriptionSom],[YearEstablished],[ParentAdministrationCode],[OrgType],[OrgHistory],[IsActive],[FileNumber],[Telephone],[POBox],[EMail],[WebURL],[SiteName],[Logo],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime] FROM [dbo].[Organization]
//                                       where [OrgGuid] not in (select OrgGuid from tblTeamInOrganization)  ORDER BY  [OrgGuid] ASC";
            string strGetAllRecords = @"SELECT [OrgGuid],[Code],[Description],[DescriptionAm],[DescriptionAmSort],[DescriptionTig],[DescriptionTigSort],[DescriptionOr],[DescriptionAf],[DescriptionSom],[YearEstablished],[ParentAdministrationCode],[OrgType],[OrgHistory],[IsActive],[FileNumber],[Telephone],[POBox],[EMail],[WebURL],[SiteName],[Logo],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime] FROM [dbo].[Organization]
                                          ORDER BY  [OrgGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Organization objOrganization = new Organization();
                    if (dr["OrgGuid"].Equals(DBNull.Value))
                        objOrganization.OrgGuid = Guid.Empty;
                    else
                        objOrganization.OrgGuid = (Guid)dr["OrgGuid"];
                    if (dr["Code"].Equals(DBNull.Value))
                        objOrganization.Code = string.Empty;
                    else
                        objOrganization.Code = (string)dr["Code"];
                    if (dr["Description"].Equals(DBNull.Value))
                        objOrganization.Description = string.Empty;
                    else
                        objOrganization.Description = (string)dr["Description"];
                    if (dr["DescriptionAm"].Equals(DBNull.Value))
                        objOrganization.DescriptionAm = string.Empty;
                    else
                        objOrganization.DescriptionAm = (string)dr["DescriptionAm"];
                    if (dr["DescriptionAmSort"].Equals(DBNull.Value))
                        objOrganization.DescriptionAmSort = string.Empty;
                    else
                        objOrganization.DescriptionAmSort = (string)dr["DescriptionAmSort"];
                    if (dr["DescriptionTig"].Equals(DBNull.Value))
                        objOrganization.DescriptionTig = string.Empty;
                    else
                        objOrganization.DescriptionTig = (string)dr["DescriptionTig"];
                    if (dr["DescriptionTigSort"].Equals(DBNull.Value))
                        objOrganization.DescriptionTigSort = string.Empty;
                    else
                        objOrganization.DescriptionTigSort = (string)dr["DescriptionTigSort"];
                    if (dr["DescriptionOr"].Equals(DBNull.Value))
                        objOrganization.DescriptionOr = string.Empty;
                    else
                        objOrganization.DescriptionOr = (string)dr["DescriptionOr"];
                    if (dr["DescriptionAf"].Equals(DBNull.Value))
                        objOrganization.DescriptionAf = string.Empty;
                    else
                        objOrganization.DescriptionAf = (string)dr["DescriptionAf"];
                    if (dr["DescriptionSom"].Equals(DBNull.Value))
                        objOrganization.DescriptionSom = string.Empty;
                    else
                        objOrganization.DescriptionSom = (string)dr["DescriptionSom"];
                    if (dr["YearEstablished"].Equals(DBNull.Value))
                        objOrganization.YearEstablished = 0;
                    else
                        objOrganization.YearEstablished = (int)dr["YearEstablished"];
                    if (dr["ParentAdministrationCode"].Equals(DBNull.Value))
                        objOrganization.ParentAdministrationCode = string.Empty;
                    else
                        objOrganization.ParentAdministrationCode = (string)dr["ParentAdministrationCode"];
                    if (dr["OrgType"].Equals(DBNull.Value))
                        objOrganization.OrgType = string.Empty;
                    else
                        objOrganization.OrgType = (string)dr["OrgType"];
                    if (dr["OrgHistory"].Equals(DBNull.Value))
                        objOrganization.OrgHistory = string.Empty;
                    else
                        objOrganization.OrgHistory = (string)dr["OrgHistory"];
                    if (dr["IsActive"].Equals(DBNull.Value))
                        objOrganization.IsActive = false;
                    else
                        objOrganization.IsActive = (bool)dr["IsActive"];
                    if (dr["FileNumber"].Equals(DBNull.Value))
                        objOrganization.FileNumber = string.Empty;
                    else
                        objOrganization.FileNumber = (string)dr["FileNumber"];
                    if (dr["Telephone"].Equals(DBNull.Value))
                        objOrganization.Telephone = string.Empty;
                    else
                        objOrganization.Telephone = (string)dr["Telephone"];
                    if (dr["POBox"].Equals(DBNull.Value))
                        objOrganization.POBox = string.Empty;
                    else
                        objOrganization.POBox = (string)dr["POBox"];
                    if (dr["EMail"].Equals(DBNull.Value))
                        objOrganization.EMail = string.Empty;
                    else
                        objOrganization.EMail = (string)dr["EMail"];
                    if (dr["WebURL"].Equals(DBNull.Value))
                        objOrganization.WebURL = string.Empty;
                    else
                        objOrganization.WebURL = (string)dr["WebURL"];
                    if (dr["SiteName"].Equals(DBNull.Value))
                        objOrganization.SiteName = string.Empty;
                    else
                        objOrganization.SiteName = (string)dr["SiteName"];
                    if (dr["Logo"].Equals(DBNull.Value))
                        objOrganization.Logo = null;
                    else
                        objOrganization.Logo = (byte[])dr["Logo"];
                    if (dr["UserName"].Equals(DBNull.Value))
                        objOrganization.UserName = string.Empty;
                    else
                        objOrganization.UserName = (string)dr["UserName"];
                    if (dr["EventDatetime"].Equals(DBNull.Value))
                        objOrganization.EventDatetime = DateTime.MinValue;
                    else
                        objOrganization.EventDatetime = (DateTime)dr["EventDatetime"];
                    if (dr["UpdatedUsername"].Equals(DBNull.Value))
                        objOrganization.UpdatedUsername = string.Empty;
                    else
                        objOrganization.UpdatedUsername = (string)dr["UpdatedUsername"];
                    if (dr["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objOrganization.UpdatedEventDatetime = DateTime.MinValue;
                    else
                        objOrganization.UpdatedEventDatetime = (DateTime)dr["UpdatedEventDatetime"];
                    RecordsList.Add(objOrganization);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Organization::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public List<Organization> GetListOrg(Guid OrganizationGuid)
        {
            List<Organization> RecordsList = new List<Organization>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
            string strGetAllRecords = "select *  from organization where orgGuid='" + OrganizationGuid.ToString() + "'"; 
            SqlCommand command = new SqlCommand
            {
                CommandText = strGetAllRecords,
                CommandType = CommandType.Text,
                Connection = connection
            };

            try
            {
                command.Parameters.Add(new SqlParameter("@OrgGuid", OrganizationGuid));
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Organization objOrganization = new Organization();
                    if (dr["OrgGuid"].Equals(DBNull.Value))
                        objOrganization.OrgGuid = Guid.Empty;
                    else
                        objOrganization.OrgGuid = (Guid)dr["OrgGuid"];
                    if (dr["InspectedBy"].Equals(DBNull.Value))
                        objOrganization.InspectedBy = string.Empty;
                    else
                        objOrganization.InspectedBy = (string)dr["InspectedBy"];
                    if (dr["Code"].Equals(DBNull.Value))
                        objOrganization.Code = string.Empty;
                    else
                        objOrganization.Code = (string)dr["Code"];
                    if (dr["Description"].Equals(DBNull.Value))
                        objOrganization.Description = string.Empty;
                    else
                        objOrganization.Description = (string)dr["Description"];
                    if (dr["DescriptionAm"].Equals(DBNull.Value))
                        objOrganization.DescriptionAm = string.Empty;
                    else
                        objOrganization.DescriptionAm = (string)dr["DescriptionAm"];
                    if (dr["DescriptionAmSort"].Equals(DBNull.Value))
                        objOrganization.DescriptionAmSort = string.Empty;
                    else
                        objOrganization.DescriptionAmSort = (string)dr["DescriptionAmSort"];
                    if (dr["DescriptionTig"].Equals(DBNull.Value))
                        objOrganization.DescriptionTig = string.Empty;
                    else
                        objOrganization.DescriptionTig = (string)dr["DescriptionTig"];
                    if (dr["DescriptionTigSort"].Equals(DBNull.Value))
                        objOrganization.DescriptionTigSort = string.Empty;
                    else
                        objOrganization.DescriptionTigSort = (string)dr["DescriptionTigSort"];
                    if (dr["DescriptionOr"].Equals(DBNull.Value))
                        objOrganization.DescriptionOr = string.Empty;
                    else
                        objOrganization.DescriptionOr = (string)dr["DescriptionOr"];
                    if (dr["DescriptionAf"].Equals(DBNull.Value))
                        objOrganization.DescriptionAf = string.Empty;
                    else
                        objOrganization.DescriptionAf = (string)dr["DescriptionAf"];
                    if (dr["DescriptionSom"].Equals(DBNull.Value))
                        objOrganization.DescriptionSom = string.Empty;
                    else
                        objOrganization.DescriptionSom = (string)dr["DescriptionSom"];
                    if (dr["YearEstablished"].Equals(DBNull.Value))
                        objOrganization.YearEstablished = 0;
                    else
                        objOrganization.YearEstablished = (int)dr["YearEstablished"];
                    if (dr["ParentAdministrationCode"].Equals(DBNull.Value))
                        objOrganization.ParentAdministrationCode = string.Empty;
                    else
                        objOrganization.ParentAdministrationCode = (string)dr["ParentAdministrationCode"];
                    if (dr["OrgType"].Equals(DBNull.Value))
                        objOrganization.OrgType = string.Empty;
                    else
                        objOrganization.OrgType = (string)dr["OrgType"];
                    if (dr["OrgHistory"].Equals(DBNull.Value))
                        objOrganization.OrgHistory = string.Empty;
                    else
                        objOrganization.OrgHistory = (string)dr["OrgHistory"];
                    if (dr["IsActive"].Equals(DBNull.Value))
                        objOrganization.IsActive = false;
                    else
                        objOrganization.IsActive = (bool)dr["IsActive"];
                    if (dr["FileNumber"].Equals(DBNull.Value))
                        objOrganization.FileNumber = string.Empty;
                    else
                        objOrganization.FileNumber = (string)dr["FileNumber"];
                    if (dr["Telephone"].Equals(DBNull.Value))
                        objOrganization.Telephone = string.Empty;
                    else
                        objOrganization.Telephone = (string)dr["Telephone"];
                    if (dr["POBox"].Equals(DBNull.Value))
                        objOrganization.POBox = string.Empty;
                    else
                        objOrganization.POBox = (string)dr["POBox"];
                    if (dr["EMail"].Equals(DBNull.Value))
                        objOrganization.EMail = string.Empty;
                    else
                        objOrganization.EMail = (string)dr["EMail"];
                    if (dr["WebURL"].Equals(DBNull.Value))
                        objOrganization.WebURL = string.Empty;
                    else
                        objOrganization.WebURL = (string)dr["WebURL"];
                    if (dr["SiteName"].Equals(DBNull.Value))
                        objOrganization.SiteName = string.Empty;
                    else
                        objOrganization.SiteName = (string)dr["SiteName"];
                    if (dr["Logo"].Equals(DBNull.Value))
                        objOrganization.Logo = null;
                    else
                        objOrganization.Logo = (byte[])dr["Logo"];
                    if (dr["UserName"].Equals(DBNull.Value))
                        objOrganization.UserName = string.Empty;
                    else
                        objOrganization.UserName = (string)dr["UserName"];
                    if (dr["EventDatetime"].Equals(DBNull.Value))
                        objOrganization.EventDatetime = DateTime.MinValue;
                    else
                        objOrganization.EventDatetime = (DateTime)dr["EventDatetime"];
                    if (dr["UpdatedUsername"].Equals(DBNull.Value))
                        objOrganization.UpdatedUsername = string.Empty;
                    else
                        objOrganization.UpdatedUsername = (string)dr["UpdatedUsername"];
                    if (dr["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objOrganization.UpdatedEventDatetime = DateTime.MinValue;
                    else
                        objOrganization.UpdatedEventDatetime = (DateTime)dr["UpdatedEventDatetime"];
                    RecordsList.Add(objOrganization);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Organization::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public List<Organization> GetListOrg(Guid OrganizationGuid, string OrganizationCatagory,int OrganizationLanguage)
        {
            List<Organization> RecordsList = new List<Organization>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = "[dbo].[sp_GetOrganization]";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.StoredProcedure };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@OrganizationGuid", OrganizationGuid));
                command.Parameters.Add(new SqlParameter("@OrganizationCatagory", OrganizationCatagory));
                command.Parameters.Add(new SqlParameter("@OrganizationLanguage", OrganizationLanguage));
                
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Organization objOrganization = new Organization();
                    if (dr["OrgGuid"].Equals(DBNull.Value))
                        objOrganization.OrgGuid = Guid.Empty;
                    else
                        objOrganization.OrgGuid = (Guid)dr["OrgGuid"];
                    if (dr["Code"].Equals(DBNull.Value))
                        objOrganization.Code = string.Empty;
                    else
                        objOrganization.Code = (string)dr["Code"];
                    if (dr["Description"].Equals(DBNull.Value))
                        objOrganization.Description = string.Empty;
                    else
                        objOrganization.Description = (string)dr["Description"];
                    if (dr["DescriptionAm"].Equals(DBNull.Value))
                        objOrganization.DescriptionAm = string.Empty;
                    else
                        objOrganization.DescriptionAm = (string)dr["DescriptionAm"];
                    if (dr["DescriptionAmSort"].Equals(DBNull.Value))
                        objOrganization.DescriptionAmSort = string.Empty;
                    else
                        objOrganization.DescriptionAmSort = (string)dr["DescriptionAmSort"];
                    if (dr["DescriptionTig"].Equals(DBNull.Value))
                        objOrganization.DescriptionTig = string.Empty;
                    else
                        objOrganization.DescriptionTig = (string)dr["DescriptionTig"];
                    if (dr["DescriptionTigSort"].Equals(DBNull.Value))
                        objOrganization.DescriptionTigSort = string.Empty;
                    else
                        objOrganization.DescriptionTigSort = (string)dr["DescriptionTigSort"];
                    if (dr["DescriptionOr"].Equals(DBNull.Value))
                        objOrganization.DescriptionOr = string.Empty;
                    else
                        objOrganization.DescriptionOr = (string)dr["DescriptionOr"];
                    if (dr["DescriptionAf"].Equals(DBNull.Value))
                        objOrganization.DescriptionAf = string.Empty;
                    else
                        objOrganization.DescriptionAf = (string)dr["DescriptionAf"];
                    if (dr["DescriptionSom"].Equals(DBNull.Value))
                        objOrganization.DescriptionSom = string.Empty;
                    else
                        objOrganization.DescriptionSom = (string)dr["DescriptionSom"];
                    if (dr["YearEstablished"].Equals(DBNull.Value))
                        objOrganization.YearEstablished = 0;
                    else
                        objOrganization.YearEstablished = (int)dr["YearEstablished"];
                    if (dr["ParentAdministrationCode"].Equals(DBNull.Value))
                        objOrganization.ParentAdministrationCode = string.Empty;
                    else
                        objOrganization.ParentAdministrationCode = (string)dr["ParentAdministrationCode"];
                    if (dr["OrgType"].Equals(DBNull.Value))
                        objOrganization.OrgType = string.Empty;
                    else
                        objOrganization.OrgType = (string)dr["OrgType"];
                    if (dr["OrgHistory"].Equals(DBNull.Value))
                        objOrganization.OrgHistory = string.Empty;
                    else
                        objOrganization.OrgHistory = (string)dr["OrgHistory"];
                    if (dr["IsActive"].Equals(DBNull.Value))
                        objOrganization.IsActive = false;
                    else
                        objOrganization.IsActive = (bool)dr["IsActive"];
                    if (dr["FileNumber"].Equals(DBNull.Value))
                        objOrganization.FileNumber = string.Empty;
                    else
                        objOrganization.FileNumber = (string)dr["FileNumber"];
                    if (dr["Telephone"].Equals(DBNull.Value))
                        objOrganization.Telephone = string.Empty;
                    else
                        objOrganization.Telephone = (string)dr["Telephone"];
                    if (dr["POBox"].Equals(DBNull.Value))
                        objOrganization.POBox = string.Empty;
                    else
                        objOrganization.POBox = (string)dr["POBox"];
                    if (dr["EMail"].Equals(DBNull.Value))
                        objOrganization.EMail = string.Empty;
                    else
                        objOrganization.EMail = (string)dr["EMail"];
                    if (dr["WebURL"].Equals(DBNull.Value))
                        objOrganization.WebURL = string.Empty;
                    else
                        objOrganization.WebURL = (string)dr["WebURL"];
                    if (dr["SiteName"].Equals(DBNull.Value))
                        objOrganization.SiteName = string.Empty;
                    else
                        objOrganization.SiteName = (string)dr["SiteName"];
                    if (dr["Logo"].Equals(DBNull.Value))
                        objOrganization.Logo = null;
                    else
                        objOrganization.Logo = (byte[])dr["Logo"];
                    if (dr["UserName"].Equals(DBNull.Value))
                        objOrganization.UserName = string.Empty;
                    else
                        objOrganization.UserName = (string)dr["UserName"];
                    if (dr["EventDatetime"].Equals(DBNull.Value))
                        objOrganization.EventDatetime = DateTime.MinValue;
                    else
                        objOrganization.EventDatetime = (DateTime)dr["EventDatetime"];
                    if (dr["UpdatedUsername"].Equals(DBNull.Value))
                        objOrganization.UpdatedUsername = string.Empty;
                    else
                        objOrganization.UpdatedUsername = (string)dr["UpdatedUsername"];
                    if (dr["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objOrganization.UpdatedEventDatetime = DateTime.MinValue;
                    else
                        objOrganization.UpdatedEventDatetime = (DateTime)dr["UpdatedEventDatetime"];
                    RecordsList.Add(objOrganization);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Organization::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public bool Exists(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT OrgGuid FROM [dbo].[Organization] WHERE [OrgGuid]=@OrgGuid";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@OrgGuid", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("Organization::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public DataTable GetOrganizationByOrgGuid(int orgGuid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [DescriptionAm] FROM [Organization] WHERE [OrgGuid]='" + orgGuid + "' ORDER BY [OrgGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Organization");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Organization::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public DataTable GetOrgnaizations()
        {
            DataTable dtOrganizations = new DataTable("Organization");
            SqlConnection connection = new SqlConnection(ConnectionString);
            string strGetAllRecords = @"SELECT [OrgGuid],[DescriptionAm] FROM [dbo].[Organization]  ORDER BY  [OrgGuid] ASC";
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;
            SqlDataAdapter daOrganizations = new SqlDataAdapter(command);
            try
            {
                connection.Open();
                daOrganizations.Fill(dtOrganizations);
            }
            catch (Exception ex)
            {
                throw new Exception("Organization::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }

            return dtOrganizations;
        }
    }
}