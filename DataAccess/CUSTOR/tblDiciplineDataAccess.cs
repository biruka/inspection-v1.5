﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblDiciplineDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords(Guid person)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[date_action_taken], [break_type] ,[break_description],[action_taken],[remark],[date_lifted] FROM [dbo].[tblDicipline] where ParentGuid=@ParentGuid  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblDicipline");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ParentGuid", person));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblDicipline::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public tblDicipline GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblDicipline objtblDicipline = new tblDicipline();
            string strGetRecord = @"SELECT [MainGuid],[seq_no],[ParentGuid],[date_action_taken],[break_type],[break_description],[action_taken],[remark],[date_lifted] FROM [dbo].[tblDicipline] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblDicipline");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblDicipline.MainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    objtblDicipline.Seq_no = (int)dTable.Rows[0]["seq_no"];
                    if (dTable.Rows[0]["ParentGuid"].Equals(DBNull.Value))
                        objtblDicipline.ParentGuid = Guid.Empty;
                    else
                        objtblDicipline.ParentGuid = (Guid)dTable.Rows[0]["ParentGuid"];
                    if (dTable.Rows[0]["date_action_taken"].Equals(DBNull.Value))
                        objtblDicipline.Date_action_taken = DateTime.MinValue;
                    else
                        objtblDicipline.Date_action_taken = (DateTime)dTable.Rows[0]["date_action_taken"];
                    if (dTable.Rows[0]["break_type"].Equals(DBNull.Value))
                        objtblDicipline.Break_type = string.Empty;
                    else
                        objtblDicipline.Break_type = (string)dTable.Rows[0]["break_type"];
                    if (dTable.Rows[0]["break_description"].Equals(DBNull.Value))
                        objtblDicipline.Break_description = string.Empty;
                    else
                        objtblDicipline.Break_description = (string)dTable.Rows[0]["break_description"];
                    if (dTable.Rows[0]["action_taken"].Equals(DBNull.Value))
                        objtblDicipline.Action_taken = string.Empty;
                    else
                        objtblDicipline.Action_taken = (string)dTable.Rows[0]["action_taken"];
                    if (dTable.Rows[0]["remark"].Equals(DBNull.Value))
                        objtblDicipline.Remark = string.Empty;
                    else
                        objtblDicipline.Remark = (string)dTable.Rows[0]["remark"];
                    if (dTable.Rows[0]["date_lifted"].Equals(DBNull.Value))
                        objtblDicipline.Date_lifted = DateTime.MinValue;
                    else
                        objtblDicipline.Date_lifted = (DateTime)dTable.Rows[0]["date_lifted"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblDicipline::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblDicipline;
        }

       
        public List<tblDicipline> GetList()
        {
            List<tblDicipline> RecordsList = new List<tblDicipline>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[date_action_taken],[break_type],[break_description],[action_taken],[remark],[date_lifted] FROM [dbo].[tblDicipline]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblDicipline objtblDicipline = new tblDicipline();
                    if (dr["MainGuid"].Equals(DBNull.Value))
                        objtblDicipline.MainGuid = Guid.Empty;
                    else
                        objtblDicipline.MainGuid = (Guid)dr["MainGuid"];
                    if (dr["seq_no"].Equals(DBNull.Value))
                        objtblDicipline.Seq_no = 0;
                    else
                        objtblDicipline.Seq_no = (int)dr["seq_no"];
                    if (dr["ParentGuid"].Equals(DBNull.Value))
                        objtblDicipline.ParentGuid = Guid.Empty;
                    else
                        objtblDicipline.ParentGuid = (Guid)dr["ParentGuid"];
                    if (dr["date_action_taken"].Equals(DBNull.Value))
                        objtblDicipline.Date_action_taken = DateTime.MinValue;
                    else
                        objtblDicipline.Date_action_taken = (DateTime)dr["date_action_taken"];
                    if (dr["break_type"].Equals(DBNull.Value))
                        objtblDicipline.Break_type = string.Empty;
                    else
                        objtblDicipline.Break_type = (string)dr["break_type"];
                    if (dr["break_description"].Equals(DBNull.Value))
                        objtblDicipline.Break_description = string.Empty;
                    else
                        objtblDicipline.Break_description = (string)dr["break_description"];
                    if (dr["action_taken"].Equals(DBNull.Value))
                        objtblDicipline.Action_taken = string.Empty;
                    else
                        objtblDicipline.Action_taken = (string)dr["action_taken"];
                    if (dr["remark"].Equals(DBNull.Value))
                        objtblDicipline.Remark = string.Empty;
                    else
                        objtblDicipline.Remark = (string)dr["remark"];
                    if (dr["date_lifted"].Equals(DBNull.Value))
                        objtblDicipline.Date_lifted = DateTime.MinValue;
                    else
                        objtblDicipline.Date_lifted = (DateTime)dr["date_lifted"];
                    RecordsList.Add(objtblDicipline);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblDicipline::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


       
    }
}