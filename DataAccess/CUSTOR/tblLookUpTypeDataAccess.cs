﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblLookUpTypeDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [code],[description],[amdescription],[id],[Tigrigna],[AfanOromo],[Afar],[Somali],[Arabic] FROM [dbo].[tblLookUpType] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblLookUpType");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblLookUpType::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public tblLookUpType GetRecord(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblLookUpType objtblLookUpType = new tblLookUpType();
            string strGetRecord = @"SELECT [code],[description],[amdescription],[id],[Tigrigna],[AfanOromo],[Afar],[Somali],[Arabic] FROM [dbo].[tblLookUpType] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblLookUpType");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    if (dTable.Rows[0]["code"].Equals(DBNull.Value))
                        objtblLookUpType.Code = string.Empty;
                    else
                        objtblLookUpType.Code = (string)dTable.Rows[0]["code"];
                    if (dTable.Rows[0]["description"].Equals(DBNull.Value))
                        objtblLookUpType.Description = string.Empty;
                    else
                        objtblLookUpType.Description = (string)dTable.Rows[0]["description"];
                    if (dTable.Rows[0]["amdescription"].Equals(DBNull.Value))
                        objtblLookUpType.Amdescription = string.Empty;
                    else
                        objtblLookUpType.Amdescription = (string)dTable.Rows[0]["amdescription"];
                    objtblLookUpType.Id = (int)dTable.Rows[0]["id"];
                    if (dTable.Rows[0]["Tigrigna"].Equals(DBNull.Value))
                        objtblLookUpType.Tigrigna = string.Empty;
                    else
                        objtblLookUpType.Tigrigna = (string)dTable.Rows[0]["Tigrigna"];
                    if (dTable.Rows[0]["AfanOromo"].Equals(DBNull.Value))
                        objtblLookUpType.AfanOromo = string.Empty;
                    else
                        objtblLookUpType.AfanOromo = (string)dTable.Rows[0]["AfanOromo"];
                    if (dTable.Rows[0]["Afar"].Equals(DBNull.Value))
                        objtblLookUpType.Afar = string.Empty;
                    else
                        objtblLookUpType.Afar = (string)dTable.Rows[0]["Afar"];
                    if (dTable.Rows[0]["Somali"].Equals(DBNull.Value))
                        objtblLookUpType.Somali = string.Empty;
                    else
                        objtblLookUpType.Somali = (string)dTable.Rows[0]["Somali"];
                    if (dTable.Rows[0]["Arabic"].Equals(DBNull.Value))
                        objtblLookUpType.Arabic = string.Empty;
                    else
                        objtblLookUpType.Arabic = (string)dTable.Rows[0]["Arabic"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblLookUpType::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblLookUpType;
        }

        public bool Insert(tblLookUpType objtblLookUpType)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[tblLookUpType]
                                            ([code],[description],[amdescription],[Tigrigna],[AfanOromo],[Afar],[Somali],[Arabic])
                                     VALUES    (@code,@description,@amdescription,@Tigrigna,@AfanOromo,@Afar,@Somali,@Arabic)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@code", objtblLookUpType.Code));
                command.Parameters.Add(new SqlParameter("@description", objtblLookUpType.Description));
                command.Parameters.Add(new SqlParameter("@amdescription", objtblLookUpType.Amdescription));
                command.Parameters.Add(new SqlParameter("@Tigrigna", objtblLookUpType.Tigrigna));
                command.Parameters.Add(new SqlParameter("@AfanOromo", objtblLookUpType.AfanOromo));
                command.Parameters.Add(new SqlParameter("@Afar", objtblLookUpType.Afar));
                command.Parameters.Add(new SqlParameter("@Somali", objtblLookUpType.Somali));
                command.Parameters.Add(new SqlParameter("@Arabic", objtblLookUpType.Arabic));
                command.Parameters.Add(new SqlParameter("@id", objtblLookUpType.Id));
                command.Parameters["@id"].Direction = ParameterDirection.Output;


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                objtblLookUpType.Id = (int)command.Parameters["@id"].Value;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblLookUpType::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(tblLookUpType objtblLookUpType)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[tblLookUpType] SET     [code]=@code,    [description]=@description,    [amdescription]=@amdescription,    [Tigrigna]=@Tigrigna,    [AfanOromo]=@AfanOromo,    [Afar]=@Afar,    [Somali]=@Somali,    [Arabic]=@Arabic ";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@code", objtblLookUpType.Code));
                command.Parameters.Add(new SqlParameter("@description", objtblLookUpType.Description));
                command.Parameters.Add(new SqlParameter("@amdescription", objtblLookUpType.Amdescription));
                command.Parameters.Add(new SqlParameter("@id", objtblLookUpType.Id));
                command.Parameters.Add(new SqlParameter("@Tigrigna", objtblLookUpType.Tigrigna));
                command.Parameters.Add(new SqlParameter("@AfanOromo", objtblLookUpType.AfanOromo));
                command.Parameters.Add(new SqlParameter("@Afar", objtblLookUpType.Afar));
                command.Parameters.Add(new SqlParameter("@Somali", objtblLookUpType.Somali));
                command.Parameters.Add(new SqlParameter("@Arabic", objtblLookUpType.Arabic));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblLookUpType::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblLookUpType] ";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblLookUpType::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<tblLookUpType> GetList()
        {
            List<tblLookUpType> RecordsList = new List<tblLookUpType>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [code],[description],[amdescription],[id],[Tigrigna],[AfanOromo],[Afar],[Somali],[Arabic] FROM [dbo].[tblLookUpType] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblLookUpType objtblLookUpType = new tblLookUpType();
                    if (dr["code"].Equals(DBNull.Value))
                        objtblLookUpType.Code = string.Empty;
                    else
                        objtblLookUpType.Code = (string)dr["code"];
                    if (dr["description"].Equals(DBNull.Value))
                        objtblLookUpType.Description = string.Empty;
                    else
                        objtblLookUpType.Description = (string)dr["description"];
                    if (dr["amdescription"].Equals(DBNull.Value))
                        objtblLookUpType.Amdescription = string.Empty;
                    else
                        objtblLookUpType.Amdescription = (string)dr["amdescription"];
                    if (dr["id"].Equals(DBNull.Value))
                        objtblLookUpType.Id = 0;
                    else
                        objtblLookUpType.Id = (int)dr["id"];
                    if (dr["Tigrigna"].Equals(DBNull.Value))
                        objtblLookUpType.Tigrigna = string.Empty;
                    else
                        objtblLookUpType.Tigrigna = (string)dr["Tigrigna"];
                    if (dr["AfanOromo"].Equals(DBNull.Value))
                        objtblLookUpType.AfanOromo = string.Empty;
                    else
                        objtblLookUpType.AfanOromo = (string)dr["AfanOromo"];
                    if (dr["Afar"].Equals(DBNull.Value))
                        objtblLookUpType.Afar = string.Empty;
                    else
                        objtblLookUpType.Afar = (string)dr["Afar"];
                    if (dr["Somali"].Equals(DBNull.Value))
                        objtblLookUpType.Somali = string.Empty;
                    else
                        objtblLookUpType.Somali = (string)dr["Somali"];
                    if (dr["Arabic"].Equals(DBNull.Value))
                        objtblLookUpType.Arabic = string.Empty;
                    else
                        objtblLookUpType.Arabic = (string)dr["Arabic"];
                    RecordsList.Add(objtblLookUpType);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblLookUpType::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT  FROM [dbo].[tblLookUpType] ";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("tblLookUpType::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}