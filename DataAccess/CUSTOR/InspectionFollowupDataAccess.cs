﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class InspectionFollowupDataAccess
    {

        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public DataTable GetRecords(Guid person)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [FollowupGUID],(SELECT  EmployeeGUID FROM  Inspection where InspectionGUID=InspectionFollowup.InspectionGUID) as EmployeeGUID,[InspectionGUID],[FollowupDate],[FollowupStatus],[ActionTaken],[Remark],[UserName],[EventDatetime],[UpdatedUsername],[approvalname],[UpdatedEventDatetime],[Isvalid],[Filename],[reanswer] FROM [dbo].[InspectionFollowup] where (SELECT  EmployeeGUID FROM  Inspection where InspectionGUID=InspectionFollowup.InspectionGUID)=@EmployeeGUID ORDER BY  [FollowupGUID] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("InspectionFollowup");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@EmployeeGUID", person));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("InspectionFollowup::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public DataTable GetRecordsByDocumentGuid(Guid documentGuid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT * FROM [dbo].[InspectionFollowup] where InspectionFollowup.DocGuid = @DocGUID ORDER BY  [FollowupGUID] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("InspectionFollowup");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@DocGUID", documentGuid));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("InspectionFollowup::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public List<InspectionFollowup> GetLists(Guid ID)
        {
            List<InspectionFollowup> RecordsList = new List<InspectionFollowup>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [FollowupGUID],[InspectionGUID],[FollowupDate],[FollowupStatus],[ActionTaken],[Remark],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime],[Isvalid],[Filename] FROM [dbo].[InspectionFollowup]  where [InspectionGUID]=@InspectionGUID ORDER BY  [FollowupGUID] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@InspectionGUID", ID));
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    InspectionFollowup objInspectionFollowup = new InspectionFollowup();
                    if (dr["FollowupGUID"].Equals(DBNull.Value))
                        objInspectionFollowup.FollowupGUID = Guid.Empty;
                    else
                        objInspectionFollowup.FollowupGUID = (Guid)dr["FollowupGUID"];
                    //id
                    if (dr["FollowupGUID"].Equals(DBNull.Value))
                        objInspectionFollowup.id = Guid.Empty;
                    else
                        objInspectionFollowup.id = (Guid)dr["FollowupGUID"];


                    if (dr["InspectionGUID"].Equals(DBNull.Value))
                        objInspectionFollowup.InspectionGUID = Guid.Empty;
                    else
                        objInspectionFollowup.InspectionGUID = (Guid)dr["InspectionGUID"];
                    if (dr["FollowupDate"].Equals(DBNull.Value))
                        objInspectionFollowup.FollowupDate = DateTime.MinValue;
                    else
                        objInspectionFollowup.FollowupDate = (DateTime)dr["FollowupDate"];
                    if (dr["FollowupStatus"].Equals(DBNull.Value))
                        objInspectionFollowup.FollowupStatus = 0;
                    else
                        objInspectionFollowup.FollowupStatus = (int)dr["FollowupStatus"];
                    if (dr["ActionTaken"].Equals(DBNull.Value))
                        objInspectionFollowup.ActionTaken = 0;
                    else
                        objInspectionFollowup.ActionTaken = (int)dr["ActionTaken"];
                    if (dr["Remark"].Equals(DBNull.Value))
                        objInspectionFollowup.Remark = string.Empty;
                    else
                        objInspectionFollowup.Remark = (string)dr["Remark"];
                    if (dr["UserName"].Equals(DBNull.Value))
                        objInspectionFollowup.UserName = string.Empty;
                    else
                        objInspectionFollowup.UserName = (string)dr["UserName"];
                    if (dr["EventDatetime"].Equals(DBNull.Value))
                        objInspectionFollowup.EventDatetime = DateTime.MinValue;
                    else
                        objInspectionFollowup.EventDatetime = (DateTime)dr["EventDatetime"];
                    if (dr["UpdatedUsername"].Equals(DBNull.Value))
                        objInspectionFollowup.UpdatedUsername = string.Empty;
                    else
                        objInspectionFollowup.UpdatedUsername = (string)dr["UpdatedUsername"];
                                        
                    if (dr["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objInspectionFollowup.UpdatedEventDatetime = DateTime.MinValue;
                    else
                        objInspectionFollowup.UpdatedEventDatetime = (DateTime)dr["UpdatedEventDatetime"];
                    if (dr["Isvalid"].Equals(DBNull.Value))
                        objInspectionFollowup.Isvalid = false;
                    else
                        objInspectionFollowup.Isvalid = (bool)dr["Isvalid"];
                    if (dr["Filename"].Equals(DBNull.Value))
                        objInspectionFollowup.Filename = string.Empty;
                    else
                        objInspectionFollowup.Filename = (string)dr["Filename"];
                    RecordsList.Add(objInspectionFollowup);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("InspectionFollowup::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public DataTable getEmployeeInspectedOrganizations(Guid personGuid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"select distinct Organization.DescriptionAm,Organization.OrgGuid from 
                                        Organization inner join tblTeamInOrganization inner join 
                                        tblInspectionTeam inner join tblInspectionTeamMembers on tblInspectionTeam.TeamGuid = tblInspectionTeamMembers.TeamGuid   on tblTeamInOrganization.TeamGuid = tblInspectionTeam.TeamGuid  
                                        on Organization.OrgGuid = tblTeamInOrganization.orgGuid 
                                        where tblInspectionTeamMembers.PersonGuid = @personGuid";
            DataTable dt = new DataTable();
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;
            command.Parameters.Add(new SqlParameter("@personGuid", personGuid));
            SqlDataAdapter da = new SqlDataAdapter(command);
            try
            {
                connection.Open();
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception("tblTeamInOrganization::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public InspectionFollowup GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            InspectionFollowup objInspectionFollowup = new InspectionFollowup();
            string strGetRecord = @"SELECT [FollowupGUID],[InspectionGUID],[FollowupDate],[FollowupStatus],[ActionTaken],[Remark],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime],[Isvalid],[Filename] FROM [dbo].[InspectionFollowup] WHERE [InspectionGUID]=@FollowupGUID";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("InspectionFollowup");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@FollowupGUID", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objInspectionFollowup.FollowupGUID = (Guid)dTable.Rows[0]["FollowupGUID"];
                    objInspectionFollowup.InspectionGUID = (Guid)dTable.Rows[0]["InspectionGUID"];
                    objInspectionFollowup.FollowupDate = (DateTime)dTable.Rows[0]["FollowupDate"];
                    objInspectionFollowup.FollowupStatus = (int)dTable.Rows[0]["FollowupStatus"];
                    objInspectionFollowup.ActionTaken = (int)dTable.Rows[0]["ActionTaken"];
                    if (dTable.Rows[0]["Remark"].Equals(DBNull.Value))
                        objInspectionFollowup.Remark = string.Empty;
                    else
                        objInspectionFollowup.Remark = (string)dTable.Rows[0]["Remark"];
                    objInspectionFollowup.UserName = (string)dTable.Rows[0]["UserName"];
                    objInspectionFollowup.EventDatetime = (DateTime)dTable.Rows[0]["EventDatetime"];
                    if (dTable.Rows[0]["UpdatedUsername"].Equals(DBNull.Value))
                        objInspectionFollowup.UpdatedUsername = string.Empty;
                    else
                        objInspectionFollowup.UpdatedUsername = (string)dTable.Rows[0]["UpdatedUsername"];
                    if (dTable.Rows[0]["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objInspectionFollowup.UpdatedEventDatetime = DateTime.MinValue;
                    else
                        objInspectionFollowup.UpdatedEventDatetime = (DateTime)dTable.Rows[0]["UpdatedEventDatetime"];
                    if (dTable.Rows[0]["Isvalid"].Equals(DBNull.Value))
                        objInspectionFollowup.Isvalid = false;
                    else
                        objInspectionFollowup.Isvalid = (bool)dTable.Rows[0]["Isvalid"];
                    if (dTable.Rows[0]["Filename"].Equals(DBNull.Value))
                        objInspectionFollowup.Filename = string.Empty;
                    else
                        objInspectionFollowup.Filename = (string)dTable.Rows[0]["Filename"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("InspectionFollowup::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objInspectionFollowup;
        }

        public bool Insert(InspectionFollowup objInspectionFollowup)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[InspectionFollowup]
                                            ([FollowupGUID],[InspectionGUID],[FollowupDate],[FollowupStatus],[ActionTaken],[Remark],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime],[Isvalid],[Filename],[DocGuid])
                                     VALUES    (@FollowupGUID,@InspectionGUID,@FollowupDate,@FollowupStatus,@ActionTaken,@Remark,@UserName,@EventDatetime,@UpdatedUsername,@UpdatedEventDatetime,@Isvalid,@Filename,@DocGuid)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@FollowupGUID", objInspectionFollowup.FollowupGUID));
                command.Parameters.Add(new SqlParameter("@InspectionGUID", objInspectionFollowup.InspectionGUID));
                command.Parameters.Add(new SqlParameter("@FollowupDate", objInspectionFollowup.FollowupDate));
                command.Parameters.Add(new SqlParameter("@FollowupStatus", objInspectionFollowup.FollowupStatus));
                command.Parameters.Add(new SqlParameter("@ActionTaken", objInspectionFollowup.ActionTaken));
                command.Parameters.Add(new SqlParameter("@Remark", objInspectionFollowup.Remark));
                command.Parameters.Add(new SqlParameter("@UserName", objInspectionFollowup.UserName));
                command.Parameters.Add(new SqlParameter("@EventDatetime", objInspectionFollowup.EventDatetime));
                command.Parameters.Add(new SqlParameter("@UpdatedUsername", objInspectionFollowup.UpdatedUsername));
                command.Parameters.Add(new SqlParameter("@UpdatedEventDatetime", objInspectionFollowup.UpdatedEventDatetime));
                command.Parameters.Add(new SqlParameter("@Isvalid", objInspectionFollowup.Isvalid));
                command.Parameters.Add(new SqlParameter("@Filename", objInspectionFollowup.Filename));
                command.Parameters.Add(new SqlParameter("@DocGuid", objInspectionFollowup.DocGuid));
                


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("InspectionFollowup::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(InspectionFollowup objInspectionFollowup)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[InspectionFollowup] SET     [FollowupGUID]=@FollowupGUID,    [InspectionGUID]=@InspectionGUID,    [FollowupDate]=@FollowupDate,    [FollowupStatus]=@FollowupStatus,    [ActionTaken]=@ActionTaken,    [Remark]=@Remark,    [UserName]=@UserName,    [EventDatetime]=@EventDatetime,    [UpdatedUsername]=@UpdatedUsername,    [UpdatedEventDatetime]=@UpdatedEventDatetime,[reanswer]=@reanswer,[DocGuid]=@DocGuid WHERE [FollowupGUID]=@FollowupGUID";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@FollowupGUID", objInspectionFollowup.FollowupGUID));
                command.Parameters.Add(new SqlParameter("@InspectionGUID", objInspectionFollowup.InspectionGUID));
                command.Parameters.Add(new SqlParameter("@FollowupDate", objInspectionFollowup.FollowupDate));
                command.Parameters.Add(new SqlParameter("@FollowupStatus", objInspectionFollowup.FollowupStatus));
                command.Parameters.Add(new SqlParameter("@ActionTaken", objInspectionFollowup.ActionTaken));
                command.Parameters.Add(new SqlParameter("@Remark", objInspectionFollowup.Remark));
                command.Parameters.Add(new SqlParameter("@UserName", objInspectionFollowup.UserName));
                command.Parameters.Add(new SqlParameter("@EventDatetime", objInspectionFollowup.EventDatetime));
                command.Parameters.Add(new SqlParameter("@UpdatedUsername", objInspectionFollowup.UpdatedUsername));
                command.Parameters.Add(new SqlParameter("@UpdatedEventDatetime", objInspectionFollowup.UpdatedEventDatetime));
                command.Parameters.Add(new SqlParameter("@Isvalid", objInspectionFollowup.Isvalid));
                command.Parameters.Add(new SqlParameter("@Filename", objInspectionFollowup.Filename));
                command.Parameters.Add(new SqlParameter("@reanswer", objInspectionFollowup.Reanswer));
                command.Parameters.Add(new SqlParameter("@DocGuid", objInspectionFollowup.DocGuid));
                


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("InspectionFollowup::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[InspectionFollowup] WHERE [InspectionGUID]=@InspectionGUID";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@InspectionGUID", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("InspectionFollowup::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public List<InspectionFollowup> GetList()
        {
            List<InspectionFollowup> RecordsList = new List<InspectionFollowup>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [FollowupGUID],[InspectionGUID],[FollowupDate],[FollowupStatus],[ActionTaken],[Remark],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime],[Isvalid],[Filename] FROM [dbo].[InspectionFollowup]  ORDER BY  [FollowupGUID] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    InspectionFollowup objInspectionFollowup = new InspectionFollowup();
                    if (dr["FollowupGUID"].Equals(DBNull.Value))
                        objInspectionFollowup.FollowupGUID = Guid.Empty;
                    else
                        objInspectionFollowup.FollowupGUID = (Guid)dr["FollowupGUID"];
                    if (dr["InspectionGUID"].Equals(DBNull.Value))
                        objInspectionFollowup.InspectionGUID = Guid.Empty;
                    else
                        objInspectionFollowup.InspectionGUID = (Guid)dr["InspectionGUID"];
                    if (dr["FollowupDate"].Equals(DBNull.Value))
                        objInspectionFollowup.FollowupDate = DateTime.MinValue;
                    else
                        objInspectionFollowup.FollowupDate = (DateTime)dr["FollowupDate"];
                    if (dr["FollowupStatus"].Equals(DBNull.Value))
                        objInspectionFollowup.FollowupStatus = 0;
                    else
                        objInspectionFollowup.FollowupStatus = (int)dr["FollowupStatus"];
                    if (dr["ActionTaken"].Equals(DBNull.Value))
                        objInspectionFollowup.ActionTaken = 0;
                    else
                        objInspectionFollowup.ActionTaken = (int)dr["ActionTaken"];
                    if (dr["Remark"].Equals(DBNull.Value))
                        objInspectionFollowup.Remark = string.Empty;
                    else
                        objInspectionFollowup.Remark = (string)dr["Remark"];
                    if (dr["UserName"].Equals(DBNull.Value))
                        objInspectionFollowup.UserName = string.Empty;
                    else
                        objInspectionFollowup.UserName = (string)dr["UserName"];
                    if (dr["EventDatetime"].Equals(DBNull.Value))
                        objInspectionFollowup.EventDatetime = DateTime.MinValue;
                    else
                        objInspectionFollowup.EventDatetime = (DateTime)dr["EventDatetime"];
                    if (dr["UpdatedUsername"].Equals(DBNull.Value))
                        objInspectionFollowup.UpdatedUsername = string.Empty;
                    else
                        objInspectionFollowup.UpdatedUsername = (string)dr["UpdatedUsername"];
                    if (dr["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objInspectionFollowup.UpdatedEventDatetime = DateTime.MinValue;
                    else
                        objInspectionFollowup.UpdatedEventDatetime = (DateTime)dr["UpdatedEventDatetime"];
                    if (dr["Isvalid"].Equals(DBNull.Value))
                        objInspectionFollowup.Isvalid = false;
                    else
                        objInspectionFollowup.Isvalid = (bool)dr["Isvalid"];
                    if (dr["Filename"].Equals(DBNull.Value))
                        objInspectionFollowup.Filename = string.Empty;
                    else
                        objInspectionFollowup.Filename = (string)dr["Filename"];
                    RecordsList.Add(objInspectionFollowup);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("InspectionFollowup::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public bool Exists(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT FollowupGUID FROM [dbo].[InspectionFollowup] WHERE [FollowupGUID]=@FollowupGUID";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@FollowupGUID", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("InspectionFollowup::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}