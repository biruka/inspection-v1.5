﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Commen;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblEducationHistoryDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }


        

        public DataTable GetRecords(Guid person, string language)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strFieldDesc = Language.GetVisibleDescriptionColumn(Convert.ToInt32(language));

            string strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[institution],[location], [dbo].fnGetLookupNew([education_type],93,"+ language + @") as [education_type],[period_from],[period_to],[dbo].[fnGetDateDifferentNew] ([period_from],[period_to],0,"+ language +@") as [net_duration],
                                      [dbo].[fnGetFiledofstudyvalueNew]([field_of_study]," + language + @") as [field_of_study],
                                      (Select {0} from tblEducationalLevel  where cast(Fcode as int)=[certificate_obtained]   ) as [certificate_obtained],
                                      [gpa],[remark],[total_cost],[percent_covered],[Country],[Attachment],[NotAttachedReson],[AffirmativeAactionGiven],[AffirmativeDoc_Attached],
                                      [Affir_Doc_NotAttachedReason],[Aff_AactionWomen],[Aff_AactionforHandicap],[Aff_AactionforNationsNationalities],[Aff_Act_NotGivenReason],
                                      [Is_Education_Related_with_Job],[Given_by_the_Org],[other_cost],[budget],[FullAddress],[Sponsor] FROM [dbo].[tblEducationHistory] WHERE [ParentGuid]=@ParentGuid";
            strGetAllRecords = String.Format(strGetAllRecords,strFieldDesc);
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblEducationHistory");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add("@ParentGuid", person);
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblEducationHistory::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public tblEducationHistory GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblEducationHistory objtblEducationHistory = new tblEducationHistory();
            string strGetRecord = @"SELECT [MainGuid],[seq_no],[ParentGuid],[institution],[location],[education_type],[period_from],[period_to],[net_duration],[field_of_study],[certificate_obtained],[gpa],[remark],[total_cost],[percent_covered],[Country],[Attachment],[NotAttachedReson],[AffirmativeAactionGiven],[AffirmativeDoc_Attached],[Affir_Doc_NotAttachedReason],[Aff_AactionWomen],[Aff_AactionforHandicap],[Aff_AactionforNationsNationalities],[Aff_Act_NotGivenReason],[Is_Education_Related_with_Job],[Given_by_the_Org],[other_cost],[budget],[FullAddress],[Sponsor] FROM [dbo].[tblEducationHistory] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblEducationHistory");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblEducationHistory.MainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    objtblEducationHistory.Seq_no = (int)dTable.Rows[0]["seq_no"];
                    if (dTable.Rows[0]["ParentGuid"].Equals(DBNull.Value))
                        objtblEducationHistory.ParentGuid = Guid.Empty;
                    else
                        objtblEducationHistory.ParentGuid = (Guid)dTable.Rows[0]["ParentGuid"];
                    if (dTable.Rows[0]["institution"].Equals(DBNull.Value))
                        objtblEducationHistory.Institution = string.Empty;
                    else
                        objtblEducationHistory.Institution = (string)dTable.Rows[0]["institution"];
                    if (dTable.Rows[0]["location"].Equals(DBNull.Value))
                        objtblEducationHistory.Location = string.Empty;
                    else
                        objtblEducationHistory.Location = (string)dTable.Rows[0]["location"];
                    if (dTable.Rows[0]["education_type"].Equals(DBNull.Value))
                        objtblEducationHistory.Education_type = 0;
                    else
                        objtblEducationHistory.Education_type = (int)dTable.Rows[0]["education_type"];
                    if (dTable.Rows[0]["period_from"].Equals(DBNull.Value))
                        objtblEducationHistory.Period_from = DateTime.MinValue;
                    else
                        objtblEducationHistory.Period_from = (DateTime)dTable.Rows[0]["period_from"];
                    if (dTable.Rows[0]["period_to"].Equals(DBNull.Value))
                        objtblEducationHistory.Period_to = DateTime.MinValue;
                    else
                        objtblEducationHistory.Period_to = (DateTime)dTable.Rows[0]["period_to"];
                    if (dTable.Rows[0]["net_duration"].Equals(DBNull.Value))
                        objtblEducationHistory.Net_duration = string.Empty;
                    else
                        objtblEducationHistory.Net_duration = (string)dTable.Rows[0]["net_duration"];
                    if (dTable.Rows[0]["field_of_study"].Equals(DBNull.Value))
                        objtblEducationHistory.Field_of_study = string.Empty;
                    else
                        objtblEducationHistory.Field_of_study = (string)dTable.Rows[0]["field_of_study"];
                    if (dTable.Rows[0]["certificate_obtained"].Equals(DBNull.Value))
                        objtblEducationHistory.Certificate_obtained = string.Empty;
                    else
                        objtblEducationHistory.Certificate_obtained = (string)dTable.Rows[0]["certificate_obtained"];
                    if (dTable.Rows[0]["gpa"].Equals(DBNull.Value))
                        objtblEducationHistory.Gpa = 0;
                    else
                        objtblEducationHistory.Gpa = (double)dTable.Rows[0]["gpa"];
                    if (dTable.Rows[0]["remark"].Equals(DBNull.Value))
                        objtblEducationHistory.Remark = string.Empty;
                    else
                        objtblEducationHistory.Remark = (string)dTable.Rows[0]["remark"];
                    if (dTable.Rows[0]["total_cost"].Equals(DBNull.Value))
                        objtblEducationHistory.Total_cost = 0;
                    else
                        objtblEducationHistory.Total_cost = (double)dTable.Rows[0]["total_cost"];
                    if (dTable.Rows[0]["percent_covered"].Equals(DBNull.Value))
                        objtblEducationHistory.Percent_covered = 0;
                    else
                        objtblEducationHistory.Percent_covered = (int)dTable.Rows[0]["percent_covered"];
                    if (dTable.Rows[0]["Country"].Equals(DBNull.Value))
                        objtblEducationHistory.Country = 0;
                    else
                        objtblEducationHistory.Country = (int)dTable.Rows[0]["Country"];
                    if (dTable.Rows[0]["Attachment"].Equals(DBNull.Value))
                        objtblEducationHistory.Attachment = false;
                    else
                        objtblEducationHistory.Attachment = (bool)dTable.Rows[0]["Attachment"];
                    if (dTable.Rows[0]["NotAttachedReson"].Equals(DBNull.Value))
                        objtblEducationHistory.NotAttachedReson = string.Empty;
                    else
                        objtblEducationHistory.NotAttachedReson = (string)dTable.Rows[0]["NotAttachedReson"];
                    if (dTable.Rows[0]["AffirmativeAactionGiven"].Equals(DBNull.Value))
                        objtblEducationHistory.AffirmativeAactionGiven = false;
                    else
                        objtblEducationHistory.AffirmativeAactionGiven = (bool)dTable.Rows[0]["AffirmativeAactionGiven"];
                    if (dTable.Rows[0]["AffirmativeDoc_Attached"].Equals(DBNull.Value))
                        objtblEducationHistory.AffirmativeDoc_Attached = false;
                    else
                        objtblEducationHistory.AffirmativeDoc_Attached = (bool)dTable.Rows[0]["AffirmativeDoc_Attached"];
                    if (dTable.Rows[0]["Affir_Doc_NotAttachedReason"].Equals(DBNull.Value))
                        objtblEducationHistory.Affir_Doc_NotAttachedReason = string.Empty;
                    else
                        objtblEducationHistory.Affir_Doc_NotAttachedReason = (string)dTable.Rows[0]["Affir_Doc_NotAttachedReason"];
                    if (dTable.Rows[0]["Aff_AactionWomen"].Equals(DBNull.Value))
                        objtblEducationHistory.Aff_AactionWomen = false;
                    else
                        objtblEducationHistory.Aff_AactionWomen = (bool)dTable.Rows[0]["Aff_AactionWomen"];
                    if (dTable.Rows[0]["Aff_AactionforHandicap"].Equals(DBNull.Value))
                        objtblEducationHistory.Aff_AactionforHandicap = false;
                    else
                        objtblEducationHistory.Aff_AactionforHandicap = (bool)dTable.Rows[0]["Aff_AactionforHandicap"];
                    if (dTable.Rows[0]["Aff_AactionforNationsNationalities"].Equals(DBNull.Value))
                        objtblEducationHistory.Aff_AactionforNationsNationalities = false;
                    else
                        objtblEducationHistory.Aff_AactionforNationsNationalities = (bool)dTable.Rows[0]["Aff_AactionforNationsNationalities"];
                    if (dTable.Rows[0]["Aff_Act_NotGivenReason"].Equals(DBNull.Value))
                        objtblEducationHistory.Aff_Act_NotGivenReason = string.Empty;
                    else
                        objtblEducationHistory.Aff_Act_NotGivenReason = (string)dTable.Rows[0]["Aff_Act_NotGivenReason"];
                    if (dTable.Rows[0]["Is_Education_Related_with_Job"].Equals(DBNull.Value))
                        objtblEducationHistory.Is_Education_Related_with_Job = false;
                    else
                        objtblEducationHistory.Is_Education_Related_with_Job = (bool)dTable.Rows[0]["Is_Education_Related_with_Job"];
                    if (dTable.Rows[0]["Given_by_the_Org"].Equals(DBNull.Value))
                        objtblEducationHistory.Given_by_the_Org = false;
                    else
                        objtblEducationHistory.Given_by_the_Org = (bool)dTable.Rows[0]["Given_by_the_Org"];
                    if (dTable.Rows[0]["other_cost"].Equals(DBNull.Value))
                        objtblEducationHistory.Other_cost = 0;
                    else
                        objtblEducationHistory.Other_cost = (double)dTable.Rows[0]["other_cost"];
                    if (dTable.Rows[0]["budget"].Equals(DBNull.Value))
                        objtblEducationHistory.Budget = 0;
                    else
                        objtblEducationHistory.Budget = (double)dTable.Rows[0]["budget"];
                    if (dTable.Rows[0]["FullAddress"].Equals(DBNull.Value))
                        objtblEducationHistory.FullAddress = string.Empty;
                    else
                        objtblEducationHistory.FullAddress = (string)dTable.Rows[0]["FullAddress"];
                    if (dTable.Rows[0]["Sponsor"].Equals(DBNull.Value))
                        objtblEducationHistory.Sponsor = string.Empty;
                    else
                        objtblEducationHistory.Sponsor = (string)dTable.Rows[0]["Sponsor"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblEducationHistory::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblEducationHistory;
        }

       
        public List<tblEducationHistory> GetList()
        {
            List<tblEducationHistory> RecordsList = new List<tblEducationHistory>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[institution],[location],[education_type],[period_from],[period_to],[net_duration],[field_of_study],[certificate_obtained],[gpa],[remark],[total_cost],[percent_covered],[Country],[Attachment],[NotAttachedReson],[AffirmativeAactionGiven],[AffirmativeDoc_Attached],[Affir_Doc_NotAttachedReason],[Aff_AactionWomen],[Aff_AactionforHandicap],[Aff_AactionforNationsNationalities],[Aff_Act_NotGivenReason],[Is_Education_Related_with_Job],[Given_by_the_Org],[other_cost],[budget],[FullAddress],[Sponsor] FROM [dbo].[tblEducationHistory]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblEducationHistory objtblEducationHistory = new tblEducationHistory();
                    if (dr["MainGuid"].Equals(DBNull.Value))
                        objtblEducationHistory.MainGuid = Guid.Empty;
                    else
                        objtblEducationHistory.MainGuid = (Guid)dr["MainGuid"];
                    if (dr["seq_no"].Equals(DBNull.Value))
                        objtblEducationHistory.Seq_no = 0;
                    else
                        objtblEducationHistory.Seq_no = (int)dr["seq_no"];
                    if (dr["ParentGuid"].Equals(DBNull.Value))
                        objtblEducationHistory.ParentGuid = Guid.Empty;
                    else
                        objtblEducationHistory.ParentGuid = (Guid)dr["ParentGuid"];
                    if (dr["institution"].Equals(DBNull.Value))
                        objtblEducationHistory.Institution = string.Empty;
                    else
                        objtblEducationHistory.Institution = (string)dr["institution"];
                    if (dr["location"].Equals(DBNull.Value))
                        objtblEducationHistory.Location = string.Empty;
                    else
                        objtblEducationHistory.Location = (string)dr["location"];
                    if (dr["education_type"].Equals(DBNull.Value))
                        objtblEducationHistory.Education_type = 0;
                    else
                        objtblEducationHistory.Education_type = (int)dr["education_type"];
                    if (dr["period_from"].Equals(DBNull.Value))
                        objtblEducationHistory.Period_from = DateTime.MinValue;
                    else
                        objtblEducationHistory.Period_from = (DateTime)dr["period_from"];
                    if (dr["period_to"].Equals(DBNull.Value))
                        objtblEducationHistory.Period_to = DateTime.MinValue;
                    else
                        objtblEducationHistory.Period_to = (DateTime)dr["period_to"];
                    if (dr["net_duration"].Equals(DBNull.Value))
                        objtblEducationHistory.Net_duration = string.Empty;
                    else
                        objtblEducationHistory.Net_duration = (string)dr["net_duration"];
                    if (dr["field_of_study"].Equals(DBNull.Value))
                        objtblEducationHistory.Field_of_study = string.Empty;
                    else
                        objtblEducationHistory.Field_of_study = (string)dr["field_of_study"];
                    if (dr["certificate_obtained"].Equals(DBNull.Value))
                        objtblEducationHistory.Certificate_obtained = string.Empty;
                    else
                        objtblEducationHistory.Certificate_obtained = (string)dr["certificate_obtained"];
                    if (dr["gpa"].Equals(DBNull.Value))
                        objtblEducationHistory.Gpa = 0;
                    else
                        objtblEducationHistory.Gpa = (double)dr["gpa"];
                    if (dr["remark"].Equals(DBNull.Value))
                        objtblEducationHistory.Remark = string.Empty;
                    else
                        objtblEducationHistory.Remark = (string)dr["remark"];
                    if (dr["total_cost"].Equals(DBNull.Value))
                        objtblEducationHistory.Total_cost = 0;
                    else
                        objtblEducationHistory.Total_cost = (double)dr["total_cost"];
                    if (dr["percent_covered"].Equals(DBNull.Value))
                        objtblEducationHistory.Percent_covered = 0;
                    else
                        objtblEducationHistory.Percent_covered = (int)dr["percent_covered"];
                    if (dr["Country"].Equals(DBNull.Value))
                        objtblEducationHistory.Country = 0;
                    else
                        objtblEducationHistory.Country = (int)dr["Country"];
                    if (dr["Attachment"].Equals(DBNull.Value))
                        objtblEducationHistory.Attachment = false;
                    else
                        objtblEducationHistory.Attachment = (bool)dr["Attachment"];
                    if (dr["NotAttachedReson"].Equals(DBNull.Value))
                        objtblEducationHistory.NotAttachedReson = string.Empty;
                    else
                        objtblEducationHistory.NotAttachedReson = (string)dr["NotAttachedReson"];
                    if (dr["AffirmativeAactionGiven"].Equals(DBNull.Value))
                        objtblEducationHistory.AffirmativeAactionGiven = false;
                    else
                        objtblEducationHistory.AffirmativeAactionGiven = (bool)dr["AffirmativeAactionGiven"];
                    if (dr["AffirmativeDoc_Attached"].Equals(DBNull.Value))
                        objtblEducationHistory.AffirmativeDoc_Attached = false;
                    else
                        objtblEducationHistory.AffirmativeDoc_Attached = (bool)dr["AffirmativeDoc_Attached"];
                    if (dr["Affir_Doc_NotAttachedReason"].Equals(DBNull.Value))
                        objtblEducationHistory.Affir_Doc_NotAttachedReason = string.Empty;
                    else
                        objtblEducationHistory.Affir_Doc_NotAttachedReason = (string)dr["Affir_Doc_NotAttachedReason"];
                    if (dr["Aff_AactionWomen"].Equals(DBNull.Value))
                        objtblEducationHistory.Aff_AactionWomen = false;
                    else
                        objtblEducationHistory.Aff_AactionWomen = (bool)dr["Aff_AactionWomen"];
                    if (dr["Aff_AactionforHandicap"].Equals(DBNull.Value))
                        objtblEducationHistory.Aff_AactionforHandicap = false;
                    else
                        objtblEducationHistory.Aff_AactionforHandicap = (bool)dr["Aff_AactionforHandicap"];
                    if (dr["Aff_AactionforNationsNationalities"].Equals(DBNull.Value))
                        objtblEducationHistory.Aff_AactionforNationsNationalities = false;
                    else
                        objtblEducationHistory.Aff_AactionforNationsNationalities = (bool)dr["Aff_AactionforNationsNationalities"];
                    if (dr["Aff_Act_NotGivenReason"].Equals(DBNull.Value))
                        objtblEducationHistory.Aff_Act_NotGivenReason = string.Empty;
                    else
                        objtblEducationHistory.Aff_Act_NotGivenReason = (string)dr["Aff_Act_NotGivenReason"];
                    if (dr["Is_Education_Related_with_Job"].Equals(DBNull.Value))
                        objtblEducationHistory.Is_Education_Related_with_Job = false;
                    else
                        objtblEducationHistory.Is_Education_Related_with_Job = (bool)dr["Is_Education_Related_with_Job"];
                    if (dr["Given_by_the_Org"].Equals(DBNull.Value))
                        objtblEducationHistory.Given_by_the_Org = false;
                    else
                        objtblEducationHistory.Given_by_the_Org = (bool)dr["Given_by_the_Org"];
                    if (dr["other_cost"].Equals(DBNull.Value))
                        objtblEducationHistory.Other_cost = 0;
                    else
                        objtblEducationHistory.Other_cost = (double)dr["other_cost"];
                    if (dr["budget"].Equals(DBNull.Value))
                        objtblEducationHistory.Budget = 0;
                    else
                        objtblEducationHistory.Budget = (double)dr["budget"];
                    if (dr["FullAddress"].Equals(DBNull.Value))
                        objtblEducationHistory.FullAddress = string.Empty;
                    else
                        objtblEducationHistory.FullAddress = (string)dr["FullAddress"];
                    if (dr["Sponsor"].Equals(DBNull.Value))
                        objtblEducationHistory.Sponsor = string.Empty;
                    else
                        objtblEducationHistory.Sponsor = (string)dr["Sponsor"];
                    RecordsList.Add(objtblEducationHistory);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblEducationHistory::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT MainGuid FROM [dbo].[tblEducationHistory] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("tblEducationHistory::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}