﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Commen;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblPromotionHistoryDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords(Guid person, int DocumentType)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            string strGetAllRecords = "";
            if (DocumentType == 312)
            {
                strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[from_salary],[to_salary],[dbo].[fnGetjobtitle] (from_job_title) as [from_job_title],[dbo].[fnGetjobtitle] (to_job_title) as [to_job_title],[date_promoted],
                                       [promotion_type] as promotion_type ,[remark],[from_rank],[to_rank],[from_allowance],[to_allowance],[dbo].[fnGetGrade] (from_grade) as [from_grade],[dbo].[fnGetGrade] (to_grade) as [to_grade],[RankGuid],[RankGuidFrom],[dbo].[fnGetPensionDate](ParentGuid,date_promoted) as PensionDate,
                                        [dbo].[fnGetGradefromposition](from_job_title) As oldGrade,[dbo].[fnGetGradefromposition](to_job_title) As newGrade 
                                        FROM [dbo].[tblPromotionHistory] 
                                        where ParentGuid=@ParentGuid  ORDER BY  [date_promoted] DESC";
            }
            else if (DocumentType == 321)
            {
                strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[from_salary],[to_salary],[dbo].[fnGetjobtitle] (from_job_title) as [from_job_title],[dbo].[fnGetjobtitle] (to_job_title) as [to_job_title],[date_promoted],
                                      [promotion_type] as promotion_type ,[remark],[from_rank],[to_rank],[from_allowance],[to_allowance],[dbo].[fnGetGrade] (from_grade) as [from_grade],[dbo].[fnGetGrade] (to_grade) as [to_grade],[RankGuid],[RankGuidFrom],[dbo].[fnGetPensionDate](ParentGuid,date_promoted) as PensionDate,
                                        [dbo].[fnGetGradefromposition](from_job_title) As oldGrade,[dbo].[fnGetGradefromposition](to_job_title) As newGrade 
                                        FROM [dbo].[tblPromotionHistory] 
                                        where ParentGuid=@ParentGuid and [promotion_type]=4  ORDER BY  [date_promoted] DESC";
            }
            else if (DocumentType == 341)
            {
                strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[from_salary],[to_salary],[dbo].[fnGetjobtitle] (from_job_title) as [from_job_title],[dbo].[fnGetjobtitle] (to_job_title) as [to_job_title],[date_promoted],
                                      [promotion_type] as promotion_type ,[remark],[from_rank],[to_rank],[from_allowance],[to_allowance],[dbo].[fnGetGrade] (from_grade) as [from_grade],[dbo].[fnGetGrade] (to_grade) as [to_grade],[RankGuid],[RankGuidFrom],[dbo].[fnGetPensionDate](ParentGuid,date_promoted) as PensionDate,
                                        [dbo].[fnGetGradefromposition](from_job_title) As oldGrade,[dbo].[fnGetGradefromposition](to_job_title) As newGrade 
                                        FROM [dbo].[tblPromotionHistory] 
                                        where ParentGuid=@ParentGuid and [promotion_type]=6 ORDER BY  [date_promoted] DESC";
            }
            else 
            {
                return null;
            }
             
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblPromotionHistory");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ParentGuid", person));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblPromotionHistory::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public tblPromotionHistory GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblPromotionHistory objtblPromotionHistory = new tblPromotionHistory();
            string strGetRecord = @"SELECT [MainGuid],[seq_no],[ParentGuid],[from_salary],[to_salary],[from_job_title],[to_job_title],[date_promoted],[promotion_type],[remark],[from_rank],[to_rank],[from_allowance],[to_allowance],[from_grade],[to_grade],[RankGuid],[RankGuidFrom] FROM [dbo].[tblPromotionHistory] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblPromotionHistory");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblPromotionHistory.MainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    objtblPromotionHistory.Seq_no = (int)dTable.Rows[0]["seq_no"];
                    if (dTable.Rows[0]["ParentGuid"].Equals(DBNull.Value))
                        objtblPromotionHistory.ParentGuid = Guid.Empty;
                    else
                        objtblPromotionHistory.ParentGuid = (Guid)dTable.Rows[0]["ParentGuid"];
                    if (dTable.Rows[0]["from_salary"].Equals(DBNull.Value))
                        objtblPromotionHistory.From_salary = 0;
                    else
                        objtblPromotionHistory.From_salary = (double)dTable.Rows[0]["from_salary"];
                    if (dTable.Rows[0]["to_salary"].Equals(DBNull.Value))
                        objtblPromotionHistory.To_salary = 0;
                    else
                        objtblPromotionHistory.To_salary = (double)dTable.Rows[0]["to_salary"];
                    if (dTable.Rows[0]["from_job_title"].Equals(DBNull.Value))
                        objtblPromotionHistory.From_job_title = string.Empty;
                    else
                        objtblPromotionHistory.From_job_title = (string)dTable.Rows[0]["from_job_title"];
                    if (dTable.Rows[0]["to_job_title"].Equals(DBNull.Value))
                        objtblPromotionHistory.To_job_title = string.Empty;
                    else
                        objtblPromotionHistory.To_job_title = (string)dTable.Rows[0]["to_job_title"];
                    if (dTable.Rows[0]["date_promoted"].Equals(DBNull.Value))
                        objtblPromotionHistory.Date_promoted = DateTime.MinValue;
                    else
                        objtblPromotionHistory.Date_promoted = (DateTime)dTable.Rows[0]["date_promoted"];
                    if (dTable.Rows[0]["promotion_type"].Equals(DBNull.Value))
                        objtblPromotionHistory.Promotion_type = 0;
                    else
                        objtblPromotionHistory.Promotion_type = (int)dTable.Rows[0]["promotion_type"];
                    if (dTable.Rows[0]["remark"].Equals(DBNull.Value))
                        objtblPromotionHistory.Remark = string.Empty;
                    else
                        objtblPromotionHistory.Remark = (string)dTable.Rows[0]["remark"];
                    if (dTable.Rows[0]["from_rank"].Equals(DBNull.Value))
                        objtblPromotionHistory.From_rank = string.Empty;
                    else
                        objtblPromotionHistory.From_rank = (string)dTable.Rows[0]["from_rank"];
                    if (dTable.Rows[0]["to_rank"].Equals(DBNull.Value))
                        objtblPromotionHistory.To_rank = string.Empty;
                    else
                        objtblPromotionHistory.To_rank = (string)dTable.Rows[0]["to_rank"];
                    if (dTable.Rows[0]["from_allowance"].Equals(DBNull.Value))
                        objtblPromotionHistory.From_allowance = 0;
                    else
                        objtblPromotionHistory.From_allowance = (double)dTable.Rows[0]["from_allowance"];
                    if (dTable.Rows[0]["to_allowance"].Equals(DBNull.Value))
                        objtblPromotionHistory.To_allowance = 0;
                    else
                        objtblPromotionHistory.To_allowance = (double)dTable.Rows[0]["to_allowance"];
                    if (dTable.Rows[0]["from_grade"].Equals(DBNull.Value))
                        objtblPromotionHistory.From_grade = 0;
                    else
                        objtblPromotionHistory.From_grade = (int)dTable.Rows[0]["from_grade"];
                    if (dTable.Rows[0]["to_grade"].Equals(DBNull.Value))
                        objtblPromotionHistory.To_grade = 0;
                    else
                        objtblPromotionHistory.To_grade = (int)dTable.Rows[0]["to_grade"];
                    if (dTable.Rows[0]["RankGuid"].Equals(DBNull.Value))
                        objtblPromotionHistory.RankGuid = Guid.Empty;
                    else
                        objtblPromotionHistory.RankGuid = (Guid)dTable.Rows[0]["RankGuid"];
                    if (dTable.Rows[0]["RankGuidFrom"].Equals(DBNull.Value))
                        objtblPromotionHistory.RankGuidFrom = Guid.Empty;
                    else
                        objtblPromotionHistory.RankGuidFrom = (Guid)dTable.Rows[0]["RankGuidFrom"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblPromotionHistory::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblPromotionHistory;
        }

       
        public List<tblPromotionHistory> GetList()
        {
            List<tblPromotionHistory> RecordsList = new List<tblPromotionHistory>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[from_salary],[to_salary],[from_job_title],[to_job_title],[date_promoted],[promotion_type],[remark],[from_rank],[to_rank],[from_allowance],[to_allowance],[from_grade],[to_grade],[RankGuid],[RankGuidFrom] FROM [dbo].[tblPromotionHistory]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblPromotionHistory objtblPromotionHistory = new tblPromotionHistory();
                    if (dr["MainGuid"].Equals(DBNull.Value))
                        objtblPromotionHistory.MainGuid = Guid.Empty;
                    else
                        objtblPromotionHistory.MainGuid = (Guid)dr["MainGuid"];
                    if (dr["seq_no"].Equals(DBNull.Value))
                        objtblPromotionHistory.Seq_no = 0;
                    else
                        objtblPromotionHistory.Seq_no = (int)dr["seq_no"];
                    if (dr["ParentGuid"].Equals(DBNull.Value))
                        objtblPromotionHistory.ParentGuid = Guid.Empty;
                    else
                        objtblPromotionHistory.ParentGuid = (Guid)dr["ParentGuid"];
                    if (dr["from_salary"].Equals(DBNull.Value))
                        objtblPromotionHistory.From_salary = 0;
                    else
                        objtblPromotionHistory.From_salary = (double)dr["from_salary"];
                    if (dr["to_salary"].Equals(DBNull.Value))
                        objtblPromotionHistory.To_salary = 0;
                    else
                        objtblPromotionHistory.To_salary = (double)dr["to_salary"];
                    if (dr["from_job_title"].Equals(DBNull.Value))
                        objtblPromotionHistory.From_job_title = string.Empty;
                    else
                        objtblPromotionHistory.From_job_title = (string)dr["from_job_title"];
                    if (dr["to_job_title"].Equals(DBNull.Value))
                        objtblPromotionHistory.To_job_title = string.Empty;
                    else
                        objtblPromotionHistory.To_job_title = (string)dr["to_job_title"];
                    if (dr["date_promoted"].Equals(DBNull.Value))
                        objtblPromotionHistory.Date_promoted = DateTime.MinValue;
                    else
                        objtblPromotionHistory.Date_promoted = (DateTime)dr["date_promoted"];
                    if (dr["promotion_type"].Equals(DBNull.Value))
                        objtblPromotionHistory.Promotion_type = 0;
                    else
                        objtblPromotionHistory.Promotion_type = (int)dr["promotion_type"];
                    if (dr["remark"].Equals(DBNull.Value))
                        objtblPromotionHistory.Remark = string.Empty;
                    else
                        objtblPromotionHistory.Remark = (string)dr["remark"];
                    if (dr["from_rank"].Equals(DBNull.Value))
                        objtblPromotionHistory.From_rank = string.Empty;
                    else
                        objtblPromotionHistory.From_rank = (string)dr["from_rank"];
                    if (dr["to_rank"].Equals(DBNull.Value))
                        objtblPromotionHistory.To_rank = string.Empty;
                    else
                        objtblPromotionHistory.To_rank = (string)dr["to_rank"];
                    if (dr["from_allowance"].Equals(DBNull.Value))
                        objtblPromotionHistory.From_allowance = 0;
                    else
                        objtblPromotionHistory.From_allowance = (double)dr["from_allowance"];
                    if (dr["to_allowance"].Equals(DBNull.Value))
                        objtblPromotionHistory.To_allowance = 0;
                    else
                        objtblPromotionHistory.To_allowance = (double)dr["to_allowance"];
                    if (dr["from_grade"].Equals(DBNull.Value))
                        objtblPromotionHistory.From_grade = 0;
                    else
                        objtblPromotionHistory.From_grade = (int)dr["from_grade"];
                    if (dr["to_grade"].Equals(DBNull.Value))
                        objtblPromotionHistory.To_grade = 0;
                    else
                        objtblPromotionHistory.To_grade = (int)dr["to_grade"];
                    if (dr["RankGuid"].Equals(DBNull.Value))
                        objtblPromotionHistory.RankGuid = Guid.Empty;
                    else
                        objtblPromotionHistory.RankGuid = (Guid)dr["RankGuid"];
                    if (dr["RankGuidFrom"].Equals(DBNull.Value))
                        objtblPromotionHistory.RankGuidFrom = Guid.Empty;
                    else
                        objtblPromotionHistory.RankGuidFrom = (Guid)dr["RankGuidFrom"];
                    RecordsList.Add(objtblPromotionHistory);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblPromotionHistory::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


       


    }
}