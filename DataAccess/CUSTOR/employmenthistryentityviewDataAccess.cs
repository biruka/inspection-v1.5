﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Commen;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class employmenthistryentityviewDataAccess
    {


        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords(Guid person, string language)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            string columnDescription = Language.GetVisibleDescriptionColumn(Convert.ToInt32(language));

            string strGetAllRecords = @"SELECT [MainGuid],[institution],[job_title],[date_from],[date_to],[date_termination],[termination_reason],[salary],[dbo].[fnGetEmploymentdatesummeryNew] (@Applicant_Guid," + language + @") as SummeryDate, 
                                      [dbo].fnGetLookupNew([emp_status],2," + language + @") as [emp_status],DATEDIFF(year,date_from,date_to) as [net_duration],
                                      (Select {0} from tblEducationalLevel  where cast(Fcode as int)=EducationLevel   )as [EducationLevel] FROM [dbo].[employmenthistryentityview] where [MainGuid]=@Applicant_Guid";
            strGetAllRecords =String.Format(strGetAllRecords,columnDescription);
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("employmenthistryentityview");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Applicant_Guid", person));
                connection.Open();
                adapter.Fill(dTable);
                System.Type typeString = Type.GetType("System.String");
                dTable.Columns.Add("WorkExperiencePeriod", typeString);
                dTable.Columns.Add("WorkExperienceNetDuration", typeString);
                int years = 0;
                int months = 0;
                int days = 0;
                string workexperience = "";
                DateTime startDate;
                DateTime endDate;
                for (int index = 0; index < dTable.Rows.Count; index++)
                {
                    if (dTable.Rows[index]["date_to"].ToString() != "01/01/1909 00:00:00")
                    {
                        TimeSpan dateDifference =  Convert.ToDateTime(dTable.Rows[index]["date_to"].ToString())
                                                                     .Subtract(Convert.ToDateTime(dTable.Rows[index]["date_from"]));
                        endDate = Convert.ToDateTime(dTable.Rows[index]["date_to"].ToString());
                        startDate = Convert.ToDateTime(dTable.Rows[index]["date_from"].ToString());
                        years = ((dateDifference.Days) / 365);
                        months = (((dateDifference.Days) % 365 ) / 30);
                        days = ((dateDifference.Days) % 365 % 30);
                    }
                    else
                    {
                        TimeSpan dateDifference  = DateTime.Today.Subtract(Convert.ToDateTime(dTable.Rows[index]["date_from"]));
                        years = ((dateDifference.Days) / 365);
                        months = (((dateDifference.Days) % 365) / 30);
                        days = (((dateDifference.Days) % 365) % 30);
            }
                    if(((int)Language.eLanguage.eAmharic).ToString() == language)
                    {
                        workexperience = years + " ዓመት ከ " + months + " ወር ከ " + days + "ቀን";
                    }
                    else if (((int)Language.eLanguage.eAfanOromo).ToString() == language)
                    {
                        workexperience = years + " Baraa  kassee" + months + " Ji`aa  Kassee " + days + "Guyyaa";
                    }
                    dTable.Rows[index]["WorkExperiencePeriod"] = workexperience;
                    dTable.Rows[index]["WorkExperienceNetDuration"] = years.ToString();
                 }
            }
            catch (Exception ex)
            {
                throw new Exception("employmenthistryentityview::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public employmenthistryentityview GetRecord(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            employmenthistryentityview objemploymenthistryentityview = new employmenthistryentityview();
            string strGetRecord = @"SELECT [Applicant_Guid],[institution],[job_title],[date_from],[date_to],[date_termination],[termination_reason],[net_duration] FROM [dbo].[employmenthistryentityview] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("employmenthistryentityview");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objemploymenthistryentityview.Applicant_Guid = (Guid)dTable.Rows[0]["Applicant_Guid"];
                    if (dTable.Rows[0]["institution"].Equals(DBNull.Value))
                        objemploymenthistryentityview.Institution = string.Empty;
                    else
                        objemploymenthistryentityview.Institution = (string)dTable.Rows[0]["institution"];
                    if (dTable.Rows[0]["job_title"].Equals(DBNull.Value))
                        objemploymenthistryentityview.Job_title = string.Empty;
                    else
                        objemploymenthistryentityview.Job_title = (string)dTable.Rows[0]["job_title"];
                    if (dTable.Rows[0]["date_from"].Equals(DBNull.Value))
                        objemploymenthistryentityview.Date_from = DateTime.MinValue;
                    else
                        objemploymenthistryentityview.Date_from = (DateTime)dTable.Rows[0]["date_from"];
                    if (dTable.Rows[0]["date_to"].Equals(DBNull.Value))
                        objemploymenthistryentityview.Date_to = DateTime.MinValue;
                    else
                        objemploymenthistryentityview.Date_to = (DateTime)dTable.Rows[0]["date_to"];
                    if (dTable.Rows[0]["date_termination"].Equals(DBNull.Value))
                        objemploymenthistryentityview.Date_termination = DateTime.MinValue;
                    else
                        objemploymenthistryentityview.Date_termination = (DateTime)dTable.Rows[0]["date_termination"];
                    if (dTable.Rows[0]["termination_reason"].Equals(DBNull.Value))
                        objemploymenthistryentityview.Termination_reason = string.Empty;
                    else
                        objemploymenthistryentityview.Termination_reason = (string)dTable.Rows[0]["termination_reason"];
                    if (dTable.Rows[0]["net_duration"].Equals(DBNull.Value))
                        objemploymenthistryentityview.Net_duration = string.Empty;
                    else
                        objemploymenthistryentityview.Net_duration = (string)dTable.Rows[0]["net_duration"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("employmenthistryentityview::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objemploymenthistryentityview;
        }

        public bool Insert(employmenthistryentityview objemploymenthistryentityview)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[employmenthistryentityview]
                                            ([Applicant_Guid],[institution],[job_title],[date_from],[date_to],[date_termination],[termination_reason],[net_duration])
                                     VALUES    (@Applicant_Guid,@institution,@job_title,@date_from,@date_to,@date_termination,@termination_reason,@net_duration)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Applicant_Guid", objemploymenthistryentityview.Applicant_Guid));
                command.Parameters.Add(new SqlParameter("@institution", objemploymenthistryentityview.Institution));
                command.Parameters.Add(new SqlParameter("@job_title", objemploymenthistryentityview.Job_title));
                command.Parameters.Add(new SqlParameter("@date_from", objemploymenthistryentityview.Date_from));
                command.Parameters.Add(new SqlParameter("@date_to", objemploymenthistryentityview.Date_to));
                command.Parameters.Add(new SqlParameter("@date_termination", objemploymenthistryentityview.Date_termination));
                command.Parameters.Add(new SqlParameter("@termination_reason", objemploymenthistryentityview.Termination_reason));
                command.Parameters.Add(new SqlParameter("@net_duration", objemploymenthistryentityview.Net_duration));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("employmenthistryentityview::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(employmenthistryentityview objemploymenthistryentityview)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[employmenthistryentityview] SET     [Applicant_Guid]=@Applicant_Guid,    [institution]=@institution,    [job_title]=@job_title,    [date_from]=@date_from,    [date_to]=@date_to,    [date_termination]=@date_termination,    [termination_reason]=@termination_reason,    [net_duration]=@net_duration ";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Applicant_Guid", objemploymenthistryentityview.Applicant_Guid));
                command.Parameters.Add(new SqlParameter("@institution", objemploymenthistryentityview.Institution));
                command.Parameters.Add(new SqlParameter("@job_title", objemploymenthistryentityview.Job_title));
                command.Parameters.Add(new SqlParameter("@date_from", objemploymenthistryentityview.Date_from));
                command.Parameters.Add(new SqlParameter("@date_to", objemploymenthistryentityview.Date_to));
                command.Parameters.Add(new SqlParameter("@date_termination", objemploymenthistryentityview.Date_termination));
                command.Parameters.Add(new SqlParameter("@termination_reason", objemploymenthistryentityview.Termination_reason));
                command.Parameters.Add(new SqlParameter("@net_duration", objemploymenthistryentityview.Net_duration));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("employmenthistryentityview::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[employmenthistryentityview] ";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("employmenthistryentityview::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<employmenthistryentityview> GetList()
        {
            List<employmenthistryentityview> RecordsList = new List<employmenthistryentityview>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Applicant_Guid],[institution],[job_title],[date_from],[date_to],[date_termination],[termination_reason],[net_duration] FROM [dbo].[employmenthistryentityview] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    employmenthistryentityview objemploymenthistryentityview = new employmenthistryentityview();
                    if (dr["Applicant_Guid"].Equals(DBNull.Value))
                        objemploymenthistryentityview.Applicant_Guid = Guid.Empty;
                    else
                        objemploymenthistryentityview.Applicant_Guid = (Guid)dr["Applicant_Guid"];
                    if (dr["institution"].Equals(DBNull.Value))
                        objemploymenthistryentityview.Institution = string.Empty;
                    else
                        objemploymenthistryentityview.Institution = (string)dr["institution"];
                    if (dr["job_title"].Equals(DBNull.Value))
                        objemploymenthistryentityview.Job_title = string.Empty;
                    else
                        objemploymenthistryentityview.Job_title = (string)dr["job_title"];
                    if (dr["date_from"].Equals(DBNull.Value))
                        objemploymenthistryentityview.Date_from = DateTime.MinValue;
                    else
                        objemploymenthistryentityview.Date_from = (DateTime)dr["date_from"];
                    if (dr["date_to"].Equals(DBNull.Value))
                        objemploymenthistryentityview.Date_to = DateTime.MinValue;
                    else
                        objemploymenthistryentityview.Date_to = (DateTime)dr["date_to"];
                    if (dr["date_termination"].Equals(DBNull.Value))
                        objemploymenthistryentityview.Date_termination = DateTime.MinValue;
                    else
                        objemploymenthistryentityview.Date_termination = (DateTime)dr["date_termination"];
                    if (dr["termination_reason"].Equals(DBNull.Value))
                        objemploymenthistryentityview.Termination_reason = string.Empty;
                    else
                        objemploymenthistryentityview.Termination_reason = (string)dr["termination_reason"];
                    if (dr["net_duration"].Equals(DBNull.Value))
                        objemploymenthistryentityview.Net_duration = string.Empty;
                    else
                        objemploymenthistryentityview.Net_duration = (string)dr["net_duration"];
                    RecordsList.Add(objemploymenthistryentityview);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("employmenthistryentityview::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT  FROM [dbo].[employmenthistryentityview] ";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("employmenthistryentityview::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}