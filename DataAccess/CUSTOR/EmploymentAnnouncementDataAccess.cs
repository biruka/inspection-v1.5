﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class EmploymentAnnouncementDataAccess
    {

        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords(Guid person, string HRActivitytype, string language)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            string strGetAllRecords = "[dbo].[GetAnnouncementValueNew]";
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.StoredProcedure };
            DataTable dTable = new DataTable("EmploymentAnnouncement");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Announcement_Type", HRActivitytype));
                command.Parameters.Add(new SqlParameter("@Applicanit_Gud", person));
                command.Parameters.Add(new SqlParameter("@language", language));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("EmploymentAnnouncement::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public EmploymentAnnouncement GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            EmploymentAnnouncement objEmploymentAnnouncement = new EmploymentAnnouncement();
            string strGetRecord = @"SELECT [Employment_Announcement_Guid],[Announcement_Date],[Deadline_Date],[Deadline_Time],[Announcement_Type],[Registration_Place],[Remark],[Employment_Requisition_Guid],[Documents_Required] FROM [dbo].[EmploymentAnnouncement] WHERE [Employment_Announcement_Guid]=@Employment_Announcement_Guid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("EmploymentAnnouncement");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Employment_Announcement_Guid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objEmploymentAnnouncement.Employment_Announcement_Guid = (Guid)dTable.Rows[0]["Employment_Announcement_Guid"];
                    if (dTable.Rows[0]["Announcement_Date"].Equals(DBNull.Value))
                        objEmploymentAnnouncement.Announcement_Date = DateTime.MinValue;
                    else
                        objEmploymentAnnouncement.Announcement_Date = (DateTime)dTable.Rows[0]["Announcement_Date"];
                    if (dTable.Rows[0]["Deadline_Date"].Equals(DBNull.Value))
                        objEmploymentAnnouncement.Deadline_Date = DateTime.MinValue;
                    else
                        objEmploymentAnnouncement.Deadline_Date = (DateTime)dTable.Rows[0]["Deadline_Date"];
                    if (dTable.Rows[0]["Deadline_Time"].Equals(DBNull.Value))
                        objEmploymentAnnouncement.Deadline_Time = DateTime.MinValue;
                    else
                        objEmploymentAnnouncement.Deadline_Time = (DateTime)dTable.Rows[0]["Deadline_Time"];
                    if (dTable.Rows[0]["Announcement_Type"].Equals(DBNull.Value))
                        objEmploymentAnnouncement.Announcement_Type = 0;
                    else
                        objEmploymentAnnouncement.Announcement_Type = (int)dTable.Rows[0]["Announcement_Type"];
                    if (dTable.Rows[0]["Registration_Place"].Equals(DBNull.Value))
                        objEmploymentAnnouncement.Registration_Place = string.Empty;
                    else
                        objEmploymentAnnouncement.Registration_Place = (string)dTable.Rows[0]["Registration_Place"];
                    if (dTable.Rows[0]["Remark"].Equals(DBNull.Value))
                        objEmploymentAnnouncement.Remark = string.Empty;
                    else
                        objEmploymentAnnouncement.Remark = (string)dTable.Rows[0]["Remark"];
                    if (dTable.Rows[0]["Employment_Requisition_Guid"].Equals(DBNull.Value))
                        objEmploymentAnnouncement.Employment_Requisition_Guid = Guid.Empty;
                    else
                        objEmploymentAnnouncement.Employment_Requisition_Guid = (Guid)dTable.Rows[0]["Employment_Requisition_Guid"];
                    if (dTable.Rows[0]["Documents_Required"].Equals(DBNull.Value))
                        objEmploymentAnnouncement.Documents_Required = string.Empty;
                    else
                        objEmploymentAnnouncement.Documents_Required = (string)dTable.Rows[0]["Documents_Required"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("EmploymentAnnouncement::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objEmploymentAnnouncement;
        }

        public bool Insert(EmploymentAnnouncement objEmploymentAnnouncement)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[EmploymentAnnouncement]
                                            ([Employment_Announcement_Guid],[Announcement_Date],[Deadline_Date],[Deadline_Time],[Announcement_Type],[Registration_Place],[Remark],[Employment_Requisition_Guid],[Documents_Required])
                                     VALUES    (@Employment_Announcement_Guid,@Announcement_Date,@Deadline_Date,@Deadline_Time,@Announcement_Type,@Registration_Place,@Remark,@Employment_Requisition_Guid,@Documents_Required)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Employment_Announcement_Guid", objEmploymentAnnouncement.Employment_Announcement_Guid));
                command.Parameters.Add(new SqlParameter("@Announcement_Date", objEmploymentAnnouncement.Announcement_Date));
                command.Parameters.Add(new SqlParameter("@Deadline_Date", objEmploymentAnnouncement.Deadline_Date));
                command.Parameters.Add(new SqlParameter("@Deadline_Time", objEmploymentAnnouncement.Deadline_Time));
                command.Parameters.Add(new SqlParameter("@Announcement_Type", objEmploymentAnnouncement.Announcement_Type));
                command.Parameters.Add(new SqlParameter("@Registration_Place", objEmploymentAnnouncement.Registration_Place));
                command.Parameters.Add(new SqlParameter("@Remark", objEmploymentAnnouncement.Remark));
                command.Parameters.Add(new SqlParameter("@Employment_Requisition_Guid", objEmploymentAnnouncement.Employment_Requisition_Guid));
                command.Parameters.Add(new SqlParameter("@Documents_Required", objEmploymentAnnouncement.Documents_Required));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("EmploymentAnnouncement::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(EmploymentAnnouncement objEmploymentAnnouncement)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[EmploymentAnnouncement] SET     [Employment_Announcement_Guid]=@Employment_Announcement_Guid,    [Announcement_Date]=@Announcement_Date,    [Deadline_Date]=@Deadline_Date,    [Deadline_Time]=@Deadline_Time,    [Announcement_Type]=@Announcement_Type,    [Registration_Place]=@Registration_Place,    [Remark]=@Remark,    [Employment_Requisition_Guid]=@Employment_Requisition_Guid,    [Documents_Required]=@Documents_Required WHERE [Employment_Announcement_Guid]=@Employment_Announcement_Guid";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Employment_Announcement_Guid", objEmploymentAnnouncement.Employment_Announcement_Guid));
                command.Parameters.Add(new SqlParameter("@Announcement_Date", objEmploymentAnnouncement.Announcement_Date));
                command.Parameters.Add(new SqlParameter("@Deadline_Date", objEmploymentAnnouncement.Deadline_Date));
                command.Parameters.Add(new SqlParameter("@Deadline_Time", objEmploymentAnnouncement.Deadline_Time));
                command.Parameters.Add(new SqlParameter("@Announcement_Type", objEmploymentAnnouncement.Announcement_Type));
                command.Parameters.Add(new SqlParameter("@Registration_Place", objEmploymentAnnouncement.Registration_Place));
                command.Parameters.Add(new SqlParameter("@Remark", objEmploymentAnnouncement.Remark));
                command.Parameters.Add(new SqlParameter("@Employment_Requisition_Guid", objEmploymentAnnouncement.Employment_Requisition_Guid));
                command.Parameters.Add(new SqlParameter("@Documents_Required", objEmploymentAnnouncement.Documents_Required));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("EmploymentAnnouncement::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[EmploymentAnnouncement] WHERE [Employment_Announcement_Guid]=@Employment_Announcement_Guid";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Employment_Announcement_Guid", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("EmploymentAnnouncement::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<EmploymentAnnouncement> GetList()
        {
            List<EmploymentAnnouncement> RecordsList = new List<EmploymentAnnouncement>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Employment_Announcement_Guid],[Announcement_Date],[Deadline_Date],[Deadline_Time],[Announcement_Type],[Registration_Place],[Remark],[Employment_Requisition_Guid],[Documents_Required] FROM [dbo].[EmploymentAnnouncement]  ORDER BY  [Employment_Announcement_Guid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    EmploymentAnnouncement objEmploymentAnnouncement = new EmploymentAnnouncement();
                    if (dr["Employment_Announcement_Guid"].Equals(DBNull.Value))
                        objEmploymentAnnouncement.Employment_Announcement_Guid = Guid.Empty;
                    else
                        objEmploymentAnnouncement.Employment_Announcement_Guid = (Guid)dr["Employment_Announcement_Guid"];
                    if (dr["Announcement_Date"].Equals(DBNull.Value))
                        objEmploymentAnnouncement.Announcement_Date = DateTime.MinValue;
                    else
                        objEmploymentAnnouncement.Announcement_Date = (DateTime)dr["Announcement_Date"];
                    if (dr["Deadline_Date"].Equals(DBNull.Value))
                        objEmploymentAnnouncement.Deadline_Date = DateTime.MinValue;
                    else
                        objEmploymentAnnouncement.Deadline_Date = (DateTime)dr["Deadline_Date"];
                    if (dr["Deadline_Time"].Equals(DBNull.Value))
                        objEmploymentAnnouncement.Deadline_Time = DateTime.MinValue;
                    else
                        objEmploymentAnnouncement.Deadline_Time = (DateTime)dr["Deadline_Time"];
                    if (dr["Announcement_Type"].Equals(DBNull.Value))
                        objEmploymentAnnouncement.Announcement_Type = 0;
                    else
                        objEmploymentAnnouncement.Announcement_Type = (int)dr["Announcement_Type"];
                    if (dr["Registration_Place"].Equals(DBNull.Value))
                        objEmploymentAnnouncement.Registration_Place = string.Empty;
                    else
                        objEmploymentAnnouncement.Registration_Place = (string)dr["Registration_Place"];
                    if (dr["Remark"].Equals(DBNull.Value))
                        objEmploymentAnnouncement.Remark = string.Empty;
                    else
                        objEmploymentAnnouncement.Remark = (string)dr["Remark"];
                    if (dr["Employment_Requisition_Guid"].Equals(DBNull.Value))
                        objEmploymentAnnouncement.Employment_Requisition_Guid = Guid.Empty;
                    else
                        objEmploymentAnnouncement.Employment_Requisition_Guid = (Guid)dr["Employment_Requisition_Guid"];
                    if (dr["Documents_Required"].Equals(DBNull.Value))
                        objEmploymentAnnouncement.Documents_Required = string.Empty;
                    else
                        objEmploymentAnnouncement.Documents_Required = (string)dr["Documents_Required"];
                    RecordsList.Add(objEmploymentAnnouncement);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("EmploymentAnnouncement::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Employment_Announcement_Guid FROM [dbo].[EmploymentAnnouncement] WHERE [Employment_Announcement_Guid]=@Employment_Announcement_Guid";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Employment_Announcement_Guid", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("EmploymentAnnouncement::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}