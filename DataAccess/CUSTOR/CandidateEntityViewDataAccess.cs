﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class CandidateEntityViewDataAccess
    {

        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public DataTable GetRecords(Guid person, string Activitytype,string language)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            //string strGetAllRecords = @"SELECT [FullName],[Filedofstudy],[Expyear],[MainGuid],[Announcement_Guid],[TheoryExamResult],[PracticalExamResult],[InterviewExamResult],[Selection_Criteria],[Result],[Is_Hired] FROM [dbo].[CandidateEntityView] WHERE  ([Announcement_Guid] = dbo.fnGetAnnouncementGuid(@Applicanit_Gud)) order by [Result] DESC ";
            string strGetAllRecords = "[dbo].[GetCandidateValueNew]";
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.StoredProcedure };
            DataTable dTable = new DataTable("CandidateEntityView");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Applicanit_Gud", person));
                command.Parameters.Add(new SqlParameter("@Announcement_Type", Activitytype));
                command.Parameters.Add(new SqlParameter("@language", language));

                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("CandidateEntityView::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public CandidateEntityView GetRecord(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            CandidateEntityView objCandidateEntityView = new CandidateEntityView();
            string strGetRecord = @"SELECT [FullName],[Filedofstudy],[Expyear],[MainGuid],[Announcement_Guid],[TheoryExamResult],[PracticalExamResult],[InterviewExamResult],[Selection_Criteria],[Result],[Is_Hired] FROM [dbo].[CandidateEntityView] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("CandidateEntityView");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    if (dTable.Rows[0]["FullName"].Equals(DBNull.Value))
                        objCandidateEntityView.FullName = string.Empty;
                    else
                        objCandidateEntityView.FullName = (string)dTable.Rows[0]["FullName"];
                    if (dTable.Rows[0]["Filedofstudy"].Equals(DBNull.Value))
                        objCandidateEntityView.Filedofstudy = string.Empty;
                    else
                        objCandidateEntityView.Filedofstudy = (string)dTable.Rows[0]["Filedofstudy"];
                    if (dTable.Rows[0]["Expyear"].Equals(DBNull.Value))
                        objCandidateEntityView.Expyear = string.Empty;
                    else
                        objCandidateEntityView.Expyear = (string)dTable.Rows[0]["Expyear"];
                    if (dTable.Rows[0]["MainGuid"].Equals(DBNull.Value))
                        objCandidateEntityView.MainGuid = Guid.Empty;
                    else
                        objCandidateEntityView.MainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    if (dTable.Rows[0]["Announcement_Guid"].Equals(DBNull.Value))
                        objCandidateEntityView.Announcement_Guid = Guid.Empty;
                    else
                        objCandidateEntityView.Announcement_Guid = (Guid)dTable.Rows[0]["Announcement_Guid"];
                    if (dTable.Rows[0]["TheoryExamResult"].Equals(DBNull.Value))
                        objCandidateEntityView.TheoryExamResult = 0;
                    else
                        objCandidateEntityView.TheoryExamResult = (double)dTable.Rows[0]["TheoryExamResult"];
                    if (dTable.Rows[0]["PracticalExamResult"].Equals(DBNull.Value))
                        objCandidateEntityView.PracticalExamResult = 0;
                    else
                        objCandidateEntityView.PracticalExamResult = (double)dTable.Rows[0]["PracticalExamResult"];
                    if (dTable.Rows[0]["InterviewExamResult"].Equals(DBNull.Value))
                        objCandidateEntityView.InterviewExamResult = 0;
                    else
                        objCandidateEntityView.InterviewExamResult = (double)dTable.Rows[0]["InterviewExamResult"];
                    objCandidateEntityView.Selection_Criteria = (string)dTable.Rows[0]["Selection_Criteria"];
                    if (dTable.Rows[0]["Result"].Equals(DBNull.Value))
                        objCandidateEntityView.Result = 0;
                    else
                        objCandidateEntityView.Result = (double)dTable.Rows[0]["Result"];
                    if (dTable.Rows[0]["Is_Hired"].Equals(DBNull.Value))
                        objCandidateEntityView.Is_Hired = false;
                    else
                        objCandidateEntityView.Is_Hired = (bool)dTable.Rows[0]["Is_Hired"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("CandidateEntityView::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objCandidateEntityView;
        }

        public bool Insert(CandidateEntityView objCandidateEntityView)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[CandidateEntityView]
                                            ([FullName],[Filedofstudy],[Expyear],[MainGuid],[Announcement_Guid],[TheoryExamResult],[PracticalExamResult],[InterviewExamResult],[Selection_Criteria],[Result],[Is_Hired])
                                     VALUES    (@FullName,@Filedofstudy,@Expyear,@MainGuid,@Announcement_Guid,@TheoryExamResult,@PracticalExamResult,@InterviewExamResult,@Selection_Criteria,@Result,@Is_Hired)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@FullName", objCandidateEntityView.FullName));
                command.Parameters.Add(new SqlParameter("@Filedofstudy", objCandidateEntityView.Filedofstudy));
                command.Parameters.Add(new SqlParameter("@Expyear", objCandidateEntityView.Expyear));
                command.Parameters.Add(new SqlParameter("@MainGuid", objCandidateEntityView.MainGuid));
                command.Parameters.Add(new SqlParameter("@Announcement_Guid", objCandidateEntityView.Announcement_Guid));
                command.Parameters.Add(new SqlParameter("@TheoryExamResult", objCandidateEntityView.TheoryExamResult));
                command.Parameters.Add(new SqlParameter("@PracticalExamResult", objCandidateEntityView.PracticalExamResult));
                command.Parameters.Add(new SqlParameter("@InterviewExamResult", objCandidateEntityView.InterviewExamResult));
                command.Parameters.Add(new SqlParameter("@Selection_Criteria", objCandidateEntityView.Selection_Criteria));
                command.Parameters.Add(new SqlParameter("@Result", objCandidateEntityView.Result));
                command.Parameters.Add(new SqlParameter("@Is_Hired", objCandidateEntityView.Is_Hired));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("CandidateEntityView::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(CandidateEntityView objCandidateEntityView)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[CandidateEntityView] SET     [FullName]=@FullName,    [Filedofstudy]=@Filedofstudy,    [Expyear]=@Expyear,    [MainGuid]=@MainGuid,    [Announcement_Guid]=@Announcement_Guid,    [TheoryExamResult]=@TheoryExamResult,    [PracticalExamResult]=@PracticalExamResult,    [InterviewExamResult]=@InterviewExamResult,    [Selection_Criteria]=@Selection_Criteria,    [Result]=@Result,    [Is_Hired]=@Is_Hired ";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@FullName", objCandidateEntityView.FullName));
                command.Parameters.Add(new SqlParameter("@Filedofstudy", objCandidateEntityView.Filedofstudy));
                command.Parameters.Add(new SqlParameter("@Expyear", objCandidateEntityView.Expyear));
                command.Parameters.Add(new SqlParameter("@MainGuid", objCandidateEntityView.MainGuid));
                command.Parameters.Add(new SqlParameter("@Announcement_Guid", objCandidateEntityView.Announcement_Guid));
                command.Parameters.Add(new SqlParameter("@TheoryExamResult", objCandidateEntityView.TheoryExamResult));
                command.Parameters.Add(new SqlParameter("@PracticalExamResult", objCandidateEntityView.PracticalExamResult));
                command.Parameters.Add(new SqlParameter("@InterviewExamResult", objCandidateEntityView.InterviewExamResult));
                command.Parameters.Add(new SqlParameter("@Selection_Criteria", objCandidateEntityView.Selection_Criteria));
                command.Parameters.Add(new SqlParameter("@Result", objCandidateEntityView.Result));
                command.Parameters.Add(new SqlParameter("@Is_Hired", objCandidateEntityView.Is_Hired));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("CandidateEntityView::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[CandidateEntityView] ";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("CandidateEntityView::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


        public List<CandidateEntityView> GetList()
        {
            List<CandidateEntityView> RecordsList = new List<CandidateEntityView>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [FullName],[Filedofstudy],[Expyear],[MainGuid],[Announcement_Guid],[TheoryExamResult],[PracticalExamResult],[InterviewExamResult],[Selection_Criteria],[Result],[Is_Hired] FROM [dbo].[CandidateEntityView] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    CandidateEntityView objCandidateEntityView = new CandidateEntityView();
                    if (dr["FullName"].Equals(DBNull.Value))
                        objCandidateEntityView.FullName = string.Empty;
                    else
                        objCandidateEntityView.FullName = (string)dr["FullName"];
                    if (dr["Filedofstudy"].Equals(DBNull.Value))
                        objCandidateEntityView.Filedofstudy = string.Empty;
                    else
                        objCandidateEntityView.Filedofstudy = (string)dr["Filedofstudy"];
                    if (dr["Expyear"].Equals(DBNull.Value))
                        objCandidateEntityView.Expyear = string.Empty;
                    else
                        objCandidateEntityView.Expyear = (string)dr["Expyear"];
                    if (dr["MainGuid"].Equals(DBNull.Value))
                        objCandidateEntityView.MainGuid = Guid.Empty;
                    else
                        objCandidateEntityView.MainGuid = (Guid)dr["MainGuid"];
                    if (dr["Announcement_Guid"].Equals(DBNull.Value))
                        objCandidateEntityView.Announcement_Guid = Guid.Empty;
                    else
                        objCandidateEntityView.Announcement_Guid = (Guid)dr["Announcement_Guid"];
                    if (dr["TheoryExamResult"].Equals(DBNull.Value))
                        objCandidateEntityView.TheoryExamResult = 0;
                    else
                        objCandidateEntityView.TheoryExamResult = (double)dr["TheoryExamResult"];
                    if (dr["PracticalExamResult"].Equals(DBNull.Value))
                        objCandidateEntityView.PracticalExamResult = 0;
                    else
                        objCandidateEntityView.PracticalExamResult = (double)dr["PracticalExamResult"];
                    if (dr["InterviewExamResult"].Equals(DBNull.Value))
                        objCandidateEntityView.InterviewExamResult = 0;
                    else
                        objCandidateEntityView.InterviewExamResult = (double)dr["InterviewExamResult"];
                    if (dr["Selection_Criteria"].Equals(DBNull.Value))
                        objCandidateEntityView.Selection_Criteria = string.Empty;
                    else
                        objCandidateEntityView.Selection_Criteria = (string)dr["Selection_Criteria"];
                    if (dr["Result"].Equals(DBNull.Value))
                        objCandidateEntityView.Result = 0;
                    else
                        objCandidateEntityView.Result = (double)dr["Result"];
                    if (dr["Is_Hired"].Equals(DBNull.Value))
                        objCandidateEntityView.Is_Hired = false;
                    else
                        objCandidateEntityView.Is_Hired = (bool)dr["Is_Hired"];
                    RecordsList.Add(objCandidateEntityView);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("CandidateEntityView::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT  FROM [dbo].[CandidateEntityView] ";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("CandidateEntityView::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}
