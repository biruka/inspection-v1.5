﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblTerminationDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords(Guid person, string language)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [MainGuid],[ParentGuid],[Terminationdate] ,[dbo].fnGetLookupNew([Termination_reason],66," + language + @") as [Termination_reason] ,[reason],[Remark],[NewJobTitle],[NewRank],LetterNo,LetterDate,[Punishment],[Returndate] FROM [dbo].[tblTermination] where ParentGuid=@ParentGuid  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblTermination");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ParentGuid", person));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblTermination::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public tblTermination GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblTermination objtblTermination = new tblTermination();
            string strGetRecord = @"SELECT [MainGuid],[ParentGuid],[Terminationdate],[Termination_reason],[reason],[Remark],[NewJobTitle],[NewRank],[Punishment],[Returndate] FROM [dbo].[tblTermination] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblTermination");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblTermination.MainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    if (dTable.Rows[0]["ParentGuid"].Equals(DBNull.Value))
                        objtblTermination.ParentGuid = Guid.Empty;
                    else
                        objtblTermination.ParentGuid = (Guid)dTable.Rows[0]["ParentGuid"];
                    if (dTable.Rows[0]["Terminationdate"].Equals(DBNull.Value))
                        objtblTermination.Terminationdate = DateTime.MinValue;
                    else
                        objtblTermination.Terminationdate = (DateTime)dTable.Rows[0]["Terminationdate"];
                    if (dTable.Rows[0]["Termination_reason"].Equals(DBNull.Value))
                        objtblTermination.Termination_reason = string.Empty;
                    else
                        objtblTermination.Termination_reason = (string)dTable.Rows[0]["Termination_reason"];
                    if (dTable.Rows[0]["reason"].Equals(DBNull.Value))
                        objtblTermination.Reason = string.Empty;
                    else
                        objtblTermination.Reason = (string)dTable.Rows[0]["reason"];
                    if (dTable.Rows[0]["Remark"].Equals(DBNull.Value))
                        objtblTermination.Remark = string.Empty;
                    else
                        objtblTermination.Remark = (string)dTable.Rows[0]["Remark"];
                    if (dTable.Rows[0]["NewJobTitle"].Equals(DBNull.Value))
                        objtblTermination.NewJobTitle = string.Empty;
                    else
                        objtblTermination.NewJobTitle = (string)dTable.Rows[0]["NewJobTitle"];
                    if (dTable.Rows[0]["NewRank"].Equals(DBNull.Value))
                        objtblTermination.NewRank = string.Empty;
                    else
                        objtblTermination.NewRank = (string)dTable.Rows[0]["NewRank"];
                    if (dTable.Rows[0]["Punishment"].Equals(DBNull.Value))
                        objtblTermination.Punishment = string.Empty;
                    else
                        objtblTermination.Punishment = (string)dTable.Rows[0]["Punishment"];
                    if (dTable.Rows[0]["Returndate"].Equals(DBNull.Value))
                        objtblTermination.Returndate = DateTime.MinValue;
                    else
                        objtblTermination.Returndate = (DateTime)dTable.Rows[0]["Returndate"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblTermination::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblTermination;
        }

        public bool Insert(tblTermination objtblTermination)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[tblTermination]
                                            ([MainGuid],[ParentGuid],[Terminationdate],[Termination_reason],[reason],[Remark],[NewJobTitle],[NewRank],[Punishment],[Returndate])
                                     VALUES    ( ISNULL(@MainGuid, (newid())),@ParentGuid,@Terminationdate,@Termination_reason,@reason,@Remark,@NewJobTitle,@NewRank,@Punishment,@Returndate)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", objtblTermination.MainGuid));
                command.Parameters.Add(new SqlParameter("@ParentGuid", objtblTermination.ParentGuid));
                command.Parameters.Add(new SqlParameter("@Terminationdate", objtblTermination.Terminationdate));
                command.Parameters.Add(new SqlParameter("@Termination_reason", objtblTermination.Termination_reason));
                command.Parameters.Add(new SqlParameter("@reason", objtblTermination.Reason));
                command.Parameters.Add(new SqlParameter("@Remark", objtblTermination.Remark));
                command.Parameters.Add(new SqlParameter("@NewJobTitle", objtblTermination.NewJobTitle));
                command.Parameters.Add(new SqlParameter("@NewRank", objtblTermination.NewRank));
                command.Parameters.Add(new SqlParameter("@Punishment", objtblTermination.Punishment));
                command.Parameters.Add(new SqlParameter("@Returndate", objtblTermination.Returndate));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblTermination::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(tblTermination objtblTermination)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[tblTermination] SET     [MainGuid]=@MainGuid,    [ParentGuid]=@ParentGuid,    [Terminationdate]=@Terminationdate,    [Termination_reason]=@Termination_reason,    [reason]=@reason,    [Remark]=@Remark,    [NewJobTitle]=@NewJobTitle,    [NewRank]=@NewRank,    [Punishment]=@Punishment,    [Returndate]=@Returndate WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", objtblTermination.MainGuid));
                command.Parameters.Add(new SqlParameter("@ParentGuid", objtblTermination.ParentGuid));
                command.Parameters.Add(new SqlParameter("@Terminationdate", objtblTermination.Terminationdate));
                command.Parameters.Add(new SqlParameter("@Termination_reason", objtblTermination.Termination_reason));
                command.Parameters.Add(new SqlParameter("@reason", objtblTermination.Reason));
                command.Parameters.Add(new SqlParameter("@Remark", objtblTermination.Remark));
                command.Parameters.Add(new SqlParameter("@NewJobTitle", objtblTermination.NewJobTitle));
                command.Parameters.Add(new SqlParameter("@NewRank", objtblTermination.NewRank));
                command.Parameters.Add(new SqlParameter("@Punishment", objtblTermination.Punishment));
                command.Parameters.Add(new SqlParameter("@Returndate", objtblTermination.Returndate));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblTermination::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblTermination] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblTermination::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<tblTermination> GetList()
        {
            List<tblTermination> RecordsList = new List<tblTermination>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [MainGuid],[ParentGuid],[Terminationdate],[Termination_reason],[reason],[Remark],[NewJobTitle],[NewRank],[Punishment],[Returndate] FROM [dbo].[tblTermination]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblTermination objtblTermination = new tblTermination();
                    if (dr["MainGuid"].Equals(DBNull.Value))
                        objtblTermination.MainGuid = Guid.Empty;
                    else
                        objtblTermination.MainGuid = (Guid)dr["MainGuid"];
                    if (dr["ParentGuid"].Equals(DBNull.Value))
                        objtblTermination.ParentGuid = Guid.Empty;
                    else
                        objtblTermination.ParentGuid = (Guid)dr["ParentGuid"];
                    if (dr["Terminationdate"].Equals(DBNull.Value))
                        objtblTermination.Terminationdate = DateTime.MinValue;
                    else
                        objtblTermination.Terminationdate = (DateTime)dr["Terminationdate"];
                    if (dr["Termination_reason"].Equals(DBNull.Value))
                        objtblTermination.Termination_reason = string.Empty;
                    else
                        objtblTermination.Termination_reason = (string)dr["Termination_reason"];
                    if (dr["reason"].Equals(DBNull.Value))
                        objtblTermination.Reason = string.Empty;
                    else
                        objtblTermination.Reason = (string)dr["reason"];
                    if (dr["Remark"].Equals(DBNull.Value))
                        objtblTermination.Remark = string.Empty;
                    else
                        objtblTermination.Remark = (string)dr["Remark"];
                    if (dr["NewJobTitle"].Equals(DBNull.Value))
                        objtblTermination.NewJobTitle = string.Empty;
                    else
                        objtblTermination.NewJobTitle = (string)dr["NewJobTitle"];
                    if (dr["NewRank"].Equals(DBNull.Value))
                        objtblTermination.NewRank = string.Empty;
                    else
                        objtblTermination.NewRank = (string)dr["NewRank"];
                    if (dr["Punishment"].Equals(DBNull.Value))
                        objtblTermination.Punishment = string.Empty;
                    else
                        objtblTermination.Punishment = (string)dr["Punishment"];
                    if (dr["Returndate"].Equals(DBNull.Value))
                        objtblTermination.Returndate = DateTime.MinValue;
                    else
                        objtblTermination.Returndate = (DateTime)dr["Returndate"];
                    RecordsList.Add(objtblTermination);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblTermination::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


       


    }
}