﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblServiceYearExtenderDataAccess
    {
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public DataTable GetRecords(Guid person)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [SYEMainGuid],[seq_no],[SYEYear],[SYEHowMuch], [Reason] ,[AnotherReason],[CreatedBy],[Approved],[ApprovedBy],[CreatedDate],[ApprovedDate],[PersonGuid],[Remark] FROM [dbo].[tblServiceYearExtender] where PersonGuid=@PersonGuid and Approved= 1 ORDER BY  [SYEMainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblServiceYearExtender");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@PersonGuid", person));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblServiceYearExtender::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public tblServiceYearExtender GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblServiceYearExtender objtblServiceYearExtender = new tblServiceYearExtender();
            string strGetRecord = @"SELECT [SYEMainGuid],[seq_no],[SYEYear],[SYEHowMuch],[Reason],[AnotherReason],[CreatedBy],[Approved],[ApprovedBy],[CreatedDate],[ApprovedDate],[PersonGuid],[Remark] FROM [dbo].[tblServiceYearExtender] WHERE [SYEMainGuid]=@SYEMainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblServiceYearExtender");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@SYEMainGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblServiceYearExtender.SYEMainGuid = (Guid)dTable.Rows[0]["SYEMainGuid"];
                    objtblServiceYearExtender.Seq_no = (int)dTable.Rows[0]["seq_no"];
                    if (dTable.Rows[0]["SYEYear"].Equals(DBNull.Value))
                        objtblServiceYearExtender.SYEYear = 0;
                    else
                        objtblServiceYearExtender.SYEYear = (int)dTable.Rows[0]["SYEYear"];
                    if (dTable.Rows[0]["SYEHowMuch"].Equals(DBNull.Value))
                        objtblServiceYearExtender.SYEHowMuch = 0;
                    else
                        objtblServiceYearExtender.SYEHowMuch = (int)dTable.Rows[0]["SYEHowMuch"];
                    if (dTable.Rows[0]["Reason"].Equals(DBNull.Value))
                        objtblServiceYearExtender.Reason = false;
                    else
                        objtblServiceYearExtender.Reason = (bool)dTable.Rows[0]["Reason"];
                    if (dTable.Rows[0]["AnotherReason"].Equals(DBNull.Value))
                        objtblServiceYearExtender.AnotherReason = string.Empty;
                    else
                        objtblServiceYearExtender.AnotherReason = (string)dTable.Rows[0]["AnotherReason"];
                    if (dTable.Rows[0]["CreatedBy"].Equals(DBNull.Value))
                        objtblServiceYearExtender.CreatedBy = string.Empty;
                    else
                        objtblServiceYearExtender.CreatedBy = (string)dTable.Rows[0]["CreatedBy"];
                    if (dTable.Rows[0]["Approved"].Equals(DBNull.Value))
                        objtblServiceYearExtender.Approved = false;
                    else
                        objtblServiceYearExtender.Approved = (bool)dTable.Rows[0]["Approved"];
                    if (dTable.Rows[0]["ApprovedBy"].Equals(DBNull.Value))
                        objtblServiceYearExtender.ApprovedBy = string.Empty;
                    else
                        objtblServiceYearExtender.ApprovedBy = (string)dTable.Rows[0]["ApprovedBy"];
                    if (dTable.Rows[0]["CreatedDate"].Equals(DBNull.Value))
                        objtblServiceYearExtender.CreatedDate = DateTime.MinValue;
                    else
                        objtblServiceYearExtender.CreatedDate = (DateTime)dTable.Rows[0]["CreatedDate"];
                    if (dTable.Rows[0]["ApprovedDate"].Equals(DBNull.Value))
                        objtblServiceYearExtender.ApprovedDate = DateTime.MinValue;
                    else
                        objtblServiceYearExtender.ApprovedDate = (DateTime)dTable.Rows[0]["ApprovedDate"];
                    objtblServiceYearExtender.PersonGuid = (Guid)dTable.Rows[0]["PersonGuid"];
                    if (dTable.Rows[0]["Remark"].Equals(DBNull.Value))
                        objtblServiceYearExtender.Remark = string.Empty;
                    else
                        objtblServiceYearExtender.Remark = (string)dTable.Rows[0]["Remark"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblServiceYearExtender::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblServiceYearExtender;
        }

       
        public List<tblServiceYearExtender> GetList()
        {
            List<tblServiceYearExtender> RecordsList = new List<tblServiceYearExtender>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [SYEMainGuid],[seq_no],[SYEYear],[SYEHowMuch],[Reason],[AnotherReason],[CreatedBy],[Approved],[ApprovedBy],[CreatedDate],[ApprovedDate],[PersonGuid],[Remark] FROM [dbo].[tblServiceYearExtender]  ORDER BY  [SYEMainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblServiceYearExtender objtblServiceYearExtender = new tblServiceYearExtender();
                    if (dr["SYEMainGuid"].Equals(DBNull.Value))
                        objtblServiceYearExtender.SYEMainGuid = Guid.Empty;
                    else
                        objtblServiceYearExtender.SYEMainGuid = (Guid)dr["SYEMainGuid"];
                    if (dr["seq_no"].Equals(DBNull.Value))
                        objtblServiceYearExtender.Seq_no = 0;
                    else
                        objtblServiceYearExtender.Seq_no = (int)dr["seq_no"];
                    if (dr["SYEYear"].Equals(DBNull.Value))
                        objtblServiceYearExtender.SYEYear = 0;
                    else
                        objtblServiceYearExtender.SYEYear = (int)dr["SYEYear"];
                    if (dr["SYEHowMuch"].Equals(DBNull.Value))
                        objtblServiceYearExtender.SYEHowMuch = 0;
                    else
                        objtblServiceYearExtender.SYEHowMuch = (int)dr["SYEHowMuch"];
                    if (dr["Reason"].Equals(DBNull.Value))
                        objtblServiceYearExtender.Reason = false;
                    else
                        objtblServiceYearExtender.Reason = (bool)dr["Reason"];
                    if (dr["AnotherReason"].Equals(DBNull.Value))
                        objtblServiceYearExtender.AnotherReason = string.Empty;
                    else
                        objtblServiceYearExtender.AnotherReason = (string)dr["AnotherReason"];
                    if (dr["CreatedBy"].Equals(DBNull.Value))
                        objtblServiceYearExtender.CreatedBy = string.Empty;
                    else
                        objtblServiceYearExtender.CreatedBy = (string)dr["CreatedBy"];
                    if (dr["Approved"].Equals(DBNull.Value))
                        objtblServiceYearExtender.Approved = false;
                    else
                        objtblServiceYearExtender.Approved = (bool)dr["Approved"];
                    if (dr["ApprovedBy"].Equals(DBNull.Value))
                        objtblServiceYearExtender.ApprovedBy = string.Empty;
                    else
                        objtblServiceYearExtender.ApprovedBy = (string)dr["ApprovedBy"];
                    if (dr["CreatedDate"].Equals(DBNull.Value))
                        objtblServiceYearExtender.CreatedDate = DateTime.MinValue;
                    else
                        objtblServiceYearExtender.CreatedDate = (DateTime)dr["CreatedDate"];
                    if (dr["ApprovedDate"].Equals(DBNull.Value))
                        objtblServiceYearExtender.ApprovedDate = DateTime.MinValue;
                    else
                        objtblServiceYearExtender.ApprovedDate = (DateTime)dr["ApprovedDate"];
                    if (dr["PersonGuid"].Equals(DBNull.Value))
                        objtblServiceYearExtender.PersonGuid = Guid.Empty;
                    else
                        objtblServiceYearExtender.PersonGuid = (Guid)dr["PersonGuid"];
                    if (dr["Remark"].Equals(DBNull.Value))
                        objtblServiceYearExtender.Remark = string.Empty;
                    else
                        objtblServiceYearExtender.Remark = (string)dr["Remark"];
                    RecordsList.Add(objtblServiceYearExtender);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblServiceYearExtender::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT SYEMainGuid FROM [dbo].[tblServiceYearExtender] WHERE [SYEMainGuid]=@SYEMainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@SYEMainGuid", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("tblServiceYearExtender::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}