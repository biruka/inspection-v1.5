﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class InspectionFollowupEntiryviewDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords(Guid person)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [EmployeeGUID],[FollowupGUID],[InspectionGUID],[FollowupDate],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime],[Isvalid],[Filename],[reanswer],[InspectionDecision],[FollowupStatus],[ActionTaken],[Remark] FROM [dbo].[InspectionFollowupEntiryview] where EmployeeGUID=@EmployeeGUID ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("InspectionFollowupEntiryview");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@EmployeeGUID", person));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("InspectionFollowupEntiryview::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public InspectionFollowupEntiryview GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            InspectionFollowupEntiryview objInspectionFollowupEntiryview = new InspectionFollowupEntiryview();
            string strGetRecord = @"SELECT [EmployeeGUID],[FollowupGUID],[InspectionGUID],[FollowupDate],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime],[Isvalid],[Filename],[reanswer],[InspectionDecision],[FollowupStatus],[ActionTaken],[Remark] FROM [dbo].[InspectionFollowupEntiryview] WHERE [FollowupGUID]=@FollowupGUID ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("InspectionFollowupEntiryview");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@FollowupGUID", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objInspectionFollowupEntiryview.EmployeeGUID = (Guid)dTable.Rows[0]["EmployeeGUID"];
                    objInspectionFollowupEntiryview.FollowupGUID = (Guid)dTable.Rows[0]["FollowupGUID"];
                    objInspectionFollowupEntiryview.InspectionGUID = (Guid)dTable.Rows[0]["InspectionGUID"];
                    objInspectionFollowupEntiryview.FollowupDate = (DateTime)dTable.Rows[0]["FollowupDate"];
                    objInspectionFollowupEntiryview.UserName = (string)dTable.Rows[0]["UserName"];
                    objInspectionFollowupEntiryview.EventDatetime = (DateTime)dTable.Rows[0]["EventDatetime"];
                    if (dTable.Rows[0]["UpdatedUsername"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.UpdatedUsername = string.Empty;
                    else
                        objInspectionFollowupEntiryview.UpdatedUsername = (string)dTable.Rows[0]["UpdatedUsername"];
                    if (dTable.Rows[0]["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.UpdatedEventDatetime = DateTime.MinValue;
                    else
                        objInspectionFollowupEntiryview.UpdatedEventDatetime = (DateTime)dTable.Rows[0]["UpdatedEventDatetime"];
                    if (dTable.Rows[0]["Isvalid"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.Isvalid = false;
                    else
                        objInspectionFollowupEntiryview.Isvalid = (bool)dTable.Rows[0]["Isvalid"];
                    if (dTable.Rows[0]["Filename"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.Filename = string.Empty;
                    else
                        objInspectionFollowupEntiryview.Filename = (string)dTable.Rows[0]["Filename"];
                    if (dTable.Rows[0]["reanswer"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.Reanswer = string.Empty;
                    else
                        objInspectionFollowupEntiryview.Reanswer = (string)dTable.Rows[0]["reanswer"];
                    if (dTable.Rows[0]["InspectionDecision"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.InspectionDecision = string.Empty;
                    else
                        objInspectionFollowupEntiryview.InspectionDecision = (string)dTable.Rows[0]["InspectionDecision"];
                    objInspectionFollowupEntiryview.FollowupStatus = (int)dTable.Rows[0]["FollowupStatus"];
                    objInspectionFollowupEntiryview.ActionTaken = (int)dTable.Rows[0]["ActionTaken"];
                    if (dTable.Rows[0]["Remark"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.Remark = string.Empty;
                    else
                        objInspectionFollowupEntiryview.Remark = (string)dTable.Rows[0]["Remark"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("InspectionFollowupEntiryview::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objInspectionFollowupEntiryview;
        }

        public bool Insert(InspectionFollowupEntiryview objInspectionFollowupEntiryview)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[InspectionFollowupEntiryview]
                                            ([FollowupDate],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime],[Isvalid],[Filename],[reanswer],[InspectionDecision],[FollowupStatus],[ActionTaken],[Remark])
                                     VALUES    (@FollowupDate,@UserName,@EventDatetime,@UpdatedUsername,@UpdatedEventDatetime,@Isvalid,@Filename,@reanswer,@InspectionDecision,@FollowupStatus,@ActionTaken,@Remark)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@EmployeeGUID", objInspectionFollowupEntiryview.EmployeeGUID));
                command.Parameters.Add(new SqlParameter("@FollowupGUID", objInspectionFollowupEntiryview.FollowupGUID));
                command.Parameters.Add(new SqlParameter("@InspectionGUID", objInspectionFollowupEntiryview.InspectionGUID));
                command.Parameters.Add(new SqlParameter("@FollowupDate", objInspectionFollowupEntiryview.FollowupDate));
                command.Parameters.Add(new SqlParameter("@UserName", objInspectionFollowupEntiryview.UserName));
                command.Parameters.Add(new SqlParameter("@EventDatetime", objInspectionFollowupEntiryview.EventDatetime));
                command.Parameters.Add(new SqlParameter("@UpdatedUsername", objInspectionFollowupEntiryview.UpdatedUsername));
                command.Parameters.Add(new SqlParameter("@UpdatedEventDatetime", objInspectionFollowupEntiryview.UpdatedEventDatetime));
                command.Parameters.Add(new SqlParameter("@Isvalid", objInspectionFollowupEntiryview.Isvalid));
                command.Parameters.Add(new SqlParameter("@Filename", objInspectionFollowupEntiryview.Filename));
                command.Parameters.Add(new SqlParameter("@reanswer", objInspectionFollowupEntiryview.Reanswer));
                command.Parameters.Add(new SqlParameter("@InspectionDecision", objInspectionFollowupEntiryview.InspectionDecision));
                command.Parameters.Add(new SqlParameter("@FollowupStatus", objInspectionFollowupEntiryview.FollowupStatus));
                command.Parameters.Add(new SqlParameter("@ActionTaken", objInspectionFollowupEntiryview.ActionTaken));
                command.Parameters.Add(new SqlParameter("@Remark", objInspectionFollowupEntiryview.Remark));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("InspectionFollowupEntiryview::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(InspectionFollowupEntiryview objInspectionFollowupEntiryview)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[InspectionFollowupEntiryview] SET     [FollowupDate]=@FollowupDate,    [UserName]=@UserName,    [EventDatetime]=@EventDatetime,    [UpdatedUsername]=@UpdatedUsername,    [UpdatedEventDatetime]=@UpdatedEventDatetime,    [Isvalid]=@Isvalid,    [Filename]=@Filename,    [reanswer]=@reanswer,    [InspectionDecision]=@InspectionDecision,    [FollowupStatus]=@FollowupStatus,    [ActionTaken]=@ActionTaken,    [Remark]=@Remark WHERE [FollowupGUID]=@FollowupGUID ";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@EmployeeGUID", objInspectionFollowupEntiryview.EmployeeGUID));
                command.Parameters.Add(new SqlParameter("@FollowupGUID", objInspectionFollowupEntiryview.FollowupGUID));
                command.Parameters.Add(new SqlParameter("@InspectionGUID", objInspectionFollowupEntiryview.InspectionGUID));
                command.Parameters.Add(new SqlParameter("@FollowupDate", objInspectionFollowupEntiryview.FollowupDate));
                command.Parameters.Add(new SqlParameter("@UserName", objInspectionFollowupEntiryview.UserName));
                command.Parameters.Add(new SqlParameter("@EventDatetime", objInspectionFollowupEntiryview.EventDatetime));
                command.Parameters.Add(new SqlParameter("@UpdatedUsername", objInspectionFollowupEntiryview.UpdatedUsername));
                command.Parameters.Add(new SqlParameter("@UpdatedEventDatetime", objInspectionFollowupEntiryview.UpdatedEventDatetime));
                command.Parameters.Add(new SqlParameter("@Isvalid", objInspectionFollowupEntiryview.Isvalid));
                command.Parameters.Add(new SqlParameter("@Filename", objInspectionFollowupEntiryview.Filename));
                command.Parameters.Add(new SqlParameter("@reanswer", objInspectionFollowupEntiryview.Reanswer));
                command.Parameters.Add(new SqlParameter("@InspectionDecision", objInspectionFollowupEntiryview.InspectionDecision));
                command.Parameters.Add(new SqlParameter("@FollowupStatus", objInspectionFollowupEntiryview.FollowupStatus));
                command.Parameters.Add(new SqlParameter("@ActionTaken", objInspectionFollowupEntiryview.ActionTaken));
                command.Parameters.Add(new SqlParameter("@Remark", objInspectionFollowupEntiryview.Remark));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("InspectionFollowupEntiryview::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[InspectionFollowupEntiryview] ";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("InspectionFollowupEntiryview::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<InspectionFollowupEntiryview> GetList()
        {
            List<InspectionFollowupEntiryview> RecordsList = new List<InspectionFollowupEntiryview>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [EmployeeGUID],[FollowupGUID],[InspectionGUID],[FollowupDate],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime],[Isvalid],[Filename],[reanswer],[InspectionDecision],[FollowupStatus],[ActionTaken],[Remark] FROM [dbo].[InspectionFollowupEntiryview] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    InspectionFollowupEntiryview objInspectionFollowupEntiryview = new InspectionFollowupEntiryview();
                    if (dr["EmployeeGUID"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.EmployeeGUID = Guid.Empty;
                    else
                        objInspectionFollowupEntiryview.EmployeeGUID = (Guid)dr["EmployeeGUID"];
                    if (dr["FollowupGUID"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.FollowupGUID = Guid.Empty;
                    else
                        objInspectionFollowupEntiryview.FollowupGUID = (Guid)dr["FollowupGUID"];
                    if (dr["InspectionGUID"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.InspectionGUID = Guid.Empty;
                    else
                        objInspectionFollowupEntiryview.InspectionGUID = (Guid)dr["InspectionGUID"];
                    if (dr["FollowupDate"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.FollowupDate = DateTime.MinValue;
                    else
                        objInspectionFollowupEntiryview.FollowupDate = (DateTime)dr["FollowupDate"];
                    if (dr["UserName"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.UserName = string.Empty;
                    else
                        objInspectionFollowupEntiryview.UserName = (string)dr["UserName"];
                    if (dr["EventDatetime"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.EventDatetime = DateTime.MinValue;
                    else
                        objInspectionFollowupEntiryview.EventDatetime = (DateTime)dr["EventDatetime"];
                    if (dr["UpdatedUsername"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.UpdatedUsername = string.Empty;
                    else
                        objInspectionFollowupEntiryview.UpdatedUsername = (string)dr["UpdatedUsername"];
                    if (dr["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.UpdatedEventDatetime = DateTime.MinValue;
                    else
                        objInspectionFollowupEntiryview.UpdatedEventDatetime = (DateTime)dr["UpdatedEventDatetime"];
                    if (dr["Isvalid"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.Isvalid = false;
                    else
                        objInspectionFollowupEntiryview.Isvalid = (bool)dr["Isvalid"];
                    if (dr["Filename"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.Filename = string.Empty;
                    else
                        objInspectionFollowupEntiryview.Filename = (string)dr["Filename"];
                    if (dr["reanswer"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.Reanswer = string.Empty;
                    else
                        objInspectionFollowupEntiryview.Reanswer = (string)dr["reanswer"];
                    if (dr["InspectionDecision"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.InspectionDecision = string.Empty;
                    else
                        objInspectionFollowupEntiryview.InspectionDecision = (string)dr["InspectionDecision"];
                    if (dr["FollowupStatus"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.FollowupStatus = 0;
                    else
                        objInspectionFollowupEntiryview.FollowupStatus = (int)dr["FollowupStatus"];
                    if (dr["ActionTaken"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.ActionTaken = 0;
                    else
                        objInspectionFollowupEntiryview.ActionTaken = (int)dr["ActionTaken"];
                    if (dr["Remark"].Equals(DBNull.Value))
                        objInspectionFollowupEntiryview.Remark = string.Empty;
                    else
                        objInspectionFollowupEntiryview.Remark = (string)dr["Remark"];
                    RecordsList.Add(objInspectionFollowupEntiryview);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("InspectionFollowupEntiryview::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT  FROM [dbo].[InspectionFollowupEntiryview] ";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("InspectionFollowupEntiryview::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}