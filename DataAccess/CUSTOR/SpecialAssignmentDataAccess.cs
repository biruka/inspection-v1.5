﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CUSTOR.Domain;


namespace CUSTOR.DataAccess
{
    public class SpecialAssignmentDataAccess
    {
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public DataTable GetRecordByPerson(Guid personGuid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            string strGetAllRecords = @"SELECT  MainGuid, seq_no, ParentGuid, from_date, to_date, work_performed, remark, Benifit, Reserve, ScaleType,(select job_title from tblPosition where seq_no=tblSpecialAssignment.work_performed) AS work_performed_str FROM tblSpecialAssignment WHERE ParentGuid=@ParentGuid and isdeliget=1 ORDER BY seq_no DESC";
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("SpecialAssignment");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ParentGuid", personGuid));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("SpecialAssignment::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[from_date],[to_date],[work_performed],[remark],[Benifit],[Reserve],[ScaleType],[Latter_No],[isdeliget] FROM [dbo].[SpecialAssignment]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("SpecialAssignment");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("SpecialAssignment::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public SpecialAssignment GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            SpecialAssignment objSpecialAssignment = new SpecialAssignment();
            string strGetRecord = @"SELECT [MainGuid],[seq_no],[ParentGuid],[from_date],[to_date],[work_performed],[remark],[Benifit],[Reserve],[ScaleType],[Latter_No],[isdeliget] FROM [dbo].[SpecialAssignment] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("SpecialAssignment");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objSpecialAssignment.MainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    objSpecialAssignment.Seq_no = (int)dTable.Rows[0]["seq_no"];
                    if (dTable.Rows[0]["ParentGuid"].Equals(DBNull.Value))
                        objSpecialAssignment.ParentGuid = Guid.Empty;
                    else
                        objSpecialAssignment.ParentGuid = (Guid)dTable.Rows[0]["ParentGuid"];
                    if (dTable.Rows[0]["from_date"].Equals(DBNull.Value))
                        objSpecialAssignment.From_date = DateTime.MinValue;
                    else
                        objSpecialAssignment.From_date = (DateTime)dTable.Rows[0]["from_date"];
                    if (dTable.Rows[0]["to_date"].Equals(DBNull.Value))
                        objSpecialAssignment.To_date = DateTime.MinValue;
                    else
                        objSpecialAssignment.To_date = (DateTime)dTable.Rows[0]["to_date"];
                    if (dTable.Rows[0]["work_performed"].Equals(DBNull.Value))
                        objSpecialAssignment.Work_performed = string.Empty;
                    else
                        objSpecialAssignment.Work_performed = (string)dTable.Rows[0]["work_performed"];
                    if (dTable.Rows[0]["remark"].Equals(DBNull.Value))
                        objSpecialAssignment.Remark = string.Empty;
                    else
                        objSpecialAssignment.Remark = (string)dTable.Rows[0]["remark"];
                    if (dTable.Rows[0]["Benifit"].Equals(DBNull.Value))
                        objSpecialAssignment.Benifit = false;
                    else
                        objSpecialAssignment.Benifit = (bool)dTable.Rows[0]["Benifit"];
                    if (dTable.Rows[0]["Reserve"].Equals(DBNull.Value))
                        objSpecialAssignment.Reserve = false;
                    else
                        objSpecialAssignment.Reserve = (bool)dTable.Rows[0]["Reserve"];
                    if (dTable.Rows[0]["ScaleType"].Equals(DBNull.Value))
                        objSpecialAssignment.ScaleType = 0;
                    else
                        objSpecialAssignment.ScaleType = (int)dTable.Rows[0]["ScaleType"];
                    if (dTable.Rows[0]["Latter_No"].Equals(DBNull.Value))
                        objSpecialAssignment.Latter_No = string.Empty;
                    else
                        objSpecialAssignment.Latter_No = (string)dTable.Rows[0]["Latter_No"];
                    if (dTable.Rows[0]["isdeliget"].Equals(DBNull.Value))
                        objSpecialAssignment.Isdeliget = false;
                    else
                        objSpecialAssignment.Isdeliget = (bool)dTable.Rows[0]["isdeliget"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("SpecialAssignment::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objSpecialAssignment;
        }

        public bool Insert(SpecialAssignment objSpecialAssignment)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[SpecialAssignment]
                                            ([MainGuid],[ParentGuid],[from_date],[to_date],[work_performed],[remark],[Benifit],[Reserve],[ScaleType],[Latter_No],[isdeliget])
                                     VALUES    ( ISNULL(@MainGuid, (newid())),@ParentGuid,@from_date,@to_date,@work_performed,@remark,@Benifit,@Reserve,@ScaleType,@Latter_No,@isdeliget)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", objSpecialAssignment.MainGuid));
                command.Parameters.Add(new SqlParameter("@ParentGuid", objSpecialAssignment.ParentGuid));
                command.Parameters.Add(new SqlParameter("@from_date", objSpecialAssignment.From_date));
                command.Parameters.Add(new SqlParameter("@to_date", objSpecialAssignment.To_date));
                command.Parameters.Add(new SqlParameter("@work_performed", objSpecialAssignment.Work_performed));
                command.Parameters.Add(new SqlParameter("@remark", objSpecialAssignment.Remark));
                command.Parameters.Add(new SqlParameter("@Benifit", objSpecialAssignment.Benifit));
                command.Parameters.Add(new SqlParameter("@Reserve", objSpecialAssignment.Reserve));
                command.Parameters.Add(new SqlParameter("@ScaleType", objSpecialAssignment.ScaleType));
                command.Parameters.Add(new SqlParameter("@Latter_No", objSpecialAssignment.Latter_No));
                command.Parameters.Add(new SqlParameter("@isdeliget", objSpecialAssignment.Isdeliget));
                command.Parameters.Add(new SqlParameter("@seq_no", objSpecialAssignment.Seq_no));
                command.Parameters["@seq_no"].Direction = ParameterDirection.Output;


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                objSpecialAssignment.Seq_no = (int)command.Parameters["@seq_no"].Value;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("SpecialAssignment::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(SpecialAssignment objSpecialAssignment)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[SpecialAssignment] SET     [MainGuid]=@MainGuid,    [ParentGuid]=@ParentGuid,    [from_date]=@from_date,    [to_date]=@to_date,    [work_performed]=@work_performed,    [remark]=@remark,    [Benifit]=@Benifit,    [Reserve]=@Reserve,    [ScaleType]=@ScaleType,    [Latter_No]=@Latter_No,    [isdeliget]=@isdeliget WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", objSpecialAssignment.MainGuid));
                command.Parameters.Add(new SqlParameter("@seq_no", objSpecialAssignment.Seq_no));
                command.Parameters.Add(new SqlParameter("@ParentGuid", objSpecialAssignment.ParentGuid));
                command.Parameters.Add(new SqlParameter("@from_date", objSpecialAssignment.From_date));
                command.Parameters.Add(new SqlParameter("@to_date", objSpecialAssignment.To_date));
                command.Parameters.Add(new SqlParameter("@work_performed", objSpecialAssignment.Work_performed));
                command.Parameters.Add(new SqlParameter("@remark", objSpecialAssignment.Remark));
                command.Parameters.Add(new SqlParameter("@Benifit", objSpecialAssignment.Benifit));
                command.Parameters.Add(new SqlParameter("@Reserve", objSpecialAssignment.Reserve));
                command.Parameters.Add(new SqlParameter("@ScaleType", objSpecialAssignment.ScaleType));
                command.Parameters.Add(new SqlParameter("@Latter_No", objSpecialAssignment.Latter_No));
                command.Parameters.Add(new SqlParameter("@isdeliget", objSpecialAssignment.Isdeliget));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("SpecialAssignment::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[SpecialAssignment] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("SpecialAssignment::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<SpecialAssignment> GetList()
        {
            List<SpecialAssignment> RecordsList = new List<SpecialAssignment>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[from_date],[to_date],[work_performed],[remark],[Benifit],[Reserve],[ScaleType],[Latter_No],[isdeliget] FROM [dbo].[SpecialAssignment]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    SpecialAssignment objSpecialAssignment = new SpecialAssignment();
                    if (dr["MainGuid"].Equals(DBNull.Value))
                        objSpecialAssignment.MainGuid = Guid.Empty;
                    else
                        objSpecialAssignment.MainGuid = (Guid)dr["MainGuid"];
                    if (dr["seq_no"].Equals(DBNull.Value))
                        objSpecialAssignment.Seq_no = 0;
                    else
                        objSpecialAssignment.Seq_no = (int)dr["seq_no"];
                    if (dr["ParentGuid"].Equals(DBNull.Value))
                        objSpecialAssignment.ParentGuid = Guid.Empty;
                    else
                        objSpecialAssignment.ParentGuid = (Guid)dr["ParentGuid"];
                    if (dr["from_date"].Equals(DBNull.Value))
                        objSpecialAssignment.From_date = DateTime.MinValue;
                    else
                        objSpecialAssignment.From_date = (DateTime)dr["from_date"];
                    if (dr["to_date"].Equals(DBNull.Value))
                        objSpecialAssignment.To_date = DateTime.MinValue;
                    else
                        objSpecialAssignment.To_date = (DateTime)dr["to_date"];
                    if (dr["work_performed"].Equals(DBNull.Value))
                        objSpecialAssignment.Work_performed = string.Empty;
                    else
                        objSpecialAssignment.Work_performed = (string)dr["work_performed"];
                    if (dr["remark"].Equals(DBNull.Value))
                        objSpecialAssignment.Remark = string.Empty;
                    else
                        objSpecialAssignment.Remark = (string)dr["remark"];
                    if (dr["Benifit"].Equals(DBNull.Value))
                        objSpecialAssignment.Benifit = false;
                    else
                        objSpecialAssignment.Benifit = (bool)dr["Benifit"];
                    if (dr["Reserve"].Equals(DBNull.Value))
                        objSpecialAssignment.Reserve = false;
                    else
                        objSpecialAssignment.Reserve = (bool)dr["Reserve"];
                    if (dr["ScaleType"].Equals(DBNull.Value))
                        objSpecialAssignment.ScaleType = 0;
                    else
                        objSpecialAssignment.ScaleType = (int)dr["ScaleType"];
                    if (dr["Latter_No"].Equals(DBNull.Value))
                        objSpecialAssignment.Latter_No = string.Empty;
                    else
                        objSpecialAssignment.Latter_No = (string)dr["Latter_No"];
                    if (dr["isdeliget"].Equals(DBNull.Value))
                        objSpecialAssignment.Isdeliget = false;
                    else
                        objSpecialAssignment.Isdeliget = (bool)dr["isdeliget"];
                    RecordsList.Add(objSpecialAssignment);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("SpecialAssignment::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT MainGuid FROM [dbo].[SpecialAssignment] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("SpecialAssignment::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
    }
}
