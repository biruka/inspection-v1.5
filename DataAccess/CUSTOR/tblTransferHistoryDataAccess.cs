﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Commen;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblTransferHistoryDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords(Guid person,int language)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string descriptionColumn = Language.GetVisibleDescriptionColumn(language);
            string strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid], [transfer_date],(SELECT  {0} FROM  tblUnit WHERE (code = tblTransferHistory.transferred_from)) as [transferred_from],
                                       (SELECT   {0} FROM  tblUnit WHERE (code = tblTransferHistory.transferred_to)) as [transferred_to],[remark],[from_salary],[to_salary],
                                       (SELECT job_title FROM tblPosition where seq_no= tblTransferHistory.[from_job_title]) as [from_job_title],
                                       (SELECT job_title FROM tblPosition where seq_no= tblTransferHistory.[to_job_title]) as [to_job_title],[from_rank],[to_rank], [TransferType] as TransferType,[dbo].[fnGetGradefromposition](from_job_title) As oldGrade,[dbo].[fnGetGradefromposition](to_job_title) As newGrade,
                                       TransferTypeReal as TransferTypeReal,[dbo].[fnGetOrganiZation]((SELECT  OrgGuid FROM  tblUnit WHERE (code = tblTransferHistory.transferred_from)),1) as [FromOrganization],
                                       [dbo].[fnGetOrganiZation]((SELECT  OrgGuid FROM  tblUnit WHERE (code = tblTransferHistory.transferred_to)),"+ language + @") as [ToOrganization]    
                                       FROM [dbo].[tblTransferHistory]  
                                       where ParentGuid=@ParentGuid  ORDER BY  [MainGuid] ASC";
            strGetAllRecords = String.Format(strGetAllRecords, descriptionColumn);
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblTransferHistory");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ParentGuid", person));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblTransferHistory::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public tblTransferHistory GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblTransferHistory objtblTransferHistory = new tblTransferHistory();
            string strGetRecord = @"SELECT [MainGuid],[seq_no],[ParentGuid],[transfer_date],[transferred_from],[transferred_to],[from_salary],[to_salary] FROM [dbo].[tblTransferHistory] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblTransferHistory");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblTransferHistory.MainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    objtblTransferHistory.Seq_no = (int)dTable.Rows[0]["seq_no"];
                    if (dTable.Rows[0]["ParentGuid"].Equals(DBNull.Value))
                        objtblTransferHistory.ParentGuid = Guid.Empty;
                    else
                        objtblTransferHistory.ParentGuid = (Guid)dTable.Rows[0]["ParentGuid"];
                    if (dTable.Rows[0]["transfer_date"].Equals(DBNull.Value))
                        objtblTransferHistory.Transfer_date = DateTime.MinValue;
                    else
                        objtblTransferHistory.Transfer_date = (DateTime)dTable.Rows[0]["transfer_date"];
                    if (dTable.Rows[0]["transferred_from"].Equals(DBNull.Value))
                        objtblTransferHistory.Transferred_from = 0;
                    else
                        objtblTransferHistory.Transferred_from = (int)dTable.Rows[0]["transferred_from"];
                    if (dTable.Rows[0]["transferred_to"].Equals(DBNull.Value))
                        objtblTransferHistory.Transferred_to = 0;
                    else
                        objtblTransferHistory.Transferred_to = (int)dTable.Rows[0]["transferred_to"];
                    if (dTable.Rows[0]["from_salary"].Equals(DBNull.Value))
                        objtblTransferHistory.From_salary = 0;
                    else
                        objtblTransferHistory.From_salary = (double)dTable.Rows[0]["from_salary"];
                    if (dTable.Rows[0]["to_salary"].Equals(DBNull.Value))
                        objtblTransferHistory.To_salary = 0;
                    else
                        objtblTransferHistory.To_salary = (double)dTable.Rows[0]["to_salary"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblTransferHistory::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblTransferHistory;
        }

       

        public List<tblTransferHistory> GetList()
        {
            List<tblTransferHistory> RecordsList = new List<tblTransferHistory>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[transfer_date],[transferred_from],[transferred_to],[from_salary],[to_salary] FROM [dbo].[tblTransferHistory]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblTransferHistory objtblTransferHistory = new tblTransferHistory();
                    if (dr["MainGuid"].Equals(DBNull.Value))
                        objtblTransferHistory.MainGuid = Guid.Empty;
                    else
                        objtblTransferHistory.MainGuid = (Guid)dr["MainGuid"];
                    if (dr["seq_no"].Equals(DBNull.Value))
                        objtblTransferHistory.Seq_no = 0;
                    else
                        objtblTransferHistory.Seq_no = (int)dr["seq_no"];
                    if (dr["ParentGuid"].Equals(DBNull.Value))
                        objtblTransferHistory.ParentGuid = Guid.Empty;
                    else
                        objtblTransferHistory.ParentGuid = (Guid)dr["ParentGuid"];
                    if (dr["transfer_date"].Equals(DBNull.Value))
                        objtblTransferHistory.Transfer_date = DateTime.MinValue;
                    else
                        objtblTransferHistory.Transfer_date = (DateTime)dr["transfer_date"];
                    if (dr["transferred_from"].Equals(DBNull.Value))
                        objtblTransferHistory.Transferred_from = 0;
                    else
                        objtblTransferHistory.Transferred_from = (int)dr["transferred_from"];
                    if (dr["transferred_to"].Equals(DBNull.Value))
                        objtblTransferHistory.Transferred_to = 0;
                    else
                        objtblTransferHistory.Transferred_to = (int)dr["transferred_to"];
                    if (dr["from_salary"].Equals(DBNull.Value))
                        objtblTransferHistory.From_salary = 0;
                    else
                        objtblTransferHistory.From_salary = (double)dr["from_salary"];
                    if (dr["to_salary"].Equals(DBNull.Value))
                        objtblTransferHistory.To_salary = 0;
                    else
                        objtblTransferHistory.To_salary = (double)dr["to_salary"];
                    RecordsList.Add(objtblTransferHistory);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblTransferHistory::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


       


    }
}