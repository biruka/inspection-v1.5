﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblLateComerDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }
                
        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[late_date],[period],[duration],[remark] FROM [dbo].[tblLateComer]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblLateComer");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblLateComer::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }
        

        public tblLateComer GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblLateComer objtblLateComer = new tblLateComer();
            string strGetRecord = @"SELECT [MainGuid],[seq_no],[ParentGuid],[late_date],[period],[duration],[remark] FROM [dbo].[tblLateComer] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblLateComer");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblLateComer.MainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    objtblLateComer.Seq_no = (int)dTable.Rows[0]["seq_no"];
                    if (dTable.Rows[0]["ParentGuid"].Equals(DBNull.Value))
                        objtblLateComer.ParentGuid = Guid.Empty;
                    else
                        objtblLateComer.ParentGuid = (Guid)dTable.Rows[0]["ParentGuid"];
                    if (dTable.Rows[0]["late_date"].Equals(DBNull.Value))
                        objtblLateComer.Late_date = DateTime.MinValue;
                    else
                        objtblLateComer.Late_date = (DateTime)dTable.Rows[0]["late_date"];
                    if (dTable.Rows[0]["period"].Equals(DBNull.Value))
                        objtblLateComer.Period = 0;
                    else
                        objtblLateComer.Period = (int)dTable.Rows[0]["period"];
                    if (dTable.Rows[0]["duration"].Equals(DBNull.Value))
                        objtblLateComer.Duration = 0;
                    else
                        objtblLateComer.Duration = (int)dTable.Rows[0]["duration"];
                    if (dTable.Rows[0]["remark"].Equals(DBNull.Value))
                        objtblLateComer.Remark = string.Empty;
                    else
                        objtblLateComer.Remark = (string)dTable.Rows[0]["remark"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblLateComer::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblLateComer;
        }

        
        public List<tblLateComer> GetListLateCome(Guid ParentID)
        {
            List<tblLateComer> RecordsList = new List<tblLateComer>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[late_date],[period],[duration],[remark] FROM [dbo].[tblLateComer] where ParentGuid=@ParentGuid ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                command.Parameters.Add(new SqlParameter("@ParentGuid", ParentID));
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblLateComer objtblLateComer = new tblLateComer();
                    if (dr["MainGuid"].Equals(DBNull.Value))
                        objtblLateComer.MainGuid = Guid.Empty;
                    else
                        objtblLateComer.MainGuid = (Guid)dr["MainGuid"];
                    if (dr["seq_no"].Equals(DBNull.Value))
                        objtblLateComer.Seq_no = 0;
                    else
                        objtblLateComer.Seq_no = (int)dr["seq_no"];
                    if (dr["ParentGuid"].Equals(DBNull.Value))
                        objtblLateComer.ParentGuid = Guid.Empty;
                    else
                        objtblLateComer.ParentGuid = (Guid)dr["ParentGuid"];
                    if (dr["late_date"].Equals(DBNull.Value))
                        objtblLateComer.Late_date = DateTime.MinValue;
                    else
                        objtblLateComer.Late_date = (DateTime)dr["late_date"];
                    if (dr["period"].Equals(DBNull.Value))
                        objtblLateComer.Period = 0;
                    else
                        objtblLateComer.Period = (int)dr["period"];
                    if (dr["duration"].Equals(DBNull.Value))
                        objtblLateComer.Duration = 0;
                    else
                        objtblLateComer.Duration = (int)dr["duration"];
                    if (dr["remark"].Equals(DBNull.Value))
                        objtblLateComer.Remark = string.Empty;
                    else
                        objtblLateComer.Remark = (string)dr["remark"];
                    RecordsList.Add(objtblLateComer);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblLateComer::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public List<tblLateComer> GetList()
        {
            List<tblLateComer> RecordsList = new List<tblLateComer>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[late_date],[period],[duration],[remark] FROM [dbo].[tblLateComer]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblLateComer objtblLateComer = new tblLateComer();
                    if (dr["MainGuid"].Equals(DBNull.Value))
                        objtblLateComer.MainGuid = Guid.Empty;
                    else
                        objtblLateComer.MainGuid = (Guid)dr["MainGuid"];
                    if (dr["seq_no"].Equals(DBNull.Value))
                        objtblLateComer.Seq_no = 0;
                    else
                        objtblLateComer.Seq_no = (int)dr["seq_no"];
                    if (dr["ParentGuid"].Equals(DBNull.Value))
                        objtblLateComer.ParentGuid = Guid.Empty;
                    else
                        objtblLateComer.ParentGuid = (Guid)dr["ParentGuid"];
                    if (dr["late_date"].Equals(DBNull.Value))
                        objtblLateComer.Late_date = DateTime.MinValue;
                    else
                        objtblLateComer.Late_date = (DateTime)dr["late_date"];
                    if (dr["period"].Equals(DBNull.Value))
                        objtblLateComer.Period = 0;
                    else
                        objtblLateComer.Period = (int)dr["period"];
                    if (dr["duration"].Equals(DBNull.Value))
                        objtblLateComer.Duration = 0;
                    else
                        objtblLateComer.Duration = (int)dr["duration"];
                    if (dr["remark"].Equals(DBNull.Value))
                        objtblLateComer.Remark = string.Empty;
                    else
                        objtblLateComer.Remark = (string)dr["remark"];
                    RecordsList.Add(objtblLateComer);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblLateComer::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT MainGuid FROM [dbo].[tblLateComer] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("tblLateComer::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}