﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.Commen;


namespace CUSTOR.DataAccess
{
    public class PersonEmploymentEntityViewDataAccess
    {
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public DataTable GetRecords(Guid OrgGuid, int DocumentType, DateTime fromdate, DateTime todate, string language, bool isHrOfficer)
        {

            string strGetAllRecords = "";
            SqlConnection connection = new SqlConnection(ConnectionString);

            if (DocumentType == 311)
            {
                string strGetAttribute = @"SELECT DISTINCT dbo.tblPerson.first_am_name + ' ' + dbo.tblPerson.father_am_name + ' ' + dbo.tblPerson.grand_am_name AS FullName, dbo.tblPerson.sex AS Gender,[dbo].[fnGetLookupNew] (dbo.tblPerson.emp_status,2," + language + @") AS Typeofemployment, dbo.tblPerson.emp_status as Typeofemployment1, dbo.tblPosition.seq_no,dbo.tblPosition.position_code, [dbo].[fnGetjobtitle] (dbo.tblPerson.job_title) as job_title, dbo.tblPerson.MainGuid,[dbo].[fnGetInspectStatus] (dbo.tblPerson.MainGuid,311) as inspectstatus,[dbo].[fnGetInspDecStatus] (dbo.tblPerson.MainGuid,311) as InspectionDecision,
                      dbo.tblPerson.person_id, dbo.tblPerson.staff_code, dbo.tblPerson.staff_code_sort, dbo.tblPerson.cost_center_code, dbo.tblPerson.unit_id, dbo.tblPerson.first_name, 
                      dbo.tblPerson.father_name, dbo.tblPerson.grand_name, dbo.tblPerson.allowance, dbo.tblPerson.salary, dbo.tblPerson.transport_allowance, 
                      dbo.tblPerson.hardship_allowance, dbo.tblPerson.housing_allowance, dbo.tblPerson.over_time, dbo.tblPerson.pension_employee_contribution, 
                      dbo.tblPerson.pension_company_contribution, dbo.tblPerson.is_ca_member, dbo.tblPerson.ca_membership_date, dbo.tblPerson.ca_termination_date, 
                      dbo.tblPerson.thrift_contribution, dbo.tblPerson.extra_contribution, dbo.tblPerson.card_fee, dbo.tblPerson.membership_fee, 
                      dbo.tblPerson.is_membership_fee_returned, dbo.tblPerson.is_lu_member, dbo.tblPerson.labour_union, dbo.tblPerson.leave_without_pay, dbo.tblPerson.absentism, 
                      dbo.tblPerson.fine, dbo.tblPerson.sex, dbo.tblPerson.religion, dbo.tblPerson.birth_date, dbo.tblPerson.birth_place, dbo.tblPerson.marital_status, 
                      dbo.tblPerson.family_size, dbo.tblPerson.nationality, dbo.tblPerson.town, dbo.tblPerson.region, dbo.tblPerson.p_o_box, dbo.tblPerson.person_address, 
                      dbo.tblPerson.tel_home, dbo.tblPerson.tel_off_direct1, dbo.tblPerson.tel_off_direct2, dbo.tblPerson.tel_off_ext1, dbo.tblPerson.room_no, dbo.tblPerson.passport_no, 
                      dbo.tblPerson.license_grade, dbo.tblPerson.education_group, dbo.tblPerson.education_level, dbo.tblPerson.education_description, dbo.tblPerson.job_title_Sort, 
                      dbo.tblPerson.occupation, dbo.tblPerson.occupation_step, dbo.tblPerson.emp_date, dbo.tblPerson.current_status, dbo.tblPerson.Kebele, dbo.tblPerson.Worede, 
                      dbo.tblPerson.Zone, dbo.tblPerson.location, dbo.tblPerson.emp_status, dbo.tblUnit.amDescription AS Unitdesc, 
                      dbo.tblGrade.am_description AS AmGrade,dbo.tblPerson.emp_status as emp_status1,isnull([dbo].[fnGetInspApproved] (dbo.tblPerson.MainGuid,311),0) as Approved ";
                string strgetfrom = "FROM  dbo.tblPerson INNER JOIN dbo.tblPosition ON dbo.tblPerson.job_title = dbo.tblPosition.seq_no INNER JOIN " +
                                    " dbo.tblUnit ON dbo.tblPosition.unit_id = dbo.tblUnit.code LEFT OUTER JOIN dbo.tblGrade ON dbo.tblPosition.grade_id = dbo.tblGrade.grade_id INNER JOIN " +
                                    " dbo.Organization ON dbo.tblUnit.OrgGuid = dbo.Organization.OrgGuid ";

                string strgetwhere = " WHERE  (emp_date between @fromTime and @todate) AND (dbo.Organization.OrgGuid = @OrgGUID) ";

                strGetAllRecords = strGetAttribute + strgetfrom + strgetwhere;
            }
            else if (DocumentType == 312)
            {
                string strGetAttribute = @"SELECT DISTINCT dbo.tblPerson.first_am_name + ' ' + dbo.tblPerson.father_am_name + ' ' + dbo.tblPerson.grand_am_name AS FullName,dbo.tblPerson.sex AS Gender,[dbo].[fnGetLookupNew](tblPromotionHistory.promotion_type,400," + language + @") AS Typeofemployment,tblPromotionHistory.promotion_type as Typeofemployment1  ,dbo.tblPosition.seq_no,dbo.tblPosition.position_code, [dbo].[fnGetjobtitle] (dbo.tblPerson.job_title) as job_title, dbo.tblPerson.MainGuid,[dbo].[fnGetInspectStatus] (dbo.tblPerson.MainGuid,312) as inspectstatus,[dbo].[fnGetInspDecStatus] (dbo.tblPerson.MainGuid,312) as InspectionDecision,
                      dbo.tblPerson.person_id, dbo.tblPerson.staff_code, dbo.tblPerson.staff_code_sort, dbo.tblPerson.cost_center_code, dbo.tblPerson.unit_id, dbo.tblPerson.first_name, 
                      dbo.tblPerson.father_name, dbo.tblPerson.grand_name, dbo.tblPerson.allowance, dbo.tblPerson.salary, dbo.tblPerson.transport_allowance, 
                      dbo.tblPerson.hardship_allowance, dbo.tblPerson.housing_allowance, dbo.tblPerson.over_time, dbo.tblPerson.pension_employee_contribution, 
                      dbo.tblPerson.pension_company_contribution, dbo.tblPerson.is_ca_member, dbo.tblPerson.ca_membership_date, dbo.tblPerson.ca_termination_date, 
                      dbo.tblPerson.thrift_contribution, dbo.tblPerson.extra_contribution, dbo.tblPerson.card_fee, dbo.tblPerson.membership_fee, 
                      dbo.tblPerson.is_membership_fee_returned, dbo.tblPerson.is_lu_member, dbo.tblPerson.labour_union, dbo.tblPerson.leave_without_pay, dbo.tblPerson.absentism, 
                      dbo.tblPerson.fine, dbo.tblPerson.sex, dbo.tblPerson.religion, dbo.tblPerson.birth_date, dbo.tblPerson.birth_place, dbo.tblPerson.marital_status, 
                      dbo.tblPerson.family_size, dbo.tblPerson.nationality, dbo.tblPerson.town, dbo.tblPerson.region, dbo.tblPerson.p_o_box, dbo.tblPerson.person_address, 
                      dbo.tblPerson.tel_home, dbo.tblPerson.tel_off_direct1, dbo.tblPerson.tel_off_direct2, dbo.tblPerson.tel_off_ext1, dbo.tblPerson.room_no, dbo.tblPerson.passport_no, 
                      dbo.tblPerson.license_grade, dbo.tblPerson.education_group, dbo.tblPerson.education_level, dbo.tblPerson.education_description, dbo.tblPerson.job_title_Sort, 
                      dbo.tblPerson.occupation, dbo.tblPerson.occupation_step, dbo.tblPerson.emp_date, dbo.tblPerson.current_status, dbo.tblPerson.Kebele, dbo.tblPerson.Worede, 
                      dbo.tblPerson.Zone, dbo.tblPerson.location,dbo.tblUnit.amDescription AS Unitdesc, 
                      dbo.tblGrade.am_description AS AmGrade,tblPromotionHistory.promotion_type as emp_status,isnull([dbo].[fnGetInspApproved]  (dbo.tblPerson.MainGuid,312),0) as Approved ";
                string strgetfrom = " FROM dbo.tblGrade INNER JOIN dbo.tblUnit INNER JOIN dbo.Organization ON dbo.tblUnit.OrgGuid = dbo.Organization.OrgGuid INNER JOIN " +
                                    " dbo.tblPosition ON tblUnit.code = dbo.tblPosition.unit_id ON dbo.tblGrade.grade_id = dbo.tblPosition.grade_id INNER JOIN dbo.tblPerson  " +
                                    " ON  dbo.tblPosition.seq_no = dbo.tblPerson.job_title  INNER JOIN dbo.tblPromotionHistory ON dbo.tblPerson.MainGuid = dbo.tblPromotionHistory.ParentGuid";
                string strgetwhere = " WHERE  (dbo.tblPromotionHistory.date_promoted between @fromTime and @todate) AND (dbo.Organization.OrgGuid = @OrgGUID) ";
                strGetAllRecords = strGetAttribute + strgetfrom + strgetwhere;

            }
            else if (DocumentType == 313)
            {
                string strGetAttribute = @"SELECT DISTINCT dbo.tblPerson.first_am_name + ' ' + dbo.tblPerson.father_am_name + ' ' + dbo.tblPerson.grand_am_name AS FullName,dbo.tblPerson.sex AS Gender,[dbo].[fnGetLookupNew] (tblTransferHistory.TransferType,401," + language + @") AS Typeofemployment,tblTransferHistory.TransferType as TransferType1 ,dbo.tblPosition.seq_no,dbo.tblPosition.position_code, [dbo].[fnGetjobtitle] (dbo.tblPerson.job_title) as job_title, dbo.tblPerson.MainGuid,[dbo].[fnGetInspectStatus] (dbo.tblPerson.MainGuid,313) as inspectstatus,[dbo].[fnGetInspDecStatus] (dbo.tblPerson.MainGuid,313) as InspectionDecision,
                      dbo.tblPerson.person_id, dbo.tblPerson.staff_code, dbo.tblPerson.staff_code_sort, dbo.tblPerson.cost_center_code, dbo.tblPerson.unit_id, dbo.tblPerson.first_name, 
                      dbo.tblPerson.father_name, dbo.tblPerson.grand_name, dbo.tblPerson.allowance, dbo.tblPerson.salary, dbo.tblPerson.transport_allowance, 
                      dbo.tblPerson.hardship_allowance, dbo.tblPerson.housing_allowance, dbo.tblPerson.over_time, dbo.tblPerson.pension_employee_contribution, 
                      dbo.tblPerson.pension_company_contribution, dbo.tblPerson.is_ca_member, dbo.tblPerson.ca_membership_date, dbo.tblPerson.ca_termination_date, 
                      dbo.tblPerson.thrift_contribution, dbo.tblPerson.extra_contribution, dbo.tblPerson.card_fee, dbo.tblPerson.membership_fee, 
                      dbo.tblPerson.is_membership_fee_returned, dbo.tblPerson.is_lu_member, dbo.tblPerson.labour_union, dbo.tblPerson.leave_without_pay, dbo.tblPerson.absentism, 
                      dbo.tblPerson.fine, dbo.tblPerson.sex, dbo.tblPerson.religion, dbo.tblPerson.birth_date, dbo.tblPerson.birth_place, dbo.tblPerson.marital_status, 
                      dbo.tblPerson.family_size, dbo.tblPerson.nationality, dbo.tblPerson.town, dbo.tblPerson.region, dbo.tblPerson.p_o_box, dbo.tblPerson.person_address, 
                      dbo.tblPerson.tel_home, dbo.tblPerson.tel_off_direct1, dbo.tblPerson.tel_off_direct2, dbo.tblPerson.tel_off_ext1, dbo.tblPerson.room_no, dbo.tblPerson.passport_no, 
                      dbo.tblPerson.license_grade, dbo.tblPerson.education_group, dbo.tblPerson.education_level, dbo.tblPerson.education_description, dbo.tblPerson.job_title_Sort, 
                      dbo.tblPerson.occupation, dbo.tblPerson.occupation_step, dbo.tblPerson.emp_date, dbo.tblPerson.current_status, dbo.tblPerson.Kebele, dbo.tblPerson.Worede, 
                      dbo.tblPerson.Zone, dbo.tblPerson.location, dbo.tblUnit.amDescription AS Unitdesc, 
                      dbo.tblGrade.am_description AS AmGrade,tblTransferHistory.TransferType as emp_status ,isnull([dbo].[fnGetInspApproved]  (dbo.tblPerson.MainGuid,313),0) as Approved ";
                string strgetfrom = " FROM   dbo.tblGrade INNER JOIN  dbo.tblUnit INNER JOIN dbo.Organization ON dbo.tblUnit.OrgGuid = dbo.Organization.OrgGuid INNER JOIN " +
                                    "  dbo.tblPosition ON tblUnit.code = dbo.tblPosition.unit_id ON dbo.tblGrade.grade_id = dbo.tblPosition.grade_id INNER JOIN " +
                                    " dbo.tblPerson ON dbo.tblPerson.job_title = dbo.tblPosition.seq_no INNER JOIN dbo.tblTransferHistory ON dbo.tblPerson.MainGuid = dbo.tblTransferHistory.ParentGuid ";
                string strgetwhere = " WHERE  (dbo.tblTransferHistory.transfer_date between @fromTime and @todate) AND (dbo.Organization.OrgGuid = @OrgGUID) ";
                strGetAllRecords = strGetAttribute + strgetfrom + strgetwhere;
            }
            else if (DocumentType == 314)
            {
                string strGetAttribute = @"SELECT DISTINCT dbo.tblPerson.first_am_name + ' ' + dbo.tblPerson.father_am_name + ' ' + dbo.tblPerson.grand_am_name AS FullName,dbo.tblPerson.sex AS Gender,[dbo].[fnGetLookupNew](dbo.tblPerson.emp_status,2," + language + @") AS Typeofemployment, dbo.tblPosition.seq_no,dbo.tblPosition.position_code, [dbo].[fnGetjobtitle] (dbo.tblPerson.job_title) as job_title, dbo.tblPerson.MainGuid,[dbo].[fnGetInspectStatus] (dbo.tblPerson.MainGuid,314) as inspectstatus,[dbo].[fnGetInspDecStatus] (dbo.tblPerson.MainGuid,314) as InspectionDecision,
                      dbo.tblPerson.person_id, dbo.tblPerson.staff_code, dbo.tblPerson.staff_code_sort, dbo.tblPerson.cost_center_code, dbo.tblPerson.unit_id, dbo.tblPerson.first_name, 
                      dbo.tblPerson.father_name, dbo.tblPerson.grand_name, dbo.tblPerson.allowance, dbo.tblPerson.salary, dbo.tblPerson.transport_allowance, 
                      dbo.tblPerson.hardship_allowance, dbo.tblPerson.housing_allowance, dbo.tblPerson.over_time, dbo.tblPerson.pension_employee_contribution, 
                      dbo.tblPerson.pension_company_contribution, dbo.tblPerson.is_ca_member, dbo.tblPerson.ca_membership_date, dbo.tblPerson.ca_termination_date, 
                      dbo.tblPerson.thrift_contribution, dbo.tblPerson.extra_contribution, dbo.tblPerson.card_fee, dbo.tblPerson.membership_fee, 
                      dbo.tblPerson.is_membership_fee_returned, dbo.tblPerson.is_lu_member, dbo.tblPerson.labour_union, dbo.tblPerson.leave_without_pay, dbo.tblPerson.absentism, 
                      dbo.tblPerson.fine, dbo.tblPerson.sex, dbo.tblPerson.religion, dbo.tblPerson.birth_date, dbo.tblPerson.birth_place, dbo.tblPerson.marital_status, 
                      dbo.tblPerson.family_size, dbo.tblPerson.nationality, dbo.tblPerson.town, dbo.tblPerson.region, dbo.tblPerson.p_o_box, dbo.tblPerson.person_address, 
                      dbo.tblPerson.tel_home, dbo.tblPerson.tel_off_direct1, dbo.tblPerson.tel_off_direct2, dbo.tblPerson.tel_off_ext1, dbo.tblPerson.room_no, dbo.tblPerson.passport_no, 
                      dbo.tblPerson.license_grade, dbo.tblPerson.education_group, dbo.tblPerson.education_level, dbo.tblPerson.education_description, dbo.tblPerson.job_title_Sort, 
                      dbo.tblPerson.occupation, dbo.tblPerson.occupation_step, dbo.tblPerson.emp_date, dbo.tblPerson.current_status, dbo.tblPerson.Kebele, dbo.tblPerson.Worede, 
                      dbo.tblPerson.Zone, dbo.tblPerson.location, dbo.tblPerson.emp_status, dbo.tblUnit.amDescription AS Unitdesc, 
                      dbo.tblGrade.am_description AS AmGrade,isnull([dbo].[fnGetInspApproved]  (dbo.tblPerson.MainGuid,314),0) as Approved ";
                string strgetfrom = " FROM   dbo.tblGrade INNER JOIN dbo.tblUnit INNER JOIN dbo.Organization ON dbo.tblUnit.OrgGuid = dbo.Organization.OrgGuid INNER JOIN " +
                                     " dbo.tblPosition ON tblUnit.code = dbo.tblPosition.unit_id ON dbo.tblGrade.grade_id = dbo.tblPosition.grade_id INNER JOIN " +
                                     " dbo.tblPerson ON dbo.tblPerson.job_title = dbo.tblPosition.seq_no INNER JOIN  dbo.tblServiceYearExtender ON dbo.tblPerson.MainGuid = dbo.tblServiceYearExtender.PersonGuid ";
                string strgetwhere = " WHERE tblServiceYearExtender.Approved = 1 and (dbo.tblServiceYearExtender.ApprovedDate between  CAST((CONVERT(datetime, '" + fromdate + "', 102)) as Date) and CAST((CONVERT(datetime, '" + todate + "', 102)) as Date)) AND (dbo.Organization.OrgGuid = @OrgGUID) ";
                strGetAllRecords = strGetAttribute + strgetfrom + strgetwhere;
            }
            else if (DocumentType == 315)
            {
                string strGetAttribute = @"SELECT DISTINCT dbo.tblPerson.first_am_name + ' ' + dbo.tblPerson.father_am_name + ' ' + dbo.tblPerson.grand_am_name AS FullName,dbo.tblPerson.sex AS Gender,[dbo].[fnGetLookupNew] (dbo.tblTermination.Termination_reason,66," + language + @") AS Typeofemployment, dbo.tblPosition.seq_no,dbo.tblPosition.position_code, [dbo].[fnGetjobtitle] (dbo.tblPerson.job_title) as job_title, dbo.tblPerson.MainGuid,[dbo].[fnGetInspectStatus] (dbo.tblPerson.MainGuid,315) as inspectstatus,[dbo].[fnGetInspDecStatus] (dbo.tblPerson.MainGuid,315) as InspectionDecision,
                      dbo.tblPerson.person_id, dbo.tblPerson.staff_code, dbo.tblPerson.staff_code_sort, dbo.tblPerson.cost_center_code, dbo.tblPerson.unit_id, dbo.tblPerson.first_name, 
                      dbo.tblPerson.father_name, dbo.tblPerson.grand_name, dbo.tblPerson.allowance, dbo.tblPerson.salary, dbo.tblPerson.transport_allowance, 
                      dbo.tblPerson.hardship_allowance, dbo.tblPerson.housing_allowance, dbo.tblPerson.over_time, dbo.tblPerson.pension_employee_contribution, 
                      dbo.tblPerson.pension_company_contribution, dbo.tblPerson.is_ca_member, dbo.tblPerson.ca_membership_date, dbo.tblPerson.ca_termination_date, 
                      dbo.tblPerson.thrift_contribution, dbo.tblPerson.extra_contribution, dbo.tblPerson.card_fee, dbo.tblPerson.membership_fee, 
                      dbo.tblPerson.is_membership_fee_returned, dbo.tblPerson.is_lu_member, dbo.tblPerson.labour_union, dbo.tblPerson.leave_without_pay, dbo.tblPerson.absentism, 
                      dbo.tblPerson.fine, dbo.tblPerson.sex, dbo.tblPerson.religion, dbo.tblPerson.birth_date, dbo.tblPerson.birth_place, dbo.tblPerson.marital_status, 
                      dbo.tblPerson.family_size, dbo.tblPerson.nationality, dbo.tblPerson.town, dbo.tblPerson.region, dbo.tblPerson.p_o_box, dbo.tblPerson.person_address, 
                      dbo.tblPerson.tel_home, dbo.tblPerson.tel_off_direct1, dbo.tblPerson.tel_off_direct2, dbo.tblPerson.tel_off_ext1, dbo.tblPerson.room_no, dbo.tblPerson.passport_no, 
                      dbo.tblPerson.license_grade, dbo.tblPerson.education_group, dbo.tblPerson.education_level, dbo.tblPerson.education_description, dbo.tblPerson.job_title_Sort, 
                      dbo.tblPerson.occupation, dbo.tblPerson.occupation_step, dbo.tblPerson.emp_date, dbo.tblPerson.current_status, dbo.tblPerson.Kebele, dbo.tblPerson.Worede, 
                      dbo.tblPerson.Zone, dbo.tblPerson.location,dbo.tblUnit.amDescription AS Unitdesc, 
                      dbo.tblGrade.am_description AS AmGrade,dbo.tblTermination.Termination_reason as emp_status ,isnull([dbo].[fnGetInspApproved]  (dbo.tblPerson.MainGuid,315),0) as Approved ";
                string strgetfrom = " FROM dbo.tblGrade INNER JOIN dbo.tblUnit INNER JOIN  dbo.Organization ON dbo.tblUnit.OrgGuid = dbo.Organization.OrgGuid INNER JOIN " +
                                   " dbo.tblPosition ON tblUnit.code = dbo.tblPosition.unit_id ON dbo.tblGrade.grade_id = dbo.tblPosition.grade_id INNER JOIN " +
                                   " dbo.tblPerson ON dbo.tblPerson.job_title = dbo.tblPosition.seq_no INNER JOIN  dbo.tblTermination ON dbo.tblPerson.MainGuid = dbo.tblTermination.ParentGuid ";
                string strgetwhere = " WHERE  (dbo.tblTermination.Terminationdate between @fromTime and @todate) AND (dbo.Organization.OrgGuid = @OrgGUID) ";
                strGetAllRecords = strGetAttribute + strgetfrom + strgetwhere;
            }
            else if (DocumentType == 316)
            {
                string strGetAttribute = @"SELECT DISTINCT dbo.tblPerson.first_am_name + ' ' + dbo.tblPerson.father_am_name + ' ' + dbo.tblPerson.grand_am_name AS FullName,dbo.tblPerson.sex AS Gender,[dbo].[fnGetLookupNew] (dbo.tblPerson.emp_status,2," + language + @") AS Typeofemployment, dbo.tblPosition.seq_no,dbo.tblPosition.position_code, [dbo].[fnGetjobtitle] (dbo.tblPerson.job_title) as job_title, dbo.tblPerson.MainGuid,[dbo].[fnGetInspectStatus] (dbo.tblPerson.MainGuid,316) as inspectstatus,[dbo].[fnGetInspDecStatus] (dbo.tblPerson.MainGuid,316) as InspectionDecision,
                      dbo.tblPerson.person_id, dbo.tblPerson.staff_code, dbo.tblPerson.staff_code_sort, dbo.tblPerson.cost_center_code, dbo.tblPerson.unit_id, dbo.tblPerson.first_name, 
                      dbo.tblPerson.father_name, dbo.tblPerson.grand_name, dbo.tblPerson.allowance, dbo.tblPerson.salary, dbo.tblPerson.transport_allowance, 
                      dbo.tblPerson.hardship_allowance, dbo.tblPerson.housing_allowance, dbo.tblPerson.over_time, dbo.tblPerson.pension_employee_contribution, 
                      dbo.tblPerson.pension_company_contribution, dbo.tblPerson.is_ca_member, dbo.tblPerson.ca_membership_date, dbo.tblPerson.ca_termination_date, 
                      dbo.tblPerson.thrift_contribution, dbo.tblPerson.extra_contribution, dbo.tblPerson.card_fee, dbo.tblPerson.membership_fee, 
                      dbo.tblPerson.is_membership_fee_returned, dbo.tblPerson.is_lu_member, dbo.tblPerson.labour_union, dbo.tblPerson.leave_without_pay, dbo.tblPerson.absentism, 
                      dbo.tblPerson.fine, dbo.tblPerson.sex, dbo.tblPerson.religion, dbo.tblPerson.birth_date, dbo.tblPerson.birth_place, dbo.tblPerson.marital_status, 
                      dbo.tblPerson.family_size, dbo.tblPerson.nationality, dbo.tblPerson.town, dbo.tblPerson.region, dbo.tblPerson.p_o_box, dbo.tblPerson.person_address, 
                      dbo.tblPerson.tel_home, dbo.tblPerson.tel_off_direct1, dbo.tblPerson.tel_off_direct2, dbo.tblPerson.tel_off_ext1, dbo.tblPerson.room_no, dbo.tblPerson.passport_no, 
                      dbo.tblPerson.license_grade, dbo.tblPerson.education_group, dbo.tblPerson.education_level, dbo.tblPerson.education_description, dbo.tblPerson.job_title_Sort, 
                      dbo.tblPerson.occupation, dbo.tblPerson.occupation_step, dbo.tblPerson.emp_date, dbo.tblPerson.current_status, dbo.tblPerson.Kebele, dbo.tblPerson.Worede, 
                      dbo.tblPerson.Zone, dbo.tblPerson.location,  dbo.tblPerson.emp_status, dbo.tblUnit.amDescription AS Unitdesc, 
                      dbo.tblGrade.am_description AS AmGrade,isnull([dbo].[fnGetInspApproved]  (dbo.tblPerson.MainGuid,316),0) as Approved  ";
                string strgetfrom = " FROM  dbo.tblGrade INNER JOIN dbo.tblUnit INNER JOIN dbo.Organization ON dbo.tblUnit.OrgGuid = dbo.Organization.OrgGuid INNER JOIN " +
                                    " dbo.tblPosition ON tblUnit.code = dbo.tblPosition.unit_id ON dbo.tblGrade.grade_id = dbo.tblPosition.grade_id INNER JOIN " +
                                    " dbo.tblPerson ON dbo.tblPerson.job_title = dbo.tblPosition.seq_no INNER JOIN dbo.tblDicipline ON dbo.tblPerson.MainGuid = dbo.tblDicipline.ParentGuid ";
                string strgetwhere = " WHERE  ( dbo.tblDicipline.date_action_taken between @fromTime and @todate) AND (dbo.Organization.OrgGuid = @OrgGUID) ";
                strGetAllRecords = strGetAttribute + strgetfrom + strgetwhere;
            }
            else if (DocumentType == 317)
            {
                string strGetAttribute = @"SELECT DISTINCT dbo.tblPerson.first_am_name + ' ' + dbo.tblPerson.father_am_name + ' ' + dbo.tblPerson.grand_am_name AS FullName,dbo.tblPerson.sex AS Gender,[dbo].[fnGetLookupNew] (dbo.tblPerson.emp_status,2," + language + @") AS Typeofemployment, dbo.tblPosition.seq_no,dbo.tblPosition.position_code, [dbo].[fnGetjobtitle] (dbo.tblPerson.job_title) as job_title, dbo.tblPerson.MainGuid,[dbo].[fnGetInspectStatus] (dbo.tblPerson.MainGuid,317) as inspectstatus,[dbo].[fnGetInspDecStatus] (dbo.tblPerson.MainGuid,317) as InspectionDecision,
                      dbo.tblPerson.person_id, dbo.tblPerson.staff_code, dbo.tblPerson.staff_code_sort, dbo.tblPerson.cost_center_code, dbo.tblPerson.unit_id, dbo.tblPerson.first_name, 
                      dbo.tblPerson.father_name, dbo.tblPerson.grand_name, dbo.tblPerson.allowance, dbo.tblPerson.salary, dbo.tblPerson.transport_allowance, 
                      dbo.tblPerson.hardship_allowance, dbo.tblPerson.housing_allowance, dbo.tblPerson.over_time, dbo.tblPerson.pension_employee_contribution, 
                      dbo.tblPerson.pension_company_contribution, dbo.tblPerson.is_ca_member, dbo.tblPerson.ca_membership_date, dbo.tblPerson.ca_termination_date, 
                      dbo.tblPerson.thrift_contribution, dbo.tblPerson.extra_contribution, dbo.tblPerson.card_fee, dbo.tblPerson.membership_fee, 
                      dbo.tblPerson.is_membership_fee_returned, dbo.tblPerson.is_lu_member, dbo.tblPerson.labour_union, dbo.tblPerson.leave_without_pay, dbo.tblPerson.absentism, 
                      dbo.tblPerson.fine, dbo.tblPerson.sex, dbo.tblPerson.religion, dbo.tblPerson.birth_date, dbo.tblPerson.birth_place, dbo.tblPerson.marital_status, 
                      dbo.tblPerson.family_size, dbo.tblPerson.nationality, dbo.tblPerson.town, dbo.tblPerson.region, dbo.tblPerson.p_o_box, dbo.tblPerson.person_address, 
                      dbo.tblPerson.tel_home, dbo.tblPerson.tel_off_direct1, dbo.tblPerson.tel_off_direct2, dbo.tblPerson.tel_off_ext1, dbo.tblPerson.room_no, dbo.tblPerson.passport_no, 
                      dbo.tblPerson.license_grade, dbo.tblPerson.education_group, dbo.tblPerson.education_level, dbo.tblPerson.education_description, dbo.tblPerson.job_title_Sort, 
                      dbo.tblPerson.occupation, dbo.tblPerson.occupation_step, dbo.tblPerson.emp_date, dbo.tblPerson.current_status, dbo.tblPerson.Kebele, dbo.tblPerson.Worede, 
                      dbo.tblPerson.Zone, dbo.tblPerson.location, dbo.tblPerson.emp_status, dbo.tblUnit.amDescription AS Unitdesc, 
                      dbo.tblGrade.am_description AS AmGrade,isnull([dbo].[fnGetInspApproved]  (dbo.tblPerson.MainGuid,317),0) as Approved  ";
                string strgetfrom = " FROM   dbo.tblGrade INNER JOIN  dbo.tblUnit INNER JOIN  dbo.Organization ON dbo.tblUnit.OrgGuid = dbo.Organization.OrgGuid INNER JOIN " +
                                    " dbo.tblPosition ON tblUnit.code = dbo.tblPosition.unit_id ON dbo.tblGrade.grade_id = dbo.tblPosition.grade_id INNER JOIN " +
                                    " dbo.tblPerson ON dbo.tblPerson.job_title = dbo.tblPosition.seq_no INNER JOIN        dbo.Attachment ON dbo.tblPerson.MainGuid = dbo.Attachment.ParentGuid";
                string strgetwhere = " WHERE  ( dbo.Attachment.SentDate between @fromTime and @todate) AND (dbo.Organization.OrgGuid = @OrgGUID) ";
                strGetAllRecords = strGetAttribute + strgetfrom + strgetwhere;
            }
            else if (DocumentType == 318)
            {
                string strGetAttribute = @"SELECT DISTINCT dbo.tblPerson.first_am_name + ' ' + dbo.tblPerson.father_am_name + ' ' + dbo.tblPerson.grand_am_name AS FullName,dbo.tblPerson.sex AS Gender,[dbo].[fnGetLookupNew] (dbo.tblPerson.emp_status,2," + language + @") AS Typeofemployment, dbo.tblPosition.seq_no,dbo.tblPosition.position_code, [dbo].[fnGetjobtitle] (dbo.tblPerson.job_title) as job_title, dbo.tblPerson.MainGuid,[dbo].[fnGetInspectStatus] (dbo.tblPerson.MainGuid,318) as inspectstatus,[dbo].[fnGetInspDecStatus] (dbo.tblPerson.MainGuid,318) as InspectionDecision,
                                              dbo.tblPerson.person_id, dbo.tblPerson.staff_code, dbo.tblPerson.staff_code_sort, dbo.tblPerson.cost_center_code, dbo.tblPerson.unit_id, dbo.tblPerson.first_name, 
                                              dbo.tblPerson.father_name, dbo.tblPerson.grand_name, dbo.tblPerson.allowance, dbo.tblPerson.salary, dbo.tblPerson.transport_allowance, 
                                              dbo.tblPerson.hardship_allowance, dbo.tblPerson.housing_allowance, dbo.tblPerson.over_time, dbo.tblPerson.pension_employee_contribution, 
                                              dbo.tblPerson.pension_company_contribution, dbo.tblPerson.is_ca_member, dbo.tblPerson.ca_membership_date, dbo.tblPerson.ca_termination_date, 
                                              dbo.tblPerson.thrift_contribution, dbo.tblPerson.extra_contribution, dbo.tblPerson.card_fee, dbo.tblPerson.membership_fee, 
                                              dbo.tblPerson.is_membership_fee_returned, dbo.tblPerson.is_lu_member, dbo.tblPerson.labour_union, dbo.tblPerson.leave_without_pay, dbo.tblPerson.absentism, 
                                              dbo.tblPerson.fine, dbo.tblPerson.sex, dbo.tblPerson.religion, dbo.tblPerson.birth_date, dbo.tblPerson.birth_place, dbo.tblPerson.marital_status, 
                                              dbo.tblPerson.family_size, dbo.tblPerson.nationality, dbo.tblPerson.town, dbo.tblPerson.region, dbo.tblPerson.p_o_box, dbo.tblPerson.person_address, 
                                              dbo.tblPerson.tel_home, dbo.tblPerson.tel_off_direct1, dbo.tblPerson.tel_off_direct2, dbo.tblPerson.tel_off_ext1, dbo.tblPerson.room_no, dbo.tblPerson.passport_no, 
                                              dbo.tblPerson.license_grade, dbo.tblPerson.education_group, dbo.tblPerson.education_level, dbo.tblPerson.education_description, dbo.tblPerson.job_title_Sort, 
                                              dbo.tblPerson.occupation, dbo.tblPerson.occupation_step, dbo.tblPerson.emp_date, dbo.tblPerson.current_status, dbo.tblPerson.Kebele, dbo.tblPerson.Worede, 
                                              dbo.tblPerson.Zone, dbo.tblPerson.location, dbo.tblPerson.emp_status, dbo.tblUnit.amDescription AS Unitdesc, 
                                              dbo.tblGrade.am_description AS AmGrade, ,isnull([dbo].[fnGetInspApproved]  (dbo.tblPerson.MainGuid,318),0) as Approved  ";
                string strgetfrom = " FROM  dbo.tblGrade INNER JOIN    dbo.tblUnit INNER JOIN  dbo.Organization ON dbo.tblUnit.OrgGuid = dbo.Organization.OrgGuid INNER JOIN " +
                                  " dbo.tblPosition ON tblUnit.code = dbo.tblPosition.unit_id ON dbo.tblGrade.grade_id = dbo.tblPosition.grade_id INNER JOIN " +
                                  " dbo.tblPerson ON dbo.tblPerson.job_title = dbo.tblPosition.seq_no INNER JOIN  dbo.Attachment ON dbo.tblPerson.MainGuid =  dbo.Attachment.ParentGuid ";
                string strgetwhere = " WHERE  (dbo.Attachment.SentDate between @fromTime and @todate) AND (dbo.Organization.OrgGuid = @OrgGUID) and dbo.Attachment.HRActivityType=318";
                strGetAllRecords = strGetAttribute + strgetfrom + strgetwhere;
            }
            else if (DocumentType == 319)
            {
                string strGetAttribute = @"SELECT DISTINCT dbo.tblPerson.first_am_name + ' ' + dbo.tblPerson.father_am_name + ' ' + dbo.tblPerson.grand_am_name AS FullName,dbo.tblPerson.sex AS Gender,[dbo].[fnGetLookupNew] (dbo.tblPerson.emp_status,2," + language + @") AS Typeofemployment, dbo.tblPosition.seq_no,dbo.tblPosition.position_code, [dbo].[fnGetjobtitle] (dbo.tblPerson.job_title) as job_title, dbo.tblPerson.MainGuid,[dbo].[fnGetInspectStatus] (dbo.tblPerson.MainGuid,319) as inspectstatus,[dbo].[fnGetInspDecStatus] (dbo.tblPerson.MainGuid,319) as InspectionDecision,
                      dbo.tblPerson.person_id, dbo.tblPerson.staff_code, dbo.tblPerson.staff_code_sort, dbo.tblPerson.cost_center_code, dbo.tblPerson.unit_id, dbo.tblPerson.first_name, 
                      dbo.tblPerson.father_name, dbo.tblPerson.grand_name, dbo.tblPerson.allowance, dbo.tblPerson.salary, dbo.tblPerson.transport_allowance, 
                      dbo.tblPerson.hardship_allowance, dbo.tblPerson.housing_allowance, dbo.tblPerson.over_time, dbo.tblPerson.pension_employee_contribution, 
                      dbo.tblPerson.pension_company_contribution, dbo.tblPerson.is_ca_member, dbo.tblPerson.ca_membership_date, dbo.tblPerson.ca_termination_date, 
                      dbo.tblPerson.thrift_contribution, dbo.tblPerson.extra_contribution, dbo.tblPerson.card_fee, dbo.tblPerson.membership_fee, 
                      dbo.tblPerson.is_membership_fee_returned, dbo.tblPerson.is_lu_member, dbo.tblPerson.labour_union, dbo.tblPerson.leave_without_pay, dbo.tblPerson.absentism, 
                      dbo.tblPerson.fine, dbo.tblPerson.sex, dbo.tblPerson.religion, dbo.tblPerson.birth_date, dbo.tblPerson.birth_place, dbo.tblPerson.marital_status, 
                      dbo.tblPerson.family_size, dbo.tblPerson.nationality, dbo.tblPerson.town, dbo.tblPerson.region, dbo.tblPerson.p_o_box, dbo.tblPerson.person_address, 
                      dbo.tblPerson.tel_home, dbo.tblPerson.tel_off_direct1, dbo.tblPerson.tel_off_direct2, dbo.tblPerson.tel_off_ext1, dbo.tblPerson.room_no, dbo.tblPerson.passport_no, 
                      dbo.tblPerson.license_grade, dbo.tblPerson.education_group, dbo.tblPerson.education_level, dbo.tblPerson.education_description, dbo.tblPerson.job_title_Sort, 
                      dbo.tblPerson.occupation, dbo.tblPerson.occupation_step, dbo.tblPerson.emp_date, dbo.tblPerson.current_status, dbo.tblPerson.Kebele, dbo.tblPerson.Worede, 
                      dbo.tblPerson.Zone, dbo.tblPerson.location,  dbo.tblPerson.emp_status, dbo.tblUnit.amDescription AS Unitdesc, 
                      dbo.tblGrade.am_description AS AmGrade,isnull([dbo].[fnGetInspApproved]  (dbo.tblPerson.MainGuid,319),0) as Approved  ";
                string strgetfrom = " FROM  dbo.tblGrade INNER JOIN  dbo.tblUnit INNER JOIN dbo.Organization ON dbo.tblUnit.OrgGuid = dbo.Organization.OrgGuid INNER JOIN " +
                                    " dbo.tblPosition ON tblUnit.code = dbo.tblPosition.unit_id ON dbo.tblGrade.grade_id = dbo.tblPosition.grade_id INNER JOIN " +
                                    " dbo.tblPerson ON dbo.tblPerson.job_title = dbo.tblPosition.seq_no INNER JOIN dbo.tblEducationHistory ON dbo.tblPerson.MainGuid =  dbo.tblEducationHistory.ParentGuid ";
                string strgetwhere = " WHERE tblEducationHistory.Given_by_the_Org = 1 and  (dbo.tblEducationHistory.period_from between @fromTime and @todate) AND (dbo.Organization.OrgGuid = @OrgGUID) ";

                strGetAllRecords = strGetAttribute + strgetfrom + strgetwhere;
            }
            else if (DocumentType == 320)
            {
                string strGetAttribute = @"SELECT DISTINCT dbo.tblPerson.first_am_name + ' ' + dbo.tblPerson.father_am_name + ' ' + dbo.tblPerson.grand_am_name AS FullName,dbo.tblPerson.sex AS Gender,[dbo].[fnGetLookupNew] (dbo.tblPerson.emp_status,2," + language + @") AS Typeofemployment, dbo.tblPosition.seq_no,dbo.tblPosition.position_code, [dbo].[fnGetjobtitle] (dbo.tblPerson.job_title) as job_title, dbo.tblPerson.MainGuid,[dbo].[fnGetInspectStatus] (dbo.tblPerson.MainGuid,320) as inspectstatus,[dbo].[fnGetInspDecStatus] (dbo.tblPerson.MainGuid,320) as InspectionDecision,
                      dbo.tblPerson.person_id, dbo.tblPerson.staff_code, dbo.tblPerson.staff_code_sort, dbo.tblPerson.cost_center_code, dbo.tblPerson.unit_id, dbo.tblPerson.first_name, 
                      dbo.tblPerson.father_name, dbo.tblPerson.grand_name, dbo.tblPerson.allowance, dbo.tblPerson.salary, dbo.tblPerson.transport_allowance, 
                      dbo.tblPerson.hardship_allowance, dbo.tblPerson.housing_allowance, dbo.tblPerson.over_time, dbo.tblPerson.pension_employee_contribution, 
                      dbo.tblPerson.pension_company_contribution, dbo.tblPerson.is_ca_member, dbo.tblPerson.ca_membership_date, dbo.tblPerson.ca_termination_date, 
                      dbo.tblPerson.thrift_contribution, dbo.tblPerson.extra_contribution, dbo.tblPerson.card_fee, dbo.tblPerson.membership_fee, 
                      dbo.tblPerson.is_membership_fee_returned, dbo.tblPerson.is_lu_member, dbo.tblPerson.labour_union, dbo.tblPerson.leave_without_pay, dbo.tblPerson.absentism, 
                      dbo.tblPerson.fine, dbo.tblPerson.sex, dbo.tblPerson.religion, dbo.tblPerson.birth_date, dbo.tblPerson.birth_place, dbo.tblPerson.marital_status, 
                      dbo.tblPerson.family_size, dbo.tblPerson.nationality, dbo.tblPerson.town, dbo.tblPerson.region, dbo.tblPerson.p_o_box, dbo.tblPerson.person_address, 
                      dbo.tblPerson.tel_home, dbo.tblPerson.tel_off_direct1, dbo.tblPerson.tel_off_direct2, dbo.tblPerson.tel_off_ext1, dbo.tblPerson.room_no, dbo.tblPerson.passport_no, 
                      dbo.tblPerson.license_grade, dbo.tblPerson.education_group, dbo.tblPerson.education_level, dbo.tblPerson.education_description, dbo.tblPerson.job_title_Sort, 
                      dbo.tblPerson.occupation, dbo.tblPerson.occupation_step, dbo.tblPerson.emp_date, dbo.tblPerson.current_status, dbo.tblPerson.Kebele, dbo.tblPerson.Worede, 
                      dbo.tblPerson.Zone, dbo.tblPerson.location, dbo.tblPerson.emp_status, dbo.tblUnit.amDescription AS Unitdesc, 
                      dbo.tblGrade.am_description AS AmGrade,isnull([dbo].[fnGetInspApproved]  (dbo.tblPerson.MainGuid,320),0) as Approved ";
                string strgetfrom = " FROM   dbo.tblGrade INNER JOIN  dbo.tblUnit INNER JOIN dbo.Organization ON dbo.tblUnit.OrgGuid = dbo.Organization.OrgGuid INNER JOIN " +
                                    " dbo.tblPosition ON tblUnit.code = dbo.tblPosition.unit_id ON dbo.tblGrade.grade_id = dbo.tblPosition.grade_id INNER JOIN " +
                                    "  dbo.tblPerson ON dbo.tblPerson.job_title = dbo.tblPosition.seq_no INNER JOIN dbo.tblSpecialAssignment ON dbo.tblPerson.MainGuid = dbo.tblSpecialAssignment.ParentGuid ";
                string strgetwhere = " WHERE (dbo.tblSpecialAssignment.from_date between  CAST((CONVERT(datetime, '" + fromdate + "', 102)) as Date) and CAST((CONVERT(datetime, '" + todate + "', 102)) as Date)) AND (dbo.Organization.OrgGuid = @OrgGUID) ";

                strGetAllRecords = strGetAttribute + strgetfrom + strgetwhere;
            }
            else if (DocumentType == 322)
            {
                string strGetAttribute = @"SELECT DISTINCT dbo.tblPerson.first_am_name + ' ' + dbo.tblPerson.father_am_name + ' ' + dbo.tblPerson.grand_am_name AS FullName,dbo.tblPerson.sex AS Gender,[dbo].[fnGetLookupNew] (dbo.tblPerson.emp_status,2," + language + @") AS Typeofemployment, dbo.tblPosition.seq_no,dbo.tblPosition.position_code, [dbo].[fnGetjobtitle] (dbo.tblPerson.job_title) as job_title, dbo.tblPerson.MainGuid,[dbo].[fnGetInspectStatus] (dbo.tblPerson.MainGuid,312) as inspectstatus,[dbo].[fnGetInspDecStatus] (dbo.tblPerson.MainGuid,312) as InspectionDecision,
                      dbo.tblPerson.person_id, dbo.tblPerson.staff_code, dbo.tblPerson.staff_code_sort, dbo.tblPerson.cost_center_code, dbo.tblPerson.unit_id, dbo.tblPerson.first_name, 
                      dbo.tblPerson.father_name, dbo.tblPerson.grand_name, dbo.tblPerson.allowance, dbo.tblPerson.salary, dbo.tblPerson.transport_allowance, 
                      dbo.tblPerson.hardship_allowance, dbo.tblPerson.housing_allowance, dbo.tblPerson.over_time, dbo.tblPerson.pension_employee_contribution, 
                      dbo.tblPerson.pension_company_contribution, dbo.tblPerson.is_ca_member, dbo.tblPerson.ca_membership_date, dbo.tblPerson.ca_termination_date, 
                      dbo.tblPerson.thrift_contribution, dbo.tblPerson.extra_contribution, dbo.tblPerson.card_fee, dbo.tblPerson.membership_fee, 
                      dbo.tblPerson.is_membership_fee_returned, dbo.tblPerson.is_lu_member, dbo.tblPerson.labour_union, dbo.tblPerson.leave_without_pay, dbo.tblPerson.absentism, 
                      dbo.tblPerson.fine, dbo.tblPerson.sex, dbo.tblPerson.religion, dbo.tblPerson.birth_date, dbo.tblPerson.birth_place, dbo.tblPerson.marital_status, 
                      dbo.tblPerson.family_size, dbo.tblPerson.nationality, dbo.tblPerson.town, dbo.tblPerson.region, dbo.tblPerson.p_o_box, dbo.tblPerson.person_address, 
                      dbo.tblPerson.tel_home, dbo.tblPerson.tel_off_direct1, dbo.tblPerson.tel_off_direct2, dbo.tblPerson.tel_off_ext1, dbo.tblPerson.room_no, dbo.tblPerson.passport_no, 
                      dbo.tblPerson.license_grade, dbo.tblPerson.education_group, dbo.tblPerson.education_level, dbo.tblPerson.education_description, dbo.tblPerson.job_title_Sort, 
                      dbo.tblPerson.occupation, dbo.tblPerson.occupation_step, dbo.tblPerson.emp_date, dbo.tblPerson.current_status, dbo.tblPerson.Kebele, dbo.tblPerson.Worede, 
                      dbo.tblPerson.Zone, dbo.tblPerson.location, dbo.tblPerson.emp_status, dbo.tblUnit.amDescription AS Unitdesc, 
                      dbo.tblGrade.am_description AS AmGrade,isnull([dbo].[fnGetInspApproved]  (dbo.tblPerson.MainGuid,322),0) as Approved  ";
                string strgetfrom = " FROM dbo.tblGrade INNER JOIN dbo.tblUnit INNER JOIN dbo.Organization ON dbo.tblUnit.OrgGuid = dbo.Organization.OrgGuid INNER JOIN " +
                                    " dbo.tblPosition ON tblUnit.code = dbo.tblPosition.unit_id ON dbo.tblGrade.grade_id = dbo.tblPosition.grade_id INNER JOIN dbo.tblPerson  " +
                                    " ON  dbo.tblPosition.seq_no = dbo.tblPerson.job_title  INNER JOIN dbo.tblPromotionHistory ON dbo.tblPerson.MainGuid = dbo.tblPromotionHistory.ParentGuid";
                string strgetwhere = " WHERE  (dbo.tblPromotionHistory.date_promoted between @fromTime and @todate) AND (dbo.Organization.OrgGuid = @OrgGUID) And tblPromotionHistory.promotion_type=4";
                strGetAllRecords = strGetAttribute + strgetfrom + strgetwhere;

            }
            else if (DocumentType == 341)
            {
                string strGetAttribute = @"SELECT DISTINCT dbo.tblPerson.first_am_name + ' ' + dbo.tblPerson.father_am_name + ' ' + dbo.tblPerson.grand_am_name AS FullName,dbo.tblPerson.sex AS Gender,[dbo].[fnGetLookupNew] (dbo.tblPerson.emp_status,2," + language + @") AS Typeofemployment, dbo.tblPosition.seq_no,dbo.tblPosition.position_code, [dbo].[fnGetjobtitle] (dbo.tblPerson.job_title) as job_title, dbo.tblPerson.MainGuid,[dbo].[fnGetInspectStatus] (dbo.tblPerson.MainGuid,312) as inspectstatus,[dbo].[fnGetInspDecStatus] (dbo.tblPerson.MainGuid,312) as InspectionDecision,
                      dbo.tblPerson.person_id, dbo.tblPerson.staff_code, dbo.tblPerson.staff_code_sort, dbo.tblPerson.cost_center_code, dbo.tblPerson.unit_id, dbo.tblPerson.first_name, 
                      dbo.tblPerson.father_name, dbo.tblPerson.grand_name, dbo.tblPerson.allowance, dbo.tblPerson.salary, dbo.tblPerson.transport_allowance, 
                      dbo.tblPerson.hardship_allowance, dbo.tblPerson.housing_allowance, dbo.tblPerson.over_time, dbo.tblPerson.pension_employee_contribution, 
                      dbo.tblPerson.pension_company_contribution, dbo.tblPerson.is_ca_member, dbo.tblPerson.ca_membership_date, dbo.tblPerson.ca_termination_date, 
                      dbo.tblPerson.thrift_contribution, dbo.tblPerson.extra_contribution, dbo.tblPerson.card_fee, dbo.tblPerson.membership_fee, 
                      dbo.tblPerson.is_membership_fee_returned, dbo.tblPerson.is_lu_member, dbo.tblPerson.labour_union, dbo.tblPerson.leave_without_pay, dbo.tblPerson.absentism, 
                      dbo.tblPerson.fine, dbo.tblPerson.sex, dbo.tblPerson.religion, dbo.tblPerson.birth_date, dbo.tblPerson.birth_place, dbo.tblPerson.marital_status, 
                      dbo.tblPerson.family_size, dbo.tblPerson.nationality, dbo.tblPerson.town, dbo.tblPerson.region, dbo.tblPerson.p_o_box, dbo.tblPerson.person_address, 
                      dbo.tblPerson.tel_home, dbo.tblPerson.tel_off_direct1, dbo.tblPerson.tel_off_direct2, dbo.tblPerson.tel_off_ext1, dbo.tblPerson.room_no, dbo.tblPerson.passport_no, 
                      dbo.tblPerson.license_grade, dbo.tblPerson.education_group, dbo.tblPerson.education_level, dbo.tblPerson.education_description, dbo.tblPerson.job_title_Sort, 
                      dbo.tblPerson.occupation, dbo.tblPerson.occupation_step, dbo.tblPerson.emp_date, dbo.tblPerson.current_status, dbo.tblPerson.Kebele, dbo.tblPerson.Worede, 
                      dbo.tblPerson.Zone, dbo.tblPerson.location, dbo.tblPerson.emp_status, dbo.tblUnit.amDescription AS Unitdesc, 
                      dbo.tblGrade.am_description AS AmGrade ,isnull([dbo].[fnGetInspApproved]  (dbo.tblPerson.MainGuid,341),0) as Approved ";
                string strgetfrom = " FROM dbo.tblGrade INNER JOIN dbo.tblUnit INNER JOIN dbo.Organization ON dbo.tblUnit.OrgGuid = dbo.Organization.OrgGuid INNER JOIN " +
                                    " dbo.tblPosition ON tblUnit.code = dbo.tblPosition.unit_id ON dbo.tblGrade.grade_id = dbo.tblPosition.grade_id INNER JOIN dbo.tblPerson  " +
                                    " ON  dbo.tblPosition.seq_no = dbo.tblPerson.job_title  INNER JOIN dbo.tblPromotionHistory ON dbo.tblPerson.MainGuid = dbo.tblPromotionHistory.ParentGuid";
                string strgetwhere = " WHERE  (dbo.tblPromotionHistory.date_promoted between @fromTime and @todate) AND (dbo.Organization.OrgGuid = @OrgGUID) And tblPromotionHistory.promotion_type=6";
                strGetAllRecords = strGetAttribute + strgetfrom + strgetwhere;

            }
            if (isHrOfficer)
            {
                strGetAllRecords = " Select x.* from ( " + strGetAllRecords + " ) x where x.Approved = 1";
            }
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("PersonEmploymentEntityView");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;
            if (strGetAllRecords != "")
            {
                try
                {
                    command.Parameters.Add(new SqlParameter("@OrgGUID", OrgGuid));
                    command.Parameters.Add(new SqlParameter("@fromTime", fromdate));
                    command.Parameters.Add(new SqlParameter("@todate", todate));
                    connection.Open();

                    adapter.Fill(dTable);

                }
                catch (Exception ex)
                {
                    throw new Exception("PersonEmploymentEntityView::GetRecords::Error!" + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                    command.Dispose();
                    adapter.Dispose();
                }
                return dTable;
            }

            return dTable;
        }



        public PersonEmploymentEntityView GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            PersonEmploymentEntityView objPersonEmploymentEntityView = new PersonEmploymentEntityView();
            string strGetRecord = @"SELECT dbo.tblPerson.MainGuid, dbo.tblPerson.first_am_name, dbo.tblPerson.father_am_name, dbo.tblPerson.grand_am_name,  dbo.Organization.DescriptionAm as org_name, dbo.Organization.OrgGuid as Org_Code FROM   dbo.tblPerson INNER JOIN
                      dbo.Organization ON dbo.tblPerson.OrgGuid = dbo.Organization.OrgGuid where dbo.tblPerson.MainGuid=@personguid ";
            //string strGetRecord = @"SELECT [first_am_name],[father_am_name],[grand_am_name],[Gender],[birth_date],[Typeofemployment],[person_id],[staff_code],[staff_code_sort],[cost_center_code],[first_name],[father_name],[allowance],[salary],[transport_allowance],[hardship_allowance],[housing_allowance],[over_time],[pension_employee_contribution],[pension_company_contribution],[is_ca_member],[ca_membership_date],[ca_termination_date],[thrift_contribution],[extra_contribution],[card_fee],[membership_fee],[is_membership_fee_returned],[is_lu_member],[labour_union],[leave_without_pay],[absentism],[fine],[sex],[religion],[birth_place],[marital_status],[family_size],[nationality],[town],[region],[p_o_box],[person_address],[tel_home],[tel_off_direct1],[tel_off_direct2],[tel_off_ext1],[room_no],[passport_no],[license_grade],[education_group],[education_level],[education_description],[job_title],[job_title_Sort],[occupation],[occupation_step],[emp_date],[emp_letter],[current_status],[date_termination],[termination_reason],[mother_tongue],[ethnicity],[location],[id],[org_Code],[org_name],[first_name_sort],[father_name_sort],[grand_name_sort],[first_name_soundeX],[father_name_soundeX],[grand_name_soundeX],[leave_balance],[pension_no],[is_teacher],[is_police],[working_days],[cost_sharing_total_amount],[cost_sharing_total_paid],[cost_sharing_monthly_payment],[Health_Status] FROM [dbo].[PersonEmploymentEntityView] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("PersonEmploymentEntityView");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@personguid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objPersonEmploymentEntityView.Org_Code = (Guid)dTable.Rows[0]["Org_Code"];
                    if (dTable.Rows[0]["first_am_name"].Equals(DBNull.Value))
                        objPersonEmploymentEntityView.First_am_name = string.Empty;
                    else
                        objPersonEmploymentEntityView.First_am_name = (string)dTable.Rows[0]["first_am_name"];
                    if (dTable.Rows[0]["father_am_name"].Equals(DBNull.Value))
                        objPersonEmploymentEntityView.Father_am_name = string.Empty;
                    else
                        objPersonEmploymentEntityView.Father_am_name = (string)dTable.Rows[0]["father_am_name"];
                    if (dTable.Rows[0]["grand_am_name"].Equals(DBNull.Value))
                        objPersonEmploymentEntityView.Grand_am_name = string.Empty;
                    else
                        objPersonEmploymentEntityView.Grand_am_name = (string)dTable.Rows[0]["grand_am_name"];
                    if (dTable.Rows[0]["org_name"].Equals(DBNull.Value))
                        objPersonEmploymentEntityView.Org_name = string.Empty;
                    else
                        objPersonEmploymentEntityView.Org_name = (string)dTable.Rows[0]["org_name"];


                }

            }
            catch (Exception ex)
            {
                throw new Exception("PersonEmploymentEntityView::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objPersonEmploymentEntityView;
        }
        public bool Insert(PersonEmploymentEntityView objPersonEmploymentEntityView)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[PersonEmploymentEntityView]
                                            ([first_am_name],[father_am_name],[grand_am_name],[Gender],[birth_date],[Typeofemployment],[job_title],[emp_date],[MainGuid],[date_termination])
                                     VALUES    (@first_am_name,@father_am_name,@grand_am_name,@Gender,@birth_date,@Typeofemployment,@job_title,@emp_date,@MainGuid,@date_termination)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@first_am_name", objPersonEmploymentEntityView.First_am_name));
                command.Parameters.Add(new SqlParameter("@father_am_name", objPersonEmploymentEntityView.Father_am_name));
                command.Parameters.Add(new SqlParameter("@grand_am_name", objPersonEmploymentEntityView.Grand_am_name));
                command.Parameters.Add(new SqlParameter("@Gender", objPersonEmploymentEntityView.Gender));
                command.Parameters.Add(new SqlParameter("@birth_date", objPersonEmploymentEntityView.Birth_date));
                command.Parameters.Add(new SqlParameter("@Typeofemployment", objPersonEmploymentEntityView.Typeofemployment));
                command.Parameters.Add(new SqlParameter("@job_title", objPersonEmploymentEntityView.Job_title));
                command.Parameters.Add(new SqlParameter("@emp_date", objPersonEmploymentEntityView.Emp_date));



                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("PersonEmploymentEntityView::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(PersonEmploymentEntityView objPersonEmploymentEntityView)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[PersonEmploymentEntityView] SET     [first_am_name]=@first_am_name,    [father_am_name]=@father_am_name,    [grand_am_name]=@grand_am_name,    [Gender]=@Gender,    [birth_date]=@birth_date,    [Typeofemployment]=@Typeofemployment,    [job_title]=@job_title,    [emp_date]=@emp_date,    [MainGuid]=@MainGuid,    [date_termination]=@date_termination ";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@first_am_name", objPersonEmploymentEntityView.First_am_name));
                command.Parameters.Add(new SqlParameter("@father_am_name", objPersonEmploymentEntityView.Father_am_name));
                command.Parameters.Add(new SqlParameter("@grand_am_name", objPersonEmploymentEntityView.Grand_am_name));
                command.Parameters.Add(new SqlParameter("@Gender", objPersonEmploymentEntityView.Gender));
                command.Parameters.Add(new SqlParameter("@birth_date", objPersonEmploymentEntityView.Birth_date));
                command.Parameters.Add(new SqlParameter("@Typeofemployment", objPersonEmploymentEntityView.Typeofemployment));
                command.Parameters.Add(new SqlParameter("@job_title", objPersonEmploymentEntityView.Job_title));
                command.Parameters.Add(new SqlParameter("@emp_date", objPersonEmploymentEntityView.Emp_date));

                command.Parameters.Add(new SqlParameter("@date_termination", objPersonEmploymentEntityView.Date_termination));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("PersonEmploymentEntityView::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[PersonEmploymentEntityView] ";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("PersonEmploymentEntityView::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<PersonEmploymentEntityView> GetList()
        {
            List<PersonEmploymentEntityView> RecordsList = new List<PersonEmploymentEntityView>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [first_am_name],[father_am_name],[grand_am_name],[Gender],[birth_date],[Typeofemployment],[job_title],[emp_date],[MainGuid],[date_termination] FROM [dbo].[PersonEmploymentEntityView] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    PersonEmploymentEntityView objPersonEmploymentEntityView = new PersonEmploymentEntityView();
                    if (dr["first_am_name"].Equals(DBNull.Value))
                        objPersonEmploymentEntityView.First_am_name = string.Empty;
                    else
                        objPersonEmploymentEntityView.First_am_name = (string)dr["first_am_name"];
                    if (dr["father_am_name"].Equals(DBNull.Value))
                        objPersonEmploymentEntityView.Father_am_name = string.Empty;
                    else
                        objPersonEmploymentEntityView.Father_am_name = (string)dr["father_am_name"];
                    if (dr["grand_am_name"].Equals(DBNull.Value))
                        objPersonEmploymentEntityView.Grand_am_name = string.Empty;
                    else
                        objPersonEmploymentEntityView.Grand_am_name = (string)dr["grand_am_name"];
                    if (dr["Gender"].Equals(DBNull.Value))
                        objPersonEmploymentEntityView.Gender = string.Empty;
                    else
                        objPersonEmploymentEntityView.Gender = (string)dr["Gender"];
                    if (dr["birth_date"].Equals(DBNull.Value))
                        objPersonEmploymentEntityView.Birth_date = DateTime.MinValue;
                    else
                        objPersonEmploymentEntityView.Birth_date = (DateTime)dr["birth_date"];
                    if (dr["Typeofemployment"].Equals(DBNull.Value))
                        objPersonEmploymentEntityView.Typeofemployment = string.Empty;
                    else
                        objPersonEmploymentEntityView.Typeofemployment = (string)dr["Typeofemployment"];
                    if (dr["job_title"].Equals(DBNull.Value))
                        objPersonEmploymentEntityView.Job_title = string.Empty;
                    else
                        objPersonEmploymentEntityView.Job_title = (string)dr["job_title"];
                    if (dr["emp_date"].Equals(DBNull.Value))
                        objPersonEmploymentEntityView.Emp_date = DateTime.MinValue;
                    else
                        objPersonEmploymentEntityView.Emp_date = (DateTime)dr["emp_date"];

                    if (dr["date_termination"].Equals(DBNull.Value))
                        objPersonEmploymentEntityView.Date_termination = DateTime.MinValue;
                    else
                        objPersonEmploymentEntityView.Date_termination = (DateTime)dr["date_termination"];
                    RecordsList.Add(objPersonEmploymentEntityView);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("PersonEmploymentEntityView::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT  FROM [dbo].[PersonEmploymentEntityView] ";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("PersonEmploymentEntityView::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}