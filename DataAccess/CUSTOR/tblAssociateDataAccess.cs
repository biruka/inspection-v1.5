﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblAssociateDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords(Guid person)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[full_name],[region],[town],[address],[p_o_box],[tel_res],[tel_off],[sex],[date_of_birth],[nationality],[occupation],[birth_place],[familiy_type],[remark],[is_contact],[is_reference],[is_beneficiary],[three_month_salary_order],[Deligation_date] FROM [dbo].[tblAssociate] where ParentGuid=@ParentGuid  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblAssociate");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ParentGuid", person));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblAssociate::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public tblAssociate GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblAssociate objtblAssociate = new tblAssociate();
            string strGetRecord = @"SELECT [MainGuid],[seq_no],[ParentGuid],[full_name],[region],[town],[address],[p_o_box],[tel_res],[tel_off],[sex],[date_of_birth],[nationality],[occupation],[birth_place],[familiy_type],[remark],[is_contact],[is_reference],[is_beneficiary],[three_month_salary_order],[Deligation_date] FROM [dbo].[tblAssociate] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblAssociate");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblAssociate.MainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    objtblAssociate.Seq_no = (int)dTable.Rows[0]["seq_no"];
                    if (dTable.Rows[0]["ParentGuid"].Equals(DBNull.Value))
                        objtblAssociate.ParentGuid = Guid.Empty;
                    else
                        objtblAssociate.ParentGuid = (Guid)dTable.Rows[0]["ParentGuid"];
                    if (dTable.Rows[0]["full_name"].Equals(DBNull.Value))
                        objtblAssociate.Full_name = string.Empty;
                    else
                        objtblAssociate.Full_name = (string)dTable.Rows[0]["full_name"];
                    if (dTable.Rows[0]["region"].Equals(DBNull.Value))
                        objtblAssociate.Region = string.Empty;
                    else
                        objtblAssociate.Region = (string)dTable.Rows[0]["region"];
                    if (dTable.Rows[0]["town"].Equals(DBNull.Value))
                        objtblAssociate.Town = string.Empty;
                    else
                        objtblAssociate.Town = (string)dTable.Rows[0]["town"];
                    if (dTable.Rows[0]["address"].Equals(DBNull.Value))
                        objtblAssociate.Address = string.Empty;
                    else
                        objtblAssociate.Address = (string)dTable.Rows[0]["address"];
                    if (dTable.Rows[0]["p_o_box"].Equals(DBNull.Value))
                        objtblAssociate.P_o_box = string.Empty;
                    else
                        objtblAssociate.P_o_box = (string)dTable.Rows[0]["p_o_box"];
                    if (dTable.Rows[0]["tel_res"].Equals(DBNull.Value))
                        objtblAssociate.Tel_res = string.Empty;
                    else
                        objtblAssociate.Tel_res = (string)dTable.Rows[0]["tel_res"];
                    if (dTable.Rows[0]["tel_off"].Equals(DBNull.Value))
                        objtblAssociate.Tel_off = string.Empty;
                    else
                        objtblAssociate.Tel_off = (string)dTable.Rows[0]["tel_off"];
                    if (dTable.Rows[0]["sex"].Equals(DBNull.Value))
                        objtblAssociate.Sex = 0;
                    else
                        objtblAssociate.Sex = (int)dTable.Rows[0]["sex"];
                    if (dTable.Rows[0]["date_of_birth"].Equals(DBNull.Value))
                        objtblAssociate.Date_of_birth = DateTime.MinValue;
                    else
                        objtblAssociate.Date_of_birth = (DateTime)dTable.Rows[0]["date_of_birth"];
                    if (dTable.Rows[0]["nationality"].Equals(DBNull.Value))
                        objtblAssociate.Nationality = 0;
                    else
                        objtblAssociate.Nationality = (int)dTable.Rows[0]["nationality"];
                    if (dTable.Rows[0]["occupation"].Equals(DBNull.Value))
                        objtblAssociate.Occupation = string.Empty;
                    else
                        objtblAssociate.Occupation = (string)dTable.Rows[0]["occupation"];
                    if (dTable.Rows[0]["birth_place"].Equals(DBNull.Value))
                        objtblAssociate.Birth_place = string.Empty;
                    else
                        objtblAssociate.Birth_place = (string)dTable.Rows[0]["birth_place"];
                    if (dTable.Rows[0]["familiy_type"].Equals(DBNull.Value))
                        objtblAssociate.Familiy_type = 0;
                    else
                        objtblAssociate.Familiy_type = (int)dTable.Rows[0]["familiy_type"];
                    if (dTable.Rows[0]["remark"].Equals(DBNull.Value))
                        objtblAssociate.Remark = string.Empty;
                    else
                        objtblAssociate.Remark = (string)dTable.Rows[0]["remark"];
                    if (dTable.Rows[0]["is_contact"].Equals(DBNull.Value))
                        objtblAssociate.Is_contact = false;
                    else
                        objtblAssociate.Is_contact = (bool)dTable.Rows[0]["is_contact"];
                    if (dTable.Rows[0]["is_reference"].Equals(DBNull.Value))
                        objtblAssociate.Is_reference = false;
                    else
                        objtblAssociate.Is_reference = (bool)dTable.Rows[0]["is_reference"];
                    if (dTable.Rows[0]["is_beneficiary"].Equals(DBNull.Value))
                        objtblAssociate.Is_beneficiary = false;
                    else
                        objtblAssociate.Is_beneficiary = (bool)dTable.Rows[0]["is_beneficiary"];
                    if (dTable.Rows[0]["three_month_salary_order"].Equals(DBNull.Value))
                        objtblAssociate.Three_month_salary_order = 0;
                    else
                        objtblAssociate.Three_month_salary_order = (int)dTable.Rows[0]["three_month_salary_order"];
                    //if (dTable.Rows[0]["Deligation_date"].Equals(DBNull.Value))
                    //    objtblAssociate.Deligation_date = DateTime.MinValue;
                    //else
                    //    objtblAssociate.Deligation_date = (DateTime)dTable.Rows[0]["Deligation_date"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblAssociate::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblAssociate;
        }

       
        public List<tblAssociate> GetList()
        {
            List<tblAssociate> RecordsList = new List<tblAssociate>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[full_name],[region],[town],[address],[p_o_box],[tel_res],[tel_off],[sex],[date_of_birth],[nationality],[occupation],[birth_place],[familiy_type],[remark],[is_contact],[is_reference],[is_beneficiary],[three_month_salary_order],[Deligation_date] FROM [dbo].[tblAssociate]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblAssociate objtblAssociate = new tblAssociate();
                    if (dr["MainGuid"].Equals(DBNull.Value))
                        objtblAssociate.MainGuid = Guid.Empty;
                    else
                        objtblAssociate.MainGuid = (Guid)dr["MainGuid"];
                    if (dr["seq_no"].Equals(DBNull.Value))
                        objtblAssociate.Seq_no = 0;
                    else
                        objtblAssociate.Seq_no = (int)dr["seq_no"];
                    if (dr["ParentGuid"].Equals(DBNull.Value))
                        objtblAssociate.ParentGuid = Guid.Empty;
                    else
                        objtblAssociate.ParentGuid = (Guid)dr["ParentGuid"];
                    if (dr["full_name"].Equals(DBNull.Value))
                        objtblAssociate.Full_name = string.Empty;
                    else
                        objtblAssociate.Full_name = (string)dr["full_name"];
                    if (dr["region"].Equals(DBNull.Value))
                        objtblAssociate.Region = string.Empty;
                    else
                        objtblAssociate.Region = (string)dr["region"];
                    if (dr["town"].Equals(DBNull.Value))
                        objtblAssociate.Town = string.Empty;
                    else
                        objtblAssociate.Town = (string)dr["town"];
                    if (dr["address"].Equals(DBNull.Value))
                        objtblAssociate.Address = string.Empty;
                    else
                        objtblAssociate.Address = (string)dr["address"];
                    if (dr["p_o_box"].Equals(DBNull.Value))
                        objtblAssociate.P_o_box = string.Empty;
                    else
                        objtblAssociate.P_o_box = (string)dr["p_o_box"];
                    if (dr["tel_res"].Equals(DBNull.Value))
                        objtblAssociate.Tel_res = string.Empty;
                    else
                        objtblAssociate.Tel_res = (string)dr["tel_res"];
                    if (dr["tel_off"].Equals(DBNull.Value))
                        objtblAssociate.Tel_off = string.Empty;
                    else
                        objtblAssociate.Tel_off = (string)dr["tel_off"];
                    if (dr["sex"].Equals(DBNull.Value))
                        objtblAssociate.Sex = 0;
                    else
                        objtblAssociate.Sex = (int)dr["sex"];
                    if (dr["date_of_birth"].Equals(DBNull.Value))
                        objtblAssociate.Date_of_birth = DateTime.MinValue;
                    else
                        objtblAssociate.Date_of_birth = (DateTime)dr["date_of_birth"];
                    if (dr["nationality"].Equals(DBNull.Value))
                        objtblAssociate.Nationality = 0;
                    else
                        objtblAssociate.Nationality = (int)dr["nationality"];
                    if (dr["occupation"].Equals(DBNull.Value))
                        objtblAssociate.Occupation = string.Empty;
                    else
                        objtblAssociate.Occupation = (string)dr["occupation"];
                    if (dr["birth_place"].Equals(DBNull.Value))
                        objtblAssociate.Birth_place = string.Empty;
                    else
                        objtblAssociate.Birth_place = (string)dr["birth_place"];
                    if (dr["familiy_type"].Equals(DBNull.Value))
                        objtblAssociate.Familiy_type = 0;
                    else
                        objtblAssociate.Familiy_type = (int)dr["familiy_type"];
                    if (dr["remark"].Equals(DBNull.Value))
                        objtblAssociate.Remark = string.Empty;
                    else
                        objtblAssociate.Remark = (string)dr["remark"];
                    if (dr["is_contact"].Equals(DBNull.Value))
                        objtblAssociate.Is_contact = false;
                    else
                        objtblAssociate.Is_contact = (bool)dr["is_contact"];
                    if (dr["is_reference"].Equals(DBNull.Value))
                        objtblAssociate.Is_reference = false;
                    else
                        objtblAssociate.Is_reference = (bool)dr["is_reference"];
                    if (dr["is_beneficiary"].Equals(DBNull.Value))
                        objtblAssociate.Is_beneficiary = false;
                    else
                        objtblAssociate.Is_beneficiary = (bool)dr["is_beneficiary"];
                    if (dr["three_month_salary_order"].Equals(DBNull.Value))
                        objtblAssociate.Three_month_salary_order = 0;
                    else
                        objtblAssociate.Three_month_salary_order = (int)dr["three_month_salary_order"];
                    //if (dr["Deligation_date"].Equals(DBNull.Value))
                    //    objtblAssociate.Deligation_date = DateTime.MinValue;
                    //else
                    //    objtblAssociate.Deligation_date = (DateTime)dr["Deligation_date"];
                    //RecordsList.Add(objtblAssociate);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblAssociate::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT MainGuid FROM [dbo].[tblAssociate] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("tblAssociate::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}