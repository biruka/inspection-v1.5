﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblApplicantExamDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords(Guid person)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT     dbo.tblApplicantExam.ApplicantGuid, dbo.tblApplicantExam.IsTheoryTaken, dbo.tblApplicantExam.IsPracticalTaken, dbo.tblApplicantExam.IsInterviewTaken, 
                      dbo.tblApplicantExam.TheoryPercentage, dbo.tblApplicantExam.PracticalPercentage, dbo.tblApplicantExam.InterviewPercentage, 
                      dbo.tblApplicantExam.TheoryExamResult, dbo.tblApplicantExam.PracticalExamResult, dbo.tblApplicantExam.InterviewExamResult, dbo.tblApplicantExam.Result
                      FROM         dbo.tblApplicantExam INNER JOIN
                      dbo.tblEmploymentHistory ON dbo.tblApplicantExam.ApplicantGuid = dbo.tblEmploymentHistory.ParentGuid Where dbo.tblEmploymentHistory.ParentGuid=@ParentGuid ORDER BY  [ApplicantExamGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblApplicantExam");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add("@ParentGuid", person);
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblApplicantExam::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public tblApplicantExam GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblApplicantExam objtblApplicantExam = new tblApplicantExam();
            string strGetRecord = @"SELECT [ApplicantExamGuid],[ApplicantGuid],[IsTheoryTaken],[IsPracticalTaken],[IsInterviewTaken],[TheoryPercentage],[PracticalPercentage],[InterviewPercentage],[TheoryExamResult],[PracticalExamResult],[InterviewExamResult],[Result] FROM [dbo].[tblApplicantExam] WHERE [ApplicantExamGuid]=@ApplicantExamGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblApplicantExam");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ApplicantExamGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblApplicantExam.ApplicantExamGuid = (Guid)dTable.Rows[0]["ApplicantExamGuid"];
                    if (dTable.Rows[0]["ApplicantGuid"].Equals(DBNull.Value))
                        objtblApplicantExam.ApplicantGuid = Guid.Empty;
                    else
                        objtblApplicantExam.ApplicantGuid = (Guid)dTable.Rows[0]["ApplicantGuid"];
                    if (dTable.Rows[0]["IsTheoryTaken"].Equals(DBNull.Value))
                        objtblApplicantExam.IsTheoryTaken = false;
                    else
                        objtblApplicantExam.IsTheoryTaken = (bool)dTable.Rows[0]["IsTheoryTaken"];
                    if (dTable.Rows[0]["IsPracticalTaken"].Equals(DBNull.Value))
                        objtblApplicantExam.IsPracticalTaken = false;
                    else
                        objtblApplicantExam.IsPracticalTaken = (bool)dTable.Rows[0]["IsPracticalTaken"];
                    if (dTable.Rows[0]["IsInterviewTaken"].Equals(DBNull.Value))
                        objtblApplicantExam.IsInterviewTaken = false;
                    else
                        objtblApplicantExam.IsInterviewTaken = (bool)dTable.Rows[0]["IsInterviewTaken"];
                    if (dTable.Rows[0]["TheoryPercentage"].Equals(DBNull.Value))
                        objtblApplicantExam.TheoryPercentage = 0;
                    else
                        objtblApplicantExam.TheoryPercentage = (int)dTable.Rows[0]["TheoryPercentage"];
                    if (dTable.Rows[0]["PracticalPercentage"].Equals(DBNull.Value))
                        objtblApplicantExam.PracticalPercentage = 0;
                    else
                        objtblApplicantExam.PracticalPercentage = (int)dTable.Rows[0]["PracticalPercentage"];
                    if (dTable.Rows[0]["InterviewPercentage"].Equals(DBNull.Value))
                        objtblApplicantExam.InterviewPercentage = 0;
                    else
                        objtblApplicantExam.InterviewPercentage = (int)dTable.Rows[0]["InterviewPercentage"];
                    if (dTable.Rows[0]["TheoryExamResult"].Equals(DBNull.Value))
                        objtblApplicantExam.TheoryExamResult = 0;
                    else
                        objtblApplicantExam.TheoryExamResult = (double)dTable.Rows[0]["TheoryExamResult"];
                    if (dTable.Rows[0]["PracticalExamResult"].Equals(DBNull.Value))
                        objtblApplicantExam.PracticalExamResult = 0;
                    else
                        objtblApplicantExam.PracticalExamResult = (double)dTable.Rows[0]["PracticalExamResult"];
                    if (dTable.Rows[0]["InterviewExamResult"].Equals(DBNull.Value))
                        objtblApplicantExam.InterviewExamResult = 0;
                    else
                        objtblApplicantExam.InterviewExamResult = (double)dTable.Rows[0]["InterviewExamResult"];
                    if (dTable.Rows[0]["Result"].Equals(DBNull.Value))
                        objtblApplicantExam.Result = 0;
                    else
                        objtblApplicantExam.Result = (double)dTable.Rows[0]["Result"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblApplicantExam::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblApplicantExam;
        }

       
        public List<tblApplicantExam> GetList()
        {
            List<tblApplicantExam> RecordsList = new List<tblApplicantExam>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [ApplicantExamGuid],[ApplicantGuid],[IsTheoryTaken],[IsPracticalTaken],[IsInterviewTaken],[TheoryPercentage],[PracticalPercentage],[InterviewPercentage],[TheoryExamResult],[PracticalExamResult],[InterviewExamResult],[Result] FROM [dbo].[tblApplicantExam]  ORDER BY  [ApplicantExamGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblApplicantExam objtblApplicantExam = new tblApplicantExam();
                    if (dr["ApplicantExamGuid"].Equals(DBNull.Value))
                        objtblApplicantExam.ApplicantExamGuid = Guid.Empty;
                    else
                        objtblApplicantExam.ApplicantExamGuid = (Guid)dr["ApplicantExamGuid"];
                    if (dr["ApplicantGuid"].Equals(DBNull.Value))
                        objtblApplicantExam.ApplicantGuid = Guid.Empty;
                    else
                        objtblApplicantExam.ApplicantGuid = (Guid)dr["ApplicantGuid"];
                    if (dr["IsTheoryTaken"].Equals(DBNull.Value))
                        objtblApplicantExam.IsTheoryTaken = false;
                    else
                        objtblApplicantExam.IsTheoryTaken = (bool)dr["IsTheoryTaken"];
                    if (dr["IsPracticalTaken"].Equals(DBNull.Value))
                        objtblApplicantExam.IsPracticalTaken = false;
                    else
                        objtblApplicantExam.IsPracticalTaken = (bool)dr["IsPracticalTaken"];
                    if (dr["IsInterviewTaken"].Equals(DBNull.Value))
                        objtblApplicantExam.IsInterviewTaken = false;
                    else
                        objtblApplicantExam.IsInterviewTaken = (bool)dr["IsInterviewTaken"];
                    if (dr["TheoryPercentage"].Equals(DBNull.Value))
                        objtblApplicantExam.TheoryPercentage = 0;
                    else
                        objtblApplicantExam.TheoryPercentage = (int)dr["TheoryPercentage"];
                    if (dr["PracticalPercentage"].Equals(DBNull.Value))
                        objtblApplicantExam.PracticalPercentage = 0;
                    else
                        objtblApplicantExam.PracticalPercentage = (int)dr["PracticalPercentage"];
                    if (dr["InterviewPercentage"].Equals(DBNull.Value))
                        objtblApplicantExam.InterviewPercentage = 0;
                    else
                        objtblApplicantExam.InterviewPercentage = (int)dr["InterviewPercentage"];
                    if (dr["TheoryExamResult"].Equals(DBNull.Value))
                        objtblApplicantExam.TheoryExamResult = 0;
                    else
                        objtblApplicantExam.TheoryExamResult = (double)dr["TheoryExamResult"];
                    if (dr["PracticalExamResult"].Equals(DBNull.Value))
                        objtblApplicantExam.PracticalExamResult = 0;
                    else
                        objtblApplicantExam.PracticalExamResult = (double)dr["PracticalExamResult"];
                    if (dr["InterviewExamResult"].Equals(DBNull.Value))
                        objtblApplicantExam.InterviewExamResult = 0;
                    else
                        objtblApplicantExam.InterviewExamResult = (double)dr["InterviewExamResult"];
                    if (dr["Result"].Equals(DBNull.Value))
                        objtblApplicantExam.Result = 0;
                    else
                        objtblApplicantExam.Result = (double)dr["Result"];
                    RecordsList.Add(objtblApplicantExam);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblApplicantExam::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT ApplicantExamGuid FROM [dbo].[tblApplicantExam] WHERE [ApplicantExamGuid]=@ApplicantExamGuid";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ApplicantExamGuid", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("tblApplicantExam::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}