﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblDocumentDetailDataAccess
    {
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [MainGuid],[DocMainGuid],[id] FROM [dbo].[tblDocumentDetail]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblDocumentDetail");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocumentDetail::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }
        public DataTable GetDocRecords(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [MainGuid],[DocMainGuid],[id] FROM [dbo].[tblDocumentDetail]  WHERE [DocMainGuid]=@DocMainGuid ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblDocumentDetail");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@DocMainGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocumentDetail::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }
        
        public tblDocumentDetail GetDocRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblDocumentDetail objtblDocumentDetail = new tblDocumentDetail();
            string strGetRecord = @"SELECT [MainGuid],[DocMainGuid],[id] FROM [dbo].[tblDocumentDetail] WHERE [DocMainGuid]=@DocMainGuid"; 

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblDocumentDetail");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@DocMainGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblDocumentDetail.MainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    objtblDocumentDetail.DocMainGuid = (Guid)dTable.Rows[0]["DocMainGuid"];
                    if (dTable.Rows[0]["id"].Equals(DBNull.Value))
                        objtblDocumentDetail.Id = 0;
                    else
                        objtblDocumentDetail.Id = (int)dTable.Rows[0]["id"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblDocumentDetail::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblDocumentDetail;
        }
        public tblDocumentDetail GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblDocumentDetail objtblDocumentDetail = new tblDocumentDetail();
            string strGetRecord = @"SELECT [MainGuid],[DocMainGuid],[id] FROM [dbo].[tblDocumentDetail] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblDocumentDetail");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblDocumentDetail.MainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    objtblDocumentDetail.DocMainGuid = (Guid)dTable.Rows[0]["DocMainGuid"];
                    if (dTable.Rows[0]["id"].Equals(DBNull.Value))
                        objtblDocumentDetail.Id = 0;
                    else
                        objtblDocumentDetail.Id = (int)dTable.Rows[0]["id"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblDocumentDetail::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblDocumentDetail;
        }

        public bool Insert(tblDocumentDetail objtblDocumentDetail)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[tblDocumentDetail]
                                            ([DocMainGuid],[id])
                                     VALUES    (@DocMainGuid,@id)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                //command.Parameters.Add(new SqlParameter("@MainGuid", objtblDocumentDetail.MainGuid));
                command.Parameters.Add(new SqlParameter("@DocMainGuid", objtblDocumentDetail.DocMainGuid));
                command.Parameters.Add(new SqlParameter("@id", objtblDocumentDetail.Id));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocumentDetail::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(tblDocumentDetail objtblDocumentDetail)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[tblDocumentDetail] SET     [MainGuid]=@MainGuid,    [DocMainGuid]=@DocMainGuid,    [id]=@id WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", objtblDocumentDetail.MainGuid));
                command.Parameters.Add(new SqlParameter("@DocMainGuid", objtblDocumentDetail.DocMainGuid));
                command.Parameters.Add(new SqlParameter("@id", objtblDocumentDetail.Id));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocumentDetail::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblDocumentDetail] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocumentDetail::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public bool DeleteDocument(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblDocumentDetail] WHERE [DocMainGuid]=@DocMainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@DocMainGuid", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocumentDetail::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        

        public List<tblDocumentDetail> GetList()
        {
            List<tblDocumentDetail> RecordsList = new List<tblDocumentDetail>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [MainGuid],[DocMainGuid],[id] FROM [dbo].[tblDocumentDetail]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblDocumentDetail objtblDocumentDetail = new tblDocumentDetail();
                    if (dr["MainGuid"].Equals(DBNull.Value))
                        objtblDocumentDetail.MainGuid = Guid.Empty;
                    else
                        objtblDocumentDetail.MainGuid = (Guid)dr["MainGuid"];
                    if (dr["DocMainGuid"].Equals(DBNull.Value))
                        objtblDocumentDetail.DocMainGuid = Guid.Empty;
                    else
                        objtblDocumentDetail.DocMainGuid = (Guid)dr["DocMainGuid"];
                    if (dr["id"].Equals(DBNull.Value))
                        objtblDocumentDetail.Id = 0;
                    else
                        objtblDocumentDetail.Id = (int)dr["id"];
                    RecordsList.Add(objtblDocumentDetail);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblDocumentDetail::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT MainGuid FROM [dbo].[tblDocumentDetail] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("tblDocumentDetail::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}