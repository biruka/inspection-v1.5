﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblLookupDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public DataTable GetRecords(string parent,Guid guid,Guid Aperson,int language)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = "[dbo].[GetLookUpDisplayValue]";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.StoredProcedure };

            DataTable dTable = new DataTable("tblLookup");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add("@parent", parent);
                command.Parameters.Add("@guid", guid);
                command.Parameters.Add("@Aperson", Aperson);
                command.Parameters.Add("@language", language);
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblLookup::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public DataTable GetLookupByLookupType(string parent)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = "select * from tblLookup where parent='" + parent + "'";
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblLookup");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblLookup::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public DataTable GetDocDetailRecords(string parent,int language)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = "[dbo].[GetLookUpDisplayforDocDetail]";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.StoredProcedure };

            DataTable dTable = new DataTable("tblLookup");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add("@parent", parent);
                command.Parameters.Add("@language", language);
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblLookup::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public List<tblLookup> GetLookupInfo(string parent, Guid guid )
        {
            List<tblLookup> RecordsList = new List<tblLookup>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;


            string strGetAllRecords = "[dbo].[GetLookUpDisplayValue]";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.StoredProcedure };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblLookup objtblLookup = new tblLookup();
                    if (dr["code"].Equals(DBNull.Value))
                        objtblLookup.Code = string.Empty;
                    else
                        objtblLookup.Code = (string)dr["code"];
                    if (dr["description"].Equals(DBNull.Value))
                        objtblLookup.Description = string.Empty;
                    else
                        objtblLookup.Description = (string)dr["description"];
                    if (dr["amdescription"].Equals(DBNull.Value))
                        objtblLookup.Amdescription = string.Empty;
                    else
                        objtblLookup.Amdescription = (string)dr["amdescription"];
                    if (dr["parent"].Equals(DBNull.Value))
                        objtblLookup.Parent = string.Empty;
                    else
                        objtblLookup.Parent = (string)dr["parent"];
                    if (dr["id"].Equals(DBNull.Value))
                        objtblLookup.Id = 0;
                    else
                        objtblLookup.Id = (int)dr["id"];
                    if (dr["Tigrigna"].Equals(DBNull.Value))
                        objtblLookup.Tigrigna = string.Empty;
                    else
                        objtblLookup.Tigrigna = (string)dr["Tigrigna"];
                    if (dr["AfanOromo"].Equals(DBNull.Value))
                        objtblLookup.AfanOromo = string.Empty;
                    else
                        objtblLookup.AfanOromo = (string)dr["AfanOromo"];
                    if (dr["Afar"].Equals(DBNull.Value))
                        objtblLookup.Afar = string.Empty;
                    else
                        objtblLookup.Afar = (string)dr["Afar"];
                    if (dr["Somali"].Equals(DBNull.Value))
                        objtblLookup.Somali = string.Empty;
                    else
                        objtblLookup.Somali = (string)dr["Somali"];
                    if (dr["Arabic"].Equals(DBNull.Value))
                        objtblLookup.Arabic = string.Empty;
                    else
                        objtblLookup.Arabic = (string)dr["Arabic"];
                    RecordsList.Add(objtblLookup);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblLookup::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public tblLookup GetRecordI(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblLookup objtblLookup = new tblLookup();
            string strGetRecord = @"SELECT [code],[description],[amdescription],[parent],[id],[Tigrigna],[AfanOromo],[Afar],[Somali],[Arabic] FROM [dbo].[tblLookup] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblLookup");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    if (dTable.Rows[0]["code"].Equals(DBNull.Value))
                        objtblLookup.Code = string.Empty;
                    else
                        objtblLookup.Code = (string)dTable.Rows[0]["code"];
                    if (dTable.Rows[0]["description"].Equals(DBNull.Value))
                        objtblLookup.Description = string.Empty;
                    else
                        objtblLookup.Description = (string)dTable.Rows[0]["description"];
                    if (dTable.Rows[0]["amdescription"].Equals(DBNull.Value))
                        objtblLookup.Amdescription = string.Empty;
                    else
                        objtblLookup.Amdescription = (string)dTable.Rows[0]["amdescription"];
                    if (dTable.Rows[0]["parent"].Equals(DBNull.Value))
                        objtblLookup.Parent = string.Empty;
                    else
                        objtblLookup.Parent = (string)dTable.Rows[0]["parent"];
                    objtblLookup.Id = (int)dTable.Rows[0]["id"];
                    if (dTable.Rows[0]["Tigrigna"].Equals(DBNull.Value))
                        objtblLookup.Tigrigna = string.Empty;
                    else
                        objtblLookup.Tigrigna = (string)dTable.Rows[0]["Tigrigna"];
                    if (dTable.Rows[0]["AfanOromo"].Equals(DBNull.Value))
                        objtblLookup.AfanOromo = string.Empty;
                    else
                        objtblLookup.AfanOromo = (string)dTable.Rows[0]["AfanOromo"];
                    if (dTable.Rows[0]["Afar"].Equals(DBNull.Value))
                        objtblLookup.Afar = string.Empty;
                    else
                        objtblLookup.Afar = (string)dTable.Rows[0]["Afar"];
                    if (dTable.Rows[0]["Somali"].Equals(DBNull.Value))
                        objtblLookup.Somali = string.Empty;
                    else
                        objtblLookup.Somali = (string)dTable.Rows[0]["Somali"];
                    if (dTable.Rows[0]["Arabic"].Equals(DBNull.Value))
                        objtblLookup.Arabic = string.Empty;
                    else
                        objtblLookup.Arabic = (string)dTable.Rows[0]["Arabic"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblLookup::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblLookup;
        }

        public tblLookup GetRecord(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblLookup objtblLookup = new tblLookup();
            string strGetRecord = @"SELECT [code],[description],[amdescription],[parent],[id],[Tigrigna],[AfanOromo],[Afar],[Somali],[Arabic] FROM [dbo].[tblLookup] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblLookup");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    if (dTable.Rows[0]["code"].Equals(DBNull.Value))
                        objtblLookup.Code = string.Empty;
                    else
                        objtblLookup.Code = (string)dTable.Rows[0]["code"];
                    if (dTable.Rows[0]["description"].Equals(DBNull.Value))
                        objtblLookup.Description = string.Empty;
                    else
                        objtblLookup.Description = (string)dTable.Rows[0]["description"];
                    if (dTable.Rows[0]["amdescription"].Equals(DBNull.Value))
                        objtblLookup.Amdescription = string.Empty;
                    else
                        objtblLookup.Amdescription = (string)dTable.Rows[0]["amdescription"];
                    if (dTable.Rows[0]["parent"].Equals(DBNull.Value))
                        objtblLookup.Parent = string.Empty;
                    else
                        objtblLookup.Parent = (string)dTable.Rows[0]["parent"];
                    objtblLookup.Id = (int)dTable.Rows[0]["id"];
                    if (dTable.Rows[0]["Tigrigna"].Equals(DBNull.Value))
                        objtblLookup.Tigrigna = string.Empty;
                    else
                        objtblLookup.Tigrigna = (string)dTable.Rows[0]["Tigrigna"];
                    if (dTable.Rows[0]["AfanOromo"].Equals(DBNull.Value))
                        objtblLookup.AfanOromo = string.Empty;
                    else
                        objtblLookup.AfanOromo = (string)dTable.Rows[0]["AfanOromo"];
                    if (dTable.Rows[0]["Afar"].Equals(DBNull.Value))
                        objtblLookup.Afar = string.Empty;
                    else
                        objtblLookup.Afar = (string)dTable.Rows[0]["Afar"];
                    if (dTable.Rows[0]["Somali"].Equals(DBNull.Value))
                        objtblLookup.Somali = string.Empty;
                    else
                        objtblLookup.Somali = (string)dTable.Rows[0]["Somali"];
                    if (dTable.Rows[0]["Arabic"].Equals(DBNull.Value))
                        objtblLookup.Arabic = string.Empty;
                    else
                        objtblLookup.Arabic = (string)dTable.Rows[0]["Arabic"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblLookup::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblLookup;
        }

        public bool Insert(tblLookup objtblLookup)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[tblLookup]
                                            ([code],[description],[amdescription],[parent],[Tigrigna],[AfanOromo],[Afar],[Somali],[Arabic])
                                     VALUES    (@code,@description,@amdescription,@parent,@Tigrigna,@AfanOromo,@Afar,@Somali,@Arabic)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@code", objtblLookup.Code));
                command.Parameters.Add(new SqlParameter("@description", objtblLookup.Description));
                command.Parameters.Add(new SqlParameter("@amdescription", objtblLookup.Amdescription));
                command.Parameters.Add(new SqlParameter("@parent", objtblLookup.Parent));
                command.Parameters.Add(new SqlParameter("@Tigrigna", objtblLookup.Tigrigna));
                command.Parameters.Add(new SqlParameter("@AfanOromo", objtblLookup.AfanOromo));
                command.Parameters.Add(new SqlParameter("@Afar", objtblLookup.Afar));
                command.Parameters.Add(new SqlParameter("@Somali", objtblLookup.Somali));
                command.Parameters.Add(new SqlParameter("@Arabic", objtblLookup.Arabic));
                command.Parameters.Add(new SqlParameter("@id", objtblLookup.Id));
                command.Parameters["@id"].Direction = ParameterDirection.Output;


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                objtblLookup.Id = (int)command.Parameters["@id"].Value;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblLookup::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(tblLookup objtblLookup)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[tblLookup] SET     [code]=@code,    [description]=@description,    [amdescription]=@amdescription,    [parent]=@parent,    [Tigrigna]=@Tigrigna,    [AfanOromo]=@AfanOromo,    [Afar]=@Afar,    [Somali]=@Somali,    [Arabic]=@Arabic ";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@code", objtblLookup.Code));
                command.Parameters.Add(new SqlParameter("@description", objtblLookup.Description));
                command.Parameters.Add(new SqlParameter("@amdescription", objtblLookup.Amdescription));
                command.Parameters.Add(new SqlParameter("@parent", objtblLookup.Parent));
                command.Parameters.Add(new SqlParameter("@id", objtblLookup.Id));
                command.Parameters.Add(new SqlParameter("@Tigrigna", objtblLookup.Tigrigna));
                command.Parameters.Add(new SqlParameter("@AfanOromo", objtblLookup.AfanOromo));
                command.Parameters.Add(new SqlParameter("@Afar", objtblLookup.Afar));
                command.Parameters.Add(new SqlParameter("@Somali", objtblLookup.Somali));
                command.Parameters.Add(new SqlParameter("@Arabic", objtblLookup.Arabic));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblLookup::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblLookup] ";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblLookup::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<tblLookup> GetList()
        {
            List<tblLookup> RecordsList = new List<tblLookup>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [code],[description],[amdescription],[parent],[id],[Tigrigna],[AfanOromo],[Afar],[Somali],[Arabic] FROM [dbo].[tblLookup] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblLookup objtblLookup = new tblLookup();
                    if (dr["code"].Equals(DBNull.Value))
                        objtblLookup.Code = string.Empty;
                    else
                        objtblLookup.Code = (string)dr["code"];
                    if (dr["description"].Equals(DBNull.Value))
                        objtblLookup.Description = string.Empty;
                    else
                        objtblLookup.Description = (string)dr["description"];
                    if (dr["amdescription"].Equals(DBNull.Value))
                        objtblLookup.Amdescription = string.Empty;
                    else
                        objtblLookup.Amdescription = (string)dr["amdescription"];
                    if (dr["parent"].Equals(DBNull.Value))
                        objtblLookup.Parent = string.Empty;
                    else
                        objtblLookup.Parent = (string)dr["parent"];
                    if (dr["id"].Equals(DBNull.Value))
                        objtblLookup.Id = 0;
                    else
                        objtblLookup.Id = (int)dr["id"];
                    if (dr["Tigrigna"].Equals(DBNull.Value))
                        objtblLookup.Tigrigna = string.Empty;
                    else
                        objtblLookup.Tigrigna = (string)dr["Tigrigna"];
                    if (dr["AfanOromo"].Equals(DBNull.Value))
                        objtblLookup.AfanOromo = string.Empty;
                    else
                        objtblLookup.AfanOromo = (string)dr["AfanOromo"];
                    if (dr["Afar"].Equals(DBNull.Value))
                        objtblLookup.Afar = string.Empty;
                    else
                        objtblLookup.Afar = (string)dr["Afar"];
                    if (dr["Somali"].Equals(DBNull.Value))
                        objtblLookup.Somali = string.Empty;
                    else
                        objtblLookup.Somali = (string)dr["Somali"];
                    if (dr["Arabic"].Equals(DBNull.Value))
                        objtblLookup.Arabic = string.Empty;
                    else
                        objtblLookup.Arabic = (string)dr["Arabic"];
                    RecordsList.Add(objtblLookup);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblLookup::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(int ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT  FROM [dbo].[tblLookup] ";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("tblLookup::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}