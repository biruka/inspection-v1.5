﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.Commen;
namespace CUSTOR.DataAccess
{
    public class InspectionDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords(Guid EmployeeGUID, string Activitytype)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            string strGetAllRecords = @"SELECT [InspectionGUID],[EmployeeGUID],[ComplaintGUID],[InspectionDate],[HRMServiceType],[InspectionDecision],[InspectionFinding],[UserName],[EventDatetime],[UpdatedUsername],[approvalname],[UpdatedEventDatetime],[approvaldate],[Activitytype],[inspectiontype],[Answer],[Subject],[AnswerDate],[Approved] FROM [dbo].[Inspection]  where [EmployeeGUID]=@EmployeeGUID and [Activitytype]=@Activitytype order by [EventDatetime] desc";
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Inspection");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@EmployeeGUID", EmployeeGUID));
                command.Parameters.Add(new SqlParameter("@Activitytype", Activitytype));
                connection.Open();
                adapter.Fill(dTable);
                System.Type typeString = Type.GetType("System.String");
                dTable.Columns.Add("approvaldateEthiopianCalander", typeString);
                DateTime approvalDate = new DateTime();
                for (int index = 0; index < dTable.Rows.Count; index++)
                {
                    if (dTable.Rows[index]["approvaldate"] != DBNull.Value)
                    {
                        approvalDate = Convert.ToDateTime(dTable.Rows[index]["approvaldate"]);
                        if (approvalDate != null)
                        {
                            dTable.Rows[index]["approvaldateEthiopianCalander"] = EthiopicDateTime.GetEthiopicDate(approvalDate.Day, approvalDate.Month, approvalDate.Year);
                        }
                    }
                    if (dTable.Rows[index]["Approved"] == DBNull.Value)
                    {
                        dTable.Rows[index]["Approved"] = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Inspection::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public tblInspection GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblInspection objInspection = new tblInspection();
            string strGetRecord = @"SELECT [InspectionGUID],[EmployeeGUID],[ComplaintGUID],[InspectionDate],[HRMServiceType],[InspectionDecision],[InspectionFinding],[UserName],[EventDatetime],[UpdatedUsername],[approvalname],[UpdatedEventDatetime],[approvaldate],[Activitytype],[inspectiontype],[Answer],[Subject],[AnswerDate],[Approved] FROM [dbo].[Inspection] where [InspectionGUID]=@InspectionGUID";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Inspection");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@InspectionGUID", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objInspection.InspectionGUID = (Guid)dTable.Rows[0]["InspectionGUID"];
                    objInspection.EmployeeGUID = (Guid)dTable.Rows[0]["EmployeeGUID"];
                    objInspection.ComplaintGUID = (Guid)dTable.Rows[0]["ComplaintGUID"];
                    if (dTable.Rows[0]["InspectionDate"].Equals(DBNull.Value))
                        objInspection.InspectionDate = DateTime.MinValue;
                    else
                        objInspection.InspectionDate = (DateTime)dTable.Rows[0]["InspectionDate"];
                    if (dTable.Rows[0]["HRMServiceType"].Equals(DBNull.Value))
                        objInspection.HRMServiceType = 0;
                    else
                        objInspection.HRMServiceType = (int)dTable.Rows[0]["HRMServiceType"];
                    if (dTable.Rows[0]["InspectionDecision"].Equals(DBNull.Value))
                        objInspection.InspectionDecision = string.Empty;
                    else
                        objInspection.InspectionDecision = (string)dTable.Rows[0]["InspectionDecision"];
                    objInspection.InspectionFinding = (string)dTable.Rows[0]["InspectionFinding"];
                    if (dTable.Rows[0]["UserName"].Equals(DBNull.Value))
                        objInspection.UserName = string.Empty;
                    else
                        objInspection.UserName = (string)dTable.Rows[0]["UserName"];
                    if (dTable.Rows[0]["EventDatetime"].Equals(DBNull.Value))
                        objInspection.EventDatetime = DateTime.MinValue;
                    else
                        objInspection.EventDatetime = (DateTime)dTable.Rows[0]["EventDatetime"];
                    if (dTable.Rows[0]["UpdatedUsername"].Equals(DBNull.Value))
                        objInspection.UpdatedUsername = string.Empty;
                    else
                        objInspection.UpdatedUsername = (string)dTable.Rows[0]["UpdatedUsername"];
                     if (dTable.Rows[0]["approvalname"].Equals(DBNull.Value))
                         objInspection.approvalname = string.Empty;
                    else
                         objInspection.approvalname = (string)dTable.Rows[0]["approvalname"];
                     if (dTable.Rows[0]["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objInspection.UpdatedEventDatetime = DateTime.MinValue;
                    else
                        objInspection.UpdatedEventDatetime = (DateTime)dTable.Rows[0]["UpdatedEventDatetime"];
                     if (dTable.Rows[0]["approvaldate"].Equals(DBNull.Value))
                         objInspection.approvaldate = DateTime.MinValue;
                    else
                         objInspection.approvaldate = (DateTime)dTable.Rows[0]["approvaldate"];

                    
                    if (dTable.Rows[0]["Activitytype"].Equals(DBNull.Value))
                        objInspection.Activitytype = 0;
                    else
                        objInspection.Activitytype = (int)dTable.Rows[0]["Activitytype"];
                    if (dTable.Rows[0]["inspectiontype"].Equals(DBNull.Value))
                        objInspection.inspectiontype = 0;
                    else
                        objInspection.inspectiontype = (int)dTable.Rows[0]["inspectiontype"];
                    
                    if (dTable.Rows[0]["Answer"].Equals(DBNull.Value))
                        objInspection.Answer = string.Empty;
                    else
                        objInspection.Answer = (string)dTable.Rows[0]["Answer"];
                    if (dTable.Rows[0]["Subject"].Equals(DBNull.Value))
                        objInspection.Subject = string.Empty;
                    else
                        objInspection.Subject = (string)dTable.Rows[0]["Subject"];
                    if (dTable.Rows[0]["AnswerDate"].Equals(DBNull.Value))
                        objInspection.AnswerDate = DateTime.MinValue;
                    else
                        objInspection.AnswerDate = (DateTime)dTable.Rows[0]["AnswerDate"];
                    if (dTable.Rows[0]["Approved"].Equals(DBNull.Value))
                        objInspection.Approved =false;
                    else
                        objInspection.Approved = (Boolean)dTable.Rows[0]["Approved"];
                    
                    
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Inspection::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objInspection;
        }

        public bool Insert(tblInspection objInspection)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[Inspection]
                                            ([InspectionGUID],[EmployeeGUID],[ComplaintGUID],[InspectionDate],[HRMServiceType],[InspectionDecision],[InspectionFinding],[UserName],[EventDatetime],[Activitytype],[inspectiontype])
                                     VALUES (ISNULL(@InspectionGUID, (newid())),@EmployeeGUID,@ComplaintGUID,@InspectionDate,@HRMServiceType,@InspectionDecision,@InspectionFinding,@UserName,@EventDatetime,@Activitytype,@inspectiontype)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@InspectionGUID", objInspection.InspectionGUID));
                command.Parameters.Add(new SqlParameter("@EmployeeGUID", objInspection.EmployeeGUID));
                command.Parameters.Add(new SqlParameter("@ComplaintGUID", objInspection.ComplaintGUID));
                command.Parameters.Add(new SqlParameter("@InspectionDate", objInspection.InspectionDate));
                command.Parameters.Add(new SqlParameter("@HRMServiceType", objInspection.HRMServiceType));
                command.Parameters.Add(new SqlParameter("@InspectionDecision", objInspection.InspectionDecision));
                command.Parameters.Add(new SqlParameter("@InspectionFinding", objInspection.InspectionFinding));
                command.Parameters.Add(new SqlParameter("@UserName", objInspection.UserName));
                command.Parameters.Add(new SqlParameter("@EventDatetime", objInspection.EventDatetime));
                command.Parameters.Add(new SqlParameter("@Activitytype", objInspection.Activitytype));
                command.Parameters.Add(new SqlParameter("@inspectiontype", objInspection.inspectiontype));
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Inspection::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public bool Update(tblInspection objInspection)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[Inspection] SET [InspectionGUID]=@InspectionGUID, [EmployeeGUID]=@EmployeeGUID,[ComplaintGUID]=@ComplaintGUID  ,    [HRMServiceType]=@HRMServiceType,    [InspectionDecision]=@InspectionDecision,    [InspectionFinding]=@InspectionFinding,[UpdatedUsername]=@UpdatedUsername,   [UpdatedEventDatetime]=@UpdatedEventDatetime, [Activitytype]=@Activitytype,[inspectiontype]=@inspectiontype where [InspectionGUID]=@InspectionGUID ";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@InspectionGUID", objInspection.InspectionGUID));
                command.Parameters.Add(new SqlParameter("@EmployeeGUID", objInspection.EmployeeGUID));
                command.Parameters.Add(new SqlParameter("@ComplaintGUID", objInspection.ComplaintGUID));
                command.Parameters.Add(new SqlParameter("@HRMServiceType", objInspection.HRMServiceType));
                command.Parameters.Add(new SqlParameter("@InspectionDecision", objInspection.InspectionDecision));
                command.Parameters.Add(new SqlParameter("@InspectionFinding", objInspection.InspectionFinding));
                command.Parameters.Add(new SqlParameter("@UpdatedUsername", objInspection.UpdatedUsername));
                command.Parameters.Add(new SqlParameter("@UpdatedEventDatetime", objInspection.UpdatedEventDatetime));
                command.Parameters.Add(new SqlParameter("@Activitytype", objInspection.Activitytype));
                command.Parameters.Add(new SqlParameter("@inspectiontype", objInspection.inspectiontype));
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Inspection::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


        public bool AnswerUpdate(tblInspection objInspection)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[Inspection] SET [Answer]=@Answer, [Subject]=@Subject ,[AnswerDate]=@AnswerDate where [InspectionGUID]=@InspectionGUID";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {                               
                command.Parameters.Add(new SqlParameter("@Answer", objInspection.Answer));
                command.Parameters.Add(new SqlParameter("@InspectionGUID", objInspection.InspectionGUID));
                command.Parameters.Add(new SqlParameter("@Subject", objInspection.Subject));
                command.Parameters.Add(new SqlParameter("@AnswerDate", objInspection.AnswerDate));
                
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Inspection::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool ApprovalUpdate(tblInspection objInspection)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[Inspection] SET [approvalname]=@approvalname , [approvaldate]=@approvaldate,[Approved]=@Approved where [InspectionGUID]=@InspectionGUID";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@approvalname", objInspection.approvalname));
                command.Parameters.Add(new SqlParameter("@InspectionGUID", objInspection.InspectionGUID));
                command.Parameters.Add(new SqlParameter("@approvaldate", objInspection.approvaldate));
                command.Parameters.Add(new SqlParameter("@Approved", objInspection.Approved));

                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Inspection::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[Inspection] WHERE [InspectionGUID]=@InspectionGUID";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@InspectionGUID", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Inspection::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<tblInspection> GetList()
        {
            List<tblInspection> RecordsList = new List<tblInspection>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [InspectionGUID],[EmployeeGUID],[ComplaintGUID],[InspectionDate],[HRMServiceType],[InspectionDecision],[InspectionFinding],[UserName],[EventDatetime],[UpdatedUsername],[approvalname],[UpdatedEventDatetime],[approvaldate],[Activitytype],[Answer],[Subject],[AnswerDate] FROM [dbo].[Inspection] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblInspection objInspection = new tblInspection();
                    if (dr["InspectionGUID"].Equals(DBNull.Value))
                        objInspection.InspectionGUID = Guid.Empty;
                    else
                        objInspection.InspectionGUID = (Guid)dr["InspectionGUID"];
                    if (dr["EmployeeGUID"].Equals(DBNull.Value))
                        objInspection.EmployeeGUID = Guid.Empty;
                    else
                        objInspection.EmployeeGUID = (Guid)dr["EmployeeGUID"];
                    if (dr["ComplaintGUID"].Equals(DBNull.Value))
                        objInspection.ComplaintGUID = Guid.Empty;
                    else
                        objInspection.ComplaintGUID = (Guid)dr["ComplaintGUID"];
                    if (dr["InspectionDate"].Equals(DBNull.Value))
                        objInspection.InspectionDate = DateTime.MinValue;
                    else
                        objInspection.InspectionDate = (DateTime)dr["InspectionDate"];
                    if (dr["HRMServiceType"].Equals(DBNull.Value))
                        objInspection.HRMServiceType = 0;
                    else
                        objInspection.HRMServiceType = (int)dr["HRMServiceType"];
                    if (dr["InspectionDecision"].Equals(DBNull.Value))
                        objInspection.InspectionDecision = string.Empty;
                    else
                        objInspection.InspectionDecision = (string)dr["InspectionDecision"];
                    if (dr["InspectionFinding"].Equals(DBNull.Value))
                        objInspection.InspectionFinding = string.Empty;
                    else
                        objInspection.InspectionFinding = (string)dr["InspectionFinding"];
                    if (dr["UserName"].Equals(DBNull.Value))
                        objInspection.UserName = string.Empty;
                    else
                        objInspection.UserName = (string)dr["UserName"];
                    if (dr["EventDatetime"].Equals(DBNull.Value))
                        objInspection.EventDatetime = DateTime.MinValue;
                    else
                        objInspection.EventDatetime = (DateTime)dr["EventDatetime"];
                    if (dr["UpdatedUsername"].Equals(DBNull.Value))
                        objInspection.UpdatedUsername = string.Empty;
                    else
                        objInspection.UpdatedUsername = (string)dr["UpdatedUsername"];
                    if (dr["approvalname"].Equals(DBNull.Value))
                        objInspection.approvalname = string.Empty;
                    else
                        objInspection.approvalname = (string)dr["approvalname"];
                    
                    if (dr["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objInspection.UpdatedEventDatetime = DateTime.MinValue;
                    else
                        objInspection.UpdatedEventDatetime = (DateTime)dr["UpdatedEventDatetime"];
                    if (dr["approvaldate"].Equals(DBNull.Value))
                        objInspection.approvaldate = DateTime.MinValue;
                    else
                        objInspection.approvaldate = (DateTime)dr["approvaldate"];
                                        
                    if (dr["Activitytype"].Equals(DBNull.Value))
                        objInspection.Activitytype = 0;
                    else
                        objInspection.Activitytype = (int)dr["Activitytype"];
                    if (dr["inspectiontype"].Equals(DBNull.Value))
                        objInspection.inspectiontype = 0;
                    else
                        objInspection.inspectiontype = (int)dr["inspectiontype"];

                    
                    if (dr["Answer"].Equals(DBNull.Value))
                        objInspection.Answer = string.Empty;
                    else
                        objInspection.Answer = (string)dr["Answer"];
                    if (dr["Subject"].Equals(DBNull.Value))
                        objInspection.Subject = string.Empty;
                    else
                        objInspection.Subject = (string)dr["Subject"];
                    if (dr["AnswerDate"].Equals(DBNull.Value))
                        objInspection.AnswerDate = DateTime.MinValue;
                    else
                        objInspection.AnswerDate = (DateTime)dr["AnswerDate"];
                    
                    RecordsList.Add(objInspection);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Inspection::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT InspectionGUID FROM [dbo].[Inspection] WHERE [InspectionGUID]=@InspectionGUID";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@InspectionGUID", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("Inspection::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool IsApproved(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT Approved FROM [dbo].[Inspection] WHERE [InspectionGUID]=@InspectionGUID";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@InspectionGUID", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("Inspection::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
    }
}