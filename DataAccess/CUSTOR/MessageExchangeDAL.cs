﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class DetailMessageExchangeDAL
    {
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }
        static readonly string Key = "NewsMessageExchange";
      //  HttpSessionState Session { get { return HttpContext.Current.Session; } }
       
        public bool Insert(MessageExchangeBO objtblMessageExchange)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[tblMessageExchange]
                                            ([OrgGuid],[Case],[Subject],[Status],[Text],[EmployeeName], [UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime])
                                     VALUES    (@OrgGuid,@Case,@Subject, @Status,@Text,@EmployeeName, @UserName,@EventDatetime,@UpdatedUsername,@UpdatedEventDatetime)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                //command.Parameters.Add(new SqlParameter("@ID", objtblMessageExchange.ID));
                //command.Parameters.Add(new SqlParameter("@parentID", objtblMessageExchange.ParentID));
                command.Parameters.Add(new SqlParameter("@OrgGuid", objtblMessageExchange.OrgGuid));
                command.Parameters.Add(new SqlParameter("@Case", objtblMessageExchange.Case));
                command.Parameters.Add(new SqlParameter("@Subject", objtblMessageExchange.Subject));
                //command.Parameters.Add(new SqlParameter("@Date", objtblMessageExchange.Date));
                command.Parameters.Add(new SqlParameter("@Status", objtblMessageExchange.Status));
                command.Parameters.Add(new SqlParameter("@Text", objtblMessageExchange.Text));
                command.Parameters.Add(new SqlParameter("@EmployeeName", objtblMessageExchange.EmployeeName));
                //command.Parameters.Add(new SqlParameter("@decission", objtblMessageExchange.Decission));
                command.Parameters.Add(new SqlParameter("@UserName", objtblMessageExchange.UserName));
                command.Parameters.Add(new SqlParameter("@EventDatetime", objtblMessageExchange.EventDatetime));
                command.Parameters.Add(new SqlParameter("@UpdatedUsername", objtblMessageExchange.UpdatedUsername));
                command.Parameters.Add(new SqlParameter("@UpdatedEventDatetime", objtblMessageExchange.UpdatedEventDatetime));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblMessageExchange::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(MessageExchangeBO objtblMessageExchange)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[tblMessageExchange] SET [OrgGuid]=@OrgGuid,  [Case]=@Case,  
                                [Subject]=@Subject, [Status]=@Status, [Text]=@Text,[EmployeeName]=@EmployeeName,
                                [UserName]=@UserName,    [EventDatetime]=@EventDatetime,    [UpdatedUsername]=@UpdatedUsername,    
                                [UpdatedEventDatetime]=@UpdatedEventDatetime WHERE [ID]=@ID";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ID", objtblMessageExchange.ID));
                //command.Parameters.Add(new SqlParameter("@parentID", objtblMessageExchange.ParentID));
                command.Parameters.Add(new SqlParameter("@OrgGuid", objtblMessageExchange.OrgGuid));
                command.Parameters.Add(new SqlParameter("@Case", objtblMessageExchange.Case));
                command.Parameters.Add(new SqlParameter("@Subject", objtblMessageExchange.Subject));
                command.Parameters.Add(new SqlParameter("@Status", objtblMessageExchange.Status));
                command.Parameters.Add(new SqlParameter("@Text", objtblMessageExchange.Text));
                command.Parameters.Add(new SqlParameter("@EmployeeName", objtblMessageExchange.EmployeeName));
                command.Parameters.Add(new SqlParameter("@UserName", objtblMessageExchange.UserName));
                command.Parameters.Add(new SqlParameter("@EventDatetime", objtblMessageExchange.EventDatetime));
                command.Parameters.Add(new SqlParameter("@UpdatedUsername", objtblMessageExchange.UpdatedUsername));
                command.Parameters.Add(new SqlParameter("@UpdatedEventDatetime", objtblMessageExchange.UpdatedEventDatetime));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblMessageExchange::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public List<MessageExchangeBO> GetParentMessageList(Int32 ParentID)
        {
            List<MessageExchangeBO> RecordsList = new List<MessageExchangeBO>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
            string strGetAllRecords = @"SELECT [ID],[OrgGuid],[Case],[Date],[Subject],[Status],[Text], [EmployeeName]
                                        FROM [dbo].[tblInspectionParentMessage] where ParentMessageID=@ParentMessageID";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ParentMessageID", ParentID));
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    MessageExchangeBO objtblMessageExchange = new MessageExchangeBO();
                    if (dr["ID"].Equals(DBNull.Value))
                        objtblMessageExchange.ID = 0;
                    else
                        objtblMessageExchange.ID = (int)dr["ID"];
                    if (dr["OrgGuid"].Equals(DBNull.Value))
                        objtblMessageExchange.OrgGuid = Guid.Empty;
                    else
                        objtblMessageExchange.OrgGuid = (Guid)dr["OrgGuid"];
                    if (dr["Case"].Equals(DBNull.Value))
                        objtblMessageExchange.Case = string.Empty;
                    else
                        objtblMessageExchange.Case = (string)dr["Case"];
                    if (dr["Subject"].Equals(DBNull.Value))
                        objtblMessageExchange.Subject = string.Empty;
                    else
                        objtblMessageExchange.Subject = (string)dr["Subject"];
                    if (dr["Status"].Equals(DBNull.Value))
                        objtblMessageExchange.Status = false;
                    else
                        objtblMessageExchange.Status = (bool)dr["Status"];
                    if (dr["Text"].Equals(DBNull.Value))
                        objtblMessageExchange.Text = string.Empty;
                    else
                        objtblMessageExchange.Text = (string)dr["Text"];
                    if (dr["EmployeeName"].Equals(DBNull.Value))
                        objtblMessageExchange.EmployeeName = string.Empty;
                    else
                        objtblMessageExchange.EmployeeName = (string)dr["EmployeeName"];

                    RecordsList.Add(objtblMessageExchange);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblMessageExchange::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }
        public List<MessageExchangeBO> GetList()
        {
            List<MessageExchangeBO> RecordsList = new List<MessageExchangeBO>();
            SqlConnection    connection=new SqlConnection(ConnectionString);
            SqlDataReader dr=null;
            string strGetAllRecords = @"SELECT [ID],[OrgGuid],[Case],[Date],[Subject],[Status],[Text], [EmployeeName]
                                        FROM [dbo].[tblInspectionParentMessage]";

            SqlCommand    command = new SqlCommand() {CommandText=strGetAllRecords, CommandType=CommandType.Text};
            command.Connection = connection;

            try
            {
            connection.Open();
            dr= command.ExecuteReader();
            while (dr.Read())
            {
                MessageExchangeBO objtblMessageExchange = new MessageExchangeBO();
                if (dr["ID"].Equals(DBNull.Value))
                    objtblMessageExchange.ID = 0;
                else
                    objtblMessageExchange.ID = (int)dr["ID"];
                 if(dr["OrgGuid"].Equals(DBNull.Value))
                     objtblMessageExchange.OrgGuid = Guid.Empty;
                else
                    objtblMessageExchange.OrgGuid = (Guid) dr["OrgGuid"];
                 if(dr["Case"].Equals(DBNull.Value))
                     objtblMessageExchange.Case = string.Empty;
                else
                    objtblMessageExchange.Case = (string) dr["Case"];
                 if(dr["Subject"].Equals(DBNull.Value))
                     objtblMessageExchange.Subject = string.Empty;
                else
                    objtblMessageExchange.Subject = (string) dr["Subject"];
                 if (dr["Status"].Equals(DBNull.Value))
                     objtblMessageExchange.Status = false;
                else
                     objtblMessageExchange.Status = (bool)dr["Status"];
                 if(dr["Text"].Equals(DBNull.Value))
                     objtblMessageExchange.Text = string.Empty;
                else
                    objtblMessageExchange.Text = (string) dr["Text"];
                 if (dr["EmployeeName"].Equals(DBNull.Value))
                     objtblMessageExchange.EmployeeName = string.Empty;
                 else
                     objtblMessageExchange.EmployeeName = (string)dr["EmployeeName"];
                
                RecordsList.Add(objtblMessageExchange);
            }

            }
            catch(Exception ex)
            {
                throw new Exception("tblMessageExchange::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return  RecordsList;
        }

        public DataTable GetQuestionerMessages(Int32 ParentID)
        {
            List<MessageExchangeBO> RecordsList = new List<MessageExchangeBO>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
            //  string condition = Session["OrgGuid"].ToString();
            string strGetAllRecords = @"SELECT [ID],[OrgGuid],[Case],[EventDateTime],[Subject],[Status],[Text],[EmployeeName]
                                        FROM [dbo].[tblInspectionMessageExchange] WHERE ParentID=" + ParentID + " ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblInspectionMessageExchange");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblNewPositionJobDescription::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }
        public DataTable GetMessages(Int32 ParentID)
        {
            List<MessageExchangeBO> RecordsList = new List<MessageExchangeBO>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
          //  string condition = Session["OrgGuid"].ToString();
            string strGetAllRecords = @"SELECT [ID],[OrgGuid],[Case],[EventDateTime],[Subject],[Status],[Text],[EmployeeName]
                                        FROM [dbo].[tblInspectionMessageExchange] WHERE ParentID=" + ParentID + " ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblInspectionMessageExchange");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblNewPositionJobDescription::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

       
        public List<MessageExchangeBO> GetParentMessages()
        {
            List<MessageExchangeBO> RecordsList = new List<MessageExchangeBO>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
           // string condition = Session["OrgGuid"].ToString();
            string strGetAllRecords = @"SELECT [ID],[OrgGuid],[Case],[Date],[Subject],[Status],[Text],[EmployeeName]
                                        FROM [dbo].[tblInspectionMessageExchange] WHERE 1=1 ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    MessageExchangeBO objtblMessageExchange = new MessageExchangeBO();
                    if (dr["ID"].Equals(DBNull.Value))
                        objtblMessageExchange.ID = 0;
                    else
                        objtblMessageExchange.ID = (int)dr["ID"];
                    if (dr["OrgGuid"].Equals(DBNull.Value))
                        objtblMessageExchange.OrgGuid = Guid.Empty;
                    else
                        objtblMessageExchange.OrgGuid = (Guid)dr["OrgGuid"];
                    if (dr["Case"].Equals(DBNull.Value))
                        objtblMessageExchange.Case = string.Empty;
                    else
                        objtblMessageExchange.Case = (string)dr["Case"];
                    if (dr["Subject"].Equals(DBNull.Value))
                        objtblMessageExchange.Subject = string.Empty;
                    else
                        objtblMessageExchange.Subject = (string)dr["Subject"];
                    if (dr["Status"].Equals(DBNull.Value))
                        objtblMessageExchange.Status = false;
                    else
                        objtblMessageExchange.Status = (bool)dr["Status"];
                    if (dr["Text"].Equals(DBNull.Value))
                        objtblMessageExchange.Text = string.Empty;
                    else
                        objtblMessageExchange.Text = (string)dr["Text"];

                    if (dr["EmployeeName"].Equals(DBNull.Value))
                        objtblMessageExchange.EmployeeName = string.Empty;
                    else
                        objtblMessageExchange.EmployeeName = (string)dr["EmployeeName"];

                    RecordsList.Add(objtblMessageExchange);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblMessageExchange::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }



        public bool DeleteMessage(Int32 ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblInspectionMessageExchange] WHERE ID=" + ID + " ";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblMessageExchange::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public DataTable poplateOrganization()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            string strPoplatePosition = @"SELECT [OrgGuid], [Description], [DescriptionAm] FROM [dbo].[OrganizationRequest] ";
            SqlCommand command = new SqlCommand() { CommandText = strPoplatePosition, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Organization");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;
            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Organization::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        //insert message attachment
        public bool InsertMessageAttachement(AttachmentMessageExchangeBO objtblMessageExchangeAttachment)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[tblInspectionMessageExchangeAttachment]
                                            ([MainGuid],[Url],[ID],[Description],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime])
                                     VALUES    (@MainGuid,@Url,@ID,@Description, @UserName,@EventDatetime,@UpdatedUsername,@UpdatedEventDatetime)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", objtblMessageExchangeAttachment.MainGuid));
                command.Parameters.Add(new SqlParameter("@Url", objtblMessageExchangeAttachment.Url));
                command.Parameters.Add(new SqlParameter("@ID", objtblMessageExchangeAttachment.ID));
                command.Parameters.Add(new SqlParameter("@Description", objtblMessageExchangeAttachment.txtFileDescription));
                command.Parameters.Add(new SqlParameter("@UserName", objtblMessageExchangeAttachment.UserName));
                command.Parameters.Add(new SqlParameter("@EventDatetime", objtblMessageExchangeAttachment.EventDatetime));
                command.Parameters.Add(new SqlParameter("@UpdatedUsername", objtblMessageExchangeAttachment.UpdatedUsername));
                command.Parameters.Add(new SqlParameter("@UpdatedEventDatetime", objtblMessageExchangeAttachment.UpdatedEventDatetime));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionMessageExchangeAttachment::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


        //message attachment
        public DataTable BindMessageAttachments(Int32 ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            string strPoplatePosition = @"SELECT     tblInspectionMessageExchangeAttachment.MainGuid, tblInspectionMessageExchangeAttachment.Url, tblInspectionMessageExchangeAttachment.Description, tblInspectionParentMessage.Subject, 
                                          tblInspectionParentMessage.ParentMessageID FROM tblInspectionMessageExchangeAttachment INNER JOIN tblInspectionParentMessage ON tblInspectionMessageExchangeAttachment.ID = tblInspectionParentMessage.ParentMessageID
                                          WHERE dbo.tblInspectionMessageExchangeAttachment.ID=" + ID + "";
            SqlCommand command = new SqlCommand() { CommandText = strPoplatePosition, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblInspectionMessageExchangeAttachment");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;
            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionMessageExchangeAttachment::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }
    }
}