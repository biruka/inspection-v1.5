﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblApplicantDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [MainGuid],[person_id],[staff_code],[staff_code_sort],[cost_center_code],[unit_id],[first_name],[father_name],[grand_name],[first_am_name],[grand_am_name],[father_am_name],[allowance],[salary],[transport_allowance],[hardship_allowance],[housing_allowance],[over_time],[pension_employee_contribution],[pension_company_contribution],[is_ca_member],[ca_membership_date],[ca_termination_date],[thrift_contribution],[extra_contribution],[card_fee],[membership_fee],[is_membership_fee_returned],[is_lu_member],[labour_union],[leave_without_pay],[absentism],[fine],[sex],[religion],[birth_date],[birth_place],[marital_status],[family_size],[nationality],[town],[region],[p_o_box],[person_address],[tel_home],[tel_off_direct1],[tel_off_direct2],[tel_off_ext1],[room_no],[passport_no],[license_grade],[education_group],[education_level],[education_description],[job_title],[job_title_Sort],[occupation],[occupation_step],[position_code],[emp_date],[emp_letter],[emp_status],[current_status],[date_termination],[termination_reason],[remark],[mother_tongue],[ethnicity],[location],[photo],[first_name_sort],[father_name_sort],[grand_name_sort],[first_name_soundeX],[father_name_soundeX],[grand_name_soundeX],[leave_balance],[pension_no],[is_teacher],[is_police],[working_days],[cost_sharing_total_amount],[cost_sharing_total_paid],[cost_sharing_monthly_payment],[Health_Status],[UserId],[BankBranchGuid],[AccountNo],[BankGuid],[RankGuid],[EnteranceCondition],[IsTheoryTaken],[IsPracticalTaken],[IsInterviewTaken],[TheoryPercentage],[PracticalPercentage],[InterviewPercentage],[TheoryExamResult],[PracticalExamResult],[InterviewExamResult],[Result],[Is_Hired],[Selection_Criteria],[Zone],[Worede],[Kebele],[Waiting_Rank] FROM [dbo].[tblApplicant]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblApplicant");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblApplicant::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public tblApplicant GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblApplicant objtblApplicant = new tblApplicant();
            string strGetRecord = @"SELECT [MainGuid],[person_id],[staff_code],[staff_code_sort],[cost_center_code],[unit_id],[first_name],[father_name],[grand_name],[first_am_name],[grand_am_name],[father_am_name],[allowance],[salary],[transport_allowance],[hardship_allowance],[housing_allowance],[over_time],[pension_employee_contribution],[pension_company_contribution],[is_ca_member],[ca_membership_date],[ca_termination_date],[thrift_contribution],[extra_contribution],[card_fee],[membership_fee],[is_membership_fee_returned],[is_lu_member],[labour_union],[leave_without_pay],[absentism],[fine],[sex],[religion],[birth_date],[birth_place],[marital_status],[family_size],[nationality],[town],[region],[p_o_box],[person_address],[tel_home],[tel_off_direct1],[tel_off_direct2],[tel_off_ext1],[room_no],[passport_no],[license_grade],[education_group],[education_level],[education_description],[job_title],[job_title_Sort],[occupation],[occupation_step],[position_code],[emp_date],[emp_letter],[emp_status],[current_status],[date_termination],[termination_reason],[remark],[mother_tongue],[ethnicity],[location],[photo],[first_name_sort],[father_name_sort],[grand_name_sort],[first_name_soundeX],[father_name_soundeX],[grand_name_soundeX],[leave_balance],[pension_no],[is_teacher],[is_police],[working_days],[cost_sharing_total_amount],[cost_sharing_total_paid],[cost_sharing_monthly_payment],[Health_Status],[UserId],[BankBranchGuid],[AccountNo],[BankGuid],[RankGuid],[EnteranceCondition],[IsTheoryTaken],[IsPracticalTaken],[IsInterviewTaken],[TheoryPercentage],[PracticalPercentage],[InterviewPercentage],[TheoryExamResult],[PracticalExamResult],[InterviewExamResult],[Result],[Is_Hired],[Selection_Criteria],[Zone],[Worede],[Kebele],[Waiting_Rank] FROM [dbo].[tblApplicant] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblApplicant");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblApplicant.MainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    objtblApplicant.Person_id = (int)dTable.Rows[0]["person_id"];
                    if (dTable.Rows[0]["staff_code"].Equals(DBNull.Value))
                        objtblApplicant.Staff_code = string.Empty;
                    else
                        objtblApplicant.Staff_code = (string)dTable.Rows[0]["staff_code"];
                    if (dTable.Rows[0]["staff_code_sort"].Equals(DBNull.Value))
                        objtblApplicant.Staff_code_sort = string.Empty;
                    else
                        objtblApplicant.Staff_code_sort = (string)dTable.Rows[0]["staff_code_sort"];
                    if (dTable.Rows[0]["cost_center_code"].Equals(DBNull.Value))
                        objtblApplicant.Cost_center_code = string.Empty;
                    else
                        objtblApplicant.Cost_center_code = (string)dTable.Rows[0]["cost_center_code"];
                    if (dTable.Rows[0]["unit_id"].Equals(DBNull.Value))
                        objtblApplicant.Unit_id = 0;
                    else
                        objtblApplicant.Unit_id = (int)dTable.Rows[0]["unit_id"];
                    if (dTable.Rows[0]["first_name"].Equals(DBNull.Value))
                        objtblApplicant.First_name = string.Empty;
                    else
                        objtblApplicant.First_name = (string)dTable.Rows[0]["first_name"];
                    if (dTable.Rows[0]["father_name"].Equals(DBNull.Value))
                        objtblApplicant.Father_name = string.Empty;
                    else
                        objtblApplicant.Father_name = (string)dTable.Rows[0]["father_name"];
                    if (dTable.Rows[0]["grand_name"].Equals(DBNull.Value))
                        objtblApplicant.Grand_name = string.Empty;
                    else
                        objtblApplicant.Grand_name = (string)dTable.Rows[0]["grand_name"];
                    if (dTable.Rows[0]["first_am_name"].Equals(DBNull.Value))
                        objtblApplicant.First_am_name = string.Empty;
                    else
                        objtblApplicant.First_am_name = (string)dTable.Rows[0]["first_am_name"];
                    if (dTable.Rows[0]["grand_am_name"].Equals(DBNull.Value))
                        objtblApplicant.Grand_am_name = string.Empty;
                    else
                        objtblApplicant.Grand_am_name = (string)dTable.Rows[0]["grand_am_name"];
                    if (dTable.Rows[0]["father_am_name"].Equals(DBNull.Value))
                        objtblApplicant.Father_am_name = string.Empty;
                    else
                        objtblApplicant.Father_am_name = (string)dTable.Rows[0]["father_am_name"];
                    if (dTable.Rows[0]["allowance"].Equals(DBNull.Value))
                        objtblApplicant.Allowance = 0;
                    else
                        objtblApplicant.Allowance = (double)dTable.Rows[0]["allowance"];
                    if (dTable.Rows[0]["salary"].Equals(DBNull.Value))
                        objtblApplicant.Salary = 0;
                    else
                        objtblApplicant.Salary = (double)dTable.Rows[0]["salary"];
                    if (dTable.Rows[0]["transport_allowance"].Equals(DBNull.Value))
                        objtblApplicant.Transport_allowance = 0;
                    else
                        objtblApplicant.Transport_allowance = (double)dTable.Rows[0]["transport_allowance"];
                    if (dTable.Rows[0]["hardship_allowance"].Equals(DBNull.Value))
                        objtblApplicant.Hardship_allowance = 0;
                    else
                        objtblApplicant.Hardship_allowance = (double)dTable.Rows[0]["hardship_allowance"];
                    if (dTable.Rows[0]["housing_allowance"].Equals(DBNull.Value))
                        objtblApplicant.Housing_allowance = 0;
                    else
                        objtblApplicant.Housing_allowance = (double)dTable.Rows[0]["housing_allowance"];
                    if (dTable.Rows[0]["over_time"].Equals(DBNull.Value))
                        objtblApplicant.Over_time = 0;
                    else
                        objtblApplicant.Over_time = (double)dTable.Rows[0]["over_time"];
                    if (dTable.Rows[0]["pension_employee_contribution"].Equals(DBNull.Value))
                        objtblApplicant.Pension_employee_contribution = 0;
                    else
                        objtblApplicant.Pension_employee_contribution = (double)dTable.Rows[0]["pension_employee_contribution"];
                    if (dTable.Rows[0]["pension_company_contribution"].Equals(DBNull.Value))
                        objtblApplicant.Pension_company_contribution = 0;
                    else
                        objtblApplicant.Pension_company_contribution = (double)dTable.Rows[0]["pension_company_contribution"];
                    if (dTable.Rows[0]["is_ca_member"].Equals(DBNull.Value))
                        objtblApplicant.Is_ca_member = false;
                    else
                        objtblApplicant.Is_ca_member = (bool)dTable.Rows[0]["is_ca_member"];
                    if (dTable.Rows[0]["ca_membership_date"].Equals(DBNull.Value))
                        objtblApplicant.Ca_membership_date = DateTime.MinValue;
                    else
                        objtblApplicant.Ca_membership_date = (DateTime)dTable.Rows[0]["ca_membership_date"];
                    if (dTable.Rows[0]["ca_termination_date"].Equals(DBNull.Value))
                        objtblApplicant.Ca_termination_date = DateTime.MinValue;
                    else
                        objtblApplicant.Ca_termination_date = (DateTime)dTable.Rows[0]["ca_termination_date"];
                    if (dTable.Rows[0]["thrift_contribution"].Equals(DBNull.Value))
                        objtblApplicant.Thrift_contribution = 0;
                    else
                        objtblApplicant.Thrift_contribution = (double)dTable.Rows[0]["thrift_contribution"];
                    if (dTable.Rows[0]["extra_contribution"].Equals(DBNull.Value))
                        objtblApplicant.Extra_contribution = 0;
                    else
                        objtblApplicant.Extra_contribution = (double)dTable.Rows[0]["extra_contribution"];
                    if (dTable.Rows[0]["card_fee"].Equals(DBNull.Value))
                        objtblApplicant.Card_fee = 0;
                    else
                        objtblApplicant.Card_fee = (double)dTable.Rows[0]["card_fee"];
                    if (dTable.Rows[0]["membership_fee"].Equals(DBNull.Value))
                        objtblApplicant.Membership_fee = 0;
                    else
                        objtblApplicant.Membership_fee = (double)dTable.Rows[0]["membership_fee"];
                    if (dTable.Rows[0]["is_membership_fee_returned"].Equals(DBNull.Value))
                        objtblApplicant.Is_membership_fee_returned = false;
                    else
                        objtblApplicant.Is_membership_fee_returned = (bool)dTable.Rows[0]["is_membership_fee_returned"];
                    if (dTable.Rows[0]["is_lu_member"].Equals(DBNull.Value))
                        objtblApplicant.Is_lu_member = false;
                    else
                        objtblApplicant.Is_lu_member = (bool)dTable.Rows[0]["is_lu_member"];
                    if (dTable.Rows[0]["labour_union"].Equals(DBNull.Value))
                        objtblApplicant.Labour_union = 0;
                    else
                        objtblApplicant.Labour_union = (double)dTable.Rows[0]["labour_union"];
                    if (dTable.Rows[0]["leave_without_pay"].Equals(DBNull.Value))
                        objtblApplicant.Leave_without_pay = 0;
                    else
                        objtblApplicant.Leave_without_pay = (double)dTable.Rows[0]["leave_without_pay"];
                    if (dTable.Rows[0]["absentism"].Equals(DBNull.Value))
                        objtblApplicant.Absentism = 0;
                    else
                        objtblApplicant.Absentism = (double)dTable.Rows[0]["absentism"];
                    if (dTable.Rows[0]["fine"].Equals(DBNull.Value))
                        objtblApplicant.Fine = 0;
                    else
                        objtblApplicant.Fine = (double)dTable.Rows[0]["fine"];
                    if (dTable.Rows[0]["sex"].Equals(DBNull.Value))
                        objtblApplicant.Sex = 0;
                    else
                        objtblApplicant.Sex = (int)dTable.Rows[0]["sex"];
                    if (dTable.Rows[0]["religion"].Equals(DBNull.Value))
                        objtblApplicant.Religion = 0;
                    else
                        objtblApplicant.Religion = (int)dTable.Rows[0]["religion"];
                    if (dTable.Rows[0]["birth_date"].Equals(DBNull.Value))
                        objtblApplicant.Birth_date = DateTime.MinValue;
                    else
                        objtblApplicant.Birth_date = (DateTime)dTable.Rows[0]["birth_date"];
                    if (dTable.Rows[0]["birth_place"].Equals(DBNull.Value))
                        objtblApplicant.Birth_place = string.Empty;
                    else
                        objtblApplicant.Birth_place = (string)dTable.Rows[0]["birth_place"];
                    if (dTable.Rows[0]["marital_status"].Equals(DBNull.Value))
                        objtblApplicant.Marital_status = 0;
                    else
                        objtblApplicant.Marital_status = (int)dTable.Rows[0]["marital_status"];
                    if (dTable.Rows[0]["family_size"].Equals(DBNull.Value))
                        objtblApplicant.Family_size = 0;
                    else
                        objtblApplicant.Family_size = (int)dTable.Rows[0]["family_size"];
                    if (dTable.Rows[0]["nationality"].Equals(DBNull.Value))
                        objtblApplicant.Nationality = 0;
                    else
                        objtblApplicant.Nationality = (int)dTable.Rows[0]["nationality"];
                    if (dTable.Rows[0]["town"].Equals(DBNull.Value))
                        objtblApplicant.Town = 0;
                    else
                        objtblApplicant.Town = (int)dTable.Rows[0]["town"];
                    if (dTable.Rows[0]["region"].Equals(DBNull.Value))
                        objtblApplicant.Region = 0;
                    else
                        objtblApplicant.Region = (int)dTable.Rows[0]["region"];
                    if (dTable.Rows[0]["p_o_box"].Equals(DBNull.Value))
                        objtblApplicant.P_o_box = string.Empty;
                    else
                        objtblApplicant.P_o_box = (string)dTable.Rows[0]["p_o_box"];
                    if (dTable.Rows[0]["person_address"].Equals(DBNull.Value))
                        objtblApplicant.Person_address = string.Empty;
                    else
                        objtblApplicant.Person_address = (string)dTable.Rows[0]["person_address"];
                    if (dTable.Rows[0]["tel_home"].Equals(DBNull.Value))
                        objtblApplicant.Tel_home = string.Empty;
                    else
                        objtblApplicant.Tel_home = (string)dTable.Rows[0]["tel_home"];
                    if (dTable.Rows[0]["tel_off_direct1"].Equals(DBNull.Value))
                        objtblApplicant.Tel_off_direct1 = string.Empty;
                    else
                        objtblApplicant.Tel_off_direct1 = (string)dTable.Rows[0]["tel_off_direct1"];
                    if (dTable.Rows[0]["tel_off_direct2"].Equals(DBNull.Value))
                        objtblApplicant.Tel_off_direct2 = string.Empty;
                    else
                        objtblApplicant.Tel_off_direct2 = (string)dTable.Rows[0]["tel_off_direct2"];
                    if (dTable.Rows[0]["tel_off_ext1"].Equals(DBNull.Value))
                        objtblApplicant.Tel_off_ext1 = string.Empty;
                    else
                        objtblApplicant.Tel_off_ext1 = (string)dTable.Rows[0]["tel_off_ext1"];
                    if (dTable.Rows[0]["room_no"].Equals(DBNull.Value))
                        objtblApplicant.Room_no = string.Empty;
                    else
                        objtblApplicant.Room_no = (string)dTable.Rows[0]["room_no"];
                    if (dTable.Rows[0]["passport_no"].Equals(DBNull.Value))
                        objtblApplicant.Passport_no = string.Empty;
                    else
                        objtblApplicant.Passport_no = (string)dTable.Rows[0]["passport_no"];
                    if (dTable.Rows[0]["license_grade"].Equals(DBNull.Value))
                        objtblApplicant.License_grade = 0;
                    else
                        objtblApplicant.License_grade = (int)dTable.Rows[0]["license_grade"];
                    if (dTable.Rows[0]["education_group"].Equals(DBNull.Value))
                        objtblApplicant.Education_group = 0;
                    else
                        objtblApplicant.Education_group = (int)dTable.Rows[0]["education_group"];
                    if (dTable.Rows[0]["education_level"].Equals(DBNull.Value))
                        objtblApplicant.Education_level = 0;
                    else
                        objtblApplicant.Education_level = (int)dTable.Rows[0]["education_level"];
                    if (dTable.Rows[0]["education_description"].Equals(DBNull.Value))
                        objtblApplicant.Education_description = string.Empty;
                    else
                        objtblApplicant.Education_description = (string)dTable.Rows[0]["education_description"];
                    if (dTable.Rows[0]["job_title"].Equals(DBNull.Value))
                        objtblApplicant.Job_title = string.Empty;
                    else
                        objtblApplicant.Job_title = (string)dTable.Rows[0]["job_title"];
                    if (dTable.Rows[0]["job_title_Sort"].Equals(DBNull.Value))
                        objtblApplicant.Job_title_Sort = string.Empty;
                    else
                        objtblApplicant.Job_title_Sort = (string)dTable.Rows[0]["job_title_Sort"];
                    if (dTable.Rows[0]["occupation"].Equals(DBNull.Value))
                        objtblApplicant.Occupation = 0;
                    else
                        objtblApplicant.Occupation = (int)dTable.Rows[0]["occupation"];
                    if (dTable.Rows[0]["occupation_step"].Equals(DBNull.Value))
                        objtblApplicant.Occupation_step = string.Empty;
                    else
                        objtblApplicant.Occupation_step = (string)dTable.Rows[0]["occupation_step"];
                    if (dTable.Rows[0]["position_code"].Equals(DBNull.Value))
                        objtblApplicant.Position_code = string.Empty;
                    else
                        objtblApplicant.Position_code = (string)dTable.Rows[0]["position_code"];
                    if (dTable.Rows[0]["emp_date"].Equals(DBNull.Value))
                        objtblApplicant.Emp_date = DateTime.MinValue;
                    else
                        objtblApplicant.Emp_date = (DateTime)dTable.Rows[0]["emp_date"];
                    if (dTable.Rows[0]["emp_letter"].Equals(DBNull.Value))
                        objtblApplicant.Emp_letter = string.Empty;
                    else
                        objtblApplicant.Emp_letter = (string)dTable.Rows[0]["emp_letter"];
                    if (dTable.Rows[0]["emp_status"].Equals(DBNull.Value))
                        objtblApplicant.Emp_status = 0;
                    else
                        objtblApplicant.Emp_status = (int)dTable.Rows[0]["emp_status"];
                    if (dTable.Rows[0]["current_status"].Equals(DBNull.Value))
                        objtblApplicant.Current_status = 0;
                    else
                        objtblApplicant.Current_status = (int)dTable.Rows[0]["current_status"];
                    if (dTable.Rows[0]["date_termination"].Equals(DBNull.Value))
                        objtblApplicant.Date_termination = DateTime.MinValue;
                    else
                        objtblApplicant.Date_termination = (DateTime)dTable.Rows[0]["date_termination"];
                    if (dTable.Rows[0]["termination_reason"].Equals(DBNull.Value))
                        objtblApplicant.Termination_reason = string.Empty;
                    else
                        objtblApplicant.Termination_reason = (string)dTable.Rows[0]["termination_reason"];
                    if (dTable.Rows[0]["remark"].Equals(DBNull.Value))
                        objtblApplicant.Remark = string.Empty;
                    else
                        objtblApplicant.Remark = (string)dTable.Rows[0]["remark"];
                    if (dTable.Rows[0]["mother_tongue"].Equals(DBNull.Value))
                        objtblApplicant.Mother_tongue = 0;
                    else
                        objtblApplicant.Mother_tongue = (int)dTable.Rows[0]["mother_tongue"];
                    if (dTable.Rows[0]["ethnicity"].Equals(DBNull.Value))
                        objtblApplicant.Ethnicity = 0;
                    else
                        objtblApplicant.Ethnicity = (int)dTable.Rows[0]["ethnicity"];
                    if (dTable.Rows[0]["location"].Equals(DBNull.Value))
                        objtblApplicant.Location = 0;
                    else
                        objtblApplicant.Location = (int)dTable.Rows[0]["location"];
                    if (dTable.Rows[0]["photo"].Equals(DBNull.Value))
                        objtblApplicant.Photo = null;
                    else
                        objtblApplicant.Photo = (byte[])dTable.Rows[0]["photo"];
                    if (dTable.Rows[0]["first_name_sort"].Equals(DBNull.Value))
                        objtblApplicant.First_name_sort = string.Empty;
                    else
                        objtblApplicant.First_name_sort = (string)dTable.Rows[0]["first_name_sort"];
                    if (dTable.Rows[0]["father_name_sort"].Equals(DBNull.Value))
                        objtblApplicant.Father_name_sort = string.Empty;
                    else
                        objtblApplicant.Father_name_sort = (string)dTable.Rows[0]["father_name_sort"];
                    if (dTable.Rows[0]["grand_name_sort"].Equals(DBNull.Value))
                        objtblApplicant.Grand_name_sort = string.Empty;
                    else
                        objtblApplicant.Grand_name_sort = (string)dTable.Rows[0]["grand_name_sort"];
                    if (dTable.Rows[0]["first_name_soundeX"].Equals(DBNull.Value))
                        objtblApplicant.First_name_soundeX = string.Empty;
                    else
                        objtblApplicant.First_name_soundeX = (string)dTable.Rows[0]["first_name_soundeX"];
                    if (dTable.Rows[0]["father_name_soundeX"].Equals(DBNull.Value))
                        objtblApplicant.Father_name_soundeX = string.Empty;
                    else
                        objtblApplicant.Father_name_soundeX = (string)dTable.Rows[0]["father_name_soundeX"];
                    if (dTable.Rows[0]["grand_name_soundeX"].Equals(DBNull.Value))
                        objtblApplicant.Grand_name_soundeX = string.Empty;
                    else
                        objtblApplicant.Grand_name_soundeX = (string)dTable.Rows[0]["grand_name_soundeX"];
                    if (dTable.Rows[0]["leave_balance"].Equals(DBNull.Value))
                        objtblApplicant.Leave_balance = 0;
                    else
                        objtblApplicant.Leave_balance = (int)dTable.Rows[0]["leave_balance"];
                    if (dTable.Rows[0]["pension_no"].Equals(DBNull.Value))
                        objtblApplicant.Pension_no = string.Empty;
                    else
                        objtblApplicant.Pension_no = (string)dTable.Rows[0]["pension_no"];
                    if (dTable.Rows[0]["is_teacher"].Equals(DBNull.Value))
                        objtblApplicant.Is_teacher = false;
                    else
                        objtblApplicant.Is_teacher = (bool)dTable.Rows[0]["is_teacher"];
                    if (dTable.Rows[0]["is_police"].Equals(DBNull.Value))
                        objtblApplicant.Is_police = 0;
                    else
                        objtblApplicant.Is_police = (int)dTable.Rows[0]["is_police"];
                    if (dTable.Rows[0]["working_days"].Equals(DBNull.Value))
                        objtblApplicant.Working_days = 0;
                    else
                        objtblApplicant.Working_days = (int)dTable.Rows[0]["working_days"];
                    if (dTable.Rows[0]["cost_sharing_total_amount"].Equals(DBNull.Value))
                        objtblApplicant.Cost_sharing_total_amount = 0;
                    else
                        objtblApplicant.Cost_sharing_total_amount = (double)dTable.Rows[0]["cost_sharing_total_amount"];
                    if (dTable.Rows[0]["cost_sharing_total_paid"].Equals(DBNull.Value))
                        objtblApplicant.Cost_sharing_total_paid = 0;
                    else
                        objtblApplicant.Cost_sharing_total_paid = (double)dTable.Rows[0]["cost_sharing_total_paid"];
                    if (dTable.Rows[0]["cost_sharing_monthly_payment"].Equals(DBNull.Value))
                        objtblApplicant.Cost_sharing_monthly_payment = 0;
                    else
                        objtblApplicant.Cost_sharing_monthly_payment = (double)dTable.Rows[0]["cost_sharing_monthly_payment"];
                    if (dTable.Rows[0]["Health_Status"].Equals(DBNull.Value))
                        objtblApplicant.Health_Status = 0;
                    else
                        objtblApplicant.Health_Status = (int)dTable.Rows[0]["Health_Status"];
                    if (dTable.Rows[0]["UserId"].Equals(DBNull.Value))
                        objtblApplicant.UserId = string.Empty;
                    else
                        objtblApplicant.UserId = (string)dTable.Rows[0]["UserId"];
                    if (dTable.Rows[0]["BankBranchGuid"].Equals(DBNull.Value))
                        objtblApplicant.BankBranchGuid = Guid.Empty;
                    else
                        objtblApplicant.BankBranchGuid = (Guid)dTable.Rows[0]["BankBranchGuid"];
                    if (dTable.Rows[0]["AccountNo"].Equals(DBNull.Value))
                        objtblApplicant.AccountNo = string.Empty;
                    else
                        objtblApplicant.AccountNo = (string)dTable.Rows[0]["AccountNo"];
                    if (dTable.Rows[0]["BankGuid"].Equals(DBNull.Value))
                        objtblApplicant.BankGuid = Guid.Empty;
                    else
                        objtblApplicant.BankGuid = (Guid)dTable.Rows[0]["BankGuid"];
                    if (dTable.Rows[0]["RankGuid"].Equals(DBNull.Value))
                        objtblApplicant.RankGuid = Guid.Empty;
                    else
                        objtblApplicant.RankGuid = (Guid)dTable.Rows[0]["RankGuid"];
                    if (dTable.Rows[0]["EnteranceCondition"].Equals(DBNull.Value))
                        objtblApplicant.EnteranceCondition = 0;
                    else
                        objtblApplicant.EnteranceCondition = (int)dTable.Rows[0]["EnteranceCondition"];
                    if (dTable.Rows[0]["IsTheoryTaken"].Equals(DBNull.Value))
                        objtblApplicant.IsTheoryTaken = false;
                    else
                        objtblApplicant.IsTheoryTaken = (bool)dTable.Rows[0]["IsTheoryTaken"];
                    if (dTable.Rows[0]["IsPracticalTaken"].Equals(DBNull.Value))
                        objtblApplicant.IsPracticalTaken = false;
                    else
                        objtblApplicant.IsPracticalTaken = (bool)dTable.Rows[0]["IsPracticalTaken"];
                    if (dTable.Rows[0]["IsInterviewTaken"].Equals(DBNull.Value))
                        objtblApplicant.IsInterviewTaken = false;
                    else
                        objtblApplicant.IsInterviewTaken = (bool)dTable.Rows[0]["IsInterviewTaken"];
                    if (dTable.Rows[0]["TheoryPercentage"].Equals(DBNull.Value))
                        objtblApplicant.TheoryPercentage = 0;
                    else
                        objtblApplicant.TheoryPercentage = (int)dTable.Rows[0]["TheoryPercentage"];
                    if (dTable.Rows[0]["PracticalPercentage"].Equals(DBNull.Value))
                        objtblApplicant.PracticalPercentage = 0;
                    else
                        objtblApplicant.PracticalPercentage = (int)dTable.Rows[0]["PracticalPercentage"];
                    if (dTable.Rows[0]["InterviewPercentage"].Equals(DBNull.Value))
                        objtblApplicant.InterviewPercentage = 0;
                    else
                        objtblApplicant.InterviewPercentage = (int)dTable.Rows[0]["InterviewPercentage"];
                    if (dTable.Rows[0]["TheoryExamResult"].Equals(DBNull.Value))
                        objtblApplicant.TheoryExamResult = 0;
                    else
                        objtblApplicant.TheoryExamResult = (double)dTable.Rows[0]["TheoryExamResult"];
                    if (dTable.Rows[0]["PracticalExamResult"].Equals(DBNull.Value))
                        objtblApplicant.PracticalExamResult = 0;
                    else
                        objtblApplicant.PracticalExamResult = (double)dTable.Rows[0]["PracticalExamResult"];
                    if (dTable.Rows[0]["InterviewExamResult"].Equals(DBNull.Value))
                        objtblApplicant.InterviewExamResult = 0;
                    else
                        objtblApplicant.InterviewExamResult = (double)dTable.Rows[0]["InterviewExamResult"];
                    if (dTable.Rows[0]["Result"].Equals(DBNull.Value))
                        objtblApplicant.Result = 0;
                    else
                        objtblApplicant.Result = (double)dTable.Rows[0]["Result"];
                    if (dTable.Rows[0]["Is_Hired"].Equals(DBNull.Value))
                        objtblApplicant.Is_Hired = false;
                    else
                        objtblApplicant.Is_Hired = (bool)dTable.Rows[0]["Is_Hired"];
                    if (dTable.Rows[0]["Selection_Criteria"].Equals(DBNull.Value))
                        objtblApplicant.Selection_Criteria = 0;
                    else
                        objtblApplicant.Selection_Criteria = (int)dTable.Rows[0]["Selection_Criteria"];
                    if (dTable.Rows[0]["Zone"].Equals(DBNull.Value))
                        objtblApplicant.Zone = string.Empty;
                    else
                        objtblApplicant.Zone = (string)dTable.Rows[0]["Zone"];
                    if (dTable.Rows[0]["Worede"].Equals(DBNull.Value))
                        objtblApplicant.Worede = string.Empty;
                    else
                        objtblApplicant.Worede = (string)dTable.Rows[0]["Worede"];
                    if (dTable.Rows[0]["Kebele"].Equals(DBNull.Value))
                        objtblApplicant.Kebele = string.Empty;
                    else
                        objtblApplicant.Kebele = (string)dTable.Rows[0]["Kebele"];
                    if (dTable.Rows[0]["Waiting_Rank"].Equals(DBNull.Value))
                        objtblApplicant.Waiting_Rank = 0;
                    else
                        objtblApplicant.Waiting_Rank = (int)dTable.Rows[0]["Waiting_Rank"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblApplicant::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblApplicant;
        }

        public bool Insert(tblApplicant objtblApplicant)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[tblApplicant]
                                            ([MainGuid],[staff_code],[staff_code_sort],[cost_center_code],[unit_id],[first_name],[father_name],[grand_name],[first_am_name],[grand_am_name],[father_am_name],[allowance],[salary],[transport_allowance],[hardship_allowance],[housing_allowance],[over_time],[pension_employee_contribution],[pension_company_contribution],[is_ca_member],[ca_membership_date],[ca_termination_date],[thrift_contribution],[extra_contribution],[card_fee],[membership_fee],[is_membership_fee_returned],[is_lu_member],[labour_union],[leave_without_pay],[absentism],[fine],[sex],[religion],[birth_date],[birth_place],[marital_status],[family_size],[nationality],[town],[region],[p_o_box],[person_address],[tel_home],[tel_off_direct1],[tel_off_direct2],[tel_off_ext1],[room_no],[passport_no],[license_grade],[education_group],[education_level],[education_description],[job_title],[job_title_Sort],[occupation],[occupation_step],[position_code],[emp_date],[emp_letter],[emp_status],[current_status],[date_termination],[termination_reason],[remark],[mother_tongue],[ethnicity],[location],[photo],[first_name_sort],[father_name_sort],[grand_name_sort],[first_name_soundeX],[father_name_soundeX],[grand_name_soundeX],[leave_balance],[pension_no],[is_teacher],[is_police],[working_days],[cost_sharing_total_amount],[cost_sharing_total_paid],[cost_sharing_monthly_payment],[Health_Status],[UserId],[BankBranchGuid],[AccountNo],[BankGuid],[RankGuid],[EnteranceCondition],[IsTheoryTaken],[IsPracticalTaken],[IsInterviewTaken],[TheoryPercentage],[PracticalPercentage],[InterviewPercentage],[TheoryExamResult],[PracticalExamResult],[InterviewExamResult],[Result],[Is_Hired],[Selection_Criteria],[Zone],[Worede],[Kebele],[Waiting_Rank])
                                     VALUES    (@MainGuid,@staff_code,@staff_code_sort,@cost_center_code,@unit_id,@first_name,@father_name,@grand_name,@first_am_name,@grand_am_name,@father_am_name,@allowance,@salary,@transport_allowance,@hardship_allowance,@housing_allowance,@over_time,@pension_employee_contribution,@pension_company_contribution,@is_ca_member,@ca_membership_date,@ca_termination_date,@thrift_contribution,@extra_contribution,@card_fee,@membership_fee,@is_membership_fee_returned,@is_lu_member,@labour_union,@leave_without_pay,@absentism,@fine,@sex,@religion,@birth_date,@birth_place,@marital_status,@family_size,@nationality,@town,@region,@p_o_box,@person_address,@tel_home,@tel_off_direct1,@tel_off_direct2,@tel_off_ext1,@room_no,@passport_no,@license_grade,@education_group,@education_level,@education_description,@job_title,@job_title_Sort,@occupation,@occupation_step,@position_code,@emp_date,@emp_letter,@emp_status,@current_status,@date_termination,@termination_reason,@remark,@mother_tongue,@ethnicity,@location,@photo,@first_name_sort,@father_name_sort,@grand_name_sort,@first_name_soundeX,@father_name_soundeX,@grand_name_soundeX,@leave_balance,@pension_no,@is_teacher,@is_police,@working_days,@cost_sharing_total_amount,@cost_sharing_total_paid,@cost_sharing_monthly_payment,@Health_Status,@UserId,@BankBranchGuid,@AccountNo,@BankGuid,@RankGuid,@EnteranceCondition,@IsTheoryTaken,@IsPracticalTaken,@IsInterviewTaken,@TheoryPercentage,@PracticalPercentage,@InterviewPercentage,@TheoryExamResult,@PracticalExamResult,@InterviewExamResult,@Result,@Is_Hired,@Selection_Criteria,@Zone,@Worede,@Kebele,@Waiting_Rank)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", objtblApplicant.MainGuid));
                command.Parameters.Add(new SqlParameter("@staff_code", objtblApplicant.Staff_code));
                command.Parameters.Add(new SqlParameter("@staff_code_sort", objtblApplicant.Staff_code_sort));
                command.Parameters.Add(new SqlParameter("@cost_center_code", objtblApplicant.Cost_center_code));
                command.Parameters.Add(new SqlParameter("@unit_id", objtblApplicant.Unit_id));
                command.Parameters.Add(new SqlParameter("@first_name", objtblApplicant.First_name));
                command.Parameters.Add(new SqlParameter("@father_name", objtblApplicant.Father_name));
                command.Parameters.Add(new SqlParameter("@grand_name", objtblApplicant.Grand_name));
                command.Parameters.Add(new SqlParameter("@first_am_name", objtblApplicant.First_am_name));
                command.Parameters.Add(new SqlParameter("@grand_am_name", objtblApplicant.Grand_am_name));
                command.Parameters.Add(new SqlParameter("@father_am_name", objtblApplicant.Father_am_name));
                command.Parameters.Add(new SqlParameter("@allowance", objtblApplicant.Allowance));
                command.Parameters.Add(new SqlParameter("@salary", objtblApplicant.Salary));
                command.Parameters.Add(new SqlParameter("@transport_allowance", objtblApplicant.Transport_allowance));
                command.Parameters.Add(new SqlParameter("@hardship_allowance", objtblApplicant.Hardship_allowance));
                command.Parameters.Add(new SqlParameter("@housing_allowance", objtblApplicant.Housing_allowance));
                command.Parameters.Add(new SqlParameter("@over_time", objtblApplicant.Over_time));
                command.Parameters.Add(new SqlParameter("@pension_employee_contribution", objtblApplicant.Pension_employee_contribution));
                command.Parameters.Add(new SqlParameter("@pension_company_contribution", objtblApplicant.Pension_company_contribution));
                command.Parameters.Add(new SqlParameter("@is_ca_member", objtblApplicant.Is_ca_member));
                command.Parameters.Add(new SqlParameter("@ca_membership_date", objtblApplicant.Ca_membership_date));
                command.Parameters.Add(new SqlParameter("@ca_termination_date", objtblApplicant.Ca_termination_date));
                command.Parameters.Add(new SqlParameter("@thrift_contribution", objtblApplicant.Thrift_contribution));
                command.Parameters.Add(new SqlParameter("@extra_contribution", objtblApplicant.Extra_contribution));
                command.Parameters.Add(new SqlParameter("@card_fee", objtblApplicant.Card_fee));
                command.Parameters.Add(new SqlParameter("@membership_fee", objtblApplicant.Membership_fee));
                command.Parameters.Add(new SqlParameter("@is_membership_fee_returned", objtblApplicant.Is_membership_fee_returned));
                command.Parameters.Add(new SqlParameter("@is_lu_member", objtblApplicant.Is_lu_member));
                command.Parameters.Add(new SqlParameter("@labour_union", objtblApplicant.Labour_union));
                command.Parameters.Add(new SqlParameter("@leave_without_pay", objtblApplicant.Leave_without_pay));
                command.Parameters.Add(new SqlParameter("@absentism", objtblApplicant.Absentism));
                command.Parameters.Add(new SqlParameter("@fine", objtblApplicant.Fine));
                command.Parameters.Add(new SqlParameter("@sex", objtblApplicant.Sex));
                command.Parameters.Add(new SqlParameter("@religion", objtblApplicant.Religion));
                command.Parameters.Add(new SqlParameter("@birth_date", objtblApplicant.Birth_date));
                command.Parameters.Add(new SqlParameter("@birth_place", objtblApplicant.Birth_place));
                command.Parameters.Add(new SqlParameter("@marital_status", objtblApplicant.Marital_status));
                command.Parameters.Add(new SqlParameter("@family_size", objtblApplicant.Family_size));
                command.Parameters.Add(new SqlParameter("@nationality", objtblApplicant.Nationality));
                command.Parameters.Add(new SqlParameter("@town", objtblApplicant.Town));
                command.Parameters.Add(new SqlParameter("@region", objtblApplicant.Region));
                command.Parameters.Add(new SqlParameter("@p_o_box", objtblApplicant.P_o_box));
                command.Parameters.Add(new SqlParameter("@person_address", objtblApplicant.Person_address));
                command.Parameters.Add(new SqlParameter("@tel_home", objtblApplicant.Tel_home));
                command.Parameters.Add(new SqlParameter("@tel_off_direct1", objtblApplicant.Tel_off_direct1));
                command.Parameters.Add(new SqlParameter("@tel_off_direct2", objtblApplicant.Tel_off_direct2));
                command.Parameters.Add(new SqlParameter("@tel_off_ext1", objtblApplicant.Tel_off_ext1));
                command.Parameters.Add(new SqlParameter("@room_no", objtblApplicant.Room_no));
                command.Parameters.Add(new SqlParameter("@passport_no", objtblApplicant.Passport_no));
                command.Parameters.Add(new SqlParameter("@license_grade", objtblApplicant.License_grade));
                command.Parameters.Add(new SqlParameter("@education_group", objtblApplicant.Education_group));
                command.Parameters.Add(new SqlParameter("@education_level", objtblApplicant.Education_level));
                command.Parameters.Add(new SqlParameter("@education_description", objtblApplicant.Education_description));
                command.Parameters.Add(new SqlParameter("@job_title", objtblApplicant.Job_title));
                command.Parameters.Add(new SqlParameter("@job_title_Sort", objtblApplicant.Job_title_Sort));
                command.Parameters.Add(new SqlParameter("@occupation", objtblApplicant.Occupation));
                command.Parameters.Add(new SqlParameter("@occupation_step", objtblApplicant.Occupation_step));
                command.Parameters.Add(new SqlParameter("@position_code", objtblApplicant.Position_code));
                command.Parameters.Add(new SqlParameter("@emp_date", objtblApplicant.Emp_date));
                command.Parameters.Add(new SqlParameter("@emp_letter", objtblApplicant.Emp_letter));
                command.Parameters.Add(new SqlParameter("@emp_status", objtblApplicant.Emp_status));
                command.Parameters.Add(new SqlParameter("@current_status", objtblApplicant.Current_status));
                command.Parameters.Add(new SqlParameter("@date_termination", objtblApplicant.Date_termination));
                command.Parameters.Add(new SqlParameter("@termination_reason", objtblApplicant.Termination_reason));
                command.Parameters.Add(new SqlParameter("@remark", objtblApplicant.Remark));
                command.Parameters.Add(new SqlParameter("@mother_tongue", objtblApplicant.Mother_tongue));
                command.Parameters.Add(new SqlParameter("@ethnicity", objtblApplicant.Ethnicity));
                command.Parameters.Add(new SqlParameter("@location", objtblApplicant.Location));
                command.Parameters.Add(new SqlParameter("@photo", objtblApplicant.Photo));
                command.Parameters.Add(new SqlParameter("@first_name_sort", objtblApplicant.First_name_sort));
                command.Parameters.Add(new SqlParameter("@father_name_sort", objtblApplicant.Father_name_sort));
                command.Parameters.Add(new SqlParameter("@grand_name_sort", objtblApplicant.Grand_name_sort));
                command.Parameters.Add(new SqlParameter("@first_name_soundeX", objtblApplicant.First_name_soundeX));
                command.Parameters.Add(new SqlParameter("@father_name_soundeX", objtblApplicant.Father_name_soundeX));
                command.Parameters.Add(new SqlParameter("@grand_name_soundeX", objtblApplicant.Grand_name_soundeX));
                command.Parameters.Add(new SqlParameter("@leave_balance", objtblApplicant.Leave_balance));
                command.Parameters.Add(new SqlParameter("@pension_no", objtblApplicant.Pension_no));
                command.Parameters.Add(new SqlParameter("@is_teacher", objtblApplicant.Is_teacher));
                command.Parameters.Add(new SqlParameter("@is_police", objtblApplicant.Is_police));
                command.Parameters.Add(new SqlParameter("@working_days", objtblApplicant.Working_days));
                command.Parameters.Add(new SqlParameter("@cost_sharing_total_amount", objtblApplicant.Cost_sharing_total_amount));
                command.Parameters.Add(new SqlParameter("@cost_sharing_total_paid", objtblApplicant.Cost_sharing_total_paid));
                command.Parameters.Add(new SqlParameter("@cost_sharing_monthly_payment", objtblApplicant.Cost_sharing_monthly_payment));
                command.Parameters.Add(new SqlParameter("@Health_Status", objtblApplicant.Health_Status));
                command.Parameters.Add(new SqlParameter("@UserId", objtblApplicant.UserId));
                command.Parameters.Add(new SqlParameter("@BankBranchGuid", objtblApplicant.BankBranchGuid));
                command.Parameters.Add(new SqlParameter("@AccountNo", objtblApplicant.AccountNo));
                command.Parameters.Add(new SqlParameter("@BankGuid", objtblApplicant.BankGuid));
                command.Parameters.Add(new SqlParameter("@RankGuid", objtblApplicant.RankGuid));
                command.Parameters.Add(new SqlParameter("@EnteranceCondition", objtblApplicant.EnteranceCondition));
                command.Parameters.Add(new SqlParameter("@IsTheoryTaken", objtblApplicant.IsTheoryTaken));
                command.Parameters.Add(new SqlParameter("@IsPracticalTaken", objtblApplicant.IsPracticalTaken));
                command.Parameters.Add(new SqlParameter("@IsInterviewTaken", objtblApplicant.IsInterviewTaken));
                command.Parameters.Add(new SqlParameter("@TheoryPercentage", objtblApplicant.TheoryPercentage));
                command.Parameters.Add(new SqlParameter("@PracticalPercentage", objtblApplicant.PracticalPercentage));
                command.Parameters.Add(new SqlParameter("@InterviewPercentage", objtblApplicant.InterviewPercentage));
                command.Parameters.Add(new SqlParameter("@TheoryExamResult", objtblApplicant.TheoryExamResult));
                command.Parameters.Add(new SqlParameter("@PracticalExamResult", objtblApplicant.PracticalExamResult));
                command.Parameters.Add(new SqlParameter("@InterviewExamResult", objtblApplicant.InterviewExamResult));
                command.Parameters.Add(new SqlParameter("@Result", objtblApplicant.Result));
                command.Parameters.Add(new SqlParameter("@Is_Hired", objtblApplicant.Is_Hired));
                command.Parameters.Add(new SqlParameter("@Selection_Criteria", objtblApplicant.Selection_Criteria));
                command.Parameters.Add(new SqlParameter("@Zone", objtblApplicant.Zone));
                command.Parameters.Add(new SqlParameter("@Worede", objtblApplicant.Worede));
                command.Parameters.Add(new SqlParameter("@Kebele", objtblApplicant.Kebele));
                command.Parameters.Add(new SqlParameter("@Waiting_Rank", objtblApplicant.Waiting_Rank));
                command.Parameters.Add(new SqlParameter("@person_id", objtblApplicant.Person_id));
                command.Parameters["@person_id"].Direction = ParameterDirection.Output;


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                objtblApplicant.Person_id = (int)command.Parameters["@person_id"].Value;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblApplicant::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(tblApplicant objtblApplicant)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[tblApplicant] SET     [MainGuid]=@MainGuid,    [staff_code]=@staff_code,    [staff_code_sort]=@staff_code_sort,    [cost_center_code]=@cost_center_code,    [unit_id]=@unit_id,    [first_name]=@first_name,    [father_name]=@father_name,    [grand_name]=@grand_name,    [first_am_name]=@first_am_name,    [grand_am_name]=@grand_am_name,    [father_am_name]=@father_am_name,    [allowance]=@allowance,    [salary]=@salary,    [transport_allowance]=@transport_allowance,    [hardship_allowance]=@hardship_allowance,    [housing_allowance]=@housing_allowance,    [over_time]=@over_time,    [pension_employee_contribution]=@pension_employee_contribution,    [pension_company_contribution]=@pension_company_contribution,    [is_ca_member]=@is_ca_member,    [ca_membership_date]=@ca_membership_date,    [ca_termination_date]=@ca_termination_date,    [thrift_contribution]=@thrift_contribution,    [extra_contribution]=@extra_contribution,    [card_fee]=@card_fee,    [membership_fee]=@membership_fee,    [is_membership_fee_returned]=@is_membership_fee_returned,    [is_lu_member]=@is_lu_member,    [labour_union]=@labour_union,    [leave_without_pay]=@leave_without_pay,    [absentism]=@absentism,    [fine]=@fine,    [sex]=@sex,    [religion]=@religion,    [birth_date]=@birth_date,    [birth_place]=@birth_place,    [marital_status]=@marital_status,    [family_size]=@family_size,    [nationality]=@nationality,    [town]=@town,    [region]=@region,    [p_o_box]=@p_o_box,    [person_address]=@person_address,    [tel_home]=@tel_home,    [tel_off_direct1]=@tel_off_direct1,    [tel_off_direct2]=@tel_off_direct2,    [tel_off_ext1]=@tel_off_ext1,    [room_no]=@room_no,    [passport_no]=@passport_no,    [license_grade]=@license_grade,    [education_group]=@education_group,    [education_level]=@education_level,    [education_description]=@education_description,    [job_title]=@job_title,    [job_title_Sort]=@job_title_Sort,    [occupation]=@occupation,    [occupation_step]=@occupation_step,    [position_code]=@position_code,    [emp_date]=@emp_date,    [emp_letter]=@emp_letter,    [emp_status]=@emp_status,    [current_status]=@current_status,    [date_termination]=@date_termination,    [termination_reason]=@termination_reason,    [remark]=@remark,    [mother_tongue]=@mother_tongue,    [ethnicity]=@ethnicity,    [location]=@location,    [photo]=@photo,    [first_name_sort]=@first_name_sort,    [father_name_sort]=@father_name_sort,    [grand_name_sort]=@grand_name_sort,    [first_name_soundeX]=@first_name_soundeX,    [father_name_soundeX]=@father_name_soundeX,    [grand_name_soundeX]=@grand_name_soundeX,    [leave_balance]=@leave_balance,    [pension_no]=@pension_no,    [is_teacher]=@is_teacher,    [is_police]=@is_police,    [working_days]=@working_days,    [cost_sharing_total_amount]=@cost_sharing_total_amount,    [cost_sharing_total_paid]=@cost_sharing_total_paid,    [cost_sharing_monthly_payment]=@cost_sharing_monthly_payment,    [Health_Status]=@Health_Status,    [UserId]=@UserId,    [BankBranchGuid]=@BankBranchGuid,    [AccountNo]=@AccountNo,    [BankGuid]=@BankGuid,    [RankGuid]=@RankGuid,    [EnteranceCondition]=@EnteranceCondition,    [IsTheoryTaken]=@IsTheoryTaken,    [IsPracticalTaken]=@IsPracticalTaken,    [IsInterviewTaken]=@IsInterviewTaken,    [TheoryPercentage]=@TheoryPercentage,    [PracticalPercentage]=@PracticalPercentage,    [InterviewPercentage]=@InterviewPercentage,    [TheoryExamResult]=@TheoryExamResult,    [PracticalExamResult]=@PracticalExamResult,    [InterviewExamResult]=@InterviewExamResult,    [Result]=@Result,    [Is_Hired]=@Is_Hired,    [Selection_Criteria]=@Selection_Criteria,    [Zone]=@Zone,    [Worede]=@Worede,    [Kebele]=@Kebele,    [Waiting_Rank]=@Waiting_Rank WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", objtblApplicant.MainGuid));
                command.Parameters.Add(new SqlParameter("@person_id", objtblApplicant.Person_id));
                command.Parameters.Add(new SqlParameter("@staff_code", objtblApplicant.Staff_code));
                command.Parameters.Add(new SqlParameter("@staff_code_sort", objtblApplicant.Staff_code_sort));
                command.Parameters.Add(new SqlParameter("@cost_center_code", objtblApplicant.Cost_center_code));
                command.Parameters.Add(new SqlParameter("@unit_id", objtblApplicant.Unit_id));
                command.Parameters.Add(new SqlParameter("@first_name", objtblApplicant.First_name));
                command.Parameters.Add(new SqlParameter("@father_name", objtblApplicant.Father_name));
                command.Parameters.Add(new SqlParameter("@grand_name", objtblApplicant.Grand_name));
                command.Parameters.Add(new SqlParameter("@first_am_name", objtblApplicant.First_am_name));
                command.Parameters.Add(new SqlParameter("@grand_am_name", objtblApplicant.Grand_am_name));
                command.Parameters.Add(new SqlParameter("@father_am_name", objtblApplicant.Father_am_name));
                command.Parameters.Add(new SqlParameter("@allowance", objtblApplicant.Allowance));
                command.Parameters.Add(new SqlParameter("@salary", objtblApplicant.Salary));
                command.Parameters.Add(new SqlParameter("@transport_allowance", objtblApplicant.Transport_allowance));
                command.Parameters.Add(new SqlParameter("@hardship_allowance", objtblApplicant.Hardship_allowance));
                command.Parameters.Add(new SqlParameter("@housing_allowance", objtblApplicant.Housing_allowance));
                command.Parameters.Add(new SqlParameter("@over_time", objtblApplicant.Over_time));
                command.Parameters.Add(new SqlParameter("@pension_employee_contribution", objtblApplicant.Pension_employee_contribution));
                command.Parameters.Add(new SqlParameter("@pension_company_contribution", objtblApplicant.Pension_company_contribution));
                command.Parameters.Add(new SqlParameter("@is_ca_member", objtblApplicant.Is_ca_member));
                command.Parameters.Add(new SqlParameter("@ca_membership_date", objtblApplicant.Ca_membership_date));
                command.Parameters.Add(new SqlParameter("@ca_termination_date", objtblApplicant.Ca_termination_date));
                command.Parameters.Add(new SqlParameter("@thrift_contribution", objtblApplicant.Thrift_contribution));
                command.Parameters.Add(new SqlParameter("@extra_contribution", objtblApplicant.Extra_contribution));
                command.Parameters.Add(new SqlParameter("@card_fee", objtblApplicant.Card_fee));
                command.Parameters.Add(new SqlParameter("@membership_fee", objtblApplicant.Membership_fee));
                command.Parameters.Add(new SqlParameter("@is_membership_fee_returned", objtblApplicant.Is_membership_fee_returned));
                command.Parameters.Add(new SqlParameter("@is_lu_member", objtblApplicant.Is_lu_member));
                command.Parameters.Add(new SqlParameter("@labour_union", objtblApplicant.Labour_union));
                command.Parameters.Add(new SqlParameter("@leave_without_pay", objtblApplicant.Leave_without_pay));
                command.Parameters.Add(new SqlParameter("@absentism", objtblApplicant.Absentism));
                command.Parameters.Add(new SqlParameter("@fine", objtblApplicant.Fine));
                command.Parameters.Add(new SqlParameter("@sex", objtblApplicant.Sex));
                command.Parameters.Add(new SqlParameter("@religion", objtblApplicant.Religion));
                command.Parameters.Add(new SqlParameter("@birth_date", objtblApplicant.Birth_date));
                command.Parameters.Add(new SqlParameter("@birth_place", objtblApplicant.Birth_place));
                command.Parameters.Add(new SqlParameter("@marital_status", objtblApplicant.Marital_status));
                command.Parameters.Add(new SqlParameter("@family_size", objtblApplicant.Family_size));
                command.Parameters.Add(new SqlParameter("@nationality", objtblApplicant.Nationality));
                command.Parameters.Add(new SqlParameter("@town", objtblApplicant.Town));
                command.Parameters.Add(new SqlParameter("@region", objtblApplicant.Region));
                command.Parameters.Add(new SqlParameter("@p_o_box", objtblApplicant.P_o_box));
                command.Parameters.Add(new SqlParameter("@person_address", objtblApplicant.Person_address));
                command.Parameters.Add(new SqlParameter("@tel_home", objtblApplicant.Tel_home));
                command.Parameters.Add(new SqlParameter("@tel_off_direct1", objtblApplicant.Tel_off_direct1));
                command.Parameters.Add(new SqlParameter("@tel_off_direct2", objtblApplicant.Tel_off_direct2));
                command.Parameters.Add(new SqlParameter("@tel_off_ext1", objtblApplicant.Tel_off_ext1));
                command.Parameters.Add(new SqlParameter("@room_no", objtblApplicant.Room_no));
                command.Parameters.Add(new SqlParameter("@passport_no", objtblApplicant.Passport_no));
                command.Parameters.Add(new SqlParameter("@license_grade", objtblApplicant.License_grade));
                command.Parameters.Add(new SqlParameter("@education_group", objtblApplicant.Education_group));
                command.Parameters.Add(new SqlParameter("@education_level", objtblApplicant.Education_level));
                command.Parameters.Add(new SqlParameter("@education_description", objtblApplicant.Education_description));
                command.Parameters.Add(new SqlParameter("@job_title", objtblApplicant.Job_title));
                command.Parameters.Add(new SqlParameter("@job_title_Sort", objtblApplicant.Job_title_Sort));
                command.Parameters.Add(new SqlParameter("@occupation", objtblApplicant.Occupation));
                command.Parameters.Add(new SqlParameter("@occupation_step", objtblApplicant.Occupation_step));
                command.Parameters.Add(new SqlParameter("@position_code", objtblApplicant.Position_code));
                command.Parameters.Add(new SqlParameter("@emp_date", objtblApplicant.Emp_date));
                command.Parameters.Add(new SqlParameter("@emp_letter", objtblApplicant.Emp_letter));
                command.Parameters.Add(new SqlParameter("@emp_status", objtblApplicant.Emp_status));
                command.Parameters.Add(new SqlParameter("@current_status", objtblApplicant.Current_status));
                command.Parameters.Add(new SqlParameter("@date_termination", objtblApplicant.Date_termination));
                command.Parameters.Add(new SqlParameter("@termination_reason", objtblApplicant.Termination_reason));
                command.Parameters.Add(new SqlParameter("@remark", objtblApplicant.Remark));
                command.Parameters.Add(new SqlParameter("@mother_tongue", objtblApplicant.Mother_tongue));
                command.Parameters.Add(new SqlParameter("@ethnicity", objtblApplicant.Ethnicity));
                command.Parameters.Add(new SqlParameter("@location", objtblApplicant.Location));
                command.Parameters.Add(new SqlParameter("@photo", objtblApplicant.Photo));
                command.Parameters.Add(new SqlParameter("@first_name_sort", objtblApplicant.First_name_sort));
                command.Parameters.Add(new SqlParameter("@father_name_sort", objtblApplicant.Father_name_sort));
                command.Parameters.Add(new SqlParameter("@grand_name_sort", objtblApplicant.Grand_name_sort));
                command.Parameters.Add(new SqlParameter("@first_name_soundeX", objtblApplicant.First_name_soundeX));
                command.Parameters.Add(new SqlParameter("@father_name_soundeX", objtblApplicant.Father_name_soundeX));
                command.Parameters.Add(new SqlParameter("@grand_name_soundeX", objtblApplicant.Grand_name_soundeX));
                command.Parameters.Add(new SqlParameter("@leave_balance", objtblApplicant.Leave_balance));
                command.Parameters.Add(new SqlParameter("@pension_no", objtblApplicant.Pension_no));
                command.Parameters.Add(new SqlParameter("@is_teacher", objtblApplicant.Is_teacher));
                command.Parameters.Add(new SqlParameter("@is_police", objtblApplicant.Is_police));
                command.Parameters.Add(new SqlParameter("@working_days", objtblApplicant.Working_days));
                command.Parameters.Add(new SqlParameter("@cost_sharing_total_amount", objtblApplicant.Cost_sharing_total_amount));
                command.Parameters.Add(new SqlParameter("@cost_sharing_total_paid", objtblApplicant.Cost_sharing_total_paid));
                command.Parameters.Add(new SqlParameter("@cost_sharing_monthly_payment", objtblApplicant.Cost_sharing_monthly_payment));
                command.Parameters.Add(new SqlParameter("@Health_Status", objtblApplicant.Health_Status));
                command.Parameters.Add(new SqlParameter("@UserId", objtblApplicant.UserId));
                command.Parameters.Add(new SqlParameter("@BankBranchGuid", objtblApplicant.BankBranchGuid));
                command.Parameters.Add(new SqlParameter("@AccountNo", objtblApplicant.AccountNo));
                command.Parameters.Add(new SqlParameter("@BankGuid", objtblApplicant.BankGuid));
                command.Parameters.Add(new SqlParameter("@RankGuid", objtblApplicant.RankGuid));
                command.Parameters.Add(new SqlParameter("@EnteranceCondition", objtblApplicant.EnteranceCondition));
                command.Parameters.Add(new SqlParameter("@IsTheoryTaken", objtblApplicant.IsTheoryTaken));
                command.Parameters.Add(new SqlParameter("@IsPracticalTaken", objtblApplicant.IsPracticalTaken));
                command.Parameters.Add(new SqlParameter("@IsInterviewTaken", objtblApplicant.IsInterviewTaken));
                command.Parameters.Add(new SqlParameter("@TheoryPercentage", objtblApplicant.TheoryPercentage));
                command.Parameters.Add(new SqlParameter("@PracticalPercentage", objtblApplicant.PracticalPercentage));
                command.Parameters.Add(new SqlParameter("@InterviewPercentage", objtblApplicant.InterviewPercentage));
                command.Parameters.Add(new SqlParameter("@TheoryExamResult", objtblApplicant.TheoryExamResult));
                command.Parameters.Add(new SqlParameter("@PracticalExamResult", objtblApplicant.PracticalExamResult));
                command.Parameters.Add(new SqlParameter("@InterviewExamResult", objtblApplicant.InterviewExamResult));
                command.Parameters.Add(new SqlParameter("@Result", objtblApplicant.Result));
                command.Parameters.Add(new SqlParameter("@Is_Hired", objtblApplicant.Is_Hired));
                command.Parameters.Add(new SqlParameter("@Selection_Criteria", objtblApplicant.Selection_Criteria));
                command.Parameters.Add(new SqlParameter("@Zone", objtblApplicant.Zone));
                command.Parameters.Add(new SqlParameter("@Worede", objtblApplicant.Worede));
                command.Parameters.Add(new SqlParameter("@Kebele", objtblApplicant.Kebele));
                command.Parameters.Add(new SqlParameter("@Waiting_Rank", objtblApplicant.Waiting_Rank));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblApplicant::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblApplicant] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblApplicant::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<tblApplicant> GetList()
        {
            List<tblApplicant> RecordsList = new List<tblApplicant>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [MainGuid],[person_id],[staff_code],[staff_code_sort],[cost_center_code],[unit_id],[first_name],[father_name],[grand_name],[first_am_name],[grand_am_name],[father_am_name],[allowance],[salary],[transport_allowance],[hardship_allowance],[housing_allowance],[over_time],[pension_employee_contribution],[pension_company_contribution],[is_ca_member],[ca_membership_date],[ca_termination_date],[thrift_contribution],[extra_contribution],[card_fee],[membership_fee],[is_membership_fee_returned],[is_lu_member],[labour_union],[leave_without_pay],[absentism],[fine],[sex],[religion],[birth_date],[birth_place],[marital_status],[family_size],[nationality],[town],[region],[p_o_box],[person_address],[tel_home],[tel_off_direct1],[tel_off_direct2],[tel_off_ext1],[room_no],[passport_no],[license_grade],[education_group],[education_level],[education_description],[job_title],[job_title_Sort],[occupation],[occupation_step],[position_code],[emp_date],[emp_letter],[emp_status],[current_status],[date_termination],[termination_reason],[remark],[mother_tongue],[ethnicity],[location],[photo],[first_name_sort],[father_name_sort],[grand_name_sort],[first_name_soundeX],[father_name_soundeX],[grand_name_soundeX],[leave_balance],[pension_no],[is_teacher],[is_police],[working_days],[cost_sharing_total_amount],[cost_sharing_total_paid],[cost_sharing_monthly_payment],[Health_Status],[UserId],[BankBranchGuid],[AccountNo],[BankGuid],[RankGuid],[EnteranceCondition],[IsTheoryTaken],[IsPracticalTaken],[IsInterviewTaken],[TheoryPercentage],[PracticalPercentage],[InterviewPercentage],[TheoryExamResult],[PracticalExamResult],[InterviewExamResult],[Result],[Is_Hired],[Selection_Criteria],[Zone],[Worede],[Kebele],[Waiting_Rank] FROM [dbo].[tblApplicant]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblApplicant objtblApplicant = new tblApplicant();
                    if (dr["MainGuid"].Equals(DBNull.Value))
                        objtblApplicant.MainGuid = Guid.Empty;
                    else
                        objtblApplicant.MainGuid = (Guid)dr["MainGuid"];
                    if (dr["person_id"].Equals(DBNull.Value))
                        objtblApplicant.Person_id = 0;
                    else
                        objtblApplicant.Person_id = (int)dr["person_id"];
                    if (dr["staff_code"].Equals(DBNull.Value))
                        objtblApplicant.Staff_code = string.Empty;
                    else
                        objtblApplicant.Staff_code = (string)dr["staff_code"];
                    if (dr["staff_code_sort"].Equals(DBNull.Value))
                        objtblApplicant.Staff_code_sort = string.Empty;
                    else
                        objtblApplicant.Staff_code_sort = (string)dr["staff_code_sort"];
                    if (dr["cost_center_code"].Equals(DBNull.Value))
                        objtblApplicant.Cost_center_code = string.Empty;
                    else
                        objtblApplicant.Cost_center_code = (string)dr["cost_center_code"];
                    if (dr["unit_id"].Equals(DBNull.Value))
                        objtblApplicant.Unit_id = 0;
                    else
                        objtblApplicant.Unit_id = (int)dr["unit_id"];
                    if (dr["first_name"].Equals(DBNull.Value))
                        objtblApplicant.First_name = string.Empty;
                    else
                        objtblApplicant.First_name = (string)dr["first_name"];
                    if (dr["father_name"].Equals(DBNull.Value))
                        objtblApplicant.Father_name = string.Empty;
                    else
                        objtblApplicant.Father_name = (string)dr["father_name"];
                    if (dr["grand_name"].Equals(DBNull.Value))
                        objtblApplicant.Grand_name = string.Empty;
                    else
                        objtblApplicant.Grand_name = (string)dr["grand_name"];
                    if (dr["first_am_name"].Equals(DBNull.Value))
                        objtblApplicant.First_am_name = string.Empty;
                    else
                        objtblApplicant.First_am_name = (string)dr["first_am_name"];
                    if (dr["grand_am_name"].Equals(DBNull.Value))
                        objtblApplicant.Grand_am_name = string.Empty;
                    else
                        objtblApplicant.Grand_am_name = (string)dr["grand_am_name"];
                    if (dr["father_am_name"].Equals(DBNull.Value))
                        objtblApplicant.Father_am_name = string.Empty;
                    else
                        objtblApplicant.Father_am_name = (string)dr["father_am_name"];
                    if (dr["allowance"].Equals(DBNull.Value))
                        objtblApplicant.Allowance = 0;
                    else
                        objtblApplicant.Allowance = (double)dr["allowance"];
                    if (dr["salary"].Equals(DBNull.Value))
                        objtblApplicant.Salary = 0;
                    else
                        objtblApplicant.Salary = (double)dr["salary"];
                    if (dr["transport_allowance"].Equals(DBNull.Value))
                        objtblApplicant.Transport_allowance = 0;
                    else
                        objtblApplicant.Transport_allowance = (double)dr["transport_allowance"];
                    if (dr["hardship_allowance"].Equals(DBNull.Value))
                        objtblApplicant.Hardship_allowance = 0;
                    else
                        objtblApplicant.Hardship_allowance = (double)dr["hardship_allowance"];
                    if (dr["housing_allowance"].Equals(DBNull.Value))
                        objtblApplicant.Housing_allowance = 0;
                    else
                        objtblApplicant.Housing_allowance = (double)dr["housing_allowance"];
                    if (dr["over_time"].Equals(DBNull.Value))
                        objtblApplicant.Over_time = 0;
                    else
                        objtblApplicant.Over_time = (double)dr["over_time"];
                    if (dr["pension_employee_contribution"].Equals(DBNull.Value))
                        objtblApplicant.Pension_employee_contribution = 0;
                    else
                        objtblApplicant.Pension_employee_contribution = (double)dr["pension_employee_contribution"];
                    if (dr["pension_company_contribution"].Equals(DBNull.Value))
                        objtblApplicant.Pension_company_contribution = 0;
                    else
                        objtblApplicant.Pension_company_contribution = (double)dr["pension_company_contribution"];
                    if (dr["is_ca_member"].Equals(DBNull.Value))
                        objtblApplicant.Is_ca_member = false;
                    else
                        objtblApplicant.Is_ca_member = (bool)dr["is_ca_member"];
                    if (dr["ca_membership_date"].Equals(DBNull.Value))
                        objtblApplicant.Ca_membership_date = DateTime.MinValue;
                    else
                        objtblApplicant.Ca_membership_date = (DateTime)dr["ca_membership_date"];
                    if (dr["ca_termination_date"].Equals(DBNull.Value))
                        objtblApplicant.Ca_termination_date = DateTime.MinValue;
                    else
                        objtblApplicant.Ca_termination_date = (DateTime)dr["ca_termination_date"];
                    if (dr["thrift_contribution"].Equals(DBNull.Value))
                        objtblApplicant.Thrift_contribution = 0;
                    else
                        objtblApplicant.Thrift_contribution = (double)dr["thrift_contribution"];
                    if (dr["extra_contribution"].Equals(DBNull.Value))
                        objtblApplicant.Extra_contribution = 0;
                    else
                        objtblApplicant.Extra_contribution = (double)dr["extra_contribution"];
                    if (dr["card_fee"].Equals(DBNull.Value))
                        objtblApplicant.Card_fee = 0;
                    else
                        objtblApplicant.Card_fee = (double)dr["card_fee"];
                    if (dr["membership_fee"].Equals(DBNull.Value))
                        objtblApplicant.Membership_fee = 0;
                    else
                        objtblApplicant.Membership_fee = (double)dr["membership_fee"];
                    if (dr["is_membership_fee_returned"].Equals(DBNull.Value))
                        objtblApplicant.Is_membership_fee_returned = false;
                    else
                        objtblApplicant.Is_membership_fee_returned = (bool)dr["is_membership_fee_returned"];
                    if (dr["is_lu_member"].Equals(DBNull.Value))
                        objtblApplicant.Is_lu_member = false;
                    else
                        objtblApplicant.Is_lu_member = (bool)dr["is_lu_member"];
                    if (dr["labour_union"].Equals(DBNull.Value))
                        objtblApplicant.Labour_union = 0;
                    else
                        objtblApplicant.Labour_union = (double)dr["labour_union"];
                    if (dr["leave_without_pay"].Equals(DBNull.Value))
                        objtblApplicant.Leave_without_pay = 0;
                    else
                        objtblApplicant.Leave_without_pay = (double)dr["leave_without_pay"];
                    if (dr["absentism"].Equals(DBNull.Value))
                        objtblApplicant.Absentism = 0;
                    else
                        objtblApplicant.Absentism = (double)dr["absentism"];
                    if (dr["fine"].Equals(DBNull.Value))
                        objtblApplicant.Fine = 0;
                    else
                        objtblApplicant.Fine = (double)dr["fine"];
                    if (dr["sex"].Equals(DBNull.Value))
                        objtblApplicant.Sex = 0;
                    else
                        objtblApplicant.Sex = (int)dr["sex"];
                    if (dr["religion"].Equals(DBNull.Value))
                        objtblApplicant.Religion = 0;
                    else
                        objtblApplicant.Religion = (int)dr["religion"];
                    if (dr["birth_date"].Equals(DBNull.Value))
                        objtblApplicant.Birth_date = DateTime.MinValue;
                    else
                        objtblApplicant.Birth_date = (DateTime)dr["birth_date"];
                    if (dr["birth_place"].Equals(DBNull.Value))
                        objtblApplicant.Birth_place = string.Empty;
                    else
                        objtblApplicant.Birth_place = (string)dr["birth_place"];
                    if (dr["marital_status"].Equals(DBNull.Value))
                        objtblApplicant.Marital_status = 0;
                    else
                        objtblApplicant.Marital_status = (int)dr["marital_status"];
                    if (dr["family_size"].Equals(DBNull.Value))
                        objtblApplicant.Family_size = 0;
                    else
                        objtblApplicant.Family_size = (int)dr["family_size"];
                    if (dr["nationality"].Equals(DBNull.Value))
                        objtblApplicant.Nationality = 0;
                    else
                        objtblApplicant.Nationality = (int)dr["nationality"];
                    if (dr["town"].Equals(DBNull.Value))
                        objtblApplicant.Town = 0;
                    else
                        objtblApplicant.Town = (int)dr["town"];
                    if (dr["region"].Equals(DBNull.Value))
                        objtblApplicant.Region = 0;
                    else
                        objtblApplicant.Region = (int)dr["region"];
                    if (dr["p_o_box"].Equals(DBNull.Value))
                        objtblApplicant.P_o_box = string.Empty;
                    else
                        objtblApplicant.P_o_box = (string)dr["p_o_box"];
                    if (dr["person_address"].Equals(DBNull.Value))
                        objtblApplicant.Person_address = string.Empty;
                    else
                        objtblApplicant.Person_address = (string)dr["person_address"];
                    if (dr["tel_home"].Equals(DBNull.Value))
                        objtblApplicant.Tel_home = string.Empty;
                    else
                        objtblApplicant.Tel_home = (string)dr["tel_home"];
                    if (dr["tel_off_direct1"].Equals(DBNull.Value))
                        objtblApplicant.Tel_off_direct1 = string.Empty;
                    else
                        objtblApplicant.Tel_off_direct1 = (string)dr["tel_off_direct1"];
                    if (dr["tel_off_direct2"].Equals(DBNull.Value))
                        objtblApplicant.Tel_off_direct2 = string.Empty;
                    else
                        objtblApplicant.Tel_off_direct2 = (string)dr["tel_off_direct2"];
                    if (dr["tel_off_ext1"].Equals(DBNull.Value))
                        objtblApplicant.Tel_off_ext1 = string.Empty;
                    else
                        objtblApplicant.Tel_off_ext1 = (string)dr["tel_off_ext1"];
                    if (dr["room_no"].Equals(DBNull.Value))
                        objtblApplicant.Room_no = string.Empty;
                    else
                        objtblApplicant.Room_no = (string)dr["room_no"];
                    if (dr["passport_no"].Equals(DBNull.Value))
                        objtblApplicant.Passport_no = string.Empty;
                    else
                        objtblApplicant.Passport_no = (string)dr["passport_no"];
                    if (dr["license_grade"].Equals(DBNull.Value))
                        objtblApplicant.License_grade = 0;
                    else
                        objtblApplicant.License_grade = (int)dr["license_grade"];
                    if (dr["education_group"].Equals(DBNull.Value))
                        objtblApplicant.Education_group = 0;
                    else
                        objtblApplicant.Education_group = (int)dr["education_group"];
                    if (dr["education_level"].Equals(DBNull.Value))
                        objtblApplicant.Education_level = 0;
                    else
                        objtblApplicant.Education_level = (int)dr["education_level"];
                    if (dr["education_description"].Equals(DBNull.Value))
                        objtblApplicant.Education_description = string.Empty;
                    else
                        objtblApplicant.Education_description = (string)dr["education_description"];
                    if (dr["job_title"].Equals(DBNull.Value))
                        objtblApplicant.Job_title = string.Empty;
                    else
                        objtblApplicant.Job_title = (string)dr["job_title"];
                    if (dr["job_title_Sort"].Equals(DBNull.Value))
                        objtblApplicant.Job_title_Sort = string.Empty;
                    else
                        objtblApplicant.Job_title_Sort = (string)dr["job_title_Sort"];
                    if (dr["occupation"].Equals(DBNull.Value))
                        objtblApplicant.Occupation = 0;
                    else
                        objtblApplicant.Occupation = (int)dr["occupation"];
                    if (dr["occupation_step"].Equals(DBNull.Value))
                        objtblApplicant.Occupation_step = string.Empty;
                    else
                        objtblApplicant.Occupation_step = (string)dr["occupation_step"];
                    if (dr["position_code"].Equals(DBNull.Value))
                        objtblApplicant.Position_code = string.Empty;
                    else
                        objtblApplicant.Position_code = (string)dr["position_code"];
                    if (dr["emp_date"].Equals(DBNull.Value))
                        objtblApplicant.Emp_date = DateTime.MinValue;
                    else
                        objtblApplicant.Emp_date = (DateTime)dr["emp_date"];
                    if (dr["emp_letter"].Equals(DBNull.Value))
                        objtblApplicant.Emp_letter = string.Empty;
                    else
                        objtblApplicant.Emp_letter = (string)dr["emp_letter"];
                    if (dr["emp_status"].Equals(DBNull.Value))
                        objtblApplicant.Emp_status = 0;
                    else
                        objtblApplicant.Emp_status = (int)dr["emp_status"];
                    if (dr["current_status"].Equals(DBNull.Value))
                        objtblApplicant.Current_status = 0;
                    else
                        objtblApplicant.Current_status = (int)dr["current_status"];
                    if (dr["date_termination"].Equals(DBNull.Value))
                        objtblApplicant.Date_termination = DateTime.MinValue;
                    else
                        objtblApplicant.Date_termination = (DateTime)dr["date_termination"];
                    if (dr["termination_reason"].Equals(DBNull.Value))
                        objtblApplicant.Termination_reason = string.Empty;
                    else
                        objtblApplicant.Termination_reason = (string)dr["termination_reason"];
                    if (dr["remark"].Equals(DBNull.Value))
                        objtblApplicant.Remark = string.Empty;
                    else
                        objtblApplicant.Remark = (string)dr["remark"];
                    if (dr["mother_tongue"].Equals(DBNull.Value))
                        objtblApplicant.Mother_tongue = 0;
                    else
                        objtblApplicant.Mother_tongue = (int)dr["mother_tongue"];
                    if (dr["ethnicity"].Equals(DBNull.Value))
                        objtblApplicant.Ethnicity = 0;
                    else
                        objtblApplicant.Ethnicity = (int)dr["ethnicity"];
                    if (dr["location"].Equals(DBNull.Value))
                        objtblApplicant.Location = 0;
                    else
                        objtblApplicant.Location = (int)dr["location"];
                    if (dr["photo"].Equals(DBNull.Value))
                        objtblApplicant.Photo = null;
                    else
                        objtblApplicant.Photo = (byte[])dr["photo"];
                    if (dr["first_name_sort"].Equals(DBNull.Value))
                        objtblApplicant.First_name_sort = string.Empty;
                    else
                        objtblApplicant.First_name_sort = (string)dr["first_name_sort"];
                    if (dr["father_name_sort"].Equals(DBNull.Value))
                        objtblApplicant.Father_name_sort = string.Empty;
                    else
                        objtblApplicant.Father_name_sort = (string)dr["father_name_sort"];
                    if (dr["grand_name_sort"].Equals(DBNull.Value))
                        objtblApplicant.Grand_name_sort = string.Empty;
                    else
                        objtblApplicant.Grand_name_sort = (string)dr["grand_name_sort"];
                    if (dr["first_name_soundeX"].Equals(DBNull.Value))
                        objtblApplicant.First_name_soundeX = string.Empty;
                    else
                        objtblApplicant.First_name_soundeX = (string)dr["first_name_soundeX"];
                    if (dr["father_name_soundeX"].Equals(DBNull.Value))
                        objtblApplicant.Father_name_soundeX = string.Empty;
                    else
                        objtblApplicant.Father_name_soundeX = (string)dr["father_name_soundeX"];
                    if (dr["grand_name_soundeX"].Equals(DBNull.Value))
                        objtblApplicant.Grand_name_soundeX = string.Empty;
                    else
                        objtblApplicant.Grand_name_soundeX = (string)dr["grand_name_soundeX"];
                    if (dr["leave_balance"].Equals(DBNull.Value))
                        objtblApplicant.Leave_balance = 0;
                    else
                        objtblApplicant.Leave_balance = (int)dr["leave_balance"];
                    if (dr["pension_no"].Equals(DBNull.Value))
                        objtblApplicant.Pension_no = string.Empty;
                    else
                        objtblApplicant.Pension_no = (string)dr["pension_no"];
                    if (dr["is_teacher"].Equals(DBNull.Value))
                        objtblApplicant.Is_teacher = false;
                    else
                        objtblApplicant.Is_teacher = (bool)dr["is_teacher"];
                    if (dr["is_police"].Equals(DBNull.Value))
                        objtblApplicant.Is_police = 0;
                    else
                        objtblApplicant.Is_police = (int)dr["is_police"];
                    if (dr["working_days"].Equals(DBNull.Value))
                        objtblApplicant.Working_days = 0;
                    else
                        objtblApplicant.Working_days = (int)dr["working_days"];
                    if (dr["cost_sharing_total_amount"].Equals(DBNull.Value))
                        objtblApplicant.Cost_sharing_total_amount = 0;
                    else
                        objtblApplicant.Cost_sharing_total_amount = (double)dr["cost_sharing_total_amount"];
                    if (dr["cost_sharing_total_paid"].Equals(DBNull.Value))
                        objtblApplicant.Cost_sharing_total_paid = 0;
                    else
                        objtblApplicant.Cost_sharing_total_paid = (double)dr["cost_sharing_total_paid"];
                    if (dr["cost_sharing_monthly_payment"].Equals(DBNull.Value))
                        objtblApplicant.Cost_sharing_monthly_payment = 0;
                    else
                        objtblApplicant.Cost_sharing_monthly_payment = (double)dr["cost_sharing_monthly_payment"];
                    if (dr["Health_Status"].Equals(DBNull.Value))
                        objtblApplicant.Health_Status = 0;
                    else
                        objtblApplicant.Health_Status = (int)dr["Health_Status"];
                    if (dr["UserId"].Equals(DBNull.Value))
                        objtblApplicant.UserId = string.Empty;
                    else
                        objtblApplicant.UserId = (string)dr["UserId"];
                    if (dr["BankBranchGuid"].Equals(DBNull.Value))
                        objtblApplicant.BankBranchGuid = Guid.Empty;
                    else
                        objtblApplicant.BankBranchGuid = (Guid)dr["BankBranchGuid"];
                    if (dr["AccountNo"].Equals(DBNull.Value))
                        objtblApplicant.AccountNo = string.Empty;
                    else
                        objtblApplicant.AccountNo = (string)dr["AccountNo"];
                    if (dr["BankGuid"].Equals(DBNull.Value))
                        objtblApplicant.BankGuid = Guid.Empty;
                    else
                        objtblApplicant.BankGuid = (Guid)dr["BankGuid"];
                    if (dr["RankGuid"].Equals(DBNull.Value))
                        objtblApplicant.RankGuid = Guid.Empty;
                    else
                        objtblApplicant.RankGuid = (Guid)dr["RankGuid"];
                    if (dr["EnteranceCondition"].Equals(DBNull.Value))
                        objtblApplicant.EnteranceCondition = 0;
                    else
                        objtblApplicant.EnteranceCondition = (int)dr["EnteranceCondition"];
                    if (dr["IsTheoryTaken"].Equals(DBNull.Value))
                        objtblApplicant.IsTheoryTaken = false;
                    else
                        objtblApplicant.IsTheoryTaken = (bool)dr["IsTheoryTaken"];
                    if (dr["IsPracticalTaken"].Equals(DBNull.Value))
                        objtblApplicant.IsPracticalTaken = false;
                    else
                        objtblApplicant.IsPracticalTaken = (bool)dr["IsPracticalTaken"];
                    if (dr["IsInterviewTaken"].Equals(DBNull.Value))
                        objtblApplicant.IsInterviewTaken = false;
                    else
                        objtblApplicant.IsInterviewTaken = (bool)dr["IsInterviewTaken"];
                    if (dr["TheoryPercentage"].Equals(DBNull.Value))
                        objtblApplicant.TheoryPercentage = 0;
                    else
                        objtblApplicant.TheoryPercentage = (int)dr["TheoryPercentage"];
                    if (dr["PracticalPercentage"].Equals(DBNull.Value))
                        objtblApplicant.PracticalPercentage = 0;
                    else
                        objtblApplicant.PracticalPercentage = (int)dr["PracticalPercentage"];
                    if (dr["InterviewPercentage"].Equals(DBNull.Value))
                        objtblApplicant.InterviewPercentage = 0;
                    else
                        objtblApplicant.InterviewPercentage = (int)dr["InterviewPercentage"];
                    if (dr["TheoryExamResult"].Equals(DBNull.Value))
                        objtblApplicant.TheoryExamResult = 0;
                    else
                        objtblApplicant.TheoryExamResult = (double)dr["TheoryExamResult"];
                    if (dr["PracticalExamResult"].Equals(DBNull.Value))
                        objtblApplicant.PracticalExamResult = 0;
                    else
                        objtblApplicant.PracticalExamResult = (double)dr["PracticalExamResult"];
                    if (dr["InterviewExamResult"].Equals(DBNull.Value))
                        objtblApplicant.InterviewExamResult = 0;
                    else
                        objtblApplicant.InterviewExamResult = (double)dr["InterviewExamResult"];
                    if (dr["Result"].Equals(DBNull.Value))
                        objtblApplicant.Result = 0;
                    else
                        objtblApplicant.Result = (double)dr["Result"];
                    if (dr["Is_Hired"].Equals(DBNull.Value))
                        objtblApplicant.Is_Hired = false;
                    else
                        objtblApplicant.Is_Hired = (bool)dr["Is_Hired"];
                    if (dr["Selection_Criteria"].Equals(DBNull.Value))
                        objtblApplicant.Selection_Criteria = 0;
                    else
                        objtblApplicant.Selection_Criteria = (int)dr["Selection_Criteria"];
                    if (dr["Zone"].Equals(DBNull.Value))
                        objtblApplicant.Zone = string.Empty;
                    else
                        objtblApplicant.Zone = (string)dr["Zone"];
                    if (dr["Worede"].Equals(DBNull.Value))
                        objtblApplicant.Worede = string.Empty;
                    else
                        objtblApplicant.Worede = (string)dr["Worede"];
                    if (dr["Kebele"].Equals(DBNull.Value))
                        objtblApplicant.Kebele = string.Empty;
                    else
                        objtblApplicant.Kebele = (string)dr["Kebele"];
                    if (dr["Waiting_Rank"].Equals(DBNull.Value))
                        objtblApplicant.Waiting_Rank = 0;
                    else
                        objtblApplicant.Waiting_Rank = (int)dr["Waiting_Rank"];
                    RecordsList.Add(objtblApplicant);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblApplicant::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT MainGuid FROM [dbo].[tblApplicant] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("tblApplicant::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}