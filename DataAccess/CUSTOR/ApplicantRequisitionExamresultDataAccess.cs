﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class ApplicantRequisitionExamresultDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords(Guid person)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [Full Name],[Employment_Requisition_Guid],[TheoryExamResult],[PracticalExamResult],[InterviewExamResult],[Result],[Applicant_Guid],[remark] FROM [dbo].[ApplicantRequisitionExamresult] where [Applicant_Guid]=@ApplicantGuid order by [Result] Desc ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("ApplicantRequisitionExamresult");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ApplicantGuid", person));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("ApplicantRequisitionExamresult::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }
        public DataTable GetRecordRequisition(Guid Requisition)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [Full Name],[Employment_Requisition_Guid],[TheoryExamResult],[PracticalExamResult],[InterviewExamResult],[Result],[Applicant_Guid],[remark],(SELECT sum(datediff(YEAR,date_from, date_to)) as totalyear FROM   tblApplicantEmploymentHistory where  ParentGuid=[dbo].[ApplicantRequisitionExamresult].Applicant_Guid) as totalyear,(SELECT    (select amdescription from tblLookup where tblLookup.code=max(EducationLevel)) as  EducationLevel,[dbo].[fnGetFiledofstudy] (Applicant_Guid) as Filedofstudy
            FROM         tblApplicantEmploymentHistory
            WHERE     (ParentGuid =[dbo].[ApplicantRequisitionExamresult].Applicant_Guid)) as  EducationLevel FROM [dbo].[ApplicantRequisitionExamresult] where [Employment_Requisition_Guid]=@Requisition order by [Result] DESC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("ApplicantRequisitionExamresult");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@Requisition", Requisition));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("ApplicantRequisitionExamresult::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }


        public ApplicantRequisitionExamresult GetRecord(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            ApplicantRequisitionExamresult objApplicantRequisitionExamresult = new ApplicantRequisitionExamresult();
            string strGetRecord = @"SELECT [Full Name],[Employment_Requisition_Guid],[TheoryExamResult],[PracticalExamResult],[InterviewExamResult],[Result],[ApplicantExamGuid],[ApplicantGuid] FROM [dbo].[ApplicantRequisitionExamresult] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("ApplicantRequisitionExamresult");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    if (dTable.Rows[0]["Full Name"].Equals(DBNull.Value))
                        objApplicantRequisitionExamresult.Full_Name = string.Empty;
                    else
                        objApplicantRequisitionExamresult.Full_Name = (string)dTable.Rows[0]["Full Name"];
                    objApplicantRequisitionExamresult.Employment_Requisition_Guid = (Guid)dTable.Rows[0]["Employment_Requisition_Guid"];
                    if (dTable.Rows[0]["TheoryExamResult"].Equals(DBNull.Value))
                        objApplicantRequisitionExamresult.TheoryExamResult = 0;
                    else
                        objApplicantRequisitionExamresult.TheoryExamResult = (double)dTable.Rows[0]["TheoryExamResult"];
                    if (dTable.Rows[0]["PracticalExamResult"].Equals(DBNull.Value))
                        objApplicantRequisitionExamresult.PracticalExamResult = 0;
                    else
                        objApplicantRequisitionExamresult.PracticalExamResult = (double)dTable.Rows[0]["PracticalExamResult"];
                    if (dTable.Rows[0]["InterviewExamResult"].Equals(DBNull.Value))
                        objApplicantRequisitionExamresult.InterviewExamResult = 0;
                    else
                        objApplicantRequisitionExamresult.InterviewExamResult = (double)dTable.Rows[0]["InterviewExamResult"];
                    if (dTable.Rows[0]["Result"].Equals(DBNull.Value))
                        objApplicantRequisitionExamresult.Result = 0;
                    else
                        objApplicantRequisitionExamresult.Result = (double)dTable.Rows[0]["Result"];
                    objApplicantRequisitionExamresult.ApplicantExamGuid = (Guid)dTable.Rows[0]["ApplicantExamGuid"];
                    if (dTable.Rows[0]["ApplicantGuid"].Equals(DBNull.Value))
                        objApplicantRequisitionExamresult.ApplicantGuid = Guid.Empty;
                    else
                        objApplicantRequisitionExamresult.ApplicantGuid = (Guid)dTable.Rows[0]["ApplicantGuid"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("ApplicantRequisitionExamresult::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objApplicantRequisitionExamresult;
        }

        

        public List<ApplicantRequisitionExamresult> GetList()
        {
            List<ApplicantRequisitionExamresult> RecordsList = new List<ApplicantRequisitionExamresult>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [Full Name],[Employment_Requisition_Guid],[TheoryExamResult],[PracticalExamResult],[InterviewExamResult],[Result],[ApplicantExamGuid],[ApplicantGuid] FROM [dbo].[ApplicantRequisitionExamresult] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    ApplicantRequisitionExamresult objApplicantRequisitionExamresult = new ApplicantRequisitionExamresult();
                    if (dr["Full Name"].Equals(DBNull.Value))
                        objApplicantRequisitionExamresult.Full_Name = string.Empty;
                    else
                        objApplicantRequisitionExamresult.Full_Name = (string)dr["Full Name"];
                    if (dr["Employment_Requisition_Guid"].Equals(DBNull.Value))
                        objApplicantRequisitionExamresult.Employment_Requisition_Guid = Guid.Empty;
                    else
                        objApplicantRequisitionExamresult.Employment_Requisition_Guid = (Guid)dr["Employment_Requisition_Guid"];
                    if (dr["TheoryExamResult"].Equals(DBNull.Value))
                        objApplicantRequisitionExamresult.TheoryExamResult = 0;
                    else
                        objApplicantRequisitionExamresult.TheoryExamResult = (double)dr["TheoryExamResult"];
                    if (dr["PracticalExamResult"].Equals(DBNull.Value))
                        objApplicantRequisitionExamresult.PracticalExamResult = 0;
                    else
                        objApplicantRequisitionExamresult.PracticalExamResult = (double)dr["PracticalExamResult"];
                    if (dr["InterviewExamResult"].Equals(DBNull.Value))
                        objApplicantRequisitionExamresult.InterviewExamResult = 0;
                    else
                        objApplicantRequisitionExamresult.InterviewExamResult = (double)dr["InterviewExamResult"];
                    if (dr["Result"].Equals(DBNull.Value))
                        objApplicantRequisitionExamresult.Result = 0;
                    else
                        objApplicantRequisitionExamresult.Result = (double)dr["Result"];
                    if (dr["ApplicantExamGuid"].Equals(DBNull.Value))
                        objApplicantRequisitionExamresult.ApplicantExamGuid = Guid.Empty;
                    else
                        objApplicantRequisitionExamresult.ApplicantExamGuid = (Guid)dr["ApplicantExamGuid"];
                    if (dr["ApplicantGuid"].Equals(DBNull.Value))
                        objApplicantRequisitionExamresult.ApplicantGuid = Guid.Empty;
                    else
                        objApplicantRequisitionExamresult.ApplicantGuid = (Guid)dr["ApplicantGuid"];
                    RecordsList.Add(objApplicantRequisitionExamresult);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("ApplicantRequisitionExamresult::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT  FROM [dbo].[ApplicantRequisitionExamresult] ";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("ApplicantRequisitionExamresult::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}