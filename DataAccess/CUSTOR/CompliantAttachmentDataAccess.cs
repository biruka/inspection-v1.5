﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;

using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class CompliantAttachmentDataAccess
    {
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public DataTable GetRecordreference(Guid person, string FileType)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [ComplaintGUID],[Referenceno] FROM [dbo].[tblComplaint] Where OrgGuid=@ParentGuid and ScreeningDecision=@FileType  ORDER BY  [ComplaintGUID] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Complaint");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ParentGuid", person));
                command.Parameters.Add(new SqlParameter("@FileType", FileType));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Complaint::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }
        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [AttachmentGuid],[CompliantGuid],[FileNumber],[AttachmentTitle],[DocumentTitle],[FileType],[FileSize],[FileUrl] FROM [dbo].[tblCompliantAttachment] ORDER BY  [AttachmentGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Attachment");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Attachment::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }
        public bool DeleteAttached(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblCompliantAttachment]WHERE [CompliantGuid]=@AttachmentGuid";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@AttachmentGuid", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Attachment::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
        public CompliantAttachment GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            CompliantAttachment objAttachment = new CompliantAttachment();
            string strGetRecord = @"SELECT [AttachmentGuid],[CompliantGuid],[FileNumber],[AttachmentTitle],[DocumentTitle],[FileType],[FileSize],[FileUrl] FROM [dbo].[tblCompliantAttachment]WHERE [AttachmentGuid]=@AttachmentGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Attachment");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@AttachmentGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objAttachment.AttachmentGuid = (Guid)dTable.Rows[0]["AttachmentGuid"];
                    objAttachment.CompliantGuid = (Guid)dTable.Rows[0]["CompliantGuid"];
                    if (dTable.Rows[0]["FileNumber"].Equals(DBNull.Value))
                        objAttachment.FileNumber = string.Empty;
                    else
                        objAttachment.FileNumber = (string)dTable.Rows[0]["FileNumber"];
                    if (dTable.Rows[0]["AttachmentTitle"].Equals(DBNull.Value))
                        objAttachment.AttachmentTitle = string.Empty;
                    else
                        objAttachment.AttachmentTitle = (string)dTable.Rows[0]["AttachmentTitle"];
                    if (dTable.Rows[0]["DocumentTitle"].Equals(DBNull.Value))
                        objAttachment.DocumentTitle = string.Empty;
                    else
                        objAttachment.DocumentTitle = (string)dTable.Rows[0]["DocumentTitle"];
                    if (dTable.Rows[0]["FileType"].Equals(DBNull.Value))
                        objAttachment.FileType = string.Empty;
                    else
                        objAttachment.FileType = (string)dTable.Rows[0]["FileType"];
                    if (dTable.Rows[0]["FileSize"].Equals(DBNull.Value))
                        objAttachment.FileSize = 0;
                    else
                        objAttachment.FileSize = (double)dTable.Rows[0]["FileSize"];
                    if (dTable.Rows[0]["FileUrl"].Equals(DBNull.Value))
                        objAttachment.FileUrl = string.Empty;
                    else
                        objAttachment.FileUrl = (string)dTable.Rows[0]["FileUrl"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Attachment::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objAttachment;
        }

        public DataTable GetRecordByCompliantGuid(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            string strGetRecord = @"SELECT [AttachmentGuid],[CompliantGuid],[FileNumber],[AttachmentTitle],[DocumentTitle],[FileType],[FileSize],[FileUrl],[SentDate] FROM [dbo].[tblCompliantAttachment]WHERE [CompliantGuid]=@CompliantGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Attachment");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@CompliantGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Attachment::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public bool Insert(CompliantAttachment objAttachment)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[tblCompliantAttachment]
                                            ([AttachmentGuid],[CompliantGuid],[FileNumber],[AttachmentTitle],[DocumentTitle],[FileType],[FileSize],[FileUrl])
                                     VALUES    (@AttachmentGuid,@CompliantGuid,@FileNumber,@AttachmentTitle,@DocumentTitle,@FileType,@FileSize,@FileUrl)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@AttachmentGuid", objAttachment.AttachmentGuid));
                command.Parameters.Add(new SqlParameter("@CompliantGuid", objAttachment.CompliantGuid));
                command.Parameters.Add(new SqlParameter("@FileNumber", objAttachment.FileNumber));
                command.Parameters.Add(new SqlParameter("@AttachmentTitle", objAttachment.AttachmentTitle));
                command.Parameters.Add(new SqlParameter("@DocumentTitle", objAttachment.DocumentTitle));
                command.Parameters.Add(new SqlParameter("@FileType", objAttachment.FileType));
                command.Parameters.Add(new SqlParameter("@FileSize", objAttachment.FileSize));
                command.Parameters.Add(new SqlParameter("@FileUrl", objAttachment.FileUrl));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Attachment::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(CompliantAttachment objAttachment)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[tblCompliantAttachment]SET     [AttachmentGuid]=@AttachmentGuid,    [CompliantGuid]=@CompliantGuid,[FileNumber]=@FileNumber    [AttachmentTitle]=@AttachmentTitle,    [DocumentTitle]=@DocumentTitle,    [FileType]=@FileType,    [FileSize]=@FileSize,    [FileUrl]=@FileUrl WHERE [AttachmentGuid]=@AttachmentGuid";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@AttachmentGuid", objAttachment.AttachmentGuid));
                command.Parameters.Add(new SqlParameter("@CompliantGuid", objAttachment.CompliantGuid));
                command.Parameters.Add(new SqlParameter("@FileNumber", objAttachment.AttachmentTitle));
                command.Parameters.Add(new SqlParameter("@AttachmentTitle", objAttachment.AttachmentTitle));
                command.Parameters.Add(new SqlParameter("@DocumentTitle", objAttachment.DocumentTitle));
                command.Parameters.Add(new SqlParameter("@FileType", objAttachment.FileType));
                command.Parameters.Add(new SqlParameter("@FileSize", objAttachment.FileSize));
                command.Parameters.Add(new SqlParameter("@FileUrl", objAttachment.FileUrl));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Attachment::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public List<CompliantAttachment> GetList(string compliantGuid)
        {
            List<CompliantAttachment> RecordsList = new List<CompliantAttachment>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [AttachmentGuid],[CompliantGuid],[FileNumber],[AttachmentTitle],[DocumentTitle],[FileType],[FileSize],[FileUrl] FROM [dbo].[tblCompliantAttachment] 
                                        where CompliantGuid=@CompliantGuid ORDER BY  [AttachmentGuid] ASC";
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                connection.Open();
                command.Parameters.Add(new SqlParameter("@CompliantGuid", compliantGuid));
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    CompliantAttachment objAttachment = new CompliantAttachment();
                    if (dr["AttachmentGuid"].Equals(DBNull.Value))
                        objAttachment.AttachmentGuid = Guid.Empty;
                    else
                        objAttachment.AttachmentGuid = (Guid)dr["AttachmentGuid"];
                    if (dr["FileNumber"].Equals(DBNull.Value))
                        objAttachment.FileNumber = string.Empty;
                    else
                        objAttachment.FileNumber = (string)dr["FileNumber"];
                    if (dr["CompliantGuid"].Equals(DBNull.Value))
                        objAttachment.CompliantGuid = Guid.Empty;
                    else
                        objAttachment.CompliantGuid = (Guid)dr["CompliantGuid"];
                    if (dr["AttachmentTitle"].Equals(DBNull.Value))
                        objAttachment.AttachmentTitle = string.Empty;
                    else
                        objAttachment.AttachmentTitle = (string)dr["AttachmentTitle"];
                    if (dr["DocumentTitle"].Equals(DBNull.Value))
                        objAttachment.DocumentTitle = string.Empty;
                    else
                        objAttachment.DocumentTitle = (string)dr["DocumentTitle"];
                    if (dr["FileType"].Equals(DBNull.Value))
                        objAttachment.FileType = string.Empty;
                    else
                        objAttachment.FileType = (string)dr["FileType"];
                    if (dr["FileSize"].Equals(DBNull.Value))
                        objAttachment.FileSize = 0;
                    else
                        objAttachment.FileSize = (double)dr["FileSize"];
                    if (dr["FileUrl"].Equals(DBNull.Value))
                        objAttachment.FileUrl = string.Empty;
                    else
                        objAttachment.FileUrl = (string)dr["FileUrl"];
                    RecordsList.Add(objAttachment);
                }

                for (int index = 0; index < RecordsList.Count; index++)
                {
                    RecordsList[index].FileSizeCaption = FormatSize(RecordsList[index].FileSize);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Attachment::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        static readonly string[] suffixes = { "Bytes", "KB", "MB", "GB", "TB", "PB" };
        public static string FormatSize(double bytes)
        {
            int counter = 0;
            decimal number = (decimal)bytes;
            while (Math.Round(number / 1024) >= 1)
            {
                number = number / 1024;
                counter++;
            }
            return string.Format("{0:n1}{1}", number, suffixes[counter]);
        }

        public bool Delete(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblCompliantAttachment]WHERE [AttachmentGuid]=@AttachmentGuid";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@AttachmentGuid", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Attachment::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public List<CompliantAttachment> GetList()
        {
            List<CompliantAttachment> RecordsList = new List<CompliantAttachment>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [AttachmentGuid],[CompliantGuid],[FileNumber],[AttachmentTitle],[DocumentTitle],[FileType],[FileSize],[FileUrl] FROM [dbo].[tblCompliantAttachment] ORDER BY  [AttachmentGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    CompliantAttachment objAttachment = new CompliantAttachment();
                    if (dr["AttachmentGuid"].Equals(DBNull.Value))
                        objAttachment.AttachmentGuid = Guid.Empty;
                    else
                        objAttachment.AttachmentGuid = (Guid)dr["AttachmentGuid"];
                    if (dr["FileNumber"].Equals(DBNull.Value))
                        objAttachment.FileNumber = string.Empty;
                    else
                        objAttachment.FileNumber = (string)dr["FileNumber"];
                    if (dr["CompliantGuid"].Equals(DBNull.Value))
                        objAttachment.CompliantGuid = Guid.Empty;
                    else
                        objAttachment.CompliantGuid = (Guid)dr["CompliantGuid"];
                    if (dr["AttachmentTitle"].Equals(DBNull.Value))
                        objAttachment.AttachmentTitle = string.Empty;
                    else
                        objAttachment.AttachmentTitle = (string)dr["AttachmentTitle"];
                    if (dr["DocumentTitle"].Equals(DBNull.Value))
                        objAttachment.DocumentTitle = string.Empty;
                    else
                        objAttachment.DocumentTitle = (string)dr["DocumentTitle"];
                    if (dr["FileType"].Equals(DBNull.Value))
                        objAttachment.FileType = string.Empty;
                    else
                        objAttachment.FileType = (string)dr["FileType"];
                    if (dr["FileSize"].Equals(DBNull.Value))
                        objAttachment.FileSize = 0;
                    else
                        objAttachment.FileSize = (double)dr["FileSize"];
                    if (dr["FileUrl"].Equals(DBNull.Value))
                        objAttachment.FileUrl = string.Empty;
                    else
                        objAttachment.FileUrl = (string)dr["FileUrl"];
                    RecordsList.Add(objAttachment);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Attachment::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public bool Exists(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT AttachmentGuid FROM [dbo].[tblCompliantAttachment]WHERE [AttachmentGuid]=@AttachmentGuid";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@AttachmentGuid", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("Attachment::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
    }
}