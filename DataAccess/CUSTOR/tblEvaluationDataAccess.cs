﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblEvaluationDataAccess
    {
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[fiscal_year],[eval_year],[period],[EvaluationLevel],[eval_point],[strong_side],[weak_side],[remark],[Is_Rejected],[Rejected_by],[Rejected_Date],[Appear_Remark],[Is_Final_Decision],[Final_Decision_Given_By],[Final_Decision],[Final_Decision_Date] FROM [dbo].[tblEvaluation] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblEvaluation");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblEvaluation::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public tblEvaluation GetRecord(string ID,Guid Person)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
            tblEvaluation objtblEvaluation = new tblEvaluation();
            string strGetAllRecords = "[dbo].[GetEvaluationDisplayValue]";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.StoredProcedure };
            command.Connection = connection;
           

            try
            {
                command.Parameters.Add(new SqlParameter("@promotionperiod", ID));
                command.Parameters.Add(new SqlParameter("@Person", Person));
                connection.Open();
                dr = command.ExecuteReader();
               
               //if (dr["MainGuid"].Equals(DBNull.Value))
               //         objtblEvaluation.MainGuid = Guid.Empty;
               //     else
               //         objtblEvaluation.MainGuid = (Guid)dr["MainGuid"];
                    //if (dr["seq_no"].Equals(DBNull.Value))
                    //    objtblEvaluation.Seq_no = 0;
                    //else
                    //    objtblEvaluation.Seq_no = (int)dr["seq_no"];
                    //if (dr["ParentGuid"].Equals(DBNull.Value))
                    //    objtblEvaluation.ParentGuid = Guid.Empty;
                    //else
                        //objtblEvaluation.ParentGuid = (Guid)dr["ParentGuid"];
                    if (dr["fiscal_year"].Equals(DBNull.Value))
                        objtblEvaluation.Fiscal_year = DateTime.MinValue;
                    else
                        objtblEvaluation.Fiscal_year = (DateTime)dr["fiscal_year"];
                    //if (dr["eval_year"].Equals(DBNull.Value))
                    //    objtblEvaluation.Eval_year = 0;
                    //else
                    //    objtblEvaluation.Eval_year = (int)dr["eval_year"];
                    if (dr["period"].Equals(DBNull.Value))
                        objtblEvaluation.Period = 0;
                    else
                        objtblEvaluation.Period = (int)dr["period"];
                    //if (dr["EvaluationLevel"].Equals(DBNull.Value))
                    //    objtblEvaluation.EvaluationLevel = 0;
                    //else
                    //    objtblEvaluation.EvaluationLevel = (int)dr["EvaluationLevel"];
                    if (dr["eval_point"].Equals(DBNull.Value))
                        objtblEvaluation.Eval_point = 0;
                    else
                        objtblEvaluation.Eval_point = (float)dr["eval_point"];
                    if (dr["strong_side"].Equals(DBNull.Value))
                        objtblEvaluation.Strong_side = string.Empty;
                    else
                        objtblEvaluation.Strong_side = (string)dr["strong_side"];
                    if (dr["weak_side"].Equals(DBNull.Value))
                        objtblEvaluation.Weak_side = string.Empty;
                    else
                        objtblEvaluation.Weak_side = (string)dr["weak_side"];
                    //if (dr["remark"].Equals(DBNull.Value))
                    //    objtblEvaluation.Remark = string.Empty;
                    //else
                    //    objtblEvaluation.Remark = (string)dr["remark"];
                    //if (dr["Is_Rejected"].Equals(DBNull.Value))
                    //    objtblEvaluation.Is_Rejected = string.Empty;
                    //else
                    //    objtblEvaluation.Is_Rejected = (string)dr["Is_Rejected"];
                    //if (dr["Rejected_by"].Equals(DBNull.Value))
                    //    objtblEvaluation.Rejected_by = string.Empty;
                    //else
                    //    objtblEvaluation.Rejected_by = (string)dr["Rejected_by"];
                    //if (dr["Rejected_Date"].Equals(DBNull.Value))
                    //    objtblEvaluation.Rejected_Date = DateTime.MinValue;
                    //else
                    //    objtblEvaluation.Rejected_Date = (DateTime)dr["Rejected_Date"];
                    //if (dr["Appear_Remark"].Equals(DBNull.Value))
                    //    objtblEvaluation.Appear_Remark = string.Empty;
                    //else
                    //    objtblEvaluation.Appear_Remark = (string)dr["Appear_Remark"];
                    //if (dr["Is_Final_Decision"].Equals(DBNull.Value))
                    //    objtblEvaluation.Is_Final_Decision = string.Empty;
                    //else
                    //    objtblEvaluation.Is_Final_Decision = (string)dr["Is_Final_Decision"];
                    //if (dr["Final_Decision_Given_By"].Equals(DBNull.Value))
                    //    objtblEvaluation.Final_Decision_Given_By = string.Empty;
                    //else
                    //    objtblEvaluation.Final_Decision_Given_By = (string)dr["Final_Decision_Given_By"];
                    //if (dr["Final_Decision"].Equals(DBNull.Value))
                    //    objtblEvaluation.Final_Decision = string.Empty;
                    //else
                    //    objtblEvaluation.Final_Decision = (string)dr["Final_Decision"];
                    //if (dr["Final_Decision_Date"].Equals(DBNull.Value))
                    //    objtblEvaluation.Final_Decision_Date = DateTime.MinValue;
                    //else
                    //    objtblEvaluation.Final_Decision_Date = (DateTime)dr["Final_Decision_Date"];
                }

            
            catch (Exception ex)
            {
                throw new Exception("tblEvaluation::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
               
            }
            return objtblEvaluation;
        }
             
        public List<tblEvaluation> GetEvaluation(DateTime ID, Guid Person)
        {
            List<tblEvaluation> RecordsList = new List<tblEvaluation>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = "[dbo].[GetEvaluationDisplayValue]";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.StoredProcedure };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@promotionperiod", ID));
                command.Parameters.Add(new SqlParameter("@Person", Person));
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblEvaluation objtblEvaluation = new tblEvaluation();

                    if (dr["fiscal_year"].Equals(DBNull.Value))
                        objtblEvaluation.strFiscal_year = string.Empty;
                    else
                        objtblEvaluation.strFiscal_year = (string)dr["fiscal_year"];
                    //if (dr["eval_year"].Equals(DBNull.Value))
                    //    objtblEvaluation.Eval_year = 0;
                    //else
                    //    objtblEvaluation.Eval_year = (int)dr["eval_year"];
                    if (dr["period"].Equals(DBNull.Value))
                        objtblEvaluation.Periods = string.Empty;
                    else
                        objtblEvaluation.Periods = (string)dr["period"];
                    //if (dr["EvaluationLevel"].Equals(DBNull.Value))
                    //    objtblEvaluation.EvaluationLevel = 0;
                    //else
                    //    objtblEvaluation.EvaluationLevel = (int)dr["EvaluationLevel"];

                    //if (dr["eval_point"].Equals(DBNull.Value))
                    //    objtblEvaluation.strEval_point = string.Empty;
                    //else
                    //    objtblEvaluation.strEval_point = (string)dr["eval_point"];

                    //if (dr["eval_point"].Equals(DBNull.Value))
                    //    objtblEvaluation.Eval_point = 0;
                    //else
                    //    objtblEvaluation.Eval_pointD = (decimal)dr["eval_point"];

                    if (dr["eval_point"].Equals(DBNull.Value))
                        objtblEvaluation.Eval_point = 0;
                    else
                        objtblEvaluation.Eval_point = float.Parse(dr["eval_point"].ToString());

                    if (dr["strong_side"].Equals(DBNull.Value))
                        objtblEvaluation.Strong_side = string.Empty;
                    else
                        objtblEvaluation.Strong_side = (string)dr["strong_side"];
                    if (dr["weak_side"].Equals(DBNull.Value))
                        objtblEvaluation.Weak_side = string.Empty;
                    else
                        objtblEvaluation.Weak_side = (string)dr["weak_side"];
                    //if (dr["remark"].Equals(DBNull.Value))
                    //    objtblEvaluation.Remark = string.Empty;
                    //else
                    //    objtblEvaluation.Remark = (string)dr["remark"];
                    //if (dr["Is_Rejected"].Equals(DBNull.Value))
                    //    objtblEvaluation.Is_Rejected = string.Empty;
                    //else
                    //    objtblEvaluation.Is_Rejected = (string)dr["Is_Rejected"];
                    //if (dr["Rejected_by"].Equals(DBNull.Value))
                    //    objtblEvaluation.Rejected_by = string.Empty;
                    //else
                    //    objtblEvaluation.Rejected_by = (string)dr["Rejected_by"];
                    //if (dr["Rejected_Date"].Equals(DBNull.Value))
                    //    objtblEvaluation.Rejected_Date = DateTime.MinValue;
                    //else
                    //    objtblEvaluation.Rejected_Date = (DateTime)dr["Rejected_Date"];
                    //if (dr["Appear_Remark"].Equals(DBNull.Value))
                    //    objtblEvaluation.Appear_Remark = string.Empty;
                    //else
                    //    objtblEvaluation.Appear_Remark = (string)dr["Appear_Remark"];
                    //if (dr["Is_Final_Decision"].Equals(DBNull.Value))
                    //    objtblEvaluation.Is_Final_Decision = string.Empty;
                    //else
                    //    objtblEvaluation.Is_Final_Decision = (string)dr["Is_Final_Decision"];
                    //if (dr["Final_Decision_Given_By"].Equals(DBNull.Value))
                    //    objtblEvaluation.Final_Decision_Given_By = string.Empty;
                    //else
                    //    objtblEvaluation.Final_Decision_Given_By = (string)dr["Final_Decision_Given_By"];
                    //if (dr["Final_Decision"].Equals(DBNull.Value))
                    //    objtblEvaluation.Final_Decision = string.Empty;
                    //else
                    //    objtblEvaluation.Final_Decision = (string)dr["Final_Decision"];
                    //if (dr["Final_Decision_Date"].Equals(DBNull.Value))
                    //    objtblEvaluation.Final_Decision_Date = DateTime.MinValue;
                    //else
                    //    objtblEvaluation.Final_Decision_Date = (DateTime)dr["Final_Decision_Date"];
                    RecordsList.Add(objtblEvaluation);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblEvaluation::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public List<tblEvaluation> GetList()
        {
            List<tblEvaluation> RecordsList = new List<tblEvaluation>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[fiscal_year],[eval_year],[period],[EvaluationLevel],[eval_point],[strong_side],[weak_side],[remark],[Is_Rejected],[Rejected_by],[Rejected_Date],[Appear_Remark],[Is_Final_Decision],[Final_Decision_Given_By],[Final_Decision],[Final_Decision_Date] FROM [dbo].[tblEvaluation] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblEvaluation objtblEvaluation = new tblEvaluation();
                    if (dr["MainGuid"].Equals(DBNull.Value))
                        objtblEvaluation.MainGuid = Guid.Empty;
                    else
                        objtblEvaluation.MainGuid = (Guid)dr["MainGuid"];
                    if (dr["seq_no"].Equals(DBNull.Value))
                        objtblEvaluation.Seq_no = 0;
                    else
                        objtblEvaluation.Seq_no = (int)dr["seq_no"];
                    if (dr["ParentGuid"].Equals(DBNull.Value))
                        objtblEvaluation.ParentGuid = Guid.Empty;
                    else
                        objtblEvaluation.ParentGuid = (Guid)dr["ParentGuid"];
                    if (dr["fiscal_year"].Equals(DBNull.Value))
                        objtblEvaluation.Fiscal_year = DateTime.MinValue;
                    else
                        objtblEvaluation.Fiscal_year = (DateTime)dr["fiscal_year"];
                    if (dr["eval_year"].Equals(DBNull.Value))
                        objtblEvaluation.Eval_year = 0;
                    else
                        objtblEvaluation.Eval_year = (int)dr["eval_year"];
                    if (dr["period"].Equals(DBNull.Value))
                        objtblEvaluation.Period = 0;
                    else
                        objtblEvaluation.Period = (int)dr["period"];
                    if (dr["EvaluationLevel"].Equals(DBNull.Value))
                        objtblEvaluation.EvaluationLevel = 0;
                    else
                        objtblEvaluation.EvaluationLevel = (int)dr["EvaluationLevel"];
                    if (dr["eval_point"].Equals(DBNull.Value))
                        objtblEvaluation.Eval_point = 0;
                    else
                        objtblEvaluation.Eval_point = (float)dr["eval_point"];
                    if (dr["strong_side"].Equals(DBNull.Value))
                        objtblEvaluation.Strong_side = string.Empty;
                    else
                        objtblEvaluation.Strong_side = (string)dr["strong_side"];
                    if (dr["weak_side"].Equals(DBNull.Value))
                        objtblEvaluation.Weak_side = string.Empty;
                    else
                        objtblEvaluation.Weak_side = (string)dr["weak_side"];
                    if (dr["remark"].Equals(DBNull.Value))
                        objtblEvaluation.Remark = string.Empty;
                    else
                        objtblEvaluation.Remark = (string)dr["remark"];
                    if (dr["Is_Rejected"].Equals(DBNull.Value))
                        objtblEvaluation.Is_Rejected = string.Empty;
                    else
                        objtblEvaluation.Is_Rejected = (string)dr["Is_Rejected"];
                    if (dr["Rejected_by"].Equals(DBNull.Value))
                        objtblEvaluation.Rejected_by = string.Empty;
                    else
                        objtblEvaluation.Rejected_by = (string)dr["Rejected_by"];
                    if (dr["Rejected_Date"].Equals(DBNull.Value))
                        objtblEvaluation.Rejected_Date = DateTime.MinValue;
                    else
                        objtblEvaluation.Rejected_Date = (DateTime)dr["Rejected_Date"];
                    if (dr["Appear_Remark"].Equals(DBNull.Value))
                        objtblEvaluation.Appear_Remark = string.Empty;
                    else
                        objtblEvaluation.Appear_Remark = (string)dr["Appear_Remark"];
                    if (dr["Is_Final_Decision"].Equals(DBNull.Value))
                        objtblEvaluation.Is_Final_Decision = string.Empty;
                    else
                        objtblEvaluation.Is_Final_Decision = (string)dr["Is_Final_Decision"];
                    if (dr["Final_Decision_Given_By"].Equals(DBNull.Value))
                        objtblEvaluation.Final_Decision_Given_By = string.Empty;
                    else
                        objtblEvaluation.Final_Decision_Given_By = (string)dr["Final_Decision_Given_By"];
                    if (dr["Final_Decision"].Equals(DBNull.Value))
                        objtblEvaluation.Final_Decision = string.Empty;
                    else
                        objtblEvaluation.Final_Decision = (string)dr["Final_Decision"];
                    if (dr["Final_Decision_Date"].Equals(DBNull.Value))
                        objtblEvaluation.Final_Decision_Date = DateTime.MinValue;
                    else
                        objtblEvaluation.Final_Decision_Date = (DateTime)dr["Final_Decision_Date"];
                    RecordsList.Add(objtblEvaluation);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblEvaluation::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }
    }
}