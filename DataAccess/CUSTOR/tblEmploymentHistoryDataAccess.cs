﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Commen;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblEmploymentHistoryDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords(Guid person,string language)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[institution],[location],[tax_payment_letter_no],[job_title],[date_from],[date_to],[salary],[date_termination],[termination_reason],[dbo].fnGetLookupNew([emp_status],2,"+ language + @") as [emp_status],[net_duration],[remark],[FieldofStudy],[EducationLevel],[dbo].[fnGetDateDifferentNew] (
                                     date_from  ,date_to  ,0,"+language+") as txtEmploymentDate FROM [dbo].[tblEmploymentHistory] where ParentGuid=@ParentGuid  ORDER BY  [date_to] DESC";
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblEmploymentHistory");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add("@ParentGuid", person);
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblEmploymentHistory::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public tblEmploymentHistory GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblEmploymentHistory objtblEmploymentHistory = new tblEmploymentHistory();
            string strGetRecord = @"SELECT [MainGuid],[seq_no],[ParentGuid],[institution],[location],[tax_payment_letter_no],[job_title],[date_from],[date_to],[salary],[date_termination],[termination_reason],[dbo].fnGetLookup([emp_status],2) as [emp_status],[net_duration],[remark] FROM [dbo].[tblEmploymentHistory] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblEmploymentHistory");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblEmploymentHistory.MainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    objtblEmploymentHistory.Seq_no = (int)dTable.Rows[0]["seq_no"];
                    if (dTable.Rows[0]["ParentGuid"].Equals(DBNull.Value))
                        objtblEmploymentHistory.ParentGuid = Guid.Empty;                    
                    else
                        objtblEmploymentHistory.ParentGuid = (Guid)dTable.Rows[0]["ParentGuid"];
                    if (dTable.Rows[0]["institution"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Institution = string.Empty;
                    else
                        objtblEmploymentHistory.Institution = (string)dTable.Rows[0]["institution"];
                    if (dTable.Rows[0]["location"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Location = string.Empty;
                    else
                        objtblEmploymentHistory.Location = (string)dTable.Rows[0]["location"];
                    if (dTable.Rows[0]["tax_payment_letter_no"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Tax_payment_letter_no = string.Empty;
                    else
                        objtblEmploymentHistory.Tax_payment_letter_no = (string)dTable.Rows[0]["tax_payment_letter_no"];
                    if (dTable.Rows[0]["job_title"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Job_title = string.Empty;
                    else
                        objtblEmploymentHistory.Job_title = (string)dTable.Rows[0]["job_title"];
                    if (dTable.Rows[0]["date_from"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Date_from = DateTime.MinValue;
                    else
                        objtblEmploymentHistory.Date_from = (DateTime)dTable.Rows[0]["date_from"];
                    if (dTable.Rows[0]["date_to"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Date_to = DateTime.MinValue;
                    else
                        objtblEmploymentHistory.Date_to = (DateTime)dTable.Rows[0]["date_to"];
                    if (dTable.Rows[0]["salary"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Salary = 0;
                    else
                        objtblEmploymentHistory.Salary = (double)dTable.Rows[0]["salary"];
                    if (dTable.Rows[0]["date_termination"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Date_termination = DateTime.MinValue;
                    else
                        objtblEmploymentHistory.Date_termination = (DateTime)dTable.Rows[0]["date_termination"];
                    if (dTable.Rows[0]["termination_reason"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Termination_reason = string.Empty;
                    else
                        objtblEmploymentHistory.Termination_reason = (string)dTable.Rows[0]["termination_reason"];
                    if (dTable.Rows[0]["emp_status"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Emp_status = 0;
                    else
                        objtblEmploymentHistory.Emp_status = (int)dTable.Rows[0]["emp_status"];
                    if (dTable.Rows[0]["net_duration"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Net_duration = string.Empty;
                    else
                        objtblEmploymentHistory.Net_duration = (string)dTable.Rows[0]["net_duration"];
                    if (dTable.Rows[0]["remark"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Remark = string.Empty;
                    else
                        objtblEmploymentHistory.Remark = (string)dTable.Rows[0]["remark"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblEmploymentHistory::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblEmploymentHistory;
        }

       
        public List<tblEmploymentHistory> GetList()
        {
            List<tblEmploymentHistory> RecordsList = new List<tblEmploymentHistory>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [MainGuid],[seq_no],[ParentGuid],[institution],[location],[tax_payment_letter_no],[job_title],[date_from],[date_to],[salary],[date_termination],[termination_reason],[emp_status],[net_duration],[remark] FROM [dbo].[tblEmploymentHistory]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblEmploymentHistory objtblEmploymentHistory = new tblEmploymentHistory();
                    if (dr["MainGuid"].Equals(DBNull.Value))
                        objtblEmploymentHistory.MainGuid = Guid.Empty;
                    else
                        objtblEmploymentHistory.MainGuid = (Guid)dr["MainGuid"];
                    if (dr["seq_no"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Seq_no = 0;
                    else
                        objtblEmploymentHistory.Seq_no = (int)dr["seq_no"];
                    if (dr["ParentGuid"].Equals(DBNull.Value))
                        objtblEmploymentHistory.ParentGuid = Guid.Empty;
                    else
                        objtblEmploymentHistory.ParentGuid = (Guid)dr["ParentGuid"];
                    if (dr["institution"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Institution = string.Empty;
                    else
                        objtblEmploymentHistory.Institution = (string)dr["institution"];
                    if (dr["location"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Location = string.Empty;
                    else
                        objtblEmploymentHistory.Location = (string)dr["location"];
                    if (dr["tax_payment_letter_no"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Tax_payment_letter_no = string.Empty;
                    else
                        objtblEmploymentHistory.Tax_payment_letter_no = (string)dr["tax_payment_letter_no"];
                    if (dr["job_title"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Job_title = string.Empty;
                    else
                        objtblEmploymentHistory.Job_title = (string)dr["job_title"];
                    if (dr["date_from"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Date_from = DateTime.MinValue;
                    else
                        objtblEmploymentHistory.Date_from = (DateTime)dr["date_from"];
                    if (dr["date_to"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Date_to = DateTime.MinValue;
                    else
                        objtblEmploymentHistory.Date_to = (DateTime)dr["date_to"];
                    if (dr["salary"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Salary = 0;
                    else
                        objtblEmploymentHistory.Salary = (double)dr["salary"];
                    if (dr["date_termination"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Date_termination = DateTime.MinValue;
                    else
                        objtblEmploymentHistory.Date_termination = (DateTime)dr["date_termination"];
                    if (dr["termination_reason"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Termination_reason = string.Empty;
                    else
                        objtblEmploymentHistory.Termination_reason = (string)dr["termination_reason"];
                    if (dr["emp_status"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Emp_status = 0;
                    else
                        objtblEmploymentHistory.Emp_status = (int)dr["emp_status"];
                    if (dr["net_duration"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Net_duration = string.Empty;
                    else
                        objtblEmploymentHistory.Net_duration = (string)dr["net_duration"];
                    if (dr["remark"].Equals(DBNull.Value))
                        objtblEmploymentHistory.Remark = string.Empty;
                    else
                        objtblEmploymentHistory.Remark = (string)dr["remark"];
                    RecordsList.Add(objtblEmploymentHistory);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblEmploymentHistory::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }
       

    }
}