﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using System.Web.SessionState;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class ParentMessageDAL
    {
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }
        static readonly string Key = "NewsMessageExchange";

        HttpSessionState Session { get { return HttpContext.Current.Session; } }
       
        public bool Insert(MessageExchangeBO objtblMessageExchange)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[tblInspectionParentMessage]
                                            ([OrgGuid],[Case],[Subject],[Text],[EmployeeName], [UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime] )
                                     VALUES    (@OrgGuid,@Case,@Subject,@Text,@EmployeeName, @UserName,@EventDatetime,@UpdatedUsername,@UpdatedEventDatetime )";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                command.Parameters.Add(new SqlParameter("@OrgGuid", objtblMessageExchange.OrgGuid));
                command.Parameters.Add(new SqlParameter("@Case", objtblMessageExchange.Case));
                command.Parameters.Add(new SqlParameter("@Subject", objtblMessageExchange.Subject));
                command.Parameters.Add(new SqlParameter("@Text", objtblMessageExchange.Text));
                command.Parameters.Add(new SqlParameter("@EmployeeName", objtblMessageExchange.EmployeeName));
                command.Parameters.Add(new SqlParameter("@UserName", objtblMessageExchange.UserName));
                command.Parameters.Add(new SqlParameter("@EventDatetime", objtblMessageExchange.EventDatetime));
                command.Parameters.Add(new SqlParameter("@UpdatedUsername", objtblMessageExchange.UpdatedUsername));
                command.Parameters.Add(new SqlParameter("@UpdatedEventDatetime", objtblMessageExchange.UpdatedEventDatetime));
                command.Parameters.Add(new SqlParameter("@FlowStatus", objtblMessageExchange.FlowStatus));

                

                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblMessageExchange::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(MessageExchangeBO objtblMessageExchange)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[tblInspectionParentMessage] SET [OrgGuid]=@OrgGuid,  [Case]=@Case,  
                                [Subject]=@Subject, [Text]=@Text, 
                                    [UpdatedUsername]=@UpdatedUsername,    
                                [UpdatedEventDatetime]=@UpdatedEventDatetime  WHERE [ParentMessageID]=@ParentMessageID";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ParentMessageID", objtblMessageExchange.ParentMessageID));
                command.Parameters.Add(new SqlParameter("@OrgGuid", objtblMessageExchange.OrgGuid));
                command.Parameters.Add(new SqlParameter("@Case", objtblMessageExchange.Case));
                command.Parameters.Add(new SqlParameter("@Subject", objtblMessageExchange.Subject));
                command.Parameters.Add(new SqlParameter("@Text", objtblMessageExchange.Text));
                command.Parameters.Add(new SqlParameter("@EmployeeName", objtblMessageExchange.EmployeeName));
                command.Parameters.Add(new SqlParameter("@UserName", objtblMessageExchange.UserName));
                command.Parameters.Add(new SqlParameter("@EventDatetime", objtblMessageExchange.EventDatetime));
                command.Parameters.Add(new SqlParameter("@UpdatedUsername", objtblMessageExchange.UpdatedUsername));
                command.Parameters.Add(new SqlParameter("@UpdatedEventDatetime", objtblMessageExchange.UpdatedEventDatetime));
                command.Parameters.Add(new SqlParameter("@MessageStatus", objtblMessageExchange.MessageStatus));
                command.Parameters.Add(new SqlParameter("@FlowStatus", objtblMessageExchange.FlowStatus));

                

                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblMessageExchange::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public MessageExchangeBO GetMaxParent()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            SqlDataReader dr = null;
            MessageExchangeBO objMessageExchangeBO = new MessageExchangeBO();
            string strGetAllRecords = @"SELECT max(ParentMessageID) as ParentMessageID
                                      FROM [dbo].[tblInspectionParentMessage]";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;
            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                if (dr.Read())
                {
                    if (dr["ParentMessageID"].Equals(DBNull.Value))
                        objMessageExchangeBO.ParentMessageID = 0;
                    else
                        objMessageExchangeBO.ParentMessageID = (int)dr["ParentMessageID"];
                }


            }
            catch (Exception ex)
            {
                throw new Exception("tblComplaint::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                dr.Dispose();
            }
            return objMessageExchangeBO;
        }


        public List<MessageExchangeBO> GetList()
        {
            List<MessageExchangeBO> RecordsList = new List<MessageExchangeBO>();
            SqlConnection    connection=new SqlConnection(ConnectionString);
            SqlDataReader dr=null;
            string strGetAllRecords = @"SELECT [ParentMessageID],[OrgGuid],[Case],[Date],[Subject],[Text], [EmployeeName]
                                        FROM [dbo].[tblInspectionParentMessage]";

            SqlCommand    command = new SqlCommand() {CommandText=strGetAllRecords, CommandType=CommandType.Text};
            command.Connection = connection;

            try
            {
            connection.Open();
            dr= command.ExecuteReader();
            while (dr.Read())
            {
                MessageExchangeBO objtblMessageExchange = new MessageExchangeBO();
                if (dr["ParentMessageID"].Equals(DBNull.Value))
                    objtblMessageExchange.ParentMessageID = 0;
                else
                    objtblMessageExchange.ParentMessageID = (int)dr["ParentMessageID"];
                 if(dr["OrgGuid"].Equals(DBNull.Value))
                     objtblMessageExchange.OrgGuid = Guid.Empty;
                else
                    objtblMessageExchange.OrgGuid = (Guid) dr["OrgGuid"];
                 if(dr["Case"].Equals(DBNull.Value))
                     objtblMessageExchange.Case = string.Empty;
                else
                    objtblMessageExchange.Case = (string) dr["Case"];
                 if(dr["Subject"].Equals(DBNull.Value))
                     objtblMessageExchange.Subject = string.Empty;
                else
                    objtblMessageExchange.Subject = (string) dr["Subject"];
                 if(dr["Text"].Equals(DBNull.Value))
                     objtblMessageExchange.Text = string.Empty;
                else
                    objtblMessageExchange.Text = (string) dr["Text"];
                 if (dr["EmployeeName"].Equals(DBNull.Value))
                     objtblMessageExchange.EmployeeName = string.Empty;
                 else
                     objtblMessageExchange.EmployeeName = (string)dr["EmployeeName"];
                
                RecordsList.Add(objtblMessageExchange);
            }

            }
            catch(Exception ex)
            {
                throw new Exception("tblMessageExchange::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return  RecordsList;
        }


        public MessageExchangeBO GetParentMessage(Int32 ID)
        {
            MessageExchangeBO objtblMessageExchange = new MessageExchangeBO();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
            //string condition = Session["OrgGuid"].ToString();
            string strGetAllRecords = @"SELECT [ParentMessageID],[OrgGuid],[Case],[Date],[Subject],[Text],[EmployeeName],Cast([FlowStatus] as nvarchar(50)) as FlowStatus 
                                        FROM [dbo].[tblInspectionParentMessage] WHERE ParentMessageID=" + ID + " ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    //MessageExchangeBO objtblMessageExchange = new MessageExchangeBO();
                    if (dr["ParentMessageID"].Equals(DBNull.Value))
                        objtblMessageExchange.ParentMessageID = 0;
                    else
                        objtblMessageExchange.ParentMessageID = (int)dr["ParentMessageID"];
                    if (dr["OrgGuid"].Equals(DBNull.Value))
                        objtblMessageExchange.OrgGuid = Guid.Empty;
                    else
                        objtblMessageExchange.OrgGuid = (Guid)dr["OrgGuid"];
                    if (dr["Case"].Equals(DBNull.Value))
                        objtblMessageExchange.Case = string.Empty;
                    else
                        objtblMessageExchange.Case = (string)dr["Case"];
                    if (dr["Subject"].Equals(DBNull.Value))
                        objtblMessageExchange.Subject = string.Empty;
                    else
                        objtblMessageExchange.Subject = (string)dr["Subject"];
                    if (dr["Text"].Equals(DBNull.Value))
                        objtblMessageExchange.Text = string.Empty;
                    else
                        objtblMessageExchange.Text = (string)dr["Text"];

                    if (dr["EmployeeName"].Equals(DBNull.Value))
                        objtblMessageExchange.EmployeeName = string.Empty;
                    else
                        objtblMessageExchange.EmployeeName = (string)dr["EmployeeName"];
                    if (dr["FlowStatus"].Equals(DBNull.Value))
                        objtblMessageExchange.FlowStatus = string.Empty;
                    else
                        objtblMessageExchange.FlowStatus = (string)dr["FlowStatus"];
                    //if (dr["FlowStatus"].Equals(DBNull.Value))
                    //    objtblMessageExchange.FlowStatus = string.Empty;
                    //else
                    //    objtblMessageExchange.FlowStatus = (string)dr["FlowStatus"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblMessageExchange::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return objtblMessageExchange;
        }


        public List<MessageExchangeBO> GetParentMessages(String Condition)
        {
            List<MessageExchangeBO> RecordsList = new List<MessageExchangeBO>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
            string strGetAllRecords="";

            strGetAllRecords = @"SELECT     dbo.tblInspectionParentMessage.ParentMessageID, dbo.Organization.OrgGuid, AnsweredDate, SentDate, dbo.Organization.DescriptionAm, dbo.tblInspectionParentMessage.Date, dbo.tblInspectionParentMessage.[Case], 
                                            dbo.tblInspectionParentMessage.Subject, dbo.tblInspectionParentMessage.Text, dbo.tblInspectionParentMessage.EmployeeName, dbo.tblInspectionParentMessage.UserName, 
                                            dbo.tblInspectionParentMessage.EventDatetime, dbo.tblInspectionParentMessage.UpdatedUsername, dbo.tblInspectionParentMessage.FinalRevisedBy, dbo.tblInspectionParentMessage.FinalAnswerdBy, 
                                            dbo.tblInspectionParentMessage.UpdatedEventDatetime, FlowStatus AS FStatus,
                      
                                            'FlowStatus'= CASE FlowStatus 
                                            WHEN  0 THEN N'ድራፍት'
                                            WHEN  2 THEN N'ተልከዋል'
                                            WHEN  4 THEN N'መልስ ተሰጥቶታል' END 
                                      FROM  dbo.tblInspectionParentMessage INNER JOIN
                                            dbo.Organization ON dbo.tblInspectionParentMessage.OrgGuid = dbo.Organization.OrgGuid
                                            Where 1=1 " + Condition; //AND [Organization].[OrgGuid]='" + Session["ProfileOrgGuid"].ToString() + "' "; //Session["parentMessageCondition"].ToString();
            
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    MessageExchangeBO objtblMessageExchange = new MessageExchangeBO();
                    if (dr["ParentMessageID"].Equals(DBNull.Value))
                        objtblMessageExchange.ParentMessageID = 0;
                    else
                        objtblMessageExchange.ParentMessageID = (int)dr["ParentMessageID"];
                    if (dr["OrgGuid"].Equals(DBNull.Value))
                        objtblMessageExchange.OrgGuid = Guid.Empty;
                    else
                        objtblMessageExchange.OrgGuid = (Guid)dr["OrgGuid"];
                    if (dr["DescriptionAm"].Equals(DBNull.Value))
                        objtblMessageExchange.OrgName = string.Empty;
                    else
                        objtblMessageExchange.OrgName = (string)dr["DescriptionAm"];

                    if (dr["Case"].Equals(DBNull.Value))
                        objtblMessageExchange.Case = string.Empty;
                    else
                        objtblMessageExchange.Case = (string)dr["Case"];
                    if (dr["Subject"].Equals(DBNull.Value))
                        objtblMessageExchange.Subject = string.Empty;
                    else
                        objtblMessageExchange.Subject = (string)dr["Subject"];
                
                    if (dr["Text"].Equals(DBNull.Value))
                        objtblMessageExchange.Text = string.Empty;
                    else
                        objtblMessageExchange.Text = (string)dr["Text"];

                      if (dr["EmployeeName"].Equals(DBNull.Value))
                        objtblMessageExchange.EmployeeName = string.Empty;
                    else
                        objtblMessageExchange.EmployeeName = (string)dr["EmployeeName"];

                    if (dr["UserName"].Equals(DBNull.Value))
                        objtblMessageExchange.UserName = string.Empty;
                    else
                        objtblMessageExchange.UserName = (string)dr["UserName"];

                      if (dr["EventDatetime"].Equals(DBNull.Value))
                        objtblMessageExchange.EventDatetime = DateTime.MaxValue;
                    else
                        objtblMessageExchange.EventDatetime = (DateTime)dr["EventDatetime"];

                    //
                      if (dr["SentDate"].Equals(DBNull.Value))
                          objtblMessageExchange.SentDate = DateTime.MaxValue;
                      else
                          objtblMessageExchange.SentDate = (DateTime)dr["SentDate"];

                      if (dr["AnsweredDate"].Equals(DBNull.Value))
                          objtblMessageExchange.AnsweredDate = DateTime.MaxValue;
                      else
                          objtblMessageExchange.AnsweredDate = (DateTime)dr["AnsweredDate"];
                    
                      if (dr["FStatus"].Equals(DBNull.Value))
                          objtblMessageExchange.FStatus = string.Empty;
                    else
                          objtblMessageExchange.FStatus = dr["FStatus"].ToString();

                      if (dr["FinalAnswerdBy"].Equals(DBNull.Value))
                          objtblMessageExchange.FinalAnswerdBy = string.Empty;
                    else
                          objtblMessageExchange.FinalAnswerdBy = (string)dr["FinalAnswerdBy"];

                      if (dr["UpdatedUsername"].Equals(DBNull.Value))
                        objtblMessageExchange.UpdatedUsername = string.Empty;
                    else
                        objtblMessageExchange.UpdatedUsername = (string)dr["UpdatedUsername"];

                      if (dr["FinalRevisedBy"].Equals(DBNull.Value))
                        objtblMessageExchange.FinalRevisedBy = string.Empty;
                    else
                        objtblMessageExchange.FinalRevisedBy = (string)dr["FinalRevisedBy"];

                      if (dr["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objtblMessageExchange.UpdatedEventDatetime = DateTime.MaxValue;
                    else
                        objtblMessageExchange.UpdatedEventDatetime = (DateTime)dr["UpdatedEventDatetime"];
                      //if (dr["FlowStatus"].Equals(DBNull.Value))
                      //    objtblMessageExchange.FlowStatus = 0;
                      //else
                      //    objtblMessageExchange.FlowStatus = (int)dr["FlowStatus"];
                      if (dr["FlowStatus"].Equals(DBNull.Value))
                          objtblMessageExchange.FlowStatus = string.Empty;
                      else
                          objtblMessageExchange.FlowStatus = (string)dr["FlowStatus"];




                    RecordsList.Add(objtblMessageExchange);
                }

            }
            catch (Exception ex)
            {
               // Msg + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }



        public List<MessageExchangeBO> GetAllReceivedParentMessages()
        {
            List<MessageExchangeBO> RecordsList = new List<MessageExchangeBO>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
            string strGetAllRecords = "";

            strGetAllRecords = @"SELECT dbo.tblInspectionParentMessage.ParentMessageID, dbo.Organization.OrgGuid, AnsweredDate, SentDate, dbo.Organization.DescriptionAm, dbo.tblInspectionParentMessage.Date, dbo.tblInspectionParentMessage.[Case], 
                                            dbo.tblInspectionParentMessage.Subject, dbo.tblInspectionParentMessage.Text, dbo.tblInspectionParentMessage.EmployeeName, dbo.tblInspectionParentMessage.UserName, 
                                            dbo.tblInspectionParentMessage.EventDatetime, dbo.tblInspectionParentMessage.UpdatedUsername, dbo.tblInspectionParentMessage.FinalRevisedBy, dbo.tblInspectionParentMessage.FinalAnswerdBy, 
                                            dbo.tblInspectionParentMessage.UpdatedEventDatetime, FlowStatus AS FStatus,
                      
                                            'FlowStatus'= CASE FlowStatus 
                                            WHEN  0 THEN N'ድራፍት'
                                            WHEN  2 THEN N'ተልከዋል'
                                            WHEN  4 THEN N'መልስ ተሰጥቶታል' END 
                                      FROM  dbo.tblInspectionParentMessage INNER JOIN
                                            dbo.Organization ON dbo.tblInspectionParentMessage.OrgGuid = dbo.Organization.OrgGuid
                                            Where 1=1 AND (FlowStatus=2 OR FlowStatus=4) "; 

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    MessageExchangeBO objtblMessageExchange = new MessageExchangeBO();
                    if (dr["ParentMessageID"].Equals(DBNull.Value))
                        objtblMessageExchange.ParentMessageID = 0;
                    else
                        objtblMessageExchange.ParentMessageID = (int)dr["ParentMessageID"];
                    if (dr["OrgGuid"].Equals(DBNull.Value))
                        objtblMessageExchange.OrgGuid = Guid.Empty;
                    else
                        objtblMessageExchange.OrgGuid = (Guid)dr["OrgGuid"];
                    if (dr["DescriptionAm"].Equals(DBNull.Value))
                        objtblMessageExchange.OrgName = string.Empty;
                    else
                        objtblMessageExchange.OrgName = (string)dr["DescriptionAm"];

                    if (dr["Case"].Equals(DBNull.Value))
                        objtblMessageExchange.Case = string.Empty;
                    else
                        objtblMessageExchange.Case = (string)dr["Case"];
                    if (dr["Subject"].Equals(DBNull.Value))
                        objtblMessageExchange.Subject = string.Empty;
                    else
                        objtblMessageExchange.Subject = (string)dr["Subject"];

                    if (dr["Text"].Equals(DBNull.Value))
                        objtblMessageExchange.Text = string.Empty;
                    else
                        objtblMessageExchange.Text = (string)dr["Text"];

                    if (dr["EmployeeName"].Equals(DBNull.Value))
                        objtblMessageExchange.EmployeeName = string.Empty;
                    else
                        objtblMessageExchange.EmployeeName = (string)dr["EmployeeName"];

                    if (dr["UserName"].Equals(DBNull.Value))
                        objtblMessageExchange.UserName = string.Empty;
                    else
                        objtblMessageExchange.UserName = (string)dr["UserName"];

                    if (dr["EventDatetime"].Equals(DBNull.Value))
                        objtblMessageExchange.EventDatetime = DateTime.MaxValue;
                    else
                        objtblMessageExchange.EventDatetime = (DateTime)dr["EventDatetime"];

                    //
                    if (dr["SentDate"].Equals(DBNull.Value))
                        objtblMessageExchange.SentDate = DateTime.MaxValue;
                    else
                        objtblMessageExchange.SentDate = (DateTime)dr["SentDate"];

                    if (dr["AnsweredDate"].Equals(DBNull.Value))
                        objtblMessageExchange.AnsweredDate = DateTime.MaxValue;
                    else
                        objtblMessageExchange.AnsweredDate = (DateTime)dr["AnsweredDate"];
                    //

                    if (dr["FStatus"].Equals(DBNull.Value))
                        objtblMessageExchange.FStatus = string.Empty;
                    else
                        objtblMessageExchange.FStatus = dr["FStatus"].ToString();


                    if (dr["FinalAnswerdBy"].Equals(DBNull.Value))
                        objtblMessageExchange.FinalAnswerdBy = string.Empty;
                    else
                        objtblMessageExchange.FinalAnswerdBy = (string)dr["FinalAnswerdBy"];

                    if (dr["UpdatedUsername"].Equals(DBNull.Value))
                        objtblMessageExchange.UpdatedUsername = string.Empty;
                    else
                        objtblMessageExchange.UpdatedUsername = (string)dr["UpdatedUsername"];

                    if (dr["FinalRevisedBy"].Equals(DBNull.Value))
                        objtblMessageExchange.FinalRevisedBy = string.Empty;
                    else
                        objtblMessageExchange.FinalRevisedBy = (string)dr["FinalRevisedBy"];

                    if (dr["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objtblMessageExchange.UpdatedEventDatetime = DateTime.MaxValue;
                    else
                        objtblMessageExchange.UpdatedEventDatetime = (DateTime)dr["UpdatedEventDatetime"];

                    //if (dr["FlowStatus"].Equals(DBNull.Value))
                    //    objtblMessageExchange.FlowStatus = string.Empty;
                    //else
                    //    objtblMessageExchange.FlowStatus = (string)dr["FlowStatus"];
                    if (dr["FlowStatus"].Equals(DBNull.Value))
                        objtblMessageExchange.FlowStatus = string.Empty;
                    else
                        objtblMessageExchange.FlowStatus = (string)dr["FlowStatus"];




                    RecordsList.Add(objtblMessageExchange);
                }

            }
            catch (Exception ex)
            {
                // Msg + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public bool DeleteMessage(Int32 ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblInspectionParentMessage] WHERE ParentMessageID=" + ID + " ";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblMessageExchange::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public DataTable poplateOrganization()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            string strPoplatePosition = @"SELECT [OrgGuid], [Description], [DescriptionAm] FROM [dbo].[Organization] ";
            SqlCommand command = new SqlCommand() { CommandText = strPoplatePosition, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Organization");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;
            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Organization::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        //insert message attachment
        public bool InsertMessageAttachement(AttachmentMessageExchangeBO objtblMessageExchangeAttachment)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[tblInspectionMessageExchangeAttachment]
                                            ([MainGuid],[Url],[ID],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime])
                                     VALUES    (@MainGuid,@Url,@ID, @UserName,@EventDatetime,@UpdatedUsername,@UpdatedEventDatetime)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", objtblMessageExchangeAttachment.MainGuid));
                command.Parameters.Add(new SqlParameter("@Url", objtblMessageExchangeAttachment.Url));
                command.Parameters.Add(new SqlParameter("@ID", objtblMessageExchangeAttachment.ID));
                //command.Parameters.Add(new SqlParameter("@Description", objtblMessageExchangeAttachment.txtFileDescription));
                command.Parameters.Add(new SqlParameter("@UserName", objtblMessageExchangeAttachment.UserName));
                command.Parameters.Add(new SqlParameter("@EventDatetime", objtblMessageExchangeAttachment.EventDatetime));
                command.Parameters.Add(new SqlParameter("@UpdatedUsername", objtblMessageExchangeAttachment.UpdatedUsername));
                command.Parameters.Add(new SqlParameter("@UpdatedEventDatetime", objtblMessageExchangeAttachment.UpdatedEventDatetime));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblMessageExchangeAttachment::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


        //message attachment
        public DataTable BindMessageAttachments(Int32 ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            string strPoplatePosition = @"SELECT dbo.tblInspectionMessageExchangeAttachment.MainGuid, dbo.tblInspectionMessageExchangeAttachment.Url,[Description], dbo.tblInspectionParentMessage.Subject, dbo.tblInspectionParentMessage.ParentMessageID
                                            FROM dbo.tblInspectionMessageExchangeAttachment INNER JOIN
                                             dbo.tblInspectionParentMessage ON dbo.tblInspectionMessageExchangeAttachment.ID = dbo.tblInspectionParentMessage.ParentMessageID
                                             WHERE dbo.tblInspectionMessageExchangeAttachment.ID=" + ID + "";
            SqlCommand command = new SqlCommand() { CommandText = strPoplatePosition, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblInspectionMessageExchangeAttachment");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;
            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblMessageExchangeAttachment::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public bool UpdateParentMessageStatus(Int32 ParentMessageID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[tblInspectionParentMessage] SET Status=1
                                WHERE ParentMessageID=" + ParentMessageID + "";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionParentMessage::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
  
//     public MessageExchangeBO GetParentMessage(Int32 ID)
//        {
//            MessageExchangeBO objtblMessageExchange = new MessageExchangeBO();
//            SqlConnection connection = new SqlConnection(ConnectionString);
//            SqlDataReader dr = null;
//            //string condition = Session["OrgGuid"].ToString();
//            string strGetAllRecords = @"SELECT [ParentMessageID],[OrgGuid],[Case],[Date],[Subject],[Text],[EmployeeName]
//                                        FROM [dbo].[tblParentMessage] WHERE ParentMessageID="+ID+" ";

//            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
//            command.Connection = connection;

//            try
//            {
//                connection.Open();
//                dr = command.ExecuteReader();
//                while (dr.Read())
//                {
//                    //MessageExchangeBO objtblMessageExchange = new MessageExchangeBO();
//                    if (dr["ParentMessageID"].Equals(DBNull.Value))
//                        objtblMessageExchange.ParentMessageID = 0;
//                    else
//                        objtblMessageExchange.ParentMessageID = (int)dr["ParentMessageID"];
//                    if (dr["OrgGuid"].Equals(DBNull.Value))
//                        objtblMessageExchange.OrgGuid = Guid.Empty;
//                    else
//                        objtblMessageExchange.OrgGuid = (Guid)dr["OrgGuid"];
//                    if (dr["Case"].Equals(DBNull.Value))
//                        objtblMessageExchange.Case = string.Empty;
//                    else
//                        objtblMessageExchange.Case = (string)dr["Case"];
//                    if (dr["Subject"].Equals(DBNull.Value))
//                        objtblMessageExchange.Subject = string.Empty;
//                    else
//                        objtblMessageExchange.Subject = (string)dr["Subject"];
//                    if (dr["Text"].Equals(DBNull.Value))
//                        objtblMessageExchange.Text = string.Empty;
//                    else
//                        objtblMessageExchange.Text = (string)dr["Text"];

//                    if (dr["EmployeeName"].Equals(DBNull.Value))
//                        objtblMessageExchange.EmployeeName = string.Empty;
//                    else
//                        objtblMessageExchange.EmployeeName = (string)dr["EmployeeName"];
//                }

//            }
//            catch (Exception ex)
//            {
//                throw new Exception("tblMessageExchange::GetList::Error!" + ex.Message, ex);
//            }
//            finally
//            {
//                connection.Close();
//                dr.Close();
//                command.Dispose();
//            }
//            return objtblMessageExchange;
//        }





     public bool MessageApproved(Int32 ParentMessageID)
     {
         bool messageApproved = false;
         SqlConnection connection = new SqlConnection(ConnectionString);
         string strPoplatePosition = @"select * from tblInspectionParentMessage
                                        where ParentMessageID=" + ParentMessageID + " AND FlowStatus=4";
         SqlCommand command = new SqlCommand() { CommandText = strPoplatePosition, CommandType = CommandType.Text };
         DataTable dTable = new DataTable("tblInspectionParentMessage");
         SqlDataAdapter adapter = new SqlDataAdapter(command);
         command.Connection = connection;
         try
         {
             connection.Open();
             adapter.Fill(dTable);
             if (dTable.Rows.Count >= 1)
                 messageApproved = true;
         }
         catch (Exception ex)
         {
             throw new Exception("Organization::GetRecord::Error!" + ex.Message, ex);
         }
         finally
         {
             connection.Close();
             command.Dispose();
             adapter.Dispose();
         }
         return messageApproved;
     }
    }


    //MessageExchange
    public class MessageExchangeDataAccess
    {



        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [parentID],[OrgGuid],[EmployeeName],[Subject],[Text],[UserName],[EventDatetime],
                                        [UpdatedUsername],[UpdatedEventDatetime] FROM [dbo].[tblMessageExchange] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblMessageExchange");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblMessageExchange::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public List<MessageExchangeBO> GetParentMessageList(Int32 ParentID)
        {
            List<MessageExchangeBO> RecordsList = new List<MessageExchangeBO>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
            string strGetAllRecords = @"SELECT [OrgGuid],[Case],[Date],[Subject],[Status],[Text], [EmployeeName]
                                        FROM [dbo].[tblInspectionParentMessage] where ParentMessageID=@ParentMessageID";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ParentMessageID", ParentID));
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    MessageExchangeBO objtblMessageExchange = new MessageExchangeBO();
                   
                   if (dr["OrgGuid"].Equals(DBNull.Value))
                        objtblMessageExchange.OrgGuid = Guid.Empty;
                    else
                        objtblMessageExchange.OrgGuid = (Guid)dr["OrgGuid"];
                    if (dr["Case"].Equals(DBNull.Value))
                        objtblMessageExchange.Case = string.Empty;
                    else
                        objtblMessageExchange.Case = (string)dr["Case"];
                    if (dr["Subject"].Equals(DBNull.Value))
                        objtblMessageExchange.Subject = string.Empty;
                    else
                        objtblMessageExchange.Subject = (string)dr["Subject"];
                    if (dr["Status"].Equals(DBNull.Value))
                        objtblMessageExchange.Status = false;
                    else
                        objtblMessageExchange.Status = (bool)dr["Status"];
                    if (dr["Text"].Equals(DBNull.Value))
                        objtblMessageExchange.Text = string.Empty;
                    else
                        objtblMessageExchange.Text = (string)dr["Text"];
                    if (dr["EmployeeName"].Equals(DBNull.Value))
                        objtblMessageExchange.EmployeeName = string.Empty;
                    else
                        objtblMessageExchange.EmployeeName = (string)dr["EmployeeName"];

                    RecordsList.Add(objtblMessageExchange);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblMessageExchange::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public MessageExchangeBO GetRecord(Int32 MessageID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            MessageExchangeBO objtblMessageExchange = new MessageExchangeBO();
            string strGetRecord = @"SELECT [parentID],[OrgGuid],[EmployeeName],[Subject],[Text],[UserName],[EventDatetime],
                                    [UpdatedUsername],[UpdatedEventDatetime] FROM [dbo].[tblInspectionMessageExchange] WHERE ID=" + MessageID + " ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblMessageExchange");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    if (dTable.Rows[0]["parentID"].Equals(DBNull.Value))
                        objtblMessageExchange.ParentID = 0;
                    else
                        objtblMessageExchange.ParentID = (int)dTable.Rows[0]["parentID"];
                    if (dTable.Rows[0]["OrgGuid"].Equals(DBNull.Value))
                        objtblMessageExchange.OrgGuid = Guid.Empty;
                    else
                        objtblMessageExchange.OrgGuid = (Guid)dTable.Rows[0]["OrgGuid"];
                    if (dTable.Rows[0]["EmployeeName"].Equals(DBNull.Value))
                        objtblMessageExchange.EmployeeName = string.Empty;
                    else
                        objtblMessageExchange.EmployeeName = (string)dTable.Rows[0]["EmployeeName"];
                    if (dTable.Rows[0]["Subject"].Equals(DBNull.Value))
                        objtblMessageExchange.Subject = string.Empty;
                    else
                        objtblMessageExchange.Subject = (string)dTable.Rows[0]["Subject"];
                    if (dTable.Rows[0]["Text"].Equals(DBNull.Value))
                        objtblMessageExchange.Text = string.Empty;
                    else
                        objtblMessageExchange.Text = (string)dTable.Rows[0]["Text"];
                    if (dTable.Rows[0]["UserName"].Equals(DBNull.Value))
                        objtblMessageExchange.UserName = string.Empty;
                    else
                        objtblMessageExchange.UserName = (string)dTable.Rows[0]["UserName"];
                    if (dTable.Rows[0]["EventDatetime"].Equals(DBNull.Value))
                        objtblMessageExchange.EventDatetime = DateTime.MinValue;
                    else
                        objtblMessageExchange.EventDatetime = (DateTime)dTable.Rows[0]["EventDatetime"];
                    if (dTable.Rows[0]["UpdatedUsername"].Equals(DBNull.Value))
                        objtblMessageExchange.UpdatedUsername = string.Empty;
                    else
                        objtblMessageExchange.UpdatedUsername = (string)dTable.Rows[0]["UpdatedUsername"];
                    if (dTable.Rows[0]["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objtblMessageExchange.UpdatedEventDatetime = DateTime.MinValue;
                    else
                        objtblMessageExchange.UpdatedEventDatetime = (DateTime)dTable.Rows[0]["UpdatedEventDatetime"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblMessageExchange::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblMessageExchange;
        }

        public bool Insert(MessageExchangeBO objtblMessageExchange)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[tblInspectionMessageExchange]
                                            ([parentID],[OrgGuid],[EmployeeName],[Subject],[Text],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime])
                                     VALUES    (@parentID,@OrgGuid,@EmployeeName,@Subject,@Text,@UserName,@EventDatetime,@UpdatedUsername,@UpdatedEventDatetime)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@parentID", objtblMessageExchange.ParentID));
                command.Parameters.Add(new SqlParameter("@OrgGuid", objtblMessageExchange.OrgGuid));
                command.Parameters.Add(new SqlParameter("@EmployeeName", objtblMessageExchange.EmployeeName));
                command.Parameters.Add(new SqlParameter("@Subject", objtblMessageExchange.Subject));
                command.Parameters.Add(new SqlParameter("@Text", objtblMessageExchange.Text));
                command.Parameters.Add(new SqlParameter("@UserName", objtblMessageExchange.UserName));
                command.Parameters.Add(new SqlParameter("@EventDatetime", objtblMessageExchange.EventDatetime));
                command.Parameters.Add(new SqlParameter("@UpdatedUsername", objtblMessageExchange.UpdatedUsername));
                command.Parameters.Add(new SqlParameter("@UpdatedEventDatetime", objtblMessageExchange.UpdatedEventDatetime));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblMessageExchange::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(MessageExchangeBO objtblMessageExchange)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[tblMessageExchange] SET [parentID]=@parentID, [OrgGuid]=@OrgGuid,    
                                 [EmployeeName]=@EmployeeName, [Subject]=@Subject, [Text]=@Text, [UserName]=@UserName,    
                                 [EventDatetime]=@EventDatetime, [UpdatedUsername]=@UpdatedUsername, [UpdatedEventDatetime]=@UpdatedEventDatetime
                                WHERE ID=@ID";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ID", objtblMessageExchange.ID));
                command.Parameters.Add(new SqlParameter("@parentID", objtblMessageExchange.ParentID));
                command.Parameters.Add(new SqlParameter("@OrgGuid", objtblMessageExchange.OrgGuid));
                command.Parameters.Add(new SqlParameter("@EmployeeName", objtblMessageExchange.EmployeeName));
                command.Parameters.Add(new SqlParameter("@Subject", objtblMessageExchange.Subject));
                command.Parameters.Add(new SqlParameter("@Text", objtblMessageExchange.Text));
                command.Parameters.Add(new SqlParameter("@UserName", objtblMessageExchange.UserName));
                command.Parameters.Add(new SqlParameter("@EventDatetime", objtblMessageExchange.EventDatetime));
                command.Parameters.Add(new SqlParameter("@UpdatedUsername", objtblMessageExchange.UpdatedUsername));
                command.Parameters.Add(new SqlParameter("@UpdatedEventDatetime", objtblMessageExchange.UpdatedEventDatetime));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblMessageExchange::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool UpdateParentMessageStatus (Int32 ParentMessageID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[tblInspectionParentMessage] SET Status=1
                                WHERE ParentMessageID=" + ParentMessageID + "";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionParentMessage::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        //call update position request status to final under this parent message!
        //Update PositionApproved parallel to PositionRequest!

        public bool UpdateParentMessageFllowStatus(Int32 ParentMessageID, String FinalRevisedBy, Int32 statusValue)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            string strUpdate = "";
                if(statusValue==2)
                {
                    strUpdate = @"UPDATE [dbo].[tblInspectionParentMessage] SET FlowStatus=" + statusValue + ", FinalRevisedBy=N'" + FinalRevisedBy + "', SentDate=GETDATE() WHERE ParentMessageID=" + ParentMessageID + "";
                }
                else if (statusValue==4)
                {
                    strUpdate = @"UPDATE [dbo].[tblInspectionParentMessage] SET FlowStatus=" + statusValue + ", FinalAnswerdBy=N'" + FinalRevisedBy + "', AnsweredDate=GETDATE() WHERE ParentMessageID=" + ParentMessageID + "";
                }
            
            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionParentMessage::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(Int32 MessageID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblInspectionMessageExchange] WHERE ID=" + MessageID + " ";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblMessageExchange::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }



        public List<MessageExchangeBO> GetList()
        {
            List<MessageExchangeBO> RecordsList = new List<MessageExchangeBO>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [parentID],[OrgGuid],[EmployeeName],[Subject],[Text],[UserName],[EventDatetime],[UpdatedUsername],
                                        [UpdatedEventDatetime] FROM [dbo].[tblInspectionMessageExchange] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    MessageExchangeBO objtblMessageExchange = new MessageExchangeBO();
                    if (dr["parentID"].Equals(DBNull.Value))
                        objtblMessageExchange.ParentID = 0;
                    else
                        objtblMessageExchange.ParentID = (int)dr["parentID"];
                    if (dr["OrgGuid"].Equals(DBNull.Value))
                        objtblMessageExchange.OrgGuid = Guid.Empty;
                    else
                        objtblMessageExchange.OrgGuid = (Guid)dr["OrgGuid"];
                    if (dr["EmployeeName"].Equals(DBNull.Value))
                        objtblMessageExchange.EmployeeName = string.Empty;
                    else
                        objtblMessageExchange.EmployeeName = (string)dr["EmployeeName"];
                    if (dr["Subject"].Equals(DBNull.Value))
                        objtblMessageExchange.Subject = string.Empty;
                    else
                        objtblMessageExchange.Subject = (string)dr["Subject"];
                    if (dr["Text"].Equals(DBNull.Value))
                        objtblMessageExchange.Text = string.Empty;
                    else
                        objtblMessageExchange.Text = (string)dr["Text"];
                    if (dr["UserName"].Equals(DBNull.Value))
                        objtblMessageExchange.UserName = string.Empty;
                    else
                        objtblMessageExchange.UserName = (string)dr["UserName"];
                    if (dr["EventDatetime"].Equals(DBNull.Value))
                        objtblMessageExchange.EventDatetime = DateTime.MinValue;
                    else
                        objtblMessageExchange.EventDatetime = (DateTime)dr["EventDatetime"];
                    if (dr["UpdatedUsername"].Equals(DBNull.Value))
                        objtblMessageExchange.UpdatedUsername = string.Empty;
                    else
                        objtblMessageExchange.UpdatedUsername = (string)dr["UpdatedUsername"];
                    if (dr["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objtblMessageExchange.UpdatedEventDatetime = DateTime.MinValue;
                    else
                        objtblMessageExchange.UpdatedEventDatetime = (DateTime)dr["UpdatedEventDatetime"];
                    RecordsList.Add(objtblMessageExchange);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblMessageExchange::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }


        public bool Exists(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT  FROM [dbo].[tblInspectionMessageExchange] ";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("tblMessageExchange::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }


    }
}