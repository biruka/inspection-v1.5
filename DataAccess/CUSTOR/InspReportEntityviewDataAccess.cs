﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.Commen;

namespace CUSTOR.DataAccess
{
    public class InspReportEntityviewDataAccess
    {
        
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }



        public DataTable GetRecords(string OrgGuid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [FullName],[MainGuid],[Gender],[Typeofemployment],[person_id],[staff_code],[staff_code_sort],[cost_center_code],[inspectstatus],[job_title]
                                      ,[Unitdesc]
                                      ,[grade_id]
                                      ,[AmGrade]
                                      ,[description]
                                      ,[InspectionDecision]
                                      ,[InspectionFinding]
                                      ,[DescriptionAm]
                                      ,[Activitytype] 
                                      ,[Answer]
                                       FROM [dbo].[InspReportEntityview] 
                                       " + OrgGuid + "";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("InspReportEntityview");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("InspReportEntityview::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

       
     public DataTable GetRecordcomplete(string OrgGuid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            string strGetAllRecords = @"SELECT ROW_NUMBER() OVER(ORDER BY dbo.Inspection.InspectionDate Desc) as row, 
                                        dbo.tblPerson.first_am_name + ' ' + dbo.tblPerson.father_am_name + ' ' + dbo.tblPerson.grand_am_name AS FullName, dbo.tblPerson.sex AS Gender, dbo.fnGetLookup(dbo.tblPerson.emp_status, 2)
                                        AS Typeofemployment, dbo.tblPosition.seq_no, dbo.tblPosition.job_title, dbo.tblPerson.MainGuid, dbo.fnGetInspectStatus(dbo.tblPerson.MainGuid, 311) AS inspectstatus, 
                                        dbo.fnGetInspDecStatus(dbo.tblPerson.MainGuid, 311) AS InspectionDecision, dbo.tblPerson.person_id, dbo.tblPerson.staff_code, dbo.tblPerson.staff_code_sort, dbo.tblPerson.cost_center_code, 
                                        dbo.tblPerson.unit_id, dbo.tblPerson.first_name, dbo.tblPerson.father_name, dbo.tblPerson.grand_name, dbo.tblPerson.allowance, dbo.tblPerson.salary, dbo.tblPerson.transport_allowance, 
                                        dbo.tblPerson.hardship_allowance, dbo.tblPerson.housing_allowance, dbo.tblPerson.over_time, dbo.tblPerson.pension_employee_contribution, dbo.tblPerson.pension_company_contribution, 
                                        dbo.tblPerson.is_ca_member, dbo.tblPerson.ca_membership_date, dbo.tblPerson.ca_termination_date, dbo.tblPerson.thrift_contribution, dbo.tblPerson.extra_contribution, dbo.tblPerson.card_fee, 
                                        dbo.tblPerson.membership_fee, dbo.tblPerson.is_membership_fee_returned, dbo.tblPerson.is_lu_member, dbo.tblPerson.labour_union, dbo.tblPerson.leave_without_pay, dbo.tblPerson.absentism, 
                                        dbo.tblPerson.fine, dbo.tblPerson.sex, dbo.tblPerson.religion, dbo.tblPerson.birth_date, dbo.tblPerson.birth_place, dbo.tblPerson.marital_status, dbo.tblPerson.family_size, 
                                        dbo.tblPerson.nationality, dbo.tblPerson.town, dbo.tblPerson.region, dbo.tblPerson.p_o_box, dbo.tblPerson.person_address, dbo.tblPerson.tel_home, dbo.tblPerson.tel_off_direct1, 
                                        dbo.tblPerson.tel_off_direct2, dbo.tblPerson.tel_off_ext1, dbo.tblPerson.room_no, dbo.tblPerson.passport_no, dbo.tblPerson.license_grade, dbo.tblPerson.education_group, 
                                        dbo.tblPerson.education_level, dbo.tblPerson.education_description, dbo.tblPerson.job_title_Sort, dbo.tblPerson.occupation, dbo.tblPerson.occupation_step, dbo.tblPerson.emp_date, 
                                        dbo.tblPerson.current_status, dbo.tblPerson.Kebele, dbo.tblPerson.Worede, dbo.tblPerson.Zone, dbo.tblPerson.location, dbo.tblPerson.emp_status, dbo.tblUnit.amDescription AS Unitdesc, 
                                        dbo.tblGrade.am_description AS AmGrade, dbo.Inspection.InspectionFinding, dbo.Inspection.InspectionDate, dbo.Inspection.Activitytype, dbo.Inspection.HRMServiceType, 
                                        dbo.Inspection.inspectiontype, dbo.tblPosition.position_code
                                        FROM  dbo.tblPerson INNER JOIN
                                        dbo.tblPosition ON dbo.tblPerson.job_title = dbo.tblPosition.seq_no INNER JOIN
                                        dbo.tblUnit ON dbo.tblPosition.unit_id = dbo.tblUnit.code INNER JOIN
                                        dbo.tblGrade ON dbo.tblPosition.grade_id = dbo.tblGrade.grade_id INNER JOIN
                                        dbo.Organization ON dbo.tblUnit.OrgGuid = dbo.Organization.OrgGuid INNER JOIN
                                        dbo.Inspection ON dbo.tblPerson.MainGuid = dbo.Inspection.EmployeeGUID  " + OrgGuid + "";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("InspReportEntityview");
            System.Type typeString = System.Type.GetType("System.String");
            dTable.Columns.Add("InspectionDate", typeString);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
                for (int index = 0; index < dTable.Rows.Count; index++)
                {
                    dTable.Rows[index]["InspectionDate"] = EthiopicDateTime.TranslateDateMonth(Convert.ToDateTime(dTable.Rows[index]["InspectionDate"]));
                    
                }
            }
            catch (Exception ex)
            {
                throw new Exception("InspReportEntityview::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }
     public DataTable GetRecordIncomplatePromotion(string OrgGuid)
     {
         SqlConnection connection = new SqlConnection(ConnectionString);

         string strGetAllRecords = @"SELECT    ROW_NUMBER() OVER(ORDER BY dbo.Inspection.InspectionDate Desc) as row, dbo.tblPromotionHistory.MainGuid, dbo.tblPromotionHistory.seq_no, dbo.tblPromotionHistory.ParentGuid, dbo.tblPromotionHistory.from_salary, dbo.tblPromotionHistory.to_salary, 
                                  dbo.fnGetjobtitle(dbo.tblPromotionHistory.from_job_title) AS from_job_title, dbo.fnGetjobtitle(dbo.tblPromotionHistory.to_job_title) AS to_job_title, dbo.tblPromotionHistory.date_promoted, 
                                  CASE [promotion_type] WHEN 1 THEN N'የደረጃ' WHEN 2 THEN N'የእርከን' WHEN 4 THEN N'በድልድል/የሥራ ምደባ' WHEN 5 THEN N'በእንደገና ድልድል' WHEN 6 THEN N'የደመወዝ ጭማሪ' END AS promotion_type, 
                                  dbo.tblPromotionHistory.remark, dbo.tblPromotionHistory.from_rank, dbo.tblPromotionHistory.to_rank, dbo.tblPromotionHistory.from_allowance, dbo.tblPromotionHistory.to_allowance, 
                                  dbo.fnGetGrade(dbo.tblPromotionHistory.from_grade) AS from_grade, dbo.fnGetGrade(dbo.tblPromotionHistory.to_grade) AS to_grade, dbo.tblPromotionHistory.RankGuid, 
                                  dbo.tblPromotionHistory.RankGuidFrom, dbo.fnGetPensionDate(dbo.tblPromotionHistory.ParentGuid, dbo.tblPromotionHistory.date_promoted) AS PensionDate, 
                                  dbo.fnGetGradefromposition(dbo.tblPromotionHistory.from_job_title) AS oldGrade, dbo.fnGetGradefromposition(dbo.tblPromotionHistory.to_job_title) AS newGrade, 
                                  dbo.tblPerson.first_am_name + '  ' + dbo.tblPerson.father_am_name + '  ' + dbo.tblPerson.grand_am_name AS FullName, dbo.Inspection.InspectionDate, dbo.Inspection.HRMServiceType, 
                                  dbo.Inspection.InspectionFinding,DATEDIFF(MONTH,dbo.tblPromotionHistory.date_promoted ,GetDate())*(dbo.tblPromotionHistory.to_salary - dbo.tblPromotionHistory.from_salary )as UnnecessaryCost
                                  FROM  dbo.tblPromotionHistory INNER JOIN
                                  dbo.Inspection ON dbo.tblPromotionHistory.ParentGuid = dbo.Inspection.EmployeeGUID INNER JOIN
                                  dbo.tblPerson ON dbo.tblPromotionHistory.ParentGuid = dbo.tblPerson.MainGuid " + OrgGuid + "";

         SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
         DataTable dTable = new DataTable("InspReportEntityview");
         System.Type typeString = System.Type.GetType("System.String");
         dTable.Columns.Add("date_promoted", typeString);
         SqlDataAdapter adapter = new SqlDataAdapter(command);
         command.Connection = connection;

         try
         {
             connection.Open();
             adapter.Fill(dTable);
             for (int index = 0; index < dTable.Rows.Count; index++)
             {
                 dTable.Rows[index]["date_promoted"] = EthiopicDateTime.TranslateDateMonth(Convert.ToDateTime(dTable.Rows[index]["date_promoted"]));
             }
         }
         catch (Exception ex)
         {
             throw new Exception("InspReportEntityview::GetRecords::Error!" + ex.Message, ex);
         }
         finally
         {
             connection.Close();
             command.Dispose();
             adapter.Dispose();
         }
         return dTable;
     }

         public DataTable GetRecordIncomplateTransfer(string OrgGuid)
     {
         SqlConnection connection = new SqlConnection(ConnectionString);

         string strGetAllRecords = @"SELECT    ROW_NUMBER() OVER(ORDER BY dbo.Inspection.InspectionDate Desc) as row, dbo.tblTransferHistory.MainGuid, dbo.tblTransferHistory.seq_no, dbo.tblTransferHistory.ParentGuid, dbo.tblTransferHistory.from_salary, dbo.tblTransferHistory.to_salary, 
                                  dbo.fnGetjobtitle(dbo.tblTransferHistory.from_job_title) AS from_job_title, dbo.fnGetjobtitle(dbo.tblTransferHistory.to_job_title) AS to_job_title, dbo.tblTransferHistory.transfer_date, 
                                  case TransferTypeReal when 0 then N'የውስጥ' when 1 then N'ተጠባባቂ' when 2 then N'እርስ በርስ' when 3 then N'ትውስት' when 4 then N'የፕሮጀክት' when 5 then N'የውጭ' End AS Transfer_type, 
                                  dbo.tblTransferHistory.remark, dbo.tblTransferHistory.from_rank, dbo.tblTransferHistory.to_rank,                                    
                                  dbo.fnGetPensionDate(dbo.tblTransferHistory.ParentGuid, dbo.tblTransferHistory.transfer_date) AS PensionDate, 
                                  dbo.fnGetGradefromposition(dbo.tblTransferHistory.from_job_title) AS oldGrade, dbo.fnGetGradefromposition(dbo.tblTransferHistory.to_job_title) AS newGrade, 
                                  dbo.tblPerson.first_am_name + '  ' + dbo.tblPerson.father_am_name + '  ' + dbo.tblPerson.grand_am_name AS FullName, dbo.Inspection.InspectionDate, dbo.Inspection.HRMServiceType, 
                                  dbo.Inspection.InspectionFinding,[dbo].[fnGetGradefromposition](dbo.tblTransferHistory.from_job_title) As from_grade,[dbo].[fnGetGradefromposition](dbo.tblTransferHistory.to_job_title) As to_grade
                                  FROM         dbo.tblTransferHistory INNER JOIN
                                  dbo.Inspection ON dbo.tblTransferHistory.ParentGuid = dbo.Inspection.EmployeeGUID INNER JOIN
                                  dbo.tblPerson ON dbo.tblTransferHistory.ParentGuid = dbo.tblPerson.MainGuid  " + OrgGuid + "";

         SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
         DataTable dTable = new DataTable("InspReportEntityview");
         System.Type typeString = System.Type.GetType("System.String");
         dTable.Columns.Add("transfer_date", typeString);
         SqlDataAdapter adapter = new SqlDataAdapter(command);
         command.Connection = connection;

         try
         {
             connection.Open();
             adapter.Fill(dTable);
             for (int index = 0; index < dTable.Rows.Count; index++)
             {
                 dTable.Rows[index]["transfer_date"] = EthiopicDateTime.TranslateDateMonth(Convert.ToDateTime(dTable.Rows[index]["transfer_date"]));
             }
         }
         catch (Exception ex)
         {
             throw new Exception("InspReportEntityview::GetRecords::Error!" + ex.Message, ex);
         }
         finally
         {
             connection.Close();
             command.Dispose();
             adapter.Dispose();
         }
         return dTable;
     }
         

        public DataTable GetHrActivitySummery(string OrgGuid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT COUNT(dbo.Inspection.HRMServiceType) AS HRMServiceType, dbo.Inspection.InspectionDecision, dbo.Inspection.HRMServiceType AS HRActivity1, dbo.Organization.DescriptionAm, 
                      dbo.Inspection.Activitytype, dbo.fnGetLookupType(dbo.Inspection.Activitytype) AS HRActivity
                      FROM dbo.tblPerson INNER JOIN
                      dbo.Inspection ON dbo.tblPerson.MainGuid = dbo.Inspection.EmployeeGUID INNER JOIN
                      dbo.tblUnit ON dbo.tblPerson.unit_id = dbo.tblUnit.code INNER JOIN
                      dbo.Organization ON dbo.tblUnit.OrgGuid = dbo.Organization.OrgGuid
                      " + OrgGuid + "  GROUP BY dbo.Inspection.InspectionDecision, dbo.Inspection.HRMServiceType, dbo.Organization.DescriptionAm, dbo.Inspection.Activitytype";
                                       

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("InspReportEntityview");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
               
            }
            catch (Exception ex)
            {
                throw new Exception("InspReportEntityview::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }
      public DataTable GetInspectorHrActivitySummery(string OrgGuid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @" SELECT     COUNT(dbo.Inspection.HRMServiceType) AS HRMServiceType, dbo.Organization.DescriptionAm, dbo.Inspection.Activitytype, dbo.fnGetLookupType(dbo.Inspection.Activitytype) AS HRActivity,dbo.Inspection.UpdatedUsername
                      FROM         dbo.tblPerson INNER JOIN
                      dbo.Inspection ON dbo.tblPerson.MainGuid = dbo.Inspection.EmployeeGUID INNER JOIN
                      dbo.tblUnit ON dbo.tblPerson.unit_id = dbo.tblUnit.code INNER JOIN
                      dbo.Organization ON dbo.tblUnit.OrgGuid = dbo.Organization.OrgGuid
                      " + OrgGuid + " GROUP BY dbo.Organization.DescriptionAm, dbo.Inspection.Activitytype,dbo.Inspection.UpdatedUsername ";
                                       

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("InspReportEntityview"); 
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("InspReportEntityview::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }



        public InspReportEntityview GetRecord(string ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            InspReportEntityview objInspReportEntityview = new InspReportEntityview();
            string strGetRecord = @"SELECT [FullName],[MainGuid],[Gender],[Typeofemployment],[person_id],[staff_code],[staff_code_sort],[cost_center_code],[inspectstatus],[job_title],[id],[org_Code],[org_name],[grade_id],[AmGrade],[description],[InspectionDecision],[InspectionFinding] FROM [dbo].[InspReportEntityview] ";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("InspReportEntityview");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    if (dTable.Rows[0]["FullName"].Equals(DBNull.Value))
                        objInspReportEntityview.FullName = string.Empty;
                    else
                        objInspReportEntityview.FullName = (string)dTable.Rows[0]["FullName"];
                    objInspReportEntityview.MainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    if (dTable.Rows[0]["Gender"].Equals(DBNull.Value))
                        objInspReportEntityview.Gender = string.Empty;
                    else
                        objInspReportEntityview.Gender = (string)dTable.Rows[0]["Gender"];
                    if (dTable.Rows[0]["Typeofemployment"].Equals(DBNull.Value))
                        objInspReportEntityview.Typeofemployment = string.Empty;
                    else
                        objInspReportEntityview.Typeofemployment = (string)dTable.Rows[0]["Typeofemployment"];
                    objInspReportEntityview.Person_id = (int)dTable.Rows[0]["person_id"];
                    if (dTable.Rows[0]["staff_code"].Equals(DBNull.Value))
                        objInspReportEntityview.Staff_code = string.Empty;
                    else
                        objInspReportEntityview.Staff_code = (string)dTable.Rows[0]["staff_code"];
                    if (dTable.Rows[0]["staff_code_sort"].Equals(DBNull.Value))
                        objInspReportEntityview.Staff_code_sort = string.Empty;
                    else
                        objInspReportEntityview.Staff_code_sort = (string)dTable.Rows[0]["staff_code_sort"];
                    if (dTable.Rows[0]["cost_center_code"].Equals(DBNull.Value))
                        objInspReportEntityview.Cost_center_code = string.Empty;
                    else
                        objInspReportEntityview.Cost_center_code = (string)dTable.Rows[0]["cost_center_code"];
                    if (dTable.Rows[0]["inspectstatus"].Equals(DBNull.Value))
                        objInspReportEntityview.Inspectstatus = null;
                    else
                        objInspReportEntityview.Inspectstatus = (string)dTable.Rows[0]["inspectstatus"];
                    if (dTable.Rows[0]["job_title"].Equals(DBNull.Value))
                        objInspReportEntityview.Job_title = string.Empty;
                    else
                        objInspReportEntityview.Job_title = (string)dTable.Rows[0]["job_title"];
                    objInspReportEntityview.Id = (int)dTable.Rows[0]["id"];
                    objInspReportEntityview.Org_Code = (int)dTable.Rows[0]["org_Code"];
                    if (dTable.Rows[0]["org_name"].Equals(DBNull.Value))
                        objInspReportEntityview.Org_name = string.Empty;
                    else
                        objInspReportEntityview.Org_name = (string)dTable.Rows[0]["org_name"];
                    objInspReportEntityview.Grade_id = (int)dTable.Rows[0]["grade_id"];
                    if (dTable.Rows[0]["AmGrade"].Equals(DBNull.Value))
                        objInspReportEntityview.AmGrade = string.Empty;
                    else
                        objInspReportEntityview.AmGrade = (string)dTable.Rows[0]["AmGrade"];
                    if (dTable.Rows[0]["description"].Equals(DBNull.Value))
                        objInspReportEntityview.Description = string.Empty;
                    else
                        objInspReportEntityview.Description = (string)dTable.Rows[0]["description"];
                    if (dTable.Rows[0]["InspectionDecision"].Equals(DBNull.Value))
                        objInspReportEntityview.InspectionDecision = string.Empty;
                    else
                        objInspReportEntityview.InspectionDecision = (string)dTable.Rows[0]["InspectionDecision"];
                    objInspReportEntityview.InspectionFinding = (string)dTable.Rows[0]["InspectionFinding"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("InspReportEntityview::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objInspReportEntityview;
        }

       }
}