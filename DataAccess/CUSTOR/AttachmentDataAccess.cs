﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class AttachmentDataAccess
    {
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public DataTable GetRecords(Guid person, string FileType,string  language)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = "[dbo].[GetAttachmentDisplayValueNew]";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.StoredProcedure };
            DataTable dTable = new DataTable("Attachment");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ParentGuid", person));
                command.Parameters.Add(new SqlParameter("@FileType", FileType));
                command.Parameters.Add(new SqlParameter("@language", language));
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Attachment::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public bool DeleteAttached(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblCompliantAttachment]WHERE [CompliantGuid]=@AttachmentGuid";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@AttachmentGuid", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Attachment::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public Attachment GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            Attachment objAttachment = new Attachment();
            string strGetRecord = @"SELECT [MainGuid],[ParentGuid],[UserName],[AttachmentContent],[AttachmentContent_Sort],[AttachmentContent_Soundx],[SelectedWord],[SelectedWord_Sort],[SelectedWord_Soundx],[Url],[Remark],[FileType],[SentDate],[FileLocation] FROM [dbo].[Attachment] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("Attachment");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objAttachment.MainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    objAttachment.ParentGuid = (Guid)dTable.Rows[0]["ParentGuid"];
                    if (dTable.Rows[0]["UserName"].Equals(DBNull.Value))
                        objAttachment.UserName = string.Empty;
                    else
                        objAttachment.UserName = (string)dTable.Rows[0]["UserName"];
                    if (dTable.Rows[0]["AttachmentContent"].Equals(DBNull.Value))
                        objAttachment.AttachmentContent = string.Empty;
                    else
                        objAttachment.AttachmentContent = (string)dTable.Rows[0]["AttachmentContent"];
                    if (dTable.Rows[0]["AttachmentContent_Sort"].Equals(DBNull.Value))
                        objAttachment.AttachmentContent_Sort = string.Empty;
                    else
                        objAttachment.AttachmentContent_Sort = (string)dTable.Rows[0]["AttachmentContent_Sort"];
                    if (dTable.Rows[0]["AttachmentContent_Soundx"].Equals(DBNull.Value))
                        objAttachment.AttachmentContent_Soundx = string.Empty;
                    else
                        objAttachment.AttachmentContent_Soundx = (string)dTable.Rows[0]["AttachmentContent_Soundx"];
                    if (dTable.Rows[0]["SelectedWord"].Equals(DBNull.Value))
                        objAttachment.SelectedWord = string.Empty;
                    else
                        objAttachment.SelectedWord = (string)dTable.Rows[0]["SelectedWord"];
                    if (dTable.Rows[0]["SelectedWord_Sort"].Equals(DBNull.Value))
                        objAttachment.SelectedWord_Sort = string.Empty;
                    else
                        objAttachment.SelectedWord_Sort = (string)dTable.Rows[0]["SelectedWord_Sort"];
                    if (dTable.Rows[0]["SelectedWord_Soundx"].Equals(DBNull.Value))
                        objAttachment.SelectedWord_Soundx = string.Empty;
                    else
                        objAttachment.SelectedWord_Soundx = (string)dTable.Rows[0]["SelectedWord_Soundx"];
                    if (dTable.Rows[0]["Url"].Equals(DBNull.Value))
                        objAttachment.Url = string.Empty;
                    else
                        objAttachment.Url = (string)dTable.Rows[0]["Url"];
                    if (dTable.Rows[0]["Remark"].Equals(DBNull.Value))
                        objAttachment.Remark = string.Empty;
                    else
                        objAttachment.Remark = (string)dTable.Rows[0]["Remark"];
                    //if (dTable.Rows[0]["FileType"].Equals(DBNull.Value))
                    //    objAttachment.FileType = string.Empty;
                    //else
                    //    objAttachment.FileType = (string)dTable.Rows[0]["FileType"];
                    if (dTable.Rows[0]["SentDate"].Equals(DBNull.Value))
                        objAttachment.SentDate = DateTime.MinValue;
                    else
                        objAttachment.SentDate = (DateTime)dTable.Rows[0]["SentDate"];
                    if (dTable.Rows[0]["FileLocation"].Equals(DBNull.Value))
                        objAttachment.FileLocation = string.Empty;
                    else
                        objAttachment.FileLocation = (string)dTable.Rows[0]["FileLocation"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Attachment::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objAttachment;
        }

        public bool Insert(Attachment objAttachment)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
           
                                  
                string strInsert = @"INSERT INTO [dbo].[Attachment]
                                            ([MainGuid],[ParentGuid],[UserName],[AttachmentContent],[AttachmentContent_Sort],[AttachmentContent_Soundx],[SelectedWord],[SelectedWord_Sort],[SelectedWord_Soundx],[Remark],[FileType],[SentDate],[FileLocation])
                                     VALUES    ( ISNULL(@MainGuid, (newid())),@ParentGuid,@UserName,@AttachmentContent,@AttachmentContent_Sort,@AttachmentContent_Soundx,@SelectedWord,@SelectedWord_Sort,@SelectedWord_Soundx,@Remark,@FileType,@SentDate,@FileLocation)";

                SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
                command.Connection = connection;

                try
                {
                 
                    command.Parameters.Add(new SqlParameter("@MainGuid", objAttachment.MainGuid));
                    command.Parameters.Add(new SqlParameter("@ParentGuid", objAttachment.ParentGuid));
                    command.Parameters.Add(new SqlParameter("@UserName", objAttachment.UserName));
                    command.Parameters.Add(new SqlParameter("@AttachmentContent", objAttachment.AttachmentContent));
                    command.Parameters.Add(new SqlParameter("@AttachmentContent_Sort", objAttachment.AttachmentContent_Sort));
                    command.Parameters.Add(new SqlParameter("@AttachmentContent_Soundx", objAttachment.AttachmentContent_Soundx));
                    command.Parameters.Add(new SqlParameter("@SelectedWord", objAttachment.SelectedWord));
                    command.Parameters.Add(new SqlParameter("@SelectedWord_Sort", objAttachment.SelectedWord_Sort));
                    command.Parameters.Add(new SqlParameter("@SelectedWord_Soundx", objAttachment.SelectedWord_Soundx));
                    command.Parameters.Add(new SqlParameter("@Remark", objAttachment.Remark));
                    command.Parameters.Add(new SqlParameter("@FileType", objAttachment.FileType));
                    command.Parameters.Add(new SqlParameter("@SentDate", objAttachment.SentDate));
                    command.Parameters.Add(new SqlParameter("@FileLocation", objAttachment.FileLocation));
                    
                    connection.Open();
                    int _rowsAffected = command.ExecuteNonQuery();
                    return true;
                    
                }
                catch (Exception ex)
                {
                    throw new Exception("Attachment::Insert::Error!" + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                    command.Dispose();
                }
        
            
        }

        public bool Update(Attachment objAttachment)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[Attachment] SET     [MainGuid]=@MainGuid,    [ParentGuid]=@ParentGuid,    [UserName]=@UserName,    [AttachmentContent]=@AttachmentContent,    [AttachmentContent_Sort]=@AttachmentContent_Sort,    [AttachmentContent_Soundx]=@AttachmentContent_Soundx,    [SelectedWord]=@SelectedWord,    [SelectedWord_Sort]=@SelectedWord_Sort,    [SelectedWord_Soundx]=@SelectedWord_Soundx,    [Remark]=@Remark,    [FileType]=@FileType,    [SentDate]=@SentDate,    [FileLocation]=@FileLocation WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", objAttachment.MainGuid));
                command.Parameters.Add(new SqlParameter("@ParentGuid", objAttachment.ParentGuid));
                command.Parameters.Add(new SqlParameter("@UserName", objAttachment.UserName));
                command.Parameters.Add(new SqlParameter("@AttachmentContent", objAttachment.AttachmentContent));
                command.Parameters.Add(new SqlParameter("@AttachmentContent_Sort", objAttachment.AttachmentContent_Sort));
                command.Parameters.Add(new SqlParameter("@AttachmentContent_Soundx", objAttachment.AttachmentContent_Soundx));
                command.Parameters.Add(new SqlParameter("@SelectedWord", objAttachment.SelectedWord));
                command.Parameters.Add(new SqlParameter("@SelectedWord_Sort", objAttachment.SelectedWord_Sort));
                command.Parameters.Add(new SqlParameter("@SelectedWord_Soundx", objAttachment.SelectedWord_Soundx));
                command.Parameters.Add(new SqlParameter("@Remark", objAttachment.Remark));
                command.Parameters.Add(new SqlParameter("@FileType", objAttachment.FileType));
                command.Parameters.Add(new SqlParameter("@SentDate", objAttachment.SentDate));
                command.Parameters.Add(new SqlParameter("@FileLocation", objAttachment.FileLocation));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Attachment::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[Attachment] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Attachment::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public List<Attachment> GetList()
        {
            List<Attachment> RecordsList = new List<Attachment>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [MainGuid],[ParentGuid],[UserName],[AttachmentContent],[AttachmentContent_Sort],[AttachmentContent_Soundx],[SelectedWord],[SelectedWord_Sort],[SelectedWord_Soundx],[Remark],[FileType],[SentDate],[FileLocation] FROM [dbo].[Attachment]  ORDER BY  [MainGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    Attachment objAttachment = new Attachment();
                    if (dr["MainGuid"].Equals(DBNull.Value))
                        objAttachment.MainGuid = Guid.Empty;
                    else
                        objAttachment.MainGuid = (Guid)dr["MainGuid"];
                    if (dr["ParentGuid"].Equals(DBNull.Value))
                        objAttachment.ParentGuid = Guid.Empty;
                    else
                        objAttachment.ParentGuid = (Guid)dr["ParentGuid"];
                    if (dr["UserName"].Equals(DBNull.Value))
                        objAttachment.UserName = string.Empty;
                    else
                        objAttachment.UserName = (string)dr["UserName"];
                    if (dr["AttachmentContent"].Equals(DBNull.Value))
                        objAttachment.AttachmentContent = string.Empty;
                    else
                        objAttachment.AttachmentContent = (string)dr["AttachmentContent"];
                    if (dr["AttachmentContent_Sort"].Equals(DBNull.Value))
                        objAttachment.AttachmentContent_Sort = string.Empty;
                    else
                        objAttachment.AttachmentContent_Sort = (string)dr["AttachmentContent_Sort"];
                    if (dr["AttachmentContent_Soundx"].Equals(DBNull.Value))
                        objAttachment.AttachmentContent_Soundx = string.Empty;
                    else
                        objAttachment.AttachmentContent_Soundx = (string)dr["AttachmentContent_Soundx"];
                    if (dr["SelectedWord"].Equals(DBNull.Value))
                        objAttachment.SelectedWord = string.Empty;
                    else
                        objAttachment.SelectedWord = (string)dr["SelectedWord"];
                    if (dr["SelectedWord_Sort"].Equals(DBNull.Value))
                        objAttachment.SelectedWord_Sort = string.Empty;
                    else
                        objAttachment.SelectedWord_Sort = (string)dr["SelectedWord_Sort"];
                    if (dr["SelectedWord_Soundx"].Equals(DBNull.Value))
                        objAttachment.SelectedWord_Soundx = string.Empty;
                    else
                        objAttachment.SelectedWord_Soundx = (string)dr["SelectedWord_Soundx"];
                    if (dr["Remark"].Equals(DBNull.Value))
                        objAttachment.Remark = string.Empty;
                    else
                        objAttachment.Remark = (string)dr["Remark"];
                    if (dr["FileType"].Equals(DBNull.Value))
                        objAttachment.FileType = string.Empty;
                    else
                        objAttachment.FileType = (string)dr["FileType"];
                    if (dr["SentDate"].Equals(DBNull.Value))
                        objAttachment.SentDate = DateTime.MinValue;
                    else
                        objAttachment.SentDate = (DateTime)dr["SentDate"];
                    if (dr["FileLocation"].Equals(DBNull.Value))
                        objAttachment.FileLocation = string.Empty;
                    else
                        objAttachment.FileLocation = (string)dr["FileLocation"];
                    RecordsList.Add(objAttachment);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Attachment::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public bool Exists(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT MainGuid FROM [dbo].[Attachment] WHERE [MainGuid]=@MainGuid";

            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@MainGuid", ID));
                return ((int)command.ExecuteScalar() > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("Attachment::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }
    }
}