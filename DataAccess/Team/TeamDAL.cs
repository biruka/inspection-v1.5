﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblTeamDataAccess
    {

        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [TeamGuid],[TeamNameAm],[TeamName],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime] FROM [dbo].[tblInspectionTeam]  ORDER BY  [TeamGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblInspectionTeam");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionTeam::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public tblTeam GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblTeam objtblTeam = new tblTeam();
            string strGetRecord = @"SELECT [TeamGuid],[TeamNameAm],[TeamName],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime] FROM [dbo].[tblInspectionTeam] WHERE [TeamGuid]=@TeamGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblInspectionTeam");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@TeamGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblTeam.TeamGuid = (Guid)dTable.Rows[0]["TeamGuid"];
                    if (dTable.Rows[0]["TeamNameAm"].Equals(DBNull.Value))
                        objtblTeam.TeamNameAm = string.Empty;
                    else
                        objtblTeam.TeamNameAm = (string)dTable.Rows[0]["TeamNameAm"];
                    if (dTable.Rows[0]["TeamName"].Equals(DBNull.Value))
                        objtblTeam.TeamName = string.Empty;
                    else
                        objtblTeam.TeamName = (string)dTable.Rows[0]["TeamName"];
                    if (dTable.Rows[0]["UserName"].Equals(DBNull.Value))
                        objtblTeam.UserName = string.Empty;
                    else
                        objtblTeam.UserName = (string)dTable.Rows[0]["UserName"];
                    if (dTable.Rows[0]["EventDatetime"].Equals(DBNull.Value))
                        objtblTeam.EventDatetime = DateTime.MinValue;
                    else
                        objtblTeam.EventDatetime = (DateTime)dTable.Rows[0]["EventDatetime"];
                    if (dTable.Rows[0]["UpdatedUsername"].Equals(DBNull.Value))
                        objtblTeam.UpdatedUsername = string.Empty;
                    else
                        objtblTeam.UpdatedUsername = (string)dTable.Rows[0]["UpdatedUsername"];
                    if (dTable.Rows[0]["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objtblTeam.UpdatedEventDatetime = DateTime.MinValue;
                    else
                        objtblTeam.UpdatedEventDatetime = (DateTime)dTable.Rows[0]["UpdatedEventDatetime"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionTeam::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblTeam;
        }

        public bool Insert(tblTeam objtblTeam)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[tblInspectionTeam]
                                            ([TeamNameAm],[TeamName],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime])
                                     VALUES    (@TeamNameAm,@TeamName,@UserName,@EventDatetime,@UpdatedUsername,@UpdatedEventDatetime)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                //command.Parameters.Add(new SqlParameter("@TeamGuid", objtblTeam.TeamGuid));
                command.Parameters.Add(new SqlParameter("@TeamNameAm", objtblTeam.TeamNameAm));
                command.Parameters.Add(new SqlParameter("@TeamName", objtblTeam.TeamName));
                command.Parameters.Add(new SqlParameter("@UserName", objtblTeam.UserName));
                command.Parameters.Add(new SqlParameter("@EventDatetime", objtblTeam.EventDatetime));
                command.Parameters.Add(new SqlParameter("@UpdatedUsername", objtblTeam.UpdatedUsername));
                command.Parameters.Add(new SqlParameter("@UpdatedEventDatetime", objtblTeam.UpdatedEventDatetime));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionTeam::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(tblTeam objtblTeam)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[tblInspectionTeam] SET     [TeamGuid]=@TeamGuid,    [TeamNameAm]=@TeamNameAm,    [TeamName]=@TeamName,    [UserName]=@UserName,    [EventDatetime]=@EventDatetime,    [UpdatedUsername]=@UpdatedUsername,    [UpdatedEventDatetime]=@UpdatedEventDatetime WHERE [TeamGuid]=@TeamGuid";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@TeamGuid", objtblTeam.TeamGuid));
                command.Parameters.Add(new SqlParameter("@TeamNameAm", objtblTeam.TeamNameAm));
                command.Parameters.Add(new SqlParameter("@TeamName", objtblTeam.TeamName));
                command.Parameters.Add(new SqlParameter("@UserName", objtblTeam.UserName));
                command.Parameters.Add(new SqlParameter("@EventDatetime", objtblTeam.EventDatetime));
                command.Parameters.Add(new SqlParameter("@UpdatedUsername", objtblTeam.UpdatedUsername));
                command.Parameters.Add(new SqlParameter("@UpdatedEventDatetime", objtblTeam.UpdatedEventDatetime));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionTeam::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblInspectionTeam] WHERE [TeamGuid]=@TeamGuid";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@TeamGuid", ID));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionTeam::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public List<tblTeam> GetList()
        {
            List<tblTeam> RecordsList = new List<tblTeam>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;

            string strGetAllRecords = @"SELECT [TeamGuid],[TeamNameAm],[TeamName],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime] FROM [dbo].[tblInspectionTeam]  ORDER BY  [TeamGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    tblTeam objtblTeam = new tblTeam();
                    if (dr["TeamGuid"].Equals(DBNull.Value))
                        objtblTeam.TeamGuid = Guid.Empty;
                    else
                        objtblTeam.TeamGuid = (Guid)dr["TeamGuid"];
                    if (dr["TeamNameAm"].Equals(DBNull.Value))
                        objtblTeam.TeamNameAm = string.Empty;
                    else
                        objtblTeam.TeamNameAm = (string)dr["TeamNameAm"];
                    if (dr["TeamName"].Equals(DBNull.Value))
                        objtblTeam.TeamName = string.Empty;
                    else
                        objtblTeam.TeamName = (string)dr["TeamName"];
                    if (dr["UserName"].Equals(DBNull.Value))
                        objtblTeam.UserName = string.Empty;
                    else
                        objtblTeam.UserName = (string)dr["UserName"];
                    if (dr["EventDatetime"].Equals(DBNull.Value))
                        objtblTeam.EventDatetime = DateTime.MinValue;
                    else
                        objtblTeam.EventDatetime = (DateTime)dr["EventDatetime"];
                    if (dr["UpdatedUsername"].Equals(DBNull.Value))
                        objtblTeam.UpdatedUsername = string.Empty;
                    else
                        objtblTeam.UpdatedUsername = (string)dr["UpdatedUsername"];
                    if (dr["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objtblTeam.UpdatedEventDatetime = DateTime.MinValue;
                    else
                        objtblTeam.UpdatedEventDatetime = (DateTime)dr["UpdatedEventDatetime"];
                    RecordsList.Add(objtblTeam);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionTeam::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public bool CheckTeamExistance(string teamNameAmh, string teamName)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT TeamGuid FROM [dbo].[tblInspectionTeam] WHERE TeamName='" + teamName + "'";
            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable dTable = new DataTable("tblInspectionTeam");
            try
            {
                connection.Open();
                int rowsAffected = command.ExecuteNonQuery();
                adapter.Fill(dTable);
                return (dTable.Rows.Count > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionTeam::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

    }
}
