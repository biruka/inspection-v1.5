﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;

using CUSTOR.Domain;

namespace CUSTOR.DataAccess
{
    public class tblTeamMembersDataAccess
    {
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strGetAllRecords = @"SELECT [TeamMembersGuid],[TeamGuid],[PersonGuid],[PositionInTeam],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime] FROM [dbo].[tblInspectionTeamMembers]  ORDER BY  [TeamMembersGuid] ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblInspectionTeamMembers");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionTeamMembers::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }

        public tblTeamMembers GetRecord(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            tblTeamMembers objtblTeamMembers = new tblTeamMembers();
            string strGetRecord = @"SELECT [TeamMembersGuid],[TeamGuid],[PersonGuid],[PositionInTeam],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime] FROM [dbo].[tblInspectionTeamMembers] WHERE [TeamMembersGuid]=@TeamMembersGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblInspectionTeamMembers");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@TeamMembersGuid", ID));
                connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    objtblTeamMembers.TeamMembersGuid = (Guid)dTable.Rows[0]["TeamMembersGuid"];
                    if (dTable.Rows[0]["TeamGuid"].Equals(DBNull.Value))
                        objtblTeamMembers.TeamGuid = Guid.Empty;
                    else
                        objtblTeamMembers.TeamGuid = (Guid)dTable.Rows[0]["TeamGuid"];
                    if (dTable.Rows[0]["PersonGuid"].Equals(DBNull.Value))
                        objtblTeamMembers.PersonGuid = Guid.Empty;
                    else
                        objtblTeamMembers.PersonGuid = (Guid)dTable.Rows[0]["PersonGuid"];
                    if (dTable.Rows[0]["PositionInTeam"].Equals(DBNull.Value))
                        objtblTeamMembers.PositionInTeam = string.Empty;
                    else
                        objtblTeamMembers.PositionInTeam = (string)dTable.Rows[0]["PositionInTeam"];
                    if (dTable.Rows[0]["UserName"].Equals(DBNull.Value))
                        objtblTeamMembers.UserName = string.Empty;
                    else
                        objtblTeamMembers.UserName = (string)dTable.Rows[0]["UserName"];
                    if (dTable.Rows[0]["EventDatetime"].Equals(DBNull.Value))
                        objtblTeamMembers.EventDatetime = DateTime.MinValue;
                    else
                        objtblTeamMembers.EventDatetime = (DateTime)dTable.Rows[0]["EventDatetime"];
                    if (dTable.Rows[0]["UpdatedUsername"].Equals(DBNull.Value))
                        objtblTeamMembers.UpdatedUsername = string.Empty;
                    else
                        objtblTeamMembers.UpdatedUsername = (string)dTable.Rows[0]["UpdatedUsername"];
                    if (dTable.Rows[0]["UpdatedEventDatetime"].Equals(DBNull.Value))
                        objtblTeamMembers.UpdatedEventDatetime = DateTime.MinValue;
                    else
                        objtblTeamMembers.UpdatedEventDatetime = (DateTime)dTable.Rows[0]["UpdatedEventDatetime"];
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionTeamMembers::GetRecord::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return objtblTeamMembers;
        }

        public List<tblTeamMembers> GetRecordByPersonGuid(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            List<tblTeamMembers> RecordsList = new List<tblTeamMembers>();
            string strGetRecord = @"SELECT [TeamMembersGuid],[TeamGuid],[PersonGuid],[PositionInTeam],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime] FROM [dbo].[tblInspectionTeamMembers] WHERE [PersonGuid]=@personGuid";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblInspectionTeamMembers");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;
            SqlDataReader dr = null;
            tblTeamMembers objtblTeamMembers = null;
            try
            {
                command.Parameters.Add(new SqlParameter("@personGuid", ID));
                connection.Open();
                dr = command.ExecuteReader();

                while (dr.Read())
                {
                    objtblTeamMembers = new tblTeamMembers();
                    if (dr["TeamMembersGuid"].Equals(DBNull.Value))
                        objtblTeamMembers.TeamMembersGuid = Guid.Empty;
                    else
                        objtblTeamMembers.TeamMembersGuid = (Guid)dr["TeamMembersGuid"];

                    if (dr["PositionInTeam"].Equals(DBNull.Value))
                        objtblTeamMembers.PositionInTeam = string.Empty;
                    else
                        objtblTeamMembers.PositionInTeam = (string)dr["PositionInTeam"];


                    RecordsList.Add(objtblTeamMembers);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionTeamMembers::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public List<tblTeamMembers> GetRecordByTeamGuid(Guid ID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            List<tblTeamMembers> RecordsList = new List<tblTeamMembers>();
            string strGetRecord = @"SELECT [TeamMembersGuid],[TeamGuid],[PersonGuid],[PositionInTeam],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime] FROM [dbo].[tblInspectionTeamMembers] WHERE [TeamGuid]='" + ID + "'";

            SqlCommand command = new SqlCommand() { CommandText = strGetRecord, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblInspectionTeamMembers");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;
            SqlDataReader dr = null;
            tblTeamMembers objtblTeamMembers = null;
            try
            {
                connection.Open();
                dr = command.ExecuteReader();

                while (dr.Read())
                {
                    objtblTeamMembers = new tblTeamMembers();
                    if (dr["TeamMembersGuid"].Equals(DBNull.Value))
                        objtblTeamMembers.TeamMembersGuid = Guid.Empty;
                    else
                        objtblTeamMembers.TeamMembersGuid = (Guid)dr["TeamMembersGuid"];

                    RecordsList.Add(objtblTeamMembers);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionTeamMembers::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public bool Insert(tblTeamMembers objtblTeamMembers)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strInsert = @"INSERT INTO [dbo].[tblInspectionTeamMembers]
                                            ([TeamMembersGuid],[TeamGuid],[PersonGuid],[PositionInTeam],[UserName],[EventDatetime],[UpdatedUsername],[UpdatedEventDatetime])
                                     VALUES    (@TeamMembersGuid,@TeamGuid,@PersonGuid,@PositionInTeam,@UserName,@EventDatetime,@UpdatedUsername,@UpdatedEventDatetime)";

            SqlCommand command = new SqlCommand() { CommandText = strInsert, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@TeamMembersGuid", objtblTeamMembers.TeamMembersGuid));
                command.Parameters.Add(new SqlParameter("@TeamGuid", objtblTeamMembers.TeamGuid));
                command.Parameters.Add(new SqlParameter("@PersonGuid", objtblTeamMembers.PersonGuid));
                command.Parameters.Add(new SqlParameter("@PositionInTeam", objtblTeamMembers.PositionInTeam));
                command.Parameters.Add(new SqlParameter("@UserName", objtblTeamMembers.UserName));
                command.Parameters.Add(new SqlParameter("@EventDatetime", objtblTeamMembers.EventDatetime));
                command.Parameters.Add(new SqlParameter("@UpdatedUsername", objtblTeamMembers.UpdatedUsername));
                command.Parameters.Add(new SqlParameter("@UpdatedEventDatetime", objtblTeamMembers.UpdatedEventDatetime));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionTeamMembers::Insert::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Update(tblTeamMembers objtblTeamMembers)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strUpdate = @"UPDATE [dbo].[tblInspectionTeamMembers] SET     [TeamMembersGuid]=@TeamMembersGuid,    [TeamGuid]=@TeamGuid,    [PersonGuid]=@PersonGuid,    [PositionInTeam]=@PositionInTeam,    [UserName]=@UserName,    [EventDatetime]=@EventDatetime,    [UpdatedUsername]=@UpdatedUsername,    [UpdatedEventDatetime]=@UpdatedEventDatetime WHERE [TeamMembersGuid]=@TeamMembersGuid";

            SqlCommand command = new SqlCommand() { CommandText = strUpdate, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@TeamMembersGuid", objtblTeamMembers.TeamMembersGuid));
                command.Parameters.Add(new SqlParameter("@TeamGuid", objtblTeamMembers.TeamGuid));
                command.Parameters.Add(new SqlParameter("@PersonGuid", objtblTeamMembers.PersonGuid));
                command.Parameters.Add(new SqlParameter("@PositionInTeam", objtblTeamMembers.PositionInTeam));
                command.Parameters.Add(new SqlParameter("@UserName", objtblTeamMembers.UserName));
                command.Parameters.Add(new SqlParameter("@EventDatetime", objtblTeamMembers.EventDatetime));
                command.Parameters.Add(new SqlParameter("@UpdatedUsername", objtblTeamMembers.UpdatedUsername));
                command.Parameters.Add(new SqlParameter("@UpdatedEventDatetime", objtblTeamMembers.UpdatedEventDatetime));


                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionTeamMembers::Update::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public bool Delete(Guid teamMemberGuid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strDelete = @"DELETE FROM [dbo].[tblInspectionTeamMembers] WHERE [TeamMembersGuid]=@teamMemberGuid";

            SqlCommand command = new SqlCommand() { CommandText = strDelete, CommandType = CommandType.Text };
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@teamMemberGuid", teamMemberGuid));

                connection.Open();
                int _rowsAffected = command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionTeamMembers::Delete::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public List<tblTeamMembers> GetList(Guid teamGuid)
        {
            List<tblTeamMembers> RecordsList = new List<tblTeamMembers>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
            int SNo = 0;

            string strGetAllRecords = @"SELECT  dbo.tblInspectionTeamMembers.TeamMembersGuid,dbo.tblInspectionTeamMembers.personGuid,
		                            dbo.tblPerson.first_am_name+ ' '+ dbo.tblPerson.father_am_name+ ' '+ dbo.tblPerson.grand_am_name as FullName,
                                                  dbo.tblInspectionTeamMembers.PositionInTeam
                            FROM         dbo.tblPerson INNER JOIN
                                                  dbo.tblInspectionTeamMembers ON dbo.tblPerson.MainGuid = dbo.tblInspectionTeamMembers.PersonGuid where tblInspectionTeamMembers.teamGuid='" + teamGuid + "'";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Parameters.Add(new SqlParameter("@TeamGuid", teamGuid));
            command.Connection = connection;
            tblTeamMembers objtblTeamMembers = null;
            try
            {
                connection.Open();
                dr = command.ExecuteReader();
               
                while (dr.Read())
                {
                    objtblTeamMembers =  new tblTeamMembers();
                    if (dr["TeamMembersGuid"].Equals(DBNull.Value))
                        objtblTeamMembers.TeamMembersGuid = Guid.Empty;
                    else
                        objtblTeamMembers.TeamMembersGuid = (Guid)dr["TeamMembersGuid"];
                    if (dr["PersonGuid"].Equals(DBNull.Value))
                        objtblTeamMembers.PersonGuid = Guid.Empty;
                    else
                        objtblTeamMembers.PersonGuid = (Guid)dr["PersonGuid"];
            
                
                    if (dr["PositionInTeam"].Equals(DBNull.Value))
                        objtblTeamMembers.PositionInTeam = string.Empty;
                    else
                        objtblTeamMembers.PositionInTeam = (string)dr["PositionInTeam"];

                    if (dr["FullName"].Equals(DBNull.Value))
                        objtblTeamMembers.FullName = string.Empty;
                    else
                        objtblTeamMembers.FullName = (string)dr["FullName"];
                    objtblTeamMembers.SNo = ++SNo;

               
                    RecordsList.Add(objtblTeamMembers);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionTeamMembers::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return RecordsList;
        }

        public bool DoesEmployeeTeamLeader(Guid personGuid, Guid organizationGuid)
        {
            List<tblTeamMembers> RecordsList = new List<tblTeamMembers>();
            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlDataReader dr = null;
            bool hasRecord;
            string strGetAllRecords = @"select tblInspectionTeamMembers.* from tblInspectionTeam inner join tblInspectionTeamMembers on  
			                            tblInspectionTeamMembers.TeamGuid = tblInspectionTeam.TeamGuid inner join tblTeamInOrganization on
			                            tblInspectionTeam.TeamGuid = tblTeamInOrganization.TeamGuid where tblInspectionTeamMembers.personGuid='" + personGuid + @"' And 
                                        tblTeamInOrganization.OrgGuid='" + organizationGuid + "' And tblInspectionTeamMembers.PositionInTeam ='1'";
            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            command.Connection = connection;
            tblTeamMembers objtblTeamMembers = null;
            try
            {
                connection.Open();
                dr = command.ExecuteReader();
                hasRecord =dr.HasRows;
            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionTeamMembers::GetList::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                dr.Close();
                command.Dispose();
            }
            return hasRecord;
        }

        public bool Exists(Guid teamGuid, Guid personGuid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            string strExists = @"SELECT TeamMembersGuid FROM [dbo].[tblInspectionTeamMembers] WHERE [PersonGuid]=@personGuid and [TeamGuid]=@TeamGuid";
            SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
            command.Connection = connection;
            SqlDataAdapter adpater = new SqlDataAdapter(command);
            DataTable dTable = new DataTable();
            try
            {
                command.Parameters.Add(new SqlParameter("@personGuid", personGuid));
                command.Parameters.Add(new SqlParameter("@teamGuid", teamGuid)); 
                adpater.Fill(dTable);

                return (dTable.Rows.Count > 0);
            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionTeamMembers::Exists::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
            }
        }

        public DataTable GetUnGroupedMembers( int unitID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            //አባል
            string strGetAllRecords = @"select tblPerson.MainGuid,[first_am_name]+' '+ [father_am_name] +' '+ [grand_am_name] as FullName,first_name_sort  from tblPerson
                                        where tblPerson.MainGuid not in ( select tblInspectionTeamMembers.PersonGuid from tblInspectionTeamMembers)
                                        and  unit_id=" + unitID + "AND current_status = 1195  ORDER BY first_name_sort ASC";

            SqlCommand command = new SqlCommand() { CommandText = strGetAllRecords, CommandType = CommandType.Text };
            DataTable dTable = new DataTable("tblPersons");
            SqlDataAdapter adapter = new SqlDataAdapter( command);

            command.Connection = connection;

            try
            {
                connection.Open();
                adapter.Fill(dTable);
            }
            catch (Exception ex)
            {
                throw new Exception("tblInspectionTeamMembers::GetRecords::Error!" + ex.Message, ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
            return dTable;
        }
    }
}
