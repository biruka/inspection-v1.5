﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;
using System.ComponentModel;

namespace CUSTOR.Bussiness
{
     [DataObjectAttribute]
    public class tblTrainingHistoryBussiness
    {


        public bool Delete(Guid ID)
        {
            tblTrainingHistoryDataAccess objtblTrainingHistoryDataAccess = new tblTrainingHistoryDataAccess();
            try
            {
                objtblTrainingHistoryDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
       [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecords(Guid person, string language)
        {
            tblTrainingHistoryDataAccess objtblTrainingHistoryDataAccess = new tblTrainingHistoryDataAccess();

            try
            {
                return objtblTrainingHistoryDataAccess.GetRecords(person, language);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public tblTrainingHistory GettblTrainingHistory(Guid ID)
        {
            tblTrainingHistoryDataAccess objtblTrainingHistoryDataAccess = new tblTrainingHistoryDataAccess();

            try
            {
                return objtblTrainingHistoryDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<tblTrainingHistory> GettblTrainingHistorys()
        {
            tblTrainingHistoryDataAccess objtblTrainingHistoryDataAccess = new tblTrainingHistoryDataAccess();

            try
            {
                return objtblTrainingHistoryDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(Guid ID)
        {
            tblTrainingHistoryDataAccess objtblTrainingHistoryDataAccess = new tblTrainingHistoryDataAccess();
            try
            {
                return objtblTrainingHistoryDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdatetblTrainingHistory(tblTrainingHistory objtblTrainingHistory)
        {
            tblTrainingHistoryDataAccess objtblTrainingHistoryDataAccess = new tblTrainingHistoryDataAccess();
            try
            {
                objtblTrainingHistoryDataAccess.Update(objtblTrainingHistory);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InserttblTrainingHistory(tblTrainingHistory objtblTrainingHistory)
        {
            tblTrainingHistoryDataAccess objtblTrainingHistoryDataAccess = new tblTrainingHistoryDataAccess();
            try
            {
                objtblTrainingHistoryDataAccess.Insert(objtblTrainingHistory);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}