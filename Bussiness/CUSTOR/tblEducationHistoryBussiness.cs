﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;
using System.ComponentModel;

namespace CUSTOR.Bussiness
{
     [DataObjectAttribute]
    public class tblEducationHistoryBussiness
    {


        
       [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecords(Guid person, string language)
        {
            tblEducationHistoryDataAccess objtblEducationHistoryDataAccess = new tblEducationHistoryDataAccess();

            try
            {
                return objtblEducationHistoryDataAccess.GetRecords(person, language);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public tblEducationHistory GettblEducationHistory(Guid ID)
        {
            tblEducationHistoryDataAccess objtblEducationHistoryDataAccess = new tblEducationHistoryDataAccess();

            try
            {
                return objtblEducationHistoryDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<tblEducationHistory> GettblEducationHistorys()
        {
            tblEducationHistoryDataAccess objtblEducationHistoryDataAccess = new tblEducationHistoryDataAccess();

            try
            {
                return objtblEducationHistoryDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        


    }
}