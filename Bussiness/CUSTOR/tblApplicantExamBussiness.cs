﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;
using System.ComponentModel;

namespace CUSTOR.Bussiness
{
    [DataObjectAttribute]
    public class tblApplicantExamBussiness
    {


        
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecords(Guid person)
        {
            tblApplicantExamDataAccess objtblApplicantExamDataAccess = new tblApplicantExamDataAccess();

            try
            {
                return objtblApplicantExamDataAccess.GetRecords(person);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public tblApplicantExam GettblApplicantExam(Guid ID)
        {
            tblApplicantExamDataAccess objtblApplicantExamDataAccess = new tblApplicantExamDataAccess();

            try
            {
                return objtblApplicantExamDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<tblApplicantExam> GettblApplicantExams()
        {
            tblApplicantExamDataAccess objtblApplicantExamDataAccess = new tblApplicantExamDataAccess();

            try
            {
                return objtblApplicantExamDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(Guid ID)
        {
            tblApplicantExamDataAccess objtblApplicantExamDataAccess = new tblApplicantExamDataAccess();
            try
            {
                return objtblApplicantExamDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        

    }
}