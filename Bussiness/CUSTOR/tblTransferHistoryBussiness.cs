﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;
using System.ComponentModel;

namespace CUSTOR.Bussiness
{
    [DataObjectAttribute]
    public class tblTransferHistoryBussiness
    {


       
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecords(Guid person, int language)
        {
            tblTransferHistoryDataAccess objtblTransferHistoryDataAccess = new tblTransferHistoryDataAccess();

            try
            {
                return objtblTransferHistoryDataAccess.GetRecords(person, language);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public tblTransferHistory GettblTransferHistory(Guid ID)
        {
            tblTransferHistoryDataAccess objtblTransferHistoryDataAccess = new tblTransferHistoryDataAccess();

            try
            {
                return objtblTransferHistoryDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<tblTransferHistory> GettblTransferHistorys()
        {
            tblTransferHistoryDataAccess objtblTransferHistoryDataAccess = new tblTransferHistoryDataAccess();

            try
            {
                return objtblTransferHistoryDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
              

    }
}