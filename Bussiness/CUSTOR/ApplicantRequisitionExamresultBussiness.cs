﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;
using System.ComponentModel;

namespace CUSTOR.Bussiness
{
    [DataObjectAttribute]
    public class ApplicantRequisitionExamresultBussiness
    {


       
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecords(Guid person)
        {
            ApplicantRequisitionExamresultDataAccess objApplicantRequisitionExamresultDataAccess = new ApplicantRequisitionExamresultDataAccess();

            try
            {
                return objApplicantRequisitionExamresultDataAccess.GetRecords(person);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecordRequisition(Guid Requisition)
        {
            ApplicantRequisitionExamresultDataAccess objApplicantRequisitionExamresultDataAccess = new ApplicantRequisitionExamresultDataAccess();

            try
            {
                return objApplicantRequisitionExamresultDataAccess.GetRecordRequisition(Requisition);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public ApplicantRequisitionExamresult GetApplicantRequisitionExamresult(string ID)
        {
            ApplicantRequisitionExamresultDataAccess objApplicantRequisitionExamresultDataAccess = new ApplicantRequisitionExamresultDataAccess();

            try
            {
                return objApplicantRequisitionExamresultDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<ApplicantRequisitionExamresult> GetApplicantRequisitionExamresults()
        {
            ApplicantRequisitionExamresultDataAccess objApplicantRequisitionExamresultDataAccess = new ApplicantRequisitionExamresultDataAccess();

            try
            {
                return objApplicantRequisitionExamresultDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        

    }
}