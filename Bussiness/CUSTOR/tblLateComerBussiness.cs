﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;

namespace CUSTOR.Bussiness
{
    public class tblLateComerBussiness
    {
       
        public tblLateComer GettblLateComer(Guid ID)
        {
            tblLateComerDataAccess objtblLateComerDataAccess = new tblLateComerDataAccess();

            try
            {
                return objtblLateComerDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<tblLateComer> GettblLateComers()
        {
            tblLateComerDataAccess objtblLateComerDataAccess = new tblLateComerDataAccess();

            try
            {
                return objtblLateComerDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<tblLateComer> GetListLateCome(Guid ParentID)
        {
            tblLateComerDataAccess objtblLateComerDataAccess = new tblLateComerDataAccess();

            try
            {
                return objtblLateComerDataAccess.GetListLateCome(ParentID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(Guid ID)
        {
            tblLateComerDataAccess objtblLateComerDataAccess = new tblLateComerDataAccess();
            try
            {
                return objtblLateComerDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        
    }
}