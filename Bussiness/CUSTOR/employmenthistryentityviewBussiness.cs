﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;
using System.ComponentModel;

namespace CUSTOR.Bussiness
{
    [DataObjectAttribute]
    public class employmenthistryentityviewBussiness
    {


        
         [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecords(Guid person, string language)
        {
            employmenthistryentityviewDataAccess objemploymenthistryentityviewDataAccess = new employmenthistryentityviewDataAccess();

            try
            {
                return objemploymenthistryentityviewDataAccess.GetRecords(person, language);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public employmenthistryentityview Getemploymenthistryentityview(string ID)
        {
            employmenthistryentityviewDataAccess objemploymenthistryentityviewDataAccess = new employmenthistryentityviewDataAccess();

            try
            {
                return objemploymenthistryentityviewDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<employmenthistryentityview> Getemploymenthistryentityviews()
        {
            employmenthistryentityviewDataAccess objemploymenthistryentityviewDataAccess = new employmenthistryentityviewDataAccess();

            try
            {
                return objemploymenthistryentityviewDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
       

    }
}