﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;

namespace CUSTOR.Bussiness
{
    public class tblDocumentDetailBussiness
    {


        public bool Delete(Guid ID)
        {
            tblDocumentDetailDataAccess objtblDocumentDetailDataAccess = new tblDocumentDetailDataAccess();
            try
            {
                objtblDocumentDetailDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool DeleteDocument(Guid ID)
        {
            tblDocumentDetailDataAccess objtblDocumentDetailDataAccess = new tblDocumentDetailDataAccess();
            try
            {
                objtblDocumentDetailDataAccess.DeleteDocument(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataTable GetDocRecord(Guid ID)
        {
            tblDocumentDetailDataAccess objtblDocumentDetailDataAccess = new tblDocumentDetailDataAccess();

            try
            {
                return objtblDocumentDetailDataAccess.GetDocRecords(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public tblDocumentDetail GettblDocumentDetail(Guid ID)
        {
            tblDocumentDetailDataAccess objtblDocumentDetailDataAccess = new tblDocumentDetailDataAccess();

            try
            {
                return objtblDocumentDetailDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<tblDocumentDetail> GettblDocumentDetails()
        {
            tblDocumentDetailDataAccess objtblDocumentDetailDataAccess = new tblDocumentDetailDataAccess();

            try
            {
                return objtblDocumentDetailDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(Guid ID)
        {
            tblDocumentDetailDataAccess objtblDocumentDetailDataAccess = new tblDocumentDetailDataAccess();
            try
            {
                return objtblDocumentDetailDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdatetblDocumentDetail(tblDocumentDetail objtblDocumentDetail)
        {
            tblDocumentDetailDataAccess objtblDocumentDetailDataAccess = new tblDocumentDetailDataAccess();
            try
            {
                objtblDocumentDetailDataAccess.Update(objtblDocumentDetail);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InserttblDocumentDetail(tblDocumentDetail objtblDocumentDetail)
        {
            tblDocumentDetailDataAccess objtblDocumentDetailDataAccess = new tblDocumentDetailDataAccess();
            try
            {
                objtblDocumentDetailDataAccess.Insert(objtblDocumentDetail);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}