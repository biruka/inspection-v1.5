﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;
using System.ComponentModel;
namespace CUSTOR.Bussiness
{
     [DataObjectAttribute]
    public class CompliantAttachmentBusiness
    {
        public bool Delete(string ID)
        {
            CompliantAttachmentDataAccess objCompliantAttachmentDataAccess = new CompliantAttachmentDataAccess();
            try
            {
                objCompliantAttachmentDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool DeleteAttached(Guid ID)
        {
            CompliantAttachmentDataAccess objCompliantAttachmentDataAccess = new CompliantAttachmentDataAccess();
            try
            {
                objCompliantAttachmentDataAccess.DeleteAttached(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public CompliantAttachment GetAttachment(Guid ID)
        {
            CompliantAttachmentDataAccess objCompliantAttachmentDataAccess = new CompliantAttachmentDataAccess();

            try
            {
                return objCompliantAttachmentDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetAttachmentByCompliantId(Guid ID)
        {
            CompliantAttachmentDataAccess objCompliantAttachmentDataAccess = new CompliantAttachmentDataAccess();

            try
            {
                return objCompliantAttachmentDataAccess.GetRecordByCompliantGuid(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<CompliantAttachment> GetAttachments(string compliantGuid)
        {
            CompliantAttachmentDataAccess objCompliantAttachmentDataAccess = new CompliantAttachmentDataAccess();

            try
            {
                return objCompliantAttachmentDataAccess.GetList(compliantGuid);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        public List<CompliantAttachment> GetAttachments()
        {
            CompliantAttachmentDataAccess objCompliantAttachmentDataAccess = new CompliantAttachmentDataAccess();

            try
            {
                return objCompliantAttachmentDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool Exists(Guid ID)
        {
            AttachmentDataAccess objCompliantAttachmentDataAccess = new AttachmentDataAccess();
            try
            {
                return objCompliantAttachmentDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool UpdateAttachment(Attachment objAttachment)
        {
            AttachmentDataAccess objCompliantAttachmentDataAccess = new AttachmentDataAccess();
            try
            {
                objCompliantAttachmentDataAccess.Update(objAttachment);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool InsertAttachment(CompliantAttachment objAttachment)
        {
            CompliantAttachmentDataAccess objCompliantAttachmentDataAccess = new CompliantAttachmentDataAccess();
            try
            {
                objCompliantAttachmentDataAccess.Insert(objAttachment);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}