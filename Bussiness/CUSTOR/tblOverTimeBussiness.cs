﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;
using System.ComponentModel;

namespace CUSTOR.Bussiness
{
    [DataObjectAttribute]
    public class tblOverTimeBussiness
    {


        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecords(Guid person)
        {
            tblOverTimeDataAccess objtblOverTimeDataAccess = new tblOverTimeDataAccess();

            try
            {
                return objtblOverTimeDataAccess.GetRecords(person);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public tblOverTime GettblOverTime(Guid ID)
        {
            tblOverTimeDataAccess objtblOverTimeDataAccess = new tblOverTimeDataAccess();

            try
            {
                return objtblOverTimeDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<tblOverTime> GettblOverTimes()
        {
            tblOverTimeDataAccess objtblOverTimeDataAccess = new tblOverTimeDataAccess();

            try
            {
                return objtblOverTimeDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
              

    }
}