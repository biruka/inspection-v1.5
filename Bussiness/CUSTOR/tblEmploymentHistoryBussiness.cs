﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;
using System.ComponentModel;

namespace CUSTOR.Bussiness
{
     [DataObjectAttribute]
    public class tblEmploymentHistoryBussiness
    {


        
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecords(Guid person, string language)
        {
            tblEmploymentHistoryDataAccess objtblEmploymentHistoryDataAccess = new tblEmploymentHistoryDataAccess();

            try
            {
                return objtblEmploymentHistoryDataAccess.GetRecords(person, language);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public tblEmploymentHistory GettblEmploymentHistory(Guid ID)
        {
            tblEmploymentHistoryDataAccess objtblEmploymentHistoryDataAccess = new tblEmploymentHistoryDataAccess();

            try
            {
                return objtblEmploymentHistoryDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<tblEmploymentHistory> GettblEmploymentHistorys()
        {
            tblEmploymentHistoryDataAccess objtblEmploymentHistoryDataAccess = new tblEmploymentHistoryDataAccess();

            try
            {
                return objtblEmploymentHistoryDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
       

    }
}