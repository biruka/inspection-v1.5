﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.ComponentModel;
using System.Data;

namespace CUSTOR.Bussiness
{
    [DataObjectAttribute]
    public class OrganizationBussiness
    {

        public bool Delete(Guid ID)
        {
            OrganizationDataAccess objOrganizationDataAccess = new OrganizationDataAccess();
            try
            {
                objOrganizationDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public Organization GetOrganization(Guid ID)
        {
            OrganizationDataAccess objOrganizationDataAccess = new OrganizationDataAccess();

            try
            {
                return objOrganizationDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<Organization> GetOrganizations()
        {
            OrganizationDataAccess objOrganizationDataAccess = new OrganizationDataAccess();

            try
            {
                return objOrganizationDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<Organization> GetListOrg(Guid OrganizationGuid)
        {
            OrganizationDataAccess objOrganizationDataAccess = new OrganizationDataAccess();

            try
            {
                return objOrganizationDataAccess.GetListOrg(OrganizationGuid);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<Organization> GetListOrg(Guid OrganizationGuid, string OrganizationCatagory, int OrganizationLanguage)
        {
            OrganizationDataAccess objOrganizationDataAccess = new OrganizationDataAccess();

            try
            {
                return objOrganizationDataAccess.GetListOrg(OrganizationGuid, OrganizationCatagory, OrganizationLanguage);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<Organization> GetActiveOrganizations()
        {
            OrganizationDataAccess objOrganizationDataAccess = new OrganizationDataAccess();

            try
            {
                return objOrganizationDataAccess.GetActiveOrganizations();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<Organization> GetActiveOrganizationsForInspectionTeam(Guid orgGuid)
        {
            OrganizationDataAccess objOrganizationDataAccess = new OrganizationDataAccess();

            try
            {
                return objOrganizationDataAccess.GetActiveOrganizationsForInspectionTeam(orgGuid);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<Organization> GetActiveOrganizationsForInspection(Guid orgGuid)
        {
            OrganizationDataAccess objOrganizationDataAccess = new OrganizationDataAccess();
            try
            {
                return objOrganizationDataAccess.GetActiveOrganizationsForInspection(orgGuid);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


            public bool Exists(Guid ID)
        {
            OrganizationDataAccess objOrganizationDataAccess = new OrganizationDataAccess();
            try
            {
                return objOrganizationDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateOrganization(Organization objOrganization)
        {
            OrganizationDataAccess objOrganizationDataAccess = new OrganizationDataAccess();
            try
            {
                objOrganizationDataAccess.Update(objOrganization);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertOrganization(Organization objOrganization)
        {
            OrganizationDataAccess objOrganizationDataAccess = new OrganizationDataAccess();
            try
            {
                objOrganizationDataAccess.Insert(objOrganization);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataTable GetOrganizationsList()
        {
            OrganizationDataAccess objOrganizationDataAccess = new OrganizationDataAccess();
            try
            {
                return objOrganizationDataAccess.GetOrgnaizations();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataTable GetOrganizationsByOrgGuid(int orgGuid)
        {
            OrganizationDataAccess objOrganizationDataAccess = new OrganizationDataAccess();
            try
            {
                return objOrganizationDataAccess.GetOrganizationByOrgGuid(orgGuid);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

    }
}