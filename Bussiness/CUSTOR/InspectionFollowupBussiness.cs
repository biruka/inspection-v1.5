﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.ComponentModel;
using System.Data;

namespace CUSTOR.Bussiness
{
     [DataObjectAttribute]
    public class InspectionFollowupBussiness
    {
        public bool Delete(Guid ID)
        {
            InspectionFollowupDataAccess objInspectionFollowupDataAccess = new InspectionFollowupDataAccess();
            try
            {
                objInspectionFollowupDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataTable GetRecordsByDocumentGuid(Guid documentGuid)
        {
            InspectionFollowupDataAccess objInspectionFollowupDataAccess = new InspectionFollowupDataAccess();
            try
            {
                return objInspectionFollowupDataAccess.GetRecordsByDocumentGuid(documentGuid);;
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecords(Guid person)
        {  
            InspectionFollowupDataAccess objInspectionFollowupDataAccess = new InspectionFollowupDataAccess();

            try
            {
                return objInspectionFollowupDataAccess.GetRecords(person);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public InspectionFollowup GetInspectionFollowup(Guid ID)
        {
            InspectionFollowupDataAccess objInspectionFollowupDataAccess = new InspectionFollowupDataAccess();

            try
            {
                return objInspectionFollowupDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<InspectionFollowup> GetInspectionFollow(Guid Id)
        {
            InspectionFollowupDataAccess objInspectionFollowupDataAccess = new InspectionFollowupDataAccess();
            List<InspectionFollowup> lst = new List<InspectionFollowup>();

            try
            {
                //lst = objInspectionFollowupDataAccess.GetList();
                //int rows = lst.Count;
                
                return objInspectionFollowupDataAccess.GetLists(Id);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<InspectionFollowup> GetInspectionFollowups()
        {
            InspectionFollowupDataAccess objInspectionFollowupDataAccess = new InspectionFollowupDataAccess();

          

            try
            {
               

                return objInspectionFollowupDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool Exists(Guid ID)
        {
            InspectionFollowupDataAccess objInspectionFollowupDataAccess = new InspectionFollowupDataAccess();
            try
            {
                return objInspectionFollowupDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public bool UpdateInspectionFollowup(InspectionFollowup objInspectionFollowup)
        {
            InspectionFollowupDataAccess objInspectionFollowupDataAccess = new InspectionFollowupDataAccess();
            try
            {
                objInspectionFollowupDataAccess.Update(objInspectionFollowup);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool InsertInspectionFollowup(InspectionFollowup objInspectionFollowup)
        {
            InspectionFollowupDataAccess objInspectionFollowupDataAccess = new InspectionFollowupDataAccess();
            try
            {
                objInspectionFollowupDataAccess.Insert(objInspectionFollowup);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataTable getEmployeeInspectedOrganizations(Guid personGuid)
        {
            InspectionFollowupDataAccess objOrganizations = new InspectionFollowupDataAccess();
            try
            {
                return objOrganizations.getEmployeeInspectedOrganizations(personGuid);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}