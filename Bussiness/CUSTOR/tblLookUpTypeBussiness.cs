﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.ComponentModel;
namespace CUSTOR.Bussiness
{
     [DataObjectAttribute]
    public class tblLookUpTypeBussiness
    {


        public bool Delete(string ID)
        {
            tblLookUpTypeDataAccess objtblLookUpTypeDataAccess = new tblLookUpTypeDataAccess();
            try
            {
                objtblLookUpTypeDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public tblLookUpType GettblLookUpType(string ID)
        {
            tblLookUpTypeDataAccess objtblLookUpTypeDataAccess = new tblLookUpTypeDataAccess();

            try
            {
                return objtblLookUpTypeDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<tblLookUpType> GettblLookUpTypes()
        {
            tblLookUpTypeDataAccess objtblLookUpTypeDataAccess = new tblLookUpTypeDataAccess();

            try
            {
                return objtblLookUpTypeDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
       
        public bool Exists(string ID)
        {
            tblLookUpTypeDataAccess objtblLookUpTypeDataAccess = new tblLookUpTypeDataAccess();
            try
            {
                return objtblLookUpTypeDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdatetblLookUpType(tblLookUpType objtblLookUpType)
        {
            tblLookUpTypeDataAccess objtblLookUpTypeDataAccess = new tblLookUpTypeDataAccess();
            try
            {
                objtblLookUpTypeDataAccess.Update(objtblLookUpType);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InserttblLookUpType(tblLookUpType objtblLookUpType)
        {
            tblLookUpTypeDataAccess objtblLookUpTypeDataAccess = new tblLookUpTypeDataAccess();
            try
            {
                objtblLookUpTypeDataAccess.Insert(objtblLookUpType);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}