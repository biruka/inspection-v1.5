﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CUSTOR.DataAccess;
using CUSTOR.Domain;

namespace CUSTOR.Bussiness
{
    public class SpecialAssignmentBusiness
    {
        public bool Delete(Guid ID)
        {
            SpecialAssignmentDataAccess objSpecialAssignmentDataAccess = new SpecialAssignmentDataAccess();
            try
            {
                objSpecialAssignmentDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataTable GetRecordByPerson(Guid ID)
        {
            SpecialAssignmentDataAccess objSpecialAssignmentDataAccess = new SpecialAssignmentDataAccess();
            try
            {
                return objSpecialAssignmentDataAccess.GetRecordByPerson(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public SpecialAssignment GettblSpecialAssignment(Guid ID)
        {
            SpecialAssignmentDataAccess objSpecialAssignmentDataAccess = new SpecialAssignmentDataAccess();

            try
            {
                return objSpecialAssignmentDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<SpecialAssignment> GettblSpecialAssignments()
        {
            SpecialAssignmentDataAccess objSpecialAssignmentDataAccess = new SpecialAssignmentDataAccess();

            try
            {
                return objSpecialAssignmentDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(Guid ID)
        {
            SpecialAssignmentDataAccess objSpecialAssignmentDataAccess = new SpecialAssignmentDataAccess();
            try
            {
                return objSpecialAssignmentDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdatetblSpecialAssignment(SpecialAssignment objtblSpecialAssignment)
        {
            SpecialAssignmentDataAccess objSpecialAssignmentDataAccess = new SpecialAssignmentDataAccess();
            try
            {
                objSpecialAssignmentDataAccess.Update(objtblSpecialAssignment);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InserttblSpecialAssignment(SpecialAssignment objtblSpecialAssignment)
        {
            SpecialAssignmentDataAccess objSpecialAssignmentDataAccess = new SpecialAssignmentDataAccess();
            try
            {
                objSpecialAssignmentDataAccess.Insert(objtblSpecialAssignment);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
