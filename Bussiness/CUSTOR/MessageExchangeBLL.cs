﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
//using DevExpress.Web.ASPxTreeList.Export;
using System.ComponentModel;
using System.Configuration;
using CUSTOR.DataAccess;
using CUSTOR.Domain;


namespace CUSTOR.Bussiness
{
    [Serializable]
    [DataObjectAttribute]
    public class NewsGroupEntry
    {
        int _id, _parentId = -1;
        Guid _orgGuid;
        string _case, _subject, _text, _url;
        DateTime _date;
        bool _hasAttachment;

    }

    [Serializable]
    [DataObjectAttribute]
    public class MessageExchangeBLL
    {
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }
      

        static readonly string Key = "MessageExchange";
        static readonly string ID = "ID";

         HttpSessionState Session { get { return HttpContext.Current.Session; } }
        
        // List<MessageExchangeBO> Data
        //{
        //    get
        //    {
        //        if (Session[Key] == null)
        //            Restore();
        //        return (List<MessageExchangeBO>)Session[Key];
        //    }
        //}

       [DataObjectMethod(DataObjectMethodType.Select, true)]
       public  DataTable GetMessages(Int32 ParentID)
       {
           DetailMessageExchangeDAL objDAL = new DetailMessageExchangeDAL();

           return objDAL.GetMessages(ParentID);
          
       }
        //new 
     public List<MessageExchangeBO> GetParentMessages()
     {
         DetailMessageExchangeDAL objDAL = new DetailMessageExchangeDAL();

         return objDAL.GetParentMessages();
     }


        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public void Insert(MessageExchangeBO item)
        {
            DetailMessageExchangeDAL objDAL = new DetailMessageExchangeDAL();
            item.ParentID = Int32.Parse(Session["ID"].ToString());
            objDAL.Insert(item);
          //  Data.Add(item);

        }
        public bool UpdateParentMessageStatus(Int32 ParentMessageID)
        {
            MessageExchangeDataAccess objtblMessageExchangeDataAccess = new MessageExchangeDataAccess();
            try
            {
                objtblMessageExchangeDataAccess.UpdateParentMessageStatus(ParentMessageID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateParentMessageFllowStatus(Int32 ParentMessageID, String FinalRevisedBy, Int32 statusValue)
        {
            MessageExchangeDataAccess objtblMessageExchangeDataAccess = new MessageExchangeDataAccess();
            try
            {
                objtblMessageExchangeDataAccess.UpdateParentMessageFllowStatus(ParentMessageID, FinalRevisedBy, statusValue);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public void Update(MessageExchangeBO item)
        {
            DetailMessageExchangeDAL objDAL = new DetailMessageExchangeDAL();
            //MessageExchangeBO storedItem = FindItem(item.ParentMessageID);
            int i = (int)IDSession["ID"];
            item.EmployeeName = "ወርቅነህ ገዳሙ";
            item.UserName = "User1";
            item.UpdatedUsername = "User";
            item.EventDatetime = DateTime.Today;
            item.UpdatedEventDatetime = DateTime.Today;

          //  storedItem.Assign(item);
            objDAL.Update(item);
        }

        public void Delete(MessageExchangeBO item)
        {
          //  MessageExchangeBO storedItem = FindItem(item.ParentMessageID);
           // Data.Remove(storedItem);
        }

        //public  void Corrupt()
        //{
        //    int count = 0;
        //    foreach (MessageExchangeBO item in Data)
        //    {
        //        int mod = count % 5;
        //        if (mod == 0)
        //            item.Subject = "";
        //        if (mod == 2)
        //           // item.From = "";
        //        if (mod == 4)
        //          //  item.Date = DateTime.Now.AddYears(-30 - count);
        //        count++;
        //    }
        //}

        //public  void Restore()
        //{
        //    DetailMessageExchangeDAL objDAL = new DetailMessageExchangeDAL();
        //    if (Session["OrgGuid"] == null)
        //        Session[Key] = objDAL.GetList();
        //    else
        //    {
        //        Session[Key] = objDAL.GetMessages();
        //    }
        //}

        HttpSessionState IDSession { get { return HttpContext.Current.Session; } }
       
        //public Int32 findID(int id)
        //{
        //    foreach (MessageExchangeBO item in Data)
        //        if (item.ParentMessageID == id)
        //        {
        //            IDSession[ID] = id;
        //            return id;
        //        }
        //    return 1;

        //}


        //public MessageExchangeBO FindItem(int id)
        //{
        //    foreach (MessageExchangeBO item in Data)
        //        if (item.ParentMessageID == id)
        //        {
        //            IDSession[ID] = id;
        //            return item;
        //        }
        //    return null;
        //}

       public bool DeleteMessage(int ID)
       {
           DetailMessageExchangeDAL objMsgDataAccess = new DetailMessageExchangeDAL();
           try
           {
               objMsgDataAccess.DeleteMessage(ID);
               return true;
           }
           catch (Exception ex)
           {
               throw (ex);
           }
       }

      [DataObjectMethod(DataObjectMethodType.Select, true)]
       public DataTable poplateOrganization()
        {
            DetailMessageExchangeDAL objDAL = new DetailMessageExchangeDAL();
           return  objDAL.poplateOrganization();

        }

      [DataObjectMethod(DataObjectMethodType.Select, true)]
      public DataTable BindMessageAttachments(Int32 id)
      {
          DetailMessageExchangeDAL objAttachmentDataAccess = new DetailMessageExchangeDAL();

          try
          {
             // id = 0;// (Int32)Session["ID"];
              return objAttachmentDataAccess.BindMessageAttachments(id);
          }
          catch (Exception ex)
          {
              throw (ex);
          }
      }

      public bool InserttblMessageExchangeAttachment(AttachmentMessageExchangeBO objtblMessageExchangeAttachment)
      {
          DetailMessageExchangeDAL objtblMessageExchangeAttachmentDataAccess = new DetailMessageExchangeDAL();
          try
          {
              objtblMessageExchangeAttachment.ID=(Int32)IDSession["ID"];
              objtblMessageExchangeAttachmentDataAccess.InsertMessageAttachement(objtblMessageExchangeAttachment);
              return true;
          }
          catch (Exception ex)
          {
              throw (ex);
          }
      }

       
    }

    [DataObjectAttribute]
    public class MessageExchangeBussiness
    {

        public bool Delete(Int32 MessageID)
        {
            MessageExchangeDataAccess objtblMessageExchangeDataAccess = new MessageExchangeDataAccess();
            try
            {
                objtblMessageExchangeDataAccess.Delete(MessageID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public MessageExchangeBO GettblMessageExchange(Int32 MessageID)
        {
            MessageExchangeDataAccess objtblMessageExchangeDataAccess = new MessageExchangeDataAccess();

            try
            {
                return objtblMessageExchangeDataAccess.GetRecord(MessageID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<MessageExchangeBO> GettblMessageExchanges()
        {
            MessageExchangeDataAccess objtblMessageExchangeDataAccess = new MessageExchangeDataAccess();

            try
            {
                return objtblMessageExchangeDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<MessageExchangeBO> GetParentMessageList(Int32 ParentID)
        {
            MessageExchangeDataAccess objtblMessageExchangeDataAccess = new MessageExchangeDataAccess();

            try
            {
                return objtblMessageExchangeDataAccess.GetParentMessageList(ParentID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(string ID)
        {
            MessageExchangeDataAccess objtblMessageExchangeDataAccess = new MessageExchangeDataAccess();
            try
            {
                return objtblMessageExchangeDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
       
        public bool UpdatetblMessageExchange(MessageExchangeBO objtblMessageExchange)
        {
            MessageExchangeDataAccess objtblMessageExchangeDataAccess = new MessageExchangeDataAccess();
            try
            {
                objtblMessageExchangeDataAccess.Update(objtblMessageExchange);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

      
        public bool InserttblMessageExchange(MessageExchangeBO objtblMessageExchange)
        {
            MessageExchangeDataAccess objtblMessageExchangeDataAccess = new MessageExchangeDataAccess();
            try
            {
                objtblMessageExchangeDataAccess.Insert(objtblMessageExchange);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}
