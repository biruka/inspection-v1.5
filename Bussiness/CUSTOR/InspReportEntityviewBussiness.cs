﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;

namespace CUSTOR.Bussiness
{
    public class InspReportEntityviewBussiness
    {


       
        public DataTable GetRecord(string OrgGuid)
            {
                InspReportEntityviewDataAccess objInspReportEntityviewDataAccess = new InspReportEntityviewDataAccess();

                    try
                    {
                        return objInspReportEntityviewDataAccess.GetRecords(OrgGuid);
                    }
                    catch (Exception ex)
                    {
                        throw (ex);
                    }
            }
        public DataTable GetRecordcomplete(string OrgGuid)
        {
            InspReportEntityviewDataAccess objInspReportEntityviewDataAccess = new InspReportEntityviewDataAccess();

            try
            {
                return objInspReportEntityviewDataAccess.GetRecordcomplete(OrgGuid);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public DataTable GetRecordIncomplatePromotion(string OrgGuid)
        {
            InspReportEntityviewDataAccess objInspReportEntityviewDataAccess = new InspReportEntityviewDataAccess();

            try
            {
                return objInspReportEntityviewDataAccess.GetRecordIncomplatePromotion(OrgGuid);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public DataTable GetRecordIncomplateTransfer(string OrgGuid)
        {
            InspReportEntityviewDataAccess objInspReportEntityviewDataAccess = new InspReportEntityviewDataAccess();

            try
            {
                return objInspReportEntityviewDataAccess.GetRecordIncomplateTransfer(OrgGuid);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        
        public DataTable GetHrActivitySummery(string OrgGuid)
        {
            InspReportEntityviewDataAccess objInspReportEntityviewDataAccess = new InspReportEntityviewDataAccess();

            try
            {
                return objInspReportEntityviewDataAccess.GetHrActivitySummery(OrgGuid);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public DataTable GetinspectorHrActivitySummery(string OrgGuid)
        {
            InspReportEntityviewDataAccess objInspReportEntityviewDataAccess = new InspReportEntityviewDataAccess();

            try
            {
                return objInspReportEntityviewDataAccess.GetInspectorHrActivitySummery(OrgGuid);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
                
        public InspReportEntityview GetInspReportEntityview(string ID)
        {
            InspReportEntityviewDataAccess objInspReportEntityviewDataAccess = new InspReportEntityviewDataAccess();

            try
            {
                return objInspReportEntityviewDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        



      
    }
}