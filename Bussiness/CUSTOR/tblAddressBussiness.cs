﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;

namespace CUSTOR.Bussiness
{
    public class tblAddressBussiness
    {


        public bool Delete(string ID)
        {
            tblAddressDataAccess objtblAddressDataAccess = new tblAddressDataAccess();
            try
            {
                objtblAddressDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public tblAddress GettblAddress(string ID)
        {
            tblAddressDataAccess objtblAddressDataAccess = new tblAddressDataAccess();

            try
            {
                return objtblAddressDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<tblAddress> GettblAddresss()
        {
            tblAddressDataAccess objtblAddressDataAccess = new tblAddressDataAccess();

            try
            {
                return objtblAddressDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(string ID)
        {
            tblAddressDataAccess objtblAddressDataAccess = new tblAddressDataAccess();
            try
            {
                return objtblAddressDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdatetblAddress(tblAddress objtblAddress)
        {
            tblAddressDataAccess objtblAddressDataAccess = new tblAddressDataAccess();
            try
            {
                objtblAddressDataAccess.Update(objtblAddress);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InserttblAddress(tblAddress objtblAddress)
        {
            tblAddressDataAccess objtblAddressDataAccess = new tblAddressDataAccess();
            try
            {
                objtblAddressDataAccess.Insert(objtblAddress);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}