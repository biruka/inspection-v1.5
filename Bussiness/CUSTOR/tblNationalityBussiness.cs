﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;

namespace CUSTOR.Bussiness
{
    public class tblNationalityBussiness
    {      
        public tblNationality GettblNationality(string ID)
        {
            tblNationalityDataAccess objtblNationalityDataAccess = new tblNationalityDataAccess();

            try
            {
                return objtblNationalityDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<tblNationality> GettblNationalitys()
        {
            tblNationalityDataAccess objtblNationalityDataAccess = new tblNationalityDataAccess();

            try
            {
                return objtblNationalityDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(string ID)
        {
            tblNationalityDataAccess objtblNationalityDataAccess = new tblNationalityDataAccess();
            try
            {
                return objtblNationalityDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
       

    }
}