﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;
using System.ComponentModel;

namespace CUSTOR.Bussiness
{
    [DataObjectAttribute]
    public class InspectionFollowupEntiryviewBussiness
    {


        public bool Delete(string ID)
        {
            InspectionFollowupEntiryviewDataAccess objInspectionFollowupEntiryviewDataAccess = new InspectionFollowupEntiryviewDataAccess();
            try
            {
                objInspectionFollowupEntiryviewDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecords(Guid person)
        {
            InspectionFollowupEntiryviewDataAccess objInspectionFollowupEntiryviewDataAccess = new InspectionFollowupEntiryviewDataAccess();

            try
            {
                return objInspectionFollowupEntiryviewDataAccess.GetRecords(person);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public InspectionFollowupEntiryview GetInspectionFollowupEntiryview(Guid ID)
        {
            InspectionFollowupEntiryviewDataAccess objInspectionFollowupEntiryviewDataAccess = new InspectionFollowupEntiryviewDataAccess();

            try
            {
                return objInspectionFollowupEntiryviewDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<InspectionFollowupEntiryview> GetInspectionFollowupEntiryviews()
        {
            InspectionFollowupEntiryviewDataAccess objInspectionFollowupEntiryviewDataAccess = new InspectionFollowupEntiryviewDataAccess();

            try
            {
                return objInspectionFollowupEntiryviewDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(string ID)
        {
            InspectionFollowupEntiryviewDataAccess objInspectionFollowupEntiryviewDataAccess = new InspectionFollowupEntiryviewDataAccess();
            try
            {
                return objInspectionFollowupEntiryviewDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateInspectionFollowupEntiryview(InspectionFollowupEntiryview objInspectionFollowupEntiryview)
        {
            InspectionFollowupEntiryviewDataAccess objInspectionFollowupEntiryviewDataAccess = new InspectionFollowupEntiryviewDataAccess();
            try
            {
                objInspectionFollowupEntiryviewDataAccess.Update(objInspectionFollowupEntiryview);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertInspectionFollowupEntiryview(InspectionFollowupEntiryview objInspectionFollowupEntiryview)
        {
            InspectionFollowupEntiryviewDataAccess objInspectionFollowupEntiryviewDataAccess = new InspectionFollowupEntiryviewDataAccess();
            try
            {
                objInspectionFollowupEntiryviewDataAccess.Insert(objInspectionFollowupEntiryview);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}