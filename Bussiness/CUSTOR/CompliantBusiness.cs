﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.ComponentModel;
namespace CUSTOR.Bussiness
{
     [DataObjectAttribute]
    public class ComplaintBusiness
    {
        public bool Delete(string ID)
        {
            ComplaintDataAccess objComplaintDataAccess = new ComplaintDataAccess();
            try
            {
                objComplaintDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        
        public DataTable GetRecordreference(Guid person, string FileType)
        {
            ComplaintDataAccess objComplaintDataAccess = new ComplaintDataAccess();

            try
            {
                return objComplaintDataAccess.GetRecordreference(person, FileType);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetComplianRecord(Guid ComptGuid, int language)
        {
            ComplaintDataAccess objComplaintDataAccess = new ComplaintDataAccess();

            try
            {
                return objComplaintDataAccess.GetComplianRecord(ComptGuid, language);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        //public Complaint GetComplaint(string ID)
        //{
        //    ComplaintDataAccess objComplaintDataAccess = new ComplaintDataAccess();

        //    try
        //    {
        //        return objComplaintDataAccess.GetRecord(ID);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw (ex);
        //    }
        //}
        public DataTable GetComplaint(Guid ID)
        {
            ComplaintDataAccess objComplaintDataAccess = new ComplaintDataAccess();

            try
            {
                return objComplaintDataAccess.GetRecordDt(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public Complaint GettblComplaint(Guid ID)
        {
            ComplaintDataAccess objtblComplaintDataAccess = new ComplaintDataAccess();

            try
            {
                return objtblComplaintDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public DataTable GetComplaintEmployee(Guid ID, bool isreg, int language)
        {
            ComplaintDataAccess objComplaintDataAccess = new ComplaintDataAccess();

            try
            {
                return objComplaintDataAccess.GetRecordEmployee(ID, isreg, language);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public DataTable GetComplaintAllEmployee(bool isreg)
        {
            ComplaintDataAccess objComplaintDataAccess = new ComplaintDataAccess();

            try
            {
                return objComplaintDataAccess.GetRecordEmployeeAll(isreg);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public DataTable GetComplaintDetail(bool isreg, int language)
        {
            ComplaintDataAccess objComplaintDataAccess = new ComplaintDataAccess();

            try
            {
                return objComplaintDataAccess.GetRecordComplianReg(isreg,language);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool MakeCompliantDecision(Complaint objComplaint)
        {
            ComplaintDataAccess objComplaintDataAccess = new ComplaintDataAccess();
            try
            {
                objComplaintDataAccess.MakeCompliantDecision(objComplaint);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool UpdateCompliant(Complaint objComplaint)
        {
            ComplaintDataAccess objComplaintDataAccess = new ComplaintDataAccess();
            try
            {
                objComplaintDataAccess.UpdateCompliant(objComplaint);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool InsertComplaint(Complaint objComplaint)
        {
            ComplaintDataAccess objComplaintDataAccess = new ComplaintDataAccess();
            try
            {
                return objComplaintDataAccess.Insert(objComplaint);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataTable SearchCompliant(Guid orgGuid, int complainType, DateTime dateFrom, DateTime dateTo, bool isGrievance, int language)
        {
            ComplaintDataAccess objComplaintDataAccess = new ComplaintDataAccess();
            try
            {
                return objComplaintDataAccess.Search(orgGuid, complainType, dateFrom, dateTo, isGrievance,language);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public DataTable RegistrationSearch(Guid orgGuid, int complainType, int decisionType, bool isGrievance)
        {
            ComplaintDataAccess objComplaintDataAccess = new ComplaintDataAccess();
            try
            {
                return objComplaintDataAccess.RegistrationSearch(orgGuid, complainType, decisionType, isGrievance);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
       
        public DataTable GenerateReport(Guid orgGuid, int complainType, DateTime dateFrom, DateTime dateTo, int decisionType, bool isGrievance)
        {
            ComplaintDataAccess objComplaintDataAccess = new ComplaintDataAccess();
            try
            {
                return objComplaintDataAccess.Report(orgGuid, complainType, dateFrom, dateTo, decisionType,isGrievance);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataTable GenerateStatisticalReport(Guid orgGuid, DateTime dateFrom, DateTime dateTo, bool isGrievance)
        {
            ComplaintDataAccess objComplaintDataAccess = new ComplaintDataAccess();
            try
            {
                return objComplaintDataAccess.StatisticalReport(orgGuid, dateFrom, dateTo, isGrievance);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataTable GenerateFindingSummaryStatisticalReport(Guid orgGuid, DateTime dateFrom, DateTime dateTo, bool isGrievance)
        {
            ComplaintDataAccess objComplaintDataAccess = new ComplaintDataAccess();
            try
            {
                return objComplaintDataAccess.FindingSummaryStatisticalReport(orgGuid, dateFrom, dateTo, isGrievance);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(string Referenceno)
        {
            ComplaintDataAccess objtblComplaintDataAccess = new ComplaintDataAccess();
            try
            {
                return objtblComplaintDataAccess.Exists(Referenceno);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }



        public DataTable SearchCompliant(Guid orgGuid, int complainType, DateTime reportedDateFrom, DateTime reportedDateTo, int descisionType, bool isGrievance)
        {
            throw new NotImplementedException();
        }
    }
}