﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;
using System.ComponentModel;
namespace CUSTOR.Bussiness
{
       [DataObjectAttribute]
    public class PersonEmploymentEntityViewBussiness
    {           
        public bool Delete(string ID)
        {
            PersonEmploymentEntityViewDataAccess objPersonEmploymentEntityViewDataAccess = new PersonEmploymentEntityViewDataAccess();
            try
            {
                objPersonEmploymentEntityViewDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
           [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecords(Guid OrgGuid, int DocumentType, DateTime fromdate, DateTime todate, string language, bool isHrOfficer)
        {
            PersonEmploymentEntityViewDataAccess objPersonEmploymentEntityViewDataAccess = new PersonEmploymentEntityViewDataAccess();

            try
            {
                                
                return objPersonEmploymentEntityViewDataAccess.GetRecords(OrgGuid, DocumentType, fromdate, todate,language, isHrOfficer);
               
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public PersonEmploymentEntityView GetPersonEmploymentEntityView(Guid ID)
        {
            PersonEmploymentEntityViewDataAccess objPersonEmploymentEntityViewDataAccess = new PersonEmploymentEntityViewDataAccess();

            try
            {
                return objPersonEmploymentEntityViewDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
          
        public List<PersonEmploymentEntityView> GetPersonEmploymentEntityViews()
        {
            PersonEmploymentEntityViewDataAccess objPersonEmploymentEntityViewDataAccess = new PersonEmploymentEntityViewDataAccess();

            try
            {
                return objPersonEmploymentEntityViewDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(string ID)
        {
            PersonEmploymentEntityViewDataAccess objPersonEmploymentEntityViewDataAccess = new PersonEmploymentEntityViewDataAccess();
            try
            {
                return objPersonEmploymentEntityViewDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdatePersonEmploymentEntityView(PersonEmploymentEntityView objPersonEmploymentEntityView)
        {
            PersonEmploymentEntityViewDataAccess objPersonEmploymentEntityViewDataAccess = new PersonEmploymentEntityViewDataAccess();
            try
            {
                objPersonEmploymentEntityViewDataAccess.Update(objPersonEmploymentEntityView);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertPersonEmploymentEntityView(PersonEmploymentEntityView objPersonEmploymentEntityView)
        {
            PersonEmploymentEntityViewDataAccess objPersonEmploymentEntityViewDataAccess = new PersonEmploymentEntityViewDataAccess();
            try
            {
                objPersonEmploymentEntityViewDataAccess.Insert(objPersonEmploymentEntityView);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}