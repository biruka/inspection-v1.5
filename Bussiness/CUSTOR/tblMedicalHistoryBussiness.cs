﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.ComponentModel;
using System.Data;

namespace CUSTOR.Bussiness
{
    [DataObjectAttribute]
    public class tblMedicalHistoryBussiness
    {


        
         [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecords(Guid person)
        {
            tblMedicalHistoryDataAccess objtblMedicalHistoryDataAccess = new tblMedicalHistoryDataAccess();

            try
            {
                return objtblMedicalHistoryDataAccess.GetRecords(person);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public tblMedicalHistory GettblMedicalHistory(int ID)
        {
            tblMedicalHistoryDataAccess objtblMedicalHistoryDataAccess = new tblMedicalHistoryDataAccess();

            try
            {
                return objtblMedicalHistoryDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<tblMedicalHistory> GettblMedicalHistorys()
        {
            tblMedicalHistoryDataAccess objtblMedicalHistoryDataAccess = new tblMedicalHistoryDataAccess();

            try
            {
                return objtblMedicalHistoryDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool Exists(int ID)
        {
            tblMedicalHistoryDataAccess objtblMedicalHistoryDataAccess = new tblMedicalHistoryDataAccess();
            try
            {
                return objtblMedicalHistoryDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
       


    }
}
