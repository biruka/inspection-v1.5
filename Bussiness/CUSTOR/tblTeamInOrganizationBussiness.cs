﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;

namespace CUSTOR.Bussiness
{
    public class tblTeamInOrganizationBussiness
    {
        public bool Delete(Guid ID)
        {
            tblTeamInOrganizationDataAccess objtblTeamInOrganizationDataAccess = new tblTeamInOrganizationDataAccess();
            try
            {
                objtblTeamInOrganizationDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool DeleteTeamOrg(Guid ID)
        {
            tblTeamInOrganizationDataAccess objtblTeamInOrganizationDataAccess = new tblTeamInOrganizationDataAccess();
            try
            {
                objtblTeamInOrganizationDataAccess.DeleteTeamOrg(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool DeleteTeamOrgByTeamOrgGuid(Guid ID,Guid OrgGuid)
        {
            tblTeamInOrganizationDataAccess objtblTeamInOrganizationDataAccess = new tblTeamInOrganizationDataAccess();
            try
            {
                objtblTeamInOrganizationDataAccess.DeleteTeamOrgBuTeamAndOrgGuid(ID,OrgGuid);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public tblTeamInOrganization GettblTeamInOrganization(Guid ID)
        {
            tblTeamInOrganizationDataAccess objtblTeamInOrganizationDataAccess = new tblTeamInOrganizationDataAccess();

            try
            {
                return objtblTeamInOrganizationDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool IsTeamBindToOrganization(Guid TeamGuid)
        {
            tblTeamInOrganizationDataAccess objtblTeamInOrganizationDataAccess = new tblTeamInOrganizationDataAccess();
            try
            {
                return objtblTeamInOrganizationDataAccess.IsTeamBindToOrganization(TeamGuid);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataTable GettblTeamInOrg(Guid ID)
        {
            tblTeamInOrganizationDataAccess objtblTeamInOrganizationDataAccess = new tblTeamInOrganizationDataAccess();

            try
            {
                return objtblTeamInOrganizationDataAccess.GetRecords(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

       
        public List<tblTeamInOrganization> GettblTeamInOrganizations()
        {
            tblTeamInOrganizationDataAccess objtblTeamInOrganizationDataAccess = new tblTeamInOrganizationDataAccess();

            try
            {
                return objtblTeamInOrganizationDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(Guid orgGuid, Guid TeamGuid)
        {
            tblTeamInOrganizationDataAccess objtblTeamInOrganizationDataAccess = new tblTeamInOrganizationDataAccess();
            try
            {
                return objtblTeamInOrganizationDataAccess.Exists(orgGuid, TeamGuid);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdatetblTeamInOrganization(tblTeamInOrganization objtblTeamInOrganization)
        {
            tblTeamInOrganizationDataAccess objtblTeamInOrganizationDataAccess = new tblTeamInOrganizationDataAccess();
            try
            {
                objtblTeamInOrganizationDataAccess.Update(objtblTeamInOrganization);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InserttblTeamInOrganization(tblTeamInOrganization objtblTeamInOrganization)
        {
            tblTeamInOrganizationDataAccess objtblTeamInOrganizationDataAccess = new tblTeamInOrganizationDataAccess();
            try
            {
                objtblTeamInOrganizationDataAccess.Insert(objtblTeamInOrganization);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}