﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;
using System.ComponentModel;

namespace CUSTOR.Bussiness
{
    [DataObjectAttribute]
    public class tblServiceYearExtenderBussiness
    {
               
         [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecords(Guid person)
        {
            tblServiceYearExtenderDataAccess objtblServiceYearExtenderDataAccess = new tblServiceYearExtenderDataAccess();

            try
            {
                return objtblServiceYearExtenderDataAccess.GetRecords(person);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public tblServiceYearExtender GettblServiceYearExtender(Guid ID)
        {
            tblServiceYearExtenderDataAccess objtblServiceYearExtenderDataAccess = new tblServiceYearExtenderDataAccess();

            try
            {
                return objtblServiceYearExtenderDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<tblServiceYearExtender> GettblServiceYearExtenders()
        {
            tblServiceYearExtenderDataAccess objtblServiceYearExtenderDataAccess = new tblServiceYearExtenderDataAccess();

            try
            {
                return objtblServiceYearExtenderDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(Guid ID)
        {
            tblServiceYearExtenderDataAccess objtblServiceYearExtenderDataAccess = new tblServiceYearExtenderDataAccess();
            try
            {
                return objtblServiceYearExtenderDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        
    }
}