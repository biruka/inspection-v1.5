﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.ComponentModel;

namespace CUSTOR.Bussiness
{
    [DataObjectAttribute]
    public class tblEvaluationBussiness
    {
        public tblEvaluation GettblEvaluation(string ID,Guid Person)
        {
            tblEvaluationDataAccess objtblEvaluationDataAccess = new tblEvaluationDataAccess();

            try
            {
                return objtblEvaluationDataAccess.GetRecord(ID, Person);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<tblEvaluation> GettblEvaluations()
        {
            tblEvaluationDataAccess objtblEvaluationDataAccess = new tblEvaluationDataAccess();

            try
            {
                return objtblEvaluationDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<tblEvaluation> GetEvaluation(DateTime ID, Guid Person)
        {
            tblEvaluationDataAccess objtblEvaluationDataAccess = new tblEvaluationDataAccess();

            try
            {
               
                return objtblEvaluationDataAccess.GetEvaluation(ID, Person);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}