﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;
using System.ComponentModel;

namespace CUSTOR.Bussiness
{
    [DataObjectAttribute]
    public class tblPromotionHistoryBussiness
    {

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecords(Guid person, int DocumentType)
        {
            tblPromotionHistoryDataAccess objtblPromotionHistoryDataAccess = new tblPromotionHistoryDataAccess();

            try
            {
                return objtblPromotionHistoryDataAccess.GetRecords(person, DocumentType);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public tblPromotionHistory GettblPromotionHistory(Guid ID)
        {
            tblPromotionHistoryDataAccess objtblPromotionHistoryDataAccess = new tblPromotionHistoryDataAccess();

            try
            {
                return objtblPromotionHistoryDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<tblPromotionHistory> GettblPromotionHistorys()
        {
            tblPromotionHistoryDataAccess objtblPromotionHistoryDataAccess = new tblPromotionHistoryDataAccess();

            try
            {
                return objtblPromotionHistoryDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
       


    }
}