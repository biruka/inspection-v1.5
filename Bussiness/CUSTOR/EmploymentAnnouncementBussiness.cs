﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;
using System.ComponentModel;

namespace CUSTOR.Bussiness
{
     [DataObjectAttribute]
    public class EmploymentAnnouncementBussiness
    {

        public bool Delete(Guid ID)
        {
            EmploymentAnnouncementDataAccess objEmploymentAnnouncementDataAccess = new EmploymentAnnouncementDataAccess();
            try
            {
                objEmploymentAnnouncementDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecords(Guid person, string HRActivitytype, string language)
        {
            EmploymentAnnouncementDataAccess objEmploymentAnnouncementDataAccess = new EmploymentAnnouncementDataAccess();

            try
            {
                string HRActivity=(HRActivitytype!=null? HRActivitytype :"340");
                return objEmploymentAnnouncementDataAccess.GetRecords(person, HRActivity, language);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public EmploymentAnnouncement GetEmploymentAnnouncement(Guid ID)
        {
            EmploymentAnnouncementDataAccess objEmploymentAnnouncementDataAccess = new EmploymentAnnouncementDataAccess();

            try
            {
                return objEmploymentAnnouncementDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<EmploymentAnnouncement> GetEmploymentAnnouncements()
        {
            EmploymentAnnouncementDataAccess objEmploymentAnnouncementDataAccess = new EmploymentAnnouncementDataAccess();

            try
            {
                return objEmploymentAnnouncementDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
       


    }
}