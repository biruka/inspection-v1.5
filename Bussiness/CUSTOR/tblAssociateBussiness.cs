﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;
using System.ComponentModel;

namespace CUSTOR.Bussiness
{
    [DataObjectAttribute]
    public class tblAssociateBussiness
    {
               
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecords(Guid person)
        {
            tblAssociateDataAccess objtblAssociateDataAccess = new tblAssociateDataAccess();

            try
            {
                return objtblAssociateDataAccess.GetRecords(person);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public tblAssociate GettblAssociate(Guid ID)
        {
            tblAssociateDataAccess objtblAssociateDataAccess = new tblAssociateDataAccess();

            try
            {
                return objtblAssociateDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<tblAssociate> GettblAssociates()
        {
            tblAssociateDataAccess objtblAssociateDataAccess = new tblAssociateDataAccess();

            try
            {
                return objtblAssociateDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }      

    }
}