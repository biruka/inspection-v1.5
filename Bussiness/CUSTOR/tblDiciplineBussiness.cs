﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;
using System.ComponentModel;

namespace CUSTOR.Bussiness
{
     [DataObjectAttribute]
    public class tblDiciplineBussiness
    {


        
         [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecords(Guid person)
        {
            tblDiciplineDataAccess objtblDiciplineDataAccess = new tblDiciplineDataAccess();

            try
            {
                return objtblDiciplineDataAccess.GetRecords(person);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public tblDicipline GettblDicipline(Guid ID)
        {
            tblDiciplineDataAccess objtblDiciplineDataAccess = new tblDiciplineDataAccess();

            try
            {
                return objtblDiciplineDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<tblDicipline> GettblDiciplines()
        {
            tblDiciplineDataAccess objtblDiciplineDataAccess = new tblDiciplineDataAccess();

            try
            {
                return objtblDiciplineDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        

    }
}