﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;
using System.ComponentModel;

namespace CUSTOR.Bussiness
{
    [DataObjectAttribute]
    public class CandidateEntityViewBussiness
    {

        
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecords(Guid person,string Activitytype,string language)
        {

            CandidateEntityViewDataAccess objCandidateEntityViewDataAccess = new CandidateEntityViewDataAccess();

            try
            {
                if (Activitytype != null)
                {
                    return objCandidateEntityViewDataAccess.GetRecords(person, Activitytype, language);
                }
                else 
                {
                    return objCandidateEntityViewDataAccess.GetRecords(person, "390", language);
                }

                    
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public CandidateEntityView GetCandidateEntityView(string ID)
        {
            CandidateEntityViewDataAccess objCandidateEntityViewDataAccess = new CandidateEntityViewDataAccess();

            try
            {
                return objCandidateEntityViewDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<CandidateEntityView> GetCandidateEntityViews()
        {
            CandidateEntityViewDataAccess objCandidateEntityViewDataAccess = new CandidateEntityViewDataAccess();

            try
            {
                return objCandidateEntityViewDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
       

    }
}