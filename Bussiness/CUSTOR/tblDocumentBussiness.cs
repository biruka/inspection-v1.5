﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;
using System.ComponentModel;
namespace CUSTOR.Bussiness
{
    [DataObjectAttribute]
    public class tblDocumentBussiness
    {
        public bool Delete(Guid ID)
        {
            tblDocumentDataAccess objtblDocumentDataAccess = new tblDocumentDataAccess();
            try
            {
                objtblDocumentDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataTable GetRecordsByDocumentTypeGuid(Guid documentGuid)
        {
            tblDocumentDataAccess objtblDocumentDataAccess = new tblDocumentDataAccess();
            try
            {
                return objtblDocumentDataAccess.GetRecordsByDocumentTypeGuid(documentGuid);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataTable SearchDocument(string documentDescription)
        {
            tblDocumentDataAccess objtblDocumentDataAccess = new tblDocumentDataAccess();
            try
            {
                return objtblDocumentDataAccess.SearchDocument(documentDescription);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecordCheckList(string parent, string HrActTypeDoc, int language)
        {
            tblDocumentDataAccess objtblDocumentDataAccess = new tblDocumentDataAccess();

            try
            {
                if (parent != null && parent != "-1")
                    return objtblDocumentDataAccess.GetRecordCheckList(parent, HrActTypeDoc, language);
                else
                    return objtblDocumentDataAccess.GetRecordCheckList("341", HrActTypeDoc, language);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecordCheckListNotOptioal(string parent, string HrActTypeDoc, int language)
        {
            tblDocumentDataAccess objtblDocumentDataAccess = new tblDocumentDataAccess();

            try
            {
                if (parent != null && parent != "-1")
                    return objtblDocumentDataAccess.GetRecordCheckListNotOptional(parent, HrActTypeDoc, language);
                else
                    return objtblDocumentDataAccess.GetRecordCheckListNotOptional("341", HrActTypeDoc, language);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

         public DataTable GetRecordEditCheckList(string parent, string HrActTypeDoc, int language, Guid InspGuid)
         {
             tblDocumentDataAccess objtblDocumentDataAccess = new tblDocumentDataAccess();

             try
             {
                 if (parent != null)
                     return objtblDocumentDataAccess.GetRecordEditCheckList(parent, HrActTypeDoc, language,InspGuid);
                 else
                     return objtblDocumentDataAccess.GetRecordEditCheckList("341", HrActTypeDoc, language, InspGuid);
             }
             catch (Exception ex)
             {
                 throw (ex);
             }
         }
        public tblDocument GettblDocument(Guid ID)
        {
            tblDocumentDataAccess objtblDocumentDataAccess = new tblDocumentDataAccess();

            try
            {
                return objtblDocumentDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public Guid GettblDocumentDocGuid(string ID)
        {
            tblDocumentDataAccess objtblDocumentDataAccess = new tblDocumentDataAccess();

            try
            {
                return objtblDocumentDataAccess.GetRecordDocGuid(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public tblDocument GetRecordDetail(string ID)
        {
            tblDocumentDataAccess objtblDocumentDataAccess = new tblDocumentDataAccess();

            try
            {
                return objtblDocumentDataAccess.GetRecordDetail(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public DataTable GettblDocuments()
        {
            tblDocumentDataAccess objtblDocumentDataAccess = new tblDocumentDataAccess();
            try
            {
                return objtblDocumentDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(Guid documentTypeGuid,string code)
        {
            tblDocumentDataAccess objtblDocumentDataAccess = new tblDocumentDataAccess();
            try
            {
                return objtblDocumentDataAccess.Exists(documentTypeGuid,code);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdatetblDocument(tblDocument objtblDocument)
        {
            tblDocumentDataAccess objtblDocumentDataAccess = new tblDocumentDataAccess();
            try
            {
                objtblDocumentDataAccess.Update(objtblDocument);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InserttblDocument(tblDocument objtblDocument)
        {
            tblDocumentDataAccess objtblDocumentDataAccess = new tblDocumentDataAccess();
            try
            {
                objtblDocumentDataAccess.Insert(objtblDocument);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}