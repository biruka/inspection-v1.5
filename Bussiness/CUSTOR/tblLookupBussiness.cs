﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;
namespace CUSTOR.Bussiness
{
    public class tblLookupBussiness
    {
        
        public tblLookup GettblLookup(int ID)
        {
            tblLookupDataAccess objtblLookupDataAccess = new tblLookupDataAccess();

            try
            {
                return objtblLookupDataAccess.GetRecordI(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<tblLookup> GettblLookups()
        {
            tblLookupDataAccess objtblLookupDataAccess = new tblLookupDataAccess();

            try
            {
                return objtblLookupDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataTable GetLookupByLookupType(string parent)
        {
            tblLookupDataAccess objtblLookupDataAccess = new tblLookupDataAccess();
            try
            {
                return objtblLookupDataAccess.GetLookupByLookupType(parent);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataTable GetDocDetailRecords(string parent, int language)
        {
            tblLookupDataAccess objtblLookupDataAccess = new tblLookupDataAccess();
            try
            {
                if (parent != null )
                    return objtblLookupDataAccess.GetDocDetailRecords(parent, language);
                else
                    return objtblLookupDataAccess.GetDocDetailRecords("341",language);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        //public DataTable GetRecords(string parent, Guid guid, Guid Aperson,int language)
        //{
        //    tblLookupDataAccess objtblLookupDataAccess = new tblLookupDataAccess();

        //    try
        //    {
        //        if (parent != null && guid != null)
        //            return objtblLookupDataAccess.GetRecords(parent, guid, Aperson, language);
        //        else
        //            return objtblLookupDataAccess.GetRecords("341", guid, Aperson, language);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw (ex);
        //    }
        //}
         public List<tblLookup> GetLookupInfo(string parent, Guid guid)
        {
            tblLookupDataAccess objtblLookUpTypeDataAccess = new tblLookupDataAccess();

            try
            {
                return objtblLookUpTypeDataAccess.GetLookupInfo(parent, guid);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<tblLookup> GetRelationtblLookups()
        {
            tblLookupDataAccess objtblLookupDataAccess = new tblLookupDataAccess();

            try
            {
                return objtblLookupDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        
        public bool Exists(int ID)
        {
            tblLookupDataAccess objtblLookupDataAccess = new tblLookupDataAccess();
            try
            {
                return objtblLookupDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
       


    }
}