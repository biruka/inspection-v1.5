﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.ComponentModel;

namespace CUSTOR.Business
{
    [DataObjectAttribute]
    public class tblOrganizationBusiness
    {


        public bool Delete(int ID)
        {
            tblOrganizationDataAccess objtblOrganizationDataAccess = new tblOrganizationDataAccess();
            try
            {
                objtblOrganizationDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public tblOrganization GettblOrganization(int ID)
        {
            tblOrganizationDataAccess objtblOrganizationDataAccess = new tblOrganizationDataAccess();

            try
            {
                return objtblOrganizationDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public List<tblOrganization> GettblOrganizations()
        {
            tblOrganizationDataAccess objtblOrganizationDataAccess = new tblOrganizationDataAccess();

            try
            {
                return objtblOrganizationDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(int ID)
        {
            tblOrganizationDataAccess objtblOrganizationDataAccess = new tblOrganizationDataAccess();
            try
            {
                return objtblOrganizationDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdatetblOrganization(tblOrganization objtblOrganization)
        {
            tblOrganizationDataAccess objtblOrganizationDataAccess = new tblOrganizationDataAccess();
            try
            {
                objtblOrganizationDataAccess.Update(objtblOrganization);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InserttblOrganization(tblOrganization objtblOrganization)
        {
            tblOrganizationDataAccess objtblOrganizationDataAccess = new tblOrganizationDataAccess();
            try
            {
                objtblOrganizationDataAccess.Insert(objtblOrganization);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}