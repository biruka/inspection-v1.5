﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
//using System.Web.SessionState;
//using System.Web.UI;
//using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
//using DevExpress.Web.ASPxTreeList.Export;
using System.ComponentModel;
using CUSTOR.Domain;
using CUSTOR.DataAccess;



namespace CUSTOR.Bussiness
{


    [Serializable]
    [DataObjectAttribute]

    public  class ParentMessageBLL
    {
        //private string ConnectionString
        //{
        //    get
        //    {
        //        return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;ለ
        //    }
        //}

        public MessageExchangeBO GetMaxParentID()
        {
            ParentMessageDAL objmaxparentDAC = new ParentMessageDAL();

            try
            {
                return objmaxparentDAC.GetMaxParent();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
     public List<MessageExchangeBO> GetParentMessages(String Condition)
     {
         ParentMessageDAL objDAL = new ParentMessageDAL();
         return objDAL.GetParentMessages(Condition);
     }

     public List<MessageExchangeBO> GetAllReceivedParentMessages()
     {
         ParentMessageDAL objDAL = new ParentMessageDAL();
         return objDAL.GetAllReceivedParentMessages();
     }

     public MessageExchangeBO GetParentMessage(Int32 ID)
     {
         ParentMessageDAL objDAL = new ParentMessageDAL();
         return objDAL.GetParentMessage(ID);
     }

        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public void Insert(MessageExchangeBO item)
        {
            ParentMessageDAL objDAL = new ParentMessageDAL();
            // item.ParentID = Int32.Parse(Session["ID"].ToString());
            objDAL.Insert(item);
            // Data.Add(item);

        }

        public void Update(MessageExchangeBO objMessage)
        {
            ParentMessageDAL objDAL = new ParentMessageDAL();

            objDAL.Update(objMessage);
        }

        public void Delete(MessageExchangeBO item)
        {
           // MessageExchangeBO storedItem = FindItem(item.ParentMessageID);
          //  Data.Remove(storedItem);
        }

       public bool DeleteMessage(int ID)
       {
           ParentMessageDAL objMsgDataAccess = new ParentMessageDAL();
           try
           {
               objMsgDataAccess.DeleteMessage(ID);
               return true;
           }
           catch (Exception ex)
           {
               throw (ex);
           }
       }

      [DataObjectMethod(DataObjectMethodType.Select, true)]
       public DataTable poplateOrganization()
        {
            ParentMessageDAL objDAL = new ParentMessageDAL();
           return  objDAL.poplateOrganization();

        }

      [DataObjectMethod(DataObjectMethodType.Select, true)]
      public DataTable BindMessageAttachments(Int32 id)
      {
          ParentMessageDAL objAttachmentDataAccess = new ParentMessageDAL();

          try
          {
             // id = 0;// (Int32)Session["ID"];
              return objAttachmentDataAccess.BindMessageAttachments(id);
          }
          catch (Exception ex)
          {
              throw (ex);
          }
      }

      public bool InserttblMessageExchangeAttachment(AttachmentMessageExchangeBO objtblMessageExchangeAttachment)
      {
          ParentMessageDAL objtblMessageExchangeAttachmentDataAccess = new ParentMessageDAL();
          try
          {
             // objtblMessageExchangeAttachment.ID=//Int32.Parse(Session["ParentMessageID"].ToString());
              objtblMessageExchangeAttachmentDataAccess.InsertMessageAttachement(objtblMessageExchangeAttachment);
              return true;
          }
          catch (Exception ex)
          {
              throw (ex);
          }
      }

      public bool MessageApproved(Int32 MessageID)
      {
          ParentMessageDAL objtblMessage = new ParentMessageDAL();
          try
          {

              return objtblMessage.MessageApproved(MessageID);

          }
          catch (Exception ex)
          {
              throw (ex);
          }
      }
        
       
    }
}
