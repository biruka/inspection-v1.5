﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;

namespace CUSTOR.Bussiness
{
    public class tblDocumentTypeBussiness
    {


        public bool Delete(Guid ID)
        {
            tblDocumentTypeDataAccess objtblDocumentTypeDataAccess = new tblDocumentTypeDataAccess();
            try
            {
                objtblDocumentTypeDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public tblDocumentType GettblDocumentType(Guid ID)
        {
            tblDocumentTypeDataAccess objtblDocumentTypeDataAccess = new tblDocumentTypeDataAccess();

            try
            {
                return objtblDocumentTypeDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataTable SearchDocumentTypes(string documentTypeDescription, string documentTypeDescriptionRegional, bool isGeezFont)
        {
            tblDocumentTypeDataAccess objtblDocumentTypeDataAccess = new tblDocumentTypeDataAccess();
            try
            {
                return objtblDocumentTypeDataAccess.SearchDocumentTypes(documentTypeDescription, documentTypeDescriptionRegional, isGeezFont);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<tblDocumentType> GetDocTypeList(int Language)
        {
            tblDocumentTypeDataAccess objtblDocumentTypeDataAccess = new tblDocumentTypeDataAccess();

            try
            {
                return objtblDocumentTypeDataAccess.GetDocTypeList(Language);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<tblDocumentType> GettblDocumentTypes()
        {
            tblDocumentTypeDataAccess objtblDocumentTypeDataAccess = new tblDocumentTypeDataAccess();

            try
            {
                return objtblDocumentTypeDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool Exists(string description, string descriptionReginalName, bool isGeez)
        {
            tblDocumentTypeDataAccess objtblDocumentTypeDataAccess = new tblDocumentTypeDataAccess();
            try
            {
                return objtblDocumentTypeDataAccess.Exists(description, descriptionReginalName, isGeez);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool UpdatetblDocumentType(tblDocumentType objtblDocumentType)
        {
            tblDocumentTypeDataAccess objtblDocumentTypeDataAccess = new tblDocumentTypeDataAccess();
            try
            {
                objtblDocumentTypeDataAccess.Update(objtblDocumentType);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool InserttblDocumentType(tblDocumentType objtblDocumentType)
        {
            tblDocumentTypeDataAccess objtblDocumentTypeDataAccess = new tblDocumentTypeDataAccess();
            try
            {
                return objtblDocumentTypeDataAccess.Insert(objtblDocumentType); ;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}