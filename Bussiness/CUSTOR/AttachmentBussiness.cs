﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;
using System.ComponentModel;
namespace CUSTOR.Bussiness
{
    [DataObjectAttribute]
    public class AttachmentBussiness
    {
        public struct Data
        {
            public string FileNumber;
            public string AttachmentTitle;
            public string DocumentTitle;
            public string FileType;
        }
        public Data[] Attdata;
     
        public bool Delete(Guid ID)
        {
            AttachmentDataAccess objAttachmentDataAccess = new AttachmentDataAccess();
            try
            {
                objAttachmentDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
       [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecords(Guid person, string FileType, string language)
        {
            AttachmentDataAccess objAttachmentDataAccess = new AttachmentDataAccess();

            try
            {
                return objAttachmentDataAccess.GetRecords(person, FileType, language);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
     
        public Attachment GetAttachment(Guid ID)
        {
            AttachmentDataAccess objAttachmentDataAccess = new AttachmentDataAccess();

            try
            {
                return objAttachmentDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<Attachment> GetAttachments()
        {
            AttachmentDataAccess objAttachmentDataAccess = new AttachmentDataAccess();

            try
            {
                return objAttachmentDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(Guid ID)
        {
            AttachmentDataAccess objAttachmentDataAccess = new AttachmentDataAccess();
            try
            {
                return objAttachmentDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdateAttachment(Attachment objAttachment)
        {
            AttachmentDataAccess objAttachmentDataAccess = new AttachmentDataAccess();
            try
            {
                objAttachmentDataAccess.Update(objAttachment);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InsertAttachment(Attachment objAttachment)
        {
            AttachmentDataAccess objAttachmentDataAccess = new AttachmentDataAccess();
            try
            {
                objAttachmentDataAccess.Insert(objAttachment);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}