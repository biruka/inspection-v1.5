﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.ComponentModel;
using System.Data;

namespace CUSTOR.Bussiness
{
    [DataObjectAttribute]
    public class InspectionBussiness
    {

       [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(Guid ID)
        {
            InspectionDataAccess objInspectionDataAccess = new InspectionDataAccess();
            try
            {
                objInspectionDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecords(Guid EmployeeGUID, string Activitytype)
        {
            InspectionDataAccess objInspectionDataAccess = new InspectionDataAccess();

            try
            {
                return objInspectionDataAccess.GetRecords(EmployeeGUID, Activitytype);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public tblInspection GetInspection(Guid ID)
        {
            InspectionDataAccess objInspectionDataAccess = new InspectionDataAccess();

            try
            {
                return objInspectionDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<tblInspection> GetInspections()
        {
            InspectionDataAccess objInspectionDataAccess = new InspectionDataAccess();

            try
            {
                return objInspectionDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool Exists(Guid ID)
        {
            InspectionDataAccess objInspectionDataAccess = new InspectionDataAccess();
            try
            {
                return objInspectionDataAccess.Exists(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool IsApproved(Guid ID)
        {
            InspectionDataAccess objInspectionDataAccess = new InspectionDataAccess();
            try
            {
                return objInspectionDataAccess.IsApproved(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
         [DataObjectMethod(DataObjectMethodType.Update, true)]
        public bool UpdateInspection(tblInspection objInspection)
        {
            InspectionDataAccess objInspectionDataAccess = new InspectionDataAccess();
            try
            {
                objInspectionDataAccess.Update(objInspection);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
         public bool UpdateInspectionAnswer(tblInspection objInspection)
         {
             InspectionDataAccess objInspectionDataAccess = new InspectionDataAccess();
             try
             {
                 objInspectionDataAccess.AnswerUpdate(objInspection);
                 return true;
             }
             catch (Exception ex)
             {
                 throw (ex);
             }
         }
         public bool UpdateApproval(tblInspection objInspection)
         {
             InspectionDataAccess objInspectionDataAccess = new InspectionDataAccess();
             try
             {
                 objInspectionDataAccess.ApprovalUpdate(objInspection);
                 return true;
             }
             catch (Exception ex)
             {
                 throw (ex);
             }
         }
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public bool InsertInspection(tblInspection objInspection)
        {
            InspectionDataAccess objInspectionDataAccess = new InspectionDataAccess();
            try
            {
                objInspectionDataAccess.Insert(objInspection);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}