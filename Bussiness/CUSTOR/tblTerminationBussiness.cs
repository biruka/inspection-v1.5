﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using System.Data;
using System.ComponentModel;

namespace CUSTOR.Bussiness
{
    [DataObjectAttribute]
    public class tblTerminationBussiness
    {


        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public DataTable GetRecords(Guid person, string language)
        {
            tblTerminationDataAccess objtblTerminationDataAccess = new tblTerminationDataAccess();

            try
            {
                return objtblTerminationDataAccess.GetRecords(person, language);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public tblTermination GettblTermination(Guid ID)
        {
            tblTerminationDataAccess objtblTerminationDataAccess = new tblTerminationDataAccess();

            try
            {
                return objtblTerminationDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<tblTermination> GettblTerminations()
        {
            tblTerminationDataAccess objtblTerminationDataAccess = new tblTerminationDataAccess();

            try
            {
                return objtblTerminationDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
      


    }
}