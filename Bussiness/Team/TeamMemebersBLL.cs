﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CUSTOR.Domain;
using CUSTOR.DataAccess;


namespace CUSTOR.Bussiness
{
    public class tblTeamMembersBussiness
    {
        public bool Delete(Guid teamMemberGuid)
        {
            tblTeamMembersDataAccess objtblTeamMembersDataAccess = new tblTeamMembersDataAccess();
            try
            {
                objtblTeamMembersDataAccess.Delete(teamMemberGuid);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public tblTeamMembers GettblTeamMembers(Guid ID)
        {
            tblTeamMembersDataAccess objtblTeamMembersDataAccess = new tblTeamMembersDataAccess();

            try
            {
                return objtblTeamMembersDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<tblTeamMembers> GetRecordByTeamGuid(Guid ID)
        {
            tblTeamMembersDataAccess objtblTeamMembersDataAccess = new tblTeamMembersDataAccess();
            try
            {
                return objtblTeamMembersDataAccess.GetRecordByTeamGuid(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<tblTeamMembers> GetRecordByPersonGuid(Guid ID)
        {
            tblTeamMembersDataAccess objtblTeamMembersDataAccess = new tblTeamMembersDataAccess();
            try
            {
                return objtblTeamMembersDataAccess.GetRecordByPersonGuid(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public List<tblTeamMembers> GettblTeamMemberss(Guid teamGuid)
        {
            tblTeamMembersDataAccess objtblTeamMembersDataAccess = new tblTeamMembersDataAccess();

            try
            {
                return objtblTeamMembersDataAccess.GetList(teamGuid);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool Exists(Guid teamGuid, Guid personGuid)
        {
            tblTeamMembersDataAccess objtblTeamMembersDataAccess = new tblTeamMembersDataAccess();
            try
            {
                return objtblTeamMembersDataAccess.Exists(teamGuid, personGuid);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool UpdatetblTeamMembers(tblTeamMembers objtblTeamMembers)
        {
            tblTeamMembersDataAccess objtblTeamMembersDataAccess = new tblTeamMembersDataAccess();
            try
            {
                objtblTeamMembersDataAccess.Update(objtblTeamMembers);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool InserttblTeamMembers(tblTeamMembers objtblTeamMembers)
        {
            tblTeamMembersDataAccess objtblTeamMembersDataAccess = new tblTeamMembersDataAccess();
            try
            {
                objtblTeamMembersDataAccess.Insert(objtblTeamMembers);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public DataTable GetUnGroupedMembers(int unitID)
        {
            tblTeamMembersDataAccess obj = new tblTeamMembersDataAccess();

            return obj.GetUnGroupedMembers(unitID);
        }

        public bool DoesEmployeeTeamLeader(Guid personGuid, Guid organizationGuid)
        {
            tblTeamMembersDataAccess objtblTeamMembersDataAccess = new tblTeamMembersDataAccess();
            try
            {
                return objtblTeamMembersDataAccess.DoesEmployeeTeamLeader(personGuid, organizationGuid);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

    }
}
