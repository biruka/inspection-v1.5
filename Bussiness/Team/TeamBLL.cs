﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CUSTOR.DataAccess;
using CUSTOR.Domain;

namespace CUSTOR.Bussiness
{
    public class tblTeamBussiness
    {


        public bool Delete(Guid ID)
        {
            tblTeamDataAccess objtblTeamDataAccess = new tblTeamDataAccess();
            try
            {
                objtblTeamDataAccess.Delete(ID);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public tblTeam GettblTeam(Guid ID)
        {
            tblTeamDataAccess objtblTeamDataAccess = new tblTeamDataAccess();

            try
            {
                return objtblTeamDataAccess.GetRecord(ID);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public List<tblTeam> GettblTeams()
        {
            tblTeamDataAccess objtblTeamDataAccess = new tblTeamDataAccess();

            try
            {
                return objtblTeamDataAccess.GetList();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool CheckTeamExistance(string teamNameAmh, string teamName)
        {
            tblTeamDataAccess objtblTeamDataAccess = new tblTeamDataAccess();
            try
            {
                return objtblTeamDataAccess.CheckTeamExistance(teamNameAmh, teamName);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool UpdatetblTeam(tblTeam objtblTeam)
        {
            tblTeamDataAccess objtblTeamDataAccess = new tblTeamDataAccess();
            try
            {
                objtblTeamDataAccess.Update(objtblTeam);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool InserttblTeam(tblTeam objtblTeam)
        {
            tblTeamDataAccess objtblTeamDataAccess = new tblTeamDataAccess();
            try
            {
                objtblTeamDataAccess.Insert(objtblTeam);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


    }
}
