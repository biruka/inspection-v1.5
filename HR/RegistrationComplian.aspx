﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RegistrationComplian.aspx.cs" Inherits="RegistrationComplian" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>








<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>




<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>
<%@ Register Src="~/Controls/ConfirmBox.ascx" TagPrefix="uc1" TagName="ConfirmBox" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        // <![CDATA[
        var fieldSeparator = "|";
        function FileUploadStart() {
            document.getElementById("uploadedListFiles").innerHTML = "";
        }
        function FileUploaded(s, e) {
            if (e.isValid) {
                var linkFile = document.createElement("a");
                var indexSeparator = e.callbackData.indexOf(fieldSeparator);
                var fileName = e.callbackData.substring(0, indexSeparator);
                var pictureUrl = e.callbackData.substring(indexSeparator + fieldSeparator.length);
                var date = new Date();
                var imgSrc = "Attached/" + pictureUrl + "?dx=" + date.getTime();
                linkFile.innerHTML = fileName;
                linkFile.setAttribute("href", imgSrc);
                linkFile.setAttribute("target", "_blank");
                var container = document.getElementById("uploadedListFiles");
                container.appendChild(linkFile);
                container.appendChild(document.createElement("br"));
            }
        }
        // ]]> 
    </script>

    <script type="text/javascript">
        // <![CDATA[
        function DoNewClick(s, e) {
            pnldata.SetVisible(true);
            //btnDeleteClient.SetEnabled(true);
            btnSaveClient.SetEnabled(true);
            cbdata.PerformCallback('New');
        }
        function DoSearchClick(s, e) {

            cbdata.PerformCallback('Search');
        }
        function DoSaveClick(s, e) {
            cbdata.PerformCallback('Save');
        }
        //===========================Grid===============
        var rowVisibleIndex;
        var strID;
        var strGuid;

        function DvgrdregEdit_CustomButtonClick(s, e) {
            if (e.buttonID != 'Del' && e.buttonID != 'Edit') return;
            if (e.buttonID == 'Del') {
                rowVisibleIndex = e.visibleIndex;
                strGuid = s.GetRowKey(e.visibleIndex);
                
                //DvgrdregEdit.GetRowValues(e.visibleIndex, 'ID', ShowPopup);
                ShowPopup();
                //ShowError("You can not delete an Complain, instead uncheck the 'Is Active' CheckBox");
            }
            if (e.buttonID == 'Edit') {
                btnSaveClient.SetEnabled(true);
                 DoToggle();
                 DvgrdregEdit.GetRowValues(e.visibleIndex, "ComplaintGUID", OnGetSelectedFieldValues);
                //cbdata.PerformCallback('Edit' + '|' + command);
               
            }
        }
        function DoToggle()
        {
            pnldata.SetVisible(true);
        }
        function OnEndCallback(s, e) {

            if (s.cpStatus == "SUCCESS") {
                //btnDeleteClient.SetEnabled(true);
                btnSaveClient.SetEnabled(true);

                if (s.cpMessage == '') {

                    return;
                }

                if (s.cpAction == "save") {

                    pnldata.SetVisible(false);
                  

                }
                if (s.cpAction == "Delete") {
                    pnldata.SetVisible(false);

                }
                ShowSuccess(s.cpMessage);

            }
            else if (s.cpStatus == "ERROR") {
                ShowError(s.cpMessage);

            }

        }

        function OnGetSelectedFieldValues(values) {

            cbdata.PerformCallback('Edit' + '|' + values);
            pnldata.SetVisible(true);


        }

        function ShowPopup(rowId) {

            popupControl.Show();
            btnYes.Focus();

        }

        function btnYes_Click(s, e) {

            ClosePopup(true);
        }

        function btnNo_Click(s, e) {
            ClosePopup(false);
        }

        function ClosePopup(result) {

            popupControl.Hide();
            //if (result) DeleteGridRow(rowVisibleIndex);
            //cbdata.PerformCallback('Delete');

            if (result) {

                //DvgrdregEdit.GetRowValues(rowVisibleIndex, "ComplaintGUID", OnGetSelectedDeleteField);

                //cbdata.PerformCallback('Delete' + '|' + strGuid);
            }

        }
        function OnGetSelectedDeleteField(values) {

            cbdata.PerformCallback('Delete' + '|' + values);

        }
        function DeleteGridRow(visibleIndex) {
            var index = DvgrdregEdit.GetFocusedRowIndex();
            DvgrdregEdit.DeleteRow(index);
        }
        function DoRefreshClick(s, e) {

            //btnDeleteClient.SetEnabled(false);

            btnSaveClient.SetEnabled(false);

            pnldata.SetVisible(false);

            cbdata.PerformCallback('Refresh');

        }
    </script>
<script  type="text/javascript">
    function pageLoad(sender, args) {

        //btnDeleteClient.SetEnabled(false);

        btnSaveClient.SetEnabled(false);

        pnldata.SetVisible(false);

    }
    </script>




    <%-- <script type="text/javascript">
         // <![CDATA[
         function DoNewClick(s, e) {
             pnldata.SetVisible(true);
             //btnDeleteClient.SetEnabled(false);
             btnSaveClient.SetEnabled(true);
             cbdata.PerformCallback('New');

         }
         //function DoSearchClick(s, e) {

         //    cbdata.PerformCallback('Search');
         //}
         function DoSaveClick(s, e) {
             cbdata.PerformCallback('Save');
         }
         //===========================Grid===============
         var rowVisibleIndex;
         var strID;
         var strGuid;

         function DvgrdregEdit_CustomButtonClick(s, e) {
             if (e.buttonID != 'Del' && e.buttonID != 'Edit') return;
             if (e.buttonID == 'Del') {
                 //btnDeleteClient.SetEnabled(true);
                 //pnldata.SetVisible(true);
                 rowVisibleIndex = e.visibleIndex;
                
                 strGuid = s.GetRowKey(e.visibleIndex);
                 //DvgrdregEdit.GetRowValues(rowVisibleIndex,'ID', ShowPopup);
                 ShowPopup();
                 
             }
             if (e.buttonID == 'Edit') {
                 btnSaveClient.SetEnabled(true);
                 pnldata.SetVisible(true);
                 //DoToggle();

                  DvgrdregEdit.GetRowValues(e.visibleIndex, "ComplaintGUID", OnGetSelectedFieldValues);
                 //cbdata.PerformCallback('Edit' + '|' + command);

             }
         }
         function DoDeleteClick(s, e) {
             ShowPopup();

         }
         function OnEndCallback(s, e) {

             if (s.cpStatus == "SUCCESS") {
                 //btnDeleteClient.SetEnabled(true);
                 btnSaveClient.SetEnabled(true);

                 if (s.cpMessage == '') {

                     return;
                 }

                 if (s.cpAction == "save") {
                     pnldata.SetVisible(false);
                     ShowSuccess(s.cpMessage);
                 }
                 if (s.cpAction == "Delete") {
                     pnldata.SetVisible(false);

                 }
                 ShowSuccess(s.cpMessage);

             }
             else if (s.cpStatus == "ERROR") {
                 ShowError(s.cpMessage);

             }
             
         }

         function OnGetSelectedFieldValues(values) {

             cbdata.PerformCallback('Edit' + '|' + values);

         }

         function ShowPopup(rowId) {

             popupControl.Show();
             btnYes.Focus();
         }

         function DoRefreshClick(s, e) {

             //btnDeleteClient.SetEnabled(false);
             btnSaveClient.SetEnabled(false);
             pnldata.SetVisible(false);
             cbdata.PerformCallback('Refresh');

         }

         function btnYes_Click(s, e) {

             ClosePopup(true);
         }

         function btnNo_Click(s, e) {
             ClosePopup(false);
         }

         function ClosePopup(result) {
             //popupControl.Hide();
            

             //if (result) {

                 var rowIndex = DvgrdregEdit.GetFocusedRowIndex();

                 //cbdata.PerformCallback('Delete' + '|' + strGuid);
                 //DvgrdregEdit.GetRowValues(rowVisibleIndex, "ComplaintGUID", OnGetSelectedDeleteField)
             popupControl.Hide();
             //if (result) DeleteGridRow(rowVisibleIndex);
             //cbdata.PerformCallback('Delete');
             if (result) {

                 cbdata.PerformCallback('Delete' + '|' + strGuid);
                 //DvgrdregEdit.GetRowValues(rowIndex, "ComplaintGUID", OnGetSelectedDeleteField);
             }
                 
          }
                         
         function OnGetSelectedDeleteField(values) {

             cbdata.PerformCallback('Delete' + '|' + values);

         }
         function DeleteGridRow(visibleIndex) {

             var index = DvgrdregEdit.GetFocusedRowIndex();
                                   
             DvgrdregEdit.DeleteRow(index);
         }
         //function DoToggle() {
         //    pnldata.SetVisible(true);
         //    }

          </script>
     <script  type="text/javascript">
         function pageLoad(sender, args) {
             //btnDeleteClient.SetEnabled(false);
             btnSaveClient.SetEnabled(false);
             pnldata.SetVisible(false);

         }
    </script>--%>
    <style type="text/css">
        .auto-style1 {
            width: 336px;
        }
        .auto-style2 {
            width: 323px;
        }
        .auto-style3 {
            width: 311px;
        }
        .auto-style4 {
            width: 303px;
        }
        .auto-style5 {
            width: 295px;
        }
        .auto-style6 {
            width: 200px;
        }
        .auto-style7 {
            width: 182px;
        }
        .auto-style9 {
            width: 126px;
        }
        .auto-style10 {
            width: 169px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:Alert runat="server" ID="Alert" />

     <div style="width: 993px">
   
        <table id="tblOuter">
            <tr>
                <td class="auto-style5">
                
                    <div>
                        &nbsp;</div>
 
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlMenu0" runat="server" CssClass="RoundPanelToolbar" width="950px">
                        <table cellpadding="0" cellspacing="0" style="width: 100%;padding:0px; " width="990px">
                            <tr>
                                <td style="width:20%"> </td>
                                <td >
                                    <table >
                                        <tr>
                                            <td>
                                                <dx:ASPxButton ID="btnNew" runat="server" AutoPostBack="False" ClientInstanceName="btnNewClient" Font-Names="Visual Geez Unicode" Font-Size="Small" HorizontalAlign="Center" Image-Url="~/Controls/ToolbarImages/new.gif" Text="አዲስ" Theme="Office2010Silver" VerticalAlign="Middle" Width="70px">
                                                     <ClientSideEvents Click="function(s, e) {
	                                                                    DoNewClick();
                                                                    }" />
                                                     <image url="~/Controls/ToolbarImages/new.gif">
                                                    </image>
                                                </dx:ASPxButton>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnSave" runat="server" AutoPostBack="False" ClientInstanceName="btnSaveClient" Font-Names="Visual Geez Unicode" Font-Size="Small" HorizontalAlign="Center" Image-Url="~/Controls/ToolbarImages/save.gif" ImagePosition="Left" Text="አስቀምጥ" Theme="Office2010Silver" VerticalAlign="Middle" Width="70px" TabIndex="11">
                                                 <ClientSideEvents Click="function(s, e) {
	                                                                    DoSaveClick();
                                                                    }" />
                                                 </dx:ASPxButton>
                                            </td>
                                            <td>
                                               <%-- <dx:ASPxButton ID="btnDelete" runat="server" AutoPostBack="False" ClientInstanceName="btnDeleteClient" EnableClientSideAPI="True" Font-Names="Visual Geez Unicode" Font-Size="Small" HorizontalAlign="Center" Image-Url="~/Controls/ToolbarImages/delete.gif" Text="ሰርዝ" Theme="Office2010Silver" VerticalAlign="Middle" Width="70px">
                                                     <ClientSideEvents Click="function(s, e) {
	                                                         DoDeleteClick();
                                                         }" />
                                                     <image url="~/Controls/ToolbarImages/delete.gif">
                                                    </image>
                                                </dx:ASPxButton>--%>
                                            </td>
                                            <td>
                                                &nbsp;</td>
                                            <td>
                                                <dx:ASPxTextBox ID="txtSearchOrg" runat="server" Width="250px" TabIndex="14">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnFindOrg" runat="server" AutoPostBack="False" ClientInstanceName="btnFindClient" Font-Names="Visual Geez Unicode" Font-Size="Small" HorizontalAlign="Center" Image-Url="~/Controls/ToolbarImages/search.gif" Text="ፈልግ" Theme="Office2010Silver" VerticalAlign="Middle" Width="70px" TabIndex="15">
                                                    <ClientSideEvents Click="function(s, e) {
	                                                         DoSearchClick();
                                                         }" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width:20%">&nbsp;</td>
                                <td style="width:10%"></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                
                <td>
                    <dx:aspxcallbackpanel ID="cbData"  ClientInstanceName="cbdata" OnCallback="CallbackPanel_Callback"  runat="server" Width="960px">
                        <ClientSideEvents EndCallback="OnEndCallback" /> 
                          <PanelCollection>
<dx:PanelContent runat="server" SupportsDisabledAttribute="True">
    <br />
    <dx:ASPxPanel runat="server" ID="pnlData" ClientInstanceName="pnldata" Width="960px">
        <panelcollection>
            <dx:PanelContent runat="server" SupportsDisabledAttribute="True">
                <table >
                     <tr>
                <td style="text-align: right" class="auto-style10">
                 <asp:Label ID="lblFullName" runat="server" Text="ሙሉ ስም " AssociatedControlID="txtFullName"></asp:Label>
                  <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*"></asp:Label>
                  
                </td>
                <td class="auto-style6">
                     <dx:ASPxTextBox ID="txtFullName" runat="server" Width="270px" Height="36px" >
                          <%--  <ValidationSettings ErrorDisplayMode="None" SetFocusOnError="True" RequiredField-IsRequired="true">
                           <RequiredField IsRequired="True"></RequiredField>
                            </ValidationSettings>--%>
                        </dx:ASPxTextBox>
                </td>
                <td style="text-align: right" class="auto-style9">
                    <asp:Label ID="lblComplainType" runat="server" AssociatedControlID="cboComplainType" Text="የቅሬታው አይነት "></asp:Label>
                    <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*"></asp:Label>
                         </td>
                <td class="auto-style7">
                      <dx:ASPxComboBox ID="cboComplainType" runat="server" EnableIncrementalFiltering="True" EnableViewState="False" IncrementalFilteringMode="StartsWith" TabIndex="1" ValueField="Name" Width="270px">
                      </dx:ASPxComboBox>

                </td>
            </tr>
            <tr>
                <td style="text-align: right" class="auto-style10">
                      <asp:Label ID="lblTelephone" runat="server" Text="ስልክ ቁጥር " AssociatedControlID="txtTelephone">
                        </asp:Label>
                </td>
                <td class="auto-style6">
                    <dx:ASPxTextBox ID="txtTelephone" runat="server" Width="270px" Height="36px" TabIndex="2">
                            <%--<MaskSettings Mask="+000 (000) 00-00-00" IncludeLiterals="None" />--%>
                            <%--<ValidationSettings ErrorDisplayMode="None" SetFocusOnError="True" RequiredField-IsRequired="true">--%>
                            <%--<RequiredField IsRequired="True"></RequiredField>--%>
                            <%--</ValidationSettings>--%>
                        </dx:ASPxTextBox>
                </td>
                <td class="auto-style9" style="text-align: right">
                    <asp:Label ID="lblemail" runat="server" AssociatedControlID="lblEmail" Text="ኢሜል "></asp:Label>
                </td>
                <td class="auto-style7">
                      <dx:ASPxTextBox ID="txtEmail" runat="server" Height="36px" TabIndex="3" Width="270px">
                          <validationsettings errordisplaymode="None" setfocusonerror="True">
                              <regularexpression validationexpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                          </validationsettings>
                      </dx:ASPxTextBox>
                </td>

            </tr>
            <tr>
                <td style="text-align: right" class="auto-style10">
                      <asp:Label ID="ASPxLabel5" runat="server" Text="ተቋም " AssociatedControlID="cboOrganization"></asp:Label>
                      <asp:Label ID="Label3" runat="server" ForeColor="Red" Text="*"></asp:Label>
                </td>
                <td class="auto-style6">
                     <dx:ASPxComboBox ID="cboOrganization" runat="server" Width="270px" Height="36px" TextField="Name"
                            ValueField="Name" IncrementalFilteringMode="StartsWith" EnableViewState="false" TabIndex="4">
                           <%-- <ValidationSettings ErrorDisplayMode="None" SetFocusOnError="True" RequiredField-IsRequired="true">
                             <RequiredField IsRequired="True"></RequiredField>
                            </ValidationSettings>--%>
                        </dx:ASPxComboBox>
                </td>
                <td class="auto-style9" style="text-align: right">
                    <asp:Label ID="lblComplaintRemark" runat="server" AssociatedControlID="txtCompliantReason" Text="የቅሬታ መነሻ "></asp:Label>
                    <asp:Label ID="Label4" runat="server" ForeColor="Red" Text="*"></asp:Label>
                </td>
                <td style="text-align: right" class="auto-style7">
                       <dx:ASPxTextBox ID="txtCompliantReason" runat="server" Height="36px" TabIndex="5" Width="270px">
                       </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td valign="top" style="text-align: right" class="auto-style10" >
                    <asp:Label ID="lblRemark" runat="server" Text="ዝርዝር መረጃ "></asp:Label>
                    <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="*"></asp:Label>
                </td>
                <td colspan="3">
                     <dx:ASPxMemo ID="txtComplaintRemark" runat="server" Width="100%" Height="150px" TabIndex="6"></dx:ASPxMemo>
                </td>
            </tr>
            <tr>
                <td class="auto-style10">
                    &nbsp;</td>
                <td valign="top" class="auto-style6">
                 
                    <dx:ASPxUploadControl ID="fuAttachment" runat="server" ClientInstanceName="UploadControl" FileInputCount="3" Height="100%" OnFileUploadComplete="UploadControl_FileUploadComplete" ShowAddRemoveButtons="True" ShowProgressPanel="True" ShowUploadButton="True" Theme="Office2010Silver" TabIndex="8" Width="273px">
                         
                        <ValidationSettings AllowedFileExtensions=".jpg, .jpeg, .jpe, .gif" GeneralErrorText="ሰነዱ በትክክል አልተቀመጠም:: እባክዎ እንደገና ያያይዙ" NotAllowedFileExtensionErrorText="የሰነዱ አይነት የተፈቀደ አይደለም:: እባክዎ እንደገና ያያይዙ">
                        </ValidationSettings>
                        <ClientSideEvents FileUploadComplete="function(s, e) { FileUploaded(s, e) }" FileUploadStart="function(s, e) { FileUploadStart(); }" />
                        <AddButton Text="ጨምር">
                        </AddButton>
                        <BrowseButton Text="አስስ...">
                        </BrowseButton>
                        <RemoveButton Text="ሰርዝ">
                        </RemoveButton>
                        <UploadButton Text="አያይዝ">
                        </UploadButton>
                    </dx:ASPxUploadControl>
                 
                </td>
                <td class="auto-style9" >
                    <dx:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" ClientInstanceName="RoundPanel" HeaderText="የተያያዙ ሰነዶች (jpeg, gif)" Height="100%" TabIndex="10" Width="325%">
                        <panelcollection>
                            <dx:PanelContent runat="server" SupportsDisabledAttribute="True">
                                <div id="uploadedListFiles" style="height: 150px; font-family: Arial;">
                                </div>
                            </dx:PanelContent>
                        </panelcollection>
                    </dx:ASPxRoundPanel>
                </td>
            </tr>
                </table>
            </dx:PanelContent>
        </panelcollection>
    </dx:ASPxPanel>
     <table>
                                   <tr>
                                       <td>
                                             <dx:ASPxGridView ID="DvgrdregEdit" runat="server"  KeyFieldName="ComplaintGUID" ClientInstanceName="DvgrdregEdit" 
                                                   AutoGenerateColumns="False" Theme="Office2010Silver" 
                                                  Width="900px" EnableTheming="True" Font-Names="Visual Geez Unicode" 
                                                  Font-Size="Small" OnPageIndexChanged="DvgrdregEdit_PageIndexChanged">
                                                 <clientsideevents custombuttonclick="DvgrdregEdit_CustomButtonClick" />
                                           <Columns>
                                            <dx:GridViewDataTextColumn Caption="ID" FieldName="ComplaintGUID" ShowInCustomizationForm="True" VisibleIndex="0" Visible="False">
                                             </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="ሙሉ ስም" FieldName="ReportedBy" VisibleIndex="2">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="ቀን:" FieldName="ReportedDate" VisibleIndex="3">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="የጥቆማ አይነት:" FieldName="ComplainType" VisibleIndex="4">
                                            </dx:GridViewDataTextColumn>
                                           <%-- <dx:GridViewDataTextColumn VisibleIndex="5">
                                                <DataItemTemplate>
                                                    <asp:LinkButton ID="lnkupdatecompain" runat="server" OnClick="lnkUpdateCompain_Click">አርም</asp:LinkButton>
                                                </DataItemTemplate>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn VisibleIndex="6">
                                                <DataItemTemplate>
                                                    <asp:LinkButton ID="lnkdeletecomplain" runat="server" OnClick="lnkDeleteComplain_Click">ሰርዝ</asp:LinkButton>
                                                </DataItemTemplate>
                                            </dx:GridViewDataTextColumn>--%>
                                             <dx:GridViewCommandColumn ShowInCustomizationForm="True" Width="100px" VisibleIndex="0">
                                                <CustomButtons>
                                                    <dx:GridViewCommandColumnCustomButton ID="Edit" Text="አርም"></dx:GridViewCommandColumnCustomButton>
                                                    <dx:GridViewCommandColumnCustomButton ID="Del" Text="ሰርዝ"></dx:GridViewCommandColumnCustomButton>
                                                </CustomButtons>
                                                </dx:GridViewCommandColumn>
                                        </Columns>
                                        <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" />
                                        <SettingsPager>
                                            <Summary Text="ገፅ {0} ከ {1} ({2} ቅሬታ)" />
                                        </SettingsPager>
                                       
                                    </dx:ASPxGridView>
                                             
                                       </td>
                                       <td>

                                           &nbsp;</td>

                                   </tr>
                        <tr>
                            <td>
                                 
<dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True"></dx:PopupControlContentControl>

                                    <dx:ASPxPopupControl ID="ASPxPopupControl1" runat="server" ClientInstanceName="popupControl" Font-Names="Visual Geez Unicode" Font-Size="Small" HeaderText="Delete Confirmation" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" >
                                        <ContentCollection>
                                            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                                                    <uc1:ConfirmBox runat="server" id="ConfirmBox" />
                                            </dx:PopupControlContentControl>
                                         </ContentCollection>
                            </dx:ASPxPopupControl>

                            </td>

                        </tr>

                         </table>
   </dx:PanelContent>
</PanelCollection>
</dx:aspxcallbackpanel>
                </td>
                </tr>
            </table>
         </div>
    <div>
    <asp:HiddenField ID="hdfldCompliantGuid" runat="server" />
    </div>
</asp:Content>

