﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeFile="MessageExchangeModified.aspx.cs" Inherits="MessageExchangeModified" Culture="auto" meta:resourcekey="PageResource4" UICulture="auto" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>
<%@ Register Assembly="CUSTOR.Controls.WebBox" Namespace="CUSTOR.Controls.WebBox" TagPrefix="cc1" %>
<%@ Register Assembly="CUSTORDatePicker" Namespace="CUSTORDatePicker" TagPrefix="cdp" %>

<%@ Register Assembly="DevExpress.XtraReports.v19.2.Web.WebForms, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%-- <link href="~/Styles/GridStyle.css" rel="stylesheet" type="text/css" />
    <link href="../App_Themes/Default/CSS/Default.css" rel="stylesheet" type="text/css" />--%>
    <script type="text/javascript">
        // <![CDATA[
        var fieldSeparator = "|";
        function FileUploadStart() {
            document.getElementById("uploadedListFiles").innerHTML = "";
        }
        function FileUploaded(s, e) {
            if (e.isValid) {
                var linkFile = document.createElement("a");
                var indexSeparator = e.callbackData.indexOf(fieldSeparator);
                var fileName = e.callbackData.substring(0, indexSeparator);
                var pictureUrl = e.callbackData.substring(indexSeparator + fieldSeparator.length);
                var date = new Date();
                var imgSrc = "Attached/" + pictureUrl + "?dx=" + date.getTime();
                linkFile.innerHTML = fileName;
                linkFile.setAttribute("href", imgSrc);
                linkFile.setAttribute("target", "_blank");
                var container = document.getElementById("uploadedListFiles");
                container.appendChild(linkFile);
                container.appendChild(document.createElement("br"));
            }
        }
        // ]]>
    </script>

    <style type="text/css">
        .style1 {
            width: 99%;
        }

        .style3 {
            width: 50%;
        }

        .style4 {
            width: 100%;
        }

        .style17 {
            width: 600px;
        }

        .style18 {
            width: 35px;
        }

        .style22 {
        }

        .style26 {
        }

        .style27 {
        }

        .style28 {
        }

        .style29 {
            height: 22px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        //
        function SelectClicked(s, e) {
            alert('hi');
            gvwMessageExchanges.PerformCallback('List');

        }



        //
        function gvwMessageExchanges_CustomButtonClick(s, e) {
            if (e.buttonID != 'DELETING' && e.buttonID != 'EDITING') return;
            if (e.buttonID == 'DELETING') {

            }
            if (e.buttonID == 'EDITING') {
                //gvwMessageExchanges.GetRowValues(gvwMessageExchanges.GetFocusedRowIndex(), 'ID', OnSelectedFieldValues12);
                gvwMessageExchanges.GetRowValues(e.visibleIndex, "ID", OnSelectedFieldValues12);
                hfselectedrowinfo.SetValue(e.visibleIndex);
                alert(e.visibleIndex);
            }
        }


        function OnSelectedFieldValues12(values) {
            // gvwMessageExchanges.PerformCallback('List');
            alert(values);
            gvwMessageExchanges.PerformCallback('List' + '|' + values);
        }

        function gvwParentMessage_CustomButtonClick(s, e) {
            pupmessage.ShowPopup();
            if (e.buttonID != 'Del' && e.buttonID != 'Edit') return;
            if (e.buttonID == 'Del') {
                rowVisibleIndex = e.visibleIndex;
                strGuid = s.GetRowKey(e.visibleIndex);
                ShowPopup();
                //rowVisibleIndex = e.visibleIndex;
                //DvgrdregEdit.GetRowValues(e.visibleIndex, 'ComplaintGUID', ShowPopup);

            }
            if (e.buttonID == 'Edit') {

                gvwParentMessage.GetRowValues(e.visibleIndex, "ParentMessageID", OnGetSelectedFieldValues);
                pupmessage.Show();

            }
        }
        function OnGetSelectedFieldValues(values) {

            cbMessageExchange.PerformCallback('Edit' + '|' + values);
            //pupmessage.ShowPopup();
            //pnldata.SetVisible(true);
        }

        function OnEndCallback() {
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceHolder" runat="server">
    &nbsp;<%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
    <dx:ASPxCallbackPanel ID="cbMessageExchange" ClientInstanceName="cbMessageExchange" runat="server" Width="100%" OnCallback="cbMessageExchange_Callback" meta:resourcekey="cbMessageExchangeResource1">
        <ClientSideEvents EndCallback="OnEndCallback"></ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent meta:resourcekey="PanelContentResource1">

                <div>
                    <table frame="box" cellpadding="0" cellspacing="0" style="border-collapse: initial; width: 100%;">
                        <tr style="border-bottom-width: 0px">
                            <td align="center">
                                <asp:Panel ID="pnlMsg" runat="server" CssClass="messageWarning" EnableViewState="False"
                                    Height="100%" Visible="False" Width="100%" BorderColor="#99CCFF" BackColor="#CCFF33" meta:resourcekey="pnlMsgResource1">
                                    <asp:Label ID="lblPersonMsg" runat="server" CssClass="Message" Font-Bold="True" ForeColor="Red" meta:resourcekey="lblPersonMsgResource1"></asp:Label>
                                </asp:Panel>
                                <cc2:MessageBox
                                    ID="MessageBox1"
                                    runat="server" meta:resourcekey="MessageBox1Resource1"></cc2:MessageBox>
                            </td>
                        </tr>
                        <tr>
                            <td height="114" align="center">
                                <asp:Panel ID="Panel2" runat="server" Width="650px" CssClass="RoundPanelToolbar" meta:resourcekey="Panel2Resource1">
                                    <table width="100%" border="0">
                                        <tr>
                                            <td width="70%" rowspan="1" align="right">
                                                <dx:ASPxLabel ID="lblOrganization" runat="server" Text="ተቋም: " meta:resourcekey="lblOrganizationResource1">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="50%" rowspan="1" align="left">
                                                <dx:ASPxComboBox ID="ComboOrg" runat="server" Height="26px" Width="250px" DropDownStyle="DropDown" EnableIncrementalFiltering="True" IncrementalFilteringMode="StartsWith" meta:resourcekey="ComboOrgResource1">
                                                </dx:ASPxComboBox>
                                            </td>
                                            <td rowspan="1" align="center" class="style1"></td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1" align="right">
                                                <dx:ASPxLabel ID="lbldoctype" runat="server" Text="የጉዳዩ አይነት" Width="125px" meta:resourcekey="lbldoctypeResource1">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="50%" rowspan="1" align="left">
                                                <dx:ASPxComboBox ID="ComboDocType" runat="server" Height="26px" Width="250px" DropDownStyle="DropDown"
                                                    PopupHorizontalAlign="RightSides" meta:resourcekey="ComboDocTypeResource1">
                                                </dx:ASPxComboBox>
                                            </td>
                                            <td rowspan="1" align="center" class="style1"></td>
                                        </tr>
                                        <tr>
                                            <td width="70%" rowspan="1" align="right">
                                                <dx:ASPxLabel ID="lbldate" runat="server" Text="ቀን:" meta:resourcekey="lbldateResource1">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td width="34%" rowspan="1" align="center">
                                                <table>
                                                    <tr>
                                                        <td align="left">

                                                            <cdp:CUSTORDatePicker ID="cdpFromDate" runat="server" SelectedDate=""
                                                                TextCssClass="" Width="110px" meta:resourcekey="cdpFromDateResource1" />

                                                        </td>
                                                        <td>
                                                            <strong>-</strong>
                                                        </td>
                                                        <td align="left">

                                                            <cdp:CUSTORDatePicker ID="cdpToDate" runat="server" Width="110px" meta:resourcekey="cdpToDateResource1" />


                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td rowspan="1" align="left" class="style1">
                                                <dx:ASPxButton ID="btnsearch" runat="server" Text="አሳይ:" OnClick="btnsearch_Click" HorizontalAlign="Center" Image-Url="~/images/Toolbar/search.gif" ClientInstanceName="btnsearch" Theme="DevEx" AutoPostBack="True" Font-Names="Visual Geez Unicode" EnableClientSideAPI="True" Height="29px" meta:resourcekey="btnsearchResource1">
                                                    <Image Url="~/images/Toolbar/search.gif">
                                                    </Image>
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" width="70%">&nbsp;</td>
                                            <td align="center" width="34%">&nbsp;</td>
                                            <td align="left" class="style1">
                                                <asp:LinkButton ID="lkbtnNew" runat="server" OnClick="lkbtnNew_Click" Width="87px" meta:resourcekey="lkbtnNewResource1">አዲስ ጥያቄ</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>

                                </asp:Panel>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxPopupControl ID="pupmessage" ClientInstanceName="pupmessage" runat="server" Height="301px" PopupHorizontalAlign="WindowCenter"
                                    PopupVerticalAlign="WindowCenter" Theme="Office2010Silver" Width="400px"
                                    Modal="True" meta:resourcekey="pupmessageResource1">
                                    <HeaderStyle Font-Names="Visual Geez Unicode" Font-Size="14px" HorizontalAlign="Center" />
                                    <ContentCollection>
                                        <dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True" meta:resourcekey="PopupControlContentControlResource1">
                                            <table class="style4">
                                                <tr>
                                                    <td class="style17"></td>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxButton ID="cmdSave" runat="server" OnClick="cmdSave_Click" Text="አስቀምጥ" Theme="DevEx" ValidationGroup="compliantDecisionDetail" Width="130px" meta:resourcekey="cmdSaveResource1">
                                                                        <Image Url="~/Images/Toolbar/save.gif">
                                                                        </Image>
                                                                    </dx:ASPxButton>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxButton ID="cmdBack" runat="server" Text="ሰርዝ" Theme="DevEx" Width="130px" meta:resourcekey="cmdBackResource1">
                                                                        <Image Url="~/Images/Toolbar/Delete.png">
                                                                        </Image>
                                                                    </dx:ASPxButton>

                                                                </td>
                                                                <td>
                                                                    <dx:ASPxButton ID="btnApproval" runat="server" Text="ጥያቄው ይላክ" Theme="DevEx" Width="130px" Visible="False" OnClick="btnApproval_Click" meta:resourcekey="btnApprovalResource1">
                                                                        <Image Url="~/Images/Toolbar/Forward.png">
                                                                        </Image>
                                                                    </dx:ASPxButton>
                                                                </td>

                                                            </tr>

                                                        </table>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <asp:Label ID="ASPxLabel13" runat="server" Text="ጉዳይ" meta:resourcekey="ASPxLabel13Resource1"></asp:Label>
                                                        <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*" meta:resourcekey="Label2Resource1"></asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <dx:ASPxComboBox ID="ddlCase" runat="server" ClientInstanceName="Case" Height="19px" AutoPostBack="False" OnSelectedIndexChanged="ddlCase_SelectedIndexChanged" SelectedIndex="0" TabIndex="1" Width="400px" meta:resourcekey="ddlCaseResource1">
                                                            <%--<Items>
                                                                <dx:ListEditItem Selected="True" Text="ይምረጡ" Value="-1" />
                                                                <dx:ListEditItem Text="የቅጥር ማወዳደሪያ መስፈርት" Value="1" />
                                                                <dx:ListEditItem Text="የደረጃ ማወዳደሪያ መስፈርት" Value="2" />
                                                                <dx:ListEditItem Text="ሌሎች" Value="8" />
                                                            </Items>--%>
                                                        </dx:ASPxComboBox>
                                                        <%--<asp:RequiredFieldValidator ID="rqvddlCase" runat="server" ControlToValidate="ddlCase" Display="None" ErrorMessage="እባክዎ ጉዳዩን ይምረጡ!" ForeColor="Red" ValidationGroup="question"></asp:RequiredFieldValidator>--%>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" align="right">
                                                        <asp:Label ID="ASPxLabel15" runat="server" Text="ይዘት" meta:resourcekey="ASPxLabel15Resource1"></asp:Label>
                                                        <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*" meta:resourcekey="Label1Resource1"></asp:Label>
                                                    </td>
                                                    <td align="left" valign="top">&nbsp;

                <dx:ASPxMemo ID="txtMessage" runat="server" AutoResizeWithContainer="True" ClientInstanceName="Message" Height="100px" TabIndex="3" Width="100%" meta:resourcekey="txtMessageResource1">
                    <ValidationSettings ValidationGroup="question">
                    </ValidationSettings>
                </dx:ASPxMemo>

                                                        <%--<asp:RequiredFieldValidator ID="rqvtxtMessage" runat="server" ControlToValidate="txtMessage" Display="None" ErrorMessage="እባክዎ የመልእክቱን ይዘት ያስገቡ!" ForeColor="Red" ValidationGroup="question"></asp:RequiredFieldValidator>--%>

                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td valign="top" align="left">
                                                        <dx:ASPxCallbackPanel ID="cbData" runat="server" ClientInstanceName="cbdata" Width="900px" meta:resourcekey="cbDataResource1">
                                                            <ClientSideEvents EndCallback="OnEndCallback" />
                                                            <PanelCollection>
                                                                <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="PanelContent1Resource1">
                                                                    <br />
                                                                    <dx:ASPxPanel ID="pnlData" runat="server" ClientInstanceName="pnldata" Width="900px" meta:resourcekey="pnlDataResource1">
                                                                        <PanelCollection>
                                                                            <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="PanelContent2Resource1">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td style="text-align: right" valign="top">
                                                                                            <dx:ASPxButton ID="btnAttach" runat="server" AutoPostBack="False" ClientInstanceName="btnAttach" Height="19px" Text="አያይዝ" meta:resourcekey="btnAttachResource1">

                                                                                                <Image Url="~/Images/expand_blue.jpg">
                                                                                                </Image>
                                                                                            </dx:ASPxButton>
                                                                                        </td>
                                                                                        <td class="style4">
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <dx:ASPxUploadControl ID="fuAttachment" runat="server" ClientInstanceName="UploadControl" FileInputCount="3" Height="100%" OnFileUploadComplete="UploadControl_FileUploadComplete" ShowAddRemoveButtons="True" ShowProgressPanel="True" ShowUploadButton="True" TabIndex="8" Theme="Office2010Silver" Width="400px" Border-BorderStyle="Solid" meta:resourcekey="fuAttachmentResource1">
                                                                                                            <ValidationSettings AllowedFileExtensions=".jpg, .jpeg, .jpe, .gif" GeneralErrorText="ሰነዱ በትክክል አልተቀመጠም:: እባክዎ እንደገና ያያይዙ" NotAllowedFileExtensionErrorText="የሰነዱ አይነት የተፈቀደ አይደለም:: እባክዎ እንደገና ያያይዙ">
                                                                                                            </ValidationSettings>
                                                                                                            <ClientSideEvents FileUploadComplete="function(s, e) { FileUploaded(s, e) }" FileUploadStart="function(s, e) { FileUploadStart(); }" />
                                                                                                            <AddButton Text="ጨምር">
                                                                                                            </AddButton>
                                                                                                            <BrowseButton Text="አስስ...">
                                                                                                            </BrowseButton>
                                                                                                            <RemoveButton Text="ሰርዝ">
                                                                                                            </RemoveButton>
                                                                                                            <UploadButton Text="አያይዝ">
                                                                                                            </UploadButton>
                                                                                                            <Border BorderStyle="Solid"></Border>
                                                                                                        </dx:ASPxUploadControl>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <dx:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" ClientInstanceName="RoundPanel" HeaderText="የተያያዙ ሰነዶች (jpeg, gif)" Height="100%" TabIndex="10" Width="400px" meta:resourcekey="ASPxRoundPanel2Resource1">
                                                                                                            <PanelCollection>
                                                                                                                <dx:PanelContent ID="PanelContent3" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="PanelContent3Resource1">
                                                                                                                    <div id="uploadedListFiles" style="height: 150px; font-family: Arial;">
                                                                                                                    </div>
                                                                                                                </dx:PanelContent>
                                                                                                            </PanelCollection>
                                                                                                        </dx:ASPxRoundPanel>
                                                                                                    </td>
                                                                                                    <td valign="top"></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </dx:PanelContent>
                                                                        </PanelCollection>
                                                                    </dx:ASPxPanel>
                                                                </dx:PanelContent>
                                                            </PanelCollection>
                                                        </dx:ASPxCallbackPanel>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>

                                                    <td>
                                                        <dx:ASPxGridView ID="DvgvAttachment" runat="server" AutoGenerateColumns="False" KeyFieldName="MainGuid" OnBeforePerformDataSelect="DvgvEmployment_BeforePerformDataSelect" OnSelectionChanged="DvgvEmployment_SelectionChanged" Width="100%" meta:resourcekey="DvgvAttachmentResource1">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="የሰነዱ ይዘት" FieldName="Subject" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource1">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="Url" ShowInCustomizationForm="True" VisibleIndex="1" Visible="false" meta:resourcekey="GridViewDataTextColumnResource2">
                                                                </dx:GridViewDataTextColumn>
                                                                <%-- <dx:GridViewDataTextColumn Caption="ሰነዱ የደረሰበት ቀን" FieldName="SentDate" ShowInCustomizationForm="True" VisibleIndex="2">
                                                            </dx:GridViewDataTextColumn>--%>
                                                                <%-- <dx:GridViewDataTextColumn Caption="ሰነዱ የሚገኝበት ቦታ" FieldName="FileLocation" ShowInCustomizationForm="True" Visible="False" VisibleIndex="3">
                                                            </dx:GridViewDataTextColumn>--%>
                                                                <dx:GridViewDataTextColumn ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource3">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxButton ID="btnView3" runat="server" OnClick="btnView3_Click" Text="አሳይ" AutoPostBack="False" meta:resourcekey="btnView3Resource1">
                                                                            <Image Url="~/Files/Images/action_bottom.gif">
                                                                            </Image>
                                                                        </dx:ASPxButton>
                                                                    </DataItemTemplate>

                                                                    <HeaderCaptionTemplate>
                                                                        <img alt="" class="auto-style16" longdesc="http://localhost:2379/Files/Images/action_bottom.gif" src="../Files/Images/action_bottom.gif" />
                                                                    </HeaderCaptionTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                            </Columns>
                                                            <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" />
                                                            <Styles>
                                                                <FocusedRow BackColor="#AAD2FF">
                                                                </FocusedRow>
                                                            </Styles>
                                                        </dx:ASPxGridView>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </dx:PopupControlContentControl>
                                    </ContentCollection>
                                </dx:ASPxPopupControl>
                            </td>
                        </tr>
                        <tr>
                            <td>

                                <dx:ASPxGridView runat="server" KeyFieldName="ParentMessageID" AutoGenerateColumns="False"
                                    Theme="Office2010Silver" Width="100%" EnableTheming="True" ID="gvwParentMessage"
                                    ClientInstanceName="gvwParentMessage"
                                    OnDetailRowExpandedChanged="gvwParentMessage_DetailRowExpandedChanged1"
                                    OnCustomColumnDisplayText="gvwParentMessage_CustomColumnDisplayText"
                                    OnHtmlRowPrepared="gvwParentMessage_HtmlRowPrepared"
                                    OnHtmlRowCreated="gvwParentMessage_HtmlRowCreated"
                                    OnPageIndexChanged="gvwParentMessage_PageIndexChanged1"
                                    OnLoad="gvwParentMessage_Load" meta:resourcekey="gvwParentMessageResource1">
                                    <Columns>
                                        <dx:GridViewDataTextColumn FieldName="OrgName" Caption="የተቋም ስም" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource4">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Subject" Caption="የጥያቄው ዝርዝር ጉዳይ" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource5">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="Case" Caption="ጉዳይ" VisibleIndex="2" Visible="false" meta:resourcekey="GridViewDataTextColumnResource6">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="EmployeeName" Caption="ያዘጋጀው" VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnResource7">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="FinalRevisedBy" Caption="ያረጋገጠው" VisibleIndex="4" meta:resourcekey="GridViewDataTextColumnResource8">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SentDate" Caption="የተላከበት ቀን" VisibleIndex="5" meta:resourcekey="GridViewDataTextColumnResource9">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="FinalAnswerdBy" Caption="ያፀደቀው" VisibleIndex="6" meta:resourcekey="GridViewDataTextColumnResource10">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="AnsweredDate" Caption="ያፀደቀበት ቀን" VisibleIndex="7" meta:resourcekey="GridViewDataTextColumnResource11">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="FlowStatus" Caption="ሁኔታ" VisibleIndex="8" meta:resourcekey="GridViewDataTextColumnResource12">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="ይምረጡ" VisibleIndex="9" Visible="False" meta:resourcekey="GridViewDataTextColumnResource13">
                                            <DataItemTemplate>
                                                <asp:LinkButton ID="lnkSelectParentMessage" runat="server"
                                                    OnClick="lnkSelectParentMessage_Click" meta:resourcekey="lnkSelectParentMessageResource1">ይምረጡ</asp:LinkButton>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="FStatus" Caption="Status" VisibleIndex="10" Visible="false" meta:resourcekey="GridViewDataTextColumnResource14">
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn Name="Edit" VisibleIndex="11" meta:resourcekey="GridViewDataTextColumnResource15">
                                            <DataItemTemplate>
                                                <dx:ASPxButton ID="EDIT" ClientInstanceName="EDIT" runat="server" Text="አርም"
                                                    ForeColor="#0099FF" OnClick="EDIT_Click" meta:resourcekey="EDITResource1">
                                                </dx:ASPxButton>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Name="requestResponse" VisibleIndex="13" Visible="true" meta:resourcekey="GridViewDataTextColumnResource16">
                                            <DataItemTemplate>
                                                <dx:ASPxButton ID="lnkMessageResponse" runat="server" OnClick="lnkMessageResponse_OnClick"
                                                    meta:resourcekey="lnkSelectParentMessageResource11"
                                                    Text="የተሰጠ ውሳኔ">
                                                </dx:ASPxButton>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True"></SettingsBehavior>
                                    <SettingsPager>
                                        <Summary Text="ገፅ {0} ከ {1} ({2} መልእክቶች)"></Summary>
                                    </SettingsPager>
                                    <SettingsDetail ShowDetailRow="True" AllowOnlyOneMasterRowExpanded="True"></SettingsDetail>
                                    <Templates>
                                        <DetailRow>
                                            <dx:ASPxPageControl ID="pagecontrol" runat="server" ActiveTabIndex="0" EnableTheming="True" Theme="Office2010Silver" Width="100%" meta:resourcekey="pagecontrolResource1">

                                                <TabPages>
                                                    <dx:TabPage Text="ሰነድ" meta:resourcekey="TabPageResource1">
                                                        <ContentCollection>
                                                            <dx:ContentControl runat="server" meta:resourcekey="ContentControlResource2">
                                                                <dx:ASPxGridView ID="DvgvEmployment" runat="server" AutoGenerateColumns="False" KeyFieldName="MainGuid" OnBeforePerformDataSelect="DvgvEmployment_BeforePerformDataSelect" OnCustomColumnDisplayText="DvgvEmployment_CustomColumnDisplayText" OnSelectionChanged="DvgvEmployment_SelectionChanged" Width="100%" DataSourceID="dsMSAttachment" meta:resourcekey="DvgvEmploymentResource1">
                                                                    <Columns>
                                                                        <dx:GridViewDataTextColumn Caption="የሰነዱ ይዘት" FieldName="Subject" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource17">
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ሰነዱ የደረሰበት ቀን" ShowInCustomizationForm="True" VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource18">
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn FieldName="Url" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource19">
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource20">
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxButton ID="btnView2" runat="server" OnClick="btnView2_Click" Text="አሳይ" AutoPostBack="False" meta:resourcekey="btnView2Resource1">
                                                                                    <Image Url="~/Files/Images/action_bottom.gif">
                                                                                    </Image>
                                                                                </dx:ASPxButton>
                                                                            </DataItemTemplate>
                                                                            <HeaderCaptionTemplate>
                                                                                <img alt="Action_btn" class="auto-style16" longdesc="http://localhost:2379/Files/Images/action_bottom.gif" src="../Files/Images/action_bottom.gif" />
                                                                            </HeaderCaptionTemplate>
                                                                        </dx:GridViewDataTextColumn>
                                                                    </Columns>
                                                                    <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" />
                                                                    <Styles>
                                                                        <FocusedRow BackColor="#AAD2FF">
                                                                        </FocusedRow>
                                                                    </Styles>
                                                                </dx:ASPxGridView>
                                                            </dx:ContentControl>
                                                        </ContentCollection>
                                                    </dx:TabPage>
                                                    <dx:TabPage Text="የጥያቄው ዝርዝር" meta:resourcekey="TabPageResource2">
                                                        <ContentCollection>
                                                            <dx:ContentControl runat="server" meta:resourcekey="ContentControlResource3">
                                                                <dx:ASPxGridView ID="DvGQuestion" runat="server" Width="100%" AutoGenerateColumns="False" DataSourceID="Dsparentsms" KeyFieldName="ParentMessageID" OnBeforePerformDataSelect="DvGQuestion_BeforePerformDataSelect" meta:resourcekey="DvGQuestionResource1">
                                                                    <Columns>
                                                                        <dx:GridViewBandColumn ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewBandColumnResource1">
                                                                        </dx:GridViewBandColumn>
                                                                    </Columns>
                                                                    <Templates>
                                                                        <DataRow>
                                                                            <table class="style4">
                                                                                <tr>
                                                                                    <td width="100px" align="right">
                                                                                        <dx:ASPxLabel ID="ASPxLabel44" runat="server" Text="ጉዳዮ:" meta:resourcekey="ASPxLabel44Resource1">
                                                                                        </dx:ASPxLabel>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dx:ASPxLabel ID="ASPxLabel43" runat="server" Text='<%# Eval("Subject") %>' meta:resourcekey="ASPxLabel43Resource1">
                                                                                        </dx:ASPxLabel>
                                                                                    </td>
                                                                                    <td>&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td valign="top" align="right">
                                                                                        <dx:ASPxLabel ID="ASPxLabel45" runat="server" Text="የመልእክት ይዘት:" meta:resourcekey="ASPxLabel45Resource1">
                                                                                        </dx:ASPxLabel>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dx:ASPxMemo ID="ASPxMemo1" runat="server" Height="150px" Text='<%# Eval("Text") %>' Width="100%" ReadOnly="True" meta:resourcekey="ASPxMemo1Resource1">
                                                                                        </dx:ASPxMemo>
                                                                                    </td>
                                                                                    <td>&nbsp;</td>
                                                                                </tr>
                                                                            </table>
                                                                        </DataRow>
                                                                    </Templates>
                                                                </dx:ASPxGridView>
                                                            </dx:ContentControl>
                                                        </ContentCollection>
                                                    </dx:TabPage>
                                                    <dx:TabPage Text="መልስ" meta:resourcekey="TabPageResource3">
                                                        <ContentCollection>
                                                            <dx:ContentControl runat="server" meta:resourcekey="ContentControlResource4">
                                                                <dx:ASPxGridView ID="gvwMessageExchange" runat="server"
                                                                    AutoGenerateColumns="False" ClientInstanceName="gvwMessageExchanges"
                                                                    EnableTheming="True"
                                                                    KeyFieldName="ID"  EnableCallBacks="False" 
                                                                    OnCustomCallback="gvwMessageExchange_CustomCallback"
                                                                    Theme="Glass" Width="100%" PreviewFieldName="Text" meta:resourcekey="gvwMessageExchangeResource1">
                                                                    <ClientSideEvents CustomButtonClick="gvwMessageExchanges_CustomButtonClick" />

                                                                    <Columns>
                                                                        <dx:GridViewDataTextColumn Caption="ጉዳዩ" FieldName="Subject" Visible="False" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource21">
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ቀን" FieldName="EventDateTime" VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource22">
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn VisibleIndex="6" meta:resourcekey="GridViewDataTextColumnResource23">
                                                                            <HeaderTemplate>
                                                                                <dx:ASPxButton ID="lnkNewMessageExchange" runat="server" OnClick="lnkNewMessageExchange_Click" Text="አዲስ" meta:resourcekey="lnkNewMessageExchangeResource1">
                                                                                </dx:ASPxButton>
                                                                            </HeaderTemplate>
                                                                        </dx:GridViewDataTextColumn>

                                                                        <dx:GridViewCommandColumn>
                                                                            <CustomButtons>
                                                                                <dx:GridViewCommandColumnCustomButton ID="EDITING" Text="አርም">
                                                                                </dx:GridViewCommandColumnCustomButton>
                                                                                <dx:GridViewCommandColumnCustomButton ID="DELETING" Text="ሰርዝ">
                                                                                </dx:GridViewCommandColumnCustomButton>
                                                                            </CustomButtons>
                                                                        </dx:GridViewCommandColumn>
                                                                        <%--         <dx:GridViewDataTextColumn VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnResource24"  >
                                                                            <DataItemTemplate>
                                                                                <asp:LinkButton ID="lnkSelectMessageExchange" runat="server" OnClick="lnkSelectMessageExchange_Click1" Text="ይምረጡ" meta:resourcekey="lnkSelectMessageExchangeResource1"></asp:LinkButton>
                                                                            </DataItemTemplate>
                                                                        </dx:GridViewDataTextColumn>--%>
                                                                        <%--<dx:GridViewDataTextColumn VisibleIndex="3" Caption="ይምረጡ" >
                                                                            <DataItemTemplate>
                                                                                <asp:LinkButton runat="server" id="lnkEditmessageexchange" OnClientClick="SelectClicked" Text="ይምረጡ"></asp:LinkButton>
                                                                            </DataItemTemplate>

                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn VisibleIndex="4" meta:resourcekey="GridViewDataTextColumnResource25" >
                                                                            <DataItemTemplate>
                                                                                <asp:LinkButton ID="lnkDeleteMessageExchange" runat="server" OnClick="lnkDeleteMessageExchange_Click" Text="ሰርዝ" meta:resourcekey="lnkDeleteMessageExchangeResource1"></asp:LinkButton>
                                                                            </DataItemTemplate>
                                                                        </dx:GridViewDataTextColumn>--%>
                                                                        <dx:GridViewDataTextColumn Caption="ያፀደቀው ስም" FieldName="EmployeeName" VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource26" ShowInCustomizationForm="True">
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn FieldName="ID" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource27" ShowInCustomizationForm="True">
                                                                        </dx:GridViewDataTextColumn>

                                                                    </Columns>
                                                                    <Settings ShowPreview="True" />
                                                                    <SettingsBehavior AllowFocusedRow="true"
                                                                        ProcessFocusedRowChangedOnServer="true"
                                                                        ProcessSelectionChangedOnServer="true" />
                                                                    <SettingsDetail IsDetailGrid="True" />
                                                                    <SettingsPager PageSize="5">
                                                                        <Summary Text="ገፅ {0} ከ {1} ({2} መልእክቶች)" />
                                                                    </SettingsPager>
                                                                    <Templates>
                                                                        <PreviewRow>
                                                                            <table>
                                                                                <tr>
                                                                                    <td style="padding-right: 12px">
                                                                                        <dx:ASPxLabel ID="Label1" runat="server" Value='<%# Bind("Subject") %>' meta:resourcekey="Label1Resource2" />
                                                                                    </td>
                                                                                    <td style="text-align: center">
                                                                                        <dx:ASPxLabel ID="lblNotes" runat="server" Text='<%# Bind("Text") %>' meta:resourcekey="lblNotesResource1" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </PreviewRow>
                                                                    </Templates>
                                                                </dx:ASPxGridView>
                                                            </dx:ContentControl>
                                                        </ContentCollection>
                                                    </dx:TabPage>
                                                </TabPages>
                                            </dx:ASPxPageControl>

                                        </DetailRow>
                                    </Templates>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                        </tr>
                    </table>
                </div>
                <div>
                    <asp:Panel runat="server" ID="pnlResponse" Visible="False" meta:resourcekey="pnlResponseResource1">
                        <dx:ASPxGridView ID="gvwOfficerMessageResponse" runat="server"
                            AutoGenerateColumns="False" ClientInstanceName="gvwMessageExchange2"
                            EnableTheming="True" OnBeforePerformDataSelect="gvwMessageExchange_BeforePerformDataSelect"
                            KeyFieldName="ID" OnCustomColumnDisplayText="gvwMessageExchange_CustomColumnDisplayText"
                            Theme="Glass" Width="100%" PreviewFieldName="Text" meta:resourcekey="gvwOfficerMessageResponseResource1">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="ጉዳዩ" FieldName="Subject" Visible="true" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource28">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="ቀን" FieldName="EventDateTime" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource29">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="ያፀደቀው ስም" FieldName="EmployeeName" VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource30">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="ID" VisibleIndex="2" Visible="false" meta:resourcekey="GridViewDataTextColumnResource31">
                                </dx:GridViewDataTextColumn>

                            </Columns>
                            <Settings ShowPreview="True" />
                            <SettingsBehavior AllowDragDrop="False" AllowFocusedRow="True" AllowSelectSingleRowOnly="True" ProcessSelectionChangedOnServer="True" />
                            <Templates>
                                <PreviewRow>
                                    <table>
                                        <tr>
                                            <td style="padding-right: 12px">
                                                <dx:ASPxLabel ID="Label1" runat="server" Value='<%# Bind("Subject") %>' meta:resourcekey="Label1Resource3" />
                                            </td>
                                            <td style="text-align: center">
                                                <dx:ASPxLabel ID="lblNotes" runat="server" Text='<%# Bind("Text") %>' meta:resourcekey="lblNotesResource2" />
                                            </td>
                                        </tr>
                                    </table>
                                </PreviewRow>
                            </Templates>
                        </dx:ASPxGridView>
                    </asp:Panel>
                </div>
                <div class="divColor">
                    <cc1:MessageBox runat="server" ID="Msg" meta:resourcekey="MsgResource2" />

                    <table cellpadding="0" cellspacing="0" style="width: 99%">
                        <tr>
                            <td align="center" colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style3">
                                <dx:ASPxPopupControl ID="popNewMessageExchange" runat="server" Height="301px" PopupHorizontalAlign="WindowCenter"
                                    PopupVerticalAlign="WindowCenter" Theme="Office2010Silver" Width="697px"
                                    HeaderText="አጭር ጥያቄ ማቅረቢያና መልስ መለዋወጫ" Modal="True" meta:resourcekey="popNewMessageExchangeResource1">
                                    <ContentCollection>
                                        <dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True" meta:resourcekey="PopupControlContentControlResource2">
                                            <table class="divColor" width="100%">
                                                <tr>
                                                    <td align="left" width="6%">
                                                        <asp:Label ID="ASPxLabel41" runat="server" Text="ጉዳዩ:" meta:resourcekey="ASPxLabel41Resource1">
                                                        </asp:Label>
                                                    </td>
                                                    <td width="85%">
                                                        <dx:ASPxTextBox ID="txtExchangeCase" runat="server" TabIndex="6" Width="100%" meta:resourcekey="txtExchangeCaseResource1">
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                    <td>
                                                        <%--  <asp:RequiredFieldValidator ID="rqvtxtExchangeCase" runat="server" ControlToValidate="txtExchangeCase"
                                                ErrorMessage="እባክዎ ጉዳዩን ያስገቡ!" ForeColor="Red" ValidationGroup="questionExchange"
                                                Display="None"></asp:RequiredFieldValidator>--%>
                                                        <%--  <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="server"
                                                Enabled="true" TargetControlID="rqvtxtExchangeCase">
                                            </ajaxToolkit:ValidatorCalloutExtender>--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" width="75%" colspan="2">
                                                        <asp:Label ID="ASPxLabel42" runat="server" Text="ዝርዝር ይዘት" meta:resourcekey="ASPxLabel42Resource1">
                                                        </asp:Label>
                                                    </td>
                                                    <td align="left" class="style27">&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <dx:ASPxMemo ID="txtExchangeText" runat="server" Height="220px" Width="100%"
                                                            TabIndex="7" meta:resourcekey="txtExchangeTextResource1">
                                                        </dx:ASPxMemo>
                                                    </td>
                                                    <td>
                                                        <%--<asp:RequiredFieldValidator ID="rqvtxtExchangeText" runat="server" ControlToValidate="txtExchangeText"
                                                ErrorMessage="እባክዎ የመልእክቱን ይዘት ያስገቡ!" ForeColor="Red" ValidationGroup="questionExchange"
                                                Display="None"></asp:RequiredFieldValidator>--%>
                                                        <%-- <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="server"
                                                Enabled="true" TargetControlID="rqvtxtExchangeText">
                                            </ajaxToolkit:ValidatorCalloutExtender>--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="style28" colspan="2">
                                                        <dx:ASPxCheckBox ID="chkCompleted" Visible="False" runat="server" CheckState="Unchecked" ClientInstanceName="Completed"
                                                            Text="ጥያቄው ተጠናቋል?" Theme="ios" TabIndex="8" meta:resourcekey="chkCompletedResource1">
                                                        </dx:ASPxCheckBox>
                                                    </td>
                                                    <td align="left" class="style28">&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style28"></td>
                                                    <td>
                                                        <table class="style1">
                                                            <tr>
                                                                <td align="right">
                                                                    <dx:ASPxButton ID="btnSaveMessageExchange" runat="server"
                                                                        Text="ለውሳኔ ይተላለፍ" Width="72px" ValidationGroup="questionExchange" TabIndex="9"
                                                                        Theme="Office2010Silver" OnClick="btnSaveMessageExchange_Click1" meta:resourcekey="btnSaveMessageExchangeResource1">
                                                                    </dx:ASPxButton>
                                                                </td>
                                                                <td align="left">
                                                                    <dx:ASPxButton ID="btnCompletAnswer" runat="server" OnClick="btnCompletAnswer_Click"
                                                                        Text="አረጋግጥ/ላክ" Width="72px" Theme="Office2010Silver" meta:resourcekey="btnCompletAnswerResource1">
                                                                    </dx:ASPxButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </dx:PopupControlContentControl>
                                    </ContentCollection>
                                </dx:ASPxPopupControl>
                            </td>
                        </tr>
                        <tr>
                            <td class="style26" style="text-align: left" colspan="2"></td>
                        </tr>
                        <tr>
                            <td class="style26" style="text-align: left" colspan="2">
                                <dx:ASPxPopupControl ID="popParentMessageCompleted" runat="server"
                                    Height="216px" PopupHorizontalAlign="WindowCenter"
                                    PopupVerticalAlign="WindowCenter" Theme="Office2010Silver" Width="339px"
                                    HeaderText="የተጠናቀቀ ጥያቄ መላኪያ" Modal="True" meta:resourcekey="popParentMessageCompletedResource1">
                                    <ContentCollection>
                                        <dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True" meta:resourcekey="PopupControlContentControlResource3">
                                            <table class="divColor">
                                                <tr>
                                                    <td align="left" class="style28" colspan="2">
                                                        <dx:ASPxCheckBox ID="chkParentMessageCompleted" runat="server"
                                                            CheckState="Unchecked" ClientInstanceName="Completed"
                                                            Text="ጥያቄው ተጠናቋል ይላክ?" meta:resourcekey="chkParentMessageCompletedResource1">
                                                        </dx:ASPxCheckBox>
                                                    </td>
                                                    <td align="left" class="style28">&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="style28" colspan="2">
                                                        <asp:Label ID="lblQuestionedBy" runat="server" Text="Label" meta:resourcekey="lblQuestionedByResource1"></asp:Label>
                                                    </td>
                                                    <td align="left" class="style28">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="style28" colspan="2">
                                                        <asp:Label ID="lblQuestionedDate" runat="server" Text="Label" meta:resourcekey="lblQuestionedDateResource1"></asp:Label>
                                                    </td>
                                                    <td align="left" class="style28">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="style28" colspan="2">&nbsp;</td>
                                                    <td align="left" class="style28">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="style28"></td>
                                                    <td>
                                                        <table class="style1">
                                                            <tr>
                                                                <td align="center">
                                                                    <dx:ASPxButton ID="btnCompleteMessage" runat="server" OnClick="btnCompleteMessage_Click"
                                                                        Text="እሺ" Width="72px" Theme="Office2010Silver" meta:resourcekey="btnCompleteMessageResource1">
                                                                    </dx:ASPxButton>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <dx:ASPxButton ID="btnCompleteMessageNo" runat="server"
                                                                        OnClick="btnCompleteMessageNo_Click" Text="አይደለም" Width="72px"
                                                                        Theme="Office2010Silver" meta:resourcekey="btnCompleteMessageNoResource1">
                                                                    </dx:ASPxButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </dx:PopupControlContentControl>
                                    </ContentCollection>
                                </dx:ASPxPopupControl>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <dx:ASPxPopupControl ID="popParentMessageAnswerCompleted" runat="server"
                                    Height="241px" PopupHorizontalAlign="WindowCenter"
                                    PopupVerticalAlign="WindowCenter" Theme="Office2010Silver" Width="346px"
                                    HeaderText="የተጠናቀቀ መልስ መላኪያ" Modal="True" meta:resourcekey="popParentMessageAnswerCompletedResource1">
                                    <ContentCollection>
                                        <dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True" meta:resourcekey="PopupControlContentControlResource4">
                                            <table class="divColor">
                                                <tr>
                                                    <td align="left" class="style28" colspan="2">
                                                        <dx:ASPxCheckBox ID="chkParentMessageAnswerCompleted" runat="server"
                                                            CheckState="Unchecked" ClientInstanceName="Completed"
                                                            Text="መልሱ ተጠናቋል ይላክ?" meta:resourcekey="chkParentMessageAnswerCompletedResource1">
                                                        </dx:ASPxCheckBox>
                                                    </td>
                                                    <td align="left" class="style28">&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="2" class="style29">
                                                        <asp:Label ID="lblAnsweredBy" runat="server" Text="Label" meta:resourcekey="lblAnsweredByResource1"></asp:Label>
                                                    </td>
                                                    <td align="left" class="style29"></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="2">
                                                        <asp:Label ID="lblAnsweredDate" runat="server" Text="Label" meta:resourcekey="lblAnsweredDateResource1"></asp:Label>
                                                    </td>
                                                    <td align="left" class="style28">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="style28"></td>
                                                    <td>
                                                        <table class="style1">
                                                            <tr>
                                                                <td align="center">
                                                                    <dx:ASPxButton ID="btnCompletAnswer1" runat="server"
                                                                        Text="ላክ" Width="72px" Theme="Office2010Silver" meta:resourcekey="btnCompletAnswer1Resource1">
                                                                    </dx:ASPxButton>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td>
                                                                    <dx:ASPxButton ID="btnCompletAnswerNo" runat="server"
                                                                        OnClick="btnCompletAnswerNo_Click" Text="አይደለም" Width="72px"
                                                                        Theme="Office2010Silver" meta:resourcekey="btnCompletAnswerNoResource1">
                                                                    </dx:ASPxButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </dx:PopupControlContentControl>
                                    </ContentCollection>
                                </dx:ASPxPopupControl>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <asp:ObjectDataSource ID="OrganizationList" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="poplateOrganization" TypeName="CUSTOR.Bussiness.MessageExchangeBLL"></asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="Dsparentsms" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetParentMessageList" TypeName="CUSTOR.Bussiness.MessageExchangeBussiness">
                        <SelectParameters>
                            <asp:SessionParameter Name="ParentID" SessionField="ParentID" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="dsMSAttachment" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="BindMessageAttachments" TypeName="CUSTOR.Bussiness.MessageExchangeBLL">
                        <SelectParameters>
                            <asp:SessionParameter Name="ID" SessionField="ParentMessageID" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:HiddenField ID="HidlLanguage" runat="server" />
                    <asp:HiddenField ID="HidUserid" runat="server" />
                    <asp:HiddenField ID="Hidparentid" runat="server" />
                    <dx:ASPxTextBox ID="hfselectedrowinfo" runat="server" ClientInstanceName="hfselectedrowinfo" ClientVisible="false" Width="170px">
                    </dx:ASPxTextBox>
                    <asp:ObjectDataSource ID="dsmessageExchange" runat="server" DataObjectTypeName="CUSTOR.Domain.MessageExchangeBO" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetMessages" TypeName="CUSTOR.Bussiness.MessageExchangeBLL">
                        <SelectParameters>
                            <asp:SessionParameter Name="ParentID" SessionField="ParentMessageID" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </div>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
