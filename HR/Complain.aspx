﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeFile="Complain.aspx.cs" Inherits="UI.Pages.Complain" Culture="auto" meta:resourcekey="PageResource4" UICulture="auto" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="CUSTORDatePicker" Namespace="CUSTORDatePicker" TagPrefix="cdp" %>
<%@ Register Assembly="CUSTOR.Controls.WebBox" Namespace="CUSTOR.Controls.WebBox" TagPrefix="cc1" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxHtmlEditor" Assembly="DevExpress.Web.ASPxHtmlEditor.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register Src="~/Controls/Inspection.ascx" TagPrefix="UC" TagName="inspection" %>
<%@ Register Src="~/Controls/Response.ascx" TagPrefix="UC" TagName="Response" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxSpellChecker" Assembly="DevExpress.Web.ASPxSpellChecker.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        // <![CDATA[
        var fieldSeparator = "|";
        function FileUploadStart() {
            document.getElementById("uploadedListFiles").innerHTML = "";
        }

        function DoSearchClick() {
            cbCompliantList.PerformCallback('Search');
        }
        var complain_visibleIndex = 0;
        function GridRowExpandint(s, e) {
            complain_visibleIndex = s.GetFocusedRowIndex();
            DvgrdDecision.GetRowValues(complain_visibleIndex, "ComplaintGuid", OnGetSelectedComplainFieldValues);

        }

        function DvgrdDecisionCustomButton(s, e) {
            complian_selectedIndex = s.visibleIndex;
            return complian_selectedIndex;
        }


        function OnGetSelectedComplainFieldValues(values) {
            cbCompliantList.PerformCallback('Complain' + '|' + complain_visibleIndex + '|' + values);
        }

        function FileUploaded(s, e) {
            if (e.isValid) {
                var linkFile = document.createElement("a");
                var indexSeparator = e.callbackData.indexOf(fieldSeparator);
                var fileName = e.callbackData.substring(0, indexSeparator);
                var pictureUrl = e.callbackData.substring(indexSeparator + fieldSeparator.length);
                var date = new Date();
                var imgSrc = "Attached/" + pictureUrl + "?dx=" + date.getTime();
                linkFile.innerHTML = fileName;
                linkFile.setAttribute("href", imgSrc);
                linkFile.setAttribute("target", "_blank");
                var container = document.getElementById("uploadedListFiles");
                container.appendChild(linkFile);
                container.appendChild(document.createElement("br"));
            }
        }
        // ]]>
    </script>

</asp:Content >
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceHolder" runat="server">
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
    <div>
        <cc1:MessageBox ID="MessageBox1" runat="server" meta:resourcekey="MessageBox1Resource1"></cc1:MessageBox>
        <cc1:ConfirmBox ID="ConfirmBox1" runat="server" meta:resourcekey="ConfirmBox1Resource1" />
    </div>

    <%--<dx:ASPxCallbackPanel runat="server" ID="cbCompliantList" ClientInstanceName="cbCompliantList" OnCallback="compliantList_OnCallback">
        <PanelCollection>
            <dx:PanelContent ID="ASPxRoundPanel2" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="PanelContent1Resource1">--%>
    <table align="center" width="100%">
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td height="114" align="center">
                            <asp:Panel ID="ASPxPanel1" runat="server" Width="650px" CssClass="RoundPanelToolbar" meta:resourcekey="ASPxPanel1Resource1">
                                <table width="100%" border="0">
                                    <tr>
                                        <td width="100%" rowspan="1" align="right">
                                            <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="ተቋም: " meta:resourcekey="ASPxLabel1Resource1">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td width="50%" rowspan="1" align="left">
                                            <dx:ASPxComboBox ID="cboOrganization" runat="server" Height="26px" Width="250px" DropDownStyle="DropDown" EnableIncrementalFiltering="True" IncrementalFilteringMode="StartsWith" meta:resourcekey="cboOrganizationResource1">
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td rowspan="1" align="center" class="auto-style24"></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="1" align="right">
                                            <dx:ASPxLabel ID="lblComplainType" runat="server" meta:resourcekey="lblComplainTypeResource1">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td width="50%" rowspan="1" align="left">
                                            <dx:ASPxComboBox ID="cboComplainType" runat="server" Height="26px" Width="250px" DropDownStyle="DropDown"
                                                PopupHorizontalAlign="RightSides" meta:resourcekey="cboComplainTypeResource1">
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td rowspan="1" align="center" class="auto-style24"></td>
                                    </tr>
                                    <tr>
                                        <td width="70%" rowspan="1" align="right" class="auto-style40">
                                            <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="ቀን:" meta:resourcekey="ASPxLabel2Resource1">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td width="34%" rowspan="1" align="center" class="auto-style40">
                                            <table>
                                                <tr>
                                                    <td align="left">

                                                        <cdp:CUSTORDatePicker ID="txtDateFrom" runat="server" SelectedDate=""
                                                            TextCssClass="" Width="110px" meta:resourcekey="txtDateFromResource1" />
                                                    </td>
                                                    <td>
                                                        <strong>-</strong>
                                                    </td>
                                                    <td align="left">
                                                        <cdp:CUSTORDatePicker ID="txtDateTo" runat="server" Width="110px" meta:resourcekey="txtDateToResource1" SelectedDate="" TextCssClass="" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td rowspan="1" align="left" class="auto-style40">
                                            <dx:ASPxButton ID="cmdSearch" runat="server" Text="ፈልግ:" HorizontalAlign="Center" ClientInstanceName="btnsearch" Theme="Office2010Silver" Font-Names="Visual Geez Unicode" EnableClientSideAPI="True" meta:resourcekey="cmdSearchResource1" OnClick="cmdSearch_Click2">
                                                <Image Url="~/images/Toolbar/search.gif">
                                                </Image>
                                            </dx:ASPxButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="34%" rowspan="1" align="center" class="style4"></td>
                                        <td width="34%" rowspan="1" align="right" class="style4"></td>
                                        <td rowspan="1" align="center" class="auto-style25">&nbsp;</td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
        <tr>
            <td class="auto-style39">
                <%--  <dx:ASPxCallbackPanel ID="pnlCallback" runat="server" Width="100%" >
                  
                    <%--<ClientSideEvents EndCallback="OnEndCallbackView" />--%>

                <%--<PanelCollection>--%>
                <%--<dx:PanelContent runat="server" SupportsDisabledAttribute="True">--%>
              <%--  <asp:UpdatePanel ID="upnlCompGrid" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>--%>
                        <dx:ASPxGridView ID="DvgrdDecision" runat="server" AutoGenerateColumns="False" ClientInstanceName="DvgrdDecision"
                             KeyFieldName="ComplaintGuid" Width="100%" Theme="Office2010Silver" meta:resourcekey="DvgrdDecisionResource1"  OnHtmlRowPrepared="DvgrdDecision_HtmlRowPrepared"
                              OnRowCommand="DvgrdDecision_RowCommand" OnStartRowEditing="DvgrdDecision_StartRowEditing" OnCustomDataCallback="DvgrdDecision_CustomDataCallback">

                            <%--<ClientSideEvents DetailRowExpanding="function(s, e) {
                                    GridRowExpandint(s,e); }"
                                     />--%>
                            <ClientSideEvents FocusedRowChanged="function(s, e) { 
                                            var key = gvwParentMessage.GetFocusedRowIndex();
                                            DvgrdDecision.PerformCustomDataCallback(key); 
                                }"></ClientSideEvents>

                            <Columns>
                                <dx:GridViewDataTextColumn Caption="ጥቆማ አቅራቢ" FieldName="ReportedBy" VisibleIndex="2">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="ተቋም" FieldName="Organization" VisibleIndex="3">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="ቀን" VisibleIndex="4" FieldName="ReportedDate" meta:resourcekey="ASPxLabelDayResource1">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="የአቤቱታ አይነት" FieldName="ComplainType" VisibleIndex="5">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="የተሰጠው ውሳኔ" FieldName="FindingSummary" VisibleIndex="6">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="ComplaintGuid" VisibleIndex="7" Visible="False">
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="" VisibleIndex="9">
                                    <DataItemTemplate>
                                        <asp:LinkButton ID="lnkSelectParentMessage" runat="server"
                                            meta:resourcekey="lnkSelectParentMessageResource1">ምረጥ</asp:LinkButton>
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <%-- 2016 <SettingsDetail ShowDetailRow="True" />
                                <Templates>
                                    <DetailRow>

                                    </DetailRow>
                                    <EmptyDataRow>
                                        መረጃ አልተገኘም
                                    </EmptyDataRow>
                                </Templates>--%>
                        </dx:ASPxGridView>
                 <%--   </ContentTemplate>
                </asp:UpdatePanel>--%>
                <%--                </dx:PanelContent>
         </PanelCollection>                  
                </dx:ASPxCallbackPanel>--%>
            </td>
        </tr>
        <tr>
            <td>
               <%-- <asp:UpdatePanel ID="upnlTabs" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>--%>
                        <dx:ASPxPageControl ID="pageControl" runat="server" ActiveTabIndex="0" Width="100%" meta:resourcekey="pageControlResource1" Theme="PlasticBlue" OnActiveTabChanged="pageControl_ActiveTabChanged">
                            <TabPages>
                                <dx:TabPage Text="ዝርዝር መረጃ" meta:resourcekey="TabPageResource1">
                                    <ContentCollection>
                                        <dx:ContentControl ID="ContentControl1" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControlResource1">
                                            <%--           <dx:ASPxGridView ID="DvdvwComplian" runat="server" AutoGenerateColumns="False" DataSourceID="dscompliandetail" OnBeforePerformDataSelect="DvdvwComplian_BeforePerformDataSelect" Width="100%" meta:resourcekey="DvdvwComplianResource1">
                                                                <Columns>
                                                                    <dx:GridViewBandColumn Caption="የቅሬታ/የጥቆማ መረጃ" ShowInCustomizationForm="True" VisibleIndex="0">
                                                                        <HeaderStyle Font-Size="Larger" HorizontalAlign="Center" />
                                                                    </dx:GridViewBandColumn>
                                                                </Columns>
                                                                <Templates>
                                                                    <DataRow>--%>
                                            <table class="templateTable">
                                                <tr>
                                                    <td class="auto-style30">
                                                        <dx:ASPxLabel ID="lblCompliantNameDecision" runat="server" Text="ሙሉ ስም: " meta:resourcekey="lblCompliantNameDecisionResource1">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td align="left" class="auto-style31">
                                                        <dx:ASPxLabel ID="lblCompliantNameDecisionValue" runat="server" Width="300px" style="font-weight: 700" Theme="BlackGlass" meta:resourcekey="lblCompliantNameDecisionValueResource1">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td class="auto-style32">
                                                        <dx:ASPxLabel ID="lblTelephoneDecision" runat="server" Text="ስልክ ቁጥር: " meta:resourcekey="lblTelephoneDecisionResource1">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td align="left">
                                                        <dx:ASPxLabel ID="lblTelephoneDecisionValue" runat="server" ValidateRequestMode="Enabled" Width="300px" Font-Bold="True" Theme="BlackGlass" meta:resourcekey="lblTelephoneDecisionValueResource1">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style30">
                                                        <dx:ASPxLabel ID="lblEmailDecision" runat="server" Text="ኢሜል: " meta:resourcekey="lblEmailDecisionResource1">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td align="left" class="auto-style31">
                                                        <dx:ASPxLabel ID="lblEmailDecisionValue" runat="server" ValidateRequestMode="Enabled" Width="300px" Font-Bold="True" Theme="BlackGlass" meta:resourcekey="lblEmailDecisionValueResource1">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td class="auto-style32">
                                                        <dx:ASPxLabel ID="lblDateDecision" runat="server" Text="ቀን: " meta:resourcekey="lblDateDecisionResource1">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td align="left">
                                                        <dx:ASPxLabel ID="lblDateDecisionValue" runat="server" ValidateRequestMode="Enabled" Width="300px" Font-Bold="True" Theme="BlackGlass" meta:resourcekey="lblDateDecisionValueResource1">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style30">
                                                        <dx:ASPxLabel ID="lblOrganizationDecision" runat="server" Text="ተቋም: " meta:resourcekey="lblOrganizationDecisionResource1">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td align="left" class="auto-style31">
                                                        <dx:ASPxLabel ID="lblOrganizationDecisionValue" runat="server" ValidateRequestMode="Enabled" Width="300px" Font-Bold="True" Theme="BlackGlass" meta:resourcekey="lblOrganizationDecisionValueResource1">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td class="auto-style32">
                                                        <dx:ASPxLabel ID="lblCompliantTypeDecision" runat="server" Text="የጥያቄው አይነት" meta:resourcekey="lblCompliantTypeDecisionResource1">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td align="left">
                                                        <dx:ASPxLabel ID="lblCompliantTypeDecisionValue" runat="server" ValidateRequestMode="Enabled" Width="300px" Font-Bold="True" Theme="BlackGlass" meta:resourcekey="lblCompliantTypeDecisionValueResource1">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style30">
                                                        <dx:ASPxLabel ID="lblMeansOfReportDecision" runat="server" Text="የቀረበበት መንገድ" meta:resourcekey="lblMeansOfReportDecisionResource1">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td align="left" class="auto-style31">
                                                        <dx:ASPxLabel ID="lblMeansOfReportDecisionValue" runat="server" ValidateRequestMode="Enabled" Width="300px" Font-Bold="True" Theme="BlackGlass" meta:resourcekey="lblMeansOfReportDecisionValueResource1">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td class="auto-style32">
                                                        <dx:ASPxLabel ID="lblCompliantRemark" runat="server" Text="አስተያየት: " meta:resourcekey="lblCompliantRemarkResource1">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxLabel ID="ASPxLabel3" runat="server" ValidateRequestMode="Enabled" Width="300px" Font-Bold="True" Theme="BlackGlass" meta:resourcekey="ASPxLabel3Resource1"></dx:ASPxLabel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style30" valign="top">
                                                        <dx:ASPxLabel ID="lblCompliantReason" runat="server" Text="ምክንያት: " meta:resourcekey="lblCompliantReasonResource1">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td align="left" class="auto-style31" colspan="3">
                                                        <dx:ASPxMemo ID="txtCompliantReason" runat="server" ReadOnly="True" Rows="6" Width="100%" meta:resourcekey="txtCompliantReasonResource1">
                                                        </dx:ASPxMemo>
                                                    </td>
                                                </tr>
                                            </table>
                                            <%--  </DataRow>
                                                                </Templates>
                                                            </dx:ASPxGridView>--%>
                                        </dx:ContentControl>
                                    </ContentCollection>
                                </dx:TabPage>
                                <dx:TabPage Text="ሰነድ" meta:resourcekey="TabPageResource2">
                                    <ContentCollection>
                                        <dx:ContentControl ID="ContentControl2" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControlResource2">
                                            <dx:ASPxGridView ID="gvAttachment" runat="server" AutoGenerateColumns="False" KeyFieldName="AttachmentGuid" Width="100%" OnBeforePerformDataSelect="gvAttachment_BeforePerformDataSelect"   OnSelectionChanged="gvAttachment_SelectionChanged" meta:resourcekey="gvAttachmentResource1" OnCustomColumnDisplayText="gvAttachment_CustomColumnDisplayText">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="የሰነዱ ይዘት" FieldName="FileNumber" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource7" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="ሰነዱ የደረሰበት ቀን" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource8" FieldName="SentDate">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="ሰነዱ የሚገኝበት ቦታ" ShowInCustomizationForm="True" Visible="False" VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource9">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource10">
                                                        <DataItemTemplate>
                                                            <dx:ASPxButton ID="btnView3" runat="server" Text="አሳይ" OnClick="btnView3_Click" meta:resourcekey="btnView3Resource1">
                                                                <Image Url="~/Files/Images/action_bottom.gif">
                                                                </Image>
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <HeaderCaptionTemplate>
                                                            <img alt="Action_btn" class="auto-style16" longdesc="http://localhost:2379/Files/Images/action_bottom.gif" src="../Files/Images/action_bottom.gif" />
                                                        </HeaderCaptionTemplate>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataColumn Caption="ሰነዶች" VisibleIndex="0" meta:resourcekey="GridViewDataColumnResource1" ShowInCustomizationForm="True" Visible="false">
                                                        <DataItemTemplate>
                                                            <a href='<%# GetDocURL((string)Eval("FileUrl")) %>'><%# Eval("AttachmentTitle") %>                                    
                                                            </a>
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                </Columns>
                                                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" />
                                                <Styles>
                                                    <FocusedRow BackColor="#AAD2FF">
                                                    </FocusedRow>
                                                </Styles>
                                                <Templates>
                                                    <EmptyDataRow>
                                                        &nbsp;የተያያዘ ፋይል አልተገኘም
                                                    </EmptyDataRow>
                                                </Templates>
                                            </dx:ASPxGridView>
                                        </dx:ContentControl>
                                    </ContentCollection>
                                </dx:TabPage>
                                 <dx:TabPage Text="ግምገማ" meta:resourcekey="TabPageResource4">
                                    <ContentCollection>
                                        <dx:ContentControl ID="ContentControl4" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControlResource3">
                                            <UC:inspection ID="inspection1" runat="server" />
                                        </dx:ContentControl>
                                    </ContentCollection>
                                </dx:TabPage>
                                <dx:TabPage Text="ውሳኔ" meta:resourcekey="TabPageResource3">
                                    <ContentCollection>
                                        <dx:ContentControl ID="ContentControl3" runat="server" SupportsDisabledAttribute="True" >
                                            <dx:ASPxGridView ID="DvcomplainDecision" runat="server"
                                                AutoGenerateColumns="False" KeyFieldName="ComplaintGUID"
                                                OnBeforePerformDataSelect="ASPxGridView1_BeforePerformDataSelect"
                                                Width="900PX"
                                                OnCustomColumnDisplayText="DvcomplainDecision_CustomColumnDisplayText"
                                                Theme="Office2010Silver" meta:resourcekey="DvcomplainDecisionResource1" OnRowCommand="DvcomplainDecision_RowCommand">
                                                <%--   <SettingsBehavior AllowSelectByRowClick="True" /> 
                                                 <SettingsText CommandEdit="ውሳኔ" /> 
                                               
                                                   <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewCommandColumnResource1">
                                                        <EditButton Visible="True">
                                                        </EditButton>
                                                    </dx:GridViewCommandColumn> --%> <Columns>
                                                <dx:GridViewDataTextColumn Caption="ምረጥ" meta:resourcekey="GridViewDataTextColumnResource11"  >
                                                    <dataitemtemplate>
                                <asp:LinkButton ID="lnkSelectParentMessage" runat="server" meta:resourcekey="lnkSelectParentMessageResource2"
                                     >ውሳኔ</asp:LinkButton>
                            </dataitemtemplate>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="ውሳኔ የተሰጠበት ቀን" Visible="false" FieldName="DateScreened" ShowInCustomizationForm="True" VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnResource11">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="ScreenedBy" ShowInCustomizationForm="True" VisibleIndex="2" Caption="ውሳኔ የሰጠው" meta:resourcekey="GridViewDataTextColumnResource12">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="የተሰጠው ውሳኔ " FieldName="FindingSummary" ShowInCustomizationForm="True" VisibleIndex="4" meta:resourcekey="GridViewDataTextColumnResource13">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="ComplaintGUID" ShowInCustomizationForm="True" VisibleIndex="0" Visible="False" meta:resourcekey="GridViewDataTextColumnResource14">
                                                </dx:GridViewDataTextColumn>
                                                </Columns>
 
                                            </dx:ASPxGridView>
                                            <table>
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <dx:ASPxLabel ID="lblDecisionTypeDetail" runat="server" Text="የውሳኔ ሀሳብ : " meta:resourcekey="lblDecisionTypeDetailResource1">
                                                        </dx:ASPxLabel>
                                                        <asp:Label ID="lblFindingSummaryDateStar" runat="server" CssClass="requiredValueIndicator" ForeColor="Red" Text="*" meta:resourcekey="lblFindingSummaryDateStarResource1" BorderColor="Red"></asp:Label>
                                                    </td>
                                                    <td class="style10" align="left">
                                                        <dx:ASPxComboBox ID="cboDescsionTypeDetail" runat="server" Width="233px"   meta:resourcekey="cboDescsionTypeDetailResource1">
                                                           <%-- <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip" ErrorText="&quot;-1&quot;">
                                                                <ErrorFrameStyle>
                                                                    <Border BorderColor="#FF3300" BorderStyle="Double" BorderWidth="1px" />
                                                                </ErrorFrameStyle>
                                                                <RequiredField IsRequired="True" ErrorText="" />
                                                            </ValidationSettings>--%>
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <dx:ASPxLabel ID="lblDecisionRemark" runat="server" Text="አስተያየት: " meta:resourcekey="lblDecisionRemarkResource1">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td class="style10" align="left">
                                                        <dx:ASPxHtmlEditor ID="txtDecisionRemark" runat="server" Height="391px" Width="789px" meta:resourcekey="txtDecisionRemarkResource1">
                                                            <SettingsImageUpload UploadImageFolder="~/UploadImages/">
                                                                <ValidationSettings AllowedFileExtensions=".jpe,.jpeg,.jpg,.gif,.png" MaxFileSize="500000">
                                                                </ValidationSettings>
                                                            </SettingsImageUpload>
                                                        </dx:ASPxHtmlEditor>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style6">

                                                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("ComplaintGUID") %>' OnDataBinding="HiddenField1_DataBinding" />

                                                    </td>
                                                    <td class="style10" align="left">
                                                        <table align="left" class="style2">
                                                            <tr>
                                                                <td align="right">
                                                                    <dx:ASPxButton ID="btnupdate" runat="server" Text="አስቀምጥ" Theme="DevEx" Width="50px" OnClick="btnupdate_Click" meta:resourcekey="btnupdateResource1">
                                                                        <Image Url="~/Images/Toolbar/save.gif">
                                                                        </Image>
                                                                    </dx:ASPxButton>
                                                                </td>
                                                                <td align="left" width="50px"></td>
                                                            </tr>
                                                        </table>

                                                    </td>
                                                </tr>
                                            </table>
                                        </dx:ContentControl>
                                    </ContentCollection>
                                </dx:TabPage>
                               
                            </TabPages>
                        </dx:ASPxPageControl>
                  <%--  </ContentTemplate>
                </asp:UpdatePanel>--%>
            </td>
        </tr>
    </table>
    <%-- </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>--%>
    <div>
        <table class="styleW">

            <tr>
                <td class="auto-style38">
                    <asp:HiddenField ID="hdfldCompliantGuid" runat="server" />
                    <asp:HiddenField ID="hdfldPageMode" runat="server" />
                    <asp:ObjectDataSource ID="dscompAttachment" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetAttachmentByCompliantId" TypeName="CUSTOR.Bussiness.CompliantAttachmentBusiness">
                        <SelectParameters>
                            <asp:SessionParameter DbType="Guid" Name="ID" SessionField="CompGuid" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="dscompliandetail" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetComplianRecord" TypeName="CUSTOR.Bussiness.ComplaintBusiness">
                        <SelectParameters>
                            <asp:SessionParameter DbType="Guid" Name="ComptGuid" SessionField="ComptGuid" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:HiddenField ID="HidlLanguage" runat="server" />
                    <asp:HiddenField ID="HidUserid" runat="server" />
                </td>
            </tr>
        </table>
        <%-- <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="1" Width="100px">
                <CustomButtons>
                    <dx:GridViewCommandColumnCustomButton ID="Select" Text="ይምረጡ">
                    </dx:GridViewCommandColumnCustomButton>
                </CustomButtons>
            </dx:GridViewCommandColumn>--%><%# Eval("AttachmentTitle") %>
    </div>
    <br />

</asp:Content>
