﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using System.Web.Security;

public partial class Login : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //CreateOromiaSpecialUser("OrSuperAdmin", "Super Administrator", "OrSuperAdmin@Oromia.et");
        //CreateOromiaSpecialUser("OrDataEntry", "HR Data Entry", "OrDataEntry@Oromia.et");
        if (!Page.IsPostBack)
        {
           lblOrg.Text= @"(c) " + GetOrgName();
        }
    }
    private void CreateOromiaSpecialUser(string strUsername, string strRole, string strEmail)
    {
        MembershipCreateStatus createStatus;
        MembershipUser newUser = Membership.CreateUser(strUsername, "p@55w0rd", strEmail, "A", "B", true, out createStatus);
        Roles.AddUserToRole(strUsername, strRole);
        SaveProfile(strUsername);

    }

    private void SaveProfile(string strUsername){
        ProfileCommon profile = this.Profile;
        profile = this.Profile.GetProfile(strUsername);

        profile.Organization.Name = "Oromia Civil Service Bureau";
        profile.Organization.Administration = "04";
        profile.Organization.GUID = "66575CB6-EA86-4396-B48E-449F4189EF33";
        profile.Organization.Language = "AfanOromo";
        profile.Organization.Category = "2";
        profile.Organization.Code = "OrCS";
        profile.Organization.LanguageID = 2;
        profile.Organization.Zone = string.Empty;
        profile.Organization.Wereda = string.Empty;



        profile.Staff.FullName = strUsername;
        profile.Staff.GUID = Guid.NewGuid().ToString();

        profile.Staff.Units = string.Empty;
        profile.Save();
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        DoLogout();
    }

    private string GetOrgName()
    {
        try
        {
            return ConfigurationManager.AppSettings["OrgName"];
        }
        catch (Exception)
        {
            return string.Empty;}
    }
    private void DoLogout()
    {
        /* Create new session ticket that expires immediately */
        FormsAuthenticationTicket ticket =
            new FormsAuthenticationTicket(
                1,
                this.Context.User.Identity.Name,
                DateTime.Now,
                DateTime.Now,
                false,
                Guid.NewGuid().ToString());

        /* Encrypt the ticket */
        string encrypted_ticket = FormsAuthentication.Encrypt(ticket);

        /* Create cookie */
        HttpCookie cookie = new HttpCookie(
            FormsAuthentication.FormsCookieName,
            encrypted_ticket);

        /* Add cookie */
        this.Context.Response.Cookies.Add(cookie);

        /* Abandon session object to destroy all session variables */
        this.Context.Session.Clear();
        this.Context.Session.Abandon();
        //==================
        Response.Redirect("~/Login.aspx");
    }
}
