﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="ComplainDetail.aspx.cs" Inherits="ComplainDetail" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>








<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>




<%@ Register Src="~/Controls/ConfirmBox.ascx" TagPrefix="uc1" TagName="ConfirmBox" %>
<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        // <![CDATA[
        var fieldSeparator = "|";
        function FileUploadStart() {
            document.getElementById("uploadedListFiles").innerHTML = "";
        }
        function FileUploaded(s, e) {
            if (e.isValid) {
                var linkFile = document.createElement("a");
                var indexSeparator = e.callbackData.indexOf(fieldSeparator);
                var fileName = e.callbackData.substring(0, indexSeparator);
                var pictureUrl = e.callbackData.substring(indexSeparator + fieldSeparator.length);
                var date = new Date();
                var imgSrc = "Attached/" + pictureUrl + "?dx=" + date.getTime();
                linkFile.innerHTML = fileName;
                linkFile.setAttribute("href", imgSrc);
                linkFile.setAttribute("target", "_blank");
                var container = document.getElementById("uploadedListFiles");
                container.appendChild(linkFile);
                container.appendChild(document.createElement("br"));
            }
        }
        // ]]>
    </script>

    <script type="text/javascript">
        // <![CDATA[
        function DoNewClick(s, e) {
            cbdata.PerformCallback('New');
            pnldata.SetVisible(true);
            fuAttachment.SetEnabled(true);
            btnSaveClient.SetEnabled(true);
            pnlcompliantList.SetVisible(false);
            DvgrdregEdit.SetVisible(false);
        }
        function DoAttachClick(s, e) {
            fuAttachment.SetVisible(true);
            cbdata.PerformCallback('Attach');
            pnlcompliantList.SetVisible(false);
            pnldata.SetVisible(true);
            RoundPanel.SetVisible(true);
        }

        function DoShowClick(s, e) {
            pnldata.SetVisible(false);
            btnSaveClient.SetEnabled(false);
            pnlcompliantList.SetVisible(true);
            fuAttachment.SetVisible(false);
            DvgrdregEdit.SetVisible(true);
        }

        function DoSaveClick(s, e) {
            if (ASPxClientEdit.ValidateGroup('compliantdetail')) {
                cbdata.PerformCallback('Save');
            }
        }
        //===========================Grid===============
        var rowVisibleIndex;
        var strID;
        var strGuid;

        function DvgrdregEdit_CustomButtonClick(s, e) {
            if (e.buttonID != 'Del' && e.buttonID != 'Edit') return;
            if (e.buttonID == 'Del') {
                rowVisibleIndex = e.visibleIndex;
                strGuid = s.GetRowKey(e.visibleIndex);
                ShowPopup();
            }
            if (e.buttonID == 'Edit') {
                btnSaveClient.SetEnabled(true);
                DvgrdregEdit.GetRowValues(e.visibleIndex, "ComplaintGuid", OnGetSelectedFieldValues);
            }
        }

        function OnEndCallback(s, e) {

            if (s.cpStatus == "Success") {
                if (s.cpAction == "save") {
                    pnldata.SetVisible(false);
                    pnlcompliantList.SetVisible(true);
                    DvgrdregEdit.SetVisible(true);
                    ShowSuccess(s.cpMessage);
                }
                if (s.cpAction == "update") {
                    pnldata.SetVisible(false);
                    pnlcompliantList.SetVisible(true);
                    DvgrdregEdit.SetVisible(true);
                    ShowSuccess(s.cpMessage);
                }
                if (s.cpAction == "delete") {
                    pnldata.SetVisible(false);
                    pnlcompliantList.SetVisible(true);
                    DvgrdregEdit.SetVisible(true);
                    ShowSuccess(s.cpMessage);
                }
                if (s.cpAction == "new") {
                    pnldata.SetVisible(true);
                    btnSaveClient.SetEnabled(true);
                    pnlcompliantList.SetVisible(false);
                    DvgrdregEdit.SetVisible(false);
                }
                if (s.cpAction == "Loaded") {
                    pnlcompliantList.SetVisible(false);
                    DvgrdregEdit.SetVisible(false);
                    txtreferenceno.SetVisible(true);
                    fuAttachment.SetVisible(true);
                    //cbdata.PerformCallback('Attach');
                    RoundPanel.SetVisible(true);
                    lblreferenceno.SetVisible(true);
                }
            }
            else if (s.cpStatus == "ERROR") {
                ShowError(s.cpMessage);

            }
        }

        function OnGetSelectedFieldValues(values) {
            cbdata.PerformCallback('Edit' + '|' + values);
            pnldata.SetVisible(true);
        }

        function ShowPopup(rowId) {

            popupControl.Show();
            btnYes.Focus();

        }

        function btnYes_Click(s, e) {

            ClosePopup(true);
        }

        function btnNo_Click(s, e) {
            ClosePopup(false);
        }

        function ClosePopup(result) {
            popupControl.Hide();
            if (result) {
                cbdata.PerformCallback('Delete' + '|' + strGuid);
            }
        }

        function DoSearch(s, e) {
            DvgrdregEdit.PerformCallback();
        }

        function gridCompliantListEndCallBack(s, e) {
            if (s.cpStatus == "Success") {
                if (s.cpAction == "Searching") {
                }

            }
            else if (s.cpStatus == "Error") {
                if (s.cpAction == "Searching") {
                    ShowError(s.cpMessage);
                }
                if (s.cpAction == "NoCompliants") {
                    ShowError(s.cpMessage);
                }
            }
        }

        function DownLoadFile(s, e) {
            if (e.buttonID == 'Download') {
                gvUploadedFiles.GetRowValues(e.visibleIndex, "AttachmentTitle", OnGetSelectedFileDownLoadValues);
            }
        }

        function OnGetSelectedFileDownLoadValues(values) {
            gvUploadedFiles.PerformCallback('DownLoad' + '|' + values);
        }
    </script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            pnldata.SetVisible(false);
            btnSaveClient.SetEnabled(false);
            pnlcompliantList.SetVisible(true);
            fuAttachment.SetVisible(false);
            DvgrdregEdit.SetVisible(true);
        }
    </script>
    <style type="text/css">
        .erroStyle {
            font-size: smaller;
        }
        .div hidden {
            display:none;
        }
    </style>
    <style type="text/css">
        .auto-style2 {
            width: 414px;
        }

        .auto-style3 {
            width: 391px;
        }

        .auto-style4 {
            width: 357px;
        }

        .auto-style6 {
        }

        .auto-style7 {
            width: 289px;
        }

        .auto-style12 {
            width: 123px;
        }

        .auto-style15 {
            width: 100%;
        }

        .auto-style17 {
            width: 144px;
        }

        .auto-style18 {
            width: 278px;
        }

        .auto-style19 {
            height: 68px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceHolder" runat="Server">
    <uc1:Alert runat="server" ID="Alert" />
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
    <div style="width: 993px">
        <table id="tblOuter">
            <tr>
                <td class="auto-style19">
                    <asp:Panel ID="pnlMenu0" runat="server" CssClass="RoundPanelToolbar" Width="950px" meta:resourcekey="pnlMenu0Resource1">
                        <table cellpadding="0" cellspacing="0" style="width: 100%; padding: 0px;" width="990px">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <dx:ASPxButton ID="btnNew" runat="server" AutoPostBack="False" ClientInstanceName="btnNewClient" Font-Names="Visual Geez Unicode" Font-Size="Small"
                                                    CausesValidation="false" HorizontalAlign="Center" Text="አዲስ" Theme="Office2010Silver" VerticalAlign="Middle" Width="70px" meta:resourcekey="btnNewResource1">
                                                    <ClientSideEvents Click="function(s, e) {
	                                                                    DoNewClick();
                                                                    }" />
                                                    <Image Url="~/Controls/ToolbarImages/new.gif">
                                                    </Image>
                                                </dx:ASPxButton>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnSave" runat="server" AutoPostBack="False" ClientInstanceName="btnSaveClient" Font-Names="Visual Geez Unicode" Font-Size="Small"
                                                    HorizontalAlign="Center" Text="አስቀምጥ" ClientEnabled="false" Theme="Office2010Silver" VerticalAlign="Middle" Width="70px" TabIndex="11" meta:resourcekey="btnSaveResource1">
                                                    <ClientSideEvents Click="function(s, e) {
	                                                                    DoSaveClick();
                                                                    }" />
                                                    <Image Url="~/Controls/ToolbarImages/save.gif">
                                                    </Image>
                                                </dx:ASPxButton>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnShow" runat="server" AutoPostBack="False"
                                                    ClientInstanceName="btnShowClient" Font-Names="Visual Geez Unicode"
                                                    Font-Size="Small" HorizontalAlign="Center" meta:resourceKey="btnShowResource1"
                                                    Text="አሳይ" Theme="Office2010Silver"
                                                    ValidationSettings-ValidationGroup="saveTeam" VerticalAlign="Middle"
                                                    Width="70px">
                                                    <ClientSideEvents Click="DoShowClick" />
                                                    <Image Url="~/Files/Images/action_settings.gif">
                                                    </Image>
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxCallbackPanel ID="cbData" ClientInstanceName="cbdata" OnCallback="CallbackPanel_Callback" runat="server" Width="960px" meta:resourcekey="cbDataResource1">
                        <ClientSideEvents EndCallback="OnEndCallback" />
                        <PanelCollection>
                            <dx:PanelContent runat="server" SupportsDisabledAttribute="True" meta:resourcekey="PanelContentResource3">
                                <br />
                                <dx:ASPxPanel runat="server" ID="pnlData" ClientInstanceName="pnldata" Width="960px" meta:resourcekey="pnlDataResource1">
                                    <PanelCollection>
                                        <dx:PanelContent runat="server" SupportsDisabledAttribute="True" meta:resourcekey="PanelContentResource2">
                                            <table class="table-condensed">
                                                <tr>
                                                    <td style="text-align: right" class="auto-style17">
                                                        <dx:ASPxLabel ID="lblreferenceno" runat="server" ClientVisible="false" ClientInstanceName="lblreferenceno" meta:resourcekey="lblreferencenoResource1">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td class="auto-style6">
                                                        <dx:ASPxTextBox ID="txtreferenceno" runat="server" ClientInstanceName="txtreferenceno" ClientVisible="false"
                                                            ClientEnabled="false" MaxLength="50" Width="270px" TabIndex="2" meta:resourcekey="txtreferencenoResource1">
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center">
                                                        <asp:Label ID="lblFullName" runat="server" AssociatedControlID="txtFullName" Text="ሙሉ ስም " meta:resourcekey="lblFullNameResource1"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxTextBox ID="txtFullName" runat="server" ClientInstanceName="txtFullName" Height="36px" Width="270px" meta:resourcekey="txtFullNameResource1">
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                    <td style="text-align: right">
                                                        <asp:Label ID="lblComplainType" runat="server" Text="የጥቆማ አይነት " AssociatedControlID="cboComplainType" meta:resourcekey="lblComplainTypeResource1"></asp:Label>
                                                        <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*" meta:resourcekey="Label1Resource1" SkinID="Notifire"></asp:Label>
                                                    </td>
                                                    <td style="text-align: right">
                                                        <dx:ASPxComboBox ID="cboComplainType" runat="server" EnableIncrementalFiltering="True" EnableViewState="False" IncrementalFilteringMode="StartsWith" TabIndex="1" ValueField="Name" Width="270px" meta:resourcekey="cboComplainTypeResource1">
                                                            <ValidationSettings Display="Dynamic" ErrorTextPosition="Bottom" SetFocusOnError="True" ErrorDisplayMode="Text" ValidationGroup="compliantdetail">
                                                                <RequiredField IsRequired="True" ErrorText="እባክዎን የጥቆማ አይነት ይምረጡ" />
                                                            </ValidationSettings>
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style17" style="text-align: right">
                                                        <asp:Label ID="lblMeansOfReport" runat="server" meta:resourcekey="lblMeansOfReportResource1" Width="150px">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="auto-style6">
                                                        <dx:ASPxComboBox ID="cboMeansOfReport" runat="server" CssClass="fullWidthPlus" EnableIncrementalFiltering="True" IncrementalFilteringMode="StartsWith"
                                                            NullText="ይምረጡ" Width="270px" TabIndex="8" meta:resourcekey="cboMeansOfReportResource1">
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                    <td class="auto-style12" style="text-align: right">
                                                        <asp:Label ID="lblDateOfReport" runat="server" Text="ቀን: " meta:resourcekey="lblDateOfReportResource1">
                                                        </asp:Label>
                                                    </td>
                                                    <td class="auto-style7" style="text-align: left">
                                                        <cdp:CUSTORDatePicker ID="txtDate" runat="server" SelectedDate="" TextCssClass="" Width="250px" TabIndex="3" meta:resourcekey="txtDateResource1" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right" class="auto-style17">
                                                        <asp:Label ID="ASPxLabel5" runat="server" AssociatedControlID="cboOrganization" Text="ተቋም " meta:resourcekey="ASPxLabel5Resource1"></asp:Label>
                                                        <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="*" meta:resourcekey="Label5Resource2" SkinID="Notifire"></asp:Label>
                                                    </td>
                                                    <td class="auto-style6">
                                                        <dx:ASPxComboBox ID="cboOrganization" runat="server" ClientInstanceName="cboOrganization" EnableIncrementalFiltering="True" IncrementalFilteringMode="StartsWith"
                                                            NullText="ይምረጡ" Width="270px" meta:resourcekey="cboOrganizationResource1">
                                                            <ValidationSettings Display="Dynamic" ErrorTextPosition="Bottom" SetFocusOnError="True" ErrorDisplayMode="Text" ValidationGroup="compliantdetail">
                                                                <RequiredField IsRequired="True" ErrorText="እባክዎን ጥቆማ የቀረበበት ተቋም ይምረጡ" />
                                                            </ValidationSettings>
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                    <td class="auto-style12" style="text-align: right">
                                                        <asp:Label ID="lblemail" runat="server" AssociatedControlID="lblEmail" Text="ኢሜል " meta:resourcekey="lblemailResource1"></asp:Label>
                                                    </td>
                                                    <td class="auto-style7" style="text-align: right">
                                                        <dx:ASPxTextBox ID="txtEmail" runat="server" Height="36px" TabIndex="5" Width="270px" ValidationGroup="Address" meta:resourcekey="txtEmailResource1">
                                                            <ValidationSettings ErrorDisplayMode="None" SetFocusOnError="True">
                                                                <RegularExpression ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style17" style="text-align: right">
                                                        <asp:Label ID="lblTelephone" runat="server" Text="ስልክ ቁጥር " AssociatedControlID="txtTelephone" meta:resourcekey="lblTelephoneResource1"></asp:Label>
                                                    </td>
                                                    <td class="auto-style6">
                                                        <dx:ASPxTextBox ID="txtTelephone" runat="server" ClientInstanceName="txtTelephone" CssClass="style235" Height="36px" onkeypress="return AcceptNumericOnly(event,false)" TabIndex="6" ValidationGroup="Address" Width="270px" meta:resourcekey="txtTelephoneResource1">
                                                            <ValidationSettings ErrorText="">
                                                                <ErrorImage ToolTip="Location required">
                                                                </ErrorImage>
                                                                <RequiredField ErrorText="" />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                    <td class="auto-style12" style="text-align: right">
                                                        <asp:Label ID="lblComplaintRemark" runat="server" AssociatedControlID="txtCompliantReason" Text="የጥቆማ መነሻ " meta:resourcekey="lblComplaintRemarkResource1"></asp:Label>
                                                        <asp:Label ID="Label3" runat="server" ForeColor="Red" Text="*" meta:resourcekey="Label3Resource1" SkinID="Notifire"></asp:Label>
                                                    </td>
                                                    <td class="auto-style7" style="text-align: right">
                                                        <dx:ASPxTextBox ID="txtCompliantReason" runat="server" Height="36px" TabIndex="7" Width="270px" meta:resourcekey="txtCompliantReasonResource1">
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style17" style="text-align: right" valign="top">
                                                        <asp:Label ID="lblRemark" runat="server" Text="ዝርዝር መረጃ " meta:resourcekey="lblRemarkResource1"></asp:Label>
                                                        <asp:Label ID="Label6" runat="server" ForeColor="Red" Text="*" meta:resourcekey="Label6Resource1" SkinID="Notifire"></asp:Label>
                                                    </td>
                                                    <td class="auto-style6" colspan="3">
                                                        <dx:ASPxMemo ID="txtComplaintRemark" runat="server" Height="150px" TabIndex="9" Width="100%" meta:resourcekey="txtComplaintRemarkResource1">
                                                        </dx:ASPxMemo>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style17" style="text-align: right" valign="top">
                                                        <dx:ASPxButton ID="btnAttach" runat="server" AutoPostBack="False" ClientInstanceName="btnAttach" Height="19px" Text="አያይዝ" meta:resourcekey="btnAttachResource1">
                                                            <ClientSideEvents Click="function(s, e) 
                                                               {
	                                                            DoAttachClick();
                                                               }" />
                                                            <Image Url="~/Images/expand_blue.jpg">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                    </td>
                                                    <td class="auto-style6" colspan="3">
                                                        <table class="auto-style15">
                                                            <tr>
                                                                <td class="auto-style18">
                                                                    <dx:ASPxUploadControl ID="fuAttachment" runat="server" ClientInstanceName="fuAttachment" FileInputCount="5" Height="100%" OnFileUploadComplete="UploadControl_FileUploadComplete" 
                                                                        ShowAddRemoveButtons="True" ShowProgressPanel="True" ShowUploadButton="false" TabIndex="10" Theme="Office2010Silver" Width="273px" meta:resourcekey="fuAttachmentResource1">
                                                                        <ValidationSettings   AllowedFileExtensions=".jpg, .jpeg, .jpe, .gif, .pdf, .doc" GeneralErrorText="ሰነዱ በትክክል አልተቀመጠም። እባክዎን እንደገና ያያይዙ" NotAllowedFileExtensionErrorText="የሰነዱ አይነት የተፈቀደ አይደለም። እባክዎ እንደገና ያያይዙ">
                                                                        </ValidationSettings>
                                                                        <ClientSideEvents FileUploadComplete="function(s, e) { FileUploaded(s, e) }" FileUploadStart="function(s, e) { FileUploadStart(); }" />
                                                                        <AddButton Text="ጨምር">
                                                                        </AddButton>
                                                                        <BrowseButton Text="አስስ...">
                                                                        </BrowseButton>
                                                                        <RemoveButton Text="ሰርዝ">
                                                                        </RemoveButton>
                                                                        <UploadButton Text="አያይዝ">
                                                                        </UploadButton>
                                                                    </dx:ASPxUploadControl>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" Theme="Moderno" ClientVisible="false" ClientInstanceName="RoundPanel" HeaderText="የተያያዙ ሰነዶች (jpeg, gif)" Height="100%" TabIndex="10"
                                                                    Width="100%" meta:resourcekey="ASPxRoundPanel2Resource1">
                                                                    <PanelCollection>
                                                                        <dx:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="PanelContent4Resource1">
                                                                            <dx:ASPxGridView ID="gvUploadedFiles" runat="server" ClientEnabled="true" Width="100%" OnCustomCallback="gvUploadedFiles_CustomCallback"
                                                                                ClientInstanceName="gvUploadedFiles" KeyFieldName="AttachmentGuid" TextField="AttachmentTitle">
                                                                                <Columns>
                                                                                    <dx:GridViewDataTextColumn FieldName="AttachmentGuid" visibleIndex="0" Visible="false">
                                                                                    </dx:GridViewDataTextColumn>
                                                                                    <dx:GridViewDataTextColumn FieldName="AttachmentTitle" visibleIndex="1" Caption="ፋይል ስም" Width="75%">
                                                                                    </dx:GridViewDataTextColumn>
                                                                                    <dx:GridViewDataTextColumn FieldName="FileSizeCaption" visibleIndex="2" Caption="መጠን" Width="50px">
                                                                                    </dx:GridViewDataTextColumn>
                                                                                    <dx:GridViewCommandColumn  Width="100px" visibleIndex="3" ShowInCustomizationForm="True">
                                                                                        <CustomButtons>
                                                                                            <dx:GridViewCommandColumnCustomButton ID="Download">
                                                                                                 <Image ToolTip="Dowload" IconID="actions_download_16x16" />
                                                                                            </dx:GridViewCommandColumnCustomButton>
                                                                                        </CustomButtons>
                                                                                        <HeaderTemplate>
                                                                                        </HeaderTemplate>
                                                                                    </dx:GridViewCommandColumn>
                                                                                </Columns>
                                                                                <ClientSideEvents CustomButtonClick="DownLoadFile"/>
                                                                            </dx:ASPxGridView>
                                                                        </dx:PanelContent>
                                                                    </PanelCollection>
                                                                </dx:ASPxRoundPanel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                </dx:ASPxPanel>
                                <dx:ASPxPanel runat="server" ID="pnlcompliantList" ClientVisible="true" ClientInstanceName="pnlcompliantList" Width="750px" meta:resourcekey="pnlDataResource1">
                                    <PanelCollection>
                                        <dx:PanelContent runat="server" SupportsDisabledAttribute="True" meta:resourcekey="PanelContentResource2">
                                            <table width="100%" border="0">
                                                <tr>
                                                    <td width="100%" rowspan="1" align="right">
                                                        <dx:ASPxLabel ID="lblOrganizationSearch" runat="server" Text="ተቋም: " meta:resourcekey="ASPxLabel1Resource1">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td width="50%" rowspan="1" align="left">
                                                        <dx:ASPxComboBox ID="cboOrganizationSearch" runat="server" Height="26px" Width="250px" DropDownStyle="DropDown" EnableIncrementalFiltering="True" IncrementalFilteringMode="Contains"  meta:resourcekey="cboOrganizationResource1">
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td rowspan="1" align="right">
                                                        <dx:ASPxLabel ID="lblComplianTypesSearch" runat="server" meta:resourcekey="lblComplainTypeResource1">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td width="50%" rowspan="1" align="left">
                                                        <dx:ASPxComboBox ID="cboComplianTypesSearch" runat="server" Height="26px" Width="250px" DropDownStyle="DropDown"
                                                            NullText="ይምረጡ" PopupHorizontalAlign="RightSides" meta:resourcekey="cboComplainTypeResource1">
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                    <td rowspan="1" align="center" class="auto-style24"></td>
                                                </tr>
                                                <tr>
                                                    <td width="70%" rowspan="1" align="right" class="auto-style40">
                                                        <dx:ASPxLabel ID="lblDate" runat="server"   meta:resourcekey="lblDateOfReportResource1">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td width="34%" rowspan="1" align="center" class="auto-style40">
                                                        <table>
                                                            <tr>
                                                                <td align="left">
                                                                    <cdp:CUSTORDatePicker ID="txtDateFrom" runat="server" SelectedDate=""
                                                                        TextCssClass="" Width="110px" meta:resourcekey="txtDateFromResource1" />
                                                                </td>
                                                                <td>
                                                                    <strong>-</strong>
                                                                </td>
                                                                <td align="left">
                                                                    <cdp:CUSTORDatePicker ID="txtDateTo" runat="server" Width="110px" meta:resourcekey="txtDateToResource1" SelectedDate="" TextCssClass="" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td rowspan="1" align="left" class="auto-style40">
                                                        <dx:ASPxButton ID="cmdSearch" runat="server" Text="ፈልግ:" HorizontalAlign="Center" CausesValidation="false" ClientInstanceName="btnsearch" Theme="Office2010Silver" Font-Names="Visual Geez Unicode"
                                                            EnableClientSideAPI="True" AutoPostBack="false" meta:resourcekey="cmdSearchResource1">
                                                            <Image Url="~/images/Toolbar/search.gif">
                                                            </Image>
                                                            <ClientSideEvents Click="DoSearch" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>

                                        </dx:PanelContent>
                                    </PanelCollection>
                                </dx:ASPxPanel>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <dx:ASPxGridView ID="DvgrdregEdit" runat="server" KeyFieldName="ComplaintGuid" ClientInstanceName="DvgrdregEdit" AutoGenerateColumns="False" Theme="Office2010Silver"
                                                Width="100%" EnableTheming="True" Font-Names="Visual Geez Unicode" OnPageIndexChanged="DvgrdregEdit_PageIndexChanged" OnHtmlRowPrepared="DvgrdregEdit_HtmlRowPrepared"
                                                OnCustomColumnDisplayText="DvgrdregEdit_CustomColumnDisplayText" OnCustomCallback="DvgrdregEdit_CustomCallback" meta:resourcekey="DvgrdregEditResource1">
                                                <ClientSideEvents EndCallback="gridCompliantListEndCallBack" CustomButtonClick="DvgrdregEdit_CustomButtonClick" />
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="ID" FieldName="ComplaintGuid" ShowInCustomizationForm="True" VisibleIndex="0" Visible="False" meta:resourcekey="GridViewDataTextColumnResource1">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="ሙሉ ስም" FieldName="ReportedBy" VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource2">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="ቀን" FieldName="ReportedDate" VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnResource3">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="የቅሬታ አይነት" FieldName="ComplainType" VisibleIndex="4" meta:resourcekey="GridViewDataTextColumnResource4">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="ውሳኔ የተሰጠበት ቀን" FieldName="DateScreened" ShowInCustomizationForm="True" VisibleIndex="5" meta:resourcekey="GridViewDataTextColumnResource5" Visible="False">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="የውሳኔው ሁኔታ" FieldName="FindingSummary" ShowInCustomizationForm="True" VisibleIndex="6" meta:resourcekey="GridViewDataTextColumnResource6">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewCommandColumn ShowInCustomizationForm="True" Width="100px" VisibleIndex="7">
                                                        <CustomButtons>
                                                            <dx:GridViewCommandColumnCustomButton ID="Edit" Text="አርም" meta:resourcekey="GridViewCommandColumnCustomButtonResource1"></dx:GridViewCommandColumnCustomButton>
                                                            <dx:GridViewCommandColumnCustomButton ID="Del" Text="ሰርዝ" meta:resourcekey="GridViewCommandColumnCustomButtonResource2"></dx:GridViewCommandColumnCustomButton>
                                                        </CustomButtons>
                                                    </dx:GridViewCommandColumn>
                                                </Columns>
                                                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True" />
                                                <SettingsPager>
                                                    <Summary Text="ገፅ {0} ከ {1} ({2} ጥቆም)" />
                                                </SettingsPager>
                                            </dx:ASPxGridView>
                                        </td>

                                    </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <dx:ASPxPopupControl ID="ASPxPopupControl1" runat="server" ClientInstanceName="popupControl" Font-Names="Visual Geez Unicode" Font-Size="Small" HeaderText="Delete Confirmation" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" meta:resourcekey="ASPxPopupControl1Resource1">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl runat="server" meta:resourcekey="PopupControlContentControlResource1">
                                                        <uc1:ConfirmBox runat="server" ID="ConfirmBox" />
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                            </dx:ASPxPopupControl>
                                        </td>
                                    </tr>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxCallbackPanel>
                </td>
            </tr>
        </table>
    </div>
    <div>
        <asp:HiddenField ID="hdfldCompliantGuid" runat="server" />
        <asp:HiddenField ID="hdfldApplicationName" runat="server" />
        <asp:HiddenField ID="HidUserid" runat="server" />
        <asp:HiddenField ID="HidlLanguage" runat="server" />
    </div>
</asp:Content>

