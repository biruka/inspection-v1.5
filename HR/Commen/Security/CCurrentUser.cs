﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Security.Principal;

namespace CUSTOR.Commen
{
    public class CCurrentUser
    {
        public static IPrincipal CurrentUser
        {
            get { return HttpContext.Current.User; }
        }


        public static string CurrentUserIP
        {
            get { return HttpContext.Current.Request.UserHostAddress; }
        }


        public static string CurrentUserName
        {
            get
            {
                string userName = string.Empty;
                if (CurrentUser.Identity.IsAuthenticated)
                {
                    userName = CurrentUser.Identity.Name;
                }
                return userName;
            }
        }
    }
}
