﻿using System;
using System.Web;
using System.Web.Profile;
using System.Web.Security;

namespace Service.Security
{
    public class ProfileGroupOrganization : System.Web.Profile.ProfileGroupBase
    {

        public virtual string Name
        {
            get
            {
                return ((string)(this.GetPropertyValue("Name")));
            }
            set
            {
                this.SetPropertyValue("Name", value);
            }
        }

        public virtual string GUID
        {
            get
            {
                return ((string)(this.GetPropertyValue("GUID")));
            }
            set
            {
                this.SetPropertyValue("GUID", value);
            }
        }

        public virtual string Administration
        {
            get
            {
                return ((string)(this.GetPropertyValue("Administration")));
            }
            set
            {
                this.SetPropertyValue("Administration", value);
            }
        }

        public virtual string Category
        {
            get
            {
                return ((string)(this.GetPropertyValue("Category")));
            }
            set
            {
                this.SetPropertyValue("Category", value);
            }
        }
        public virtual string Language
        {
            get
            {
                return ((string)(this.GetPropertyValue("Language")));
            }
            set
            {
                this.SetPropertyValue("Language", value);
            }
        }
        public virtual string Zone
        {
            get
            {
                return ((string)(this.GetPropertyValue("Zone")));
            }
            set
            {
                this.SetPropertyValue("Zone", value);
            }
        }
        public virtual string Wereda
        {
            get
            {
                return ((string)(this.GetPropertyValue("Wereda")));
            }
            set
            {
                this.SetPropertyValue("Wereda", value);
            }
        }
    }

    public class ProfileGroupStaff : System.Web.Profile.ProfileGroupBase
    {

        public virtual string FullName
        {
            get
            {
                return ((string)(this.GetPropertyValue("FullName")));
            }
            set
            {
                this.SetPropertyValue("FullName", value);
            }
        }

        public virtual string GUID
        {
            get
            {
                return ((string)(this.GetPropertyValue("GUID")));
            }
            set
            {
                this.SetPropertyValue("GUID", value);
            }
        }

        public virtual string Units
        {
            get
            {
                return ((string)(this.GetPropertyValue("Units")));
            }
            set
            {
                this.SetPropertyValue("Units", value);
            }
        }

    }
    public class ProfileCommon : System.Web.Profile.ProfileBase
    {


        public virtual ProfileGroupOrganization Organization
        {
            get
            {
                return ((ProfileGroupOrganization)(this.GetProfileGroup("Organization")));
            }
        }

        public virtual ProfileGroupStaff Staff
        {
            get
            {
                return ((ProfileGroupStaff)(this.GetProfileGroup("Staff")));
            }
        }

        public virtual ProfileCommon GetProfile(string username)
        {
            return ((ProfileCommon)(ProfileBase.Create(username)));
        }
        public static ProfileCommon GetProfile()
        {
            return Create(HttpContext.Current.Request.IsAuthenticated ?
                   HttpContext.Current.User.Identity.Name : HttpContext.Current.Request.AnonymousID,
                   HttpContext.Current.Request.IsAuthenticated) as ProfileCommon;
        }

        public static ProfileCommon GetUserProfile(string username)
        {
            return Create(username) as ProfileCommon;
        }

        //public static ProfileCommon GetUserProfile()
        //{
            //return Create(Membership.GetUser().UserName) as ProfileCommon;
        //}
        [CustomProviderData("FirstName;nvarchar")]
        public virtual string FirstName
        {
            get
            {
                return ((string)(this.GetPropertyValue("FirstName")));
            }
            set
            {
                this.SetPropertyValue("FirstName", value);
            }
        }

        [CustomProviderData("LastName;nvarchar")]
        public virtual string LastName
        {
            get
            {
                return ((string)(this.GetPropertyValue("LastName")));
            }
            set
            {
                this.SetPropertyValue("LastName", value);
            }
        }


        [CustomProviderData("Age;int")]
        public virtual int Age
        {
            get
            {
                return ((int)(this.GetPropertyValue("Age")));
            }
            set
            {
                this.SetPropertyValue("Age", value);
            }
        }

        [CustomProviderData("DateOfBirth;datetime")]
        public virtual DateTime DateOfBirth
        {
            get
            {
                return ((DateTime)(this.GetPropertyValue("DateOfBirth")));
            }
            set
            {
                this.SetPropertyValue("DateOfBirth", value);
            }
        }
    }
}