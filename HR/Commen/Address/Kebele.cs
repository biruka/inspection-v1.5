﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
namespace CUSTOR.Commen
{
    [Serializable]
    public class Kebele
    {
        public string KebeleCode
        { get; set; }

        public string KebeleName
        { get; set; }


        private static string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }
        public List<Kebele> GetKebeles(string strWeredaCode)
        {
            string strSQL = "SELECT code, amDescription from tblKebele Where Parent=@Parent";
            List<Kebele> Kebeles = new List<Kebele>();

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = strSQL;
                        command.CommandType = CommandType.Text;
                        command.Parameters.AddWithValue("@Parent", strWeredaCode);
                        connection.Open();
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            Kebele Kebele;
                            if (dataReader != null)
                                while (dataReader.Read())
                                {
                                    Kebele = new Kebele();
                                    Kebele.KebeleCode = Convert.ToString(dataReader["code"]);
                                    Kebele.KebeleName = Convert.ToString(dataReader["amDescription"]);
                                    Kebeles.Add(Kebele);
                                }
                        }
                    }
                }
                return Kebeles;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in GetKebeles: " + ex.Message);

            }
        }
    }
}