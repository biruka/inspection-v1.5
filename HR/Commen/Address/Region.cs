﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
namespace CUSTOR.Commen
{
    [Serializable]
    public class Region
    {
        public string RegionCode
        { get; set; }

        public string RegionName
        { get; set; }


        private static string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }
        public List<Region> GetRegions()
        {
            string strSQL = "SELECT code, amDescription from tblRegion";
            List<Region> Regions = new List<Region>();

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = strSQL;
                        command.CommandType = CommandType.Text;
                        connection.Open();
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            Region Region;
                            if (dataReader != null)
                                while (dataReader.Read())
                                {
                                    Region = new Region();
                                    Region.RegionCode = Convert.ToString(dataReader["code"]);
                                    Region.RegionName = Convert.ToString(dataReader["amDescription"]);
                                    Regions.Add(Region);
                                }
                        }
                    }
                }
                return Regions;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in GetRegions: " + ex.Message);

            }
        }


    }
}