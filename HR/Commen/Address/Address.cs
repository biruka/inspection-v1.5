﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for Address
/// </summary>

namespace CUSTOR.Commen
{
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.ComponentModel.ToolboxItem(false)]
//[System.Web.Script.Services.ScriptService]

public class Address : System.Web.Services.WebService {

    public Address () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

   
    [WebMethod]
    public List<Region> GetRegions()
    {
        Region r = new Region();
        return r.GetRegions();

    }

    [WebMethod]
    public List<Zone> GetZones(string RegionCode)
    {
        Zone z = new Zone();
        if (RegionCode.Length == 1)
            RegionCode = "0" + RegionCode;
        return z.GetZones(RegionCode);

    }

    [WebMethod]
    public List<Wereda> GetWeredas(string ZoneCode)
    {
        if (ZoneCode.Length == 1)
            ZoneCode = "0" + ZoneCode;
        Wereda w = new Wereda();
        return w.GetWeredas(ZoneCode);

    }

    [WebMethod]
    public List<Kebele> GetKebeles(string WeredaCode)
    {
        if (WeredaCode.Length == 1)
            WeredaCode = "0" + WeredaCode;
        Kebele k = new Kebele();
        return k.GetKebeles(WeredaCode);

    }
}
}
