﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
namespace CUSTOR.Commen
{
    [Serializable]
    public class Wereda
    {
        public string WeredaCode
        { get; set; }

        public string WeredaName
        { get; set; }

        private static string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }
        public List<Wereda> GetWeredas(string strZoneCode)
        {
            string strSQL = "SELECT code, amDescription from tblWoreda Where Parent=@Parent";
            List<Wereda> Weredas = new List<Wereda>();

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = strSQL;
                        command.CommandType = CommandType.Text;
                        command.Parameters.AddWithValue("@Parent", strZoneCode);
                        connection.Open();
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            Wereda Wereda;
                            if (dataReader != null)
                                while (dataReader.Read())
                                {
                                    Wereda = new Wereda();
                                    Wereda.WeredaCode = Convert.ToString(dataReader["code"]);
                                    Wereda.WeredaName = Convert.ToString(dataReader["amDescription"]);
                                    Weredas.Add(Wereda);
                                }
                        }
                    }
                }
                return Weredas;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in GetWeredas: " + ex.Message);

            }
        }

    }
}