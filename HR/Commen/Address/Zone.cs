﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;
namespace CUSTOR.Commen
{
    [Serializable]
    public class Zone
    {
        public string ZoneCode
        { get; set; }

        public string ZoneName
        { get; set; }
        private static string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }
        public List<Zone> GetZones(string strRegionCode)
        {
            string strSQL = "SELECT code, amDescription from tblZone Where Parent=@Parent";
            List<Zone> Zones = new List<Zone>();

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = strSQL;
                        command.CommandType = CommandType.Text;
                        command.Parameters.AddWithValue("@Parent", strRegionCode);
                        connection.Open();
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            Zone Zone;
                            if (dataReader != null)
                                while (dataReader.Read())
                                {
                                    Zone = new Zone();
                                    Zone.ZoneCode = Convert.ToString(dataReader["code"]);
                                    Zone.ZoneName = Convert.ToString(dataReader["amDescription"]);
                                    Zones.Add(Zone);
                                }
                        }
                    }
                }
                return Zones;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in GetZones: " + ex.Message);

            }
        }
    }
}