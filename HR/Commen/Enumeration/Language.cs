﻿using System;
using System.Collections.Generic;
using System.Web;
namespace CUSTOR.Commen
{
    public static class Language
    {
        public enum eLanguage
        {
            eEnglish = 0,
            eAmharic = 1,
            eAfanOromo = 2,
            eTigrigna = 3,
            eAfar = 4,
            eSomali = 5,
            eArabic = 6,
            eFrench = 7
        }
    }
}