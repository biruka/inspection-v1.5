﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace CUSTOR.Commen
{
   public class Inspectype
    {
       public enum eInspectType
       {
           [Description("Recruitment|ስራ ቅጥር|RecruitmentOromo|ስራ ቅጥርTig|RecruitmentAfar|RecruitmentSomali")]
           Recruitment=311,
           [Description("Promotion|ዕድገት|PromotionOromo|ዕድገትTig|PromotionAfar|PromotionSomali")]
           Promotion,
           [Description("Transfer|ዝውውር|TransferOromo|ዝውውርTig|TransferAfar|TransferSomali")]
            Transfer,
            [Description("ServiceExtension|አገልግሎት ማራዘም|ServiceExtensionOromo|አገልግሎት ስለመቀጠልTig|ServiceExtension|ServiceExtensionSomali")]
            ServiceExtension,
            [Description("ServiceTermination|አገልግሎት ስለሟቋረጥ|ServiceTerminationOromo|አገልግሎት ስለሟቋረጥTig|ServiceTerminationAfar|ServiceTerminationSomali")]
            ServiceTermination,
            [Description("DisciplinaryMeasuresAndGrievanceHandling|ስለደንብ መተላለፍ ውሳኔ እና ቅሬታ ሰሚ|DisciplinaryMeasuresAndGrievanceHandlingOromo|አገልግሎት ስለሟቋረጥTig|DisciplinaryMeasuresAndGrievanceHandlingAfar|DisciplinaryMeasuresAndGrievanceHandlingSomali")]
            DisciplinaryMeasuresAndGrievanceHandling,
            [Description("HealthAndSafety|ጤና እና ደህንነት|HealthAndSafetyOromo|ጤና እና ደህንነትTig|HealthAndSafetyAfar|HealthAndSafetySomali")]
            HealthAndSafety,
            [Description("OvertimeWork|የትርፍ ስራ ሰዓት|OvertimeWorkOromo|ጤና እና ደህንነትTig|OvertimeWorkAfar|OvertimeWorkSomali")]
            OvertimeWork,
            [Description("ScholarshipTraining|የትምህርት ዕድል|ScholarshipTrainingOromo|ጤና እና ደህንነትTig|ScholarshipTrainingAfar|ScholarshipTrainingSomali")]
            ScholarshipTraining,
            [Description("AuthorityDelegation|የስልጣን ውክልና|AuthorityDelegationOromo|ጤና እና ደህንነትTig|AuthorityDelegationAfar|AuthorityDelegationSomali")]
            AuthorityDelegation
       }
    }
}
