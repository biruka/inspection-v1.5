﻿using System;
using System.Text;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using CUSTOR.Commen;
using CUSTOR.Bussiness;
using System.Security.Principal;
using DevExpress.Web;
using System.Web.UI.WebControls.Expressions;
using DevExpress.Web.Internal;
using System.Data.SqlTypes;
using System.Data.SqlClient;

public partial class RegistrationComplian : System.Web.UI.Page
{
    const string UploadDirectory = "~/Attached/";
    const int ThumbnailSize = 100;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //LoadOrganizations();


            Session["PesrsonGuid"] = "6a6fc328-e0e6-47d8-bec5-03d961c1fbf9";
            hdfldCompliantGuid.Value = Convert.ToString(Session["PesrsonGuid"].ToString());
            PersonEmploymentEntityView objorg = new PersonEmploymentEntityView();
            PersonEmploymentEntityViewBussiness objorgBusiness = new PersonEmploymentEntityViewBussiness();
            objorg = objorgBusiness.GetPersonEmploymentEntityView(Guid.Parse(Session["PesrsonGuid"].ToString()));
            string fullname = objorg.First_am_name + " " + objorg.Father_am_name + " " + objorg.Grand_am_name;
            txtFullName.Text = fullname;
            HttpContext.Current.Session["Org_Code"] = objorg.Org_Code;
            cboOrganization.Text = objorg.Org_name;
            txtFullName.ReadOnly = true;
            cboOrganization.ReadOnly = true;
            
            BindGrid();
            //Search();


        }

    }
    //protected void Search()
    //{
    //    ComplaintBusiness compliantInformation = new ComplaintBusiness();

    //    //Search compliants
    //    DataTable dtCompliant = compliantInformation.GetComplaintEmployee(Guid.Parse(Session["PesrsonGuid"].ToString()));
    //    DvgrdregEdit.DataSource = dtCompliant;
    //    DvgrdregEdit.DataBind();

    //}
    protected void LoadComplianTypes()
    {
        FillCombo(cboComplainType, Language.eLanguage.eAmharic, typeof(Enumeration.ComplainTypes));
    }
    private void FillCombo(ASPxComboBox ddl, Language.eLanguage eLang, Type t)
    {
        ddl.DataSource = EnumHelper.GetEnumDescriptions(t, eLang);
        ddl.TextField = "Value";
        ddl.ValueField = "Key";
        ddl.DataBind();

        ddl.SelectedIndex = 1;

        ListEditItem le = new ListEditItem("ይምረጡ", "-1");
        ddl.Items.Insert(0, le);
        ddl.SelectedIndex = 0;

    }
    protected void LoadOrganizations()
    {

        OrganizationBussiness obj = new OrganizationBussiness();
        cboOrganization.ValueField = "OrgGuid";
        cboOrganization.TextField = "DescriptionAm";
        cboOrganization.DataSource = obj.GetOrganizations();
        cboOrganization.DataBind();
        ListEditItem le = new ListEditItem("ይምረጡ", "-1");
        cboOrganization.Items.Insert(0, le);
        cboOrganization.SelectedIndex = 0;
        //}                   

    }
    private void ShowError(string strMsg)
    {
        cbData.JSProperties["cpMessage"] = strMsg;
        cbData.JSProperties["cpStatus"] = "ERROR";
    }
    private string GetErrorDisplay(List<string> strMessages)
    {

        StringBuilder sb = new StringBuilder();
        if (strMessages.Count == 0) return string.Empty;
        sb.Append("Error");
        sb.Append("<ul>");
        foreach (string s in strMessages)
        {
            sb.Append(String.Format("<li>{0}</li>", s));
        }
        sb.Append("</ul>");

        return sb.ToString();

    }
    private List<string> GetErrorMessage()
    {
        List<string> errMessages = new List<string>();


        if (cboOrganization.Value.ToString() == "")
        {
            errMessages.Add("እባክዎ የተቋሙን ስም ይምረጡ");

        }

        if (cboComplainType.Value.ToString() == "" || cboComplainType.Value.ToString() == "-1" )
        {
            errMessages.Add("እባክዎ ቅሬታ አይነት ይምረጡ");

        }
        if (txtCompliantReason.Text == "")
        {
            errMessages.Add("እባክዎ ቅሬታ ምክንያት ያስገቡ");

        }
        if (txtComplaintRemark.Text == "")
        {
            errMessages.Add("እባክዎ ዝርዝር መረጃ ያስገቡ");

        }
        
        return errMessages;
    }
    protected bool DoSave()
    {
        Page.Validate();
        if (!Page.IsValid)
        {

            ShowError("Please fix the errors");
            return false;
        }
        List<string> errMessages = GetErrorMessage();
        if (errMessages.Count > 0)
        {

            ShowError(GetErrorDisplay(errMessages));
            return false;
        }

        try
        {
            Complaint complaintInformation = new Complaint();
            complaintInformation.ComplaintGUID = Guid.NewGuid();
            complaintInformation.OrgGuid = (Guid)HttpContext.Current.Session["Org_Code"];
            complaintInformation.EmployeeGUID = Guid.Parse(Session["PesrsonGuid"].ToString());
            complaintInformation.ReportedBy = txtFullName.Text;
            complaintInformation.ReporterEmail = txtEmail.Text;
            complaintInformation.Referenceno = "";
            complaintInformation.ReporterTel = txtTelephone.Text;
            complaintInformation.MeansOfReport = 1;//"በድረ ገፅ";
            if (cboComplainType.Value.ToString() == null)
                complaintInformation.ScreeningDecision = -1;
            else
                complaintInformation.ScreeningDecision = Convert.ToInt16(cboComplainType.Value.ToString());
            //complaintInformation.ScreeningDecision = (cboComplainType.SelectedItem != null ? Convert.ToInt32(cboComplainType.SelectedItem.Value) : 0);
            complaintInformation.DateReported = DateTime.Now;
            complaintInformation.ComplaintRemark = txtComplaintRemark.Text;
            complaintInformation.ComplaintDescription = txtCompliantReason.Text;
            complaintInformation.DateScreened = Convert.ToDateTime(ConfigurationManager.AppSettings.GetValues("FakeDate")[0]);
            complaintInformation.FindingSummary = Convert.ToInt32(Enumeration.FindingSummary.NotInspected);
            complaintInformation.ScreenedBy = "";
            complaintInformation.FindingSummaryDecision = "";
            complaintInformation.IsGrievance = true;
            complaintInformation.Isselfservice = true;
            hdfldCompliantGuid.Value = complaintInformation.ComplaintGUID.ToString();
            ComplaintBusiness objcomplian = new ComplaintBusiness();
            if ((bool)Session["IsNew"])
            {

                objcomplian.InsertComplaint(complaintInformation);
                //cbData.JSProperties["cpAction"] = "save";
                //ShowSuccess("የጥቆማው መረጃ በትክክል ተቀምጥዋል.");
            }
            else
            {
                complaintInformation.ComplaintGUID = new Guid(Session["ID"].ToString());//Attention
                //complaintInformation. = DateTime.Now;
                objcomplian.UpdateCompliant(complaintInformation);
                //cbData.JSProperties["cpAction"] = "save";
                //ShowSuccess("የጥቆማው መረጃ.");
            }

            BindGrid();
            ClearForm();
            Session["IsNew"] = false;
            cbData.JSProperties["cpAction"] = "save";
            ShowSuccess(" Record was saved successfully.");
            //DoNew();
            return true;
        }
        catch
        {
            //ShowError(ex.Message);
            return false;
        }
    }
   
    protected void UploadControl_FileUploadComplete(object sender,
      FileUploadCompleteEventArgs e)
    {
        try
        {
            e.CallbackData = SavePostedFiles(e.UploadedFile);
        }
        catch (Exception ex)
        {
            e.IsValid = false;
            e.ErrorText = ex.Message;
        }
    }
    DataTable dtAttachment = new DataTable();

    List<Object[]> _dataArray = new List<Object[]>();
    
    //List<AttachmentBussiness.Attdata> _dataArray = new List<double[]>();
    
    //IList<CUSTOR.Bussiness.AttachmentBussiness.Data> Attachdata = new List<CUSTOR.Bussiness.AttachmentBussiness.Data>();

    int count = 0;

    string SavePostedFiles(UploadedFile uploadedFile)
    {

        if (!uploadedFile.IsValid)
            return string.Empty;
        
       

        FileInfo fileInfo = new FileInfo(uploadedFile.FileName);

        string resFileName = MapPath(UploadDirectory) + fileInfo.Name;
        uploadedFile.SaveAs(resFileName);
        CompliantAttachment objCompliantAttachment = new CompliantAttachment();

        

        //if (count == 0)
        //{
        //    System.Type typeString = System.Type.GetType("System.String");
        //    System.Type typeLong = System.Type.GetType("System.Int64");
        //    System.Type typeGuid = System.Type.GetType("System.Guid");
        //    dtAttachment.Columns.Add("AttachmentGuid", typeString);
        //    dtAttachment.Columns.Add("FileNumber", typeString);
        //    dtAttachment.Columns.Add("AttachmentTitle", typeString);
        //    dtAttachment.Columns.Add("DocumentTitle", typeString);
        //    dtAttachment.Columns.Add("FileSize", typeString);
        //    dtAttachment.Columns.Add("FileUrl", typeString);
        //    dtAttachment.Columns.Add("FileType", typeString);

        //}

        //count++;
        //DataRow dr;

        //dtAttachment.Rows.Add(new Object[] { Guid.Empty, "test1", fileInfo.Name, fileInfo.Name, uploadedFile.ContentLength / 1024, fileInfo.Extension, fileInfo.Extension });
        
        _dataArray.Add(new Object[] { Guid.Empty, "test1", fileInfo.Name, fileInfo.Name, uploadedFile.ContentLength / 1024, fileInfo.Extension, fileInfo.Extension });
        //dr = dtAttachment.NewRow();
        //dr["AttachmentGuid"] = ;
        //dr["FileNumber"] = "test1";
        //dr["AttachmentTitle"] = fileInfo.Name;
        //dr["DocumentTitle"] = "doctitle";
        //dr["FileSize"] = (uploadedFile.ContentLength / 1024);
        //dr["FileUrl"] = 
        //dr["FileType"] = fileInfo.Extension;
        //dtAttachment.Rows.Add(dr);


        UploadingUtils.RemoveFileWithDelay(uploadedFile.FileName, resFileName, 5);
        string fileLabel = fileInfo.Name;
        string fileLength = uploadedFile.ContentLength / 1024 + "K";

        fuAttachment.SaveAs(Server.MapPath(Path.Combine("~/AttachedFile/", hdfldCompliantGuid.Value + "_" + fileLabel)));

        //Session["attachedFiles"] = dtAttachment;
        Session["attachedFiles"] = _dataArray;

        return string.Format("{0} ({1})|{2}", fileLabel, fileLength, fileInfo.Name);


    }
    protected void ASPxCallbackPanel1_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {

    }
    private void ShowSuccess(string strMsg)
    {
        cbData.JSProperties["cpMessage"] = strMsg;
        cbData.JSProperties["cpStatus"] = "SUCCESS";
    }
    protected void ClearForm()
    {

        //txtFullName.Text = string.Empty;
        txtTelephone.Text = string.Empty;
        //cboOrganization.Text = string.Empty;
        txtEmail.Text = string.Empty;
        //cboComplainType.Text = string.Empty;
        txtCompliantReason.Text = string.Empty;
        txtComplaintRemark.Text = string.Empty;
        //txtLogo.Text = string.Empty;
        LoadComplianTypes();
    }
    protected bool DoNew()
    {

        try
        {            
            Session["IsNew"] = true;
            ClearForm();
            cbData.JSProperties["cpAction"] = "new";
            cbData.JSProperties["cpStatus"] = "SUCCESS";
            cbData.JSProperties["cpMessage"] = string.Empty;
            return true;
        }
        catch (Exception ex)
        {
            ShowError(ex.Message);
            return false;
        }
    }
    protected void CallbackPanel_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {
        string strID = string.Empty;
        string[] parameters = e.Parameter.Split('|');
        string strParam = parameters[0].ToString();
        if (e.Parameter.ToString().Contains('|'))
            strID = parameters[1].ToString();

        switch (strParam)
        {
            case "New":
                DoNew();
                break;
            case "Edit":
                DoFind(strID);
                break;
            case "Search":
                //DoSearch();
                break;
            case "Save":
                DoSave();
                break;
            case "Delete":
                if (strID.Length > 0)
                    Session["ID"] = new Guid(strID);
                DoDelete();
                break;
            case "Refresh":
                //BindGrid();
                //cbData.JSProperties["cpStatus"] = "REFRESH";
                //cbData.JSProperties["cpAction"] = "refresh";
                break;
            default:
                break;

        }
    }

    protected bool DoFind(string strID)
    {
        cbData.JSProperties["cpAction"] = "new";
        cbData.JSProperties["cpStatus"] = "SUCCESS";
        cbData.JSProperties["cpMessage"] = string.Empty;
        Guid ID = Guid.Parse(strID);
        ComplaintBusiness objcomplian = new ComplaintBusiness();

        Complaint complaintInformation = new Complaint();

        try
        {
            LoadComplianTypes();
            complaintInformation = objcomplian.GettblComplaint(ID);
            if (objcomplian == null)
            {
                ShowMessage("No record was found.");
                return false;
            }
            //Now Record was found;
            Session["IsNew"] = false;
            ClearForm();
            pnlData.Visible = true;
            this.hdfldCompliantGuid.Value = complaintInformation.ComplaintGUID.ToString();
            this.txtFullName.Text = complaintInformation.ReportedBy.ToString();
            this.txtEmail.Text = complaintInformation.ReporterEmail.ToString();
            this.txtTelephone.Text = complaintInformation.ReporterTel;
            this.cboComplainType.Text = complaintInformation.ScreeningDecision.ToString();
            this.txtComplaintRemark.Text = complaintInformation.ComplaintRemark;
            this.txtCompliantReason.Text = complaintInformation.ComplaintDescription;
            this.cboComplainType.Text = complaintInformation.ScreeningDecision.ToString();

            //this.txtComplaintDescription.Text = complaintInformation.ComplaintDescription.ToString();
            //this.txtDateReported.Text = complaintInformation.DateReported.ToString();
            //this.txtDateScreened.Text = complaintInformation.DateScreened.ToString();
            //this.txtScreenedBy.Text = complaintInformation.ScreenedBy;
            //this.txtScreenedBy.Text = complaintInformation.ScreenedBy.ToString();           
            //this.txtFindingSummary.Text = complaintInformation.FindingSummary.ToString();
            //this.txtFindingSummaryDecision.Text = complaintInformation.FindingSummaryDecision;
            //this.txtFindingSummaryDecision.Text = complaintInformation.FindingSummaryDecision.ToString();

            Session["ID"] = ID;
            return true;
        }
        catch (Exception ex)
        {
            ShowError(ex.Message);
            return false;
        }

    }

    protected bool DoDelete()
    {
        cbData.JSProperties["cpAction"] = "new";
        cbData.JSProperties["cpStatus"] = "SUCCESS";
        cbData.JSProperties["cpMessage"] = string.Empty;
        ComplaintBusiness compliant = new ComplaintBusiness(); ;
        ComplaintBusiness objComplaintBusiness = new ComplaintBusiness();
        try
        {
            //Session["ID"] = DvgrdregEdit.GetRowValues(Convert.ToInt16(i.ToString()), DvgrdregEdit.KeyFieldName).ToString();
            objComplaintBusiness.Delete(Convert.ToString(Session["ID"].ToString()));
            Session["IsNew"] = false;
                       
            BindGrid();
            ClearForm();

            cbData.JSProperties["cpAction"] = "Delete";
            ShowSuccess("Record was deleted successfully.");             
                       
            //ShowMessage("Record was deleted successfully.");
            return true;
        }
        catch (Exception ex)
        {
            ShowError(ex.Message);
            return false;
        }
    }
    private void ShowMessage(string strMsg)
    {
        cbData.JSProperties["cpMessage"] = strMsg;
        cbData.JSProperties["cpStatus"] = "INFO";
    }
    private void BindGrid()
    {
        try
        {
            ComplaintBusiness objcompliantInformation = new ComplaintBusiness();
            ProfileCommon p = this.Profile;
            p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
            DvgrdregEdit.DataSource = objcompliantInformation.GetComplaintEmployee(Guid.Parse(Session["PesrsonGuid"].ToString()),false,p.Organization.LanguageID); ;
            DvgrdregEdit.DataBind();

        }
        catch { }
    }
    protected void DvgrdregEdit_PageIndexChanged(object sender, EventArgs e)
    {
        BindGrid();
    }
}