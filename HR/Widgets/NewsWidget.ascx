﻿<%@ Control Language="C#" ClassName="NewsWidget" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<script runat="server"> 
    protected void Page_Init(object sender, EventArgs e) {
        ASPxNewsControl.ItemSettings.DateVerticalPosition = DateVerticalPosition.BelowHeader;
        ASPxNewsControl.ItemSettings.DateHorizontalPosition = DateHorizontalPosition.Right;
        ASPxNewsControl.Paddings.Padding = 10;
        ASPxNewsControl.ContentStyle.Paddings.Padding = 0;
    }
</script>

<dx:ASPxNewsControl ID="ASPxNewsControl" runat="server" NavigateUrlFormatString=""
    Width="100%">
    <Border BorderStyle="None"/>
    <ItemSettings ImagePosition="Left" MaxLength="1000000">
      
    </ItemSettings>
   
    <Items>
            <dx:NewsItem Image-Url="Images/CivilLogo.jpg"
                HeaderText="ሲቪል ሰርቪስ ሚኒስቴር">
              <Image Url="~/Images/CivilLogo.jpg"></Image>
        </dx:NewsItem>
        <dx:NewsItem 
            HeaderText="የቅሬታ አቀራረብ ሥርዓት "
Text="
1. ቅሬታ ያጋጠመዉ ዜጋ ቅሬታዉን በቀጥታ አገልግሎት ለሰጠዉ ፈጻሚ  በቃል፣ በጽሁፍ፣ በስልክ፣ በፋክስ እና  በኢሜይል ማቅረብ  ይችላል፣.
2. ቅሬታዉ የቀረበለት ፈጻሚም የቀረበለትን ቅሬታ አጣርቶ ወዲያዉኑ ለቅሬታ አቅራቢዉ ተገቢዉን ምላሽ ይሰጣል፣
3. በተሰጠዉ ምላሽ ያልረካ ዜጋ ቀጥሎ ላለዉ የሥራ ኃላፊ ቅሬታዉን ማቅረብ ይችላል፣
4. ቅሬታ የቀረበለት የሥራ ኃላፊም ቅሬታዉን አጣርቶ ወዲዉኑ ምላሽ መስጠት አለበት፣
5. በኃላፊው በተሠጠው ምላሽ ያልረካ ዜጋ ለተቋሙ  የሥነ-ምግባርና አቤቱታ መኰንን ቅሬታውን ያቀርባል፡፡  መኮንኑም ሁኔታዉን  ወዲያዉኑ  አጣርቶ ምላሽ ይሰጣል፣
6. በመኰንኑ ምላሽ ያልረካ ዜጋ ጉዳዩን ለተቋሙ የበላይ ኃላፊ ማቅረብ ይችላል፣
7. የበላይ ኃላፊዉም ቅሬታዉን እንደአስፈላጊነቱ እንዲጣራ በማድረግ ለቅሬታ አቅራቢዉ መልስ ይሰጣል፣  
8. v.	በመጨረሻም ከላይ የተጠቀሡትን ደረጃዎች ተከትሎ ዜጋው አሁንም በተሰጠዉ ዉሳኔ ካልረካ እንደ አግባቡ ለሚመለከተዉ ፍርድ ቤት እና ለሕዝብ እንባ ጠባቂ፣ ለሰብአዊ መብት፣ለሥነምግባርና ጸረ-ሙስና ኮሚሽኖች እንዲሁም ለሚዲያ ማሳወቅ ይችላል ፡፡">

        </dx:NewsItem>
    
    </Items>
</dx:ASPxNewsControl>
