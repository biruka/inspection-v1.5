﻿$(document).ready(function () {

    var cbo = $("select[name$=cboCustomerType]");
    cbo.bind('change', cboChange);

    function cboChange() {

        if (cbo.val() !== 0) {
            DoToggle(cbo.val());
        }
    };



});

function DoToggle(customer) {
    if (customer != undefined) {

        if (customer == "0") {

            $("#<%=txtFatherName.ClientID %>").show();
            $("#<%=lblFatherName.ClientID %>").show();
            $("#<%=lblFatherNameR.ClientID %>").show();
            $("#<%=txtGrandName.ClientID %>").show();
            $("#<%=lblGrand.ClientID %>").show();
            $("#<%=lblGrandR.ClientID %>").show();
            //ValidatorEnable($("#<%=FatherR.ClientID %>")[0], true);//Enable Validator

        }
        else {

            $("#<%=txtFatherName.ClientID %>").hide();
            $("#<%=lblFatherName.ClientID %>").hide();
            $("#<%=lblFatherNameR.ClientID %>").hide();
            $("#<%=txtGrandName.ClientID %>").hide();
            $("#<%=lblGrand.ClientID %>").hide();
            $("#<%=lblGrandR.ClientID %>").hide();
            //ValidatorEnable($("#<%=FatherR.ClientID %>")[0], false);//Disable Validator


        }
    }
}
