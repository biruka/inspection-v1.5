﻿ 

    $(function () {

        //Set controls instances
        var cboRegion = $("select[name$=cboRegion]");
        var cboZone = $("select[name$=cboZone]");
        var cboWereda = $("select[name$=cboWereda]");
        var cboKebele = $("select[name$=cboKebele]");

        var ZoneUrl = 'Service/Address.asmx/GetZones';
        var WeredaUrl = 'Service/Address.asmx/GetWeredas';
        var KebeleUrl = 'Service/Address.asmx/GetKebeles';


        //EVENTS
        cboRegion.bind('change', cboRegionChange);
        cboZone.bind('change', cboZoneChange);
        cboWereda.bind('change', cboWeredaChange);
        //            cboKebele.bind('change', cboKebeleChange);

        //Events handlers
        function cboRegionChange() {
            if (cboRegion.val() != 0) {
                doAjaxCall(ZoneUrl, '{RegionCode: ' + cboRegion.val() + '}', ZoneSuccessHandler);
            }
        }

        function cboZoneChange() {
            if (cboZone.val() != 0) {
                doAjaxCall(WeredaUrl, '{ZoneCode: ' + cboZone.val() + '}', WeredaSuccessHandler);
            }
        }

        function cboWeredaChange() {
            if (cboWereda.val() != 0) {
                doAjaxCall(KebeleUrl, '{WeredaCode: ' + cboWereda.val() + '}', KebeleSuccessHandler);
            }
        }

        //Disabled  initially
        cboZone.attr('disabled', 'disabled');
        cboWereda.attr('disabled', 'disabled');
        cboKebele.attr('disabled', 'disabled');


        function doAjaxCall(url, data, successHandler) {
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    successHandler(response);
                }
            });
        }

        function ZoneSuccessHandler(response) {
            var zones = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            //Remove all the options from all the child dropdowns
            cboZone.find('option').remove();
            cboWereda.find('option').remove();
            cboKebele.find('option').remove();

            //Append default option 
            cboZone.attr('disabled', false).append($('<option></option>').
                            attr('value', 0).text('--Please Select--'));
            var doc = $('<div></div>');
            for (var i = 0; i < zones.length; i++) {
                doc.append($('<option></option>').
                            attr('value', zones[i].ZoneCode + '').text(zones[i].ZoneName)
                            );
            }
            cboZone.append(doc.html());
            doc.remove();


        }

        function WeredaSuccessHandler(response) {
            var weredas = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            cboWereda.find('option').remove();
            cboKebele.find('option').remove();

            //Append default option 
            cboWereda.attr('disabled', false).append($('<option></option>').
                            attr('value', 0).text('--Please Select--'));
            var doc = $('<div></div>');
            for (var i = 0; i < weredas.length; i++) {
                doc.append($('<option></option>').
                            attr('value', weredas[i].WeredaCode).text(weredas[i].WeredaName));
            }
            cboWereda.append(doc.html());
            doc.remove();


        }
        function KebeleSuccessHandler(response) {
            var kebeles = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;

            cboKebele.find('option').remove();

            //Append default option 
            cboKebele.attr('disabled', false).append($('<option></option>').
                            attr('value', 0).text('--Please Select--'));
            var doc = $('<div></div>');
            for (var i = 0; i < kebeles.length; i++) {
                doc.append($('<option></option>').
                            attr('value', kebeles[i].KebeleCode).text(kebeles[i].KebeleName));
            }
            cboKebele.append(doc.html());
            doc.remove();
        }


    });
 