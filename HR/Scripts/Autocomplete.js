﻿$(document).ready(function () {
    $.ajax({
        type: "POST",
        url: "Service/AutoUser.asmx/GetUsers",
        dataType: "json",
        data: "{'User': '" + $('.searchinput').val() + "'}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('.searchinput').autocomplete({
                minLength: 0,
                source: data.d,
                focus: function (event, ui) {
                    $('.searchinput').val(ui.item.value);
                    return false;
                },
                select: function (event, ui) {
                    $('.searchinput').val(ui.item.value);
                    //                        $('#UserHidden').text( ui.item.Comment);
                    return false;
                }
            });
        }

    });
    $.ajax({
        type: "POST",
        url: "Service/Lookup.asmx/GetLookups",
        dataType: "json",
        data: "{'SearchPhrase': '" + $('.searchTitle').val() + "'}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('.searchTitle').autocomplete({
                minLength: 1,
                source: data.d,
                focus: function (event, ui) {
                    $('.searchTitle').val(ui.item.value);
                    return false;
                },
                select: function (event, ui) {
                    $('.searchTitle').val(ui.item.value);
                    //                        $('#UserHidden').text( ui.item.Comment);
                    return false;
                }
            });
        }

    });
 
});