﻿
function ShowAlert(type,msg) {
     noty({
        text: msg,
        type: type,
        dismissQueue: true,
        killer: true,
        closeWith: ['click', 'button'],
        modal: false,
        layout: 'topRight',
        theme: 'defaultTheme',
        timeout: 6500
    });

}



 
//function ShowWarning(msg) {  noty({ text: msg, type: 'warning', dismissQueue: true, killer: true, closeWith: ['click', 'button'], layout: 'topRight', theme: 'defaultTheme', timeout: 4000 }); }

function ShowConfirm(layout) {
     noty({
        text: 'Are you sure?',
        type: 'warning',
        dismissQueue: true,
        killer: true,
        layout: layout,
        theme: 'defaultTheme',
        animation: {
            open: { height: 'toggle' },
            close: { height: 'toggle' },
            easing: 'swing',
            speed: 1 
        },
        buttons: [
            {
                addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                    $noty.close();
                    ClosePopup(true);
                }
            },
            {
                addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                    $noty.close();
                    ClosePopup(false);
                }
            }
        ]
       
    });
}