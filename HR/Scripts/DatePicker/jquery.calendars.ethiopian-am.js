﻿
(function($) {
	$.calendars.calendars.ethiopian.prototype.regionalOptions['am'] = {
		name: 'የኢትዮጵያ ዘመን አቆጣጠር',
		epochs: ['BEE', 'EE'],
		monthNames: ['መስከረም', 'ጥቅምት', 'ኅዳር', 'ታህሣሥ', 'ጥር', 'የካቲት',
		'መጋቢት', 'ሚያዝያ', 'ግንቦት', 'ሰኔ', 'ሐምሌ', 'ነሐሴ', 'ጳጉሜ'],
		monthNamesShort: ['መስከ', 'ጥቅም', 'ኅዳር', 'ታህሣ', 'ጥር', 'የካቲ',
		'መጋቢ', 'ሚያዝ', 'ግንቦ', 'ሰኔ', 'ሐምሌ', 'ነሐሴ', 'ጳጉሜ'],
		dayNames: ['እሑድ', 'ሰኞ', 'ማክሰኞ', 'ረቡዕ', 'ሓሙስ', 'ዓርብ', 'ቅዳሜ'],
		dayNamesShort: ['እሑድ', 'ሰኞ', 'ማክሰ', 'ረቡዕ', 'ሓሙስ', 'ዓርብ', 'ቅዳሜ'],
		dayNamesMin: ['እሑ', 'ሰኞ', 'ማክ', 'ረቡ', 'ሐሙ', 'ዓር', 'ቅዳ'],
		dateFormat: 'dd/mm/yyyy',
		firstDay: 0,
		isRTL: false
	};
	$.calendars.calendars.ethiopian.prototype.regionalOptions['or'] = {
	    name: 'Ethiopian Calendar',
	    epochs: ['BEE', 'EE'],
	    monthNames: ['Fulbaana', 'Onkoloolessa', 'Sadaasa', 'Mudde', 'Amajji', 'Guraandhala',
		'Bitootessa', 'Ebila', 'Caamsaa', 'Waxabajjii', 'Adoolessa', 'Hagayya', 'Qaamee'],
	    monthNamesShort: ['Ful', 'Onk', 'Sad', 'Mud', 'Amaj', 'Gur',
		'Bit', 'Ebi', 'Caam', 'Wax', 'Ado', 'Hag', 'Qam'],
	    dayNames: ['Dilbata', 'Wixata', 'Kibxata', 'Roobii', 'Kamisa', 'Jimaata', 'Sanbata'],
	    dayNamesShort: ['Dilb', 'Wix', 'Kib', 'Rob', 'Kam', 'Jim', 'San'],
	    dayNamesMin: ['Di', 'Wi', 'Ki', 'Ro', 'Ka', 'Ji', 'Sa'],
	    dateFormat: 'dd/mm/yyyy',
	    firstDay: 0,
	    isRTL: false
	};
}


)(jQuery);
