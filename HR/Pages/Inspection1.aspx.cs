﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using CUSTOR.Bussiness;
using CUSTOR.Domain;
using CUSTOR;
using CUSTOR.Commen;
using System.IO;
using System.Threading;
using System.Drawing;
using System.Data;
using System.Security.Principal;
using System.Text;
using System.Configuration;
using System.Resources;
using DevExpress.Web.ASPxHtmlEditor;

namespace UI.Pages
{
    public partial class Inspection1 : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect("~/Login.aspx");
            }
            else
            {
                pnlMsg.Visible = false;
                try
                {
                    if (!IsCallback && !IsPostBack)
                    {
                        if (Session["ProLanguage"] != null)
                        {
                            HidlLanguage.Value = Convert.ToString(Session["ProLanguage"].ToString());
                        }

                        if (Session["UserID"] != null)
                        {
                            HidUserid.Value = Session["UserID"].ToString();
                            LoadOrganizations(Guid.Parse(Session["ProfileOrgGuid"].ToString()),
                            Session["ProfileOrgCatagory"].ToString(), int.Parse(HidlLanguage.Value));
                            LoadOHRActivityType(int.Parse(HidlLanguage.Value));
                        }
                        Session["IsNew"] = true;
                        pnlMsg.Visible = false;
                        dpFrom.SelectedDate =
                            Convert.ToString(
                                (EthiopicDateTime.GetEthiopicDate(int.Parse(DateTime.Today.Day.ToString()),
                                    int.Parse(DateTime.Today.Month.ToString()), int.Parse(DateTime.Today.Year.ToString()))));
                        dpTo.SelectedDate =
                            Convert.ToString(
                                (EthiopicDateTime.GetEthiopicDate(int.Parse(DateTime.Today.Day.ToString()),
                                    int.Parse(DateTime.Today.Month.ToString()), int.Parse(DateTime.Today.Year.ToString()))));
                        //Secure(Session["DocumentType"].ToString());
                        Session["APersonGuid"] = Guid.Empty;
                        Session["DocumentType"] = 0;
                    }

                    else //If   callback
                    {
                        //Secure(Session["DocumentType"].ToString());
                    }
                    //if (Convert.ToBoolean(Session["btnclk"]) == true)
                    //    Showpersoninfo();
                    pnlMsg.Visible = false;
                    lblPersonMsg.Text = "";
                    //--------------------------2016-------------------
                    //if (cbInspection.IsCallback || Page.IsCallback)
                    //{
                    //    ArrayList selectedChecklistItems = new ArrayList();
                    //    if (ChklstApprove.Items.Count > 0)
                    //    {
                    //        for (int index = 1; index <= ChklstApprove.Items.Count; index++)
                    //        {
                    //            if (ChklstApprove.Items[index].Selected)
                    //            {
                    //                selectedChecklistItems.Add(ChklstApprove.Items[index].Value);
                    //            }
                    //        }

                    //    }
                    //    LoadInspectionCheckList(selectedChecklistItems);
                    //Session["IsNewUnit"] = false;

                    //}

                    //-----------------------------------------         
                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void LoadOHRActivityType(int OrganizationLanguage)
        {
            tblDocumentTypeBussiness objHRtype = new tblDocumentTypeBussiness();
            cboHRActivity.ValueField = "Code";
            cboHRActivity.TextField = "DescriptionAm";
            cboHRActivity.DataSource = objHRtype.GetDocTypeList(OrganizationLanguage);
            cboHRActivity.DataBind();
            cboHRActivity.Value = null;
        }

        protected void LoadOrganizations(Guid OrganizationGuid, string OrganizationCatagory, int OrganizationLanguage)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                ProfileCommon objProfile = this.Profile;
                objProfile = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
                Session["OrgLanguage"] = objProfile.Organization.Language;
                if (HttpContext.Current.User.IsInRole("Inspection Director") ||
                    HttpContext.Current.User.IsInRole("Inspection Supervisor") ||
                    HttpContext.Current.User.IsInRole("Inspection officer"))
                {
                    //Load all list of Organizations
                    List<Organization> objorg = new List<Organization>();
                    OrganizationBussiness objorgBusiness = new OrganizationBussiness();
                    objorg = objorgBusiness.GetActiveOrganizationsForInspection(Guid.Parse(objProfile.Organization.GUID));
                    if (objorg.Count > 0)
                    {
                        ComboOrg.ValueField = "OrgGuid";
                        ComboOrg.TextField = "DescriptionAm";
                        ComboOrg.DataSource = objorg;
                        ComboOrg.DataBind();
                        ComboOrg.Value = null;
                    }
                }

                else if (CAccessRight.CanAccess(CResource.eResource.ehrofficer))
                {
                    List<Organization> objorg = new List<Organization>();
                    OrganizationBussiness objorgBusiness = new OrganizationBussiness();
                    objorg = objorgBusiness.GetListOrg(Guid.Parse(objProfile.Organization.GUID));
                    if (objorg.Count > 0)
                    {
                        ComboOrg.ValueField = "OrgGuid";
                        ComboOrg.TextField = "DescriptionAm";
                        ComboOrg.DataSource = objorg;
                        ComboOrg.DataBind();
                        ComboOrg.Value = null;
                    }
                }
                else
                {
                    InspectionFollowupBussiness objOrganizations = new InspectionFollowupBussiness();
                    DataTable dt = objOrganizations.getEmployeeInspectedOrganizations(Guid.Parse(objProfile.Staff.GUID));
                    //check whethere the logged in user can view approve button
                    Session["loggedInUser"] = Guid.Parse(objProfile.Staff.GUID).ToString();

                    ComboOrg.ValueField = "OrgGuid";
                    ComboOrg.TextField = "DescriptionAm";
                    ComboOrg.DataSource = dt;
                    ComboOrg.DataBind();
                    ComboOrg.Value = null;
                }
            }
        }

        private void FillCombo(ASPxComboBox cboName, Language.eLanguage eLang, Type t)
        {

            cboName.DataSource = EnumHelper.GetEnumDescriptions(t, eLang);
            cboName.ValueField = "Key";
            cboName.TextField = "Value";
            cboName.DataBind();
            if (HidlLanguage.Value == "0")
            {
                ListEditItem le = new ListEditItem("--Select--", "-1");
                cboName.Items.Insert(0, le);
                //cboName.Text = "-1";
            }
            else if (HidlLanguage.Value == "1")
            {
                ListEditItem le = new ListEditItem("ይምረጡ", "-1");
                cboName.Items.Insert(0, le);
                //cboName.Text = "-1";
            }
            else if (HidlLanguage.Value == "2")
            {
                ListEditItem le = new ListEditItem("--mret--", "-1");
                cboName.Items.Insert(0, le);

                //cboName.Text = "-1";
            }


        }

        private bool checkdate()
        {
            try
            {
                DateTime dtfrom = dpFrom.SelectedGCDate;
                DateTime dtto = dpTo.SelectedGCDate;
                Session["dtfrom"] = dtfrom;
                Session["dtto"] = dtto;
                if (dtfrom != new DateTime())
                {
                    if (dtto != new DateTime())
                    {
                        if (dtfrom.CompareTo(dtto) > 0)
                        {
                            CMsgBar.ShowError(Resources.Message.MSG_VRDATE, pnlMsg, lblPersonMsg);
                            return false;
                        }
                        else if ((dtfrom == new DateTime()) || (dtto == new DateTime()))
                        {
                            CMsgBar.ShowError(Resources.Message.MSG_VDATE, pnlMsg, lblPersonMsg);
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                CMsgBar.ShowError(Resources.Message.MSG_VDATE, pnlMsg, lblPersonMsg);
                return false;
            }
        }

        private bool orgcombochk()
        {
            try
            {
                if (!CAccessRight.CanAccess(CResource.eResource.ehrofficer))
                {
                    if (ComboOrg.Value.ToString() == "-1")
                    {
                        CMsgBar.ShowError(Resources.Message.MSG_SELECTORGANIZATION, pnlMsg, lblPersonMsg);
                        return false;
                    }
                    else
                    {
                        Session["OrgGuid"] = (string)ComboOrg.SelectedItem.Value;
                        return true;
                    }

                }
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        private void Showpersoninfo()
        {
            DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e;
            //int visibleIndex = Convert.ToInt32(e.Parameters.ToString());
            try
            {
                ProfileCommon p = this.Profile;
                p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
                //ASPxPanel1.Visible = true;
                Session["FileType"] = (string)cboHRActivity.SelectedItem.Value;
                Session["fromdate"] = Session["dtfrom"];
                Session["todate"] = Session["dtto"];
                Session["OrgGuid"] = Guid.Parse(ComboOrg.Value.ToString());
                bool isHRSupervisorOrDirector = false;
                if (HttpContext.Current.User.IsInRole("HR Supervisor"))
                {
                    isHRSupervisorOrDirector = true;
                }
                PersonEmploymentEntityViewBussiness personList = new PersonEmploymentEntityViewBussiness();
                DataTable personListDataTable = personList.GetRecords(Guid.Parse(ComboOrg.Value.ToString()),
                                                                        Convert.ToInt32(cboHRActivity.Value.ToString()),
                                                                        dpFrom.SelectedGCDate, dpTo.SelectedGCDate, p.Organization.LanguageID.ToString(),
                                                                        isHRSupervisorOrDirector);
                DvgvwPerson.DataSource = personListDataTable;
                DvgvwPerson.PageIndex = 0;
                DvgvwPerson.DataBind();
                if (Session["FileType"].ToString() == "311")
                {
                    DvgvwPerson.Columns[3].Caption = "የቅጥር አይነት";
                }
                else if (Session["FileType"].ToString() == "312")
                {
                    DvgvwPerson.Columns[3].Caption = "የዕድገት አይነት";
                }
                else if (Session["FileType"].ToString() == "313")
                {
                    DvgvwPerson.Columns[3].Caption = "የዝውውር አይነት";
                }
                else if (Session["FileType"].ToString() == "315")
                {
                    DvgvwPerson.Columns[3].Caption = "የስንብት አይነት";
                }
            }

            catch (Exception ex)
            {
                CMsgBar.ShowError(Resources.Message.MSG_SELECTHRACTIVITY, pnlMsg, lblPersonMsg);
            }
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            //Session["btnclk"] = true;
            Showpersoninfo();
            //cure();
        }

        protected void pageControl_ActiveTabChanged(object source, TabControlEventArgs e)
        {
            //int j = int.Parse(Session["j"].ToString());
            //ASPxPageControl pageControl = DvgvwPerson.FindDetailRowTemplateControl(j, "pageControl") as ASPxPageControl;
            //if (pageControl != null) //True when called from Page_Load
            //{
            //if (pageControl.TabPages[17].Visible)
            //{
            //Inspcontrol ins = pageControl.FindControl("Inspcontrol1") as Inspcontrol;
            //if (ins != null)
            //{

            //}
            //}

            //}
        }

        protected string GetMemoText()
        {
            try
            {
                int j = Convert.ToInt16(Session["j"].ToString());
                ASPxPageControl pageControl = DvgvwPerson.FindDetailRowTemplateControl(j, "pageControl") as ASPxPageControl;
                if (pageControl != null) //True when called from Page_Load
                {
                    ASPxGridView Gridinsp = (ASPxGridView)pageControl.TabPages[15].FindControl("DvgvInspection");
                    if (Gridinsp != null)
                    {
                        ASPxMemo memo = Gridinsp.FindEditFormTemplateControl("Memo") as ASPxMemo;
                        return memo.Text;
                    }
                }
                return "";
            }
            catch
            {
                return "";
            }

        }

        protected void CombDocType_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlMsg.Visible = false;
            lblPersonMsg.Text = "";
        }

        protected bool Save()
        {
            tblInspection objInspection = new tblInspection();
            InspectionBussiness objInspectionBusiness = new InspectionBussiness();
            try
            {
                if ((bool)Session["IsNew"])
                {
                    objInspectionBusiness.InsertInspection(objInspection);
                }
                else
                {
                    objInspectionBusiness.UpdateInspection(objInspection);
                }
            }
            catch (Exception ex)
            {
                //Master.ShowMessage(new RMessage("Error:" + ex.Message, RMessageType.Error));
                return false;
            }
            return true;
        }

        protected void DvgvwPerson_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
        {
            ProfileCommon p = this.Profile;
            p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
            if (e.Column.FieldName == "inspectstatus")
                if ((e.Value).ToString().Length != 0)
                {
                    if (e.Value.ToString() == "yes")
                    {
                        e.DisplayText = Resources.Message.MSG_EVALUATE;
                    }
                    else
                    {
                        e.DisplayText = Resources.Message.MSG_NOEVALUATE;
                    }
                }
            if (e.Column.FieldName == "birth_date")
                if ((e.Value).ToString().Length != 0)
                {
                    string ethiodate;
                    ethiodate = EthiopicDateTime.GetEthiopicDate(((DateTime)(e.Value)).Day, ((DateTime)(e.Value)).Month, ((DateTime)(e.Value)).Year);
                    e.DisplayText = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate, Convert.ToInt32(p.Organization.LanguageID));
                }
            if (e.Column.FieldName == "Gender")
            {
                if (p.Organization.LanguageID == 1)
                {
                    e.DisplayText = EnumHelper.GetEnumDescription(typeof(Enumeration.sex),
                        Language.eLanguage.eAmharic, Convert.ToInt32(e.Value));
                }
                if (p.Organization.LanguageID == 2)
                {
                    e.DisplayText = EnumHelper.GetEnumDescription(typeof(Enumeration.sex),
                        Language.eLanguage.eAfanOromo, Convert.ToInt32(e.Value));
                }
            }

            if (e.Column.FieldName == "emp_date")
            {
                if ((e.Value).ToString().Length != 0)
                {
                    string ethiodate;
                    ethiodate = EthiopicDateTime.GetEthiopicDate(((DateTime)(e.Value)).Day, ((DateTime)(e.Value)).Month, ((DateTime)(e.Value)).Year);
                    e.DisplayText = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate, p.Organization.LanguageID);
                }
                else
                {
                    { return; }
                }
            }
            if (e.Column.FieldName == "InspectionDecision")
            {
                int test = 0;
                if (int.TryParse((e.Value).ToString(), out test))
                {

                    ArrayList list = new ArrayList();
                    if (p.Organization.LanguageID == 1)
                    {
                        e.DisplayText = EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionDecisionType), Language.eLanguage.eAmharic, Convert.ToInt32(e.Value));
                    }
                    if (p.Organization.LanguageID == 2)
                    {
                        e.DisplayText = EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionDecisionType), Language.eLanguage.eAfanOromo, Convert.ToInt32(e.Value));
                    }
                }
            }
        }

        //protected bool DoFindAttachment(Guid MainGuid)
        //{
        //    try
        //    {
        //        Attachment objAttachment = new Attachment();
        //        AttachmentBussiness objAttachmentBusiness = new AttachmentBussiness();
        //        objAttachment = objAttachmentBusiness.GetAttachment(MainGuid);
        //        string url = objAttachment.Url;
        //        string NavigateUrl = @"C:\inetpub\wwwroot\hr\Attachment\" + url; //this is Acctual path where the HR Attachment is exist;
        //        FileInfo fi = new FileInfo(NavigateUrl);
        //        System.Diagnostics.Process.Start(NavigateUrl);
        //        long sz = fi.Length;
        //        FileStream MyFileStream = new FileStream(NavigateUrl, FileMode.Open);
        //        sz = MyFileStream.Length;
        //        byte[] Buffer = new byte[(int)sz];
        //        MyFileStream.Read(Buffer, 0, (int)MyFileStream.Length);
        //        MyFileStream.Close();
        //        Response.ContentType = MimeType(Path.GetExtension(NavigateUrl));
        //        Response.AddHeader("Content-Disposition", string.Format("attachment; filename = {0}", System.IO.Path.GetFileName(NavigateUrl)));
        //        Response.AddHeader("Content-Length", sz.ToString("F0"));
        //        Response.BinaryWrite(Buffer);
        //        // string fName = NavigateUrl;
        //        FileInfo fi = new FileInfo(fName);
        //        long sz = fi.Length;
        //        string filePath = Path.Combine(fName);
        //        Response.ClearContent();
        //        Response.ContentType = MimeType(Path.GetExtension(fName));
        //        Response.AddHeader("Content-Disposition", string.Format("attachment; filename = {0}", System.IO.Path.GetFileName(fName)));
        //        Response.AddHeader("Content-Length", sz.ToString("F0"));
        //        Response.TransmitFile(fName);
        //        Response.Flush();
        //        Response.SuppressContent = true;
        //        FileStream fileStream = new FileStream(NavigateUrl, FileMode.Open, FileAccess.Read);

        //        Response.Ap = true;
        //        Response.End();
        //    }
        //    catch
        //    {

        //    }                               

        //    return true;
        //}

        protected void ChklstApprove_DataBinding(object sender, EventArgs e)
        {


        }

        private void ShowMessage(string strMsg)
        {

            //MessageBox1.ShowInfo(strMsg);
            CMsgBar.ShowInfo(strMsg, pnlMsg, lblPersonMsg);
        }

        enum serviceType
        {
            recruitment = 311,
            promotion,
            Transfer,
            serviceextension,
            servicetermination,
            disciplinary,
            health,
            overtime,
            scholarship,
            Authoritydeligation,
            Salary = 341
        }

        protected void DvgvwPerson_DetailRowGetButtonVisibility(object sender, ASPxGridViewDetailRowButtonEventArgs e)
        {
            //PersonEmploymentEntityViewBussiness objperson = new PersonEmploymentEntityViewBussiness();
            //PersonEmploymentEntityView currentUser = objperson.((DvgvwPerson.GetRowValues(e.VisibleIndex, "ID"));


            //if (!currentUser.HasProjects())
            //    e.ButtonState = GridViewDetailRowButtonState.Hidden;
        }

        protected void DvgvwPerson_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            Guid strID = Guid.Empty;
            string[] parameters = e.Parameters.Split('|');
            string strParam = parameters[0].ToString();
            switch (strParam)
            {
                case "Search":
                    Showpersoninfo();
                    break;
            }
        }

        protected void DvgvwPerson_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != GridViewRowType.Data) return;
            string status = Convert.ToString(e.GetValue("inspectstatus"));
            if (status == "no")
            {
                //e.Row.BackColor = Color.LightCyan; // Changes The BackColor of ENTIRE ROW
                //e.Row.ForeColor = Color.DarkRed; // Change the Font Color of ENTIRE ROW
            }
            else
            {

            }

        }

        protected void DvgvwPerson_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != GridViewRowType.Data) return;
            Session["DescisionReached"] = false;
            string status = Convert.ToString(e.GetValue("inspectstatus"));
            string statusdecision = Convert.ToString(e.GetValue("InspectionDecision"));
            bool Approved = Convert.ToBoolean(e.GetValue("Approved")); ;
            if (status == "no")
            {
                e.Row.BackColor = Color.LightCyan; // Changes The BackColor of ENTIRE ROW
                e.Row.ForeColor = Color.DarkRed; // Change the Font Color of ENTIRE ROW
                Session["DescisionReached"] = false;
            }
            if (Convert.ToBoolean(e.GetValue("Approved")) == true)
            {
                ProfileCommon p = this.Profile;
                int language = p.Organization.LanguageID;
                if (statusdecision == EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionDecisionType), (Language.eLanguage)language, (int)Enumeration.InspectionDecisionType.INComplete).ToString() 
                    || statusdecision == EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionDecisionType), (Language.eLanguage)language, (int)Enumeration.InspectionDecisionType.Invalid).ToString())
                {
                    Session["DescisionReached"] = true;
                    e.Row.Font.Bold = true; // Changes The BackColor of ENTIRE ROW
                    e.Row.ForeColor = Color.DarkRed;
                }
                else if (statusdecision == ((int)Enumeration.FindingSummary.Accepted).ToString()||
                         statusdecision == EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionDecisionType), (Language.eLanguage)language, (int)Enumeration.InspectionDecisionType.Complete).ToString())
                {
                    Session["DescisionReached"] = true;
                    if (Approved == true)
                    {
                        e.Row.Font.Bold = true;
                    }
                }
            }
        }

        protected void DvgvwPerson_BeforePerformDataSelect(object sender, EventArgs e)
        {
            //Showpersoninfo();
        }

        protected void DvgvwPerson_PageIndexChanged(object sender, EventArgs e)
        {
            DvgvwPerson.DataBind();
        }

        protected void cbdata_OnCallback(object sender, CallbackEventArgsBase e)
        {
            Guid strID = Guid.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0].ToString();
            if (e.Parameter.ToString().Contains('|'))
                strID = Guid.Parse(parameters[1].ToString());
            switch (strParam)
            {
                //case "Search":
                //    Showpersoninfo();
                //    break;
                case "Inspect":
                    Session["j"] = parameters[2].ToString();
                    Session["APersonGuid"] = strID.ToString();
                    Session["DocumentType"] =cboHRActivity.Value;
                    Session["LoadCriterias"] = true;
                    Session["Language"] = HidlLanguage.Value;
                    Session["emp_status"] = DvgvwPerson.GetRowValues(Convert.ToInt32(Session["j"].ToString()), "emp_status").ToString();
                    Session["isApproved"] = DvgvwPerson.GetRowValues(Convert.ToInt32(Session["j"].ToString()), "Approved").ToString();
                    string selectedDocumentType = parameters[3].ToString();
                    Session["DocumentType"] = selectedDocumentType;
                    Session["NormalInspectionType"] = true;
                    //Secure(selectedDocumentType);
                    //pageControl.Visible = true; 
                    //ShowPoup();
                    string url = "~/Pages/InspectionDetail.aspx";
                    DevExpress.Web.ASPxWebControl.RedirectOnCallback(url);
                    break;
                default:
                    break;
            }
        }

        protected void DvgvwPerson_OnSelectionChanged(object sender, EventArgs e)
        {

        }

        private void ShowPoup()
        {
            cbdata.JSProperties["cpMessage"] = "ShowCriterias";
            cbdata.JSProperties["cpStatus"] = "verified";
        }

        protected void DvgvwPerson_OnPageIndexChanged(object sender, EventArgs e)
        {
            Showpersoninfo();
        }

        protected void DvgvwPerson_CustomButtonInitialize(object sender, ASPxGridViewCustomButtonEventArgs e)
        {
            if (HttpContext.Current.User.IsInRole("HR Officer") || HttpContext.Current.User.IsInRole("HR Supervisor"))
            {
                e.Text = "አሳይ";
            }
            else
            {
                string[] fields = { "Approved" };
                object values = DvgvwPerson.GetRowValues(e.VisibleIndex, fields) as object;
                bool isApproved = (bool)values;
                if (isApproved)
                {
                    if (e.Column.Index.Equals(13))
                    {
                        e.Text = "አሳይ";

                    }
                }
            }
        }
    }

    public class MyFileHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            string filePath = Path.Combine(@"d:\wwwroot\portal_daten", context.Request.QueryString["dateiname"]);

            context.Response.ContentType = "application/pdf";
            context.Response.WriteFile(filePath);
        }
    }
}