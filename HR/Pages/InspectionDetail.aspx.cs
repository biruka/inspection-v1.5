﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using CUSTOR.Bussiness;
using CUSTOR.Domain;
using CUSTOR;
using CUSTOR.Commen;
using System.IO;
using System.Threading;
using System.Drawing;
using System.Data;
using System.Security.Principal;
using System.Text;
using System.Configuration;
using System.Globalization;
using System.Resources;
////using System.Windows.Documents;
using CUSTOR.Commen;

public partial class Pages_InspectionDetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["DocumentType"] != null)
        { 
            Secure(Session["DocumentType"].ToString());
        }
    }

    enum serviceType
    {
        recruitment = 311,
        promotion,
        Transfer,
        serviceextension,
        servicetermination,
        disciplinary,
        health,
        overtime,
        scholarship,
        Authoritydeligation,
        Salary = 341
    }

    protected override void InitializeCulture()
    {
        ProfileCommon p = this.Profile;
        if (p.Organization.LanguageID != null)
        {
            String selectedLanguage = string.Empty;
            switch (p.Organization.LanguageID)
            {
                case 0: selectedLanguage = "en-US";//English
                    break;
                case 2: selectedLanguage = "am-ET";//Afan Oromo
                    break;
                case 3: selectedLanguage = "am-ET"; //Tig
                    break;
                case 4: selectedLanguage = "en-GB";//afar
                    break;
                case 5: selectedLanguage = "en-AU";//Somali
                    break;
                default: break;//Amharic
            }
            UICulture = selectedLanguage;
            Culture = selectedLanguage;

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(selectedLanguage);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(selectedLanguage);
        }
        base.InitializeCulture();
    }

    protected void Secure(string selectedDocumentType)
    {
        try
        {
            int j = (Session["j"] != null ? Convert.ToInt16(Session["j"].ToString()) : 0);
            if (!selectedDocumentType.Equals(""))
            {
                if (selectedDocumentType.Equals(Convert.ToInt32(serviceType.recruitment).ToString()))
                // check it recruitment
                {
                    if (pageControl != null) //True when called from Page_Load 
                    {
                        if (CAccessRight.CanAccess(CResource.eResource.einspectorofficer) ||
                            CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) ||
                            CAccessRight.CanAccess(CResource.eResource.einspectordirector))
                        {
                            //tabpage access not authorise;
                            for (int i = 0; i < pageControl.TabPages.Count - 12; i++)
                            {
                                pageControl.TabPages[i].Visible = true;
                            }
                            for (int i = 5; i < pageControl.TabPages.Count - 3; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[13].Visible = false;
                            pageControl.TabPages[14].Visible = true;
                            pageControl.TabPages[15].Visible = true;
                            pageControl.TabPages[16].Visible = true;
                        }
                        else if (CAccessRight.CanAccess(CResource.eResource.ehrofficer))
                        {
                            for (int i = 0; i < pageControl.TabPages.Count; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[16].Visible = true;
                        }
                    }

                }
                else if (selectedDocumentType.Equals(Convert.ToInt32(serviceType.promotion).ToString()))
                {

                    if (pageControl != null) //True when called from Page_Load
                    {
                        if (CAccessRight.CanAccess(CResource.eResource.einspectorofficer) ||
                            CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) ||
                            CAccessRight.CanAccess(CResource.eResource.einspectordirector))
                        {

                            for (int i = 0; i < pageControl.TabPages.Count - 11; i++)
                            {
                                pageControl.TabPages[i].Visible = true;
                            }
                            for (int i = 6; i < pageControl.TabPages.Count - 3; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[13].Visible = false;
                            pageControl.TabPages[14].Visible = true;
                            pageControl.TabPages[15].Visible = true;
                            pageControl.TabPages[16].Visible = true;
                            //pageControl.TabPages[17].Visible = true;
                        }
                        else if (CAccessRight.CanAccess(CResource.eResource.ehrofficer))
                        {
                            for (int i = 0; i < pageControl.TabPages.Count; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[16].Visible = true;
                        }

                    }
                }
                else if (selectedDocumentType.Equals(Convert.ToInt32(serviceType.Transfer).ToString()))
                // tab page access for transfer hr avtivity
                {

                    if (pageControl != null) //True when called from Page_Load
                    {
                        if (CAccessRight.CanAccess(CResource.eResource.einspectorofficer) ||
                            CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) ||
                            CAccessRight.CanAccess(CResource.eResource.einspectordirector))
                        {
                            for (int i = 0; i < pageControl.TabPages.Count - 11; i++)
                            {
                                pageControl.TabPages[i].Visible = true;
                            }
                            for (int i = 7; i < pageControl.TabPages.Count - 3; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[5].Visible = false;
                            pageControl.TabPages[6].Visible = true;
                            pageControl.TabPages[14].Visible = true;
                            pageControl.TabPages[15].Visible = true;
                            pageControl.TabPages[13].Visible = false;
                            pageControl.TabPages[16].Visible = true;
                        }
                        else if (CAccessRight.CanAccess(CResource.eResource.ehrofficer))
                        {
                            for (int i = 0; i < pageControl.TabPages.Count; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[16].Visible = true;
                        }
                    }
                }
                else if (selectedDocumentType.Equals(Convert.ToInt32(serviceType.serviceextension).ToString()))
                //tab page access for serviceextension hr avtivity
                {
                    if (pageControl != null) //True when called from Page_Load
                    {
                        if (CAccessRight.CanAccess(CResource.eResource.einspectorofficer) ||
                            CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) ||
                            CAccessRight.CanAccess(CResource.eResource.einspectordirector))
                        {
                            for (int i = 0; i < pageControl.TabPages.Count - 10; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            for (int i = 8; i < pageControl.TabPages.Count - 3; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[7].Visible = true;
                            pageControl.TabPages[14].Visible = true;
                            pageControl.TabPages[15].Visible = true;
                            pageControl.TabPages[13].Visible = false;
                            pageControl.TabPages[16].Visible = true;
                        }
                        else if (CAccessRight.CanAccess(CResource.eResource.ehrofficer))
                        {
                            for (int i = 0; i < pageControl.TabPages.Count; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[16].Visible = true;
                        }
                    }
                }
                else if (
                    selectedDocumentType.Equals(
                        Convert.ToInt32(serviceType.servicetermination).ToString()))
                {
                    if (pageControl != null) //True when called from Page_Load
                    {
                        if (CAccessRight.CanAccess(CResource.eResource.einspectorofficer) ||
                            CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) ||
                            CAccessRight.CanAccess(CResource.eResource.einspectordirector))
                        {

                            for (int i = 0; i < pageControl.TabPages.Count - 9; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            for (int i = 9; i < pageControl.TabPages.Count - 3; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[13].Visible = false;
                            pageControl.TabPages[8].Visible = true;
                            pageControl.TabPages[14].Visible = true;
                            pageControl.TabPages[15].Visible = true;
                            pageControl.TabPages[16].Visible = true;
                        }
                        else if (CAccessRight.CanAccess(CResource.eResource.ehrofficer))
                        {
                            for (int i = 0; i < pageControl.TabPages.Count; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[16].Visible = true;
                        }

                    }
                }
                else if (selectedDocumentType.Equals(Convert.ToInt32(serviceType.disciplinary).ToString()))
                {
                    if (pageControl != null) //True when called from Page_Load
                    {
                        if (CAccessRight.CanAccess(CResource.eResource.einspectorofficer) ||
                            CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) ||
                            CAccessRight.CanAccess(CResource.eResource.einspectordirector))
                        {
                            for (int i = 0; i < pageControl.TabPages.Count - 8; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            for (int i = 10; i < pageControl.TabPages.Count - 3; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[13].Visible = false;
                            pageControl.TabPages[9].Visible = true;
                            pageControl.TabPages[14].Visible = true;
                            pageControl.TabPages[15].Visible = true;
                            pageControl.TabPages[16].Visible = true;
                        }
                        else if (CAccessRight.CanAccess(CResource.eResource.ehrofficer))
                        {
                            for (int i = 0; i < pageControl.TabPages.Count; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[16].Visible = true;
                        }


                    }
                }
                else if (selectedDocumentType.Equals(Convert.ToInt32(serviceType.health).ToString()))
                {
                    if (pageControl != null) //True when called from Page_Load
                    {
                        if (CAccessRight.CanAccess(CResource.eResource.einspectorofficer) ||
                            CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) ||
                            CAccessRight.CanAccess(CResource.eResource.einspectordirector))
                        {
                            for (int i = 0; i < pageControl.TabPages.Count - 7; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            for (int i = 10; i < pageControl.TabPages.Count - 3; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[13].Visible = false;
                            pageControl.TabPages[10].Visible = true;
                            pageControl.TabPages[14].Visible = true;
                            pageControl.TabPages[15].Visible = true;
                            pageControl.TabPages[16].Visible = true;
                        }
                        else if (CAccessRight.CanAccess(CResource.eResource.ehrofficer))
                        {
                            for (int i = 0; i < pageControl.TabPages.Count; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[16].Visible = true;
                        }


                    }
                }
                else if (
                    selectedDocumentType.Equals(
                        Convert.ToInt32(serviceType.overtime).ToString()))
                {
                    if (pageControl != null) //True when called from Page_Load
                    {
                        if (CAccessRight.CanAccess(CResource.eResource.einspectorofficer) ||
                            CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) ||
                            CAccessRight.CanAccess(CResource.eResource.einspectordirector))
                        {
                            for (int i = 0; i < pageControl.TabPages.Count - 3; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[13].Visible = false;
                            pageControl.TabPages[14].Visible = true;
                            pageControl.TabPages[15].Visible = true;
                            pageControl.TabPages[16].Visible = true;
                        }
                        else if (CAccessRight.CanAccess(CResource.eResource.ehrofficer))
                        {
                            for (int i = 0; i < pageControl.TabPages.Count; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[16].Visible = true;
                        }

                    }
                }
                else if (
                    selectedDocumentType.Equals(
                        Convert.ToInt32(serviceType.scholarship).ToString()))
                {
                    if (pageControl != null) //True when called from Page_Load
                    {
                        if (
                            CAccessRight.CanAccess(CResource.eResource.einspectorofficer) ||
                            CAccessRight.CanAccess(
                                CResource.eResource.einspectorsupervisor) ||
                            CAccessRight.CanAccess(
                                CResource.eResource.einspectordirector))
                        {
                            for (int i = 0; i < pageControl.TabPages.Count - 3; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[13].Visible = false;
                            pageControl.TabPages[14].Visible = true;
                            pageControl.TabPages[15].Visible = false;
                            pageControl.TabPages[16].Visible = false;
                        }
                        else if (CAccessRight.CanAccess(CResource.eResource.ehrofficer))
                        {
                            for (int i = 0; i < pageControl.TabPages.Count; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[15].Visible = true;
                        }

                    }
                }
                else if (selectedDocumentType.Equals(Convert.ToInt32(serviceType.Authoritydeligation).ToString()))
                {
                    if (pageControl != null) //True when called from Page_Load
                    {
                        if (CAccessRight.CanAccess(
                                CResource.eResource.einspectorofficer) ||
                            CAccessRight.CanAccess(
                                CResource.eResource.einspectorsupervisor) ||
                            CAccessRight.CanAccess(
                                CResource.eResource.einspectordirector))
                        {
                            for (int i = 0; i < pageControl.TabPages.Count - 4; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[13].Visible = false;
                            pageControl.TabPages[14].Visible = true;
                            pageControl.TabPages[15].Visible = false;
                            pageControl.TabPages[16].Visible = false;
                        }
                        else if (
                            CAccessRight.CanAccess(
                                CResource.eResource.ehrofficer))
                        {
                            for (int i = 0;
                                i < pageControl.TabPages.Count;
                                i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[15].Visible = true;
                        }
                    }
                }
                else if (selectedDocumentType.Equals(Convert.ToInt32(serviceType.Salary).ToString()))
                {
                    if (pageControl != null) //True when called from Page_Load
                    {

                        if (CAccessRight.CanAccess(CResource.eResource.einspectorofficer) ||
                            CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) ||
                            CAccessRight.CanAccess(CResource.eResource.einspectordirector))
                        {
                            for (int i = 0;
                                i < pageControl.TabPages.Count - 3;
                                i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[5].Visible = true;
                            pageControl.TabPages[13].Visible = true;
                            pageControl.TabPages[14].Visible = true;
                            pageControl.TabPages[15].Visible = false;
                            pageControl.TabPages[16].Visible = true;
                        }
                        else if (CAccessRight.CanAccess(CResource.eResource.ehrofficer))
                        {
                            for (int i = 0; i < pageControl.TabPages.Count; i++)
                            {
                                pageControl.TabPages[i].Visible = false;
                            }
                            pageControl.TabPages[15].Visible = true;
                        }
                    }
                }
                else
                {
                    if (pageControl != null) //True when called from Page_Load
                    {

                        for (int i = 0; i < pageControl.TabPages.Count; i++)
                        {
                            pageControl.TabPages[i].Visible = false;
                        }
                    }
                }
            }
            else
            {
                if (pageControl != null) //True when called from Page_Load
                {
                    for (int i = 0; i < pageControl.TabPages.Count; i++)
                    {
                        pageControl.TabPages[i].Visible = false;
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void cbInspectionDetail_OnCallback(object sender, CallbackEventArgsBase e)
    {
        cbInspectionDetail.JSProperties["cpMessage"] = "ShowCriterias";
        cbInspectionDetail.JSProperties["cpStatus"] = "verified";
    }
}