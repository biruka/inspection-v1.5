﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using CUSTOR.Bussiness;
using DevExpress.Web;

using System.Text;
using System.Threading;

public partial class Pages_TeamwithOrganization : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["IsNew"] = true;
            LoadTeam();
            LoadOrganizations();
        }
    }

    protected override void InitializeCulture()
    {
        ProfileCommon p = this.Profile;
        if (p.Organization.LanguageID != null)
        {
            String selectedLanguage = string.Empty;
            switch (p.Organization.LanguageID)
            {
                case 0: selectedLanguage = "en-US";//English
                    break;
                case 2: selectedLanguage = "am-ET";//Afan Oromo
                    break;
                case 3: selectedLanguage = "am-ET"; //Tig
                    break;
                case 4: selectedLanguage = "en-GB";//afar
                    break;
                case 5: selectedLanguage = "en-AU";//Somali
                    break;
                default: break;//Amharic
            }
            UICulture = selectedLanguage;
            Culture = selectedLanguage;

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(selectedLanguage);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(selectedLanguage);
        }
        base.InitializeCulture();
    }

    protected void LoadTeam()
    {
        tblTeamBussiness objTeam = new tblTeamBussiness();
        ddlTeam.ValueField = "TeamGuid";
        ddlTeam.TextField = "TeamNameAm";
        ddlTeam.DataSource = objTeam.GettblTeams();
        ddlTeam.DataBind();
    }

    protected void LoadOrganizations()
    {
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        OrganizationBussiness obj = new OrganizationBussiness();
        lbAvailableOrg.ValueField = "OrgGuid";
        lbAvailableOrg.TextField = "DescriptionAm";
        lbAvailableOrg.DataSource = obj.GetActiveOrganizationsForInspectionTeam(Guid.Parse(p.Organization.GUID));
        lbAvailableOrg.DataBind();
        //ListEditItem le = new ListEditItem("ይምረጡ", "-1");
        //lbAvailableOrg.Items.Insert(0, le);
        //lbAvailableOrg.SelectedIndex = 0;

    }

    protected void LoadAssignOrginTeam(Guid TeamGuID)
    {
        lbChoosen.Items.Clear();
        tblTeamInOrganizationBussiness objtblTeamInOrganizationBussiness = new tblTeamInOrganizationBussiness();
        lbChoosen.ValueField = "OrgGuid";
        lbChoosen.TextField = "DescriptionAm";
        if (objtblTeamInOrganizationBussiness.GettblTeamInOrg(TeamGuID).Rows.Count > 0)
        {
            lbChoosen.DataSource = objtblTeamInOrganizationBussiness.GettblTeamInOrg(TeamGuID);
            lbChoosen.DataBind();
            btnTeamDelete.Enabled = true;
        } 
    }

    private void ShowError(string strMsg)
    {
        cbTeamwithOrganization.JSProperties["cpMessage"] = strMsg;
        cbTeamwithOrganization.JSProperties["cpStatus"] = "ERROR";
    }

    private void ShowMessage(string strMsg)
    {
        cbTeamwithOrganization.JSProperties["cpMessage"] = strMsg;
        cbTeamwithOrganization.JSProperties["cpStatus"] = "INFO";
    }

    private void ShowSuccess(string strMsg)
    {
        cbTeamwithOrganization.JSProperties["cpMessage"] = strMsg;
        cbTeamwithOrganization.JSProperties["cpStatus"] = "SUCCESS";
    }

    protected void DoTeamInOrganizationDelete(ArrayList team)
    {
        try
        {
            tblTeamInOrganizationBussiness objTeamInOrganization = new tblTeamInOrganizationBussiness();
            int deletedItemsCount = 0;
            if (team.Count > 0)
            {
                tblTeamMembersBussiness objTeamMemebers = new tblTeamMembersBussiness();

                for (int index = 0; index < team.Count; index++)
                {
                    //Check whethere there are team members
                    if (objTeamInOrganization.DeleteTeamOrgByTeamOrgGuid(Guid.Parse(Session["TeamGuid"].ToString()),Guid.Parse(team[index].ToString())))
                    {
                            deletedItemsCount++;
                    }
                }
            }
            if (deletedItemsCount > 0)
            {
                ShowMessage(Resources.Message.MSG_TeamOrganizationDeleted);
                ddlTeamSelectedIndexChanged();
            }
           
        }

        catch (Exception ex)
        {
            ShowError(ex.Message);
        }
        LoadAssignOrginTeam(Guid.Parse(ddlTeam.Value.ToString())); 
    }

    protected bool DoDeleteTeamOrg(Guid TeamGuid)
    {
        tblTeamInOrganizationBussiness objtblTeamInOrganizationBussiness = new tblTeamInOrganizationBussiness();
        try
        {
            objtblTeamInOrganizationBussiness.DeleteTeamOrg(TeamGuid);
            //Session["IsNew"] = false;
            //pnlData.Visible = false;
            //BindGrid();
            //ShowMessage("Record was deleted successfully.");
            return true;
        }
        catch (Exception ex)
        {
            //ShowError(ex.Message);
            return false;
        }
    }

    protected bool DoFind(Guid ID)
    {
        tblTeamInOrganizationBussiness objtblTeamInOrganizationBussiness = new tblTeamInOrganizationBussiness();

        tblTeamInOrganization objtblTeamInOrganization = new tblTeamInOrganization();

        try
        {
            objtblTeamInOrganization = objtblTeamInOrganizationBussiness.GettblTeamInOrganization(ID);
            if (objtblTeamInOrganization == null)
            {
                //ShowMessage("No record was found.");
                return false;
            }
            //Now Record was found;
            //Session["IsNew"] = false;
            ClearForm();
            //pnlData.Visible = true;
            //this.cboMainGuid.SelectedValue = objtblTeamInOrganization.MainGuid.ToString();
            //this.cboTeamGuid.SelectedValue = objtblTeamInOrganization.TeamGuid.ToString();
            //this.cboOrgGuid.SelectedValue = objtblTeamInOrganization.OrgGuid.ToString();
            //this.txtBudgetYear.Text = objtblTeamInOrganization.BudgetYear.ToString();
            Session["ID"] = ID;
            return true;
        }
        catch (Exception ex)
        {
            //ShowError(ex.Message);
            return false;
        }
    }

    protected void ClearForm()
    {
            lbChoosen.Items.Clear();
            LoadOrganizations();
            LoadTeam();        
    }

    protected bool DoNew()
    {
        try
        {
            Session["IsNew"] = true;
            //LoadAssignOrginTeam(Guid.Parse(ddlTeam.Value.ToString())); 
            //pnlData.Visible = true;
            //ClearForm();
            return true;
        }
        catch (Exception ex)
        {
            //ShowError(ex.Message);
            return false;
        }
    }

    protected bool ValidateTeamOrgnizationRelationShip()
    {
        bool isValid = true;
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        Guid TeamGuid = Guid.Parse(ddlTeam.Value.ToString()); 
        OrganizationBussiness objOrganization = new OrganizationBussiness();
        //Validate Wherther the selected team has organization that can be inspected by the same Parent organization
        //Check wherther the selected team has organizations assgined to it
        tblTeamInOrganizationBussiness objTeamInOrganization = new tblTeamInOrganizationBussiness();
        DataTable dtTeamOrganizations = objTeamInOrganization.GettblTeamInOrg(TeamGuid);
        List<Organization> objOrg = new List<Organization>();
        if (dtTeamOrganizations.Rows.Count > 0)
        {
            for (int index = 0; index < dtTeamOrganizations.Rows.Count; index++)
            {
                //retrive who can inspected the orgnization 
                objOrg = objOrganization.GetListOrg(Guid.Parse(dtTeamOrganizations.Rows[0]["OrgGuid"].ToString()));
                if (objOrg[0].InspectedBy != p.Organization.GUID)
                {
                    isValid = false;
                   
                }
                else
                {
                    isValid = true;
                }
            }

            return isValid;
        }
        else
        {
                    
            return isValid;
        }
    }

    protected void DoSave(ArrayList organizationList)
    {
        bool saved = false;
        tblTeamInOrganization objtblTeamInOrganization = new tblTeamInOrganization();
        try
        {
            if (ddlTeam.Value != null)
            {
                //if (ddlTeam.Value.ToString() != "-1")
                //{
                List<string> errMessages = GetErrorMessage();
                if (errMessages.Count > 0)
                {
                    ShowError(GetErrorDisplay(errMessages));
                }
                //DoDeleteTeamOrg(Guid.Parse(ddlTeam.Value.ToString()));
                if (ValidateTeamOrgnizationRelationShip())
                {
                    int count = organizationList.Count;
                    if (count != 0)
                    {
                        for(int i = 0; i < count; i++)
                        {
                            objtblTeamInOrganization.TeamGuid = Guid.Parse(ddlTeam.Value.ToString()); //Attention
                            objtblTeamInOrganization.OrgGuid = Guid.Parse(organizationList[i].ToString());
                            tblTeamInOrganizationBussiness objtblTeamInOrganizationBussiness =new tblTeamInOrganizationBussiness();
                            if ((bool)Session["IsNew"])
                            {
                                if (!objtblTeamInOrganizationBussiness.Exists(objtblTeamInOrganization.OrgGuid,
                                        objtblTeamInOrganization.TeamGuid))
                                {
                                    if (objtblTeamInOrganizationBussiness.InserttblTeamInOrganization(
                                        objtblTeamInOrganization))
                                    {
                                        saved = true;
                                    }
                                }
                                else
                                {
                                    //cbTeamwithOrganization.JSProperties["cpAction"] = "save";
                                    ShowError(Resources.Message.MSG_OrganizationAlreadyInTeam);
                                    break;
                                }
                            }
                            else
                            {
                                if(i == 0)
                                {
                                    objtblTeamInOrganizationBussiness.DeleteTeamOrg(Guid.Parse(ddlTeam.Value.ToString()));
                                }
                                if (objtblTeamInOrganizationBussiness.InserttblTeamInOrganization(objtblTeamInOrganization))
                                {
                                    saved = true;
                                }
                            }
                        }
                        
                    }
                    
                }
                else
                {
                    ShowError(Resources.Message.MSG_TeamOccupiedWithOtherOrganizations);
                }
            }
        }
        catch (Exception ex)
        {
            //ShowError(ex.Message);
        }
        
        if (saved)
        {
            
            cbTeamwithOrganization.JSProperties["cpAction"] = "save";
            ShowSuccess(Resources.Message.MSG_SAVED);
            LoadAssignOrginTeam(Guid.Parse(ddlTeam.Value.ToString()));
        }
    }

    protected void btnNew_Click(object sender, EventArgs e)
    {
        DoNew();
    }

    protected void ddlTeam_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadOrganizations();
        if (ddlTeam.Value != null)
        {
            if (ddlTeam.Value.ToString() != "-1")
            {
                
                LoadAssignOrginTeam(Guid.Parse(ddlTeam.Value.ToString()));
            }
            
        }
    }

    protected void ddlTeamSelectedIndexChanged()
    {
        if (ddlTeam.Value != null)
        {
            if (ddlTeam.Value.ToString() != "-1")
            {
                Session["IsNew"] = false;
                Session["TeamGuid"] = ddlTeam.Value.ToString();
                LoadAssignOrginTeam(Guid.Parse(ddlTeam.Value.ToString())); 
                
            }

        }
    }

    protected void cbTeamwithOrganization_Callback(object sender, CallbackEventArgsBase e)
    {
        ArrayList selelectedOrganizations = new ArrayList();
        string[] parameters = e.Parameter.Split('|');
        string strParam = parameters[0].ToString();
        switch (strParam)
        {
            case "New":
                DoNew();
                break;
            case "TeamChanged":
                ddlTeamSelectedIndexChanged();
                break;
            case "Save":
                int count = lbChoosen.Items.Count;
                if (parameters.Length >= 2)
                {
                    if (e.Parameter.ToString().Contains('|'))
                    {
                        for (int index = 1; index < parameters.Length; index++)
                        {
                            selelectedOrganizations.Add(parameters[index]);
                        }
                    }
                }
                DoSave(selelectedOrganizations);
                break;
            case "Delete":
                if (parameters.Length >= 2)
                {
                    if (e.Parameter.ToString().Contains('|'))
                    {
                        for (int index = 1; index < parameters.Length; index++)
                        {
                            selelectedOrganizations.Add(parameters[index]);
                        }
                    }
                }
                DoTeamInOrganizationDelete(selelectedOrganizations);
                break;
        }
    }

    //private void ShowError(string strMsg)
    //{
    //    cbTeamwithOrganization.JSProperties["cpMessage"] = strMsg;
    //    cbTeamwithOrganization.JSProperties["cpStatus"] = "ERROR";
    //}

    //private void ShowSuccess(string strMsg)
    //{
    //    cbTeamwithOrganization.JSProperties["cpMessage"] = strMsg;
    //    cbTeamwithOrganization.JSProperties["cpStatus"] = "SUCCESS";
    //}
   
    //private void ShowMessage(string strMsg)
    //{
    //    cbTeamwithOrganization.JSProperties["cpMessage"] = strMsg;
    //    cbTeamwithOrganization.JSProperties["cpStatus"] = "INFO";
    //}

    private string GetErrorDisplay(List<string> strMessages)
    {

        StringBuilder sb = new StringBuilder();
        if (strMessages.Count == 0) return string.Empty;
        sb.Append(Resources.Message.MSG_CVALD);
        sb.Append("<ul>");
        foreach (string s in strMessages)
        {
            sb.Append(String.Format("<li>{0}</li>", s));
        }
        sb.Append("</ul>");

        return sb.ToString();

    }

    private List<string> GetErrorMessage()
    {

        List<string> errMessages = new List<string>();
        if (ddlTeam.Value.ToString() == "-1")
        {
            errMessages.Add(Resources.Message.MSG_GM);
        }

        return errMessages;
    }
}