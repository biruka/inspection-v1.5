﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="TeamwithOrganization.aspx.cs" Inherits="Pages_TeamwithOrganization" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>




<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>
<%@ Register Src="../Controls/ConfirmBox.ascx" TagName="ConfirmBoxHR" TagPrefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        // <![CDATA[

        function ClosePopupTeamOrganizationConfirmation(result)
        {
            if (popupControl.IsVisible()) {
                popupControl.Hide();
            }
            if (result)
            {
                var items = lbChoosen.GetSelectedItems();
                var value = "";
                for (var index = 0; index < items.length; index++) {
                    if (index == 0) {
                        value = items[index].value;
                    } else {
                        value += '|' + items[index].value;
                    }
                }
                cbTeamwithOrganization.PerformCallback('Delete' + '|' + value);
            }
        }

        function DoTeamOrganizationDeleteClick() {
            ShowPopupConfirmation();
        }

        function ShowPopupConfirmation() {
            popupControl.Show();
            btnYes.Focus();
        }

        function btnYes_Click(s, e)
        {
            ClosePopupTeamOrganizationConfirmation(true);
        }

        function btnNo_Click(s, e) {
            ClosePopupTeamOrganizationConfirmation(false);
        }

        function DoNewClick(s, e) {
            cbTeamwithOrganization.PerformCallback('New');
        }
        function DoShowTeamOrganizationClick(s, e) {
            if (ddlTeam.GetValue() != null) {
                cbTeamwithOrganization.PerformCallback('TeamChanged' + '|' + ddlTeam.GetValue().toString());
            }
        }
        function DoSaveClick(s, e) {
            var result = lbChoosen.GetSelectedValues();
            var value = "";
            if (result.length > 0) {
                for (var index = 0; index < result.length; index++) {
                    if (index == 0) {
                        value = result[index];
                    } else {
                        value += '|' + result[index];
                    }
                }
                cbTeamwithOrganization.PerformCallback('Save' + '|' + value);
            }
            else {
                ShowError('ለቡድኑ የተመረጡ ተቋም የሉም እባክዎን ተቋማት ይምረጡ');
            }
            
        }
        function AddSelectedItems() {
            MoveSelectedItems(lbAvailable, lbChoosen);
            ShowUpdateButtonState();
        }
        function AddAllItems() {
            MoveAllItems(lbAvailable, lbChoosen);
            ShowUpdateButtonState();
        }
        function RemoveSelectedItems() {
            MoveSelectedItems(lbChoosen, lbAvailable);
            ShowUpdateButtonState();
        }
        function RemoveAllItems() {
            MoveAllItems(lbChoosen, lbAvailable);
            ShowUpdateButtonState();
        }
        function MoveSelectedItems(srcListBox, dstListBox) {
            srcListBox.BeginUpdate();
            dstListBox.BeginUpdate();
            var items = srcListBox.GetSelectedItems();
            for (var i = items.length - 1; i >= 0; i = i - 1) {
                dstListBox.AddItem(items[i].text, items[i].value);
                srcListBox.RemoveItem(items[i].index);
            }
            srcListBox.EndUpdate();
            dstListBox.EndUpdate();
            dstListBox.SelectAll();
        }
        function MoveAllItems(srcListBox, dstListBox) {
            srcListBox.BeginUpdate();
            var count = srcListBox.GetItemCount();
            for (var i = 0; i < count; i++) {
                var item = srcListBox.GetItem(i);
                dstListBox.AddItem(item.text, item.value);
            }
            srcListBox.EndUpdate();
            srcListBox.ClearItems();
            dstListBox.SelectAll();
        }
        function ShowUpdateButtonState() {
            btnMoveAllItemsToRight.SetEnabled(lbAvailable.GetItemCount() > 0);
            btnMoveAllItemsToLeft.SetEnabled(lbChoosen.GetItemCount() > 0);
            btnMoveSelectedItemsToRight.SetEnabled(lbAvailable.GetSelectedItems().length > 0);
            btnMoveSelectedItemsToLeft.SetEnabled(lbChoosen.GetSelectedItems().length > 0);
        }
        function OnEndTeamCallback(s, e)
        {
            if (s.cpStatus == "SUCCESS") {
                if (s.cpAction == 'save') {
                    ShowSuccess(s.cpMessage);
                }
            }
            else if (s.cpStatus == "ERROR") {
                ShowError(s.cpMessage);
            }
            else if (s.cpStatus == "INFO") {
                ShowSuccess(s.cpMessage);
            }
            s.cpStatus = "";
            s.cpAction = "";
            s.cpMessage = "";
        }
    </script>
    <style type="text/css">
        .auto-style2 {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceHolder" runat="Server">
    <uc1:Alert runat="server" ID="Alert" />

    <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
    <dx:ASPxGlobalEvents ID="GlobalEvents" runat="server">
        <ClientSideEvents ControlsInitialized="function(s, e) { ShowUpdateButtonState(); }" />
    </dx:ASPxGlobalEvents>
    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="100%" ShowHeader="False" meta:resourcekey="ASPxRoundPanel1Resource1">
        <PanelCollection>
            <dx:PanelContent meta:resourcekey="PanelContentResource1">
                <dx:ASPxCallbackPanel ID="cbTeamwithOrganization" ClientInstanceName="cbTeamwithOrganization" runat="server" Width="100%" OnCallback="cbTeamwithOrganization_Callback" meta:resourcekey="cbTeamwithOrganizationResource1">
                    <ClientSideEvents EndCallback="OnEndTeamCallback" />
                    <PanelCollection>
                        <dx:PanelContent meta:resourcekey="PanelContentResource2">
                            <table cellpadding="0" cellspacing="0" width="100%">

                                <tr>
                                    <td valign="top" colspan="3">
                                        <asp:Panel ID="pnlMenu0" runat="server" CssClass="RoundPanelToolbar" Width="950px" meta:resourcekey="pnlMenu0Resource1">
                                            <table cellpadding="0" cellspacing="0" style="width: 100%; padding: 0px;" width="990px">
                                                <tr>
                                                    <td style="width: 40%"></td>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxButton ID="btnNew" runat="server" AutoPostBack="false" ClientInstanceName="btnNew" Font-Names="Visual Geez Unicode" Font-Size="Small" HorizontalAlign="Center" Text="አዲስ" Theme="Office2010Silver" VerticalAlign="Middle" Width="70px" meta:resourcekey="btnNewResource1">
                                                                        <ClientSideEvents Click="function(s, e) {
	                                                                    DoNewClick();
                                                                    }" />
                                                                        <Image Url="~/Controls/ToolbarImages/new.gif">
                                                                        </Image>
                                                                    </dx:ASPxButton>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxButton ID="btnSave" runat="server" AutoPostBack="false" ClientInstanceName="btnSaveClient" Font-Names="Visual Geez Unicode" Font-Size="Small" HorizontalAlign="Center" Text="አስቀምጥ" Theme="Office2010Silver" VerticalAlign="Middle" Width="70px" TabIndex="11" meta:resourcekey="btnSaveResource1">
                                                                        <ClientSideEvents Click="function(s, e) {
	                                                                    DoSaveClick();
                                                                    }" />
                                                                        <Image Url="~/Controls/ToolbarImages/save.gif">
                                                                        </Image>
                                                                    </dx:ASPxButton>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxButton ID="btnTeamDelete" runat="server" AutoPostBack="False"
                                                                        ClientInstanceName="btnDeleteClient" Font-Names="Visual Geez Unicode"
                                                                        Font-Size="Small" HorizontalAlign="Center" meta:resourceKey="btnDeleteResource1"
                                                                        Text="ሰርዝ" Theme="Office2010Silver" Enabled="False"
                                                                        ValidationSettings-ValidationGroup="saveTeam" VerticalAlign="Middle"
                                                                        Width="70px">
                                                                        <ClientSideEvents Click="function(s, e) {
	                                                                         DoTeamOrganizationDeleteClick();
                                                                            }" />
                                                                        <Image Url="~/Controls/ToolbarImages/delete.gif">
                                                                        </Image>
                                                                    </dx:ASPxButton>
                                                                </td>
                                                                <td><dx:ASPxButton ID="btnShowTeamOrganization" runat="server" AutoPostBack="False"
                                                                        ClientInstanceName="btnShowTeamOrganization" Font-Names="Visual Geez Unicode"
                                                                        Font-Size="Small" HorizontalAlign="Center" meta:resourceKey="btnShowResource1"
                                                                        Text="አሳይ" Theme="Office2010Silver" Enabled="true"
                                                                        ValidationSettings-ValidationGroup="saveTeam" VerticalAlign="Middle"
                                                                        Width="70px">
                                                                        <ClientSideEvents Click="function(s, e) {
	                                                                         DoShowTeamOrganizationClick();
                                                                            }" />
                                                                         <Image Url="~/Files/Images/action_settings.gif">
                                                                         </Image>
                                                                    </dx:ASPxButton></td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="width: 20%">&nbsp;</td>
                                                    <td style="width: 10%"></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>

                                <tr>
                                    <td valign="top" style="width: 35%">&nbsp;</td>
                                    <td valign="middle" align="center" style="padding: 10px; width: 30%">
                                        <table class="auto-style2">
                                            <tr>
                                                <td>
                                                    <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="ቡድን" meta:resourcekey="ASPxLabel2Resource1">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td>
                                                    <dx:ASPxComboBox ID="ddlTeam" ClientInstanceName="ddlTeam" runat="server" Width="100%" meta:resourcekey="ddlTeamResource1">
                                                    </dx:ASPxComboBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top" style="width: 35%">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td valign="top" style="width: 35%">
                                        <div class="BottomPadding">
                                            <dx:ASPxLabel ID="lblAvailable" runat="server" Text="መስሪያ ቤቶች:" meta:resourcekey="lblAvailableResource1" />
                                        </div>
                                        <dx:ASPxListBox ID="lbAvailableOrg" runat="server" ClientInstanceName="lbAvailable"
                                            Width="100%"  Height="240px" SelectionMode="CheckColumn" Theme="Office2010Silver" meta:resourcekey="lbAvailableOrgResource1">
                                            
                                            <ClientSideEvents SelectedIndexChanged="function(s, e) { ShowUpdateButtonState(); }" />
                                        </dx:ASPxListBox>
                                    </td>
                                    <td valign="middle" align="center" style="padding: 10px; width: 30%">
                                        <div>
                                            <dx:ASPxButton ID="btnMoveSelectedItemsToRight" runat="server" ClientInstanceName="btnMoveSelectedItemsToRight"
                                                AutoPostBack="False" Text="ጨምር >" Width="130px" Height="23px" ClientEnabled="False"
                                                ToolTip="Add selected items" Theme="Office2010Silver" meta:resourcekey="btnMoveSelectedItemsToRightResource1">
                                                <ClientSideEvents Click="function(s, e) { AddSelectedItems(); }" />
                                            </dx:ASPxButton>
                                        </div>
                                        <div class="TopPadding">
                                            <dx:ASPxButton ID="btnMoveAllItemsToRight" runat="server" ClientInstanceName="btnMoveAllItemsToRight"
                                                AutoPostBack="False" Text="ሁሉንም ጨምር >>" Width="130px" Height="23px" ToolTip="Add all items" Theme="Office2010Silver" meta:resourcekey="btnMoveAllItemsToRightResource1">
                                                <ClientSideEvents Click="function(s, e) { AddAllItems(); }" />
                                            </dx:ASPxButton>
                                        </div>
                                        <div style="height: 32px">
                                        </div>
                                        <div>
                                            <dx:ASPxButton ID="btnMoveSelectedItemsToLeft" runat="server" ClientInstanceName="btnMoveSelectedItemsToLeft"
                                                AutoPostBack="False" Text="< ቀንስ" Width="130px" Height="23px" ClientEnabled="False"
                                                ToolTip="Remove selected items" Theme="Office2010Silver" meta:resourcekey="btnMoveSelectedItemsToLeftResource1">
                                                <ClientSideEvents Click="function(s, e) { RemoveSelectedItems(); }" />
                                            </dx:ASPxButton>
                                        </div>
                                        <div class="TopPadding">
                                            <dx:ASPxButton ID="btnMoveAllItemsToLeft" runat="server" ClientInstanceName="btnMoveAllItemsToLeft"
                                                AutoPostBack="False" Text="<< ሁሉንም ቀንስ " Width="130px" Height="23px" ClientEnabled="False"
                                                ToolTip="Remove all items" Theme="Office2010Silver" meta:resourcekey="btnMoveAllItemsToLeftResource1">
                                                <ClientSideEvents Click="function(s, e) { RemoveAllItems(); }" />
                                            </dx:ASPxButton>
                                        </div>
                                    </td>
                                    <td valign="top" style="width: 35%">
                                        <div class="BottomPadding">
                                            <dx:ASPxLabel ID="lblChosen" runat="server" Text="የተመረጡ መስሪያ ቤቶች::" meta:resourcekey="lblChosenResource1" />
                                        </div>
                                        <dx:ASPxListBox ID="lbChoosen" runat="server" ClientInstanceName="lbChoosen" Width="100%"
                                            Height="240px" SelectionMode="CheckColumn" Theme="Office2010Silver" meta:resourcekey="lbChoosenResource1">
                                            <ClientSideEvents SelectedIndexChanged="function(s, e) { ShowUpdateButtonState(); }"></ClientSideEvents>
                                        </dx:ASPxListBox>
                                    </td>
                                </tr>
                            </table>
                            <dx:ASPxPopupControl ID="ASPxPopupControl1" runat="server"
                                ClientInstanceName="popupControl" Font-Names="Visual Geez Unicode"
                                Font-Size="Small" HeaderText="Delete Confirmation"
                                meta:resourceKey="ASPxPopupControl1Resource1" Modal="True"
                                PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
                                <ContentCollection>
                                    <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server"
                                        meta:resourceKey="PopupControlContentControl1Resource1"
                                        SupportsDisabledAttribute="True">
                                        <uc6:confirmboxhr id="ConfirmBoxHR1" runat="server" />

                                    </dx:PopupControlContentControl>
                                </ContentCollection>
                            </dx:ASPxPopupControl>
                        </dx:PanelContent>
                    </PanelCollection>


                </dx:ASPxCallbackPanel>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>


</asp:Content>

