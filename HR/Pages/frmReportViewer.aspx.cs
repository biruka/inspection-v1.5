﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Bussiness;
using CUSTOR.Commen;
using Commen.Report;
using DevExpress.Web;
using DevExpress.XtraReports.Web;
using CUSTOR.Commen;

namespace UI.Pages
{
    public partial class frmReportViewer : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ProfileCommon p = this.Profile;
                p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
                OrganizationBussiness obj = new OrganizationBussiness();
                ComboOrg.ValueField = "OrgGuid";
                ComboOrg.TextField = "DescriptionAm";
                ComboOrg.DataSource = obj.GetOrganizations();
                ComboOrg.DataBind();
                ComboOrg.SelectedIndex = 0;
                if (p.Organization.LanguageID == 0)
                {
                    ListEditItem le = new ListEditItem("--Select--", "-1");
                    ComboOrg.Items.Insert(0, le);
                }
                if (p.Organization.LanguageID == 1)
                {
                    ListEditItem le = new ListEditItem("ይምረጡ", "-1");
                    ComboOrg.Items.Insert(0, le);
                }
                if (p.Organization.LanguageID == 2)
                {
                    ListEditItem le = new ListEditItem("Filadha", "-1");
                    ComboOrg.Items.Insert(0, le);
                }
                LoadOHRActivityType(p.Organization.LanguageID);
                dpFrom.SelectedDate =
                       Convert.ToString(
                           (EthiopicDateTime.GetEthiopicDate(int.Parse(DateTime.Today.Day.ToString()),
                               int.Parse(DateTime.Today.Month.ToString()), int.Parse(DateTime.Today.Year.ToString()))));
                dpTo.SelectedDate =
                    Convert.ToString(
                        (EthiopicDateTime.GetEthiopicDate(int.Parse(DateTime.Today.Day.ToString()),
                            int.Parse(DateTime.Today.Month.ToString()), int.Parse(DateTime.Today.Year.ToString()))));
            }
            try
            {
                if (Convert.ToBoolean(Session["rptview"]) == true)
                { 
                    ShowReport();
                    
                }
                   Secure();
            }
            catch (Exception ex)
            {
                MessageBox1.ShowInfo(ex.Message);
              
            }
        }

        protected void LoadOHRActivityType(int OrganizationLanguage)
        {
            tblDocumentTypeBussiness objHRtype = new tblDocumentTypeBussiness();
            ComboDocType.ValueField = "Code";
            ComboDocType.TextField = "DescriptionAm";
            ComboDocType.DataSource = objHRtype.GetDocTypeList(OrganizationLanguage);
            ComboDocType.DataBind();
            if (OrganizationLanguage == 0)
            {
                ListEditItem le = new ListEditItem("--Select--", "-1");
                ComboDocType.Items.Insert(0, le);
            }
            if (OrganizationLanguage == 1)
            {
                ListEditItem le = new ListEditItem("ይምረጡ", "-1");
                ComboDocType.Items.Insert(0, le);
            }
            if (OrganizationLanguage == 2)
            {
                ListEditItem le = new ListEditItem("Filadha", "-1");
                ComboDocType.Items.Insert(0, le);
            }
            //}
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            
            Session["rptview"] = true;
            ShowReport();
        }

        protected override void InitializeCulture()
        {
            ProfileCommon p = this.Profile;
            if (p.Organization.LanguageID != null)
            {
                String selectedLanguage = string.Empty;
                switch (p.Organization.LanguageID)
                {
                    case 0: selectedLanguage = "en-US";//English
                        break;
                    case 2: selectedLanguage = "am-ET";//Afan Oromo
                        break;
                    case 3: selectedLanguage = "am-ET"; //Tig
                        break;
                    case 4: selectedLanguage = "en-GB";//afar
                        break;
                    case 5: selectedLanguage = "en-AU";//Somali
                        break;
                    default: break;//Amharic
                }
                UICulture = selectedLanguage;
                Culture = selectedLanguage;

                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(selectedLanguage);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(selectedLanguage);
            }
            base.InitializeCulture();
        }

        private void ShowReport()
        {
            string ReportType = Request.QueryString["ReportType"];
            switch (ReportType)
            {
                case "InspectionList":
                     InspectionList();
                   break;
                case "Complete":
                   Complete();
                   break;
                case "IncompleteRecruitment":
                   IncompleteRecruitment();
                   break;
                case "IncompletePromotion":
                   IncompletePromotion();
                   break;
                case "IncompleteTransfer":
                   IncompleteTransfer();
                   break;                                 
                case "Reject":
                   Reject();
                   break;
                case "HRActivitySummery":
                   HRActivity();
                   break;
                case "InspectorHRActivitySummery":
                   InspectorHRActivity();
                   break;
                 default:
                    break;
            }

        
        }

        private void Complete()
        {
            try
            {
                string orgid = "Where HRMServiceType=1  ";
                rptInspection rptinspector = new rptInspection();
                InspReportEntityviewBussiness insbll = new InspReportEntityviewBussiness();
                rptComplete rprt = new rptComplete();
                rprt.xrLabel1.Text = "የተሟላ ግምገማዎች";
                if (!dpFrom.SelectedDate.Equals(""))
                {
                    if (!dpFrom.SelectedDate.Equals(""))
                    {
                        string displayEmpDate = dpFrom.SelectedDate;
                        DateTime empDate = dpFrom.SelectedGCDate;
                        string displayEmpDateTo = dpTo.SelectedDate;
                        DateTime empDateTo = (dpTo.SelectedGCDate);
                        orgid = orgid + " And  dbo.Inspection.approvaldate >= CAST((CONVERT(datetime, " +
                                empDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) +
                                ", 102)) as Date) And dbo.Inspection.approvaldate <= CAST((CONVERT(datetime, " +
                                empDateTo.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + ", 102)) as Date)";
                    }

                }

                if (ComboOrg.Value.ToString() != "-1")
                {
                    orgid = orgid + " and tblUnit.OrgGuid= " + "'" + (string)ComboOrg.SelectedItem.Value + "'";
                }
                if (ComboDocType.Value.ToString() != "-1")
                {
                    orgid = orgid + " and Activitytype= " + "'" + (string) ComboDocType.SelectedItem.Value + "'";
                }
                DataTable dtReport = insbll.GetRecordcomplete(orgid);
                if (dtReport.Rows.Count > 0)
                {
                    rprt.DataSource = insbll.GetRecordcomplete(orgid);
                    ReportViewer1.Report = rprt;
                    //rprt.xrDateFrom.Text = dpFrom.SelectedDate.ToString();
                    //rprt.xrDateTo.Text = dpTo.SelectedDate.ToString();
                    rptinspector.ShowDateRange();
                }
        }
            catch (Exception ex)
            {
                throw (ex);

            }
        }

        private void IncompleteRecruitment()
        {
            try
            {
                //string orgid = "Where dbo.Inspection.Activitytype='311' and dbo.Inspection.HRMServiceType=2 or dbo.Inspection.HRMServiceType=3";
                //desi
                string orgid = "Where dbo.Inspection.Activitytype='311' and (dbo.Inspection.HRMServiceType=2 or dbo.Inspection.HRMServiceType=3)";
                //
                rptInspection rptinspector = new rptInspection();
                InspReportEntityviewBussiness insbll = new InspReportEntityviewBussiness();
                rptComplete rprt = new rptComplete();
                rprt.xrLabel1.Text = Resources.Message.MSG_RR;

                if (!dpFrom.SelectedDate.Equals(""))
                {
                    if (!dpFrom.SelectedDate.Equals(""))
                    {
                        string displayEmpDate = dpFrom.SelectedDate;
                        var empDate = dpFrom.SelectedGCDate.Date;
                        string displayEmpDateTo = dpTo.SelectedDate;
                        var empDateTo = (dpTo.SelectedGCDate.Date);
                        orgid = orgid + " And  dbo.Inspection.approvaldate >= CAST((CONVERT(datetime, " + empDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + ", 102)) as Date) And dbo.Inspection.approvaldate <= CAST((CONVERT(datetime, " + empDateTo.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + ", 102)) as Date)";
                    }
                }
               if (ComboOrg.Value.ToString() != "-1" )
                {
                    orgid = orgid + " and dbo.tblUnit.OrgGuid= " + "'" + (string)ComboOrg.SelectedItem.Value + "'";


                    //orgid = orgid + " and  dbo.tblPerson.OrgGuid= " + "'" + (string)ComboOrg.SelectedItem.Value + "'";
                }
                //if (ComboDocType.Value.ToString() != "-1" )
                //{
                //    orgid = orgid + " and dbo.Inspection.Activitytype= " + "'" + (string)ComboDocType.SelectedItem.Value + "'";
                //}
                else if (AreUserInputsValid())
                {

                }
               DataTable dtReport = insbll.GetRecordcomplete(orgid);
               if (dtReport.Rows.Count > 0)
               {
                   rprt.DataSource = insbll.GetRecordcomplete(orgid);
                   ReportViewer1.Report = rprt;
                   //rprt.xrDateFrom.Text = dpFrom.SelectedDate.ToString();
                   //rprt.xrDateTo.Text = dpTo.SelectedDate.ToString();
                   rptinspector.ShowDateRange();
               }
            }
            catch (Exception ex)
            {
                throw (ex);

            }
        }

        private void IncompletePromotion()
        {
            try
            {
                string orgid = "Where dbo.Inspection.HRMServiceType=2 or dbo.Inspection.HRMServiceType=3";              
                InspReportEntityviewBussiness insbll = new InspReportEntityviewBussiness();
                rptPromotionComplete rprt = new rptPromotionComplete();
                rprt.xrLabel1.Text = Resources.Message.MSG_RP;

                if (!dpFrom.SelectedDate.Equals("") && !dpFrom.IsValidDate)
                {
                    if (!dpTo.SelectedDate.Equals("") && !dpTo.IsValidDate)
                    {
                        string displayEmpDate = dpFrom.SelectedDate;
                        DateTime empDate = dpFrom.SelectedGCDate;
                        string displayEmpDateTo = dpTo.SelectedDate;
                        DateTime empDateTo = (dpTo.SelectedGCDate);
                        orgid = orgid + " And  dbo.Inspection.approvaldate >= CAST((CONVERT(datetime, " + empDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + ", 102)) as Date) And dbo.Inspection.approvaldate <= CAST((CONVERT(datetime, " + empDateTo.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + ", 102)) as Date)";
                    }

                }


                if (ComboOrg.Value.ToString() != "-1")
                {
                    orgid = orgid + " and  dbo.tblPerson.OrgGuid= " + "'" + (string)ComboOrg.SelectedItem.Value + "'";
                }
                if (ComboDocType.Value.ToString() != "-1")
                {
                    orgid = orgid + " and dbo.Inspection.Activitytype= " + "'" + (string)ComboDocType.SelectedItem.Value + "'";
                }
                else if (AreUserInputsValid())
                {

                }

                DataTable dtReport = insbll.GetRecordIncomplatePromotion(orgid);
                if (dtReport.Rows.Count > 0)
                {
                    rprt.DataSource = insbll.GetRecordcomplete(orgid);
                    ReportViewer1.Report = rprt;
                    rprt.xrDateFrom.Text = dpFrom.SelectedDate.ToString();
                    rprt.xrDateTo.Text = dpTo.SelectedDate.ToString();
                    //rptinspector.ShowDateRange();
                }
                //rptinspector.ShowDateRange();
            }
            catch (Exception ex)
            {
                throw (ex);

            }
        }

        private void IncompleteTransfer()
        {
            try
            {
                string orgid = "Where dbo.Inspection.Activitytype='313' and dbo.Inspection.HRMServiceType=2 or dbo.Inspection.HRMServiceType=3 ";
                //rptInspection rptinspector = new rptInspection();
                InspReportEntityviewBussiness insbll = new InspReportEntityviewBussiness();
                rptTransferComplete rprt = new rptTransferComplete();
                rprt.xrLabel1.Text = Resources.Message.MSG_RT;
                if (!dpFrom.SelectedDate.Equals("") && !dpFrom.IsValidDate)
                {
                    if (!dpTo.SelectedDate.Equals("") && !dpTo.IsValidDate)
                    {
                        string displayEmpDate = dpFrom.SelectedDate;
                        DateTime empDate = dpFrom.SelectedGCDate;
                        string displayEmpDateTo = dpTo.SelectedDate;
                        DateTime empDateTo = (dpTo.SelectedGCDate);
                        orgid = orgid + " And  dbo.Inspection.approvaldate >= CAST((CONVERT(datetime, " + empDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + ", 102)) as Date) And dbo.Inspection.approvaldate <= CAST((CONVERT(datetime, " + empDateTo.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + ", 102)) as Date)";
                    }

                }


                if (ComboOrg.Value.ToString() != "-1")
                {
                    orgid = orgid + " and dbo.tblPerson.OrgGuid= " + "'" + (string)ComboOrg.SelectedItem.Value + "'";
                }
                if (ComboDocType.Value.ToString() != "-1")
                {
                    orgid = orgid + " and Activitytype= " + "'" + (string)ComboDocType.SelectedItem.Value + "'";
                }
                else if (AreUserInputsValid())
                {

                }
                DataTable dtReport = insbll.GetRecordcomplete(orgid);
                if (dtReport.Rows.Count > 0)
                {
                    rprt.DataSource = insbll.GetRecordIncomplateTransfer(orgid);
                    ReportViewer1.Report = rprt;
                    rprt.xrDateFrom.Text = dpFrom.SelectedDate.ToString();
                    rprt.xrDateTo.Text = dpTo.SelectedDate.ToString();
                }
                
            }
            catch (Exception ex)
            {
                throw (ex);

            }
        }

        private void Reject()
        {
            try
            {
                string orgid = "Where HRMServiceType=3 ";
                rptInspection rptinspector = new rptInspection();
                InspReportEntityviewBussiness insbll = new InspReportEntityviewBussiness();
                rptComplete rprt = new rptComplete();
                rprt.xrLabel1.Text = "የተሳሳቱ ግምገማዎች";
                if (!dpFrom.SelectedDate.Equals(""))
                {
                    //rptinspector.dateFrom = dpFrom.SelectedGCDate;
                }
                if (!dpTo.SelectedDate.Equals(""))
                {
                    //rptinspector.dateTo = dpTo.SelectedGCDate;
                }

                if (ComboOrg.Value.ToString() != "-1")
                {
                    orgid = orgid + "  and  OrgGuid= " + "'" + (string)ComboOrg.SelectedItem.Value + "'";
                }
                if (ComboDocType.Value.ToString() != "-1")
                {
                    orgid = orgid + " and Activitytype= " + "'" + (string)ComboDocType.SelectedItem.Value + "'";
                }
                else if (AreUserInputsValid())
                {

                }

                //rprt.DataSource = insbll.GetRecordcomplete(orgid);
                ReportViewer1.Report = rprt;
                //rprt.xrDateFrom.Text = dpFrom.SelectedDate.ToString();
                //rprt.xrDateTo.Text = dpTo.SelectedDate.ToString();
                rptinspector.ShowDateRange();
            }
            catch (Exception ex)
            {
                throw (ex);

            }

        }

         private void InspectorHRActivity()
        {
            try
            {
                string orgid = "";
                rptInspection rptinspector = new rptInspection();
                InspReportEntityviewBussiness insbll = new InspReportEntityviewBussiness();
                rptinspectorHractivitysummery rpt = new rptinspectorHractivitysummery();

                if (!dpFrom.SelectedDate.Equals("") && !dpFrom.IsValidDate)
                {
                    if (!dpTo.SelectedDate.Equals("") && !dpTo.IsValidDate)
                    {
                        string displayEmpDate = dpFrom.SelectedDate;
                        DateTime empDate = dpFrom.SelectedGCDate;
                        string displayEmpDateTo = dpTo.SelectedDate;
                        DateTime empDateTo = (dpTo.SelectedGCDate);
                        orgid = orgid + " And  dbo.Inspection.approvaldate >= CAST((CONVERT(datetime, " + empDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + ", 102)) as Date) And dbo.Inspection.approvaldate <= CAST((CONVERT(datetime, " + empDateTo.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + ", 102)) as Date)";
                    }

                }
                if (ComboOrg.Value.ToString() != "-1")
                {
                    orgid = orgid + " and dbo.tblPerson.OrgGuid= " + "'" + (string)ComboOrg.SelectedItem.Value + "'";
                }
                if (ComboDocType.Value.ToString() != "-1")
                {
                    orgid = orgid + " and Activitytype= " + "'" + (string)ComboDocType.SelectedItem.Value + "'";
                }
                else if (AreUserInputsValid())
                {

                }
                DataTable dtReport = insbll.GetinspectorHrActivitySummery(orgid);
                if (dtReport.Rows.Count > 0)
                {
                    rpt.DataSource = dtReport;
                    ReportViewer1.Report = rpt;
                    //rpt.xrDateFrom.Text = dpFrom.SelectedDate.ToString();
                    //rpt.xrDateTo.Text = dpTo.SelectedDate.ToString();
                }
                
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            }

        private void HRActivity()
        {
            try
            {
                string orgid="where 1=1  ";
                rptInspection rptinspector = new rptInspection();
                InspReportEntityviewBussiness insbll = new InspReportEntityviewBussiness();
                HrActivitySummery rprt = new HrActivitySummery();

                if (!dpFrom.SelectedDate.Equals("") && !dpFrom.IsValidDate)
                {
                    if (!dpTo.SelectedDate.Equals("") && !dpTo.IsValidDate)
                    {
                        string displayEmpDate = dpFrom.SelectedDate;
                        DateTime empDate = dpFrom.SelectedGCDate;
                        string displayEmpDateTo = dpTo.SelectedDate;
                        DateTime empDateTo = (dpTo.SelectedGCDate);
                        orgid = orgid + " And  dbo.Inspection.approvaldate >= CAST((CONVERT(datetime, " + empDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + ", 102)) as Date) And dbo.Inspection.approvaldate <= CAST((CONVERT(datetime, " + empDateTo.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + ", 102)) as Date)";
                        //}
                        //if (!dpTo.SelectedDate.Equals(""))
                        //{
                        //    rptinspector.dateTo = dpTo.SelectedGCDate;
                        //}

                    }

                }
                //CCommon objcom = new CCommon();
                if (ComboOrg.SelectedItem.Value.ToString() != "-1" )
                {
                    orgid = orgid + " And dbo.Organization.OrgGuid= " + "'" + (string)ComboOrg.SelectedItem.Value + "'";
                }
                if (ComboDocType.SelectedItem.Value.ToString () != "-1")
                {
                    orgid = orgid + " and Activitytype= " + "'" + (string)ComboDocType.SelectedItem.Value + "'";
                }
                //else if (AreUserInputsValid())
                //{

                //}
                DataTable dtReport = insbll.GetHrActivitySummery(orgid);
                if (dtReport.Rows.Count > 0)
                {
                    rprt.DataSource = dtReport;
                    ReportViewer1.Report = rprt;
                    //rpt.xrDateFrom.Text = dpFrom.SelectedDate.ToString();
                    //rpt.xrDateTo.Text = dpTo.SelectedDate.ToString();
                    rptinspector.ShowDateRange();
                }
            }
            catch (Exception ex)
            {
                throw (ex);

            }
        }

        private void InspectionList()
        {
            try
            { 
             string orgid = " Where 1=1";
            rptInspection rptinspector = new rptInspection();
            InspReportEntityviewBussiness insbll = new InspReportEntityviewBussiness();
            rptInspection rprt = new rptInspection();
            if (ComboOrg.Value.ToString() != "-1")
            {
                orgid = orgid + " And  OrgGuid= " + "'" + (string)ComboOrg.SelectedItem.Value + "'";
            }
            if (ComboDocType.Value.ToString() != "-1")
            {
                orgid = orgid + " and Activitytype= " + "'" + (string)ComboDocType.SelectedItem.Value + "'";
            }

                if (!dpFrom.SelectedDate.Equals(""))
                {
                    if (!dpTo.SelectedDate.Equals(""))
                    {
                        string displayEmpDate = dpFrom.SelectedDate;
                        DateTime empDate = dpFrom.SelectedGCDate;
                        string displayEmpDateTo = dpTo.SelectedDate;
                        DateTime empDateTo = (dpTo.SelectedGCDate);
                        orgid = orgid + " And  approvaldate >= CAST((CONVERT(datetime, " +
                                empDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) +
                                ", 102)) as Date) And approvaldate <= CAST((CONVERT(datetime, " +
                                empDateTo.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + ", 102)) as Date)";
                    }

                }

                DataTable dtReport = insbll.GetRecord(orgid);
            if (dtReport.Rows.Count > 0)
            {
                rprt.DataSource = dtReport;
                ReportViewer1.Report = rprt;
                rptinspector.ShowDateRange();
            }
        }
        catch (Exception ex)
        {
            throw (ex);

        }          
    }

        protected bool AreUserInputsValid()
        {
            bool userInputsAreValid = true;
            DateTime reportedDateFrom = (dpFrom.SelectedDate.Equals("") ? new DateTime() : Convert.ToDateTime(dpFrom.SelectedGCDate));
            DateTime reportedDateTo = (dpTo.SelectedDate.Equals("") ? new DateTime() : Convert.ToDateTime(dpTo.SelectedGCDate));
            if (!dpTo.SelectedDate.Equals("") && reportedDateFrom > reportedDateTo)
            {
                MessageBox1.ShowInfo(Resources.Message.MSG_VALIDDATE);
                userInputsAreValid = false;
            }

            return userInputsAreValid;
        }
        
        protected void Secure()
        {
            if (!(CAccessRight.CanAccess(CResource.eResource.einspector)) )
                
            {
                //tabpage access = true;
            }
            else if (CAccessRight.CanAccess(CResource.eResource.einspector))
            {

            }
            else
            {
                Response.Redirect("login.aspx", false);
            }
            if (CAccessRight.CanDelete(CResource.eResource.einspector))
            {
                //tabpage access = true;
            }

        }
       
    }
}