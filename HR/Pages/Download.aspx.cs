﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_Download : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DownloadFile(Request.Params[0].ToString(), Request.Params[1].ToString());
    }

    protected void DownloadFile(string fileUrl, string fileName)
    {
        HttpResponse response = HttpContext.Current.Response;
        response.Clear();
        response.ClearContent();
        response.ClearHeaders();
        response.Buffer = true;
        response.AddHeader("Content-Disposition", "attachment;filename=" + fileName );
        response.Flush();
        response.TransmitFile(fileUrl);
        response.End();
    }
}