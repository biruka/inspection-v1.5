﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Bussiness;
using CUSTOR.Domain;
public partial class Pages_DisplayFile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["MainGuid"] != null)
        {

            Session["MainGuid"] = Request.QueryString["MainGuid"];
        }
        else
        {




            Session["MainGuid"] = "ba86ec6d-ce8f-4507-82b4-5e5dd8487692";
        }
        try
        {
            Attachment objAttachment = new Attachment();
            AttachmentBussiness objAttachmentBusiness = new AttachmentBussiness();
            objAttachment = objAttachmentBusiness.GetAttachment(Guid.Parse(Session["MainGuid"].ToString()));
            string url = objAttachment.Url;
            string NavigateUrl = @"C:\inetpub\wwwroot\hr\Attachment\" + url; //this is Physical path where the HR Attachment is exist        
            System.Diagnostics.Process.Start(NavigateUrl);
        }
        catch
        { }
        
    }
}