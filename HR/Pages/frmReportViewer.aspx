﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeFile="frmReportViewer.aspx.cs" Inherits="UI.Pages.frmReportViewer" %>
<%@ Register assembly="DevExpress.XtraReports.v19.2.Web.WebForms, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<link href="../App_Themes/Default/CSS/Default.css" rel="stylesheet" type="text/css" />--%>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceHolder" runat="server">

    <div>
        <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
        <cc2:MessageBox ID="MessageBox1" runat="server" />
        <table frame="box" cellpadding="0" cellspacing="0" style="border-collapse: initial; width: 100%;">
            <tr style="border-bottom-width: 0px">
                <%-- <td height="21" align="center" bgcolor="#EAECF4" class="divTopBar">
                    <font color="#5976AA" size="2" face="Verdana, Arial, Helvetica, sans-serif">የግምገማ ሲስተም
                    </font>
                </td>--%>
            </tr>
            <tr>
                <td align="center">
                    <asp:Panel ID="pnlMsg" runat="server" CssClass="messageWarning" EnableViewState="False"
                        Height="100%" Visible="False" Width="100%" BorderColor="#99CCFF">

                        <asp:Label ID="lblPersonMsg" runat="server" CssClass="Message" Font-Bold="True" ForeColor="#CC3300"></asp:Label>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td height="114" align="center">
                    <asp:Panel ID="Panel1" runat="server" Width="521px" CssClass="RoundPanelToolbar">
                        <table width="100%" border="0">
                            <tr>
                                <td width="70%" rowspan="1" align="right">
                                    <dx:ASPxLabel ID="lblOrganization" runat="server" meta:resourcekey="lblOrganizationResource1" Text="ድርጅት:">
                                    </dx:ASPxLabel>
                                </td>
                                <td rowspan="1" align="left">
                                    <dx:ASPxComboBox ID="ComboOrg" runat="server" Height="26px" Width="200px" DropDownStyle="DropDown" EnableIncrementalFiltering="True" IncrementalFilteringMode="StartsWith">
                                        <%--<Items>
                                                    <dx:ListEditItem Text="-1" Value= "ይምረጡ"  Selected="True" />
                                                 </Items>--%>
                                    </dx:ASPxComboBox>
                                </td>
                                <td rowspan="1" align="center" class="style1"></td>
                            </tr>
                            <tr>
                                <td width="70%" rowspan="1" align="right">
                                    <dx:ASPxLabel ID="lbldoctype" runat="server" Text="የሰው ሃይል አስተዳደር ክንውን:" meta:resourcekey="lbldoctypeResource1" Width="125px">
                                    </dx:ASPxLabel>
                                </td>
                                <td width="50%" rowspan="1" align="left">
                                    <dx:ASPxComboBox ID="ComboDocType" runat="server" Height="26px" Width="200px" DropDownStyle="DropDown"
                                        PopupHorizontalAlign="RightSides" SelectedIndex="0">
                                    </dx:ASPxComboBox>
                                </td>
                                <td rowspan="1" align="center" class="style1"></td>
                            </tr>
                            <tr>
                                <td width="70%" rowspan="1" align="right">
                                    <dx:ASPxLabel ID="lbldate" runat="server" Text="ቀን:" meta:resourcekey="lbldateResource1">
                                    </dx:ASPxLabel>
                                </td>
                                <td width="34%" rowspan="1" align="center">
                                    <table>
                                        <tr>
                                            <td align="left">
                                                <cdp:CUSTORDatePicker ID="dpFrom" runat="server" SelectedDate="" TextCssClass="" />
                                            </td>
                                            <td>
                                                <strong>-</strong>
                                            </td>
                                            <td align="left">
                                                <cdp:CUSTORDatePicker ID="dpTo" runat="server" SelectedDate="" TextCssClass="" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td rowspan="1" align="left" class="style1">
                                    <dx:ASPxButton ID="btnsearch" runat="server" Text="ፈልግ:" HorizontalAlign="Center" Image-Url="~/images/Toolbar/search.gif" ClientInstanceName="btnsearch" Theme="Office2010Silver"
                                        OnClick="btnsearch_Click">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                            <tr>
                                <td width="34%" rowspan="1" align="center" class="style4"></td>
                                <td width="34%" rowspan="1" align="right" class="style4"></td>
                                <td rowspan="1" align="center" class="style4"></td>
                                <td class="style4"></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td height="114" align="center">
                    <table width="100%" border="0">
                        <tr>
                            <td width="88%" class="droplink">
                                <asp:ObjectDataSource ID="dsorganization" runat="server"
                                    OldValuesParameterFormatString="original_{0}"
                                    SelectMethod="GettblOrganizations"
                                    TypeName="CUSTOR.Business.tblOrganizationBusiness"></asp:ObjectDataSource>
                                <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="#" class="droplink"></a></font>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <dx:ReportToolbar ID="ReportToolbar21" runat="server" ReportViewerID="ReportViewer1" ShowDefaultButtons="False" Width="100%">
                                    <Items>
                                        <dx:ReportToolbarButton ItemKind="Search" />
                                        <dx:ReportToolbarSeparator />
                                        <dx:ReportToolbarButton ItemKind="PrintReport" />
                                        <dx:ReportToolbarButton ItemKind="PrintPage" />
                                        <dx:ReportToolbarSeparator />
                                        <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                                        <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                                        <dx:ReportToolbarLabel ItemKind="PageLabel" />
                                        <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                                        </dx:ReportToolbarComboBox>
                                        <dx:ReportToolbarLabel ItemKind="OfLabel" />
                                        <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                                        <dx:ReportToolbarButton ItemKind="NextPage" />
                                        <dx:ReportToolbarButton ItemKind="LastPage" />
                                        <dx:ReportToolbarSeparator />
                                        <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                                        <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                                        <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                                            <Elements>
                                                <dx:ListElement Value="pdf" />
                                                <dx:ListElement Value="xls" />
                                                <dx:ListElement Value="xlsx" />
                                                <dx:ListElement Value="rtf" />
                                                <dx:ListElement Value="mht" />
                                                <dx:ListElement Value="html" />
                                                <dx:ListElement Value="txt" />
                                                <dx:ListElement Value="csv" />
                                                <dx:ListElement Value="png" />
                                            </Elements>
                                        </dx:ReportToolbarComboBox>
                                    </Items>
                                    <Styles>
                                        <LabelStyle>
                                            <Margins MarginLeft='3px' MarginRight='3px' />
                                        </LabelStyle>
                                    </Styles>
                                </dx:ReportToolbar>
                            </td>
                        </tr>
                        <tr>
                            <td height="18" class="droplink">
                                <dx:ReportViewer ID="ReportViewer1" runat="server" Theme="Aqua">
                                    <LoadingPanelStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </LoadingPanelStyle>
                                </dx:ReportViewer>
                                <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="#" class="droplink"></a></font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
