﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_TeamAndTeamMembers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected override void InitializeCulture()
    {
        ProfileCommon p = this.Profile;
        if (p.Organization.LanguageID != null)
        {
            String selectedLanguage = string.Empty;
            switch (p.Organization.LanguageID)
            {
                case 0: selectedLanguage = "en-US";//English
                    break;
                case 2: selectedLanguage = "am-ET";//Afan Oromo
                    break;
                case 3: selectedLanguage = "am-ET"; //Tig
                    break;
                case 4: selectedLanguage = "en-GB";//afar
                    break;
                case 5: selectedLanguage = "en-AU";//Somali
                    break;
                default: break;//Amharic
            }
            UICulture = selectedLanguage;
            Culture = selectedLanguage;

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(selectedLanguage);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(selectedLanguage);
        }
        base.InitializeCulture();
    }
}