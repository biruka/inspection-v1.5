﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="InspectionDetail.aspx.cs" Inherits="Pages_InspectionDetail" culture="auto" meta:resourcekey="PageResource12" uiculture="auto" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Src="~/Controls/Promotion.ascx" TagPrefix="UC" TagName="promotion" %>
<%@ Register Src="~/Controls/EducationalBackground.ascx" TagPrefix="UC" TagName="EducationalBackground" %>
<%@ Register Src="~/Controls/Transfer.ascx" TagPrefix="UC" TagName="transfer" %>
<%@ Register Src="~/Controls/VacancyAnnouncement.ascx" TagPrefix="UC" TagName="vacancy" %>
<%@ Register Src="~/Controls/Inspection.ascx" TagPrefix="UC" TagName="inspection" %>
<%@ Register Src="~/Controls/Scholarship.ascx" TagPrefix="UC" TagName="Scholarship" %>
<%@ Register Src="~/Controls/WorkExperiance.ascx" TagPrefix="UC" TagName="workexperience" %>
<%@ Register Src="~/Controls/Candidates.ascx" TagPrefix="UC" TagName="candidates" %>
<%@ Register Src="~/Controls/Training.ascx" TagPrefix="UC" TagName="Training" %>
<%@ Register Src="~/Controls/Discipline.ascx" TagPrefix="UC" TagName="Discipline" %>
<%@ Register Src="~/Controls/ServiceTermination.ascx" TagPrefix="UC" TagName="Termination" %>
<%@ Register Src="~/Controls/Medical.ascx" TagPrefix="UC" TagName="Medical" %>
<%@ Register Src="~/Controls/Attachment.ascx" TagPrefix="UC" TagName="Attachment" %>
<%@ Register Src="~/Controls/Evaluation.ascx" TagPrefix="UC" TagName="Evaluation" %>
<%@ Register Src="~/Controls/Response.ascx" TagPrefix="UC" TagName="Response" %>
<%@ Register Src="~/Controls/Delegation.ascx" TagPrefix="UC" TagName="Delegation" %>
<%@ Register Src="~/Controls/ServiceExtension.ascx" TagPrefix="UC" TagName="ServiceExtension" %>
<%@ Register Src="~/Controls/Appraisal.ascx" TagPrefix="UC" TagName="Appraisal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" language="javascript">
        function OnEndCallback(s, e)
        {
            if (s.cpStatus == "verified") {
                if (s.cpMessage == 'ShowCriterias') {
                    if (criterialist.GetTab(0).visible) {
                        DvgwVanacncy.PerformCallback();
                    }
                    if (criterialist.GetTab(1).visible) {
                        DvGcandidate.PerformCallback();
                    }
                    if (criterialist.GetTab(2).visible) {
                        DvgvEducationalBackground.PerformCallback();
                    }
                    if (criterialist.GetTab(3).visible) {
                        Training.PerformCallback();
                    }
                    if (criterialist.GetTab(4).visible) {
                        DvgvWorkexp.PerformCallback();
                    }
                    if (criterialist.GetTab(5).visible) {
                        DvgEvaluation.PerformCallback();
                    }
                    if (criterialist.GetTab(6).visible) {
                        Promotion.PerformCallback();
                    }
                    if (criterialist.GetTab(7).visible) {
                        Dvgvtransfer.PerformCallback();
                    }
                    if (criterialist.GetTab(8).visible) {
                        Dvgvserextend.PerformCallback();
                    }
                    if (criterialist.GetTab(9).visible) {
                        DVgvtermination.PerformCallback();
                    }
                    if (criterialist.GetTab(10.visible) {
                        Dvgvdeciplin.PerformCallback();
                    }
                    if (criterialist.GetTab(11).visible) {
                        Dvgvhealth.PerformCallback();
                    }
                    if (criterialist.GetTab(12).visible) {
                        scholarship.PerformCallback();
                    }
                    if (criterialist.GetTab(13).visible) {
                        delegation.PerformCallback();
                    }
                    if (criterialist.GetTab(14).visible) {
                        DvgvAttachment.PerformCallback();
                    }
                    if (criterialist.GetTab(15).visible) {
                        criterialist.SetActiveTabIndex(15);
                        InspectionList.PerformCallback();
                    }
                    if (criterialist.GetTab(16).visible) {
                        cbResponse.PerformCallback('Load');
                    }
                }
            }
        }
        function pageLoad(sender, args) {
            cbInspectionDetail.PerformCallback();
        };

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceHolder" runat="Server">
    <table class="auto-style26" align="center">
    <tr style="border-bottom-width: 0px">
        <td style="text-align: center">
            <asp:Panel ID="pnlMsg" runat="server" CssClass="messageWarning" EnableViewState="False"
                Height="100%" Visible="False" Width="100%" BorderColor="#99CCFF" BackColor="#CCFF33" meta:resourcekey="pnlMsgResource1">
                <asp:Label ID="lblPersonMsg" runat="server" CssClass="Message" Font-Bold="True" ForeColor="Red" meta:resourcekey="lblPersonMsgResource1"></asp:Label>
            </asp:Panel>
        </td>
    </tr>
</table>
<dx:ASPxCallbackPanel ID="cbInspectionDetail" runat="server" ClientInstanceName="cbInspectionDetail" OnCallback="cbInspectionDetail_OnCallback" Width="100%" meta:resourcekey="cbInspectionDetailResource1">
    <ClientSideEvents EndCallback="OnEndCallback"></ClientSideEvents>
    <PanelCollection>
        <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="PanelContent1Resource1">
            <dx:ASPxPageControl ID="pageControl" runat="server"  ClientInstanceName="criterialist" ActiveTabIndex="15" Width="100%" EnableHierarchyRecreation="True" EnableViewState="False"
                EnableTheming="True"  meta:resourcekey="pageControlResource1" Theme="PlasticBlue">
                <TabPages>
                    <dx:TabPage Text="ክፍት የስራ ቦታ" Visible="True" meta:resourcekey="TabPageResource1">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl2" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControl2Resource1">
                                <UC:vacancy ID="ucVacancy" runat="server" />
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="ዕጩ ተወዳዳሪዋች" Visible="True" meta:resourcekey="TabPageResource5">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl5" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControl5Resource1">
                                <UC:candidates runat="server" ID="ucCandidates" />
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="የትምህርት ማስረጃ" Visible="True" meta:resourcekey="TabPageResource2">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl1" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControl1Resource1">
                                <UC:EducationalBackground ID="ucEducationalBackground" runat="server" />
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="የተከታተሉት ስልጠና" Visible="True" meta:resourcekey="TabPageResource3">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl4" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControl4Resource1">
                                <UC:Training runat="server" ID="ucTraining" />
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="የስራ ልምድ" Visible="True" meta:resourcekey="TabPageResource4">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl3" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControl3Resource1">
                                <UC:workexperience runat="server" ID="ucWorkExperiance" />
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="የስራ አፈፃፀም ውጤት" Visible="True" meta:resourcekey="TabPageResource18">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl18" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControl18Resource1">
                                <UC:Evaluation runat="server" ID="ucEvaluation" />
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="ዕድገት" Visible="True" meta:resourcekey="TabPageResource6">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl8" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControl8Resource1">
                                <UC:promotion ID="ucPromotion" runat="server" />
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="ዝውውር" meta:resourcekey="TabPageResource7">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl7" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControl7Resource1">
                                <UC:transfer ID="ucTransfer" runat="server" />
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="አገልግሎት ማራዘም" Visible="True" meta:resourcekey="TabPageResource8">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl6" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControl6Resource1">
                                <UC:ServiceExtension ID="serviceExtension" runat="server"></UC:ServiceExtension>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="አገልግሎት ሟቋረጥ" Visible="True" meta:resourcekey="TabPageResource9">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl10" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControl10Resource1">
                                <UC:Termination ID="ucServiceTermination" runat="server"></UC:Termination>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="የደንብ መተላለፍ ውሳኔ እና ቅሬታ ሰሚ" Visible="True" meta:resourcekey="TabPageResource10">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl9" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControl9Resource1">
                                <UC:Discipline ID="ucViolationdecsion" runat="server"></UC:Discipline>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="ጤና እና ደህንነት" Visible="True" meta:resourcekey="TabPageResource11">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl12" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControl12Resource1">
                                <UC:Medical ID="ucMedical" runat="server"></UC:Medical>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="የትምህርት ዕድል" Visible="True" meta:resourcekey="TabPageResource13">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl14" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControl14Resource1">
                                <UC:Scholarship ID="ucScholarShip" runat="server" />
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="የስልጣን ውክልና" Visible="True" meta:resourcekey="TabPageResource14">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl13" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControl13Resource1">
                                <UC:Delegation runat="server" ID="UCdelegation" />
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="ሰነድ" meta:resourcekey="TabPageResource15">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl15" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControl15Resource1">
                                <UC:Attachment ID="ucAttachment" runat="server"></UC:Attachment>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="ግምገማ" meta:resourcekey="TabPageResource16">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl16" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControl16Resource1">
                                <UC:Inspection ID="ucInspection" runat="server"></UC:Inspection>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <dx:TabPage Text="መልስ" meta:resourcekey="TabPageResource17">
                        <ContentCollection>
                            <dx:ContentControl ID="ContentControl17" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControl17Resource1">
                                <UC:Response ID="ucResponse" runat="server"></UC:Response>
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                </TabPages>
            </dx:ASPxPageControl>
        </dx:PanelContent>
    </PanelCollection>
</dx:ASPxCallbackPanel>
</asp:Content>

