﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TeamAndMembersl.ascx.cs" Inherits="Position_TeamAndMembersl" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>

<%@ Register Assembly="CUSTOR.Controls.WebBox" Namespace="CUSTOR.Controls.WebBox" TagPrefix="cc1" %>
<%@ Register Src="../Controls/ConfirmBox.ascx" TagName="ConfirmBox" TagPrefix="uc2" %>
<%@ Register Src="../Controls/Alert.ascx" TagName="Alert" TagPrefix="uc1" %>
<%@ Register Src="../Controls/ConfirmBox.ascx" TagName="ConfirmBoxHR" TagPrefix="uc6" %>

<uc1:Alert ID="Alert1" runat="server" />
<%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>



<uc1:Alert ID="Alert2" runat="server" />
<%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
<script type="text/javascript">
    // <![CDATA[
    var selectedEmployees = [];
    function DoNewClick(s, e) {
        pnlDataPosition.SetVisible(true);
        //btnSaveClient.SetEnabled(true);
        cbPositionType.PerformCallback('New');
    }

    function ShowPopupHRConfirmation() {
        popupControlHR.Show();
        btnYes.Focus();
    }

    function DoDeleteClick(s, e) {
        if (selectedTeamsEmployees.length > 0) {
            ShowPopupHRConfirmation();
            btnNewClient.SetEnabled(true);
            btnSaveClient.SetEnabled(false);
        }
        else {
            ShowError("እባክዎን ከቡድኑ ውስጥ መሰረዝ የሚፈልጓቸውን ሰራተኞች ይምረጡ!");
        }

    }

    function ShowPopupTeamConfirmation() {
        popupControlHR.Show();
        btnYes.Focus();
    }

    function DoTeamDeleteClick(s, e) {
        ShowPopupTeamConfirmation();
    }

    function DoNewTeamMembers(s, e) {
        btnSaveTeamMember.SetVisible(true);
        pnlTeamMembersDetail.SetVisible(true);
        EmployessGroupedToTeam.SetVisible(false);
        btnNewClient.SetVisible(true);
        GroupEmployess.PerformCallback('New');
    }

    function onSelectedUnGroupedEmployee(s, e) {
        s.GetSelectedFieldValues("PersonGuid", ReturnSelectedUnGroupedEmployees);
    }

    function ReturnSelectedUnGroupedEmployees(values) {
        selectedEmployees = [];
        for (var index = 0; index < values.length; index++) {
            selectedEmployees.push(values[index]);
        }
    }

    function DoSaveTeamMembers(s, e) {
        if (ASPxClientEdit.ValidateGroup('TeamMembers')) {
            if (selectedEmployees.length > 0) {
                GroupEmployess.PerformCallback('Save');
            }
            else {
                ShowError("እባክዎን መመዝገብ የሚፈልጉዋቸውን የንዑስ ክፍሎች ሰራተኞች ይምረጡ!");
            }
        }
    }

    function LoadEmployees(s, e) {
        GroupEmployess.PerformCallback('Search');
    }

    function DoShowClick() {
        cbPositionType.PerformCallback('Show');
    }

    function OnOrganizationForSearchChanged(s, e) {
        alert(CboOrganization.GetValue().toString());
        CboUnit.PerformCallback(CboOrganization.GetValue().toString());
    }

    function OnUnitsForSearchChanged() {
        CboSubUnit.PerformCallback(CboUnit.GetValue().toString());
    }

    function LoadTeamMembers() {
        grwGroupedEmployees.PerformCallback();
    }

    function ClosePopupHRConfirmation(result) {
        if (result) {
            GroupEmployess.PerformCallback('Delete');
        }
        else {
            return;
        }
        popupControlHR.Hide();
    }

    function ClosePopupTeamConfirmation(result) {
        if (result) {
            cbPositionType.PerformCallback('Delete');
        }
        popupControlHR.Hide();
    }

    function btnYes_Click(s, e) {
        var index = TeamOrganization.GetActiveTabIndex();
        if (index == 0) {
            ClosePopupTeamConfirmation(true);
        }
        else if (index == 1) {
            ClosePopupHRConfirmation(true);
        }
    }

    function btnNo_Click(s, e) {

        var index = TeamOrganization.GetActiveTabIndex();
        if (index == 0) {
            ClosePopupTeamConfirmation(false);
        }
        else if (index == 1) {
            ClosePopupHRConfirmation(false);
        }
    }

    function DoSearchPositionClick(s, e) {

        cbPositionType.PerformCallback('Search');
    }

    function DoSaveClick(s, e) {
        cbPositionType.PerformCallback('Save');
    }

    //===========================Grid===============
    var rowVisibleIndex;
    var strID;

    function grdOrganization_CustomButtonClick(s, e) {
        if (e.buttonID != 'Del' && e.buttonID != 'Edit') return;
        if (e.buttonID == 'Del') {
            //rowVisibleIndex = e.visibleIndex;
            //s.GetRowValues(e.visibleIndex, 'ID', ShowPopup);
            ShowError("You can not delete a position type, instead uncheck the 'Is Active' CheckBox");
        }
        if (e.buttonID == 'Edit') {
            pnlDataPosition.SetVisible(true);
            //DoToggle();
            grdPositionTypes.GetRowValues(e.visibleIndex, "TeamGuid", OnGetSelectedFieldValues);
        }
    }

    function OnEndCallback(s, e) {
        if (cbPositionType.cpStatus == "SUCCESS") {
            if (cbPositionType.cpAction == "new") {
                if (cbPositionType.cpMessage == '') {
                    return;
                }
            }

            if (cbPositionType.cpAction == "save") {
                pnlDataPosition.SetVisible(false);
                ShowSuccess(cbPositionType.cpMessage);
            }
        }
        else if (cbPositionType.cpStatus == "ERROR") {
            alert(cbPositionType.cpStatus);
            alert(cbPositionType.cpAction);
            if (cbPositionType.cpAction == "Delete") {
                ShowError(cbPositionType.cpMessage);
                pnlDataPosition.SetVisible(false);
                grdPositionTypes.UnselectRows();
            }
        }
        else if (cbPositionType.cpStatus == "INFO") {
            ShowAlert(cbPositionType.cpMessage);
        }
        cbPositionType.cpStatus = "";
        cbPositionType.cpAction = "";
        cbPositionType.cpMessage = "";
    }

    function GEOnEndCallback(s, e) {
        if (s.cpStatus == "SUCCESS") {
            if (s.cpAction == "save") {
                ShowSuccess(GroupEmployess.cpMessage);
                btnNewClient.SetEnabled(true);
                btnSaveTeamMember.SetEnabled(false);
            }
            if (s.cpAction == "delete") {
                ShowSuccess(GroupEmployess.cpMessage);
            }
        }
        else if (s.cpStatus == "ERROR") {
            ShowError(s.cpMessage);
        }
        s.cpStatus = "";
        s.cpAction = "";
        s.cpMessage = "";
    }

    function OnGetSelectedFieldValues(values) {
        cbPositionType.PerformCallback('Edit' + '|' + values);
    }

    function ShowPopup(rowId) {
        popupControl.Show();
        btnYes.Focus();
    }

    function DoRefreshClick(s, e) {
        btnSaveClient.SetEnabled(false);
        pnlDataPosition.SetVisible(false);
        cbPositionType.PerformCallback('Refresh');
    }

    function ClosePopup(result) {
        popupControl.Hide();
        if (result) {

            grdPositionTypes.GetRowValues(rowVisibleIndex, "PositionTypeGuid", OnGetSelectedDeleteField);
        }
    }

    function OnGetSelectedDeleteField(values) {

        cbPositionType.PerformCallback('Delete' + '|' + values);

    }

    function DeleteGridRow(visibleIndex) {
        var index = grdPositionTypes.GetFocusedRowIndex();
        grdPositionTypes.DeleteRow(index);
    }
    var selectedTeams = [];
    function onSelectedChanged(s, e) {
        s.GetSelectedFieldValues("Id", ReturnSelectedTeamValues);
    }

    function ReturnSelectedTeamValues(values) {
        selectedTeams = [];
        for (var index = 0; index < values.length; index++) {
            selectedTeams.push(values[index]);
        }
        if (selectedTeams.length > 0) {
            btnTeamDelete.SetEnabled(true);
        }
        else {
            btnTeamDelete.SetEnabled(false);
        }
    }

    function DoShowTeamMembersClick() {
        if (cboTeam.GetValue() != null) {
            grwGroupedEmployees.PerformCallback();
        }
        else {
            ShowError('እባክዎን የቡድን አባላትን ለመፈለግ መጀመሪያ ቡድኑን ይምረጡ!');
        }
    }
    function onTeamMembersEndCallBack(s, e) {
        if (s.cpStatus == "SUCCESS") {
            if (s.cpAction == "LoadTeamMembers") {
                btnSaveTeamMember.SetEnabled(false);
                pnlTeamMembersDetail.SetVisible(false);
                EmployessGroupedToTeam.SetVisible(true);
            }
        }
        else if (s.cpStatus == "ERROR") {
            if (s.cpAction == "NotLoadTeamMembers") {
                ShowError(s.cpMessage);
            }
        }
        s.cpStatus = "";
        s.cpAction = "";
        s.cpMessage = "";
    }

    var selectedTeamsEmployees = [];

    function onGroupedEmplyeesSelected(s, e) {
        s.GetSelectedFieldValues("Id", ReturnSelectedTeamEmployeesValues);
    }

    function ReturnSelectedTeamEmployeesValues(values) {
        selectedTeamsEmployees = [];
        for (var index = 0; index < values.length; index++) {
            selectedTeamsEmployees.push(values[index]);
        }

    }
</script>


<script type="text/javascript">
    function pageLoad(sender, args) {
        //btnDeleteClient.SetEnabled(false);
        btnSaveClient.SetEnabled(false);
        pnlDataPosition.SetVisible(false);

    }
</script>

<cc1:MessageBox runat="server" ID="Msg" meta:resourcekey="MsgResource1" />
<div>
    <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ClientInstanceName="TeamOrganization" ActiveTabIndex="1" Width="950px" Theme="PlasticBlue" meta:resourcekey="ASPxPageControl1Resource1">
        <TabPages>
            <dx:TabPage Text="ቡድን መመዝገቢያ" meta:resourcekey="TabPageResource1">
                <ContentCollection>
                    <dx:ContentControl ID="ContentControl1" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControl1Resource2">
                        <dx:ASPxCallbackPanel ID="cbPositionType" runat="server"
                            ClientInstanceName="cbPositionType" meta:resourceKey="cbPositionTypeResource1"
                            OnCallback="CallbackPanel_Callback" Width="960px">
                            <ClientSideEvents EndCallback="OnEndCallback" />
                            <PanelCollection>
                                <dx:PanelContent ID="PanelContent1" runat="server" meta:resourceKey="PanelContent2Resource1"
                                    SupportsDisabledAttribute="True">
                                    <table class="table-condensed">
                                        <tr>
                                            <td>
                                                <asp:Panel ID="pnlMenu1" runat="server" CssClass="RoundPanelToolbar" meta:resourceKey="pnlMenu0Resource1" Width="890px">
                                                    <table class="table-condensed">
                                                        <tr>
                                                            <td>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <dx:ASPxButton ID="btnNew" runat="server" AutoPostBack="False"
                                                                                ClientInstanceName="btnNewClient" Font-Names="Visual Geez Unicode"
                                                                                Font-Size="Small" HorizontalAlign="Center" meta:resourceKey="btnNewResource1"
                                                                                Text="አዲስ" Theme="Office2010Silver" VerticalAlign="Middle" Width="70px" CausesValidation="False">
                                                                                <ClientSideEvents Click="function(s, e) {
	                                                                                DoNewClick();
                                                                                }" />
                                                                                <Image Url="~/Controls/ToolbarImages/new.gif">
                                                                                </Image>
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxButton ID="btnSave" runat="server" AutoPostBack="False"
                                                                                ClientInstanceName="btnSaveClient" Font-Names="Visual Geez Unicode"
                                                                                Font-Size="Small" HorizontalAlign="Center" Enabled="False" meta:resourceKey="btnSaveResource1"
                                                                                Text="አስቀምጥ" Theme="Office2010Silver"
                                                                                VerticalAlign="Middle"
                                                                                Width="70px">
                                                                                <ClientSideEvents Click="function(s, e) {
	                                                                                DoSaveClick();
                                                                                }" />
                                                                                <Image Url="~/Controls/ToolbarImages/save.gif">
                                                                                </Image>
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxButton ID="btnShow" runat="server" AutoPostBack="False"
                                                                                ClientInstanceName="btnShowClient" Font-Names="Visual Geez Unicode"
                                                                                Font-Size="Small" HorizontalAlign="Center" meta:resourceKey="btnShowResource1"
                                                                                Text="አሳይ" Theme="Office2010Silver"
                                                                                ValidationSettings-ValidationGroup="saveTeam" VerticalAlign="Middle"
                                                                                Width="70px">
                                                                                <ClientSideEvents Click="function(s, e) {
	                                                                    DoShowClick();
                                                                    }" />
                                                                                <Image Url="~/Files/Images/action_settings.gif">
                                                                                </Image>
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxButton ID="btnTeamDelete" runat="server" AutoPostBack="False"
                                                                                ClientInstanceName="btnTeamDelete" ClientEnabled="false" Font-Names="Visual Geez Unicode"
                                                                                Font-Size="Small" HorizontalAlign="Center" Enabled="True" meta:resourceKey="btnDeleteResource1"
                                                                                Text="ሰርዝ" Theme="Office2010Silver"
                                                                                ValidationSettings-ValidationGroup="saveTeam" VerticalAlign="Middle"
                                                                                Width="70px">
                                                                                <ClientSideEvents Click="function(s, e) {
	                                                                                    DoTeamDeleteClick();
                                                                                    }" />
                                                                                <Image Url="~/Controls/ToolbarImages/delete.gif">
                                                                                </Image>
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxPanel ID="pnlData" runat="server" ClientInstanceName="pnlDataPosition"
                                                    meta:resourceKey="pnlDataResource1" Width="960px">
                                                    <PanelCollection>
                                                        <dx:PanelContent ID="PanelContent2" runat="server" meta:resourceKey="PanelContentResource1"
                                                            SupportsDisabledAttribute="True">
                                                            <table class="table-condensed">
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="የቡድን መጠሪያ(በክልሉ ቋንቋ)" meta:resourcekey="ASPxLabel4Resource1"></dx:ASPxLabel>
                                                                        <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*" meta:resourcekey="Label2Resource1"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxTextBox ID="txtTeamNameRegional" runat="server"
                                                                            meta:resourceKey="txtDescriptionAmResource1" Width="404px">
                                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                ValidationGroup="saveTeam">
                                                                                <RequiredField ErrorText="" IsRequired="True" />
                                                                            </ValidationSettings>
                                                                        </dx:ASPxTextBox>
                                                                    </td>

                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="የቡድን መጠሪያ (English)" meta:resourcekey="ASPxLabel3Resource1">
                                                                        </dx:ASPxLabel>
                                                                        <asp:Label ID="Label3" runat="server" ForeColor="Red"
                                                                            meta:resourceKey="Label1Resource1" Text="*"></asp:Label>

                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxTextBox ID="txtTeamName" runat="server" Width="404px" meta:resourcekey="txtTeamNameResource1">
                                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                ValidationGroup="saveTeam">
                                                                                <RequiredField ErrorText="" IsRequired="True" />
                                                                            </ValidationSettings>
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: right">&nbsp;</td>
                                                                    <td>&nbsp;</td>

                                                                </tr>
                                                            </table>
                                                        </dx:PanelContent>
                                                    </PanelCollection>
                                                </dx:ASPxPanel>
                                                <table class="table-condensed">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxGridView ID="gvwOrganization" runat="server"
                                                                AutoGenerateColumns="False" ClientInstanceName="grdPositionTypes"
                                                                EnableTheming="True" Font-Names="Visual Geez Unicode" Font-Size="Small"
                                                                KeyFieldName="TeamGuid" meta:resourceKey="gvwOrganizationResource1"
                                                                Theme="Office2010Silver" OnPageIndexChanged="gvwOrganization_OnPageIndexChanged"
                                                                Width="100%">
                                                                <ClientSideEvents SelectionChanged="onSelectedChanged" CustomButtonClick="grdOrganization_CustomButtonClick" />
                                                                <Columns>
                                                                    <dx:GridViewCommandColumn ShowInCustomizationForm="True"
                                                                        ShowSelectCheckbox="True" VisibleIndex="0" Width="5%">
                                                                    </dx:GridViewCommandColumn>
                                                                    <dx:GridViewDataTextColumn Caption="ID" FieldName="TeamGuid"
                                                                        meta:resourceKey="GridViewDataTextColumnResource1"
                                                                        ShowInCustomizationForm="True" Visible="False" VisibleIndex="1">
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="የቡድን መጠሪያ(አማርኛ)" FieldName="TeamNameAm"
                                                                        meta:resourceKey="GridViewDataTextColumnResource2"
                                                                        ShowInCustomizationForm="True" VisibleIndex="2">
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="የቡድን መጠሪያ(English)"
                                                                        FieldName="TeamName" meta:resourceKey="GridViewDataTextColumnResource3"
                                                                        ShowInCustomizationForm="True" VisibleIndex="3">
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewCommandColumn
                                                                        ShowInCustomizationForm="True" VisibleIndex="4" Width="100px">
                                                                        <CustomButtons>
                                                                            <dx:GridViewCommandColumnCustomButton ID="Edit"
                                                                                meta:resourceKey="GridViewCommandColumnCustomButtonResource1" Text="አርም">
                                                                            </dx:GridViewCommandColumnCustomButton>
                                                                        </CustomButtons>
                                                                        <HeaderTemplate></HeaderTemplate>
                                                                    </dx:GridViewCommandColumn>

                                                                </Columns>
                                                                <SettingsBehavior AllowFocusedRow="True" />
                                                            </dx:ASPxGridView>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxCallbackPanel>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="የቡድን አባላት መመደቢያ" meta:resourcekey="TabPageResource2">
                <ContentCollection>
                    <dx:ContentControl ID="ContentControl2" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="ContentControl2Resource1">

                        <dx:ASPxCallbackPanel ID="GroupEmployess" runat="server" ClientInstanceName="GroupEmployess" OnCallback="OnGroupEmployee_Callback" Width="900px" meta:resourcekey="GroupEmployessResource1">
                            <ClientSideEvents EndCallback="GEOnEndCallback" />
                            <PanelCollection>
                                <dx:PanelContent ID="PanelContent3" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="PanelContent3Resource1">
                                    <asp:Panel ID="pnlMenu0" runat="server" CssClass="RoundPanelToolbar" meta:resourcekey="pnlMenu0Resource2">
                                        <table class="table-condensed">
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxButton ID="btnNewTeamMember" runat="server" AutoPostBack="False"
                                                                    ClientInstanceName="btnNewClient" Font-Names="Visual Geez Unicode"
                                                                    Font-Size="Small" HorizontalAlign="Center"
                                                                    Image-Url="~/Controls/ToolbarImages/new.gif" Text="አዲስ"
                                                                    Theme="Office2010Silver" VerticalAlign="Middle"
                                                                    CausesValidation="False" meta:resourcekey="btnNewTeamMemberResource1">
                                                                    <Image Url="~/Controls/ToolbarImages/new.gif">
                                                                    </Image>
                                                                    <ClientSideEvents Click="function(s, e) { DoNewTeamMembers(s,e); }"></ClientSideEvents>
                                                                </dx:ASPxButton>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxButton ID="btnSaveTeamMember" runat="server" AutoPostBack="False"
                                                                    Font-Names="Visual Geez Unicode" ClientInstanceName="btnSaveTeamMember"
                                                                    Font-Size="Small" HorizontalAlign="Center"
                                                                    Image-Url="~/Controls/ToolbarImages/save.gif" ImagePosition="Left" Text="መድብ"
                                                                    Theme="Office2010Silver" VerticalAlign="Middle" meta:resourcekey="btnSaveTeamMemberResource1">
                                                                    <ClientSideEvents Click="function(s, e) { DoSaveTeamMembers(s,e); }"></ClientSideEvents>
                                                                    <Image Url="~/Controls/ToolbarImages/save.gif"></Image>

                                                                </dx:ASPxButton>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxButton ID="btnDelete" runat="server" AutoPostBack="False" ClientInstanceName="btnDeleteClient"
                                                                    Visible="true" EnableClientSideAPI="True" Font-Names="Visual Geez Unicode"
                                                                    Font-Size="Small" HorizontalAlign="Center" Image-Url="~/Controls/ToolbarImages/delete.gif" Text="ሰርዝ"
                                                                    Theme="Office2010Silver" VerticalAlign="Middle" CausesValidation="False" meta:resourcekey="btnDeleteResource2">
                                                                    <ClientSideEvents Click="function(s, e) {
	                                                                     DoDeleteClick();
                                                                     }" />
                                                                    <Image Url="~/Controls/ToolbarImages/delete.gif">
                                                                    </Image>
                                                                </dx:ASPxButton>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxComboBox ID="cboTeam" runat="server" AutoPostBack="false" NullText="ይምረጡ"
                                                                    ClientInstanceName="cboTeam" Width="200px" meta:resourcekey="ddlTeamResource1">
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxButton ID="cmdShowTeamMembers" runat="server" AutoPostBack="False" ClientInstanceName="btnDeleteClient"
                                                                    Visible="true" EnableClientSideAPI="True" Font-Names="Visual Geez Unicode"
                                                                    Font-Size="Small" HorizontalAlign="Center" Image-Url="~/Controls/ToolbarImages/delete.gif" Text="ሰርዝ"
                                                                    Theme="Office2010Silver" VerticalAlign="Middle" CausesValidation="False" meta:resourcekey="btnShowResource1">
                                                                    <ClientSideEvents Click="function(s, e) {
	                                                                                     DoShowTeamMembersClick();
                                                                                     }" />
                                                                    <Image Url="~/Files/Images/action_settings.gif">
                                                                    </Image>
                                                                </dx:ASPxButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <dx:ASPxPanel ID="pnlTeamMembersDetail" runat="server" ClientInstanceName="pnlTeamMembersDetail">
                                            <PanelCollection>
                                                <dx:PanelContent>
                                                    <table class="table-condensed">
                                                        <tr>
                                                            <td align="left">
                                                                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="ተቋም" Visible="true" meta:resourcekey="ASPxLabel5Resource1">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td align="left">
                                                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text=" የስራ ክፍል" meta:resourcekey="ASPxLabel1Resource1">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxComboBox ID="CboOrganization" runat="server" Width="400px" Visible="true" ClientInstanceName="CboOrganization"
                                                                    IncrementalFilteringMode="Contains" NullText="ይምረጡ" meta:resourcekey="CboOrganizationResource1">
                                                                    <ClientSideEvents SelectedIndexChanged="OnOrganizationForSearchChanged" />
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxComboBox ID="CboUnit" runat="server" ClientInstanceName="CboUnit" OnCallback="CboUnit_OnCallback" Width="400px"
                                                                    IncrementalFilteringMode="Contains" NullText="ይምረጡ" meta:resourcekey="CboUnitResource1">
                                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) { OnUnitsForSearchChanged(s); }" />
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td style="text-align: left">
                                                                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="ንኡስ የስራ ክፍል" meta:resourcekey="ASPxLabel6Resource1">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="ቡድን" meta:resourcekey="ASPxLabel2Resource1">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxComboBox ID="CboSubUnit" runat="server" ClientInstanceName="CboSubUnit" OnCallback="CboSubUnit_OnCallback" Width="400px" meta:resourcekey="CboSubUnitResource1">
                                                                    <ClientSideEvents SelectedIndexChanged="LoadEmployees" />
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxComboBox ID="ddlTeam" runat="server" AutoPostBack="false" Width="400px" meta:resourcekey="ddlTeamResource1">
                                                                    <ValidationSettings ErrorDisplayMode="Text" SetFocusOnError="True" Display="Dynamic" ErrorTextPosition="Bottom" ValidationGroup="TeamMembers">
                                                                        <RequiredField ErrorText="ባዶ መሆን አይችልም።" IsRequired="True" />
                                                                        <ErrorFrameStyle CssClass="erroStyle"></ErrorFrameStyle>
                                                                    </ValidationSettings>
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 50%">
                                                                <dx:ASPxGridView ID="grwUnGroupedEmployees" runat="server" ClientInstanceName="grwUnGroupedEmployees" OnPageIndexChanged="grwUnGroupedEmployees_OnPageIndexChanged"
                                                                    AutoGenerateColumns="False" KeyFieldName="MainGuid" Theme="Office2010Silver" Width="100%" meta:resourcekey="grwUnGroupedEmployeesResource1">
                                                                    <Columns>
                                                                        <dx:GridViewDataTextColumn FieldName="MainGuid" Visible="false" ShowInCustomizationForm="True" Width="70%" meta:resourcekey="GridViewDataTextColumnResource4">
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewCommandColumn ShowInCustomizationForm="True" ShowSelectCheckbox="True" VisibleIndex="0">
                                                                        </dx:GridViewCommandColumn>
                                                                        <dx:GridViewDataTextColumn Caption="የሰራተኛ ሙሉ ስም" FieldName="FullName" ShowInCustomizationForm="True" VisibleIndex="1" Width="70%" meta:resourcekey="GridViewDataTextColumnResource4">
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ሃላፊነት" FieldName="PositionTeam" ShowInCustomizationForm="True" VisibleIndex="2" Width="25%" meta:resourcekey="GridViewDataTextColumnResource5">
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxComboBox ID="ddlTeamPosition" runat="server" SelectedIndex="0" Width="100%" meta:resourcekey="ddlTeamPositionResource1">
                                                                                    <Items>
                                                                                        <dx:ListEditItem Selected="True" Text="አባል" Value="0" meta:resourcekey="ListEditItemResource1" />
                                                                                        <dx:ListEditItem Text="ቡድን መሪ" Value="1" meta:resourcekey="ListEditItemResource2" />
                                                                                    </Items>
                                                                                </dx:ASPxComboBox>
                                                                            </DataItemTemplate>
                                                                        </dx:GridViewDataTextColumn>
                                                                    </Columns>
                                                                    <Settings ShowTitlePanel="True" />
                                                                    <SettingsText Title="በቡድን ያልተመደቡ የክፍሉ አባላት" />
                                                                    <ClientSideEvents SelectionChanged="onSelectedUnGroupedEmployee" />
                                                                </dx:ASPxGridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxPanel>
                                        <dx:ASPxPanel ID="EmployessGroupedToTeam" ClientInstanceName="EmployessGroupedToTeam" ClientVisible="false" runat="server">
                                            <PanelCollection>
                                                <dx:PanelContent>
                                                    <div class="col-md-8">
                                                        <div class="row">
                                                            <dx:ASPxGridView ID="grwGroupedEmployees" ClientInstanceName="grwGroupedEmployees" runat="server" AutoGenerateColumns="False" KeyFieldName="TeamMembersGuid"
                                                                OnCustomColumnDisplayText="grwGroupedEmployees_CustomColumnDisplayText" OnPageIndexChanged="grwGroupedEmployees_OnPageIndexChanged" Theme="Office2010Silver" Width="100%"
                                                                OnCustomCallback="grwGroupedEmployees_OnCustomCallback" meta:resourcekey="grwGroupedEmployeesResource1">
                                                                <ClientSideEvents SelectionChanged="onGroupedEmplyeesSelected" EndCallback="onTeamMembersEndCallBack" />
                                                                <Columns>
                                                                    <dx:GridViewDataTextColumn Caption="የሰራተኛ ሙሉ ስም" FieldName="TeamMembersGuid" Visible="false"
                                                                        VisibleIndex="0" Width="70%" meta:resourcekey="GridViewDataTextColumnResource6">
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewCommandColumn ShowInCustomizationForm="True"
                                                                        ShowSelectCheckbox="True" VisibleIndex="0" Width="5%">
                                                                    </dx:GridViewCommandColumn>
                                                                    <dx:GridViewDataTextColumn Caption="የሰራተኛ ሙሉ ስም" FieldName="FullName"
                                                                        ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource6">
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="ሃላፊነት" FieldName="PositionInTeam"
                                                                        ShowInCustomizationForm="True" VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource7">
                                                                    </dx:GridViewDataTextColumn>
                                                                </Columns>
                                                                <Settings ShowTitlePanel="True" />
                                                                <SettingsText Title="በቡድኑ የተመደቡ ሰራተኞች" />
                                                            </dx:ASPxGridView>
                                                        </div>
                                                    </div>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxPanel>
                                    </asp:Panel>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxCallbackPanel>

                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
    </dx:ASPxPageControl>
    <dxpop:ASPxPopupControl ID="ASPxPopupControl2" runat="server" ClientInstanceName="popupControlHR" Font-Names="Visual Geez Unicode" Font-Size="Small" HeaderText="Delete Confirmation" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" meta:resourcekey="ASPxPopupControl1Resource1">
        <ContentCollection>
            <dxpop:PopupControlContentControl ID="PopupControlContentControl2" runat="server" meta:resourcekey="PopupControlContentControl1Resource1">
                <uc6:ConfirmBoxHR ID="ConfirmBoxHR" runat="server" />
            </dxpop:PopupControlContentControl>
        </ContentCollection>
    </dxpop:ASPxPopupControl>
</div>

