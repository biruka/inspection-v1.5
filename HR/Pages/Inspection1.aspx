﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeFile="Inspection1.aspx.cs" Inherits="UI.Pages.Inspection1" Culture="auto" meta:resourcekey="PageResource19" UICulture="auto" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>




<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .erroStyle {
            font-size: smaller;
        }
    </style>
    <script type="text/javascript" language="javascript">
        //$(document).ready(function () {
        //    $("#txtCalFrom").calendarsPicker({
        //        calendar:  $.calendars.instance('ethiopian', 'or'),
        //        todayText: '<img src="../../Images/clear.gif">',
        //        prevText: '< M',
        //        nextText: 'M >',
        //        clearText: '<img src="../../Images/clear.gif">',
        //        closeText: '<img src="../../Images/close.gif">',
        //        commandsAsDateFormat: true,
        //        dayStatus: ''
        //    });
        //});

    </script>
    <script type="text/javascript">
        // <![CDATA[
        var visibileIndex = 0;
        var emp_status;

        function DoSearchClick(s, e) {
            var isValid = ASPxClientEdit.ValidateGroup('HrActivitySearch');
            if (isValid) {
                DvgvwPerson.PerformCallback('Search');
            }
        }

        function DvgvwPerson_CustomButtonClick(s, e) {
            if (e.buttonID = 'Inspect') {
                visibileIndex = e.visibleIndex;
                emp_status = DvgvwPerson.GetRowValues(e.visibleIndex, "emp_status");
                DvgvwPerson.GetRowValues(e.visibleIndex, "MainGuid", OnGetSelectedFieldValues);
            } else {
                return;
            }
        }

        function OnGetSelectedFieldValues(values) {
            cbdata.PerformCallback('Inspect' + '|' + values + '|' + visibileIndex + '|' + cboHRActivity.GetValue() + '|' + emp_status);
        }

        function OnEndCallback(s, e) {
        }

        function DvgvwPersonEndCallBack(s, e) {

        }


    </script>
    <%--<script src="../Scripts/jquery-1.7.1.js" type="text/javascript"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceHolder" runat="server">
    <dx:ASPxCallbackPanel ID="cbdata" runat="server" OnCallback="cbdata_OnCallback" ClientInstanceName="cbdata" Width="100%" meta:resourcekey="cbdataResource1">
        <ClientSideEvents EndCallback="OnEndCallback"></ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="PanelContent1Resource1">
                <div class="RoundPanel">
                    <table class="auto-style26">
                        <tr>
                            <td>
                                <div>
                                    <table align="center">
                                        <tr style="border-bottom-width: 0px">
                                            <td style="text-align: center">
                                                <asp:Panel ID="pnlMsg" runat="server" CssClass="messageWarning" EnableViewState="False"
                                                    Height="100%" Visible="False" Width="100%" BorderColor="#99CCFF" BackColor="#CCFF33" meta:resourcekey="pnlMsgResource1">
                                                    <asp:Label ID="lblPersonMsg" runat="server" CssClass="Message" Font-Bold="True" ForeColor="Red" meta:resourcekey="lblPersonMsgResource1"></asp:Label>
                                                </asp:Panel>
                                                <%--<cc2:MessageBox ID="MessageBox1" runat="server" meta:resourcekey="MessageBox1Resource1"></cc2:MessageBox>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="container RoundPanelWhite" style="width: 1000px; margin: 20px">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-4  offset-md-3">
                                                                <dx:ASPxLabel ID="lblOrganization" runat="server" Text="ተቋም: " meta:resourcekey="lblOrganizationResource1" Font-Size="Small">
                                                                </dx:ASPxLabel>
                                                            </div>
                                                            <div class="col-md-4" style="margin-left: -190px">
                                                                <dx:ASPxComboBox ID="ComboOrg" ClientInstanceName="ComboOrg" runat="server" EnableIncrementalFiltering="True" IncrementalFilteringMode="Contains" meta:resourcekey="ComboOrgResource1">
                                                                    <ValidationSettings SetFocusOnError="True" ErrorTextPosition="Bottom" Display="Dynamic" ValidationGroup="HrActivitySearch" ErrorDisplayMode="Text">
                                                                        <RequiredField  IsRequired="True" />
                                                                        <ErrorFrameStyle CssClass="erroStyle"></ErrorFrameStyle>
                                                                    </ValidationSettings>
                                                                </dx:ASPxComboBox>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4  offset-md-3">
                                                                <dx:ASPxLabel ID="lbldoctype" runat="server" Text="የሰው ሀብት ክንውን:" DropDownStyle="DropDown" EnableIncrementalFiltering="True" IncrementalFilteringMode="StartsWith" meta:resourcekey="lbldoctypeResource1" Font-Size="Small">
                                                                </dx:ASPxLabel>
                                                            </div>
                                                            <div class="col-md-4" style="margin-left: -190px">
                                                                <dx:ASPxComboBox ID="cboHRActivity" runat="server"  ClientInstanceName="cboHRActivity" IncrementalFilteringMode="StartsWith" meta:resourcekey="ComboDocTypeResource1">
                                                                    <ValidationSettings SetFocusOnError="True" ErrorTextPosition="Bottom" ErrorDisplayMode="Text" ValidationGroup="HrActivitySearch">
                                                                        <RequiredField  IsRequired="True" />
                                                                        <ErrorFrameStyle CssClass="erroStyle"></ErrorFrameStyle>
                                                                    </ValidationSettings>
                                                                </dx:ASPxComboBox>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-1  offset-md-3">
                                                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="ቀን:" meta:resourcekey="lbldateResource1" Font-Size="Small">
                                                                </dx:ASPxLabel>
                                                            </div>
                                                            <div class="row col-md-8">
                                                                <div class="col-md-4" style="margin-left: 60px">
                                                                    <cdp:CUSTORDatePicker ID="dpFrom" runat="server" TextCssClass="" Width="110px" meta:resourcekey="dpFromResource1" />
                                                                </div>
                                                                <div class="col-md-4" style="margin-left: -65px">
                                                                    <cdp:CUSTORDatePicker ID="dpTo" runat="server" Width="110px" meta:resourcekey="dpToResource1" SelectedDate="" TextCssClass="" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1" style="margin-left: -285px">
                                                                <dx:ASPxButton ID="btnsearch" runat="server" Theme="DevEx" class="btn btn-primary" CausesValidation="true"
                                                                    Text="ፈልግ" AutoPostBack="false" HorizontalAlign="Center" Image-Url="~/images/Toolbar/search.gif"
                                                                    ClientInstanceName="btnsearch" Font-Names="Visual Geez Unicode" EnableClientSideAPI="True"
                                                                    meta:resourcekey="btnsearchResource1">
                                                                    <ClientSideEvents Click="function(s, e) { DoSearchClick(); }" />
                                                                    <Image Url="~/images/Toolbar/search.gif">
                                                                    </Image>
                                                                </dx:ASPxButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" border="0">
                                                    <tr>
                                                        <td width="88%" class="droplink">
                                                            <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="#" class="droplink"></a></font>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <dx:ASPxPanel ID="PnlRecuirtment1" runat="server" Width="100%" meta:resourcekey="PnlRecuirtment1Resource1">
                                                                <PanelCollection>
                                                                    <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="PanelContent2Resource1">
                                                                        <%--<dx:ASPxPanel ID="Rpnlfilter" runat="server" Width="306px" Height="126px" HeaderText="የላቀ ወንፊት"
                                                                            Visible="False" meta:resourcekey="RpnlfilterResource1">
                                                                            <PanelCollection>
                                                                                <dx:PanelContent ID="PanelContent3" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="PanelContent3Resource1">
                                                                                    <table class="style2">
                                                                                        <tr>
                                                                                            <td colspan="2">
                                                                                                <dx:ASPxFilterControl ID="ASPxFilterControl1" runat="server"
                                                                                                    ClientInstanceName="filter" meta:resourcekey="ASPxFilterControl1Resource1">
                                                                                                    <Columns>
                                                                                                        <dx:FilterControlColumn DisplayName="ሙሉ ስም" PropertyName="FullName">
                                                                                                        </dx:FilterControlColumn>
                                                                                                        <dx:FilterControlColumn DisplayName="ፆታ" PropertyName="Gender">
                                                                                                        </dx:FilterControlColumn>
                                                                                                        <dx:FilterControlColumn DisplayName="የትውልድ ዓመት" PropertyName="Birth_date">
                                                                                                        </dx:FilterControlColumn>
                                                                                                        <dx:FilterControlColumn DisplayName="የስራ መደብ" PropertyName="job_title">
                                                                                                        </dx:FilterControlColumn>
                                                                                                        <dx:FilterControlColumn DisplayName="የቅጥር ቀን" PropertyName="emp_date">
                                                                                                        </dx:FilterControlColumn>
                                                                                                        <dx:FilterControlColumn DisplayName="ደረጃ" PropertyName="AmGrade">
                                                                                                        </dx:FilterControlColumn>
                                                                                                        <dx:FilterControlColumn DisplayName="ደመወዝ" PropertyName="salary">
                                                                                                        </dx:FilterControlColumn>
                                                                                                        <dx:FilterControlColumn DisplayName="ንኡስ መ/ቤት" PropertyName="Unitdesc">
                                                                                                        </dx:FilterControlColumn>
                                                                                                        <dx:FilterControlColumn ColumnType="String" DisplayName="የግምገማ ሁኔታ" PropertyName="inspectstatus">
                                                                                                        </dx:FilterControlColumn>
                                                                                                    </Columns>
                                                                                                    <ClientSideEvents Applied="function(s, e) { DvgvwPerson.ApplyFilter(e.filterExpression);}" />
                                                                                                    <ClientSideEvents Applied="function(s, e) { DvgvwPerson.ApplyFilter(e.filterExpression);}"></ClientSideEvents>
                                                                                                </dx:ASPxFilterControl>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="right">
                                                                                                <dx:ASPxButton ID="btnApply" runat="server" AutoPostBack="False" Text="ተግብር" Width="70px"
                                                                                                    UseSubmitBehavior="False" meta:resourcekey="btnApplyResource1"
                                                                                                    Theme="DevEx">
                                                                                                    <ClientSideEvents Click="function() { filter.Apply(); }" />
                                                                                                    <ClientSideEvents Click="function() { filter.Apply(); }"></ClientSideEvents>
                                                                                                </dx:ASPxButton>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <dx:ASPxButton ID="btnCancel" runat="server" Text="አፅዳ" AutoPostBack="False" UseSubmitBehavior="False"
                                                                                                    Width="70px" Theme="DevEx" meta:resourcekey="btnCancelResource1">
                                                                                                    <ClientSideEvents Click="function() { filter.Reset(); filter.Apply();  }" />
                                                                                                    <ClientSideEvents Click="function() { filter.Reset(); filter.Apply();  }"></ClientSideEvents>
                                                                                                </dx:ASPxButton>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </dx:PanelContent>
                                                                            </PanelCollection>
                                                                        </dx:ASPxPanel>--%>

                                                                        <dx:ASPxGridView ID="DvgvwPerson" runat="server" Width="95%"
                                                                            AutoGenerateColumns="False" KeyFieldName="MainGuid" ClientInstanceName="DvgvwPerson"
                                                                            OnCustomColumnDisplayText="DvgvwPerson_CustomColumnDisplayText" OnPageIndexChanged="DvgvwPerson_OnPageIndexChanged"
                                                                            OnCustomCallback="DvgvwPerson_CustomCallback" OnHtmlRowCreated="DvgvwPerson_HtmlRowCreated"
                                                                            OnBeforePerformDataSelect="DvgvwPerson_BeforePerformDataSelect"
                                                                            OnHtmlRowPrepared="DvgvwPerson_HtmlRowPrepared"
                                                                            OnSelectionChanged="DvgvwPerson_OnSelectionChanged" OnCustomButtonInitialize="DvgvwPerson_CustomButtonInitialize"
                                                                            Theme="Office2010Silver" meta:resourcekey="DvgvwPersonResource1">
                                                                            <ClientSideEvents EndCallback="DvgvwPersonEndCallBack" CustomButtonClick="DvgvwPerson_CustomButtonClick" />
                                                                            <Columns>
                                                                                <dx:GridViewDataTextColumn FieldName="FullName" ShowInCustomizationForm="True" Caption="ሙሉ ስም" Width="150px"
                                                                                    VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource1">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="Gender" ShowInCustomizationForm="True" Caption="ጾታ" Width="10px"
                                                                                    VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource2">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="birth_date" ShowInCustomizationForm="True" Width="100px"
                                                                                    Caption="የትውልድ ዘመን" VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource3">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="Typeofemployment" ShowInCustomizationForm="True" Width="20px"
                                                                                    Caption="የቅጥር አይነት" VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnResource4">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="position_code" ShowInCustomizationForm="True"
                                                                                    Caption="መደብ መ. ቁጥር" VisibleIndex="5" meta:resourcekey="GridViewDataTextColumnResource5">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="job_title" ShowInCustomizationForm="True" VisibleIndex="6" Width="150px"
                                                                                    Caption="የስራ መደብ" meta:resourcekey="GridViewDataTextColumnResource6">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="AmGrade" ShowInCustomizationForm="True" Caption="ደረጃ" Width="50px"
                                                                                    VisibleIndex="7" meta:resourcekey="GridViewDataTextColumnResource7">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn Caption="ደመወዝ" FieldName="salary" ShowInCustomizationForm="True" Width="50px"
                                                                                    VisibleIndex="8" meta:resourcekey="GridViewDataTextColumnResource8">
                                                                                    <PropertiesTextEdit DisplayFormatString="#,00.00">
                                                                                    </PropertiesTextEdit>
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="emp_date" ShowInCustomizationForm="True" Caption="የቅጥር ዘመን" Width="100px"
                                                                                    VisibleIndex="4" meta:resourcekey="GridViewDataTextColumnResource9">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn Caption="የሥራ ክፍል" FieldName="Unitdesc" ShowInCustomizationForm="True" Width="150px"
                                                                                    VisibleIndex="9" meta:resourcekey="GridViewDataTextColumnResource10">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn Caption="የግምገማ ሁኔታ" ShowInCustomizationForm="True" Width="15px"
                                                                                    VisibleIndex="10" FieldName="inspectstatus" meta:resourcekey="GridViewDataTextColumnResource11">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn
                                                                                    ShowInCustomizationForm="True" VisibleIndex="11" Caption="የመጨረሻ ግምገማ ውሳኔ" Width="15px"
                                                                                    FieldName="InspectionDecision"
                                                                                    meta:resourcekey="GridViewDataTextColumnResource12">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataCheckColumn
                                                                                    ShowInCustomizationForm="True" VisibleIndex="12" Visible="true"
                                                                                    FieldName="Approved" meta:resourcekey="GridViewDataTextColumnResource14">
                                                                                </dx:GridViewDataCheckColumn>
                                                                                <dx:GridViewCommandColumn  ShowInCustomizationForm="True" VisibleIndex="13" Width="100px">
                                                                                    <CustomButtons>
                                                                                        <dx:GridViewCommandColumnCustomButton ID="Inspect"
                                                                                            Text="ገምግም" meta:resourcekey="inspect">
                                                                                        </dx:GridViewCommandColumnCustomButton>
                                                                                    </CustomButtons>
                                                                                </dx:GridViewCommandColumn>
                                                                                <dx:GridViewDataTextColumn
                                                                                    ShowInCustomizationForm="True" VisibleIndex="14" Caption="emp_status" Visible="false"
                                                                                    FieldName="emp_status" meta:resourcekey="GridViewDataTextColumnResource12">
                                                                                </dx:GridViewDataTextColumn>

                                                                            </Columns>
                                                                            <Templates>
                                                                                <DetailRow>
                                                                                </DetailRow>
                                                                            </Templates>
                                                                        </dx:ASPxGridView>
                                                                    </dx:PanelContent>
                                                                </PanelCollection>
                                                            </dx:ASPxPanel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="18" class="droplink">
                                                            <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="#" class="droplink"></a></font>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:HiddenField ID="visibleindx1" runat="server" />
                                                <asp:HiddenField ID="HidUserid" runat="server" />
                                                <asp:HiddenField ID="HidlLanguage" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
