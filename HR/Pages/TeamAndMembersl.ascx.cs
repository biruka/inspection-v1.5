﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Threading;
using CUSTOR;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using DevExpress.Web;
using CUSTOR.Bussiness;



public partial class Position_TeamAndMembersl : System.Web.UI.UserControl
{
    int SNo = 0;
    protected void Page_Load(object sender, EventArgs e)
    {

        tblTeamBussiness objTeam = new tblTeamBussiness();
        if (!IsPostBack)
        {
            ddlTeam.ValueField = "TeamGuid";
            ddlTeam.TextField = "TeamNameAm";
            ddlTeam.DataSource = objTeam.GettblTeams();
            ddlTeam.DataBind();
            cboTeam.ValueField = "TeamGuid";
            cboTeam.TextField = "TeamNameAm";
            cboTeam.DataSource = objTeam.GettblTeams();
            cboTeam.DataBind();
            FillOrganization(CboOrganization);
        }
    }

    private void FillOrganization(DevExpress.Web.ASPxComboBox cboName)
    {
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        string strSQL = @"Select *  from Organization where isactive=1 and OrgGuid='" + p.Organization.GUID + "' ORDER BY  EventDatetime Asc";
        if (p.Organization.LanguageID == 1)
        {
            CCommon.FillAspxComboFromDB(cboName, strSQL, "DescriptionAm", "OrgGuid");
        }
        else if (p.Organization.LanguageID == 3)
        {
            CCommon.FillAspxComboFromDB(cboName, strSQL, "DescriptionTig", "OrgGuid");
        }
        else if (p.Organization.LanguageID == 4)
        {
            CCommon.FillAspxComboFromDB(cboName, strSQL, "DescriptionAf", "OrgGuid");
        }
        else if (p.Organization.LanguageID == 5)
        {
            CCommon.FillAspxComboFromDB(cboName, strSQL, "DescriptionSom", "OrgGuid");
        }
        CboOrganization.SelectedIndex = 0;
        CboOrganization.Value = null;
    }
 
    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
         String unitCode=  System.Configuration.ConfigurationManager.AppSettings.Get("HRStudyUnit");
    }

    protected void NewTeamMember()
    {
        btnSaveTeamMember.Enabled = true;
        FillOrganization(CboOrganization);
        CboUnit.Items.Clear();
        CboUnit.Value = null;
        CboSubUnit.Items.Clear();
        CboSubUnit.Value = null;
        grwUnGroupedEmployees.DataSource = null;
        grwUnGroupedEmployees.DataBind();
    }

    protected bool CheckMemberIsAssigned()
    {
        bool existed = false;
        tblTeamMembersBussiness objTeamMembersBLL = new tblTeamMembersBussiness();
        for (int i = 0; i < grwUnGroupedEmployees.VisibleRowCount; i++)
        {
            if (grwUnGroupedEmployees.Selection.IsRowSelected(i))
            {
                if (objTeamMembersBLL.Exists(Guid.Parse(ddlTeam.Value.ToString()),
                                             Guid.Parse(grwUnGroupedEmployees.GetRowValues(i, "MainGuid").ToString())))
                {
                    existed = true;
                    break;
                }
            }
        }
        return existed;
    }

    protected void SaveTeamMember()
    {
        try
        {
            tblTeamMembers objTeamMembersBO = new tblTeamMembers();
            tblTeamMembersBussiness objTeamMembersBLL = new tblTeamMembersBussiness();
            if ((!ddlTeam.Value.Equals("-1") && !ddlTeam.Value.Equals("null")))
            {
                //Check whether the selected employess are already assigned to the teams
                if (!CheckMemberIsAssigned())
                {
                    int count = 0;
                    for (int i = 0; i < grwUnGroupedEmployees.VisibleRowCount; i++)
                    {
                        if (grwUnGroupedEmployees.Selection.IsRowSelected(i))
                        {
                            ASPxComboBox ddlTeamPosition =grwUnGroupedEmployees.FindRowCellTemplateControl(i,grwUnGroupedEmployees.Columns["TeamPosition"] as GridViewDataColumn,"ddlTeamPosition") as ASPxComboBox;
                            objTeamMembersBO.PositionInTeam = ddlTeamPosition.Value.ToString();
                            objTeamMembersBO.TeamGuid = Guid.Parse(ddlTeam.Value.ToString());
                            objTeamMembersBO.TeamMembersGuid = Guid.NewGuid();
                            objTeamMembersBO.PersonGuid = Guid.Parse(grwUnGroupedEmployees.GetRowValues(i, "MainGuid").ToString());
                            objTeamMembersBO.UserName = Session["ProfileFullName"].ToString();
                            objTeamMembersBO.UpdatedUsername = Session["ProfileFullName"].ToString();
                            objTeamMembersBO.UpdatedEventDatetime = DateTime.Now.Date;
                            objTeamMembersBO.EventDatetime = DateTime.Now.Date;
                            if(objTeamMembersBLL.InserttblTeamMembers(objTeamMembersBO))
                            {
                                count++; 
                            }
                        }
                    }
                    if (count == grwUnGroupedEmployees.Selection.Count)
                    {
                        CboOrganization.Value = null;
                        CboUnit.Value = null;
                        CboSubUnit.Value = null;
                        ddlTeam.Value = null;
                        grwUnGroupedEmployees.DataSource = null;
                        grwUnGroupedEmployees.DataBind();
                        ShowTeamMemberSuccess(Resources.Message.MSG_SAVED);
                    }
                }
                else
                {
                    ShowTeamMemberError(Resources.Message.MSG_EmployeeIsAssginedToTeam);
                }
            }
            else
            {
                ShowTeamMemberError(Resources.Message.MSG_TeamNotSaved);
            }
        }
        catch (Exception ex)
        {
            //Msg.ShowError(Resources.Message.MSG_ERR);
        }
    }

    protected void CallbackPanel_Callback(object sender, CallbackEventArgsBase e)
    {
        Guid strID = Guid.Empty;
        string[] parameters = e.Parameter.Split('|');
        string strParam = parameters[0].ToString();
        if (e.Parameter.ToString().Contains('|'))
            strID = Guid.Parse(parameters[1].ToString());

        Session["TeamGuid"] = strID;

        switch (strParam)
        {
            case "New":
                DoNew();
                break;
            case "Edit":
                DoFind(strID);
                break;
            case "Save":
                DoSave();
                break;
            case "Show":
                DoShow();
                break;
            case "Delete":
                DoTeamDelete();
                break;
            case "Refresh":
                BindGrid();
                cbPositionType.JSProperties["cpStatus"] = "REFRESH";
                cbPositionType.JSProperties["cpAction"] = "refresh";
                break;
            default:
                break;
        }

    }

    private void RestoreComboValues()
    {
        try
        {
            //Cascaded combos lose their items in this case
            string Organization = Session["Restore_OrgamizationIns"].ToString();
            string ParentUnit = Session["Restore_ParentUnitIns"].ToString();
            string Unit = Session["Restore_UnitIns"].ToString();

            //cboOrganizationHR 
            //cboParentDeptHR 
            // cboUnit 
            //Now restore previous values
            if (!string.IsNullOrEmpty(Organization))
            {
                FillOrganization(CboOrganization);
                CboOrganization.Value = Organization;
            }
            else if (CboOrganization.Value != null)
            {
                FillParentDept();

            }

            if (!string.IsNullOrEmpty(ParentUnit) && CboOrganization.Value != null)
            {
                FillParentDept();
                CboUnit.Value = ParentUnit;
            }
            //else if (CboOrganization.Value != null)
            //{
            //    FillParentDept();
            //}

            if (!string.IsNullOrEmpty(Unit) && CboUnit.Value != null)
            {
                //FillDepartmentCombo(CboSubUnit);
                FillSubUnit();
                CboSubUnit.Value = Unit;
            }
            else if (CboUnit.Value != null)
            {
               // FillDepartmentCombo(CboSubUnit);
                FillSubUnit();
            }
        }
        catch
        {
            //do nothing
        }

    }

    private void StorePreErrorValues()
    {
        try
        {
            //Cascaded combos lose their items in this case
            string Organization = string.Empty;
            string ParentUnit = string.Empty;
            string Unit = string.Empty;

            //check if the combos hold selections
            if (CboOrganization.Value != null)
            {
                Organization = CboOrganization.Value.ToString();
            }
            if (CboUnit.Value != null)
            {
                ParentUnit = CboUnit.Value.ToString();
            }
            if (CboSubUnit.Value != null)
            {
                Unit = CboSubUnit.Value.ToString();
            }
            Session["Restore_OrgamizationIns"] = Organization;
            Session["Restore_ParentUnitIns"] = ParentUnit;
            Session["Restore_UnitIns"] = Unit;

        }
        catch
        {
            //do nothing
        }
    }

    //protected void DoSearch()
    //{
    //    //if (txtSearchOrg.Text.Length == 0)
    //    //{
    //    //    cbPositionType.JSProperties["cpAction"] = "search";
    //    //    ShowError(Resources.Message.Not_Found);
    //    //    return;
    //    //}

    //    tblTeamBussiness objPositionTypeBusiness = new tblTeamBussiness();
    //    CVGeez objGeez = new CVGeez();
    //    try
    //    {
    //        List<tblTeam> l = new List<tblTeam>();
    //        l = objPositionTypeBusiness.GetRecordByPositionName(objGeez.GetSortValueU(txtSearchOrg.Text));
    //        if (l.Count > 0)
    //        {
    //            gvwOrganization.DataSource = l;
    //            gvwOrganization.DataBind();
    //        }
    //        else
    //        {
    //            cbPositionType.JSProperties["cpAction"] = "search";
    //            ShowMessage(Resources.Message.Not_Found);
    //            return;
    //        }
    //        cbPositionType.JSProperties["cpAction"] = "search";
    //        ShowSuccess(string.Empty);

    //    }
    //    catch (Exception ex)
    //    {
    //        ShowError(ex.Message);
    //    }
    //}

    private void BindGrid()
    {
        try
        {
            tblTeamBussiness objPositionType = new tblTeamBussiness();
            List<tblTeam> l = new List<tblTeam>();
            l = objPositionType.GettblTeams();
            if (l.Count > 0)
            {
                gvwOrganization.DataSource = objPositionType.GettblTeams();
                gvwOrganization.DataBind();
            }
        }
        catch { }
    }

    protected void DoShow()
    {
        tblTeamBussiness objTeam = new tblTeamBussiness();
        List<tblTeam> TeamsList = objTeam.GettblTeams();
        if (TeamsList.Count > 0)
        {
            btnDelete.Enabled = true;
            gvwOrganization.DataSource = objTeam.GettblTeams();
            gvwOrganization.DataBind();
            pnlData.Visible = false;
        }
    }

    private List<string> GetErrorMessage()
    {
        List<string> errMessages = new List<string>();


        if (txtTeamName.Text.Length == 0)
            errMessages.Add(Resources.Message.Enter_PositionName);
        if (txtTeamNameRegional.Text.Length == 0)
            errMessages.Add(Resources.Message.Enter_PostionNameRe);

        return errMessages;
    }

    private void ShowErrorMessage(List<string> strMessages)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<ul>");
        foreach (string s in strMessages)
        {
            sb.Append(String.Format("<li>{0}</li>", s));
        }
        sb.Append("</ul>");
    }

    private string GetErrorDisplay(List<string> strMessages)
    {
        StringBuilder sb = new StringBuilder();
        if (strMessages.Count == 0) return string.Empty;
        sb.Append("Error");
        sb.Append("<ul>");
        foreach (string s in strMessages)
        {
            sb.Append(String.Format("<li>{0}</li>", s));
        }
        sb.Append("</ul>");

        return sb.ToString();

    }

    protected bool DoDelete()
    {
        tblTeam objPosition = new tblTeam();
        tblTeamBussiness objPositionTypeBusiness = new tblTeamBussiness();
        try
        {
            objPositionTypeBusiness.Delete(Guid.Parse(Session["TeamGuid"].ToString()));
            Session["IsNew"] = false;

            BindGrid();
            ShowMessage(Resources.Message.MSG_DELETED);
            return true;
        }
        catch (Exception ex)
        {
            ShowError(ex.Message);
            return false;
        }
    }

    protected bool DoTeamMembersDelete()
    {
        try
        {
            for (int i = 0; i < grwGroupedEmployees.VisibleRowCount; i++)
            {
                if (grwGroupedEmployees.Selection.IsRowSelected(i))
                {
                   Guid teamMemberGuid = Guid.Parse(grwGroupedEmployees.GetRowValues(i, "TeamMembersGuid").ToString());
                   Guid teamGuid = Guid.Parse(cboTeam.Value.ToString());
                   tblTeamMembersBussiness objTeamMemebers = new tblTeamMembersBussiness();
                    if (objTeamMemebers.Delete(teamMemberGuid))
                    {
                        Guid selectedTeamGuid = Guid.Parse(cboTeam.Value.ToString());
                        List<tblTeamMembers> objTeamMembers = objTeamMemebers.GettblTeamMemberss(selectedTeamGuid);
                        if (objTeamMembers.Count > 0)
                        {
                            grwGroupedEmployees.DataSource = objTeamMembers;
                            grwGroupedEmployees.DataBind();
                            
                        }
                        GroupEmployess.JSProperties["cpMessage"] = "የተመረጡት የቡድን አባላቶች ከቡድኑ ተሰርዘዋል";
                        GroupEmployess.JSProperties["cpStatus"] = "SUCCESS";
                        GroupEmployess.JSProperties["cpAction"] = "delete";
                    }
                }
            }
           
           
            return true;
        }
        catch (Exception ex)
        {
            ShowError(ex.Message);
            return false;
        }
    }

    protected void DoTeamDelete()
    {
        try
        {                    
            tblTeamBussiness objTeam = new tblTeamBussiness();
            int selectedItems = gvwOrganization.VisibleRowCount;
            int deletedItemsCount = 0;
            if (selectedItems > 0)
            {
                tblTeamMembersBussiness objTeamMemebers = new tblTeamMembersBussiness();
                tblTeamInOrganizationBussiness objTeamInOrganization = new tblTeamInOrganizationBussiness();
                for (int i = 0; i < selectedItems; i++)
                {
                    if (gvwOrganization.Selection.IsRowSelected(i))
                    {
                        Guid teamGuid = Guid.Parse(gvwOrganization.GetRowValues(i, "TeamGuid").ToString());
                        //Check whethere there are team members
                        if (objTeamMemebers.GetRecordByTeamGuid(teamGuid).Count == 0)
                        {
                            //Check Whether team is related to organization
                            if (objTeamInOrganization.IsTeamBindToOrganization(teamGuid))
                            {
                                cbPositionType.JSProperties["cpMessage"] = Resources.Message.MSG_TeamRelatedToOrganization;
                                cbPositionType.JSProperties["cpStatus"] = "ERROR";
                                cbPositionType.JSProperties["cpAction"] = "Delete";
                            }
                            else
                            {
                                if (objTeam.Delete(teamGuid))
                                {
                                    deletedItemsCount++;
                                }                                
                            }
                        }
                        else
                        {
                            cbPositionType.JSProperties["cpMessage"] = Resources.Message.MSG_TeamHasMembers;
                            cbPositionType.JSProperties["cpStatus"] = "ERROR";
                            cbPositionType.JSProperties["cpAction"] = "Delete";
                        }
                    }
                }
                if (deletedItemsCount > 0)
                {
                    DoShow();
                    cbPositionType.JSProperties["cpMessage"] = Resources.Message.MSG_TeamDeleted;
                    cbPositionType.JSProperties["cpStatus"] = "SUCCESS";
                    cbPositionType.JSProperties["cpAction"] = "Delete";
                }
            }
            else
            {
                ShowError(Resources.Message.MSG_TeamNotSelected);
            }
        }
        catch (Exception ex)
        {
            ShowError(ex.Message);
        }
    }

    private void ShowError(string strMsg)
    {
        cbPositionType.JSProperties["cpMessage"] = strMsg;
        cbPositionType.JSProperties["cpStatus"] = "ERROR";
    }

    private void ShowMessage(string strMsg)
    {
        cbPositionType.JSProperties["cpMessage"] = strMsg;
        cbPositionType.JSProperties["cpStatus"] = "INFO";

    }

    private void ShowSuccess(string strMsg)
    {
        cbPositionType.JSProperties["cpMessage"] = strMsg;
        cbPositionType.JSProperties["cpStatus"] = "SUCCESS";
    }

    private void ShowTeamMemberError(string strMsg)
    {
        GroupEmployess.JSProperties["cpMessage"] = strMsg;
        GroupEmployess.JSProperties["cpStatus"] = "ERROR";
    }

    private void ShowTeamMemberMessage(string strMsg)
    {
        GroupEmployess.JSProperties["cpMessage"] = strMsg;
        GroupEmployess.JSProperties["cpStatus"] = "INFO";

    }

    private void ShowTeamMemberSuccess(string strMsg)
    {
        GroupEmployess.JSProperties["cpMessage"] = strMsg;
        GroupEmployess.JSProperties["cpStatus"] = "SUCCESS";
        GroupEmployess.JSProperties["cpAction"] = "save";

    }

    protected bool DoFind(Guid PositionTypeGuid)
    {
        tblTeamBussiness objPositionTypeBLL = new tblTeamBussiness();
        tblTeam objPositionType = new tblTeam();
        try
        {
            objPositionType = objPositionTypeBLL.GettblTeam(PositionTypeGuid);
            if (objPositionType == null)
            {
                ShowMessage(Resources.Message.Not_Found);
                return false;
            }
            //Now Record was found;
            Session["IsNew"] = false;
            ClearForm();
            txtTeamName.Text = objPositionType.TeamName;
            txtTeamNameRegional.Text = objPositionType.TeamNameAm;
            Session["Guid"] = PositionTypeGuid;
            btnSave.Enabled = true;
            cbPositionType.JSProperties["cpStatus"] = "SUCCESS";
            return true;
        }
        catch (Exception ex)
        {
            ShowError(ex.Message);
            return false;
        }
    }

    protected void ClearForm()
    {

     txtTeamName.Text    = string.Empty;
     txtTeamNameRegional.Text = string.Empty;

    }

    protected bool DoNew()
    {
        try
        {
            Session["IsNew"] = true;
            ClearForm();
            btnSave.Enabled = true;
            btnNew.Enabled = false;
            cbPositionType.JSProperties["cpAction"] = "new";
            cbPositionType.JSProperties["cpStatus"] = "SUCCESS";
            cbPositionType.JSProperties["cpMessage"] = string.Empty;
            return true;
        }
        catch (Exception ex)
        {
            ShowError(ex.Message);
            return false;
        }
    }
   
    protected bool DoSave()
    {
        Page.Validate();
        //List<string> errMessages = GetErrorMessage();
        //if (errMessages.Count > 0)
        //{

        //    ShowError(GetErrorDisplay(errMessages));
        //    return false;
        //}

        List<string> errMessages = GetErrorMessage();
        StorePreErrorValues();

        if (errMessages.Count > 0)
        {
            RestoreComboValues();
            ShowError(GetErrorDisplay(errMessages));
            return false;
        }

        tblTeam objTeam = new tblTeam();
        //CVGeez objGeez = new CVGeez();
        try
        {
            tblTeamBussiness objPositionTypeBLL = new tblTeamBussiness();
            if (!objPositionTypeBLL.CheckTeamExistance(txtTeamNameRegional.Text, txtTeamName.Text))
            {
                objTeam.TeamName = txtTeamName.Text;
                objTeam.TeamNameAm = txtTeamNameRegional.Text;
                objTeam.UserName = Session["ProfileFullName"].ToString();
                objTeam.EventDatetime = DateTime.Now.Date;
                objTeam.UpdatedUsername = Session["ProfileFullName"].ToString();
                objTeam.UpdatedEventDatetime = DateTime.Now.Date;
                if ((bool) Session["IsNew"])
                {
                    objTeam.TeamGuid = Guid.NewGuid();
                    objPositionTypeBLL.InserttblTeam(objTeam);
                }
                else
                {
                    objTeam.TeamGuid = new Guid(Session["Guid"].ToString()); //Attention
                    objPositionTypeBLL.UpdatetblTeam(objTeam);
                }

                BindGrid();
                ClearForm();
                cbPositionType.JSProperties["cpAction"] = "save";
                ShowSuccess(Resources.Message.MSG_SAVED);

                return true;
            }
            else
            {
                ShowError(Resources.Message.MSG_TeamAlreadyExisted);
                return false;
            }

        }
        catch (Exception ex)
        {
            ShowError(ex.Message);
            return false;
        }
    }

    protected void grwGroupedEmployees_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "PositionInTeam")
            if ((e.Value).ToString() == "0")
            {
                e.DisplayText = "አባል";

            }
            if ((e.Value).ToString() == "1")
            {
                e.DisplayText = "ቡድን መሪ";

            }
    }
    
    
    protected void BindTeam()
    {
        btnNew.Enabled = true;
        tblTeamMembersBussiness objTeamMemebers = new tblTeamMembersBussiness();
        Guid selectedTeamGuid =Guid.Parse(cboTeam.Value.ToString());
        List<tblTeamMembers> objTeamMembers = objTeamMemebers.GettblTeamMemberss(selectedTeamGuid);
        if(objTeamMembers.Count > 0)
        {
            grwGroupedEmployees.DataSource = objTeamMembers;
            grwGroupedEmployees.DataBind();
            grwGroupedEmployees.JSProperties["cpStatus"] = "SUCCESS";
            grwGroupedEmployees.JSProperties["cpAction"] = "LoadTeamMembers";
        }
        else
        {
            grwGroupedEmployees.JSProperties["cpStatus"] = "ERROR";
            grwGroupedEmployees.JSProperties["cpAction"] = "NotLoadTeamMembers";
            grwGroupedEmployees.JSProperties["cpMessage"] = "በቡድኑ ውስጥ የተመደቡ ሰራተኞች የሉም";
        }
    }

    protected void OnGroupEmployee_Callback(object sender, CallbackEventArgsBase e)
    {
        StorePreErrorValues();
        Guid strID = Guid.Empty;
        string[] parameters = e.Parameter.Split('|');
        string strParam = parameters[0].ToString();
        if (e.Parameter.ToString().Contains('|'))
            strID = Guid.Parse(parameters[1].ToString());
        switch (strParam)
        {
            case "Search":
                LoadEmployees();
                break;
            case "Delete":
                DoTeamMembersDelete();
                break;
            case "New":
                NewTeamMember();
                break;
            case "Save":
                SaveTeamMember();
                break;
        }
       
    }

    private void LoadEmployees()
    {
        tblTeamMembersBussiness objTeamMemeber = new tblTeamMembersBussiness();
        grwUnGroupedEmployees.DataSource = objTeamMemeber.GetUnGroupedMembers(Convert.ToInt32((CboSubUnit.Value)));
        grwUnGroupedEmployees.DataBind();
        GroupEmployess.JSProperties["cpStatus"] = "SUCCESS";
        GroupEmployess.JSProperties["cpAction"] = "";
    }

    protected void CboUnit_OnCallback(object sender, CallbackEventArgsBase e)
    {
        StorePreErrorValues();//2016
        FillParentDept();
        RestoreComboValues();//2016
    }

    private void FillParentDept()
    {
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        string strSQL;
        strSQL = @"Select code ,amDescription  from tblUnit where level=1 and  OrgGuid='" + CboOrganization.Value.ToString() + "' ";
        CCommon.FillAspxComboFromDB(CboUnit, strSQL, "amDescription", "code");
        CboUnit.Value = null;
    }

    protected void CboSubUnit_OnCallback(object sender, CallbackEventArgsBase e)
    {
        StorePreErrorValues();
        FillSubUnit();
        RestoreComboValues();
    }

    private void FillSubUnit()
    {
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        string strSQL;
        strSQL = @"Select code ,amDescription  from tblUnit where level=0 and dept_code = " + Convert.ToInt32(CboUnit.Value) + " and OrgGuid='" + CboOrganization.Value.ToString() + "' ";
        CCommon.FillAspxComboFromDB(CboSubUnit, strSQL, "amDescription", "code");
        CboSubUnit.Value = null;
    }

    protected void grwGroupedEmployees_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        BindTeam();
    }

    protected void gvwOrganization_OnPageIndexChanged(object sender, EventArgs e)
    {
        DoShow();
    }

    protected void grwUnGroupedEmployees_OnPageIndexChanged(object sender, EventArgs e)
    {
        LoadEmployees();
    }

    protected void grwGroupedEmployees_OnPageIndexChanged(object sender, EventArgs e)
    {
        BindTeam();
    }
}