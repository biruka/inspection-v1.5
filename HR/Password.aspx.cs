﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CUSTOR.PasswordPolicy;
using System.Text;
using System.Text.RegularExpressions;

public partial class Security_Password : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!User.Identity.IsAuthenticated)
            Response.Redirect("~/Login.aspx");
    }
    protected void ChangePwd_ChangedPassword(object sender, EventArgs e)
    {
        Response.Redirect("~/Login.aspx");
    }
    protected void ChangePasswordPushButton_Click(object sender, EventArgs e)
    {
      
    }
    protected void btnChange_Click(object sender, EventArgs e)
    {
        DoValidate();
    }
    private void DoValidate()
    {
        PasswordSetting passwordSetting = CPasswordManager.GetPasswordSetting();
        StringBuilder sbPasswordRegx = new StringBuilder(string.Empty);

        sbPasswordRegx.Append(@"(?=^.{" + passwordSetting.MinLength + "," + passwordSetting.MaxLength + "}$)");
        sbPasswordRegx.Append(@"(?=(?:.*?\d){" + passwordSetting.NumsLength + "})");
        sbPasswordRegx.Append(@"(?=.*[a-z])");
        sbPasswordRegx.Append(@"(?=(?:.*?[A-Z]){" + passwordSetting.UpperLength + "})");
        sbPasswordRegx.Append(@"(?=(?:.*?[" + passwordSetting.SpecialChars + "]){" + passwordSetting.SpecialLength + "})");
        sbPasswordRegx.Append(@"(?!.*\s)[0-9a-zA-Z" + passwordSetting.SpecialChars + "]*$");

        if (Regex.IsMatch(ChangePwd.NewPassword, sbPasswordRegx.ToString()))
        {


            MembershipUser usrInfo = Membership.GetUser(User.Identity.Name);
            usrInfo.IsApproved = true;
            usrInfo.ChangePassword(ChangePwd.CurrentPassword,ChangePwd.NewPassword);
            Membership.UpdateUser(usrInfo);
            //lblError.BackColor = System.Drawing.Color.Green;
            //lblError.Text = "Password was successfuly changed!";
            CUSTOR.CMsgBar.ShowMessage("ፓስወርዶ ተቀይሮዋል", pnlMsg, lblMessage);
        }
        else
        {
            //lblError.Text = "Password does not conform to password policy!";
            CUSTOR.CMsgBar.ShowError("ፓስወርዶ ከ ፓስወርድ መቀየሪያ ፖሊሲ ጋር አልተጣጣመም ከንደገና ይሞክሩ", pnlMsg, lblMessage);
        }
}
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Login.aspx");
    }
}
