﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Password.aspx.cs" Inherits="Security_Password"
    MasterPageFile="~/Default.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="Server">
    <center>

        <script src="Script/jquery-1.4.4.min.js" type="text/javascript"></script>

        <script src="Script/jquery.password-strength.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                var myPlugin = $("[id$='NewPassword']").password_strength();

                $("[id$='ChangePasswordPushButton']").click(function() {
                    return myPlugin.metReq();
                });
            });
        </script>

        <table class="style1">
            <tr>
                <td class="style2">
                    &nbsp;
                </td>
                <td class="style6">
                    &nbsp;
                </td>
                <td style="text-align: center">
                    <br />
                    <asp:Label ID="lblError" runat="server" BackColor="Red" Font-Bold="True" ForeColor="White"></asp:Label>
                    <br />
                                            <asp:Panel runat="server" CssClass="messageWarning" 
                        Height="100%" ID="pnlMsg"  Visible="False">
                                                <asp:Label runat="server" CssClass="Message" ID="lblMessage"></asp:Label>

                                            </asp:Panel>

                </td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;
                </td>
                <td class="style6">
                    &nbsp;
                </td>
                <td style="border-style: solid; border-width: 1px; border-color: Navy">
                    <asp:ChangePassword ID="ChangePwd" runat="server" OnChangedPassword="ChangePwd_ChangedPassword"
                        Height="218px" Width="691px">
                        <TitleTextStyle BackColor="#CCCCCC" Font-Bold="True" ForeColor="#006699" HorizontalAlign="Center"
                            VerticalAlign="Middle" />
                        <ChangePasswordTemplate>
                            <table border="0" cellpadding="1" cellspacing="0" style="border-collapse: collapse;">
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0" style="height: 218px; width: 681px;">
                                            <tr>
                                                <td align="right" class="style3" style="text-align: right">
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" class="style3" style="text-align: right">
                                                    <asp:Label ID="CurrentPasswordLabel" runat="server" AssociatedControlID="CurrentPassword">Old Password:</asp:Label>
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
                                                        ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ChangePwd">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" class="style3" style="text-align: right">
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" style="color: #006699; background-color: #CCCCCC;
                                                    font-weight: bold;" valign="middle" class="style7">
                                                    Change Your Password
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" class="style9" style="text-align: right">
                                                    &nbsp;
                                                </td>
                                                <td class="style10">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" class="style9" style="text-align: right">
                                                    <asp:Label ID="ConfirmNewPasswordLabel" runat="server" AssociatedControlID="ConfirmNewPassword">New Password:</asp:Label>
                                                </td>
                                                <td class="style10" align="left">
                                                    <asp:TextBox ID="NewPassword" runat="server" TextMode="Password"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword"
                                                        ErrorMessage="New Password is required." ToolTip="New Password is required."
                                                        ValidationGroup="ChangePwd">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" class="style4" style="text-align: right">
                                                    &nbsp;
                                                </td>
                                                <td class="style5">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" class="style4" style="text-align: right">
                                                    <asp:Label ID="NewPasswordLabel" runat="server" AssociatedControlID="NewPassword">Confirm New Password:</asp:Label>
                                                </td>
                                                <td class="style5" align="left">
                                                    <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                                                        ErrorMessage="Confirm New Password is required." ToolTip="Confirm New Password is required."
                                                        ValidationGroup="ChangePwd">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" style="color: Red;">
                                                    <asp:Literal ID="FailureText" runat="server" ></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" class="style7">
                                                    <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                                                        ControlToValidate="ConfirmNewPassword" Display="Dynamic" ErrorMessage="The Confirm New Password must match the New Password entry."
                                                        ValidationGroup="ChangePwd"></asp:CompareValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" class="style3">
                                                    <asp:Button ID="ChangePasswordPushButton" runat="server" CommandName="ChangePassword"
                                                        Text="Change Password" ValidationGroup="ChangePwd" OnClick="ChangePasswordPushButton_Click"
                                                        Width="138px" Visible="False" />
                                                </td>
                                                <td>
                                                    &nbsp;
                                                    <asp:Button ID="CancelPushButton" runat="server" CausesValidation="False" CommandName="Cancel"
                                                        Text="Cancel" Visible="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </ChangePasswordTemplate>
                    </asp:ChangePassword>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;
                </td>
                <td class="style6">
                    &nbsp;
                </td>
                <td style="text-align: center">
                    <table class="style8">
                        <tr>
                            <td style="text-align: right">
                                <asp:Button ID="btnChange" runat="server" OnClick="btnChange_Click" Text="Change" />
                            </td>
                            <td style="text-align: left">
                                &nbsp;
                                <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;
                </td>
                <td class="style6">
                    &nbsp;
                </td>
                <td>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </center>
</asp:Content>
