﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Login.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<%--<%@ Register assembly="WebControlCaptcha" namespace="WebControlCaptcha" tagprefix="cc1" %>--%>
<%@ Register Src="~/Controls/Alert.ascx" TagName="Alert" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        .auto-style1 {width: 298px;}
        .captcha {
        }
        .auto-style2 {
            width: 109px;
        }.auto-style3 {color: #FF3300;

        }
    </style></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphLogin" runat="Server">
    <asp:Panel class="alert alert-info" runat="server" ID="pnlInfo" Visible="False">
        <asp:Label runat="server" ID="lblInfo"></asp:Label>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlMsg">
        <asp:Label runat="server" ID="lblError" EnableTheming="False"></asp:Label>
    </asp:Panel>
    <%--<asp:Panel CssClass="RoundPanel" runat="server">--%>
    <%--  <div style="margin: 35px; width: 700px" class="panel  panel-info">
            <div class="panel-heading">
                <asp:Label ID="Label1" Text="Login to Penality Management System" runat="server" />
            </div>--%>
    <div style="margin-left: 0px; width: 700px">
        <table>
            <tr>
                <td class="auto-style1"></td>
                <td class="auto-style1">
                    <div style="text-align: left">
                        <asp:Label ID="lblMessage" ForeColor="Red" Font-Bold="true" EnableTheming="false" runat="server"></asp:Label>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">&nbsp;</td>
                <td class="auto-style1">
                    <uc1:Alert ID="Alert1" runat="server" />
                    <div style="margin-top: 50px">
                        <asp:LoginView ID="loginBox" runat="server">
                            <LoggedInTemplate>
                            </LoggedInTemplate>
                            <AnonymousTemplate>
                                <%--<div class="loginIcon">
                      </div>--%>
                                <asp:Login ID="Login1" runat="server" OnAuthenticate="Login1_Authenticate" OnLoggingIn="Login1_LoggingIn" OnLoginError="Login1_LoginError" VisibleWhenLoggedIn="False">
                                    <LayoutTemplate>
                                        <asp:Panel ID="pnlLogin" runat="server" CssClass="RoundPanelToolbar" DefaultButton="LoginButton">

                                            <table style="width: 500px;">
                                                <tr>
                                                    <td colspan="5">
                                                        <div class="alert alert-success" style="width: 500px; height: 55px; margin: 15px">
                                                            <b>Please login with your Username and Password.</b>

                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style2">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name </asp:Label><span class="auto-style3">*</span>
                                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" Display="Dynamic" EnableClientScript="False" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="Login1" Font-Bold="True">User Name is required.</asp:RequiredFieldValidator>
                                                    </td>
                                                    <td></td>
                                                    <td></td>        
                                                </tr>
                                                <tr >
                                                    <td class="auto-style2">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:TextBox ID="UserName" runat="server" AutoCompleteType="None" MaxLength="50" TabIndex="1" ToolTip="enter your user name" Width="70%"></asp:TextBox>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style2">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password</asp:Label><span class="auto-style3">*</span>
                                                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" Display="Dynamic" EnableClientScript="False" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="Login1" Font-Bold="True">Password is required.</asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style2">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:TextBox ID="Password" runat="server" AutoCompleteType="None" MaxLength="50" TabIndex="2" TextMode="Password" ToolTip="enter your password" Width="70%"></asp:TextBox>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style2">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style2">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <dx:ASPxButton ID="LoginButton" runat="server" Checked="true" CommandName="Login" TabIndex="4" Text="Log In" Theme="iOS" ValidationGroup="Login1" Width="100px" />
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style2">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:HyperLink ID="lnkLostPassword" runat="server" NavigateUrl="~/recover-password.aspx" Visible="False">Lost password?</asp:HyperLink>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                            <asp:LoginView ID="userStatus" runat="server">
                                                <LoggedInTemplate>
                                                    <div class="logoutIcon">
                                                    </div>
                                                    <div style="font-size: 20px;">
                                                        Howdy!
                                              <asp:LoginName ID="LoginName1" runat="server" />
                                                    </div>
                                                    You are currently logged in.
                                          <br />
                                                    <br />
                                                    <asp:LoginStatus ID="LoginStatus1" runat="server" />
                                                </LoggedInTemplate>
                                            </asp:LoginView>
                                            <asp:CheckBox ID="RememberMe" runat="server" TabIndex="5" Text="Remember me next time." Visible="False" />
                                            <br />
                                        </asp:Panel>
                                    </LayoutTemplate>
                                </asp:Login>
                            </AnonymousTemplate>
                        </asp:LoginView>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td class="auto-style1">&nbsp;</td>
            </tr>
        </table>
    </div>
    <%--</div>--%>
    <%--</asp:Panel>--%>
    <div style="margin: 40px; width: 700px">

        <div class="alert alert-warning">
            <p>Please note that:</p>
            <ul>
                <li style="margin-left: 15px">
                    <p>
                        You need to change your <strong>Password</strong> when it is your first time
                        <br />
                        to access the application or when your password is <strong>Reset</strong> by Administrator.
                        <br />
                        Enter any password and click Login. You will be redirected to Password Change form
                    </p>

                </li>
                <li style="margin-left: 15px">
                    <p>
                        <asp:Label runat="server" ID="lblExpiryMsg"></asp:Label>
                    </p>
                </li>
            </ul>


        </div>
    </div>
</asp:Content>

