﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using CUSTOR;
using DevExpress.Web;

public partial class Forms_Reward : System.Web.UI.UserControl
{
    public static bool _IsNewRecord;
    public bool IsNewRecord
    {
        get { return _IsNewRecord; }
        set { _IsNewRecord = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {
            BindRewardType();
            if (Session["PersonGuid"] != null)
            {
                Attachment1.SelectedHRActivityType = (int)CGlobals.enumHRActivity.eRewardTab;
                Attachment1.ApplicantGuid = new Guid(Session["PersonGuid"].ToString());
            }
           
        }

        SetupClientScript();
    }
    private bool CanAddEditDelete()
    {
        return HttpContext.Current.User.IsInRole("HR Officer") || HttpContext.Current.User.IsInRole("HR Supervisor");
    }
    private bool CanAddEdit()
    {
        return HttpContext.Current.User.IsInRole("HR Data Entry");
    }
   
    public void BindRewardType()
    {
        BindRewardType(ddlRewardType);
    }
    private void BindRewardType(DevExpress.Web.ASPxComboBox cboName)
    {
        string strSQL = @"SELECT * FROM tblLookUp INNER JOIN tblLookUpType ON " +
                         "tblLookUp.parent = tblLookUpType.code " +
                         "WHERE (tblLookUpType.code = 55)";

        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);

        if (p.Organization.LanguageID == 0)
        {
            CCommon.FillAspxComboFromDB(cboName, strSQL, "description", "code");
        }
        else if (p.Organization.LanguageID == 1)
        {
            CCommon.FillAspxComboFromDB(cboName, strSQL, "amDescription", "code");
        }
        else if (p.Organization.LanguageID == 2)
        {
            CCommon.FillAspxComboFromDB(cboName, strSQL, "AfanOromo", "code");
        }
        else if (p.Organization.LanguageID == 3)
        {
            CCommon.FillAspxComboFromDB(cboName, strSQL, "Tigrigna", "code");
        }
        else if (p.Organization.LanguageID == 4)
        {
            CCommon.FillAspxComboFromDB(cboName, strSQL, "Afar", "code");
        }
        else if (p.Organization.LanguageID == 5)
        {
            CCommon.FillAspxComboFromDB(cboName, strSQL, "Somali", "code");
        }

        cboName.Items.Insert(0, new ListEditItem("--ይምረጡ--", "-1"));
        cboName.SelectedIndex = 0;
    }

    private void SetupClientScript()
    {
        string js = @"
			<script language=JavaScript>
				function ConfirmDeletion()
				{
					return confirm('Are you sure you want to delete?');
				}
			</script>";

        if (!Page.IsClientScriptBlockRegistered("ConfirmDeletion"))
        {
            Page.RegisterClientScriptBlock("ConfirmDeletion", js);
        }
    }

   
    private void BindRewardGrid()
    {
        CReward objtblReward = new CReward();
        DataTable dt = new DataTable();

        try
        {
            objtblReward.ParentGuid = new Guid(Session["PersonGuid"].ToString());
            dt = objtblReward.GetRecords();
            if (dt.Rows.Count > 0)
            {
                gvwReward.Visible = true;
                gvwReward.DataSource = dt;
                gvwReward.DataBind();

            }
            else
            {
                gvwReward.Visible = false;
                ShowMsg(Resources.Message.MSG_RECORDNOTFOUND);
            }
        }
        catch (Exception ex)
        {
            lblMsg.Visible = true;
            lblMsg.Text = Resources.Message.MSG_OPERATIONNOTPERFORMED;
        }
    }
    public void ResetControl()
    {
        ClearForm();
        gvwReward.Visible = false;
        //Panel1.Visible = false;
    }
  
    private void DoNew()
    {
        if (Session["PersonGuid"] != null)
        {
            lblMsg.Visible = false;
            Session["NewReward"] = true;
            //Panel1.Visible = true;
            ClearForm();
        }
    }

    protected void ClearForm()
    {
       
        txtReason.Text = string.Empty;
        if (ddlRewardType.Items.Count > 0)
            ddlRewardType.SelectedIndex = 0;
        txtRewardDescription.Text = string.Empty;
        txtRemark.Text = string.Empty;

    }

    public void ShowMsg(string msg)
    {
        lblMsg.Visible = true;
        lblMsg.Text = msg;
        CMsgBar.ShowMessage(msg, pnlMsg, lblMsg);
    }

    public void ShowErr(string msg)
    {
        lblMsg.Visible = true;
        lblMsg.Text = msg;
        CMsgBar.ShowError(msg, pnlMsg, lblMsg);
    }

    protected bool IsValidRecord()
    {
        if (BDOB.IsValidDate!=true)
        {
            ShowErr(Resources.Message.MSG_R_Reward);
            return false;
        }
        

        if (txtReason.Text.Trim().Length == 0)
        {
            ShowErr(Resources.Message.MSG_R_Reward1);
            return false;
        }

        if (txtRewardDescription.Text.Trim().Length == 0)
        {
            ShowErr(Resources.Message.MSG_R_Reward2);
            return false;
        }
        return true;
    }

    protected bool DoSave()
    {

        if (!IsValidRecord()) return false;
        CReward objtblReward = new CReward();

        try
        {
            if (BDOB.IsValidDate==true)
            {
              objtblReward.Date_rewarded = BDOB.SelectedGCDate;               
            }
            else
                objtblReward.Date_rewarded = EthiopicDateTime.GetGregorianDate(System.DateTime.Today.Day, System.DateTime.Today.Month, System.DateTime.Today.Year);

            objtblReward.ParentGuid = new Guid(Session["PersonGuid"].ToString());
            objtblReward.Reason = txtReason.Text;
            objtblReward.Reward_type = int.Parse(ddlRewardType.Value.ToString());
            objtblReward.Reward_description = txtRewardDescription.Text;
            if (txtRemark.Text.ToString() != string.Empty)
                objtblReward.Remark = txtRemark.Text;

            bool isNewAbsentism = (bool)Session["NewReward"];
            if (isNewAbsentism)
            {

                objtblReward.Insert();
                //ShowMsg(Resources.Message.MSG_SAVE);
            }
            else
            {

                objtblReward.MainGuid = new Guid(Session["EditStaffID"].ToString());
                objtblReward.Update();
            }
            //lblMsg.Visible = true;
            CMsgBar.ShowMessage(Resources.Message.MSG_SAVE, pnlMsg, lblMsg);
            Session["NewReward"] = false;
            //Panel1.Visible = false;
            BindRewardGrid();
            ClearForm();
            return true;
        }
        catch (Exception ex)
        {
            //lblMsg.Visible = true;
            //lblMsg.Text = Resources.Message.MSG_OPERATIONNOTPERFORMED;
            CMsgBar.ShowError(Resources.Message.MSG_OPERATIONNOTPERFORMED, pnlMsg, lblMsg);
            return false;
        }
    }
   

    protected void DoFind(Guid Seqno)
    {
        CReward objtblReward = new CReward();
        DataTable dt = new DataTable();

        //Panel1.Visible = true;
        objtblReward.MainGuid = Seqno;
        dt = objtblReward.GetRecord();
        if (dt.Rows.Count == 0)
        {
            ShowErr(Resources.Message.MSG_RECORDNOTFOUND);
            return;
        }


        txtReason.Text = objtblReward.Reason;
        ddlRewardType.Value = objtblReward.Reward_type.ToString();
        txtRewardDescription.Text = objtblReward.Reward_description;
        txtRemark.Text = objtblReward.Remark;

        DateTime TransferDate= Convert.ToDateTime(EthiopicDateTime.GetEthiopicDate(objtblReward.Date_rewarded.Day, objtblReward.Date_rewarded.Month, objtblReward.Date_rewarded.Year));
        BDOB.SelectedDate = TransferDate.ToShortDateString();
    }


    private bool DoDelete(Guid Seqno)
    {
        CReward objtblReward = new CReward();
        lblMsg.Visible = false;
        try
        {
            objtblReward.MainGuid = Seqno;
            bool success = objtblReward.Delete();

            if (success)
            {
                ShowMsg(Resources.Message.MSG_DELETED);
                ClearForm();
                return true;
            }
            return false;

        }
        catch (Exception ex)
        {
            lblMsg.Visible = true;
            lblMsg.Text = Resources.Message.MSG_OPERATIONNOTPERFORMED;
            return false;
        }

    }
    protected void gvwReward_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvwReward.PageIndex = e.NewPageIndex;
        BindRewardGrid();
    }



    protected void imgUnit_Click(object sender, ImageClickEventArgs e)
    {

    }

    protected void lbtnAttach_Click(object sender, EventArgs e)
    {
        Response.Redirect("../Forms/Attachment.aspx");
    }
    public enum enumToolbar
    {
        eNew = 0,
        eSave = 2,
        eDelete = 4,
        eRefresh = 6,
        eFind = 8,
        ePrint = 10
    }

   
    protected void lbBack_Click(object sender, EventArgs e)
    {
        //pnlMenu.Visible = true;
        pnlAttachement.Visible = false;
    }
   
    protected void btnAttachment_Click(object sender, EventArgs e)
    {
        //Panel1.Visible = false;
        //pnlMenu.Visible = false;
        gvwReward.Visible = false;
        pnlAttachement.Visible = true;
    }
    protected void clbReward_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {
        try
        {
            string strID = string.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0].ToString();
            if (e.Parameter.ToString().Contains('|'))
                strID = parameters[1].ToString();
            switch (strParam)
            {
                case "NewReward":
                    if (Session["PersonGuid"] == null)
                    {
                        ShowMsg(Resources.Message.MSG_PERSON);
                        return;
                    }
                    DoNew();
                    pnlAttachement.Visible = false;
                    btnNewReward.Enabled = false;
                    btnSaveReward.Enabled = true;
                    break;

                case "SaveReward":
                    lblMsg.Visible = true;
                    if (IsValidRecord())
                    {
                        if (DoSave())
                        {
                            btnNewReward.Enabled = true;
                            btnSaveReward.Enabled = false;
                            pnlAttachement.Visible = false;
                        }
                    }
                    break;

                case "FindReward":


                    pnlAttachement.Visible = false;
                    BindRewardGrid();

                    btnNewReward.Enabled = true;
                    btnSaveReward.Enabled = false;
                    break;

                case "EditReward":

                    //Panel1.Visible = true;
                    lblMsg.Visible = false;
                    Session["NewReward"] = false;
                    btnNewReward.Enabled = false;
                    btnSaveReward.Enabled = true;
                    Session["EditStaffID"] = new Guid(strID);
                    DoFind(new Guid(strID));
                    break;

                case "DeleteReward":
                   
                    DoDelete(new Guid(strID));
                    gvwReward.Visible = false;


                    break;


                default:
                    break;
            }
        }
        catch
        {

        }
    }
}
