﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;

    public class CEvaluation
    {
        string _ConnectionString = string.Empty;

        public CEvaluation()
        {
            _ConnectionString = ConnectionString;
        }

        public CEvaluation(Guid MainGuid, int Seq_no, Guid ParentGuid, DateTime Fiscal_year, int Eval_year, int Period, int EvaluationLevel, float Eval_point, string Strong_side, string Weak_side, string Remark , int Budget_year)
        {
            _mainGuid = MainGuid;
            _seq_no = Seq_no;
            _parentGuid = ParentGuid;
            _fiscal_year = Fiscal_year;
            _eval_year = Eval_year;
            _period = Period;
            _EvaluationLevel = EvaluationLevel;
            _eval_point = Eval_point;
            _strong_side = Strong_side;
            _weak_side = Weak_side;
            _remark = Remark;
        //   _budgetYear = Budget_Year;
           _budget_year = Budget_year;

    }

    private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }

    #region Class Property Declarations
    ////
    //int _Budget_Year;
    //public int Budget_Year
    //{
    //    get { return _Budget_Year; }
    //    set { _Budget_Year = value; }
    //}


    ////
    //Elezer
    int _budget_year;
    public int Budget_year
    {
        get { return _budget_year; }
        set { _budget_year = value; }
    }
//
    Guid _mainGuid;
        public Guid MainGuid
        {
            get { return _mainGuid; }
            set { _mainGuid = value; }
        }

        int _seq_no;
        public int Seq_no
        {
            get { return _seq_no; }
            set { _seq_no = value; }
        }
    int _LangId;
    public int LangId
    {
        get { return _LangId; }
        set { _LangId = value; }
    }
    Guid _parentGuid;
        public Guid ParentGuid
        {
            get { return _parentGuid; }
            set { _parentGuid = value; }
        }

        DateTime _fiscal_year;
        public DateTime Fiscal_year
        {
            get { return _fiscal_year; }
            set { _fiscal_year = value; }
        }

        int _eval_year;
        public int Eval_year
        {
            get { return _eval_year; }
            set { _eval_year = value; }
        }

        int _period;
        public int Period
        {
            get { return _period; }
            set { _period = value; }
        }
        int _EvaluationLevel;
        public int EvaluationLevel
        {
            get { return _EvaluationLevel; }
            set { _EvaluationLevel = value; }
        }

        double _eval_point;
        public double Eval_point
        {
            get { return _eval_point; }
            set { _eval_point = value; }
        }

        string _strong_side;
        public string Strong_side
        {
            get { return _strong_side; }
            set { _strong_side = value; }
        }

        string _period_text;
        public string period_text
        {
            get { return _period_text; }
            set { _period_text = value; }
        }

        string _weak_side;
        public string Weak_side
        {
            get { return _weak_side; }
            set { _weak_side = value; }
        }

        string _remark;
        public string Remark
        {
            get { return _remark; }
            set { _remark = value; }
        }

        #endregion

        //public DataTable GetRecord()
        //{
        //    SqlConnection connection = new SqlConnection(_ConnectionString);
        //    SqlCommand command = new SqlCommand();
        //    command.CommandText = "dbo.[tblEvaluation_GetRecord]";
        //    command.CommandType = CommandType.StoredProcedure;
        //    DataTable dTable = new DataTable("tblEvaluation");
        //    SqlDataAdapter adapter = new SqlDataAdapter(command);
        //    command.Connection = connection;

        //    try
        //    {
        //        command.Parameters.Add(new SqlParameter("@MainGuid", _mainGuid));
        //        connection.Open();
        //        adapter.Fill(dTable);
        //        if (dTable.Rows.Count > 0)
        //        {
        //            _mainGuid = (Guid)dTable.Rows[0]["MainGuid"];
        //            _seq_no = (int)dTable.Rows[0]["seq_no"];

        //            if (dTable.Rows[0]["ParentGuid"].Equals(DBNull.Value))
        //                _parentGuid = Guid.Empty;
        //            else
        //                _parentGuid = (Guid)dTable.Rows[0]["ParentGuid"];
        //            if (dTable.Rows[0]["fiscal_year"].Equals(DBNull.Value))
        //                _fiscal_year = DateTime.MinValue;
        //            else
        //                _fiscal_year = (DateTime)dTable.Rows[0]["fiscal_year"];
        //            if (dTable.Rows[0]["period"].Equals(DBNull.Value))
        //                _period = 0;
        //            else
        //                _period = (int)dTable.Rows[0]["period"];
        //            if (dTable.Rows[0]["EvaluationLevel"].Equals(DBNull.Value))
        //                _EvaluationLevel = 0;
        //            else
        //                _EvaluationLevel = (int)dTable.Rows[0]["EvaluationLevel"];
        //            if (dTable.Rows[0]["eval_point"].Equals(DBNull.Value))
        //                _eval_point = 0;
        //            else
        //                _eval_point = (double)dTable.Rows[0]["eval_point"];
        //            if (dTable.Rows[0]["strong_side"].Equals(DBNull.Value))
        //                _strong_side = string.Empty;
        //            else
        //                _strong_side = (string)dTable.Rows[0]["strong_side"];
        //            if (dTable.Rows[0]["weak_side"].Equals(DBNull.Value))
        //                _weak_side = string.Empty;
        //            else
        //                _weak_side = (string)dTable.Rows[0]["weak_side"];
        //            if (dTable.Rows[0]["remark"].Equals(DBNull.Value))
        //                _remark = string.Empty;
        //            else
        //                _remark = (string)dTable.Rows[0]["remark"];

        //        if (dTable.Rows[0]["budgetyear"].Equals(DBNull.Value))
        //            _budget_year = 0;
        //        else
        //            _budget_year = (int)dTable.Rows[0]["budgetyear"];
        //    }

        //        return dTable;
        //    }
        //    catch (Exception ex)
        //    {
        //     //   throw new Exception("CEvaluation::GetRecord::Error!" + Resources.Message.MSG_OPERATIONNOTPERFORMED, ex);
        //    }
        //    finally
        //    {
        //        connection.Close();
        //        command.Dispose();
        //        adapter.Dispose();
        //    }
        //}

        public DataTable GetRecords()
        {
            SqlConnection connection = new SqlConnection(_ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandText = "dbo.[tblEvaluation_GetRecords]";
            command.CommandType = CommandType.StoredProcedure;
            DataTable dTable = new DataTable("tblEvaluation");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@ParentGuid", _parentGuid));
            //    command.Parameters.Add(new SqlParameter("@LangId", _LangId));
            connection.Open();
                adapter.Fill(dTable);
                if (dTable.Rows.Count > 0)
                {
                    _mainGuid = (Guid)dTable.Rows[0]["MainGuid"];
                    //_seq_no = (int)dTable.Rows[0]["seq_no"];

                    //if (dTable.Rows[0]["ParentGuid"].Equals(DBNull.Value))
                    //    _parentGuid = Guid.Empty;
                    //else
                    //    _parentGuid = (Guid)dTable.Rows[0]["ParentGuid"];


                    if (dTable.Rows[0]["fiscal_year"].Equals(DBNull.Value))
                        _fiscal_year = DateTime.MinValue;
                    else
                        _fiscal_year = (DateTime)dTable.Rows[0]["fiscal_year"];
                    if (dTable.Rows[0]["period_text"].Equals(DBNull.Value))
                        _period_text = string.Empty;
                    else
                        _period_text = (string)dTable.Rows[0]["period_text"];
                    if (dTable.Rows[0]["eval_point"].Equals(DBNull.Value))
                        _eval_point = 0;
                    else
                        _eval_point = (double)dTable.Rows[0]["eval_point"];

                    //EEE
                    if (dTable.Rows[0]["budgetyear"].Equals(DBNull.Value))
                    _budget_year = 0;
                    else
                    _budget_year  = (Int32)dTable.Rows[0]["budgetyear"];


                    //
                //if (dTable.Rows[0]["eval_year"].Equals(DBNull.Value))
                //    _eval_year = 0;
                //else
                //    _eval_year = (int)dTable.Rows[0]["eval_year"];


                //if (dTable.Rows[0]["EvaluationLevel"].Equals(DBNull.Value))
                //    _EvaluationLevel = 0;
                //else
                //    _EvaluationLevel = (int)dTable.Rows[0]["EvaluationLevel"];

                //if (dTable.Rows[0]["strong_side"].Equals(DBNull.Value))
                //    _strong_side = string.Empty;
                //else
                //    _strong_side = (string)dTable.Rows[0]["strong_side"];
                //if (dTable.Rows[0]["weak_side"].Equals(DBNull.Value))
                //    _weak_side = string.Empty;
                //else
                //    _weak_side = (string)dTable.Rows[0]["weak_side"];
                //if (dTable.Rows[0]["remark"].Equals(DBNull.Value))
                //    _remark = string.Empty;
                //else
                //    _remark = (string)dTable.Rows[0]["remark"];
            }

            return dTable;
            }
            catch (Exception ex)
            {
                throw new Exception("CEvaluation::GetRecord::Error!" + " ", ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
        }
            //EEE
            public int ExistEvalation(Guid ParentGuid, Int32 Budget_year ,int Period)
            {
                SqlConnection connection = new SqlConnection(ConnectionString);

                string strExists = @"SELECT seq_no FROM [dbo].[tblEvaluation] WHERE [ParentGuid]=@ParentGuid AND
                                         [BudgetYear]=@BudgetYear AND [period] =@period";

                SqlCommand command = new SqlCommand() { CommandText = strExists, CommandType = CommandType.Text };
                command.Connection = connection;

                try
                {
                    string currentDate = DateTime.Now.ToString();
                    command.Parameters.Add(new SqlParameter("@ParentGuid", ParentGuid));
                    command.Parameters.Add(new SqlParameter("@BudgetYear", Budget_year));
            command.Parameters.Add(new SqlParameter("@period", Period));

            
                    connection.Open();
                    return Convert.ToInt32(command.ExecuteScalar());
                }
                catch (Exception ex)
                {
                    throw new Exception("tblApplicant::Exists::Error!" + ex.Message, ex);
                }
                finally
                {
                    connection.Close();
                    command.Dispose();
                }
            }

    //

    //EEE
   
    }
