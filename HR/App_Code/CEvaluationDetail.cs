﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;

namespace EvaluationDetail
{
    public class CEvaluationDetail
    {


        string _ConnectionString = string.Empty;
        public CEvaluationDetail()
        {
            _ConnectionString = ConnectionString;
        }
        public CEvaluationDetail(Guid EvaluationDetailGuid, Guid EvaluationGuid, Guid EvaluationChecklistGuid, string EvaluationCriteria, string EvluationResult)
        {
            _evaluationDetailGuid = EvaluationDetailGuid;
            _evaluationGuid = EvaluationGuid;
            _evaluationChecklistGuid = EvaluationChecklistGuid;
            _evaluationCriteria = EvaluationCriteria;
            _evluationResult = EvluationResult;
        }
        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            }
        }


        #region Class Property Declarations
        Guid _evaluationDetailGuid;
        public Guid EvaluationDetailGuid
        {
            get { return _evaluationDetailGuid; }
            set { _evaluationDetailGuid = value; }
        }

        Guid _evaluationGuid;
        public Guid EvaluationGuid
        {
            get { return _evaluationGuid; }
            set { _evaluationGuid = value; }
        }
        Guid _parentGuid;
        public Guid ParentGuid
        {
            get { return _parentGuid;  }
            set { _parentGuid = value; }
        }

        Guid _evaluationChecklistGuid;
        public Guid EvaluationChecklistGuid
        {
            get { return _evaluationChecklistGuid; }
            set { _evaluationChecklistGuid = value; }
        }

        string _evaluationCriteria;
        public string EvaluationCriteria
        {
            get { return _evaluationCriteria; }
            set { _evaluationCriteria = value; }
        }

        string _evluationResult;
        public string EvluationResult
        {
            get { return _evluationResult; }
            set { _evluationResult = value; }
        }

        #endregion

 
        public DataTable Records()
        {
            SqlConnection connection = new SqlConnection(_ConnectionString);
            SqlCommand command = new SqlCommand();
            command.CommandText = "dbo.[tblEvaluationDetail_Records]";
            command.CommandType = CommandType.StoredProcedure;
            DataTable dTable = new DataTable("tblEvaluationDetail");
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            command.Connection = connection;

            try
            {
                command.Parameters.Add(new SqlParameter("@EvaluationGuid", _evaluationGuid));
                connection.Open();
                adapter.Fill(dTable);
                return dTable;
            }
            catch (Exception ex)
            {
                throw new Exception("CtblEvaluationDetail::GetRecords::Error!" + " ", ex);
            }
            finally
            {
                connection.Close();
                command.Dispose();
                adapter.Dispose();
            }
        }
      

    }
}