﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
////using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
////using System.Xml.Linq;
using DevExpress.Web;

namespace CUSTOR
{
    /// <summary>
    /// Summary description for CGlobals
    /// </summary>
    public static class CGlobals
    {
        public enum enumToolbar
        {
            eNew = 0,
            eSave = 2,
            eDelete = 4,
            eRefresh = 6,
            eFind = 8,
            ePrint = 10,
            eEdit = 12
        }
        public enum UnitType
        {
            Federal,
            Region,
            Zone,
            Woreda
        }
        public enum eLanguages
        {
            English = 1,
            Amharic = 2,
            Tigrigna = 3,
            AfanOromo = 4,
            Afar = 5,
            Somali = 6,
            Arabic = 7
        }
        public enum enumMenuItems
        {
            eLogin = 0,
            eFile = 1,
            eTools = 3,
            eReports = 4,
            eHelp = 5,
            ePassword = 6,
            eLogout = 7,
        }
     
        public enum enumLookupToolbar
        {
            eNew = 0,
            eSave = 2,
            eDelete = 4,
            eCancel = 6,
            eRefresh = 8,
            eFind = 10,
            ePrint = 12,
            eShow = 14,
            eVoid = 16

        }

        public static void FillComboBox(DropDownList cboName, string strSQL, string strTextField, string strConnection)
        {

            SqlCommand cmd = null;
            SqlDataReader dr = null;
            SqlConnection cn = new SqlConnection(strConnection);
            using (cn)
            {

                using (cmd = new SqlCommand())
                {

                    cmd.CommandText = strSQL;
                    cmd.CommandType = CommandType.Text;
                    cn.Open();
                    cmd.Connection = cn;
                    using (dr = cmd.ExecuteReader())
                    {
                        cboName.Items.Clear();
                        cboName.DataTextField = strTextField;
                        //cboName.DataValueField = strValueField;
                        cboName.DataSource = dr;
                        cboName.DataBind();
                        dr.Close();
                    }
                }
            }
        }
        public static void FillComboFromDB(DevExpress.Web.ASPxComboBox cboName, string strSQL, string strTextField, string strValueField)
        {

            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string strConnection = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection cn = new SqlConnection(strConnection);
            using (cn)
            {

                using (cmd = new SqlCommand())
                {

                    cmd.CommandText = strSQL;
                    cmd.CommandType = CommandType.Text;
                    cn.Open();
                    cmd.Connection = cn;

                    using (dr = cmd.ExecuteReader())
                    {
                        cboName.Items.Clear();

                        cboName.TextField = strTextField;
                        cboName.ValueField = strValueField;
                        cboName.DataSource = dr;
                        cboName.DataBind();
                        dr.Close();
                    }
                }
            }
        }
        public static void FillComboFromDB(DevExpress.Web.ASPxComboBox cboName, string strSQL, string strTextField, string strValueField, string strConnection)
        {

            SqlCommand cmd = null;
            SqlDataReader dr = null;
            SqlConnection cn = new SqlConnection(strConnection);
            using (cn)
            {

                using (cmd = new SqlCommand())
                {

                    cmd.CommandText = strSQL;
                    cmd.CommandType = CommandType.Text;
                    cn.Open();
                    cmd.Connection = cn;

                    using (dr = cmd.ExecuteReader())
                    {
                        cboName.Items.Clear();

                        cboName.TextField = strTextField;
                        cboName.ValueField = strValueField;
                        cboName.DataSource = dr;
                        cboName.DataBind();
                        dr.Close();
                    }
                }
            }
        }
        public static void FillComboFromDB(DropDownList cboName, string strSQL, string strTextField, string strValueField, string strConnection)
        {

            SqlCommand cmd = null;
            SqlDataReader dr = null;
            SqlConnection cn = new SqlConnection(strConnection);
            using (cn)
            {

                using (cmd = new SqlCommand())
                {

                    cmd.CommandText = strSQL;
                    cmd.CommandType = CommandType.Text;
                    cn.Open();
                    cmd.Connection = cn;

                    using (dr = cmd.ExecuteReader())
                    {
                        cboName.Items.Clear();

                        cboName.DataTextField = strTextField;
                        cboName.DataValueField = strValueField;
                        cboName.DataSource = dr;
                        cboName.DataBind();
                        dr.Close();
                    }
                }
            }
        }

        
        public static void FillComboFromDB(DropDownList cboName, string strSQL, string strTextField, string strValueField)
        {

            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string strConnection = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection cn = new SqlConnection(strConnection);
            using (cn)
            {

                using (cmd = new SqlCommand())
                {

                    cmd.CommandText = strSQL;
                    cmd.CommandType = CommandType.Text;
                    cn.Open();
                    cmd.Connection = cn;

                    using (dr = cmd.ExecuteReader())
                    {
                        cboName.Items.Clear();

                        cboName.DataTextField = strTextField;
                        cboName.DataValueField = strValueField;
                        cboName.DataSource = dr;
                        cboName.DataBind();
                        dr.Close();
                    }
                }
            }
        }

        public static void FillComboXFromDB(ASPxComboBox cboName, string strSQL, string strTextField, string strValueField)
        {

            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string strConnection = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlConnection cn = new SqlConnection(strConnection);
            using (cn)
            {

                using (cmd = new SqlCommand())
                {

                    cmd.CommandText = strSQL;
                    cmd.CommandType = CommandType.Text;
                    cn.Open();
                    cmd.Connection = cn;

                    using (dr = cmd.ExecuteReader())
                    {
                        cboName.Items.Clear();

                        cboName.TextField = strTextField;
                        cboName.ValueField = strValueField;
                        cboName.DataSource = dr;
                        cboName.DataBind();
                        dr.Close();
                    }
                }
            }
        }

        private static void AddComboItem(DropDownList cboLang, string strText, int Lang)
        {
            ListItem li = new ListItem(strText, Convert.ToString(Lang));
            cboLang.Items.Add(li);
        }

        //get the root
        public static string GetUrl()
        {
            string strURL = ConfigurationManager.AppSettings["AppURL"];
            if (strURL.EndsWith("/") || strURL.EndsWith(@"\"))
                return strURL;
            else
                return strURL + "/";
        }


        public static string GetUrl(string url)
        {


            string orginalUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            if (HttpContext.Current.Request.Url.Query.Length > 0)
                orginalUrl = orginalUrl.Replace(HttpContext.Current.Request.Url.Query, string.Empty);

            return orginalUrl.Replace(HttpContext.Current.Request.Url.AbsolutePath, string.Empty) +
              ((HttpContext.Current.Request.ApplicationPath == "/" ?
              "" : HttpContext.Current.Request.ApplicationPath)) + '/' + url;


        }

        public static void FillLanguages(DropDownList cboLang)
        {
            cboLang.Items.Clear();
            AddComboItem(cboLang, eLanguages.English.ToString(), (int)eLanguages.English);
            AddComboItem(cboLang, eLanguages.Amharic.ToString(), (int)eLanguages.Amharic);
            AddComboItem(cboLang, eLanguages.Tigrigna.ToString(), (int)eLanguages.Tigrigna);
            AddComboItem(cboLang, eLanguages.AfanOromo.ToString(), (int)eLanguages.AfanOromo);
            AddComboItem(cboLang, eLanguages.Afar.ToString(), (int)eLanguages.Afar);
            AddComboItem(cboLang, eLanguages.Somali.ToString(), (int)eLanguages.Somali);
            AddComboItem(cboLang, eLanguages.Arabic.ToString(), (int)eLanguages.Arabic);
        }
        public static void ShowMessage(string strText, Panel pnlMsg, Label lblMsg)
        {
            if (strText.Length > 0)
            {
                pnlMsg.CssClass = "messageInformational";
                lblMsg.Text = strText.Replace("\n", "<br />");
                pnlMsg.Visible = true;
            }
            else
                pnlMsg.Visible = false;

        }
        public static void ShowError(string strText, Panel pnlMsg, Label lblMsg)
        {
            if (strText.Length > 0)
            {
                pnlMsg.CssClass = "messageError";
                lblMsg.Text = strText.Replace("\n", "<br />");
                pnlMsg.Visible = true;
            }
            else
                pnlMsg.Visible = false;
        }
    }
}