﻿ 
using System.Web.UI.WebControls;
using DevExpress.Web;
using DevExpress.Web.ASPxHtmlEditor;

namespace CUSTOR
{
public class CMsgBar{

    public static void ShowMessage(string strText, Panel pnlMsg, Label lblMsg)
    {
        if (strText.Length > 0)
        {
            pnlMsg.CssClass = "messageInformational";
            lblMsg.Text = strText.Replace("\n", "<br />");
            pnlMsg.Visible = true;
        }
        else
            pnlMsg.Visible = false;

    }
    public static void ShowSucess(string strText, Panel pnlMsg, Label lblMsg)
    {
        if (strText.Length > 0)
        {
            pnlMsg.CssClass = "alert alert-success";
            lblMsg.Text = strText.Replace("\n", "<br />");
            pnlMsg.Visible = true;
        }
        else
            pnlMsg.Visible = false;

    }
    public static void ShowError(string strText, Panel pnlMsg, Label lblMsg)
    {
        if (strText.Length > 0)
        {
            pnlMsg.CssClass = "alert alert-danger";
            lblMsg.Text = strText.Replace("\n", "<br />");
            pnlMsg.Visible = true;
        }
        else
            pnlMsg.Visible = false;
    }

    public static void ShowError(string strText,  ASPxPanel pnlMsg, Label lblMsg)
    {
        if (strText.Length > 0)
        {
            pnlMsg.CssClass = "messageError";
            lblMsg.Text = strText.Replace("\n", "<br/>");
            pnlMsg.Visible = true;
        }
        else
            pnlMsg.Visible = false;
    }

    public static void ShowInfo(string strText, Panel pnlMsg, Label lblMsg)
    {
        if (strText.Length > 0)
        {
            pnlMsg.CssClass = "alert alert-info";
            lblMsg.Text = strText.Replace("\n", "<br />");
            pnlMsg.Visible = true;
        }
        else
            pnlMsg.Visible = false;
    }

    public static void ShowInfo(string strText, ASPxPanel pnlMsg, Label lblMsg)
    {
        if (strText.Length > 0)
        {
            pnlMsg.CssClass = "alert alert-info";
            lblMsg.Text = strText.Replace("\n", "<br />");
            pnlMsg.Visible = true;
        }
        else
            pnlMsg.Visible = false;
    }

}
}