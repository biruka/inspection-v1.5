﻿
using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using DevExpress.Web;

namespace CUSTOR
{
    /// <summary>
    /// Summary description for CCommon
    /// </summary>
    public class CCommon
    {
        public CCommon()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static String getMainOfficeID(string strSQL)
        {
            string strConnection = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlCommand cmd = null;
            string mainOfficeCode = "";
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(strConnection);
            SqlDataAdapter adapter = new SqlDataAdapter(strSQL, con);
            adapter.Fill(dt);
            SqlDataReader dr = null;

            if (dt.Rows.Count > 0)
            {
                mainOfficeCode = dt.Rows[0]["code"].ToString();
            }
            return mainOfficeCode;
        }

        public static void FillComboFromDB(DropDownList cboName, string strSQL, string strTextField, string strValueField)
        {

            string strConnection = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            SqlConnection cn = new SqlConnection(strConnection);
            using (cn)
            {

                using (cmd = new SqlCommand())
                {

                    cmd.CommandText = strSQL;
                    cmd.CommandType = CommandType.Text;
                    cn.Open();
                    cmd.Connection = cn;

                    using (dr = cmd.ExecuteReader())
                    {
                        cboName.Items.Clear();
                        cboName.DataSource = dr;
                        cboName.DataTextField = strTextField;
                        cboName.DataValueField = strValueField;

                        cboName.DataBind();
                        dr.Close();
                    }
                }
            }
        }

        public static void FillAspxComboFromDB(ASPxComboBox cboName, string strSQL, string strTextField, string strValueField)
        {
            try
            {
                string strConnection = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                SqlCommand cmd = null;
                SqlDataReader dr = null;
                SqlConnection cn = new SqlConnection(strConnection);
                using (cn)
                {

                    using (cmd = new SqlCommand())
                    {

                        cmd.CommandText = strSQL;
                        cmd.CommandType = CommandType.Text;
                        cn.Open();
                        cmd.Connection = cn;

                        using (dr = cmd.ExecuteReader())
                        {
                            cboName.Items.Clear();
                            cboName.DataSource = dr;
                            cboName.TextField = strTextField;
                            cboName.ValueField = strValueField;

                            cboName.DataBind();
                            dr.Close();
                        }
                    }
                }
            }
            catch
            {
            }
        }
        private static void AddComboItem(DropDownList cboLang, string strText, int Lang)
        {
            ListItem li = new ListItem(strText, Convert.ToString(Lang));
            cboLang.Items.Add(li);
        }

        public static void FillListBoxFromDB(ListBox lstBoxName, string strSQL, string strTextField, string strValueField)
        {

            string strConnection = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            SqlConnection cn = new SqlConnection(strConnection);
            using (cn)
            {

                using (cmd = new SqlCommand())
                {

                    cmd.CommandText = strSQL;
                    cmd.CommandType = CommandType.Text;
                    cn.Open();
                    cmd.Connection = cn;

                    using (dr = cmd.ExecuteReader())
                    {
                        lstBoxName.Items.Clear();

                        lstBoxName.DataTextField = strTextField;
                        lstBoxName.DataValueField = strValueField;
                        lstBoxName.DataSource = dr;
                        lstBoxName.DataBind();
                        dr.Close();
                    }
                }
            }
        }

        public static void FillASPxCheckBoxListFromDB(ASPxCheckBoxList lstBoxName, string strSQL, string strTextField, string strValueField)
        {

            string strConnection = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            SqlConnection cn = new SqlConnection(strConnection);
            using (cn)
            {

                using (cmd = new SqlCommand())
                {

                    cmd.CommandText = strSQL;
                    cmd.CommandType = CommandType.Text;
                    cn.Open();
                    cmd.Connection = cn;

                    using (dr = cmd.ExecuteReader())
                    {
                        lstBoxName.Items.Clear();

                        lstBoxName.TextField = strTextField;
                        lstBoxName.ValueField = strValueField;
                        lstBoxName.DataSource = dr;
                        lstBoxName.DataBind();
                        dr.Close();
                    }
                }
            }
        }

        public static int GetComboIndex(DropDownList cbo, string paramStringValue, int paramIntValue)
        {
            for (int i = 0; i < cbo.Items.Count; i++)
            {
                if (paramStringValue != "")
                {
                    if (cbo.Items[i].Text.ToLower() == paramStringValue.ToLower())
                    {
                        return i;
                    }
                }
                else
                {
                    if (cbo.Items[i].Value == paramIntValue.ToString())
                    {
                        return i;
                    }
                }
            }
            return -1;
        }
        public static int GetComboIndexAspx(DevExpress.Web.ASPxComboBox cbo, string paramStringValue, int paramIntValue)
        {
            for (int i = 0; i < cbo.Items.Count; i++)
            {
                if (paramStringValue != "")
                {
                    if (cbo.Items[i].Text.ToLower() == paramStringValue.ToLower())
                    {
                        return i;
                    }
                }
                else
                {
                    if (cbo.Items[i].Value == paramIntValue.ToString())
                    {
                        return i;
                    }
                }
            }
            return -1;
        }


        public static int GetComboIndex(DropDownList cbo, string paramStringValue, int paramIntValue, bool isGuid)
        {
            for (int i = 0; i < cbo.Items.Count; i++)
            {
                if (paramStringValue != "")
                {
                    if (isGuid)
                    {
                        if (cbo.Items[i].Value.ToLower() == paramStringValue.ToString().ToLower())
                        {
                            return i;
                        }
                    }
                    else
                    {
                        if (cbo.Items[i].Text.ToLower() == paramStringValue.ToLower())
                        {
                            return i;
                        }
                    }
                }
                else
                {
                    if (cbo.Items[i].Value == paramIntValue.ToString())
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        public static int GetComboIndex(DropDownList cbo, string paramStringValue, double paramIntValue)
        {
            for (int i = 0; i < cbo.Items.Count; i++)
            {
                if (paramStringValue != "")
                {
                    if (cbo.Items[i].Text.ToLower() == paramStringValue.ToLower())
                    {
                        return i;
                    }
                }
                else
                {
                    if (cbo.Items[i].Value == paramIntValue.ToString())
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        public static string GetOrganizationType(Guid OrgGuid)
        {
            string strConnection = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(strConnection))
            {
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT OrgType FROM Organization WHERE OrgGuid = @OrgGuid";
                    command.Parameters.AddWithValue("@OrgGuid", OrgGuid);

                    connection.Open();
                    try
                    {
                        string OrgType = (string)command.ExecuteScalar();
                        return OrgType;
                    }
                    catch
                    {
                        return "";
                    }


                }
            }

        }
        public string  GetDateFactor(DateTime nextDay)
        {
            string m = nextDay.ToString();
            try
            {
                m = m.Substring(0, m.LastIndexOf('ጡ'));
            }
            catch { }
            try
            {
                m = m.Substring(0, m.LastIndexOf('ከ'));
            }
            catch { }
            try
            {
                m = m.Substring(0, m.LastIndexOf('A'));
            }
            catch { }
            try
            {
                m = m.Substring(0, m.LastIndexOf('P'));
            }
            catch { }
            try
            {
                m = m.Substring(0, m.LastIndexOf('ጧ'));
            }
            catch { }

            return  m ;
        }

    }
}

