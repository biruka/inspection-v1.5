﻿using CUSTOR.Bussiness;
using CUSTOR.Commen;
using CUSTOR.Domain;
using DevExpress.Web;
using System;
using System.Net;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;

public partial class ComplainDetail : BasePage
{
    const string UploadDirectory = "~/Attached/";
    const int ThumbnailSize = 100;
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["PesrsonGuid"] = Session["UserID"];
        if (!Page.IsPostBack)
        {
            if (Session["ProLanguage"] != null)
            {

                HidlLanguage.Value = Session["ProLanguage"].ToString();
                if (Session["UserID"] != null)
                {
                    HidUserid.Value = Session["UserID"].ToString();
                    LoadOrganizations(Guid.Parse(Session["ProfileOrgGuid"].ToString()), Session["ProfileOrgCatagory"].ToString(), int.Parse(HidlLanguage.Value));
                    GetActiveOrganizations();
                }
            }
            txtDateFrom.SelectedDate = Convert.ToString((EthiopicDateTime.GetEthiopicDate(int.Parse(DateTime.Today.Day.ToString()), int.Parse(DateTime.Today.Month.ToString()), int.Parse(DateTime.Today.Year.ToString()))));
            txtDateTo.SelectedDate = Convert.ToString((EthiopicDateTime.GetEthiopicDate(int.Parse(DateTime.Today.Day.ToString()), int.Parse(DateTime.Today.Month.ToString()), int.Parse(DateTime.Today.Year.ToString()))));
            LoadComplianTypes();
            
            string[] modeType = Request.QueryString["ModeType"].Split(new char[] { '?' });
            ProfileCommon p = this.Profile;
            p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
            switch (modeType[0])
            {
                case "Allude":
                    //lblComplainDetail.Text = this.Page.Title = "የጥቆማ ሀተታዎች";
                    //lblCompliantDetailHeaderText.Text = "የጥቆማ ሀተታ";
                    
                    if (p.Organization.LanguageID == 1)
                    {
                        hdfldApplicationName.Value = "ጥቆማ";
                        lblMeansOfReport.Text = "ጥቆማ የቀረበበት መንገድ:";
                        DvgrdregEdit.Columns["ComplainType"].Caption = "የጥቆማ አይነት";
                        lblComplainType.Text = "የጥቆማ አይነት:";
                        lblreferenceno.Text = "የጥቆማ ቁጥር:";
                        lblComplaintRemark.Text = "የጥቆማ መነሻ:";
                        Session["Grievance"] = false;
                        cboComplainType.ValidationSettings.ErrorText = "እባክዎ የጥቆማ ዓይነት ይምረጡ";
                    }
                    else if (p.Organization.LanguageID == 2)
                    {
                        hdfldApplicationName.Value = "Saaxilaa";
                        lblMeansOfReport.Text = "Haala Saaxilii Itti Dhiyaatte:";
                        DvgrdregEdit.Columns["ComplainType"].Caption = "Gosa Saaxilaa";
                        lblComplainType.Text = "Gosa Saaxilaa:";
                        lblreferenceno.Text = "Lakk. Saxxiila:";
                        lblComplaintRemark.Text = "Jalqabaa Saxxiilaa:";
                        Session["Grievance"] = false;
                    }
                    //LoadMeansOfReport();
                        //LoadComplianTypes();
                        break;
                case "Grievance":

                    if (p.Organization.LanguageID == 1)
                    {
                        hdfldApplicationName.Value = "ቅሬታ";
                        //lblCompliantDetailHeaderText.Text = "የቅሬታ ሀተታ";
                        lblMeansOfReport.Text = "ቅሬታ የቀረበበት መንገድ:";
                        lblComplainType.Text = "የቅሬታ አይነት :";
                        lblreferenceno.Text = "የቅሬታ ቁጥር:";
                        lblComplaintRemark.Text = "የቅሬታ መነሻ:";
                        Session["Grievance"] = true;
                        cboComplainType.ValidationSettings.ErrorText = "እባክዎ የቅሬታ ዓይነት ይምረጡ";
                    }
                    else if (p.Organization.LanguageID == 2)
                    {
                        hdfldApplicationName.Value = "Iyyaataa";
                        lblMeansOfReport.Text = "Haala Iyyaatichi Itti Dhiyaatte:";
                        lblComplainType.Text = " Gosa Iyyaataa:";
                        lblreferenceno.Text = "Lakk.Iyyaataa:";
                        lblComplaintRemark.Text = "Jalqabaa Heyyaata:";
                        Session["Grievance"] = true;
                    }
                    PersonEmploymentEntityView objorg = new PersonEmploymentEntityView();
                    PersonEmploymentEntityViewBussiness objorgBusiness = new PersonEmploymentEntityViewBussiness();
                    break;
            }
            fuAttachment.ClientVisible = false;
            ClearForm();
            //LoadComplianTypes();
            //BindGrid();
            //if (!CAccessRight.CanAccess(CResource.eResource.einspectorofficer) || !CAccessRight.CanAccess(CResource.eResource.einspectordirector) || !CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor))
            //{
            //    Response.Redirect("~/AccessDenied.aspx");
            //}
        }

    }

    protected override void InitializeCulture()
    {
        ProfileCommon p = this.Profile;
        if (p.Organization.LanguageID != null)
        {
            String selectedLanguage = string.Empty;
            switch (p.Organization.LanguageID)
            {
                case 0: selectedLanguage = "en-US";//English
                    break;
                case 2: selectedLanguage = "am-ET";//Afan Oromo
                    break;
                case 3: selectedLanguage = "am-ET"; //Tig
                    break;
                case 4: selectedLanguage = "en-GB";//afar
                    break;
                case 5: selectedLanguage = "en-AU";//Somali
                    break;
                default: break;//Amharic
            }
            UICulture = selectedLanguage;
            Culture = selectedLanguage;

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(selectedLanguage);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(selectedLanguage);
        }
        base.InitializeCulture();
    }

    protected void GetActiveOrganizations()
    {
        try
        {
            OrganizationBussiness obj = new OrganizationBussiness();
            cboOrganization.ValueField = "OrgGuid";
            cboOrganization.TextField = "DescriptionAm";
            List<Organization> l = new List<Organization>();
            l = obj.GetActiveOrganizations();
            if (l.Count > 0)
            {
                cboOrganization.DataSource = l;
                cboOrganization.DataBind();
            }
        }
        catch (Exception ex)
        {
            //MessageBox1.ShowError(ex.ToString());
        }

    }

    protected void LoadCompliantAttachments()
    {
        CompliantAttachmentBusiness objAttachmentBusiness = new CompliantAttachmentBusiness();
        List<CompliantAttachment> objCompliantAttachment = objAttachmentBusiness.GetAttachments(Session["ID"].ToString());
        gvUploadedFiles.DataSource = objCompliantAttachment;
        gvUploadedFiles.DataBind();
    }

    protected void LoadOrganizations(Guid OrganizationGuid, string OrganizationCatagory, int OrganizationLanguage)
    {
        if (!CAccessRight.CanAccess(CResource.eResource.ehrofficer))
        {
            List<Organization> objorgList = new List<Organization>();
            cboOrganization.ValueField = "OrgGuid";
            cboOrganization.TextField = "DescriptionAm";
            cboOrganizationSearch.ValueField = "OrgGuid";
            cboOrganizationSearch.TextField = "DescriptionAm";
            ProfileCommon p = this.Profile;
            p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
            OrganizationBussiness objorgBusiness = new OrganizationBussiness();
            objorgList = objorgBusiness.GetActiveOrganizationsForInspection(Guid.Parse(p.Organization.GUID));
            if (objorgList.Count > 0)
            {
                cboOrganization.DataSource = objorgList;
                cboOrganization.DataBind();
                cboOrganizationSearch.DataSource = objorgList;
                cboOrganizationSearch.DataBind();
                cboOrganization.Value = null;
                cboOrganizationSearch.Value = null;
            }
        }
        else
        {
            Session["OrgGuid"] = Session["ProfileOrgGuid"];// "8ca7db60-a5cc-4f1d-8956-5f52a82afae1"; //
            Organization objorg = new Organization();
            OrganizationBussiness objorgBusiness = new OrganizationBussiness();
            if (Session["OrgGuid"] != null)
            {
                objorg = objorgBusiness.GetOrganization(Guid.Parse(Session["OrgGuid"].ToString()));
                cboOrganization.Items.Insert(0, new ListEditItem(objorg.DescriptionAm, objorg.OrgGuid));
                cboOrganizationSearch.Items.Insert(0, new ListEditItem(objorg.DescriptionAm, objorg.OrgGuid));
                cboOrganization.SelectedIndex = 0;
                cboOrganizationSearch.SelectedIndex = 0;
                cboOrganization.ClientEnabled = false;
                cboOrganizationSearch.ClientEnabled = false;
                cboOrganization.Value = null;
                cboOrganizationSearch.Value = null;
            }
        }
    }

    protected void LoadMeansOfReport()
    {
        if (HidlLanguage.Value == "0")
        {
            FillCombo(cboMeansOfReport, Language.eLanguage.eEnglish, typeof(Enumeration.MeansOfReport));
        }
        else if (HidlLanguage.Value == "1")
        {
            FillCombo(cboMeansOfReport, Language.eLanguage.eAmharic, typeof(Enumeration.MeansOfReport));
        }
        else if (HidlLanguage.Value == "2")
        {
            FillCombo(cboMeansOfReport, Language.eLanguage.eAfanOromo, typeof(Enumeration.MeansOfReport));
        }
    }

    protected void LoadComplianTypes()
    {
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        if (p.Organization.LanguageID == 0)
        {
            FillCombo(cboComplainType, Language.eLanguage.eEnglish, typeof(Enumeration.ComplainTypes));
            FillCombo(cboComplianTypesSearch, Language.eLanguage.eEnglish, typeof(Enumeration.ComplainTypes));
        }
        else if (p.Organization.LanguageID == 1)
        {
            FillCombo(cboComplainType, Language.eLanguage.eAmharic, typeof (Enumeration.ComplainTypes));
            FillCombo(cboComplianTypesSearch, Language.eLanguage.eAmharic, typeof(Enumeration.ComplainTypes));

        }
        else if (p.Organization.LanguageID == 2)
        {
            FillCombo(cboComplainType, Language.eLanguage.eAfanOromo, typeof(Enumeration.ComplainTypes));
            FillCombo(cboComplianTypesSearch, Language.eLanguage.eAfanOromo, typeof(Enumeration.ComplainTypes));
        }
    }

    private void FillCombo(ASPxComboBox ddl, Language.eLanguage eLang, Type t)
    {
        ddl.TextField = "Value";
        ddl.ValueField = "Key";
        ddl.DataSource = EnumHelper.GetEnumDescriptions(t, eLang);
        ddl.DataBind();
        if (HidlLanguage.Value == "1")
        {
            ListEditItem le = new ListEditItem("ይምረጡ", "-1");
            ddl.Items.Insert(0, le);
            ddl.SelectedIndex = 0;
        }
        else if (HidlLanguage.Value == "0")
        {
            ListEditItem le = new ListEditItem("select", "-1");
            ddl.Items.Insert(0, le);
            ddl.SelectedIndex = 0;
        }
        else if (HidlLanguage.Value == "2")
        {
            ListEditItem le = new ListEditItem("Filadha", "-1");
            ddl.Items.Insert(0, le);
            ddl.SelectedIndex = 0;
        }
        
    }

    //protected void LoadOrganizations()
    //{

    //    OrganizationBussiness obj = new OrganizationBussiness();
    //    cboOrganization.ValueField = "OrgGuid";
    //    cboOrganization.TextField = "DescriptionAm";
    //    cboOrganization.DataSource = obj.GetOrganizations();
    //    cboOrganization.DataBind();
    //    ListEditItem le = new ListEditItem("ይምረጡ", "-1");
    //    cboOrganization.Items.Insert(0, le);
    //    cboOrganization.SelectedIndex = 0;
    //    //}                   

    //}
    private void ShowError(string strMsg)
    {
        cbData.JSProperties["cpMessage"] = strMsg;
        cbData.JSProperties["cpStatus"] = "ERROR";
    }

    private string GetErrorDisplay(List<string> strMessages)
    {

        StringBuilder sb = new StringBuilder();
        if (strMessages.Count == 0) return string.Empty;
        sb.Append("Error");
        sb.Append("<ul>");
        foreach (string s in strMessages)
        {
            sb.Append(String.Format("<li>{0}</li>", s));
        }
        sb.Append("</ul>");

        return sb.ToString();

    }

    protected bool AreUserInputsValid()
    {
        bool userInputsAreValid = true;
        DateTime reportedDateFrom = (txtDateFrom.SelectedDate.Equals("") ? new DateTime() : Convert.ToDateTime(txtDateFrom.SelectedGCDate));
        DateTime reportedDateTo = (txtDateTo.SelectedDate.Equals("") ? new DateTime() : Convert.ToDateTime(txtDateTo.SelectedGCDate));
        if (!txtDateTo.SelectedDate.Equals("") && reportedDateFrom > reportedDateTo)
        {
            DvgrdregEdit.JSProperties["cpStatus"] = "Error";
            DvgrdregEdit.JSProperties["cpAction"] = "Searching";
            DvgrdregEdit.JSProperties["cpMessage"] = Resources.Message.MSG_VALIDDATE;
            userInputsAreValid = false;
        }

        return userInputsAreValid;
    }

    private void LoadCompliants()
    {
        try
        {
            DataTable dt = new DataTable();
            ComplaintBusiness objcompliantInformation = new ComplaintBusiness();
            if (AreUserInputsValid())
            {
                ComplaintBusiness compliantInformation = new ComplaintBusiness();
                DateTime reportedDateFrom = (txtDateFrom.SelectedGCDate.ToString().Equals("") ? new DateTime() : txtDateFrom.SelectedGCDate);
                DateTime reportedDateTo = (txtDateTo.SelectedGCDate.ToString().Equals("") ? new DateTime() : txtDateTo.SelectedGCDate);
                Guid orgGuid  = !cboOrganizationSearch.Value.ToString().Equals("-1") ? Guid.Parse(cboOrganizationSearch.Value.ToString()) : Guid.Empty;
                int complainType  = !cboComplianTypesSearch.SelectedItem.Value.ToString().Equals("-1") ? Convert.ToInt32(cboComplianTypesSearch.SelectedItem.Value.ToString()) : -1;
                bool isGrievance = false;
                DataTable dtCompliant = compliantInformation.SearchCompliant(orgGuid, complainType, reportedDateFrom, reportedDateTo, isGrievance, Convert.ToInt32(HidlLanguage.Value));
                if (dtCompliant.Rows.Count > 0)
                {
                    DvgrdregEdit.DataSource = dtCompliant;
                    DvgrdregEdit.DataBind();
                    DvgrdregEdit.JSProperties["cpStatus"] = "Success";
                    DvgrdregEdit.JSProperties["cpAction"] = "Searching";
                }
                else
                {
                    DvgrdregEdit.JSProperties["cpStatus"] = "Error";
                    DvgrdregEdit.JSProperties["cpAction"] = "NoCompliants";
                    DvgrdregEdit.JSProperties["cpMessage"] = Resources.Message.Not_Found;
                }
            }
        }
        catch { }
    }

    private List<string> GetErrorMessage()
    {
        List<string> errMessages = new List<string>();

        if (Int32.Parse(Session["ProLanguage"].ToString()) == 1)
        {
            if (cboOrganization.Value.ToString() == "")
            {

                errMessages.Add(Resources.Message.MSG_SelectrOrganizationName);
            }

            if (cboComplainType.Value.ToString() == "" || cboComplainType.Value.ToString() == "-1")
            {
                errMessages.Add(Resources.Message.MSG_SelectCompliantType);

            }
            if (txtCompliantReason.Text == "")
            {
                errMessages.Add(Resources.Message.MSG_EnterCompliantReason);
            }
        }
        else if (Int32.Parse(Session["ProLanguage"].ToString()) == 2)
        {
            if (cboOrganization.Value.ToString() == "")
            {
                errMessages.Add(Resources.Message.MSG_SelectrOrganizationName);

            }

            if (cboComplainType.Value.ToString() == "" || cboComplainType.Value.ToString() == "-1")
            {
                errMessages.Add(Resources.Message.MSG_SelectCompliantType);

            }
            if (txtCompliantReason.Text == "")
            {
                errMessages.Add(Resources.Message.MSG_EnterCompliantReason);
            }
        }

        return errMessages;
    }

    protected bool DoSave()
    {
        ComplaintBusiness objcomplian = new ComplaintBusiness();
        Complaint complaintInformation = new Complaint();
        bool saved = false;
        List<string> errMessages = GetErrorMessage();
        if (errMessages.Count > 0)
        {

            ShowError(GetErrorDisplay(errMessages));
            saved = false;
        }
        try
        {
            complaintInformation.ComplaintGUID = Guid.NewGuid();
            complaintInformation.OrgGuid = Guid.Parse(cboOrganization.Value.ToString());
            complaintInformation.EmployeeGUID = complaintInformation.ComplaintGUID;
            complaintInformation.ReportedBy = txtFullName.Text;
            complaintInformation.ReporterEmail = txtEmail.Text;
            complaintInformation.ReporterTel = txtTelephone.Text;
            complaintInformation.MeansOfReport = Convert.ToInt32(cboMeansOfReport.Value.ToString());
            if (cboComplainType.Value == null)
                complaintInformation.ScreeningDecision = -1;
            else
                complaintInformation.ScreeningDecision = Convert.ToInt16(cboComplainType.Value.ToString());
            complaintInformation.DateReported = txtDate.SelectedGCDate;
            complaintInformation.ComplaintRemark = txtComplaintRemark.Text;
            complaintInformation.ComplaintDescription = txtCompliantReason.Text;
            complaintInformation.DateScreened = DateTime.Now;
            complaintInformation.FindingSummary = Convert.ToInt32(Enumeration.FindingSummary.NotInspected);
            complaintInformation.ScreenedBy = "";
            complaintInformation.FindingSummaryDecision = "";
            complaintInformation.IsGrievance = Convert.ToBoolean(Session["Grievance"]);
            complaintInformation.Isselfservice = false;
            hdfldCompliantGuid.Value = complaintInformation.ComplaintGUID.ToString();
            if ((bool)Session["IsNew"])
            {
                if (objcomplian.InsertComplaint(complaintInformation))
                {
                    cbData.JSProperties["cpStatus"] = "Success";
                    cbData.JSProperties["cpAction"] = "save";
                    cbData.JSProperties["cpMessage"] = Resources.Message.MSG_SAVED;
                    SaveAttachment();
                    saved = true;
                }
            }
            else
            {
                complaintInformation.ComplaintGUID = new Guid(Session["ID"].ToString());
                hdfldCompliantGuid.Value = complaintInformation.ComplaintGUID.ToString();
                if (objcomplian.UpdateCompliant(complaintInformation))
                {
                    cbData.JSProperties["cpStatus"] = "Success";
                    cbData.JSProperties["cpAction"] = "update";
                    cbData.JSProperties["cpMessage"] = Resources.Message.MSG_SAVED;
                    SaveAttachment();
                    saved = true;
                }
            }
            ComplaintBusiness compliantInformation = new ComplaintBusiness();
            DateTime reportedDateFrom = (txtDateFrom.SelectedGCDate.ToString().Equals("") ? new DateTime() : txtDateFrom.SelectedGCDate);
            DateTime reportedDateTo = (txtDateTo.SelectedGCDate.ToString().Equals("") ? new DateTime() : txtDateTo.SelectedGCDate);
            Guid orgGuid = !(cboOrganizationSearch.Value == null) ? Guid.Parse(cboOrganizationSearch.Value.ToString()) : Guid.Empty;
            int complainType = !cboComplianTypesSearch.SelectedItem.Value.ToString().Equals("-1") ? Convert.ToInt32(cboComplianTypesSearch.SelectedItem.Value.ToString()) : -1;
            bool isGrievance = false;
            DataTable dtCompliant = compliantInformation.SearchCompliant(orgGuid, complainType, reportedDateFrom, reportedDateTo, isGrievance, Convert.ToInt32(HidlLanguage.Value));
            DvgrdregEdit.DataSource = dtCompliant;
            DvgrdregEdit.DataBind();
            ClearForm();
            Session["IsNew"] = false;
        }
        catch (Exception ex)
        {
            ShowError(ex.Message);
            saved = false;
        }
        return saved;
    }

    protected void UploadControl_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        try
        {
            e.CallbackData = SavePostedFiles(e.UploadedFile);
        }
        catch (Exception ex)
        {
            e.IsValid = false;
            e.ErrorText = ex.Message;
        }
    }

    DataTable dtAttachment = new DataTable();

    //List<Object[]> _dataArray = new List<Object[]>();

    //List<AttachmentBussiness.Attdata> _dataArray = new List<double[]>();

    int count = 0;

    static readonly string[] suffixes = { "Bytes", "KB", "MB", "GB", "TB", "PB" };
    public static string FormatSize(double bytes)
    {
        int counter = 0;
        decimal number = (decimal)bytes;
        while (Math.Round(number / 1024) >= 1)
        {
            number = number / 1024;
            counter++;
        }
        return string.Format("{0:n1}{1}", number, suffixes[counter]);
    }

    string SavePostedFiles(UploadedFile uploadedFile)
    {

        if (!uploadedFile.IsValid)
            return string.Empty;
        FileInfo fileInfo = new FileInfo(uploadedFile.FileName);
        string resFileName = MapPath(UploadDirectory) + fileInfo.Name;
        uploadedFile.SaveAs(resFileName);
        CompliantAttachment objCompliantAttachment = new CompliantAttachment();
        if (count == 0)
        {
            System.Type typeString = System.Type.GetType("System.String");
            System.Type typeLong = System.Type.GetType("System.Int64");
            System.Type typeGuid = System.Type.GetType("System.Guid");
            dtAttachment.Columns.Add("AttachmentGuid", typeString);
            dtAttachment.Columns.Add("FileNumber", typeString);
            dtAttachment.Columns.Add("AttachmentTitle", typeString);
            dtAttachment.Columns.Add("DocumentTitle", typeString);
            dtAttachment.Columns.Add("FileSize", typeString);
            dtAttachment.Columns.Add("FileUrl", typeString);
            dtAttachment.Columns.Add("FileType", typeString);
        }
        count++;
        dtAttachment.Rows.Add(new Object[] { Guid.Empty, "test1", fileInfo.Name, fileInfo.Name, uploadedFile.ContentLength, fileInfo.Extension, fileInfo.Extension });
        UploadingUtils.RemoveFileWithDelay(uploadedFile.FileName, resFileName, 5);
        string fileLabel = fileInfo.Name;
        string fileLength = FormatSize(fileInfo.Length);
        string virtualAttachmentLocation = System.Configuration.ConfigurationManager.AppSettings["VirtualAttachmentLocation"];
        fuAttachment.SaveAs(Server.MapPath(Path.Combine(virtualAttachmentLocation, fileLabel)));
        Session["attachedFiles"] = dtAttachment;

        return string.Format("{0} ({1})|{2}", fileLabel, fileLength, fileInfo.Name);
    }

    private void ShowSuccess(string strMsg)
    {
        cbData.JSProperties["cpMessage"] = strMsg;
        cbData.JSProperties["cpStatus"] = "SUCCESS";
    }

    protected void ClearForm()
    {
        string[] modeType = Request.QueryString["ModeType"].Split(new char[] { '?' });
        switch (modeType[0])
        {
            case "Allude":
                txtFullName.Text = string.Empty;
                break;
        }
        txtFullName.Text = string.Empty;
        txtTelephone.Text = string.Empty;
        txtreferenceno.Text = string.Empty;
        txtDate.SelectedDate = Convert.ToString((EthiopicDateTime.GetEthiopicDate(int.Parse(DateTime.Today.Day.ToString()), int.Parse(DateTime.Today.Month.ToString()), int.Parse(DateTime.Today.Year.ToString()))));
        txtEmail.Text = string.Empty;
        cboComplainType.Text = string.Empty;
        txtCompliantReason.Text = string.Empty;
        txtComplaintRemark.Text = string.Empty;
        LoadOrganizations(Guid.Parse(Session["ProfileOrgGuid"].ToString()), Session["ProfileOrgCatagory"].ToString(), int.Parse(HidlLanguage.Value));
        LoadComplianTypes();
        LoadMeansOfReport();
    }

    protected bool DoNew()
    {
        try
        {
            Session["IsNew"] = true;
            ClearForm();
            cbData.JSProperties["cpAction"] = "new";
            cbData.JSProperties["cpStatus"] = "Success";
            cbData.JSProperties["cpMessage"] = string.Empty;
            return true;
        }
        catch (Exception ex)
        {
            ShowError(ex.Message);
            return false;
        }
    }

    protected void CallbackPanel_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {
        string strID = string.Empty;
        string[] parameters = e.Parameter.Split('|');
        string strParam = parameters[0].ToString();
        if (e.Parameter.ToString().Contains('|'))
            strID = parameters[1].ToString();

        switch (strParam)
        {
            case "New":
                DoNew();
                break;
            case "Edit":
                DoFind(strID);
                break;
            case "Save":
                DoSave();
                break;
            case "Delete":
                if (strID.Length > 0)
                    Session["ID"] = new Guid(strID);
                DoDelete();
                break;
            case "Attach":
                DoAttach();
                break;
            default:
                break;

        }
    }

    protected bool DoAttach()
    {
        fuAttachment.ClientVisible = true;
        pnlcompliantList.ClientVisible= false;
        return true;
    }

    protected bool DoFind(string strID)
    {
        Guid ID = Guid.Parse(strID);
        ComplaintBusiness objcomplian = new ComplaintBusiness();
        Complaint complaintInformation = new Complaint();
        try
        {
            bool loaded = false;
            LoadComplianTypes();
            complaintInformation = objcomplian.GettblComplaint(ID);
            if (objcomplian == null)
            {
                ShowMessage("No record was found.");
            }
            else
            {
                DateTime complaindate = complaintInformation.DateReported;
                Session["IsNew"] = false;
                ClearForm();
                lblreferenceno.Text = "የጥቆማ ቁጥር:";
                lblreferenceno.Visible = true;
                this.txtreferenceno.Text = complaintInformation.Referenceno.ToString();
                this.hdfldCompliantGuid.Value = complaintInformation.ComplaintGUID.ToString();
                this.txtFullName.Text = complaintInformation.ReportedBy.ToString();
                this.txtEmail.Text = complaintInformation.ReporterEmail.ToString();
                this.txtTelephone.Text = complaintInformation.ReporterTel;
                this.cboComplainType.Text = complaintInformation.ScreeningDecision.ToString();
                this.txtComplaintRemark.Text = complaintInformation.ComplaintRemark;
                this.txtCompliantReason.Text = complaintInformation.ComplaintDescription;
                this.cboComplainType.Text = complaintInformation.ScreeningDecision.ToString();
                this.cboMeansOfReport.Text = complaintInformation.MeansOfReport.ToString();
                txtDate.SelectedDate = Convert.ToString((EthiopicDateTime.GetEthiopicDate(int.Parse(complaindate.Day.ToString()), int.Parse(complaindate.Month.ToString()), int.Parse(complaindate.Year.ToString()))));
                this.txtreferenceno.Text = complaintInformation.Referenceno;
                cboOrganization.Value = complaintInformation.OrgGuid.ToString();
                Session["ID"] = ID;
                LoadCompliantAttachments();
                cbData.JSProperties["cpAction"] = "Loaded";
                cbData.JSProperties["cpStatus"] = "Success";
                loaded = true;
            }
            return loaded;
        }
        catch (Exception ex)
        {
            ShowError(ex.Message);
            return false;
        }
    }

    protected void LoadOrganizations(Guid OrgGuid)
    {
        try
        {
            Organization objorg = new Organization();
            OrganizationBussiness objorgBusiness = new OrganizationBussiness();
            if (OrgGuid != null)
            {
                //cboOrganization.ValueField = "OrgGuid";
                //cboOrganization.TextField = "DescriptionAm";
                objorg = objorgBusiness.GetOrganization(OrgGuid);
                //string DescriptionAm = 

                cboOrganization.Value = objorg.OrgGuid;
                cboOrganization.Text = objorg.DescriptionAm;
                //cboOrganization.DataBind();
                //ListEditItem le = new ListEditItem("ይምረጡ", "-1");
                //cboOrganization.Items.Insert(0, le);
                //cboOrganization.SelectedIndex = 0;
            }




        }
        catch (Exception ex)
        {
            ShowError(ex.ToString());
        }

    }

    protected bool DoDelete()
    {
        ComplaintBusiness compliant = new ComplaintBusiness(); ;
        ComplaintBusiness objComplaintBusiness = new ComplaintBusiness();
        bool deleted = false;
        try
        {
            if (objComplaintBusiness.Delete(Convert.ToString(Session["ID"].ToString())))
            {
                Session["IsNew"] = false;
                ComplaintBusiness compliantInformation = new ComplaintBusiness();
                DateTime reportedDateFrom = (txtDateFrom.SelectedGCDate.ToString().Equals("") ? new DateTime() : txtDateFrom.SelectedGCDate);
                DateTime reportedDateTo = (txtDateTo.SelectedGCDate.ToString().Equals("") ? new DateTime() : txtDateTo.SelectedGCDate);
                Guid orgGuid = !cboOrganizationSearch.Value.ToString().Equals("-1") ? Guid.Parse(cboOrganizationSearch.Value.ToString()) : Guid.Empty;
                int complainType = !cboComplianTypesSearch.SelectedItem.Value.ToString().Equals("-1") ? Convert.ToInt32(cboComplianTypesSearch.SelectedItem.Value.ToString()) : -1;
                bool isGrievance = false;
                DataTable dtCompliant = compliantInformation.SearchCompliant(orgGuid, complainType, reportedDateFrom, reportedDateTo, isGrievance, Convert.ToInt32(HidlLanguage.Value));
                DvgrdregEdit.DataSource = dtCompliant;
                DvgrdregEdit.DataBind();
                cbData.JSProperties["cpStatus"] = "Success";
                cbData.JSProperties["cpAction"] = "delete";
                cbData.JSProperties["cpMessage"] = Resources.Message.MSG_DELETED;
                deleted = true;
            }
        }
        catch (Exception ex)
        {
            ShowError(ex.Message);
            deleted = false;
        }
        return deleted;
    }

    private void ShowMessage(string strMsg)
    {
        cbData.JSProperties["cpMessage"] = strMsg;
        cbData.JSProperties["cpStatus"] = "INFO";
    }

    private void BindGrid()
    {
        try
        {
            ComplaintBusiness objcompliantInformation = new ComplaintBusiness();
            ProfileCommon p = this.Profile;
            p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
            DvgrdregEdit.DataSource = objcompliantInformation.GetComplaintDetail(Convert.ToBoolean(Session["Grievance"]),p.Organization.LanguageID);
            DvgrdregEdit.DataBind();
        }
        catch { }
    }

    protected void DvgrdregEdit_PageIndexChanged(object sender, EventArgs e)
    {
        LoadCompliants();
    }

    protected bool SaveAttachment()
    {
        bool saved = false;

        CompliantAttachmentBusiness attachment = new CompliantAttachmentBusiness();
        //List<Object[]> gridViewAttachment = (List<Object[]>)Session["attachedFiles"];
        DataTable gridViewAttachment = (DataTable)Session["attachedFiles"];
        if (gridViewAttachment != null)
        {
            for (int index = 0; index < gridViewAttachment.Rows.Count; index++)
            {
                if (gridViewAttachment.Rows[index]["AttachmentGuid"].ToString().Equals(Guid.Empty.ToString()))
                {

                    CompliantAttachment complaintAttachmentInfo = new CompliantAttachment();
                    complaintAttachmentInfo.AttachmentGuid = Guid.NewGuid();
                    complaintAttachmentInfo.CompliantGuid = Guid.Parse(hdfldCompliantGuid.Value);
                    complaintAttachmentInfo.FileNumber = gridViewAttachment.Rows[index]["FileNumber"].ToString();
                    complaintAttachmentInfo.AttachmentTitle = gridViewAttachment.Rows[index]["AttachmentTitle"].ToString();
                    complaintAttachmentInfo.FileSize = float.Parse(gridViewAttachment.Rows[index]["FileSize"].ToString());
                    complaintAttachmentInfo.DocumentTitle = gridViewAttachment.Rows[index]["DocumentTitle"].ToString();
                    string fileUrl = gridViewAttachment.Rows[index]["FileUrl"].ToString();
                    complaintAttachmentInfo.FileUrl = "~/AttachedFile/" + fileUrl;
                    complaintAttachmentInfo.FileType = gridViewAttachment.Rows[index]["FileType"].ToString();
                    if (attachment.InsertAttachment(complaintAttachmentInfo))
                    {
                        saved = true;
                    }
                }
            }
        }


        return saved;
    }

    protected void DvgrdregEdit_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        //ProfileCommon p = this.Profile;
        //p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        //if (e.Column.FieldName == "DateScreened")
        //{
        //    if ((e.Value).ToString().Length != 0)
        //    {
        //        if (e.Value.ToString() == "06/06/1900 12:00:00 AM")
        //            e.DisplayText = "";
        //        else
        //        {
        //            string ethiodate;
        //            ethiodate = EthiopicDateTime.GetEthiopicDate(((DateTime) (e.Value)).Day,
        //                ((DateTime) (e.Value)).Month, ((DateTime) (e.Value)).Year);
        //            e.DisplayText = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate,
        //                Convert.ToInt32(p.Organization.LanguageID));
        //        }
        //    }
        //    else
        //    {
        //        {
        //            return;
        //        }
        //    }
        //}
        //if (e.Column.FieldName == "FindingSummary")
        //{
        //    if (e.Value != null)
        //    {
        //        if (p.Organization.LanguageID == 1)
        //        {
        //            e.DisplayText = EnumHelper.GetEnumDescription(typeof (Enumeration.FindingSummary),
        //                Language.eLanguage.eAmharic, Convert.ToInt32(e.Value));
        //        }
        //        if (p.Organization.LanguageID == 2)
        //        {
        //            e.DisplayText = EnumHelper.GetEnumDescription(typeof (Enumeration.FindingSummary),
        //                Language.eLanguage.eAfanOromo, Convert.ToInt32(e.Value));
        //        }
        //    }
        //}
        //if (e.Column.FieldName == "ComplainType")
        //{
        //    if (e.Value != null)
        //    {
        //        if (p.Organization.LanguageID == 1)
        //        {
        //            e.DisplayText = EnumHelper.GetEnumDescription(typeof (Enumeration.ComplainTypes),
        //                Language.eLanguage.eAmharic, Convert.ToInt32(e.Value));
        //        }
        //        if (p.Organization.LanguageID == 2)
        //        {
        //            e.DisplayText = EnumHelper.GetEnumDescription(typeof (Enumeration.ComplainTypes),
        //                Language.eLanguage.eAfanOromo, Convert.ToInt32(e.Value));
        //        }
        //    }
        //}
    }

    protected void DvgrdregEdit_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
    {
        ProfileCommon profile = this.Profile;
        int language = profile.Organization.LanguageID;
        if (e.RowType != GridViewRowType.Data) return;

        string statusdecision = Convert.ToString(e.GetValue("FindingSummary"));
        if (statusdecision == EnumHelper.GetEnumDescription(typeof(Enumeration.FindingSummary), (Language.eLanguage)language, (int)Enumeration.FindingSummary.NotInspected))
        {
            e.Row.BackColor = Color.LightCyan; // Changes The BackColor of ENTIRE ROW
            e.Row.ForeColor = Color.DarkRed; // Change the Font Color of ENTIRE ROW
        }

        else if (statusdecision == EnumHelper.GetEnumDescription(typeof(Enumeration.FindingSummary), (Language.eLanguage)language, (int)Enumeration.FindingSummary.Accepted) ||
                 statusdecision == EnumHelper.GetEnumDescription(typeof(Enumeration.FindingSummary), (Language.eLanguage)language, (int)Enumeration.FindingSummary.Incomplete) ||
                 statusdecision == EnumHelper.GetEnumDescription(typeof(Enumeration.FindingSummary), (Language.eLanguage)language, (int)Enumeration.FindingSummary.Invalid))
        {
            e.Row.Font.Bold = true; // Changes The BackColor of ENTIRE ROW
            if (e.Row.Cells.Count > 1)
            {
                e.Row.Cells[4].Controls[0].Visible = true;
                //e.Row.Cells[4].Text = "አሳይ";
                e.Row.Cells[4].Controls[1].Visible = false;
            }
        }
    }

    protected void DvgrdregEdit_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        LoadCompliants();
    }

    protected void gvUploadedFiles_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        string strID = string.Empty;
        string[] parameters = e.Parameters.Split('|');
        string strParam = parameters[0].ToString();
        switch (strParam)
        {
            case "DownLoad":
                string fileAttachmentLocation = System.Configuration.ConfigurationManager.AppSettings["fileAttachmentLocation"];
                string fileLocalton = fileAttachmentLocation + @"\" + parameters[1];
                string url = "~/Pages/Download.aspx?fileUrl=" + fileLocalton+ "&fileName="+ parameters[1];
                DevExpress.Web.ASPxWebControl.RedirectOnCallback(url);
                break;
        }
    }
}