﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Bussiness;
using CUSTOR.Commen;
using DevExpress.Web;

public partial class Pages_VacancyAnnouncement : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    { 
    }

    protected void DvgwVanacncy_BeforePerformDataSelect(object sender, EventArgs e)
    {
        if (Session["FileType"] != null)
        {
            string Hrfiletype = Convert.ToString(Session["FileType"].ToString());
            Session["person"] = (sender as ASPxGridView).GetMasterRowKeyValue();
            Session["HRFileType"] = Hrfiletype;
        }
        else { Session["HRFileType"] = ""; }
    }

    int count = 0;
    protected void DvgwVanacncy_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        ASPxGridView gridView = (ASPxGridView)DvgwVanacncy;
        if (gridView != null)
        {
            ASPxLabel lblvacdate = gridView.FindRowTemplateControl(0, "lblvacdate") as ASPxLabel;
            if (lblvacdate != null)
            {
                if (count == 0)
                {
                    ProfileCommon p = this.Profile;
                    p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
                    DateTime vacdate = Convert.ToDateTime(lblvacdate.Text);
                    string date = EthiopicDateTime.GetEthiopicDate(int.Parse(vacdate.Day.ToString()), int.Parse(vacdate.Month.ToString()), int.Parse(vacdate.Year.ToString())).ToString();
                    lblvacdate.Text = EthiopicDateTime.TranslateEthiopicDateMonth(date,Convert.ToInt32(p.Organization.LanguageID));
                    count = count + 1;
                }
            }
        }
    }

    protected void DvgwVanacncy_OnCustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        throw new NotImplementedException();
    }

    protected void LoadVacancy()
    {
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        Guid person = Guid.Parse(Session["APersonGuid"].ToString());
        string documentType = Session["DocumentType"].ToString();
        EmploymentAnnouncementBussiness promotions = new EmploymentAnnouncementBussiness();
        DvgwVanacncy.DataSource = promotions.GetRecords(person, documentType, p.Organization.LanguageID.ToString());
        DvgwVanacncy.DataBind();
    }

    protected void DvgwVanacncy_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        LoadVacancy();
    }
}