﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ImagePopup.ascx.cs" Inherits="Controls_ImagePopup" %>
<%@ Register Assembly="DevExpress.Web.v19.2" Namespace="DevExpress.Web"
    TagPrefix="dx" %>

<dx:ASPxPopupControl ID="ASPxPopupControl1" runat="server" ClientInstanceName="pcImageDialog"
    PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Modal="true"
    Height="470px" ShowShadow="False" Width="644px"
    ShowHeader="False" ShowPageScrollbarWhenModal="true">
    <CloseButtonImage Url="~/Content/Images/pcButtonClose.png">
    </CloseButtonImage>
    <ModalBackgroundStyle Opacity="80" BackColor="Black">
    </ModalBackgroundStyle>
    <ContentCollection>
        <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
            <div class="pcPreviewCloseBtn" onclick="pcImageDialog.Hide()">
                <img runat="server" title="" alt="Close" src="~/Content/Images/pcButtonClose.png" /></div>
                <div class="clear"></div>
            <div class="pcPreviewImage">
                <dx:ASPxImage ID="ASPxImage1" runat="server" ClientInstanceName="previewImage">
                </dx:ASPxImage>
            </div>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>
