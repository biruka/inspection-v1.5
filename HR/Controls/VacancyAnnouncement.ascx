﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VacancyAnnouncement.ascx.cs" Inherits="Pages_VacancyAnnouncement" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>

<dx:ASPxGridView ID="DvgwVanacncy" runat="server" ClientInstanceName="DvgwVanacncy" OnCustomCallback="DvgwVanacncy_OnCustomCallback" AutoGenerateColumns="False"  OnBeforePerformDataSelect="DvgwVanacncy_BeforePerformDataSelect"
    Width="100%" OnHtmlRowPrepared="DvgwVanacncy_HtmlRowPrepared" Theme="Moderno" meta:resourcekey="DvgwVanacncyResource1">
    <Columns>
        <dx:GridViewBandColumn ShowInCustomizationForm="True" VisibleIndex="0" Caption="ክፍት የስራ ቦታ ማስታወቂያ" meta:resourcekey="GridViewBandColumnResource1">
            <Columns>
                <dx:GridViewDataTextColumn FieldName="Announcement_Date" ShowInCustomizationForm="True" Visible="False" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource13">
                </dx:GridViewDataTextColumn>
            </Columns>
            <HeaderStyle Font-Size="Larger" HorizontalAlign="Center" />
        </dx:GridViewBandColumn>
    </Columns>
    <Templates>
        <DataRow>
            <table class="table-condensed">
                <tr>
                    <td align="right" class="auto-style19">
                        <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="የስራ መደብ መጠሪያ" meta:resourcekey="ASPxLabel9Resource1">
                        </dx:ASPxLabel>
                    </td>
                    <td align="left" class="auto-style21">
                        <dx:ASPxLabel ID="lbljob_title" runat="server" Text='<%# Eval("job_title") %>' meta:resourcekey="lbljob_titleResource1">
                        </dx:ASPxLabel>
                    </td>
                    <td align="right" class="auto-style22">
                        <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="ደረጃ" meta:resourcekey="ASPxLabel10Resource1">
                        </dx:ASPxLabel>
                    </td>
                    <td align="left" class="value">
                        <dx:ASPxLabel ID="lblgrade" runat="server" Text='<%# Eval("AmGrade") %>' meta:resourcekey="lblgradeResource1">
                        </dx:ASPxLabel>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="right" class="auto-style19">
                        <dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="ደመወዝ" meta:resourcekey="ASPxLabel12Resource1">
                        </dx:ASPxLabel>
                    </td>
                    <td align="left" class="value" style="height: 28px; width: 144px">
                        <dx:ASPxLabel ID="lblinitialsalary" runat="server" Text='<%# Eval("initial_salary") %>' meta:resourcekey="lblinitialsalaryResource1">
                        </dx:ASPxLabel>
                    </td>
                    <td align="right" class="auto-style23">
                        <dx:ASPxLabel ID="ASPxLabel14" runat="server" Text="ብዛት" meta:resourcekey="ASPxLabel14Resource1">
                        </dx:ASPxLabel>
                    </td>
                    <td class="value" style="height: 28px">
                        <dx:ASPxLabel ID="lblamount" runat="server" meta:resourcekey="lblamountResource1">
                        </dx:ASPxLabel>
                    </td>
                    <td class="auto-style20"></td>
                </tr>
                <tr>
                    <td align="right" class="auto-style19">
                        <dx:ASPxLabel ID="ASPxLabel16" runat="server" Text="የመደብ መታወቂያ ቁጥር" meta:resourcekey="ASPxLabel16Resource1">
                        </dx:ASPxLabel>
                    </td>
                    <td align="left" class="value" style="width: 144px">
                        <dx:ASPxLabel ID="lblpostioncode" runat="server" Text='<%# Eval("position_code") %>' meta:resourcekey="lblpostioncodeResource1">
                        </dx:ASPxLabel>
                    </td>
                    <td align="right" class="auto-style22">
                        <dx:ASPxLabel ID="ASPxLabel18" runat="server" Text="ክፍት የስራ መደብ የሚገኝበት" meta:resourcekey="ASPxLabel18Resource1">
                        </dx:ASPxLabel>
                    </td>
                    <td align="left" class="value">
                        <dx:ASPxLabel ID="lblrequesting_unit" runat="server" Text='<%# Eval("Requesting_Unit") %>' meta:resourcekey="lblrequesting_unitResource1">
                        </dx:ASPxLabel>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td align="right" class="auto-style19">
                        <dx:ASPxLabel ID="ASPxLabel22" runat="server" Text="ተፈላጊ ልዩ ችሎታ/ልዩ ሙያ" meta:resourcekey="ASPxLabel22Resource1">
                        </dx:ASPxLabel>
                    </td>
                    <td align="left" class="value" style="width: 144px">
                        <dx:ASPxLabel ID="ASPxLabel23" runat="server" Text='<%# Eval("skillability") %>' meta:resourcekey="ASPxLabel23Resource1">
                        </dx:ASPxLabel>
                    </td>
                    <td align="right" class="auto-style22">
                        <dx:ASPxLabel ID="ASPxLabel24" runat="server" Text="ጥቅማጥቅም" meta:resourcekey="ASPxLabel24Resource1">
                        </dx:ASPxLabel>
                    </td>
                    <td align="left" class="value">
                        <dx:ASPxLabel ID="ASPxLabel25" runat="server" Text='<%# Eval("Allowance") %>' meta:resourcekey="ASPxLabel25Resource1">
                        </dx:ASPxLabel>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="right" class="auto-style19">
                        <dx:ASPxLabel ID="ASPxLabel21" runat="server" Text="ምዝገባው የሚጠናቀቅበት ቀንና ሰዓት" meta:resourcekey="ASPxLabel21Resource1">
                        </dx:ASPxLabel>
                    </td>
                    <td align="left" class="value" colspan="2">
                        <dx:ASPxLabel ID="lblvacdate" runat="server" Text='<%# Eval("Announcement_Date") %>' meta:resourcekey="lblvacdateResource1">
                        </dx:ASPxLabel>
                        <dx:ASPxLabel ID="lblvactime" runat="server" Text='<%# Eval("Deadline_Time") %>' meta:resourcekey="lblvactimeResource1">
                        </dx:ASPxLabel>
                    </td>
                    <td align="left" class="value">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" valign="top" align="right">
                        <dx:ASPxLabel ID="ASPxLabel20" runat="server" Text="የስራ መደቡ የሚጠይቀው ከስራው ጋር አግባብነት ያለው የስራ ልምድና የትምህርት ደረጃ" meta:resourcekey="ASPxLabel20Resource1">
                        </dx:ASPxLabel>
                    </td>
                    <td class="auto-style22" colspan="2">
                        <dx:ASPxMemo ID="ASPxMemo1" runat="server" Height="71px" Text='<%# Eval("EducationlevelandExp") %>' Width="100%" meta:resourcekey="ASPxMemo1Resource1">
                        </dx:ASPxMemo>
                    </td>
                </tr>
            </table>
        </DataRow>
        <EmptyDataRow>
            መረጃ አልተገኘም
        </EmptyDataRow>
    </Templates>
</dx:ASPxGridView>
