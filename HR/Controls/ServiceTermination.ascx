﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ServiceTermination.ascx.cs" Inherits="Controls_ServiceTermination" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>

<div>
    <dx:ASPxGridView ID="DVgvtermination" ClientInstanceName="DVgvtermination" runat="server" AutoGenerateColumns="False" EnableTheming="True" 
        Width="100%" OnBeforePerformDataSelect="DVgvtermination_BeforePerformDataSelect"
        OnCustomColumnDisplayText="DVgvtermination_CustomColumnDisplayText" OnCustomCallback="DVgvtermination_OnCustomCallback"
         Theme="Moderno" meta:resourcekey="DvgvdeciplinResource1">
        <Columns>
            <dx:GridViewDataTextColumn Caption="እርምጃ የተወስደበት ቀን" FieldName="Terminationdate"
                ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource82">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ደብዳቤ ቁጥር" FieldName="LetterNo" ShowInCustomizationForm="True"
                VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnResource78">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="የተሰናበቱበት ምክንያት" FieldName="Termination_reason" ShowInCustomizationForm="True"
                VisibleIndex="7" meta:resourcekey="GridViewDataTextColumnResource79">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="የተወስደው እርምጃ" FieldName="Punishment" ShowInCustomizationForm="True"
                VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnResource85">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ማስታወሻ" FieldName="remark" ShowInCustomizationForm="True"
                VisibleIndex="4" Visible="False" meta:resourcekey="GridViewDataTextColumnResource86">
            </dx:GridViewDataTextColumn>
        </Columns>
        <Templates>
            <EmptyDataRow>
                &nbsp;መረጃ አልተገኘም
            </EmptyDataRow>
        </Templates>
    </dx:ASPxGridView>
    --%>  
</div>
