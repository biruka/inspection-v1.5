﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EducationalBackground.ascx.cs" Inherits="Controls_EducationalBackground" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>

<div>
    <dx:ASPxGridView ID="DvgvCandidate" runat="server" AutoGenerateColumns="False" ClientInstanceName="DvgvEducationalBackground" OnCustomCallback="DvgvCandidate_OnCustomCallback"
        Width="100%" OnBeforePerformDataSelect="DvgvCandidate_BeforePerformDataSelect" OnCustomColumnDisplayText="DvgvCandidate_CustomColumnDisplayText"
        Theme="Office2010Silver" meta:resourcekey="DvgvCandidateResource1">
        <Columns>
            <dx:GridViewDataTextColumn Caption="ተቋም" FieldName="institution" ShowInCustomizationForm="True"
                VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource14">
            </dx:GridViewDataTextColumn>
            <dx:GridViewBandColumn Caption="ትምህርት" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewBandColumnResource2">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="የትምህርት መስክ" FieldName="field_of_study" ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource15">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="የትምህርት መረጃ" FieldName="certificate_obtained" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource16">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="የትምህርት ውጤት(GPA)" FieldName="gpa" ShowInCustomizationForm="True" VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource17">
                    </dx:GridViewDataTextColumn>
                </Columns>
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewBandColumn>
            <dx:GridViewBandColumn Caption="ቀን" ShowInCustomizationForm="True" VisibleIndex="2" meta:resourcekey="GridViewBandColumnResource3">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="ከ" FieldName="period_from" ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource18">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="እስከ" FieldName="period_to" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource19">
                    </dx:GridViewDataTextColumn>
                </Columns>
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewBandColumn>
            <dx:GridViewDataTextColumn Caption=" ትምህርቱ የወሰደው ጊዜ" FieldName="net_duration" ShowInCustomizationForm="True" VisibleIndex="4" meta:resourcekey="GridViewDataTextColumnResource20">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="የትምህርቱ መርሃ ግብር" FieldName="education_type" ShowInCustomizationForm="True" VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnResource21">
            </dx:GridViewDataTextColumn>
        </Columns>
        <Templates>
            <EmptyDataRow>
                &nbsp;መረጃ አልተገኘም
            </EmptyDataRow>
        </Templates>
    </dx:ASPxGridView>
</div>
