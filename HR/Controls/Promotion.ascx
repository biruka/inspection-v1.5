﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Promotion.ascx.cs" Inherits="Controls_Promotion" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>

<div>
    <dx:ASPxGridView ID="DvgvPromotion" runat="server" AutoGenerateColumns="False"  ClientInstanceName="Promotion"
        EnableTheming="True" OnCustomCallback="DvgvPromotion_OnCustomCallback" OnBeforePerformDataSelect="DvgvPromotion_BeforePerformDataSelect"
        Width="100%" OnCustomColumnDisplayText="DvgvPromotion_CustomColumnDisplayText" Theme="Moderno" meta:resourcekey="DvgvPromotionResource1">
        <Columns>
            <dx:GridViewDataTextColumn ShowInCustomizationForm="True"
                VisibleIndex="3" Caption="የተራዘመበት ምክንያት " meta:resourcekey="GridViewDataTextColumnResource76">
            </dx:GridViewDataTextColumn>
            <dx:GridViewBandColumn Caption="የስራ መደብ" ShowInCustomizationForm="True" VisibleIndex="3" meta:resourcekey="GridViewBandColumnResource9">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="የቀድሞ የስራ መደብ" FieldName="from_job_title" ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource49">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="አዲሱ የስራ መደብ" FieldName="to_job_title" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource50">
                    </dx:GridViewDataTextColumn>
                </Columns>
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewBandColumn>
            <dx:GridViewDataTextColumn Caption="የአገልግሎት የተራዘመበት ቀን " ShowInCustomizationForm="True"
                VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource72">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ማስታወሻ" FieldName="remark" ShowInCustomizationForm="True"
                VisibleIndex="10" meta:resourcekey="GridViewDataTextColumnResource53">
            </dx:GridViewDataTextColumn>
            <%--<dx:GridViewDataTextColumn Caption="የአገልግሎት ዘመን" FieldName="SYEYear" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource74">
            </dx:GridViewDataTextColumn>--%>
            <dx:GridViewDataTextColumn Caption="ለጡረታ መውጫ የቀረው ቀን" FieldName="PensionDate" ShowInCustomizationForm="True"
                VisibleIndex="8" meta:resourcekey="GridViewDataTextColumnResource54">
            </dx:GridViewDataTextColumn>
            <dx:GridViewBandColumn Caption="ደረጃ" ShowInCustomizationForm="True" VisibleIndex="2" meta:resourcekey="GridViewBandColumnResource10">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="የቀድሞ ደረጃ" FieldName="oldGrade" ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource55">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="አዲሱ ደረጃ" FieldName="newGrade" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource56">
                    </dx:GridViewDataTextColumn>
                </Columns>
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewBandColumn>
            <dx:GridViewBandColumn Caption="ደመወዝ" ShowInCustomizationForm="True" VisibleIndex="5" meta:resourcekey="GridViewBandColumnResource11">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="የቀድሞ ደመወዝ" FieldName="from_salary" ShowInCustomizationForm="True" UnboundType="Decimal" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource57">
                        <PropertiesTextEdit DisplayFormatString="#,00.00"></PropertiesTextEdit>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="አዲሱ ደመወዝ" FieldName="to_salary" ShowInCustomizationForm="True" UnboundType="Decimal" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource58">
                        <PropertiesTextEdit DisplayFormatString="#,00.00"></PropertiesTextEdit>
                    </dx:GridViewDataTextColumn>
                </Columns>
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewBandColumn>
        </Columns>
        <Templates>
            <EmptyDataRow>
                &nbsp;መረጃ አልተገኘም
            </EmptyDataRow>
        </Templates>
    </dx:ASPxGridView>
</div>
