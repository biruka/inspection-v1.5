﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Bussiness;
using CUSTOR.Commen;
 using CUSTOR.Domain;
using DevExpress.Web;
using DevExpress.Web.ASPxHtmlEditor;
using CUSTOR;
public partial class Controls_Response : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void ShowError(string msg)
    {
        lblPersonMsg.Visible = true;
        pnlMsg.Visible = true;
        CMsgBar.ShowError(msg, pnlMsg, lblPersonMsg);
    }

    protected void LoadInspectionForResponse()
    {
        Guid person = Guid.Parse(Session["APersonGuid"].ToString());
        InspectionBussiness inspection = new InspectionBussiness();
        DataTable dtInspection = inspection.GetRecords(person, Session["DocumentType"].ToString());
        if (dtInspection.Rows.Count > 0)
        {
            pnlResponseList.Visible = true;
            Dvgvreanswer.DataSource = dtInspection;
            Dvgvreanswer.DataBind();
        }
        else
        {
            pnlResponseDetail.Visible = false;
        }
    }

    protected string GetReanswerText()
    {
        int j = Convert.ToInt16(Session["j"].ToString());
        ASPxGridView Gridinsp = (ASPxGridView)Dvgvreanswer;
        if (Gridinsp != null)
        {
            ASPxHtmlEditor memo = Gridinsp.FindEditFormTemplateControl("answerMemo") as ASPxHtmlEditor;
            return memo.Html;
        }

        return "";
    }

    protected string GetFollowupGUIDText()
    {
        int j = Convert.ToInt16(Session["j"].ToString());
        ASPxGridView Gridinsp = (ASPxGridView)Dvgvreanswer;
        if (Gridinsp != null)
        {
            HiddenField insguid = Gridinsp.FindEditFormTemplateControl("HiddenField1") as HiddenField;
            if (insguid != null)
            {
                return insguid.Value;
            }
        }
        return "";
    }

    protected string GetInspectionGUIDText()
    {
        int j = Convert.ToInt16(Session["j"].ToString());
        ASPxGridView Gridinsp = (ASPxGridView)Dvgvreanswer;
        if (Gridinsp != null)
        {
            ASPxTextBox memo = Gridinsp.FindEditFormTemplateControl("ASPxTextBox2") as ASPxTextBox;
            return memo.Text;
        }
        return "";
    }

    protected void NewResponse()
    {
        InspectionBussiness inspectionInfo = new InspectionBussiness();
        tblInspection objInspection = inspectionInfo.GetInspection(Guid.Parse(Session["InspectionGuid"].ToString()));
        if (objInspection.InspectionFinding != "")
        {
            ASPxLabel3.Text = objInspection.InspectionFinding;
        }
        txtsubject.Text = "";
        answerMemo.Html = "";
    }

    protected void cbRespnse_OnCallbackPanel(object sender, CallbackEventArgsBase e)
    {
        string[] parameters = e.Parameter.Split('|');
        string strParam = parameters[0].ToString();
        if (parameters.Length > 1)
        {
            Session["InspectionGuid"] = parameters[1];
        }
        switch (strParam)
        {
            case "Load":
                //LoadInspectionForResponse();
                break;
            case "Save":
                saveResponse();
                break;
            case "New":
                pnlResponseDetail.Visible = true;
                NewResponse();
                break;
            case "Response":
                pnlResponseDetail.Visible = true;
                NewResponse();
                break;
        }
    }

    //public void ShowError(string msg)
    //{
    //    lblPersonMsg.Visible = true;
    //    pnlMsg.Visible = true;
    //    CMsgBar.ShowError(msg, pnlMsg, lblPersonMsg);
    //}

    protected void saveResponse() // update the answer 
    {
        if (Guid.Parse(Session["InspectionGuid"].ToString()) != Guid.Empty)
        {
            tblInspection objInspection = new tblInspection();
            InspectionBussiness objInspectionBusiness = new InspectionBussiness();
            try
            {
                objInspection.Answer = answerMemo.Html;
                objInspection.Subject = txtsubject.Text;
                objInspection.InspectionGUID = Guid.Parse(Session["InspectionGuid"].ToString());//Attention;
                objInspection.AnswerDate = DateTime.Today;
                objInspectionBusiness.UpdateInspectionAnswer(objInspection);
                //MessageBox1.ShowSucess(Resources.Message.MSG_SAVED);
                Panel1.Visible = true;
                pnlResponseList.Visible = true;
                LoadInspectionForResponse();
                CMsgBar.ShowSucess(Resources.Message.MSG_SAVED, Panel1, Label3);
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }
    }

    protected string GetsubjectText()
    {
        int j = Convert.ToInt16(Session["j"].ToString());
        ASPxGridView Gridinsp = (ASPxGridView)Dvgvreanswer;
        if (Gridinsp != null)
        {
            ASPxTextBox memo = Gridinsp.FindEditFormTemplateControl("txtsubject") as ASPxTextBox;
            return memo.Text;
        }

        return "";
    }

    protected void Dvgvreanswer_OnCustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        if (e.Column.FieldName == "InspectionDecision")
        {
            int test = 0;
            if (int.TryParse((e.Value).ToString(), out test))
            {
                if (p.Organization.LanguageID == 1)
                {
                    e.DisplayText = EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionDecisionType), Language.eLanguage.eAmharic, Convert.ToInt32(e.Value));
                }
                if (p.Organization.LanguageID == 2)
                {
                    e.DisplayText = EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionDecisionType), Language.eLanguage.eAfanOromo, Convert.ToInt32(e.Value));
                }
            }
        }

        if (e.Column.FieldName == "AnswerDate")
            if ((e.Value).ToString().Length != 0)
            {
                if (e.Value.ToString() == "6/06/1900 12:00:00 AM")
                    e.DisplayText = "";
                else
                {
                    string ethiodate;
                    ethiodate = EthiopicDateTime.GetEthiopicDate(((DateTime)(e.Value)).Day, ((DateTime)(e.Value)).Month, ((DateTime)(e.Value)).Year);
                    e.DisplayText = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate,Convert.ToInt32(p.Organization.LanguageID));
                }
            }
            else
            {
                { return; }
            }
    }

    protected void Dvgvreanswer_OnHtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;
        DateTime AnswerDate = Convert.ToDateTime(e.GetValue("AnswerDate"));
        string strAnDate = Convert.ToString(AnswerDate.Date.ToShortDateString());
        string approvalname = Convert.ToString(e.GetValue("approvalname"));

        if (strAnDate == "6/6/1900")
        {
            e.Row.BackColor = Color.LightCyan; // Changes The BackColor of ENTIRE ROW
            e.Row.ForeColor = Color.DarkRed; // Change the Font Color of ENTIRE ROW

        }
    }

    protected void Dvgvreanswer_OnBeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["person"] = Session["ParentGuid"];
    }

    protected void Dvgvreanswer_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        LoadInspectionForResponse();
    }

    protected void Dvgvreanswer_PageIndexChanged(object sender, EventArgs e)
    {
        LoadInspectionForResponse();
    }
}