﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Toolbar.ascx.cs" Inherits="CUSTOR_CToolbar" %>
<%@ Register assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
 
<%@ Register assembly="CUSTOR.Controls.WebBox" namespace="CUSTOR.Controls.WebBox" tagprefix="cc1" %>

<style type="text/css">
    .auto-style4
    {
        width: 1039px;
    }
    .Panel
    {
         
    background: #F5F5F5;
    border: 2px solid #f1f1f1;
    border-radius: 5px;
 
    }
</style>
<cc1:ConfirmBox ID="ConfirmBox1" runat="server" />
<asp:Panel ID="pnlToolbar" runat="server" CssClass="Panel" Width="98%">
    <center>
        <div style="width: 100%">
               <table>
                   <tr>
                    <td>
                    <dx:ASPxButton ID="btnNew" runat="server" HorizontalAlign="Center" Image-Url="~/Controls/ToolbarImages/new.gif" ImagePosition="Left" Text="አዲስ" Theme="Office2010Silver" VerticalAlign="Middle" Width="70px" OnClick="btnNew_Click"></dx:ASPxButton>
                    </td>
                    <td>
                    <dx:ASPxButton ID="btnSave" runat="server" HorizontalAlign="Center" Image-Url="~/Controls/ToolbarImages/save.gif" ImagePosition="Left" Text="አስቀምጥ" Theme="Office2010Silver" VerticalAlign="Middle" Width="70px" OnClick="btnSave_Click"></dx:ASPxButton>
                    </td>
                    <td>
                     <dx:ASPxButton ID="btnDelete" runat="server" HorizontalAlign="Center" Image-Url="~/Controls/ToolbarImages/delete.gif" Text="ሰርዝ" Theme="Office2010Silver"  VerticalAlign="Middle" Width="70px" AutoPostBack="False"  >
                         <ClientSideEvents Click="function(s, e) {
	                                                ShowConfirmBox(this,'እርግጠኛ ነዎት መሰረዝ ይፈልጋሉ?','Confirm');return false;
                                        }" />
                         <Image Url="~/Controls/ToolbarImages/delete.gif">
                         </Image>
                        </dx:ASPxButton>
                    </td>
                    <td>
                    <dx:ASPxButton ID="btnFind" runat="server" HorizontalAlign="Center" Image-Url="~/Controls/ToolbarImages/search.gif" Text="ፈልግ" Theme="Office2010Silver" VerticalAlign="Middle" Width="70px"></dx:ASPxButton>
                    </td>
                    <td>
                     <dx:ASPxButton ID="btnCancel" runat="server" HorizontalAlign="Center" Image-Url="~/Controls/ToolbarImages/cancel.png" Text="ተው" Theme="Office2010Silver" VerticalAlign="Middle" Width="70px"></dx:ASPxButton>
                    </td>
                    
                </tr>
               </table>
           </div>
        </center>
</asp:Panel>

<asp:Label ID="Label1" runat="server" BackColor="#2082B9" Text="Hello There"></asp:Label>


