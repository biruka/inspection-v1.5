﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Bussiness;
using CUSTOR.Commen;
using DevExpress.Web;

public partial class Controls_ServiceExstension : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    protected void ASPxGridView6_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["person"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }

    protected void LoadServiceExtension()
    {
        Guid person = Guid.Parse(Session["APersonGuid"].ToString());
        tblServiceYearExtenderBussiness serviceExtension = new tblServiceYearExtenderBussiness();
        Dvgvserextend.DataSource = serviceExtension.GetRecords(person); ;
        Dvgvserextend.DataBind();
    }
    
    protected void Dvgvserextend_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        LoadServiceExtension();
    }

    protected void Dvgvserextend_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        if (e.Column.FieldName.Equals("ApprovedDate"))
        {
            if ((e.Value).ToString().Length != 0)
            {
                string ethiodate;
                DateTime date = Convert.ToDateTime(e.Value);
                ethiodate = EthiopicDateTime.GetEthiopicDate(date.Day, date.Month, date.Year);
                e.DisplayText = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate,
                    Convert.ToInt32(p.Organization.LanguageID));
            }
        }
    }
}