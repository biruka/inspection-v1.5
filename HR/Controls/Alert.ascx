﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Alert.ascx.cs" Inherits="Controls_Alert" %>


<div id="divMessageBody" >
    <a class="close-btn" onclick="HideMessage();">x</a>
    <div id="divMessage" class="message" ></div>
</div>
<script type="text/javascript">
        var Timeout;
        var DisplayInterval = 8000;  
        var mRight = -720; 

        function ShowError(msg) {

            ShowAlert(msg, "error");
        }
        function ShowMessage(msg) {

            ShowAlert(msg, "info");
        }
        function ShowSuccess(msg) {

            ShowAlert(msg, "success");
        }
        function ShowAlert(msg, type) {
            clearInterval(Timeout);
            $("#divMessageBody").css("right", mRight);
            var classAttr = "message-box " + type;
            $("#divMessage").html(msg);
            $("#divMessageBody").attr("class", classAttr);

            $("#divMessageBody").stop().animate({
                right: "0px"
            }, 700, "easeInOutCirc");

            Timeout = setTimeout(function () {
                HideMessage();
            }, DisplayInterval);
        }

        function HideMessage() {
            $("#divMessageBody").stop().animate({
                right: mRight
            }, 600, "easeInOutCirc");

            clearInterval(Timeout);
        }
</script>


