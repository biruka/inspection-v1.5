﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Candidates.ascx.cs" Inherits="Controls_Candidates" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>

<dx:ASPxGridView ID="DvGcandidate" runat="server" AutoGenerateColumns="False"
    OnBeforePerformDataSelect="DvGcandidate_BeforePerformDataSelect" ClientInstanceName="DvGcandidate" OnCustomCallback="DvGcandidate_OnCustomCallback"
    Width="100%" Theme="Office2010Silver" meta:resourcekey="DvGcandidateResource1">
    <Columns>
        <dx:GridViewDataTextColumn Caption="ሙሉ ስም" FieldName="FullName" ShowInCustomizationForm="True" VisibleIndex="0" Width="150px" meta:resourcekey="GridViewDataTextColumnResource39">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="የትምህርት መስክ" FieldName="Filedofstudy" ShowInCustomizationForm="True" VisibleIndex="1" Width="150px" meta:resourcekey="GridViewDataTextColumnResource40">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="የስራ ልምድ" FieldName="Expyear" ShowInCustomizationForm="True" VisibleIndex="2" Width="50px" meta:resourcekey="GridViewDataTextColumnResource41">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="ልዩ ትኩረት ምክንያት" FieldName="Selection_Criteria" ShowInCustomizationForm="True" VisibleIndex="8" Width="50px" meta:resourcekey="GridViewDataTextColumnResource42">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataCheckColumn Caption="ታዳጊ ክልልሎች ልዩ ትኩረት" FieldName="IsEthnicBased" ShowInCustomizationForm="True" VisibleIndex="7" meta:resourcekey="GridViewDataCheckColumnResource9">
            <CellStyle HorizontalAlign="Center" Wrap="True">
            </CellStyle>
        </dx:GridViewDataCheckColumn>
        <dx:GridViewDataCheckColumn Caption="የአካል ጉዳት ልዩ ትኩረት" FieldName="IsDisability" ShowInCustomizationForm="True" VisibleIndex="7" meta:resourcekey="GridViewDataCheckColumnResource9">
            <CellStyle HorizontalAlign="Center" Wrap="True">
            </CellStyle>
            <HeaderStyle HorizontalAlign="Left" Wrap="True" />

        </dx:GridViewDataCheckColumn>
        <dx:GridViewDataCheckColumn Caption="የሴቶች ልዩ ትኩረት" FieldName="IsGender" ShowInCustomizationForm="True" VisibleIndex="7" meta:resourcekey="GridViewDataCheckColumnResource9">
            <CellStyle HorizontalAlign="Center" Wrap="True">
            </CellStyle>
        </dx:GridViewDataCheckColumn>
        <dx:GridViewDataTextColumn FieldName="Is_Hired" ShowInCustomizationForm="True" VisibleIndex="7" Visible="False" meta:resourcekey="GridViewDataTextColumnResource43">
        </dx:GridViewDataTextColumn>
        <dx:GridViewBandColumn Caption="የፈተና ውጤት" ShowInCustomizationForm="True" VisibleIndex="3" meta:resourcekey="GridViewBandColumnResource8">
            <Columns>
                <dx:GridViewDataTextColumn Caption="የፅሑፍ ውጤት" FieldName="TheoryExamResult" ShowInCustomizationForm="True" VisibleIndex="0" Width="60px" meta:resourcekey="GridViewDataTextColumnResource44">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="የተግባር ውጤት" FieldName="PracticalExamResult" ShowInCustomizationForm="True" VisibleIndex="1" Width="60px" meta:resourcekey="GridViewDataTextColumnResource45">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="የቃለመጠይቅ ውጤት" FieldName="InterviewExamResult" ShowInCustomizationForm="True" VisibleIndex="2" Width="60px" meta:resourcekey="GridViewDataTextColumnResource46">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="አጠቃላይ ውጤት" FieldName="Result" ShowInCustomizationForm="True" VisibleIndex="3" Width="60px" meta:resourcekey="GridViewDataTextColumnResource47">
                </dx:GridViewDataTextColumn>
            </Columns>
            <HeaderStyle HorizontalAlign="Center" />
        </dx:GridViewBandColumn>
        <dx:GridViewDataTextColumn Caption="ጠቅላላ ድምር ውጤት" FieldName="EvalPoint" ShowInCustomizationForm="True" VisibleIndex="10" Width="20px" meta:resourcekey="GridViewDataTextColumnResource48">
        </dx:GridViewDataTextColumn>
    </Columns>
    <Templates>
        <EmptyDataRow>
            &nbsp;መረጃ አልተገኘም
        </EmptyDataRow>
    </Templates>
</dx:ASPxGridView>
