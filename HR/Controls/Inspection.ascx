﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Inspection.ascx.cs" ValidateRequestMode="Disabled" Inherits="Controls_Inspection" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Src="ConfirmBox.ascx" TagName="ConfirmBoxHR" TagPrefix="uc6" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>


<style type="text/css">
    .auto-style2 {
        width: 384px;
    }

    #calendar.div-style {
        z-index: 120000;
    }

    #selectYear, #selectMonth {
        z-index: 120000;
    }

    #overflow {
        overflow: visible;
    }
    div.hidden {
            display: none
        }
</style>

<script type="text/javascript">

    function DoNewClick(s, e) {
        cbInspection.PerformCallback('New');
    }

    function DoSaveClick(s, e) {

        var result = ChklstApprove.GetSelectedValues();
        var value = "";
        for (var index = 0; index < result.length; index++) {
            if (index == 0) {
                value = result[index];
            } else {
                value += '|' + result[index];
            }
        }
        cbInspection.PerformCallback('Save' + '|' + value);
        alert('3');
    }

    function DoSearchClick(s, e) {
        cbdata.PerformCallback('Search');
    }

    function DoShowClick(s, e) {
        InspectionList.PerformCallback();
    }

    function DoApproveClick(s, e) {
        cbInspection.PerformCallback('Approve');
    }

    function OnGetSelectedInspectionFieldValues(values) {
        cbInspection.PerformCallback('Edit' + '|' + values);
    }

    function ShowPopupDocument() {
        popupControlHR.Show();
        btnYes.Focus();
    }

    function btnYes_Click(s, e) {

        ClosePopupDocument(true);

    }

    function btnNo_Click(s, e) {
        ClosePopupDocument(false);
    }

    function ClosePopupDocument(result) {
        popupControlHR.Hide();
        if (result) {
            cbInspection.PerformCallback('Delete' + '|' + strGuid);

        }
    }
    var strGuid;
    function OnGetSelectedFieldValues(values) {
        cbdata.PerformCallback('Inspect' + '|' + values + '|' + visibileIndex + '|' + ComboDocType.GetValue() + '|' + emp_status);
    }

    function DoNewClick(s, e) {
        cbInspection.PerformCallback('New');
        
    }
    function OnInspectionEndCallback(s, e) {
        if (s.cpAction == "LoadInspectionDetail") {
            var indicesToSelect = new Array();
            var test = s.cpcheckedInspection;
            for (var index = 0; index < ChklstApprove.GetItemCount(); index++) {
                var item = ChklstApprove.GetItem(index);
                for (var checkedIndex = 0; checkedIndex < test.length; checkedIndex++) {
                    if (test[checkedIndex] == item.value) {
                        indicesToSelect.push(index);
                    }
                }
            }
            ChklstApprove.SelectIndices(indicesToSelect);
            $("#inspectiondetail").show();
            pnlInspectionList.SetVisible(false);
        }
        else if (s.cpAction == "Deletion") {
            if (s.cpSuccess == "successful") {
                ShowSuccess(s.cpMessage);
                cbInspection.PerformCallback('Search');
            }
        }
        else if (s.cpAction == "UPDATE" || s.cpAction == "REGISTRATION") {
            if (s.cpStatus == "SUCCESS") {
                ShowSuccess(s.cpMessage);
            }
        }
        else if (s.cpAction == "newInspection") {
            if (s.cpStatus == "success") {
                $("#inspectiondetail").show();
                btnNew.SetVisible(true);
                btnsave.SetVisible(true);
            }
        }
    }

    function DvgvInspection_CustomButtonClick(s, e) {
        if (e.buttonID == 'Edit') {
            InspectionList.GetRowValues(e.visibleIndex, "InspectionGUID", OnGetSelectedInspectionFieldValues);
        }
        else if (e.buttonID == 'Delete') {
            strGuid = s.GetRowKey(e.visibleIndex);
            InspectionList.GetRowValues(e.visibleIndex, "InspectionGUID", ShowPopupDocument());
        }
    }

    function InspectionListEndCallBack(s, e) {
        if (s.cpStatus === "success") {
            if (s.cpAction === "loadedInspectionList") {
                pnlInspectionList.SetVisible(true);
                $("#inspectiondetail").hide();
            }
            if (s.cpAction === "NotloadedInspectionList") {
                if (s.cpHrActivityApproved === "yes") {
                    btnNew.SetVisible(false);
                    btnsave.SetVisible(false);
                }
                else if (s.cpHrActivityApproved === "no"){
                    btnNew.SetVisible(true);
                    btnsave.SetVisible(true);
                }
                cbInspection.PerformCallback('New');
            }
        }
    }

    jQuery(document).ready(function ($) {
        InspectionList.PerformCallback();
    });
</script>
<dx:ASPxPanel ID="pnlMsg" runat="server" CssClass="messageWarning" EnableViewState="False"
    Height="100%" Visible="False" Width="100%" BackColor="#CCFF33" meta:resourcekey="pnlMsgResource1">
    <PanelCollection>
        <dx:PanelContent ID="PanelContent2" runat="server" meta:resourcekey="PanelContent2Resource1">
            <asp:Label ID="lblPersonMsg" runat="server" CssClass="Message" Font-Bold="True" ForeColor="Red" meta:resourcekey="lblPersonMsgResource1"></asp:Label>
        </dx:PanelContent>
    </PanelCollection>
</dx:ASPxPanel>
<div>
    <dx:ASPxCallbackPanel ID="cbInspection" OnCallback="cbInspection_OnCallbackPanel" ClientInstanceName="cbInspection" runat="server" meta:resourcekey="cbInspectionResource1">
        <ClientSideEvents EndCallback="OnInspectionEndCallback" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent5" runat="server" meta:resourceKey="PanelContent2Resource1"
                SupportsDisabledAttribute="True">
                <table align="center">
                    <tr>
                        <td align="right" class="auto-style10">
                            <dx:ASPxButton ID="btnNew" runat="server" Text="አዲስ" Theme="DevEx" ClientVisible="false" AutoPostBack="False" CausesValidation="False" ClientInstanceName="btnNew" Width="70px" meta:resourcekey="btncancelResource2">
                                <Image Url="~/Images/Toolbar/New.png">
                                </Image>
                                <ClientSideEvents Click="function(s, e) {
                                                                                                                                            DoNewClick();
                                                                                                                                            }" />
                            </dx:ASPxButton>
                        </td>
                        <td align="left" class="auto-style10">
                            <dx:ASPxButton ID="btnShow" runat="server" Text="አሳይ" Theme="DevEx" Width="70px"  AutoPostBack="False" ClientInstanceName="btnShow" CausesValidation="False" meta:resourcekey="btnsaveResource2">
                                <ClientSideEvents Click=" DoShowClick" />
                                <Image Url="~/images/Toolbar/search.gif">
                                </Image>
                            </dx:ASPxButton>
                        </td>
                        <td align="left" class="auto-style10">
                            <dx:ASPxButton ID="btnsave" runat="server" Text="አስቀምጥ" Theme="DevEx" Width="70px" ClientVisible="false" ClientInstanceName="btnsave" AutoPostBack="False" CausesValidation="True" meta:resourcekey="btnsaveResource1">
                                <ClientSideEvents Click="function(s, e) {  DoSaveClick(); }" />
                                <Image Url="~/Images/Toolbar/save.gif">
                                </Image>
                            </dx:ASPxButton>
                        </td>
                        <td align="left" class="auto-style10">
                            <dx:ASPxButton ID="btnApproved" runat="server" Text="አፅድቅ" CausesValidation="False" AutoPostBack="False" Theme="DevEx" Visible="false" meta:resourcekey="btnApprovedResource1">
                                <ClientSideEvents Click="function(s, e) {  DoApproveClick(); }" />
                                <Image Url="~/Images/Toolbar/Forward.png">
                                </Image>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
                <br />
                <asp:Panel ID="inspectionPanle" runat="server" CssClass="messageWarning" EnableViewState="False"
                    Height="100%" Visible="False" Width="100%" BorderColor="#99CCFF" BackColor="#CCFF33" meta:resourcekey="pnlMsgResource1">
                    <asp:Label ID="lblInspectionLabel" runat="server" CssClass="Message" Font-Bold="True" ForeColor="Red" meta:resourcekey="lblPersonMsgResource1"></asp:Label>
                </asp:Panel>
                <div class="hidden" id="inspectiondetail">
                    <div class="container">
                        <div class="row">
                            <div class="col col-md-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <dx:ASPxLabel ID="lblInsDecision" runat="server" Text="የውሳኔ አይነት:" meta:resourcekey="lblInsDecisionResource1">
                                        </dx:ASPxLabel>
                                        <span>
                                            <asp:Label ID="Label6" runat="server" ForeColor="Red" Text="*" SkinID="Notifir" meta:resourcekey="Label6Resource2"></asp:Label></span>
                                    </div>
                                    <div class="col-md-4">
                                        <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="የግምገማ አይነት:" meta:resourcekey="lblInsMethodResource1">
                                        </dx:ASPxLabel>
                                        <span>
                                            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*" SkinID="Notifir" meta:resourcekey="Label6Resource2"></asp:Label></span>
                                    </div>
                                    <div class="col-md-4">
                                        <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="ቀን:" meta:resourcekey="ASPxLabel5Resource1">
                                        </dx:ASPxLabel>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <dx:ASPxComboBox ID="ComboDecision" runat="server" ClientInstanceName="ComboDecision" Width="90%" EnableTheming="True" meta:resourcekey="ComboDecisionResource1">
                                        </dx:ASPxComboBox>
                                    </div>
                                    <div class="col-md-4">
                                        <dx:ASPxComboBox ID="cboInspectionType" runat="server" ClientEnabled="false" ClientInstanceName="cboInspectionType" EnableTheming="True" Width="100%"
                                            meta:resourcekey="ComboDecisionResource1">
                                        </dx:ASPxComboBox>
                                    </div>
                                    <div class="col-md-4" style="margin-top: -3px">
                                        <cdp:CUSTORDatePicker ID="cdpInspectionDate" runat="server" Width="110px" Visible="true" />
                                    </div>
                                </div>
                                <br />
                                <div class="form-row">
                                    <div class="col-md-2">
                                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="የሰነድ አይነት ማረጋገጥ" Font-Names="Visual Geez Unicode" Font-Size="10pt" meta:resourcekey="ASPxLabel1Resource1">
                                        </dx:ASPxLabel>
                                    </div>
                                </div>
                                <div class="for-row">
                                    <div class="col-md-12">
                                        <dx:ASPxCheckBoxList ID="ChklstApprove" ClientInstanceName="ChklstApprove" AutoPostBack="False" runat="server" TextField="amDescription" Theme="ios"
                                            ValueField="code" Width="95%" Height="150px" EnableTheming="True" Font-Size="Small" Font-Names="Nyala" RepeatColumns="5" meta:resourcekey="ChklstApproveResource1">
                                        </dx:ASPxCheckBoxList>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-2">
                                        <dx:ASPxLabel ID="lblmemo" runat="server" Text="የውሳኔ አስተያየት :" meta:resourcekey="lblmemoResource1">
                                        </dx:ASPxLabel>
                                        <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="*" SkinID="Notifir" meta:resourcekey="Label2Resource1"></asp:Label>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12">
                                        <dx:ASPxHtmlEditor ID="Memo" Width="95%" runat="server" meta:resourcekey="MemoResource1">
                                            <SettingsImageUpload UploadImageFolder="~/UploadImages/">
                                                <ValidationSettings AllowedFileExtensions=".jpe,.jpeg,.jpg,.gif,.png" MaxFileSize="500000">
                                                </ValidationSettings>
                                            </SettingsImageUpload>
                                        </dx:ASPxHtmlEditor>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table align="center">
                        <tr>
                            <td></td>
                            <td align="left" valign="top" colspan="2">
                                <asp:Panel ID="pnlResponse" runat="server" Visible="False" meta:resourcekey="pnlResponseResource1">
                                    <dx:ASPxRoundPanel ID="pnanswer" runat="server" HeaderText="መልስ" Width="90%" meta:resourcekey="pnanswerResource1">
                                        <PanelCollection>
                                            <dx:PanelContent ID="PanelContent7" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="PanelContent1Resource1">
                                                <dx:ASPxLabel ID="lblanswer" runat="server" Height="20px" Width="100%" meta:resourcekey="lblanswerResource1">
                                                </dx:ASPxLabel>
                                            </dx:PanelContent>
                                        </PanelCollection>
                                    </dx:ASPxRoundPanel>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="2" valign="top" class="auto-style15">
                                <asp:Panel ID="pnlapproval" runat="server" Visible="False" Width="100%" meta:resourcekey="pnlapprovalResource1">
                                    <table width="90%">
                                        <tr>
                                            <td align="right">
                                                <dx:ASPxLabel ID="lblperparedname" runat="server" Text="የተዘጋጀበት ቀን" meta:resourcekey="lblperparednameResource1">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left">
                                                <dx:ASPxLabel ID="lblprepareddate" runat="server" Width="200px" meta:resourcekey="lblprepareddateResource1">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="right">
                                                <dx:ASPxLabel ID="lblaprovedname" runat="server" Text="የፀደቀበት ቀን" meta:resourcekey="lblaprovednameResource1">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left">
                                                <dx:ASPxLabel ID="lblapproveddate" runat="server" Width="200px" meta:resourcekey="lblapproveddateResource1">
                                                </dx:ASPxLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <dx:ASPxLabel ID="lblpreparedtext" runat="server" Text="አዘጋጅ" meta:resourcekey="lblpreparedtextResource1">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left">
                                                <dx:ASPxLabel ID="lblupdatename" runat="server" Width="200px" meta:resourcekey="lblupdatenameResource1">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="right">
                                                <dx:ASPxLabel ID="lblapprovedtext" runat="server" Text="አፅዳቂ" meta:resourcekey="lblapprovedtextResource1">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left">
                                                <dx:ASPxLabel ID="lblapprovalname" runat="server" Width="200px" meta:resourcekey="lblapprovalnameResource1">
                                                </dx:ASPxLabel>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </div>

                <dx:ASPxPanel runat="server" ID="pnlInspectionList" ClientInstanceName="pnlInspectionList" ClientVisible="False" meta:resourcekey="pnlInspectionListResource1">
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent8" runat="server" meta:resourcekey="PanelContent8Resource1">
                            <dx:ASPxGridView ID="DvgvInspection" runat="server" AutoGenerateColumns="False" ClientInstanceName="InspectionList"
                                OnCustomButtonInitialize="DvgvInspection_CustomButtonInitialize1" OnBeforePerformDataSelect="DvgvInspection_BeforePerformDataSelect"
                                OnCustomColumnDisplayText="DvgvInspection_CustomColumnDisplayText" 
                                KeyFieldName="InspectionGUID" OnCustomCallback="DvgvInspection_OnCustomCallback"
                                OnSelectionChanged="DvgvInspection_SelectionChanged" Width="100%" OnRowInserting="DvgvInspection_RowInserting"
                                OnRowUpdating="DvgvInspection_RowUpdating" OnLoad="DvgvInspection_Load" OnPageIndexChanged="DvgvInspection_PageIndexChanged" OnHtmlRowPrepared="DvgvInspection_HtmlRowPrepared" Theme="Moderno" meta:resourcekey="DvgvInspectionResource1">
                                <ClientSideEvents CustomButtonClick="DvgvInspection_CustomButtonClick" EndCallback="InspectionListEndCallBack" />
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="InspectionDecision" ShowInCustomizationForm="True"
                                        Caption="የውሳኔ አይነት" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource119">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn ShowInCustomizationForm="True"
                                        Caption="የውሳኔ ቃለ ጉባኤ" VisibleIndex="2" Visible="False" meta:resourcekey="GridViewDataTextColumnResource120">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="InspectionDate" ShowInCustomizationForm="True" Caption="የግምገማ ቀን"
                                        VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnResource121">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="UserName" Caption="የባለሙያው ስም" Visible="true" VisibleIndex="4" meta:resourcekey="GridViewDataTextColumnResource123">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="approvalname" Caption="ያፀደቀው" Visible="true" VisibleIndex="6" meta:resourcekey="GridViewDataTextColumnResource125">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataCheckColumn Caption="ጸድቋል" FieldName="Approved" ShowInCustomizationForm="True" VisibleIndex="5" meta:resourcekey="GridViewDataTextColumnResource124">
                                        <CellStyle HorizontalAlign="Center" Wrap="True">
                                        </CellStyle>
                                    </dx:GridViewDataCheckColumn>
                                    <dx:GridViewDataTextColumn FieldName="approvaldateEthiopianCalander" Caption="የፀደቀበት ቀን" Visible="true" VisibleIndex="7" meta:resourcekey="GridViewDataTextColumnResource126">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="8" Width="100px">
                                        <CustomButtons>
                                            <dx:GridViewCommandColumnCustomButton ID="Edit"
                                                Text="አርም" meta:resourcekey="GridViewCommandColumnCustomButtonResource1">
                                            </dx:GridViewCommandColumnCustomButton>
                                        </CustomButtons>
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewCommandColumn
                                        ShowInCustomizationForm="True" VisibleIndex="9" Width="100px">
                                        <CustomButtons>
                                            <dx:GridViewCommandColumnCustomButton ID="Delete"
                                                Text="ሰርዝ" meta:resourcekey="GridViewCommandColumnCustomButtonResource2">
                                            </dx:GridViewCommandColumnCustomButton>
                                        </CustomButtons>
                                    </dx:GridViewCommandColumn>
                                </Columns>
                                <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" />
                                <Styles>
                                    <FocusedGroupRow ForeColor="Black">
                                    </FocusedGroupRow>
                                    <Row ForeColor="Black">
                                    </Row>
                                    <FocusedRow BackColor="#AAD2FF" ForeColor="Black">
                                    </FocusedRow>
                                    <Cell ForeColor="Black">
                                    </Cell>
                                </Styles>
                            </dx:ASPxGridView>
                            <dxpop:ASPxPopupControl ID="ASPxPopupControl2" runat="server" ClientInstanceName="popupControlHR" Font-Names="Visual Geez Unicode" Font-Size="Small" HeaderText="Delete Confirmation" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" meta:resourcekey="ASPxPopupControl1Resource1">
                                <ContentCollection>
                                    <dxpop:PopupControlContentControl ID="PopupControlContentControl2" runat="server" meta:resourcekey="PopupControlContentControl1Resource1">
                                        <uc6:ConfirmBoxHR ID="ConfirmBoxHR" runat="server" />
                                    </dxpop:PopupControlContentControl>
                                </ContentCollection>
                            </dxpop:ASPxPopupControl>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>

</div>
<asp:HiddenField ID="visibleindx1" runat="server" />
<asp:HiddenField ID="HidUserid" runat="server" />
<asp:HiddenField ID="HidlLanguage" runat="server" />
