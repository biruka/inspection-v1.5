﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ServiceExtension.ascx.cs" Inherits="Controls_ServiceExstension" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>

<div>
    <dx:ASPxGridView ID="Dvgvserextend" runat="server" AutoGenerateColumns="False" ClientInstanceName="Dvgvserextend"
        EnableTheming="True" OnBeforePerformDataSelect="ASPxGridView6_BeforePerformDataSelect" OnCustomCallback="Dvgvserextend_OnCustomCallback"
        Class="" Width="100%" Theme="Office2010Silver" meta:resourcekey="DvgvserextendResource1" OnCustomColumnDisplayText="Dvgvserextend_CustomColumnDisplayText">
        <Columns>
            <dx:GridViewDataTextColumn ShowInCustomizationForm="True" FieldName="AnotherReason"
                VisibleIndex="3" Caption="የተራዘመበት ምክንያት " meta:resourcekey="GridViewDataTextColumnResource76">
            </dx:GridViewDataTextColumn>
            <%--<dx:GridViewBandColumn Caption="የስራ መደብ" ShowInCustomizationForm="True" VisibleIndex="3" meta:resourcekey="GridViewBandColumnResource9">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="የቀድሞ የስራ መደብ" FieldName="from_job_title" ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource49">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="አዲሱ የስራ መደብ" FieldName="to_job_title" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource50">
                    </dx:GridViewDataTextColumn>
                </Columns>
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewBandColumn>--%>
            <dx:GridViewDataTextColumn ShowInCustomizationForm="True" FieldName="ApprovedDate"
                VisibleIndex="0" Caption="የአገልግሎት የተራዘመበት ቀን " meta:resourcekey="GridViewDataTextColumnResource72">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn ShowInCustomizationForm="True" FieldName="SYEHowMuch"
                VisibleIndex="2" Caption="የተራዘመበት ጊዜ" meta:resourcekey="GridViewDataTextColumnResource73">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="SYEYear" ShowInCustomizationForm="True" VisibleIndex="1"
                Caption="የአገልግሎት ዘመን" meta:resourcekey="GridViewDataTextColumnResource74">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn ShowInCustomizationForm="True" VisibleIndex="4"
                Caption="ማስታወሻ" FieldName="Remark" meta:resourcekey="GridViewDataTextColumnResource75">
            </dx:GridViewDataTextColumn>
<%--            <dx:GridViewBandColumn Caption="ደረጃ" ShowInCustomizationForm="True" VisibleIndex="2" meta:resourcekey="GridViewBandColumnResource10">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="የቀድሞ ደረጃ" FieldName="oldGrade" ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource55">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="አዲሱ ደረጃ" FieldName="newGrade" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource56">
                    </dx:GridViewDataTextColumn>
                </Columns>
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewBandColumn>--%>
            <%--<dx:GridViewBandColumn Caption="ደመወዝ" ShowInCustomizationForm="True" VisibleIndex="5" meta:resourcekey="GridViewBandColumnResource11">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="የቀድሞ ደመወዝ" FieldName="from_salary" ShowInCustomizationForm="True" UnboundType="Decimal" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource57">
                        <PropertiesTextEdit DisplayFormatString="#,00.00"></PropertiesTextEdit>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="አዲሱ ደመወዝ" FieldName="to_salary" ShowInCustomizationForm="True" UnboundType="Decimal" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource58">
                        <PropertiesTextEdit DisplayFormatString="#,00.00"></PropertiesTextEdit>
                    </dx:GridViewDataTextColumn>
                </Columns>
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewBandColumn>--%>
        </Columns>
        <Templates>
            <EmptyDataRow>
                &nbsp;መረጃ አልተገኘም
            </EmptyDataRow>
        </Templates>
    </dx:ASPxGridView>
</div>
