﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConfirmBox.ascx.cs" Inherits="Controls_ConfirmBox" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<style type="text/css">
    .auto-style1
    {
        width: 52px;
        height: 35px;
    }
    .auto-style2
    {
        width: 221px;
    }
    .auto-style3
    {
        height: 35px;
    }
    .auto-style4
    {
        width: 100%;
        height: 35px;
    }
</style>

<dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="300px" HeaderText="Delete Confirmation" DefaultButton="btnYes" ShowHeader="False" Theme="Office2010Silver" meta:resourcekey="ASPxRoundPanel1Resource1">
    <PanelCollection>
        <dx:PanelContent runat="server" meta:resourcekey="PanelContentResource1">
<%-- BeginRegion Dialog information elements: an image and labels --%>
            <table>
                <tr>
                    <td rowspan="2">
                        <dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="~/Images/warning.png" meta:resourcekey="ASPxImage1Resource1">
                        </dx:ASPxImage>
                    </td>

                    <td class="auto-style2">
                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="እርግጠኛ ነዎት ይህን መረጃ ማጥፋት ይፈልጋሉ?" meta:resourcekey="ASPxLabel1Resource1">
                        </dx:ASPxLabel>
                    </td>
                </tr>

                <tr>
                    <td class="auto-style2">
                        &nbsp;</td>
                </tr>
            </table>
            <br />
<%-- EndRegion --%>
<%-- BeginRegion Dialog control elements: a check box and buttons --%>
            <table>
                <tr>
                    <td class="auto-style3">
                    </td>
                    
                    <td class="auto-style4">
                    </td>
                    
                    <td class="auto-style3">
                        <dx:ASPxButton ID="btnYes" runat="server" Text="አዎ" Width="50px" AutoPostBack="False" ClientInstanceName="btnYes" Theme="iOS" meta:resourcekey="btnYesResource1">
                            <ClientSideEvents  Click="function(s, e) { 
                                btnYes_Click()}" />
                        </dx:ASPxButton>
                    </td>
                    
                    <td >
                        <dx:ASPxButton ID="btnNo" runat="server" Text="አይ" Width="50px" AutoPostBack="False" Theme="iOS" meta:resourcekey="btnNoResource1">
                            <ClientSideEvents  Click="function(s, e) {
                                btnNo_Click()}" />
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
<%-- EndRegion --%>
        </dx:PanelContent>
    </PanelCollection>
</dx:ASPxRoundPanel>


