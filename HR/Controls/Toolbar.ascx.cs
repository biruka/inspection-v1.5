﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;



 
    public delegate void ToolbarClick();
    public partial class CUSTOR_CToolbar : System.Web.UI.UserControl
    {
        public event ToolbarClick OnNewClick;
        public event ToolbarClick OnDeleteClick;
        public event ToolbarClick OnSaveClick;
        public event ToolbarClick OnFindClick;
        public event ToolbarClick OnCancelClick;

        protected void btnNew_Click(object sender, EventArgs e)
        {
            if (OnNewClick != null)
            {
                OnNewClick();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((!Page.IsPostBack))
            {
                btnSave.Enabled = false;
                btnDelete.Enabled = false;
              
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (OnSaveClick != null)
            {
                OnSaveClick();
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (OnDeleteClick != null)
            {
                OnDeleteClick();
            }
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
            if (OnFindClick != null)
            {
                OnFindClick();
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (OnCancelClick != null)
            {
                OnCancelClick();
            }
        }

        public DevExpress.Web.ASPxButton ShowNewButton
        {
            get
            {
                return btnNew;
            }
            set
            {
                btnNew = value;
            }
        }
        public DevExpress.Web.ASPxButton SaveButton
        {
            get
            {
                return btnSave;
            }
            set
            {
                btnSave = value;
            }
        }
        public DevExpress.Web.ASPxButton ShowDeleteButton
        {
            get
            {
                return btnDelete;
            }
            set
            {
                btnDelete = value;
            }
        }
        public DevExpress.Web.ASPxButton FindButton
        {
            get
            {
                return btnFind;
            }
            set
            {
                btnFind = value;
            }
        }

        public DevExpress.Web.ASPxButton ShowCancelButton
        {
            get
            {
                return btnCancel;
            }
            set
            {
                btnCancel = value;
            }
        }

   
    }
 