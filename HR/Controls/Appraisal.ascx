﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Appraisal.ascx.cs" Inherits="Controls_Appraisal" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>




<script type="text/javascript">

    function DoNewApprisal(s, e) {
        clbAppraisal.PerformCallback('New');
    }

    function DoFindApprisal(s, e) {
        clbAppraisal.PerformCallback('Find');
    }



    function DoSaveApprisal(s, e) {
        clbAppraisal.PerformCallback('Save');
    }

    function DoSaveEvaluationDetail(s, e) {
        clbAppraisal.PerformCallback('SaveEvaluationDetail');
    }
    function DoEvaluationApprisal(s, e) {
        clbAppraisal.PerformCallback('EvaluationApprisal');
    }


    //===========================Grid===============
    var rowVisibleIndex;
    var strID;

    function gvwAppraisal_CustomButtonClick(s, e) {
        if (e.buttonID != 'Del' && e.buttonID != 'Edit') return;
        if (e.buttonID == 'Del') {
            rowVisibleIndex = e.visibleIndex;
            s.GetRowValues(e.visibleIndex, 'MainGuid', ShowPopupAppraisal);
        }
        if (e.buttonID == 'Edit') {
            s.GetRowValues(e.visibleIndex, 'MainGuid', OnGetSelectedAppraisalFieldValues);
        }
    }

    function OnEndCallback(s, e) {

        if (s.cpMessage == '') {
            return;
        }

        if (s.cpStatus == "SUCCESS") {
            if (s.cpMessage == '') {
                return;
            }


            //if (s.cpAction == "ButtonAvailability") {

            //    alert('hi');
            //    clbAppraisal.PerformCallback('SetButtonAvailability');

            //    return;
            //}
            ShowSuccess(s.cpMessage);
        }
        else if (s.cpStatus == "ERROR") {
            ShowError(s.cpMessage);
        }
    }

    function OnGetSelectedAppraisalFieldValues(values) {
        clbAppraisal.PerformCallback('Edit' + '|' + values);
    }


    function OnGetSelectedDeleteAppraisal(values) {

        clbAppraisal.PerformCallback('Del' + '|' + values);

    }

    function ShowPopupAppraisal(rowId) {

        popupControlAppraisal.Show();
        btnYesAppraisal.Focus();
    }

    function btnYesAppraisal_Click(s, e) {

        ClosePopupAppraisal(true);
    }

    function btnNoAppraisal_Click(s, e) {
        ClosePopupAppraisal(false);
    }

    function ClosePopupAppraisal(result) {
        popupControlAppraisal.Hide();

        if (result) {

            gvwAppraisal.GetRowValues(rowVisibleIndex, "MainGuid", OnGetSelectedDeleteAppraisal);
        }

    }

    //





</script>






<dx:ASPxGridView ID="gvwAppraisal" runat="server" ClientVisible="true" SettingsPager-PageSize="5" AutoGenerateColumns="False" ClientInstanceName="gvwAppraisal" EnableTheming="True" Font-Names="Visual Geez Unicode" Width="600px"
    OnCustomCallback="gvwAppraisal_CustomCallback" Font-Size="Small" KeyFieldName="MainGuid" Theme="Office2010Silver" OnBeforePerformDataSelect="gvwAppraisal_BeforePerformDataSelect" OnCustomColumnDisplayText="gvwAppraisal_CustomColumnDisplayText" meta:resourcekey="gvwAppraisalResource1">

    <SettingsPager PageSize="5"></SettingsPager>

    <SettingsDetail ShowDetailRow="True" AllowOnlyOneMasterRowExpanded="True"></SettingsDetail>
    <ClientSideEvents CustomButtonClick="gvwAppraisal_CustomButtonClick" />
    <Columns>
        <dx:GridViewDataTextColumn Caption="MainGuid" FieldName="MainGuid" ShowInCustomizationForm="True" Visible="false" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource1">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="የግምገማ ቀን" FieldName="fiscal_year" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource2">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="የግምገማ ወቅት" FieldName="period_text" ShowInCustomizationForm="True" VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource3">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="የግምገማ ነጥብ" FieldName="eval_point" ShowInCustomizationForm="True" VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnResource4">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="የአፈፃፀም ደረጃ" FieldName="EvaluationLevel_text" ShowInCustomizationForm="True" VisibleIndex="4" meta:resourcekey="GridViewDataTextColumnResource5">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="የበጀት አመ" FieldName="budgetyear" ShowInCustomizationForm="True" VisibleIndex="5" meta:resourcekey="GridViewDataTextColumnResource7">
        </dx:GridViewDataTextColumn>
        <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="6" Width="100px" Visible="false">
            <CustomButtons>
                <dx:GridViewCommandColumnCustomButton ID="Edit" Text="ምረጥ" meta:resourcekey="GridViewCommandColumnCustomButtonResource1">
                </dx:GridViewCommandColumnCustomButton>
                <dx:GridViewCommandColumnCustomButton ID="Del" Text="ሰርዝ" meta:resourcekey="GridViewCommandColumnCustomButtonResource2">
                </dx:GridViewCommandColumnCustomButton>
            </CustomButtons>
        </dx:GridViewCommandColumn>
    </Columns>
    <SettingsBehavior AllowFocusedRow="True" />

    <SettingsDetail ShowDetailRow="true" />
    <Templates>
        <DetailRow>
            <dx:ASPxGridView ID="grvEvaluationDetail" runat="server" Theme="Office2010Silver" EnableTheming="True" Font-Names="Visual Geez Unicode" AutoGenerateColumns="False" Caption="ዝርዝር የግምገማ ውጤት" ClientInstanceName="grvEvaluationDetail" DataKeyNames="EvaluationChecklistGuid" meta:resourceKey="grvEvaluationDetailResource1" Width="500px" OnInit="grvEvaluationDetail_Init">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="የግምገማ መለኪያ" FieldName="EvaluationCriteria" ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource6">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="የግምገማ መለኪያ ነጥብ" FieldName="Evaluationpoint" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource7">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="የግምገማ ነጥብ ውጤት" FieldName="EvluationResult" ShowInCustomizationForm="True" VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource8">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataColumn Caption="የአፈፃፀም ፈርጅ" FieldName="GroupPointDesc" ShowInCustomizationForm="True" VisibleIndex="3" meta:resourcekey="GridViewDataColumnResource1">
                    </dx:GridViewDataColumn>
                </Columns>
                <Settings ShowFooter="True" />
                <TotalSummary>
                    <dx:ASPxSummaryItem FieldName="Evaluationpoint" SummaryType="Sum" DisplayFormat="ድምር={0:n2}" meta:resourcekey="ASPxSummaryItemResource1" />
                    <dx:ASPxSummaryItem FieldName="EvluationResult" SummaryType="Sum" DisplayFormat="ድምር={0:n2}" meta:resourcekey="ASPxSummaryItemResource2" />
                </TotalSummary>
            </dx:ASPxGridView>
        </DetailRow>
    </Templates>
</dx:ASPxGridView>


<dx:ASPxTextBox runat="server" ID="hdntxtCurrentStatus" ClientInstanceName="hdntxtCurrentStatus" ClientVisible="false"></dx:ASPxTextBox>

