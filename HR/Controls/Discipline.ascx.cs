﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Bussiness;
using CUSTOR.Commen;
using DevExpress.Web;
using DevExpress.XtraRichEdit.Layout;

public partial class Controls_Discipline : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    protected void Dvgvdeciplin_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["person"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }

    protected void Dvgvdeciplin_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        if (e.Column.FieldName == "date_action_taken")
        {
            if ((e.Value).ToString().Length != 0)
            {
                string ethiodate;
                ethiodate = EthiopicDateTime.GetEthiopicDate(((DateTime)(e.Value)).Day, ((DateTime)(e.Value)).Month, ((DateTime)(e.Value)).Year);
                e.DisplayText = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate,p.Organization.LanguageID);
            }
            else
            {
                { return; }
            }
        }
        if (e.Column.FieldName == "break_type")
        {
            if ((e.Value).ToString().Length != 0)
            {
                if (p.Organization.LanguageID == 1)
                {
                    switch (e.Value.ToString())
                    {
                        case "1":
                            e.Value = "ተደጋጋሚ መቅረት";
                            break;
                        case "2":
                            e.Value = "ተደጋጋሚ ማርፈድ";
                            break;
                        case "3":
                            e.Value = "ሌላ";
                            break;
                    }
                }
                else if (p.Organization.LanguageID == 2)
                {
                    switch (e.Value.ToString())
                    {
                        case "1":
                            e.Value = " Irraa Deddebiidhan Haafu";
                            break;
                        case "2":
                            e.Value = "Irraa Deddebiidhan  Yeroo dhan Argamu Dhisu";
                            break;
                        case "3":
                            e.Value = "Gara bira";
                            break;
                    }
                }
            }
            else
            {
                { return; }
            }
        }

    }

    protected void LoadDescipln()
    {
        Guid person = Guid.Parse(Session["APersonGuid"].ToString());
        var documentType = Session["DocumentType"].ToString();
        tblDiciplineBussiness discipline = new tblDiciplineBussiness();
        Dvgvdeciplin.DataSource = discipline.GetRecords(person);
        Dvgvdeciplin.DataBind();
    }

    protected void Dvgvdeciplin_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (Convert.ToBoolean(Session["LoadCriterias"]) == true)
        {
            LoadDescipln();
        }
    }
}