﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_Alert : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public void ShowAlert(string msg, string type)
    {
        Page.ClientScript.RegisterStartupScript(GetType(), "Script", "<script type=\"text/javascript\">ShowAlert(\"" + msg + "\",\"" + type + "\");</script>");
    }
    public void ShowMessage(string msg)
    {
        Page.ClientScript.RegisterStartupScript(GetType(), "Script", "<script type=\"text/javascript\">ShowMessage(\"" + msg + "\");</script>");
    }
    public void ShowError(string msg)
    {
        Page.ClientScript.RegisterStartupScript(GetType(), "Script", "<script type=\"text/javascript\">ShowError(\"" + msg + "\");</script>");
    }
    public void ShowSuccess(string msg)
    {
        Page.ClientScript.RegisterStartupScript(GetType(), "Script", "<script type=\"text/javascript\">ShowSuccess(\"" + msg + "\");</script>");
    }
}