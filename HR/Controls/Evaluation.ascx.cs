﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Bussiness;
using CUSTOR.Commen;
using DevExpress.Web;

public partial class Controls_Evaluation : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void DvgEvaluation_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        if (e.Column.FieldName == "strFiscal_year")
        {
            if ((e.Value).ToString().Length != 0)
            {
                string ethiodate;
                DateTime date = Convert.ToDateTime(e.Value);
                ethiodate = EthiopicDateTime.GetEthiopicDate(date.Day, date.Month, date.Year);
                e.DisplayText = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate,p.Organization.LanguageID);
            }
            else
            {
                return;
            }
        }
    }

    protected void DvgEvaluation_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType == GridViewRowType.Footer)
        {
            ASPxGridView gridView = (ASPxGridView)DvgEvaluation;
            if (gridView != null)
            {
                ASPxLabel lblTotal = gridView.FindFooterCellTemplateControl(gridView.Columns["Eval_point"], "lblaverage") as ASPxLabel;
                ASPxSummaryItem si = new ASPxSummaryItem();
                si.FieldName = "Eval_point";
                si.SummaryType = DevExpress.Data.SummaryItemType.Average;
                gridView.TotalSummary.Add(si);
                object value = gridView.GetTotalSummaryValue(si);
                gridView.TotalSummary.Remove(si);

                if (lblTotal != null)
                {
                    if (HidlLanguage.Value == "0")
                    {
                        lblTotal.Text = "Average Result = " + value.ToString();
                    }
                    else if (HidlLanguage.Value == "1")
                    {
                        lblTotal.Text = "አማካኝ ውጤት = " + value.ToString();
                    }
                }
            }
        }
    }

    protected void DvgEvaluation_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["Person"] = (sender as ASPxGridView).GetMasterRowKeyValue();
        Session["ID"] = Session["dtto"];
    }

    protected void LoadEvaluation()
    {
        Guid person = Guid.Parse(Session["APersonGuid"].ToString());
        string documentType = Session["DocumentType"].ToString();
        tblEvaluationBussiness evaluation = new tblEvaluationBussiness();
        DvgEvaluation.DataSource = evaluation.GetEvaluation(DateTime.Today, person);
        DvgEvaluation.DataBind();
    }

    protected void DvgEvaluation_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        LoadEvaluation();
    }
}
