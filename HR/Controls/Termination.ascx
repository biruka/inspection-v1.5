﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Termination.ascx.cs" Inherits="Controls_Termination" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>

<dx:ASPxGridView ID="DVgvtermination" ClientInstanceName="DVgvtermination" runat="server" AutoGenerateColumns="False"
    EnableTheming="True" OnBeforePerformDataSelect="DVgvtermination_BeforePerformDataSelect"
    Width="100%" OnCustomCallback="DVgvtermination_OnCustomCallback"
    OnCustomColumnDisplayText="DVgvtermination_CustomColumnDisplayText" 
    Theme="Office2010Silver" meta:resourcekey="DVgvterminationResource1">
    <Columns>
        <dx:GridViewDataTextColumn ShowInCustomizationForm="True"
            VisibleIndex="0" Caption="የተሰናበቱበት ቀን " FieldName="Terminationdate" meta:resourcekey="GridViewDataTextColumnResource77">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn ShowInCustomizationForm="True" VisibleIndex="3"
            Caption="ደብዳቤ ቁጥር" FieldName="LetterNo" meta:resourcekey="GridViewDataTextColumnResource78">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn ShowInCustomizationForm="True" VisibleIndex="7"
            Caption="የተሰናበቱበት ምክንያት" FieldName="Termination_reason" meta:resourcekey="GridViewDataTextColumnResource79">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn ShowInCustomizationForm="True"
            VisibleIndex="2" Caption="የደብዳቤ ቀን " FieldName="LetterDate" meta:resourcekey="GridViewDataTextColumnResource80">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn ShowInCustomizationForm="True"
            VisibleIndex="9" Caption="ማስታወሻ" FieldName="Remark" meta:resourcekey="GridViewDataTextColumnResource81">
        </dx:GridViewDataTextColumn>
    </Columns>
    <Templates>
        <EmptyDataRow>
            &nbsp;መረጃ አልተገኘም
        </EmptyDataRow>
    </Templates>
</dx:ASPxGridView>
