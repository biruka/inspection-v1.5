﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Scholarship.ascx.cs" Inherits="Controls_Scholarship" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>

<div>
    <dx:ASPxGridView ID="Dvgvscholarship" runat="server" AutoGenerateColumns="False" ClientInstanceName="scholarship" 
        EnableTheming="True" OnBeforePerformDataSelect="ASPxGridView7_BeforePerformDataSelect" OnCustomCallback="Dvgvscholarship_OnCustomCallback"
        Width="100%" OnCustomColumnDisplayText="ASPxGridView7_CustomColumnDisplayText" Theme="Office2010Silver" meta:resourcekey="DvgvscholarshipResource1">
        <Columns>
            <dx:GridViewDataTextColumn FieldName="institution" ShowInCustomizationForm="True"
                VisibleIndex="2" Caption="ተቋም" meta:resourcekey="GridViewDataTextColumnResource101">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="location" ShowInCustomizationForm="True" VisibleIndex="3"
                Caption="ቦታ" meta:resourcekey="GridViewDataTextColumnResource102">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="period_from" ShowInCustomizationForm="True"
                VisibleIndex="4" Caption="ከ" meta:resourcekey="GridViewDataTextColumnResource103">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="period_to" ShowInCustomizationForm="True" VisibleIndex="5"
                Caption="እስከ" meta:resourcekey="GridViewDataTextColumnResource104">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="course_name" ShowInCustomizationForm="True"
                VisibleIndex="0" Caption="ትምህርቱ አይነት" meta:resourcekey="GridViewDataTextColumnResource105">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="training_type" ShowInCustomizationForm="True"
                VisibleIndex="1" Caption="የተከታተለው ትምህርት አይነት" meta:resourcekey="GridViewDataTextColumnResource106">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="sponsor" ShowInCustomizationForm="True" VisibleIndex="6"
                Caption="ድጋፍ ሰጪ" meta:resourcekey="GridViewDataTextColumnResource107">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="remark" ShowInCustomizationForm="True" VisibleIndex="7"
                Caption="ማስታወሻ" meta:resourcekey="GridViewDataTextColumnResource108">
            </dx:GridViewDataTextColumn>
        </Columns>
        <Templates>
            <EmptyDataRow>
                &nbsp;መረጃ አልተገኘም
            </EmptyDataRow>
        </Templates>
    </dx:ASPxGridView>
</div>
