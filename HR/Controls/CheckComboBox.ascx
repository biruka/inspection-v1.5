﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CheckComboBox.ascx.cs" Inherits="Controls_CheckComboBox" %>
<%@ Register assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<script type="text/javascript">
    // <![CDATA[
        var textSeparator = ", ";
        function UpdateText(s, e) {
            var lb = window[s.name + "_ListBox"];
            var selectedItems = lb.GetSelectedItems();
            s.SetText(GetSelectedItemsText(selectedItems));
        }
        function GetSelectedItemsText(items) {
            var texts = [];
            for(var i = 0; i < items.length; i++)
                texts.push(items[i].text);
            return texts.join(textSeparator);
        }
    // ]]>
    </script>
    <aspxdropdownedit ID="ASPxDropDownEdit1" Width="220px" Height="26px" runat="server" ClientIDMode="AutoID" EnableAnimation="False" ReadOnly="true" CssClass="low nonBorder" DropDownWindowHeight="150px" ShowShadow="false">
        <DropDownWindowTemplate>
            <dx:aspxcheckboxlist ID="checkListBox" runat="server" ValueType="System.String" CssClass="dropDownListBox" Font-Size="14px" RepeatLayout="Flow" ItemSpacing="0" TextSpacing="5px">
                <Paddings PaddingTop="1px" PaddingBottom="1px" />
            </dx:ASPxCheckBoxList>
        </DropDownWindowTemplate>
        <ClientSideEvents TextChanged="UpdateText" DropDown="UpdateText" Init="UpdateText" CloseUp="UpdateText" />
    </aspxdropdownedit>


