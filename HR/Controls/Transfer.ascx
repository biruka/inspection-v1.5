﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Transfer.ascx.cs" Inherits="Controls_Transfer" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>

<div>
    <dx:ASPxGridView ID="Dvgvtransfer" ClientInstanceName="Dvgvtransfer" OnCustomCallback="Dvgvtransfer_OnCustomCallback" runat="server" AutoGenerateColumns="False"
        EnableTheming="True" OnBeforePerformDataSelect="Dvgvtransfer_OnBeforePerformDataSelect"
        Width="100%" OnCustomColumnDisplayText="Dvgvtransfer_OnCustomColumnDisplayText"
        Theme="Office2010Silver" meta:resourcekey="DvgvtransferResource1">
        <Columns>
            <dx:GridViewBandColumn Caption="ደረጃ" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewBandColumnResource12">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="የቀድሞ ደረጃ" FieldName="oldGrade" ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource59">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="አዲሱ ደረጃ" FieldName="newGrade" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource60">
                    </dx:GridViewDataTextColumn>
                </Columns>
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewBandColumn>
            <dx:GridViewDataTextColumn Caption="የዝውውር ቀን" FieldName="transfer_date" ShowInCustomizationForm="True"
                VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource61">
            </dx:GridViewDataTextColumn>
            <dx:GridViewBandColumn Caption="የስራ መደብ" ShowInCustomizationForm="True" VisibleIndex="2" meta:resourcekey="GridViewBandColumnResource13">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="የቀድሞ የስራ መደብ" FieldName="from_job_title" ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource62">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="አዲስ የስራ መደብ" FieldName="to_job_title" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource63">
                    </dx:GridViewDataTextColumn>
                </Columns>
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewBandColumn>
            <dx:GridViewBandColumn Caption="የስራ ክፍል" ShowInCustomizationForm="True" VisibleIndex="3" meta:resourcekey="GridViewBandColumnResource14">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="የቀድሞ የስራ ክፍል" FieldName="transferred_from" ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource64">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="አዲስ የስራ ክፍል" FieldName="transferred_to" ShowInCustomizationForm="True" VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource65">
                    </dx:GridViewDataTextColumn>
                </Columns>
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewBandColumn>
            <dx:GridViewBandColumn Caption="ደመወዝ" ShowInCustomizationForm="True" VisibleIndex="6" meta:resourcekey="GridViewBandColumnResource15">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="ከደመወዝ" FieldName="from_salary" ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource66">
                        <PropertiesTextEdit DisplayFormatString="#,00.00"></PropertiesTextEdit>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="ወደ ደመወዝ" FieldName="to_salary" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource67">
                        <PropertiesTextEdit DisplayFormatString="#,00.00"></PropertiesTextEdit>
                    </dx:GridViewDataTextColumn>
                </Columns>
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewBandColumn>
            <dx:GridViewDataTextColumn Caption="የዝውውር አይነት" FieldName="TransferTypeReal" ShowInCustomizationForm="True" VisibleIndex="10" meta:resourcekey="GridViewDataTextColumnResource68">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="የመተካካት ሁኔታ" FieldName="TransferType" ShowInCustomizationForm="True" VisibleIndex="12" meta:resourcekey="GridViewDataTextColumnResource69">
            </dx:GridViewDataTextColumn>
            <dx:GridViewBandColumn Caption="መስሪያ ቤት" ShowInCustomizationForm="True" VisibleIndex="8" meta:resourcekey="GridViewBandColumnResource16">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="ከ" FieldName="FromOrganization" ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource70">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="ወደ" FieldName="ToOrganization" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource71">
                    </dx:GridViewDataTextColumn>
                </Columns>
            </dx:GridViewBandColumn>
        </Columns>
        <Templates>
            <EmptyDataRow>
                &nbsp;መረጃ አልተገኘም
            </EmptyDataRow>
        </Templates>
    </dx:ASPxGridView>
</div>
