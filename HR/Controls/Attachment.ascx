﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Attachment.ascx.cs" Inherits="Controls_Attachment" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>

<script type="text/javascript">
    function OnFileopenClick(element, MainGuid) {
        popUpTab.SetContentHtml(null);
        popUpTab.SetContentUrl('DisplayFile.aspx?MainGuid=' + MainGuid);
        window.popUpTab.SetHeaderText('');
        popUpTab.SetSize(1080, 405);
        popUpTab.ShowAtElement(element);
        popUpTab.Hide();
    }

    function DvgvAttachment_CustomButtonClick(s, e) {
        if (e.buttonID = 'Download') {
            DvgvAttachment.GetRowValues("MainGuid");
        }
    }

</script>
<div>
    <dx:ASPxGridView ID="DvgvAttachment" runat="server" ClientInstanceName="DvgvAttachment"
        OnCustomCallback="DvgvAttachment_OnCustomCallback"
        OnCustomButtonCallback="DvgvAttachment_CustomButtonCallback" AutoGenerateColumns="False"
        OnBeforePerformDataSelect="DvgvAttachment_OnBeforePerformDataSelect"
        Width="100%" KeyFieldName="MainGuid"
        OnCustomColumnDisplayText="DvgvAttachment_OnCustomColumnDisplayText"
        OnSelectionChanged="DvgvAttachment_OnSelectionChanged" OnPageIndexChanged="DvgvAttachment_PageIndexChanged"
        Theme="Office2010Silver" meta:resourcekey="DvgvEmploymentResource1">
        <ClientSideEvents RowClick="function(s, e) 
            { 
                OnFileopenClick(e, s.GetRowKey(DvgvAttachment.GetFocusedRowIndex())); 
            }" />
        <Columns>
            <dx:GridViewDataTextColumn Caption="የሰነዱ ይዘት" FieldName="AttachmentContent" ShowInCustomizationForm="True"
                VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource115">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ሰነዱ የደረሰበት ቀን" FieldName="SentDate" ShowInCustomizationForm="True"
                VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource116">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ሰነዱ የሚገኝበት ቦታ" FieldName="FileLocation"
                ShowInCustomizationForm="True" VisibleIndex="2" Visible="False" meta:resourcekey="GridViewDataTextColumnResource117">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataColumn Caption="ሰነድ" ShowInCustomizationForm="True" VisibleIndex="4" Width="50px" meta:resourcekey="GridViewDataColumnResource1">
                <DataItemTemplate>
                    <a  href='<%# GetDocURL((string)Eval("Url")) %>'>አሳይ</a>
                </DataItemTemplate>
            </dx:GridViewDataColumn>
            <%--<dx:GridViewCommandColumn VisibleIndex="0" ButtonType="Image">
                <CustomButtons>
                    <dx:GridViewCommandColumnCustomButton ID="Download">
                        <Image ToolTip="Download" Url="~/Files/Images/action_bottom.gif" />
                    </dx:GridViewCommandColumnCustomButton>
                </CustomButtons>
            </dx:GridViewCommandColumn>--%>
        </Columns>
        <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" />
        <Styles>
            <FocusedRow BackColor="#AAD2FF">
            </FocusedRow>
        </Styles>
        <Templates>
            <EmptyDataRow>
                &nbsp;የተያያዘ ፋይል አልተገኘም
            </EmptyDataRow>
        </Templates>
    </dx:ASPxGridView>
</div>
