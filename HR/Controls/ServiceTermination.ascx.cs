﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Bussiness;
using CUSTOR.Commen;
using DevExpress.Web;

public partial class Controls_ServiceTermination : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void DVgvtermination_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["person"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }

    protected void LoadTermination()
    {
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        Guid person = Guid.Parse(Session["APersonGuid"].ToString());
        tblTerminationBussiness termination = new tblTerminationBussiness();
        DVgvtermination.DataSource = termination.GetRecords(person, p.Organization.LanguageID.ToString());
        DVgvtermination.DataBind();
    }

    protected void DVgvtermination_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {

        if (e.Column.FieldName == "Terminationdate")
        {
            ProfileCommon p = this.Profile;
            p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
            if ((e.Value).ToString().Length != 0)
            {
                string ethiodate;
                ethiodate = EthiopicDateTime.GetEthiopicDate(((DateTime)(e.Value)).Day, ((DateTime)(e.Value)).Month, ((DateTime)(e.Value)).Year);
                e.DisplayText = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate, p.Organization.LanguageID);
            }
            else
            {
                {
                    return;
                }
            }
        }
    }

    protected void DVgvtermination_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (Convert.ToBoolean(Session["LoadCriterias"]) == true)
        {
            LoadTermination();
        }
    }
}