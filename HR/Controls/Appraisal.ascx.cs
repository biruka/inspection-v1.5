﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using CUSTOR;
using DevExpress.Web;
using System.Text;
using CUSTOR.Commen;

public partial class Controls_Appraisal : System.Web.UI.UserControl
{
    public bool isEvaluationpt = false;
    public double Evaluationpt = 0;
    public double Evaluationpt1 = 0;
    public double Evaluationpt2 = 0;
    public static bool _IsNewRecord;
    public bool IsNewRecord
    {
        get { return _IsNewRecord; }
        set { _IsNewRecord = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                //SetMenu();
                //FillPeriod();
                //FillEvaluationLevel();
                // DoNew();
            }
           // BindAppraisalGrid();
        }
        catch
        {

        }
    }
   
    protected void LoadAppraissal()
    {

        CEvaluation objtblEvaluation = new CEvaluation();
        DataTable dt = new DataTable();

        objtblEvaluation.ParentGuid = Guid.Parse(Session["APersonGuid"].ToString());

        dt = objtblEvaluation.GetRecords();
        if (dt.Rows.Count > 0)
        {
            gvwAppraisal.ClientVisible = true;
            gvwAppraisal.DataSource = dt;
            gvwAppraisal.DataBind();


        }

        //Guid person = Guid.Parse(Session["APersonGuid"].ToString());
        //var documentType = Session["DocumentType"].ToString();
        //tblDiciplineBussiness discipline = new tblDiciplineBussiness();
        //Dvgvdeciplin.DataSource = discipline.GetRecords(person);
        //Dvgvdeciplin.DataBind();
    }


    protected void gvwAppraisal_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (Convert.ToBoolean(Session["LoadCriterias"]) == true)
        {
            LoadAppraissal();
        }
    }



    protected void grvEvaluationDetail_Init(object sender, EventArgs e)
    {
        object key = ((ASPxGridView)sender).GetMasterRowKeyValue();
        if (key != null)
        {
            EvaluationDetail.CEvaluationDetail ObjEvaluationDeatil = new EvaluationDetail.CEvaluationDetail();
            DataTable dt = new DataTable();

            ObjEvaluationDeatil.EvaluationGuid = new Guid(key.ToString());
            dt = ObjEvaluationDeatil.Records();

            ASPxGridView grid = sender as ASPxGridView;
            grid.DataSource = dt;
            grid.DataSourceID = string.Empty;
            grid.DataBind();
        }
    }

    protected void gvwAppraisal_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {

        if (e.Column.FieldName == "fiscal_year")
            if ((e.Value).ToString().Length != 0)
            {
                DateTime fiscalyear = (DateTime)(e.Value);
                if (DateTime.Parse(e.Value.ToString()).ToShortDateString() == DateTime.MaxValue.ToShortDateString())
                {
                    e.DisplayText = "";
                }
                else
                {
                    e.DisplayText = EthiopicDateTime.GetEthiopicDate(fiscalyear.Day, fiscalyear.Month, fiscalyear.Year).ToString();
                }
            }
            else
            {
                { return; }
            }

    }

  
    protected void gvwAppraisal_BeforePerformDataSelect(object sender, EventArgs e)
    {
        gvwAppraisal.DetailRows.CollapseAllRows();
       

    }

}