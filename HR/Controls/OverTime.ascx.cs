﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Bussiness;
using DevExpress.Web;

public partial class Controls_OverTime : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Dvgvovertime_OnBeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["person"] = (sender as ASPxGridView).GetMasterRowKeyValue();

    }

    protected void LoadOvertime()
    {
        Guid person = Guid.Parse(Session["APersonGuid"].ToString());
        tblOverTimeBussiness overTime = new tblOverTimeBussiness();
        DataTable dtOvertime = overTime.GetRecords(person);
        Dvgvovertime.DataSource = dtOvertime;
        Dvgvovertime.DataBind();

    }

    protected void Dvgvovertime_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        LoadOvertime();
    }
}