﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Domain;
using CUSTOR.Business;
using CUSTOR.Bussiness;
using CUSTOR.Commen;
using DevExpress.Web;

public partial class Controls_Promotion : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void DvgvPromotion_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["person"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }

    protected void DvgvPromotion_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        if (e.Column.FieldName == "date_promoted")
        {
            if ((e.Value).ToString().Length != 0)
            {
                string ethiodate;
                DateTime date = Convert.ToDateTime(e.Value);
                ethiodate = EthiopicDateTime.GetEthiopicDate(date.Day, date.Month, date.Year);
                e.DisplayText = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate,
                    Convert.ToInt32(p.Organization.LanguageID));
            }
            else
            {
                {
                    return;
                }

            }
        }
        if (e.Column.FieldName == "promotion_type")
        {
            if ((e.Value).ToString().Length != 0)
            {
                if (p.Organization.LanguageID == 1)
                {
                    switch (e.Value.ToString())
                    {
                        case "1":
                            e.DisplayText = "የደረጃ";
                            break;
                        case "2":
                            e.DisplayText = "የእርከን";
                            break;
                        case "4":
                            e.DisplayText = "በድልድል/የሥራ ምደባ";
                            break;
                        case "5":
                            e.DisplayText = "በእንደገና ድልድል";
                            break;
                        case "6":
                            e.DisplayText = "የደመወዝ ጭማሪ";
                            break;
                    }
                }
                if (p.Organization.LanguageID == 2)
                {
                    switch (e.Value.ToString())
                    {
                        case "1":
                            e.DisplayText = "Sadarkaa";
                            break;
                        case "2":
                            e.DisplayText = "Gulaantaa";
                            break;
                        case "4":
                            e.DisplayText = "Ramaddi Irraaa Deebiittin";
                            break;
                        case "5":
                            e.DisplayText = "Ramaddi Irraaa Deebiittin";
                            break;
                        case "6":
                            e.DisplayText = "Mindaa Dabalaata";
                            break;
                    }
                }
            }
        }
    }

    protected void LoadPromotions()
    {
        Guid person = Guid.Parse(Session["APersonGuid"].ToString());
        int documentType = Convert.ToInt32(Session["DocumentType"].ToString());
        tblPromotionHistoryBussiness promotions = new tblPromotionHistoryBussiness();
        DvgvPromotion.DataSource = promotions.GetRecords(person, documentType);
        DvgvPromotion.DataBind();

    }

    protected void DvgvPromotion_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (Convert.ToBoolean(Session["LoadCriterias"]) == true)
        {
            LoadPromotions();
        }
    }
}