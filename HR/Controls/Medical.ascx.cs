﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Bussiness;
using CUSTOR.Commen;
using DevExpress.Web;

public partial class Controls_Medical : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void ASPxGridView5_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["person"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }

    protected void ASPxGridView5_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        if (e.Column.FieldName != "diagnostic_date") { return; }
        if ((e.Value).ToString().Length != 0)
        {
            string ethiodate;
            ethiodate = EthiopicDateTime.GetEthiopicDate(((DateTime)(e.Value)).Day, ((DateTime)(e.Value)).Month, ((DateTime)(e.Value)).Year);
            e.DisplayText = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate, Convert.ToInt32(p.Organization.LanguageID));
        }

    }

    protected void LoadMedicaleInformation()
    {
        Guid person = Guid.Parse(Session["APersonGuid"].ToString());
        tblMedicalHistoryBussiness termination = new tblMedicalHistoryBussiness();
        DataTable dtMedicalInfo = termination.GetRecords(person);
        Dvgvhealth.DataSource = dtMedicalInfo;
        Dvgvhealth.DataBind();
    }

    protected void Dvgvhealth_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        LoadMedicaleInformation();
    }
}