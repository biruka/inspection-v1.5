﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Bussiness;
using CUSTOR.Commen;
using DevExpress.Web;

public partial class Controls_WorkExperiance : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void DvgvWorkexp_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        if (e.Column.FieldName == "date_from")
            if ((e.Value).ToString().Length != 0)
            {
                string ethiodate;
                Session["fromDate"] = e.Value;
                ethiodate = EthiopicDateTime.GetEthiopicDate(((DateTime)(e.Value)).Day, ((DateTime)(e.Value)).Month, ((DateTime)(e.Value)).Year);
                e.DisplayText = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate,Convert.ToInt32(p.Organization.LanguageID));
            }
         if (e.Column.FieldName == "date_to")
            if ((e.Value).ToString().Length != 0)
            {
                string toDate = string.Format("{0:M/d/yyyy}", (DateTime)e.Value);
                if (toDate != "1/1/1909")
                {
                    string ethiodate;
                    ethiodate = EthiopicDateTime.GetEthiopicDate(((DateTime)(e.Value)).Day, ((DateTime)(e.Value)).Month, ((DateTime)(e.Value)).Year);
                    e.DisplayText = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate,Convert.ToInt32(p.Organization.LanguageID));
                    
                }
                else
                {
                    e.DisplayText = "በስራ ላይ";
                    
                }
            }
            else
            {
                { return; }
            }
            if (e.Column.FieldName == "net_duration")
            {
                object value = e.Value;
                if (Convert.ToInt32(value) < 0)
                {
                    DateTime latestWorkExperiance = Convert.ToDateTime(Session["fromDate"].ToString());
                    e.DisplayText = DateTime.Today.Year.CompareTo(latestWorkExperiance.Year).ToString();
                }
            }
    }

    protected void DvgvWorkexp_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["person"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }

    protected void LoadWorkExperiance()
    {
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        Guid person = Guid.Parse(Session["APersonGuid"].ToString());
        string documentType = Session["DocumentType"].ToString();
        employmenthistryentityviewBussiness promotions = new employmenthistryentityviewBussiness();
        DvgvWorkexp.DataSource = promotions.GetRecords(person,p.Organization.LanguageID.ToString());
        DvgvWorkexp.DataBind();
    }


    protected void DvgvWorkexp_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (Convert.ToBoolean(Session["LoadCriterias"]) == true)
        {
            LoadWorkExperiance();
        }
    }
}
