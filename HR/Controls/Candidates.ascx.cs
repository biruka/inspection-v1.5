﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Bussiness;
using DevExpress.Web;

public partial class Controls_Candidates : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
      //  if (!IsPostBack)
            LoadCandidates();
    }

    protected void DvGcandidate_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["person"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }

    protected void LoadCandidates()
    {
        Guid person = Guid.Empty ;
        if(Session["APersonGuid"]!=null)
          person = Guid.Parse(Session["APersonGuid"].ToString());
        string documentType = "311";
        if (Session["DocumentType"] != null)
          documentType = Session["DocumentType"].ToString();
        CandidateEntityViewBussiness candidate = new CandidateEntityViewBussiness();
       // candidate.GetRecords(person, documentType);
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        DvGcandidate.DataSource = candidate.GetRecords(person, documentType,p.Organization.LanguageID.ToString());
        DvGcandidate.DataBind();
    }

    protected void DvGcandidate_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (Convert.ToBoolean(Session["LoadCriterias"]) == true)
        {
            LoadCandidates();
             
        }
    }
}