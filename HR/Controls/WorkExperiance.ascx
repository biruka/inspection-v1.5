﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WorkExperiance.ascx.cs" Inherits="Controls_WorkExperiance" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>

<div>
    <dx:ASPxGridView ID="DvgvWorkexp" runat="server" OnCustomCallback="DvgvWorkexp_OnCustomCallback" ClientInstanceName="DvgvWorkexp" AutoGenerateColumns="False" EnableTheming="True"
        OnBeforePerformDataSelect="DvgvWorkexp_BeforePerformDataSelect"
        OnCustomColumnDisplayText="DvgvWorkexp_CustomColumnDisplayText"
        Width="100%" Theme="Office2010Silver" meta:resourcekey="DvgvWorkexpResource1">
        <Columns>
            <dx:GridViewDataTextColumn Caption="መስሪያ ቤት" FieldName="institution" ShowInCustomizationForm="True"
                VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource30">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="የስራ መደብ" FieldName="job_title" ShowInCustomizationForm="True"
                VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource31">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="የትምህርት ደረጃ" FieldName="EducationLevel" ShowInCustomizationForm="True"
                VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource32">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="የቅጥር ሁኔታ" FieldName="emp_status" ShowInCustomizationForm="True" VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnResource33">
            </dx:GridViewDataTextColumn>
            <dx:GridViewBandColumn Caption="ቀን" ShowInCustomizationForm="True" VisibleIndex="4" meta:resourcekey="GridViewBandColumnResource6">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="ከ" FieldName="date_from" ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource34">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="እስከ" FieldName="date_to" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource35">
                    </dx:GridViewDataTextColumn>
                </Columns>
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewBandColumn>
            <dx:GridViewDataTextColumn Caption="የስራ ልምድ"  FieldName="WorkExperiencePeriod" ShowInCustomizationForm="True" VisibleIndex="5" meta:resourcekey="GridViewDataTextColumnResource36">
                 <CellStyle HorizontalAlign="Center"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="አጠቃላይ የስራ ልምድ በዓመት"  FieldName="WorkExperienceNetDuration" ShowInCustomizationForm="True" VisibleIndex="6" meta:resourcekey="GridViewDataTextColumnResource37">
                <CellStyle HorizontalAlign="Center"></CellStyle>
            </dx:GridViewDataTextColumn>
        </Columns>
        <Settings UseFixedTableLayout="true" ShowFooter="true" />
        <SettingsBehavior AllowSort="false" />
        <TotalSummary>
            <dx:ASPxSummaryItem FieldName="WorkExperienceNetDuration" ValueDisplayFormat="{0:n}" SummaryType="Sum"></dx:ASPxSummaryItem>
        </TotalSummary>
        <Templates>
            <EmptyDataRow>
                &nbsp;መረጃ አልተገኘም
            </EmptyDataRow>
        </Templates>
    </dx:ASPxGridView>
    <asp:HiddenField ID="HidlLanguage" runat="server" />
</div>
