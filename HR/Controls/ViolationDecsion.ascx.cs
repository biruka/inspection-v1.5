﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Bussiness;
using CUSTOR.Commen;
using DevExpress.Web;

public partial class Controls_ViolationDecsion : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void Dvgvdeciplin_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["person"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }

    protected void Dvgvdeciplin_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {

        if (e.Column.FieldName == "date_action_taken")
        {
            ProfileCommon p = this.Profile;
            p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
            if ((e.Value).ToString().Length != 0)
            {
                string ethiodate;
                ethiodate = EthiopicDateTime.GetEthiopicDate(((DateTime) (e.Value)).Day, ((DateTime) (e.Value)).Month,((DateTime)(e.Value)).Year);
                e.DisplayText = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate,Convert.ToInt32(p.Organization.LanguageID));
            }
            else
            {
                {
                    return;
                }
            }
        }
    }

    protected void LoadViolationDescision()
    {
        Guid person = Guid.Parse(Session["APersonGuid"].ToString());
        tblDiciplineBussiness discipline = new tblDiciplineBussiness();
        Dvgvdeciplin.DataSource = discipline.GetRecords(person);
        Dvgvdeciplin.DataBind();
    }


    protected void Dvgvdeciplin_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        LoadViolationDescision();
    }
}