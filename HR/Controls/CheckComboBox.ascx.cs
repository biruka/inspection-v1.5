﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using System.Collections;

public partial class Controls_CheckComboBox : UserControl {
    protected ASPxCheckBoxList CheckListBox { get { return (ASPxDropDownEdit1.FindControl("checkListBox") as ASPxCheckBoxList); } }

    public object DataSource { get { return CheckListBox.DataSource; } set { CheckListBox.DataSource = value; } }
    public new void DataBind() {
        CheckListBox.DataBind();
    }
    public IEnumerable Values {
        get { return CheckListBox.SelectedValues; }
        set {
            foreach(var item in value)
                CheckListBox.Items.FindByValue(item).Selected = true;
        }
    }
    public Type ValueType {
        set { 
            CheckListBox.ValueType = value;
            if(value == typeof(int) && !CheckListBox.CssClass.Contains("num"))
                CheckListBox.CssClass += " num";
        }
    }

    protected void Page_Init(object sender, EventArgs e) {
        //ASPxDropDownEdit1.ClientInstanceName = this.ID + "_DD";
        //CheckListBox.ClientInstanceName = ASPxDropDownEdit1.ClientID + "_ListBox";
    }
}
