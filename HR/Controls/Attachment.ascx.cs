﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using CUSTOR.Bussiness;
using CUSTOR.Domain;
using CUSTOR;
using CUSTOR.Commen;
using System.IO;
using System.Threading;
using System.Drawing;
using System.Data;
using System.Security.Principal;
using System.Text;
using System.Configuration;
using System.Resources;
////using System.Windows.Documents;
using CUSTOR.Commen;
public partial class Controls_Attachment : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void DvgvAttachment_OnCustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        if (e.Column.FieldName == "SentDate")
        {
            string ethiodate;
            DateTime date = Convert.ToDateTime(e.Value);
            ethiodate = EthiopicDateTime.GetEthiopicDate(date.Day, date.Month, date.Year);
            e.DisplayText = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate, Convert.ToInt32(p.Organization.LanguageID));
        }
    }

    protected void DvgvAttachment_OnSelectionChanged(object sender, EventArgs e)
    {
        if (Session["j"] != null)
        {
            int j = Convert.ToInt16(Session["j"].ToString());
            ASPxGridView GridAtt = (ASPxGridView)DvgvAttachment;
            if (GridAtt != null)
            {
                GridAtt.Focus();
                int i = GridAtt.FocusedRowIndex;
                string MainGuid = GridAtt.GetRowValues(i, GridAtt.KeyFieldName).ToString();
                Session["AMainGuid"] = MainGuid;
            }
        }
    }

    protected void DvgvAttachment_OnBeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["ParentGuid"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }

    public static string MimeType(string Extension)
    {
        string mime = "application/octetstream";
        if (string.IsNullOrEmpty(Extension))
            return mime;

        string ext = Extension.ToLower();
        Microsoft.Win32.RegistryKey rk = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
        if (rk != null && rk.GetValue("Content Type") != null)
            mime = rk.GetValue("Content Type").ToString();
        return mime;
    } 

    protected bool DoFindAttachment(Guid MainGuid)
    {
        try
        {
            Attachment objAttachment = new Attachment();
            AttachmentBussiness objAttachmentBusiness = new AttachmentBussiness();
            objAttachment = objAttachmentBusiness.GetAttachment(MainGuid);
            string url = objAttachment.Url;
            string NavigateUrl = @"~/Attachment/" + url; //this is Acctual path where the HR Attachment is exist;
            Response.ContentType = "APPLICATION/OCTET-STREAM";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + NavigateUrl);
            Response.TransmitFile(Server.MapPath(NavigateUrl));
            Response.Flush();
            Response.SuppressContent = true;
            
            //FileInfo fi = new FileInfo(NavigateUrl);
            //System.Diagnostics.Process.Start(NavigateUrl);
            //long sz = fi.Length;
            //FileStream MyFileStream = new FileStream(NavigateUrl, FileMode.Open);
            //sz = MyFileStream.Length;
            //byte[] Buffer = new byte[(int)sz];
            //MyFileStream.Read(Buffer, 0, (int)MyFileStream.Length);
            //MyFileStream.Close();
            //Response.ContentType = MimeType(Path.GetExtension(NavigateUrl));
            //Response.AddHeader("Content-Disposition", string.Format("attachment; filename = {0}", System.IO.Path.GetFileName(NavigateUrl)));
            //Response.AddHeader("Content-Length", sz.ToString("F0"));
            //Response.BinaryWrite(Buffer);
            //// string fName = NavigateUrl;
            //FileInfo fi = new FileInfo(fName);
            //long sz = fi.Length;
            //string filePath = Path.Combine(fName);
            //Response.ClearContent();
            //Response.ContentType = MimeType(Path.GetExtension(fName));
            //Response.AddHeader("Content-Disposition", string.Format("attachment; filename = {0}", System.IO.Path.GetFileName(fName)));
            //Response.AddHeader("Content-Length", sz.ToString("F0"));
            //Response.TransmitFile(fName);
            //Response.Flush();
            //Response.SuppressContent = true;
            //FileStream fileStream = new FileStream(NavigateUrl, FileMode.Open, FileAccess.Read);

            ////Response.a = true;
            //Response.End();
        }
        catch
        {

        }

        return true;
    }

    protected void btnView3_Click(object sender, EventArgs e)
    {
        if (Session["APersonGuid"] != null)
        {
             DoFindAttachment(Guid.Parse(Session["APersonGuid"].ToString()));
        }
    }

    protected void LoadAttachments()
    {
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        Guid person = Guid.Parse(Session["APersonGuid"].ToString());
        string documentType = Session["DocumentType"].ToString();
        AttachmentBussiness candidate = new AttachmentBussiness();
        DataTable dtAttachment  = candidate.GetRecords(person, documentType, p.Organization.LanguageID.ToString());
        DvgvAttachment.DataSource = dtAttachment;
        DvgvAttachment.DataBind();
    }

    public string GetDocURL(string strID)
    {
        try
        {
            //return Path.Combine(MapPath(UploadDirectory), strID);
            return "../Attachment/" + strID;
        }
        catch
        {
            return string.Empty;
        }
    }


    protected void DvgvAttachment_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        LoadAttachments();
    }

    protected void DvgvAttachment_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
    {
        if (e.ButtonID == "Download")
        {
            object attachmentGuid = DvgvAttachment.GetRowValues(e.VisibleIndex, "MainGuid");
            Attachment objAttachment = new Attachment();
            AttachmentBussiness objAttachmentBusiness = new AttachmentBussiness();
            objAttachment = objAttachmentBusiness.GetAttachment((Guid)attachmentGuid);
            string NavigateUrl = @"~/Attachment/" + objAttachment.Url;; //this is Acctual path where the HR Attachment is exist;
            try
            {
                HttpContext.Current.Response.ContentType = "APPLICATION/OCTET-STREAM";
                string header = "Attachment; FileName=" + NavigateUrl;
                HttpContext.Current.Response.AppendHeader("Content-Disposition", header);
                System.IO.FileInfo file = new System.IO.FileInfo(Server.MapPath(NavigateUrl));
                //HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename = {0}", System.IO.Path.GetFileName(NavigateUrl)));
                HttpContext.Current.Response.WriteFile(file.FullName);
                //HttpContext.Current.Response.End();
            }
            catch (Exception ex)
            { 
                
            }
        }
    }

    protected void DvgvAttachment_PageIndexChanged(object sender, EventArgs e)
    {
        LoadAttachments();
    }
}