﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlTypes;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Bussiness;
using CUSTOR.Commen;
using CUSTOR;
using CUSTOR.Domain;
using DevExpress.Web;
using DevExpress.Web.ASPxHtmlEditor;
using DevExpress.XtraPrinting.Native.LayoutAdjustment;

public partial class Controls_Inspection : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (cbInspection.IsCallback || Page.IsCallback)
        {
            ArrayList selectedChecklistItems = new ArrayList();
            if (ChklstApprove.Items.Count > 0)
            {
                for (int index = 1; index <= ChklstApprove.Items.Count; index++)
                {
                    if (ChklstApprove.Items[index].Selected)
                    {
                        selectedChecklistItems.Add(ChklstApprove.Items[index].Value);
                    }
                }

            }
            if (Session["isApproved"] != null)
            {
                if (Session["isApproved"].ToString() == "True")
                {
                    btnsave.Visible = false;
                    btnNew.Visible = false;
                    btnApproved.Visible = false;
                }
                else
                {
                    if (HttpContext.Current.User.IsInRole("Inspection officer"))
                    {
                        btnsave.Text = "ላክ";
                    }
                }
            }
            LoadInspectionCheckList(selectedChecklistItems);
        }
    }

    enum serviceType
    {
        recruitment = 311,
        promotion,
        Transfer,
        serviceextension,
        servicetermination,
        disciplinary,
        health,
        overtime,
        scholarship,
        Authoritydeligation,
        Salary = 341
    }

    private void ShowErrorMessage(List<string> strMessages)
    {

        StringBuilder sb = new StringBuilder();
        sb.Append("<ul>");
        foreach (string s in strMessages)
        {
            sb.Append(String.Format("<li>{0}</li>", s));
        }
        sb.Append("</ul>");

        //MessageBox1.ShowError(sb.ToString());
        CMsgBar.ShowError(sb.ToString(), inspectionPanle, lblInspectionLabel);
    }

    protected bool IsValidInspectionRecord(ArrayList selectedInspectionList)
    {
        //List<string> errMessages = GetErrorMessaages();
        //if (errMessages.Count > 0)
        //{
        //    ShowErrorMessage(errMessages);
        //    return false;
        //}
        //-----------------------2016-----
        //----2016--------
        pnlInspectionList.Visible = true;
        //pnlInspectionDetail.Visible = true;
        //----------------------
        List<string> errMessages = GetErrorMessaages(selectedInspectionList);
        StorePreErrorValues();

        if (errMessages.Count > 0)
        {
            RestoreComboValues();
            ShowErrorMessage(errMessages);
            return false;
        }
        //--------------------------------

        return true;
    }

    protected bool DocumentMustBeAttached(ArrayList selectedInspectionList)
    {
        bool notValid = false;
        int count = 0;
        //Get inspection criteris list for the selected hr activity
        tblDocumentBussiness objDoc = new tblDocumentBussiness();
        DataTable objDocGetRecordCheckList = objDoc.GetRecordCheckListNotOptioal(Session["DocumentType"].ToString(), Session["emp_status"].ToString(), Convert.ToInt32(Session["Language"].ToString())); ;
        for (int index = 0; index < objDocGetRecordCheckList.Rows.Count; index++)
        {
            for (int selectedIndex = index; selectedIndex < selectedInspectionList.Count; selectedIndex++)
            {
                if (objDocGetRecordCheckList.Rows[index]["code"].ToString() ==
                    selectedInspectionList[selectedIndex].ToString())
                {
                    break;
                }
                else
                {
                    count++;
                }
            }
        }
        if (count > 0)
        {
            notValid = true;
        }
        return notValid;
    }

    private void RestoreComboValues()
    {
        try
        {
            //Cascaded combos lose their items in this case
            string Restore_Decision_Type = Session["Restore_Decision_Type"].ToString();
            string Restore_Inspection_Type = Session["Restore_Inspection_Type"].ToString();

            if (!string.IsNullOrEmpty(Restore_Decision_Type))
            {
                if (Session["Language"].ToString() == "0")
                {
                    FillCombo(Language.eLanguage.eEnglish, typeof(Enumeration.InspectionDecisionType));
                    FillInspectionTypeCombo(Language.eLanguage.eEnglish, typeof (Enumeration.InspectionType));
                }
                else if (Session["Language"].ToString() == "1")
                {
                    FillCombo(Language.eLanguage.eAmharic, typeof(Enumeration.InspectionDecisionType));
                    FillInspectionTypeCombo(Language.eLanguage.eEnglish, typeof(Enumeration.InspectionType));
                }
                else if (Session["Language"].ToString() == "2")
                {
                    FillCombo(Language.eLanguage.eAfanOromo, typeof(Enumeration.InspectionDecisionType));
                    FillInspectionTypeCombo(Language.eLanguage.eEnglish, typeof(Enumeration.InspectionType));
                }
                ComboDecision.Value = Restore_Decision_Type;
                cboInspectionType.Value = Restore_Inspection_Type;
            }
        }
        catch
        {
            //do nothing
        }

    }

    private void StorePreErrorValues()
    {
        try
        {
            //Cascaded combos lose their items in this case
            string Decision_Type = string.Empty;
            string inspectionType = String.Empty;

            //check if the combos hold selections
            if (ComboDecision.Value != null)
            {
                Decision_Type = ComboDecision.Value.ToString();
            }
            if (cboInspectionType.Value != null)
            {
                inspectionType = cboInspectionType.Value.ToString();
            }

            Session["Restore_Decision_Type"] = Decision_Type;
            Session["Restore_Inspection_Type"] = inspectionType;

        }
        catch
        {
            //do nothing
        }
    }

    private List<string> GetErrorMessaages(ArrayList selectedInspectionList)
    {
        List<string> errMessages = new List<string>();
        int decision = Convert.ToInt32(Enumeration.InspectionDecisionType.Complete);
        if ((DocumentMustBeAttached(selectedInspectionList) && Convert.ToInt32(ComboDecision.Value) == decision))
        {
            errMessages.Add(Resources.Message.MSG_VALIDINSPECTIONECISION);
        }
        //if ( ComboDecision.Text == Resources.Message.MSG_COMPLATE)
        //    {
        //        if (Getchklist() == false)
        //        {
        //             errMessages.Add(Resources.Message.MSG_VALIDATECOMPLATE);
        //        }
        //    }
        if (ComboDecision.Value.ToString() == "-1")
             errMessages.Add(Resources.Message.MSG_NotVALIDINSPECTIONDECISION);
        if (cboInspectionType.Value.ToString() == "-1")
            errMessages.Add(Resources.Message.MSG_NotVALIDINSPECTIONTYPE);
        if (!cdpInspectionDate.IsValidDate)
            errMessages.Add(Resources.Message.MSG_DDATE);
        else
        {
            if(cdpInspectionDate.IsFutureDate)
            {
                errMessages.Add(Resources.Message.MSG_InspectionDateIsFutureDate);
            }
        }
        return errMessages;
    }

    protected string GetDecisionvalue()
    {
        if (ComboDecision.Value != "-1")
        {
            switch (ComboDecision.Text.ToString())
            {
                case "የተሟላ":
                    return "1";
                    break;
                case "ያልተሟላ":
                    return "2";
                    break;
                case "የተሳሳተ":
                    return "3";
                    break;
            }
            return ComboDecision.Value.ToString();
        }
        else
        {
            return "0";
        }
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        FillInspectionDeafaultDate();
        Session["canbtn"] = true;
        Session["IsNew"] = true;
        ASPxGridView Gridinsp = (ASPxGridView)DvgvInspection;
        if (Gridinsp != null)
        {
            ASPxComboBox combo = Gridinsp.FindEditFormTemplateControl("ComboDecision") as ASPxComboBox;
            ASPxMemo memo = Gridinsp.FindEditFormTemplateControl("Memo") as ASPxMemo;
            ASPxCheckBoxList cbl = Gridinsp.FindEditFormTemplateControl("ChklstApprove") as ASPxCheckBoxList;
            ASPxRoundPanel panswer = Gridinsp.FindEditFormTemplateControl("pnanswer") as ASPxRoundPanel;
            ASPxLabel lblanswer = Gridinsp.FindEditFormTemplateControl("ASPxLabel4") as ASPxLabel;
            ASPxLabel lblanswertxt = Gridinsp.FindEditFormTemplateControl("ASPxLabel5") as ASPxLabel;
            ASPxPanel pnlreference = Gridinsp.FindEditFormTemplateControl("pnlreference") as ASPxPanel;
            ASPxComboBox como = Gridinsp.FindEditFormTemplateControl("comboreference") as ASPxComboBox;
            panswer.Visible = false;
            int count = cbl.Items.Count;
            for (int i = 0; i < count; i++)
            {
                if (cbl.Items[i].Selected)
                {
                    //do for selected clear
                    cbl.Items[i].Selected = false;
                }
            }
            if (pnlreference != null)
            {
                pnlreference.Visible = false;
            }
            if (memo != null)
            {
                memo.Text = "";
            }
            if (combo != null)
            {
                combo.Text = "";
            }
        }
    }

    protected void btnApproved_Click1(object sender, EventArgs e)
    {
        tblInspection objInspection = new tblInspection();
        InspectionBussiness objInspectionBusiness = new InspectionBussiness();
        try
        {
            objInspection.approvalname = Convert.ToString(Session["ProfileFullname"].ToString());
            objInspection.InspectionGUID = Guid.Parse(Session["InspectionGuid"].ToString()); //Attention;
            objInspection.approvaldate = DateTime.Today;
            objInspection.Approved = true;
            if (objInspectionBusiness.UpdateApproval(objInspection))
            {
                //LoadInspection();
                CMsgBar.ShowSucess(Resources.Message.MSG_INSPECTIONPPROVED, inspectionPanle, lblInspectionLabel);
            }
        }
        catch (Exception ex)
        {
            ShowError(ex.Message);
        }
    }

    private void FillCombo(Language.eLanguage eLang, Type t)
    {
        ComboDecision.ValueField = "Key";
        ComboDecision.TextField = "Value";
        ComboDecision.DataSource = EnumHelper.GetEnumDescriptions(t, eLang);
        ComboDecision.DataBind();
        if (Session["Language"].ToString() == "0")
        {
            ListEditItem le = new ListEditItem("--Select--", "-1");
            ComboDecision.Items.Insert(0, le);
            ComboDecision.Text = "-1";
        }
        else if (Session["Language"].ToString() == "1")
        {
            ListEditItem le = new ListEditItem("ይምረጡ", "-1");
            ComboDecision.Items.Insert(0, le);
            ComboDecision.Text = "-1";
        }
        else if (Session["Language"].ToString() == "2")
        {
            ListEditItem le = new ListEditItem("--mret--", "-1");
            ComboDecision.Items.Insert(0, le);
            ComboDecision.Text = "-1";
        }

    }

    private void FillInspectionTypeCombo(Language.eLanguage eLang, Type t)
    {
        cboInspectionType.ValueField = "Key";
        cboInspectionType.TextField = "Value";
        cboInspectionType.DataSource = EnumHelper.GetEnumDescriptions(t, eLang);
        cboInspectionType.DataBind();
        if (Session["Language"].ToString() == "0")
        {
            ListEditItem le = new ListEditItem("--Select--", "-1");
            cboInspectionType.Items.Insert(0, le);
            cboInspectionType.Text = "-1";
        }
        else if (Session["Language"].ToString() == "1")
        {
            ListEditItem le = new ListEditItem("ይምረጡ", "-1");
            cboInspectionType.Items.Insert(0, le);
            cboInspectionType.Text = "-1";
        }
        else if (Session["Language"].ToString() == "2")
        {
            ListEditItem le = new ListEditItem("--mret--", "-1");
            cboInspectionType.Items.Insert(0, le);
            cboInspectionType.Text = "-1";
        }
 
        cboInspectionType.SelectedIndex = 2;
 
    }

    protected void LoadInspectionTypes()
    {
        if (Session["j"] != null)
        {
            ComplaintBusiness obj = new ComplaintBusiness();
            if (Session["Language"].ToString() == "0")
            {
                FillInspectionTypeCombo(Language.eLanguage.eEnglish, typeof(Enumeration.InspectionType));
            }
            else if (Session["Language"].ToString() == "1")
            {
                FillInspectionTypeCombo(Language.eLanguage.eAmharic, typeof(Enumeration.InspectionType));
            }
            else if (Session["Language"].ToString() == "2")
            {
                FillInspectionTypeCombo(Language.eLanguage.eAfanOromo, typeof(Enumeration.InspectionType));
            }
            if((bool)Session["NormalInspectionType"] == true)
            {
                cboInspectionType.Value = ((int)Enumeration.InspectionType.RegularInspection).ToString();
            }
            else
            {
                cboInspectionType.Value = ((int)Enumeration.InspectionType.complain).ToString();
            }
        }
    }

    protected void LoadInspectionDecisions()
    {
        if (Session["j"] != null)
        {
            ComplaintBusiness obj = new ComplaintBusiness();
            if (Session["Language"].ToString() == "0")
            {
                FillCombo(Language.eLanguage.eEnglish, typeof(Enumeration.InspectionDecisionType));
            }
            else if (Session["Language"].ToString() == "1")
            {
                FillCombo(Language.eLanguage.eAmharic, typeof(Enumeration.InspectionDecisionType));
            }
            else if (Session["Language"].ToString() == "2")
            {
                FillCombo(Language.eLanguage.eAfanOromo, typeof(Enumeration.InspectionDecisionType));
            }

        }
    }

    protected bool Getchklist() // check weather checklist select all or not
    {
        bool result = true;
        int count = ChklstApprove.Items.Count;
        int selectedCount = 0;
        for (int i = 0; i < count; i++)
        {
            if (ChklstApprove.Items[i].Selected == false)
            {
                selectedCount++;
            }
        }
        if (selectedCount == count)
        {
            result = false;
        }
        return result;
    }

    protected bool GetApprovevalue(Guid InspGuid)
    {
        try
        {
            tblInspection objInspection = new tblInspection();
            InspectionBussiness objInspectionBusiness = new InspectionBussiness();
            objInspection = objInspectionBusiness.GetInspection(InspGuid);
            return objInspection.Approved;
        }
        catch
        {
            return false;
        }

    }

    private void FillInspectionDeafaultDate()
    {
        try
        {
            DateTime dt = System.DateTime.Today;

            int day = Convert.ToInt32(dt.Day);
            int month = Convert.ToInt32(dt.Month);
            int year = Convert.ToInt32(dt.Year);

            if (EthiopicDateTime.IsValidGregorianDate(day, month, year) == true)
            {

                ASPxGridView Gridinsp = (ASPxGridView)DvgvInspection;
                if (Gridinsp != null)
                {
                    ASPxTextBox txtYear = Gridinsp.FindEditFormTemplateControl("txtFileYearFrom") as ASPxTextBox;
                    ASPxTextBox txtMonth = Gridinsp.FindEditFormTemplateControl("txtFileMonthFrom") as ASPxTextBox;
                    ASPxTextBox txtDay = Gridinsp.FindEditFormTemplateControl("txtFileDateFrom") as ASPxTextBox;

                    if (txtYear != null && txtMonth != null && txtDay != null)
                    {
                        txtMonth.Text = Convert.ToString(EthiopicDateTime.GetEthiopicMonth(day, month, year));
                        txtDay.Text = Convert.ToString(EthiopicDateTime.GetEthiopicDay(day, month, year));
                        txtYear.Text = Convert.ToString(EthiopicDateTime.GetEthiopicYear(day, month, year));
                    }
                }
            }
        }
        catch
        {
        }
    }

    protected void LoadInspectionCheckList()
    {
        try
        {
            ChklstApprove.Items.Clear();
            ChklstApprove.DataSource = null;
            InspectionBussiness objinspectionbusiness = new InspectionBussiness();
            tblDocumentBussiness objDoc = new tblDocumentBussiness();
            ChklstApprove.ValueField = "code";
            ChklstApprove.TextField = "DescriptionAm";
            DataTable objDocGetRecordCheckList = objDoc.GetRecordCheckList(Session["DocumentType"].ToString(), Session["emp_status"].ToString(), Convert.ToInt32(Session["Language"].ToString())); ;
            ChklstApprove.DataSource = objDocGetRecordCheckList;
            ChklstApprove.DataBind();
        }
        catch (Exception ex)
        {

        }
    }

    protected void LoadInspectionCheckList(ArrayList selectedChecklistItems)
    {
        try
        {
            InspectionBussiness objinspectionbusiness = new InspectionBussiness();
            tblDocumentBussiness objDoc = new tblDocumentBussiness();
            ChklstApprove.ValueField = "code";
            ChklstApprove.TextField = "DescriptionAm";
            DataTable objDocGetRecordCheckList = objDoc.GetRecordCheckList(Session["DocumentType"].ToString(), Session["emp_status"].ToString(), Convert.ToInt32(Session["Language"].ToString())); ;
            ChklstApprove.DataSource = objDocGetRecordCheckList;
            ChklstApprove.DataBind();


            if (ChklstApprove.Items.Count > 0)
            {
                for (int i = 0; i < ChklstApprove.Items.Count; i++)
                {
                    for (int j = 0; j < selectedChecklistItems.Count; j++)
                    {
                        if (ChklstApprove.Items[i].Value == selectedChecklistItems[j])
                        {
                            ChklstApprove.Items[i].Selected = true;
                        }
                    }
                }

            }
        }
        catch (Exception ex)
        {

        }
    }

    protected bool funsave(ArrayList selectedInspectionChecklList)
    {
        tblInspection objInspection = new tblInspection();
        InspectionBussiness objInspectionBusiness = new InspectionBussiness();
        try
        {
            objInspection.EmployeeGUID = new Guid(Session["APersonGuid"].ToString());
            objInspection.EventDatetime = DateTime.Today;
            if (Session["Fullname"] != null)
                objInspection.UserName = Convert.ToString(Session["ProfileFullname"].ToString());
            else
                objInspection.UserName = "";
            objInspection.InspectionDate = cdpInspectionDate.SelectedGCDate;
            objInspection.InspectionFinding = Memo.Html; //finding;  
            objInspection.InspectionDecision = ComboDecision.Value.ToString(); //decision;
            objInspection.inspectiontype = Convert.ToInt32(cboInspectionType.Value); //decision;
            objInspection.Activitytype = Convert.ToInt16(Session["DocumentType"].ToString());
            objInspection.ComplaintGUID = Guid.Empty;
            objInspection.Answer = "";
            objInspection.Subject = "";
            objInspection.approvalname = "";
            //objInspection.approvaldate = Convert.ToDateTime(ConfigurationManager.AppSettings.GetValues("FakeDate")[0]);
            objInspection.approvaldate = Convert.ToDateTime(DateTime.Now);
            objInspection.HRMServiceType = Convert.ToInt32(ComboDecision.Value);

            if ((bool)Session["IsNew"])
            {
                if (Session["ProfileFullname"] != null)
                    objInspection.UserName = Convert.ToString(Session["ProfileFullname"].ToString());
                else
                    objInspection.UserName = "";
                objInspection.EventDatetime = DateTime.Today;
                objInspection.InspectionGUID = Guid.NewGuid();
                if (objInspectionBusiness.InsertInspection(objInspection))
                {
                    setInspectionDetail(objInspection.InspectionGUID, selectedInspectionChecklList);
                    cbInspection.JSProperties["cpAction"] = "REGISTRATION";
                }

            }
            else
            {
                objInspection.UpdatedEventDatetime = DateTime.Today;
                objInspection.UpdatedUsername = (Session["ProfileFullname"] != null
                    ? Convert.ToString(Session["ProfileFullname"].ToString())
                    : "");
                objInspection.InspectionGUID = Guid.Parse(Session["InspectionGuid"].ToString());
                if (objInspectionBusiness.UpdateInspection(objInspection))
                {
                    setInspectionDetail(objInspection.InspectionGUID, selectedInspectionChecklList);
                    cbInspection.JSProperties["cpAction"] = "UPDATE";
                }
            }
            //ShowSuccess(Resources.Message.MSG_SAVED);
            CMsgBar.ShowSucess(Resources.Message.MSG_SAVED, inspectionPanle, lblInspectionLabel);
            return true;

        }
        catch (Exception ex)
        {
            ShowError(ex.Message);
            return false;
        }
    }

    private void ShowSuccess(string strMsg)
    {
        cbInspection.JSProperties["cpMessage"] = strMsg;
        cbInspection.JSProperties["cpStatus"] = "SUCCESS";
    }

    public void ShowError(string strMsg)
    {
        cbInspection.JSProperties["cpMessage"] = strMsg;
        cbInspection.JSProperties["cpStatus"] = "ERROR";
    }

    public void setInspectionDetail(Guid InspectionGUID, ArrayList selectedInspectionChecklList)
    {
        try
        {
            string filetext = "";
            bool isvalid = false;
            int Activityvalue = 0;
            List<InspectionFollowup> objInspectionFollowupList = new List<InspectionFollowup>();
            tblDocumentBussiness objDocument = new tblDocumentBussiness();
            InspectionFollowupBussiness objInspectionFollowupBussiness = new InspectionFollowupBussiness();
            for (int index = 0; index < selectedInspectionChecklList.Count; index++)
            {
                InspectionFollowup objInspectionFollowup = new InspectionFollowup();
                //do for checked value assign
                Activityvalue = int.Parse(selectedInspectionChecklList[index].ToString());
                try
                {
                    objInspectionFollowup.FollowupGUID = Guid.NewGuid();
                    objInspectionFollowup.InspectionGUID = InspectionGUID; //Attention
                    objInspectionFollowup.FollowupDate = DateTime.Today; //Attention
                    objInspectionFollowup.FollowupStatus = 2;
                    objInspectionFollowup.ActionTaken = Activityvalue;
                    objInspectionFollowup.Remark = "";
                    objInspectionFollowup.Reanswer = "";
                    objInspectionFollowup.UserName = HttpContext.Current.User.Identity.Name;
                    objInspectionFollowup.EventDatetime = DateTime.Today; //Attention
                    objInspectionFollowup.UpdatedUsername = HttpContext.Current.User.Identity.Name;
                    objInspectionFollowup.UpdatedEventDatetime = DateTime.Today; //Attention
                    objInspectionFollowup.Isvalid = isvalid; //Attention
                    objInspectionFollowup.Filename = filetext;
                    objInspectionFollowup.DocGuid = objDocument.GettblDocumentDocGuid(Convert.ToString(Activityvalue));
                    objInspectionFollowupList.Add(objInspectionFollowup);
                }
                catch (Exception ex)
                {
                    ShowError(ex.Message);

                }
            }

            if ((bool)Session["IsNew"])
            {
                for (int index = 0; index < objInspectionFollowupList.Count; index++)
                {
                    objInspectionFollowupBussiness.InsertInspectionFollowup(objInspectionFollowupList[index]);
                }
            }
            else
            {
                if (objInspectionFollowupBussiness.Delete(InspectionGUID))
                {
                    for (int index = 0; index < objInspectionFollowupList.Count; index++)
                    {
                        objInspectionFollowupBussiness.InsertInspectionFollowup(objInspectionFollowupList[index]);
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }

    }

    protected void DvgvInspection_BeforePerformDataSelect(object sender, EventArgs e)
    {
        if (Session["DocumentType"].ToString() == "-1")
        {
            //CMsgBar.ShowError(Resources.Message.MSG_SELECTHRACTIVITY, inspectionPanle, lblInspectionLabel);
        }
        else
        {
            if (Session["j"] != null)
            {
                if (!Session["DocumentType"].ToString().Equals(""))
                {
                    if (Session["DocumentType"].Equals(Convert.ToInt32(serviceType.recruitment).ToString()))
                    {
                        Session["parent"] = Convert.ToInt32(serviceType.recruitment).ToString();
                        Session["Activitytype"] = Convert.ToInt32(serviceType.recruitment).ToString();
                    }
                    else if (Session["DocumentType"].Equals(Convert.ToInt32(serviceType.promotion).ToString()))
                    {
                        Session["parent"] = Convert.ToInt32(serviceType.promotion).ToString();
                        Session["Activitytype"] = Convert.ToInt32(serviceType.promotion).ToString();
                    }
                    else if (Session["DocumentType"].Equals(Convert.ToInt32(serviceType.Transfer).ToString()))
                    {
                        Session["parent"] = Convert.ToInt32(serviceType.Transfer).ToString();
                        Session["Activitytype"] = Convert.ToInt32(serviceType.Transfer).ToString();
                    }
                    else if (Session["DocumentType"].Equals(Convert.ToInt32(serviceType.serviceextension).ToString()))
                    {
                        Session["parent"] = Convert.ToInt32(serviceType.serviceextension).ToString();
                        Session["Activitytype"] = Convert.ToInt32(serviceType.serviceextension).ToString();
                    }
                    else if (Session["DocumentType"].Equals(Convert.ToInt32(serviceType.servicetermination).ToString()))
                    {
                        Session["parent"] = Convert.ToInt32(serviceType.servicetermination).ToString();
                        Session["Activitytype"] = Convert.ToInt32(serviceType.servicetermination).ToString();
                    }
                    else if (Session["DocumentType"].Equals(Convert.ToInt32(serviceType.disciplinary).ToString()))
                    {
                        Session["parent"] = Convert.ToInt32(serviceType.disciplinary).ToString();
                        Session["Activitytype"] = Convert.ToInt32(serviceType.disciplinary).ToString();
                    }
                    else if (Session["DocumentType"].Equals(Convert.ToInt32(serviceType.health).ToString()))
                    {
                        Session["parent"] = Convert.ToInt32(serviceType.health).ToString();
                        Session["Activitytype"] = Convert.ToInt32(serviceType.health).ToString();
                    }
                    else if (Session["DocumentType"].Equals(Convert.ToInt32(serviceType.overtime).ToString()))
                    {
                        Session["parent"] = Convert.ToInt32(serviceType.overtime).ToString();
                        Session["Activitytype"] = Convert.ToInt32(serviceType.overtime).ToString();
                    }
                    else if (Session["DocumentType"].Equals(Convert.ToInt32(serviceType.scholarship).ToString()))
                    {
                        Session["parent"] = Convert.ToInt32(serviceType.scholarship).ToString();
                        Session["Activitytype"] = Convert.ToInt32(serviceType.scholarship).ToString();
                    }
                    else if (Session["DocumentType"].Equals(Convert.ToInt32(serviceType.Authoritydeligation).ToString()))
                    {
                        Session["parent"] = Convert.ToInt32(serviceType.Authoritydeligation).ToString();
                        Session["Activitytype"] = Convert.ToInt32(serviceType.Authoritydeligation).ToString();
                    }

                    ASPxGridView Gridinsp = (ASPxGridView)DvgvInspection;
                    if (Gridinsp != null)
                    {
                        ASPxRoundPanel panswer = Gridinsp.FindEditFormTemplateControl("pnanswer") as ASPxRoundPanel;
                        if (panswer != null)
                        {
                            panswer.Visible = false;
                        }
                    }
                }
            }
        }
    }

    protected void DvgvInspection_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "InspectionDate")
        {
            ProfileCommon p = this.Profile;
            p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
            if ((e.Value).ToString().Length != 0)
            {
                string ethiodate;
                ethiodate = EthiopicDateTime.GetEthiopicDate(((DateTime)(e.Value)).Day, ((DateTime)(e.Value)).Month, ((DateTime)(e.Value)).Year);
                e.DisplayText = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate,p.Organization.LanguageID);
            }
        }
        if (e.Column.FieldName == "InspectionDecision")
        {
            ProfileCommon objProfile = this.Profile;
            objProfile = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
            if (objProfile.Organization.LanguageID == 1)
            {
                e.DisplayText = EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionDecisionType), Language.eLanguage.eAmharic, Convert.ToInt32(e.Value));
            }
            if (objProfile.Organization.LanguageID == 2)
            {
                e.DisplayText = EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionDecisionType), Language.eLanguage.eAfanOromo, Convert.ToInt32(e.Value));
            }
        }
    }

    protected void DvgvInspection_SelectionChanged(object sender, EventArgs e)
    {

    }

    protected bool DoFindAttachment(Guid MainGuid)
    {
        try
        {
            //Attachment objAttachment = new Attachment();
            //AttachmentBussiness objAttachmentBusiness = new AttachmentBussiness();
            //objAttachment = objAttachmentBusiness.GetAttachment(MainGuid);
            //string url = objAttachment.Url;
            //string NavigateUrl = @"C:\inetpub\wwwroot\hr\Attachment\" + url; //this is Acctual path where the HR Attachment is exist;
            //FileInfo fi = new FileInfo(NavigateUrl);
            //System.Diagnostics.Process.Start(NavigateUrl);
            //long sz = fi.Length;
            //FileStream MyFileStream = new FileStream(NavigateUrl, FileMode.Open);
            //sz = MyFileStream.Length;
            //byte[] Buffer = new byte[(int)sz];
            //MyFileStream.Read(Buffer, 0, (int)MyFileStream.Length);
            //MyFileStream.Close();
            //Response.ContentType = MimeType(Path.GetExtension(NavigateUrl));
            //Response.AddHeader("Content-Disposition", string.Format("attachment; filename = {0}", System.IO.Path.GetFileName(NavigateUrl)));
            //Response.AddHeader("Content-Length", sz.ToString("F0"));
            //Response.BinaryWrite(Buffer);
            //               // string fName = NavigateUrl;
            //FileInfo fi = new FileInfo(fName);
            //long sz = fi.Length;
            //string filePath = Path.Combine(fName);                
            //Response.ClearContent();
            //Response.ContentType = MimeType(Path.GetExtension(fName));
            //Response.AddHeader("Content-Disposition", string.Format("attachment; filename = {0}", System.IO.Path.GetFileName(fName)));
            //Response.AddHeader("Content-Length", sz.ToString("F0"));
            //Response.TransmitFile(fName);
            //Response.Flush();
            //Response.SuppressContent = true;
            //FileStream fileStream = new FileStream(NavigateUrl, FileMode.Open, FileAccess.Read);

            //Response.Ap = true;
            //Response.End();
        }
        catch
        {

        }

        return true;
    }

    protected void btnViewClick(Guid MainGuid)
    {
        if (MainGuid != null)
        {
            DoFindAttachment(MainGuid);
            //Secure();
            //DvgvwPerson.DetailRows.CollapseAllRows();
        }
    }

    protected void DvgvInspection_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        string strID = string.Empty;

        string[] parameters = e.Parameters.Split('|');
        string strParam = parameters[0].ToString();
        if (e.Parameters.ToString().Contains('|'))
            strID = parameters[1].ToString();

        switch (strParam)
        {
            case "btnsave":
                //btnsaveClick();
                break;
            case "Appregistration":
                //btnAppregistrationClick();
                break;
            case "Filter":
                btnViewClick(Guid.Parse(strID));
                break;
        }
    }

    protected void DvgvInspection_Load(object sender, EventArgs e)
    {

    }

    protected void DvgvInspection_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;
        //bool approved = Convert.ToBoolean(e.GetValue("Approved"));
        //if (approved)
        //{
        //    if (e.Row.Cells.Count > 1)
        //    {
        //        e.Row.Cells[6].Enabled = false;
        //        e.Row.Cells[6].Text = "አሳይ";
        //        e.Row.Cells[7].Enabled = false;
        //    }
        //}
    }

    protected void DvgvInspection_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        Session["IsNew"] = true;
    }

    protected void DvgvInspection_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {


    }

    private bool DoDeleteRecord(Guid ID)
    {
        InspectionBussiness objinspection = new InspectionBussiness();
        try
        {
            bool bDone = objinspection.Delete(ID);
            #region Delete Inspection
            if (bDone == true)
            {
                bool deleted = false;

                InspectionFollowupBussiness inspectiondetail = new InspectionFollowupBussiness();

                if (inspectiondetail.Delete(ID))
                {
                    deleted = true;
                }
            #endregion


                if (bDone && deleted)
                {
                    //MessageBox1.ShowSucess(Resources.Message.MSG_DELETED);
                    CMsgBar.ShowSucess(Resources.Message.MSG_DELETED, inspectionPanle, lblInspectionLabel);
                    Session["DeleteCode"] = string.Empty;

                    return true;
                }
                return false;
            }
            return false;
        }
        catch (Exception ex)
        {
            //MessageBox1.ShowError(ex.Message);
            CMsgBar.ShowError(ex.Message, inspectionPanle, lblInspectionLabel);
            return false;
        }
    }

    protected void btndelete_Click(object sender, EventArgs e)
    {
        try
        {
            DoDeleteRecord(Guid.Parse(Session["inspguid"].ToString()));
        }
        catch
        {
        }
    }

    protected void DoNew()
    {
        cdpInspectionDate.SelectedDate = EthiopicDateTime.GetEthiopicDate(DateTime.Today.Day, DateTime.Today.Month, DateTime.Today.Year);
        pnlInspectionList.Visible = false;
        //pnlInspectionDetail.Visible = true;
        LoadInspectionTypes();
        LoadInspectionDecisions();
        LoadInspectionCheckList();
        Memo.Html = null;
        Session["InspectionModeNew"] = true;
        Session["IsNew"] = true;
        cbInspection.JSProperties["cpAction"] = "newInspection";
        cbInspection.JSProperties["cpStatus"] = "success";
    }

    protected void lbtnNew_Click(object sender, EventArgs e)
    {
        DoNew();
    }

    protected void lbtnSave_Click(object sender, EventArgs e, ArrayList selectedCheckList)
    {
        if (IsValidInspectionRecord(selectedCheckList))
        {

            if (funsave(selectedCheckList))
            {
                LoadInspection();
            }
        }
    }

    protected void cbInspection_OnCallbackPanel(object sender, CallbackEventArgsBase e)
    {
        ArrayList selectedChecklistItems = new ArrayList();
        string[] parameters = e.Parameter.Split('|');
        string strParam = parameters[0].ToString();
        switch (strParam)
        {
            case "New":
                lbtnNew_Click(sender, e);
                break;
            case "Save":
                //-------------------------2016-----------------
                if (parameters.Length >= 2)
                {
                    if (e.Parameter.ToString().Contains('|'))
                    {
                        for (int index = 1; index < parameters.Length; index++)
                        {
                            selectedChecklistItems.Add(parameters[index]);
                        }
                    }
                }
                lbtnSave_Click(sender, e, selectedChecklistItems);
                break;
            case "Approve":
                btnApproved_Click1(sender, e);
                break;
            case "Search":
                LoadInspection();
                break;
            case "Edit":
                ProfileCommon p = this.Profile;
                p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
                tblTeamMembersBussiness objTeamMembersBusiness = new tblTeamMembersBussiness();
                if (objTeamMembersBusiness.DoesEmployeeTeamLeader(Guid.Parse(p.Staff.GUID.ToString()), Guid.Parse(Session["OrgGuid"].ToString()))
                    || HttpContext.Current.User.IsInRole("Inspection Director") || HttpContext.Current.User.IsInRole("Inspection Supervisor"))
                {
                    btnApproved.Visible = true;
                }
                LoadInspectionDetail(Guid.Parse(parameters[1].ToString()));
                break;
            case "Delete":
                DeleteInspectionDetail(Guid.Parse(parameters[1].ToString()));
                break;
        }
    }

    protected void DeleteInspectionDetail(Guid InsepctionGuid)
    {
        InspectionBussiness inspection = new InspectionBussiness();
        //Check whether there inspection folloups under the selected inspection
        InspectionFollowupBussiness inspectionFollowup = new InspectionFollowupBussiness();
        List<InspectionFollowup> objinspectionFollowup = inspectionFollowup.GetInspectionFollow(InsepctionGuid);
        if (inspection.Delete(InsepctionGuid))
        {
            if (objinspectionFollowup.Count > 0)
            {
                inspectionFollowup.Delete(InsepctionGuid);
            }
            
            ShowSuccess(Resources.Message.MSG_DELETED);
            CMsgBar.ShowSucess(Resources.Message.MSG_DELETED, inspectionPanle, lblInspectionLabel);
            cbInspection.JSProperties["cpSuccess"] = "successful";
            cbInspection.JSProperties["cpAction"] = "Deletion";
        }
    }

    protected void LoadInspectionDetail(Guid InsepctionGuid)
    {
        ArrayList checkedInspection = new ArrayList();
        LoadInspectionTypes();
        LoadInspectionDecisions();
        LoadInspectionCheckList();
        InspectionBussiness inspection = new InspectionBussiness();
        tblInspection objInspection = inspection.GetInspection(InsepctionGuid);
        if (objInspection != null)
        {
            int inspectionType = objInspection.inspectiontype;
            if (Session["Language"].ToString() == "0")
            {
                cboInspectionType.Text = EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionType), Language.eLanguage.eEnglish,
                                                                      inspectionType);
            }
            else if (Session["Language"].ToString() == "1")
            {
                cboInspectionType.Text = EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionType), Language.eLanguage.eAmharic,
                                                                    inspectionType);
            }
            else if (Session["Language"].ToString() == "2")
            {
                cboInspectionType.Text = EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionType), Language.eLanguage.eAfanOromo,
                                                                    inspectionType);
            }
            ComboDecision.Value = objInspection.InspectionDecision;
            cdpInspectionDate.SelectedDate = EthiopicDateTime.GetEthiopicDate(int.Parse(objInspection.InspectionDate.Day.ToString()), int.Parse(objInspection.InspectionDate.Month.ToString()),
                                                           int.Parse(objInspection.InspectionDate.Year.ToString())).ToString();
            Memo.Html = objInspection.InspectionFinding;
            InspectionFollowupBussiness inspectionFollowup = new InspectionFollowupBussiness();
            List<InspectionFollowup> objinspectionFollowup = inspectionFollowup.GetInspectionFollow(InsepctionGuid);
            tblDocumentBussiness objDoc = new tblDocumentBussiness();
            DataTable objDocGetRecordCheckList = objDoc.GetRecordCheckList(Session["DocumentType"].ToString(), Session["emp_status"].ToString(), Convert.ToInt32(Session["Language"].ToString())); ;
            for (int index = 0; index < objinspectionFollowup.Count; index++)
            {
                for (int inspectionindex = 0; inspectionindex < objDocGetRecordCheckList.Rows.Count; inspectionindex++)
                {
                    if (objinspectionFollowup[index].ActionTaken.ToString() == objDocGetRecordCheckList.Rows[inspectionindex]["Code"].ToString())
                    {
                        checkedInspection.Add(objinspectionFollowup[index].ActionTaken.ToString());
                    }
                }
            }
            //pnlInspectionDetail.Visible = true;
            //-----------------------2016
            cbInspection.JSProperties["cpcheckedInspection"] = checkedInspection;
            cbInspection.JSProperties["cpAction"] = "LoadInspectionDetail";
            //---------------------------
            Session["IsNew"] = false;
            Session["InspectionGuid"] = InsepctionGuid;

            if (objInspection.Approved)
            {
                pnlapproval.Visible = true;
                lblapprovalname.Text = objInspection.approvalname;
                lblupdatename.Text = objInspection.approvalname;
                string ethday = EthiopicDateTime.GetEthiopicDay(int.Parse(objInspection.approvaldate.Day.ToString()), int.Parse(objInspection.approvaldate.Month.ToString()),
                                                           int.Parse(objInspection.approvaldate.Year.ToString())).ToString();
                string ethmonth = EthiopicDateTime.GetEthiopicMonth(int.Parse(objInspection.approvaldate.Day.ToString()), int.Parse(objInspection.approvaldate.Month.ToString()),
                                                               int.Parse(objInspection.approvaldate.Year.ToString())).ToString();
                string ethyear = EthiopicDateTime.GetEthiopicYear(int.Parse(objInspection.approvaldate.Day.ToString()), int.Parse(objInspection.approvaldate.Month.ToString()),
                                                               int.Parse(objInspection.approvaldate.Year.ToString())).ToString();

                lblprepareddate.Text = ethday + "/" + ethmonth + "/" + ethyear;
                lblapproveddate.Text = ethday + "/" + ethmonth + "/" + ethyear;
            }
            if (objInspection.Answer != "")
            {
                pnlResponse.Visible = true;
                lblanswer.Text = objInspection.Answer;
            }

            if (objInspection.Approved)
            {
                btnApproved.Visible = false;
                btnNew.Visible = false;
            }
        }
    }

    protected void LoadInspection()
    {
        Guid person = Guid.Parse(Session["APersonGuid"].ToString());
        InspectionBussiness inspection = new InspectionBussiness();
        DataTable dtInspection = inspection.GetRecords(person, Session["DocumentType"].ToString());
        if (dtInspection.Rows.Count > 0)
        {
            DvgvInspection.JSProperties["cpStatus"] = "success";
            DvgvInspection.JSProperties["cpAction"] = "loadedInspectionList";
            pnlInspectionList.Visible = true;
            DvgvInspection.DataSource = dtInspection;
            DvgvInspection.DataBind();
            int count = 0;
            for (int index = 0; index < dtInspection.Rows.Count; index++)
            {
                if (DBNull.Value != dtInspection.Rows[index]["Approved"])
                {
                    if (Convert.ToBoolean(dtInspection.Rows[index]["Approved"]))
                    {
                        count++;
                        break;
                    }
                }
            }
            if(count > 1)
            {
                DvgvInspection.JSProperties["cpHrActivityApproved"] = "yes";
            }
            else
            {
                DvgvInspection.JSProperties["cpHrActivityApproved"] = "no";
            }
        }
        else
        {
            DvgvInspection.JSProperties["cpStatus"] = "success";
            DvgvInspection.JSProperties["cpAction"] = "NotloadedInspectionList";
            DvgvInspection.JSProperties["cpHrActivityApproved"] = "no";
        }
    }

    protected void DvgvInspection_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        LoadInspection();
    }

    protected void DvgvInspection_PageIndexChanged(object sender, EventArgs e)
    {
        LoadInspection();
    }

    protected void DvgvInspection_CustomButtonInitialize(object sender, ASPxGridViewCustomButtonEventArgs e)
    {
        
    }

    protected void DvgvInspection_CustomButtonInitialize1(object sender, ASPxGridViewCustomButtonEventArgs e)
    {
        string[] fields = { "Approved" };
        object values = DvgvInspection.GetRowValues(e.VisibleIndex, fields) as object;
        bool isApproved = (bool)values;
        if (isApproved)
        {
            if (e.Column.Index.Equals(7))
            {
                e.Text = "አሳይ";

            }
            if (e.Column.Index.Equals(8))
            {
                e.Enabled = false;
            }
        }
    }
}