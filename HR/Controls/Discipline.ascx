﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Discipline.ascx.cs" Inherits="Controls_Discipline" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>


<dx:ASPxGridView ID="Dvgvdeciplin" ClientInstanceName="Dvgvdeciplin" OnCustomCallback="Dvgvdeciplin_OnCustomCallback" runat="server" AutoGenerateColumns="False" EnableTheming="True"
    Width="100%" OnBeforePerformDataSelect="Dvgvdeciplin_BeforePerformDataSelect" 
    OnCustomColumnDisplayText="Dvgvdeciplin_CustomColumnDisplayText" Theme="Office2010Silver" 
    meta:resourcekey="DvgvdeciplinResource1">
    <Columns>
        <dx:GridViewDataTextColumn Caption="እርምጃ የተወስደበት ቀን" FieldName="date_action_taken"
            ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource82">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="የጥፋቱ አይነት" FieldName="break_type" ShowInCustomizationForm="True"
            VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource83">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="የጥፋቱ ዝርዝር" FieldName="break_description" ShowInCustomizationForm="True"
            VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource84">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="የተወስደው እርምጃ" FieldName="action_taken" ShowInCustomizationForm="True"
            VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnResource85">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="ማስታወሻ" FieldName="remark" ShowInCustomizationForm="True"
            VisibleIndex="4" Visible="False" meta:resourcekey="GridViewDataTextColumnResource86">
        </dx:GridViewDataTextColumn>
    </Columns>
    <Templates>
        <EmptyDataRow>
            &nbsp;መረጃ አልተገኘም
        </EmptyDataRow>
    </Templates>
</dx:ASPxGridView>
