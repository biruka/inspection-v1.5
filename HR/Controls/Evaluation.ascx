﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Evaluation.ascx.cs" Inherits="Controls_Evaluation" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>

<div>
    <dx:ASPxGridView ID="DvgEvaluation" runat="server" ClientInstanceName="DvgEvaluation" OnCustomCallback="DvgEvaluation_OnCustomCallback" AutoGenerateColumns="False" OnBeforePerformDataSelect="DvgEvaluation_BeforePerformDataSelect"
        OnCustomColumnDisplayText="DvgEvaluation_CustomColumnDisplayText" OnHtmlRowPrepared="DvgEvaluation_HtmlRowPrepared" Width="100%" Theme="Office2010Silver" meta:resourcekey="DvgEvaluationResource1">
        <Columns>
            <dx:GridViewDataTextColumn Caption="ቀን " FieldName="strFiscal_year" ShowInCustomizationForm="True" VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnResource126">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ፔሬድ" FieldName="Periods" ShowInCustomizationForm="True" VisibleIndex="6" meta:resourcekey="GridViewDataTextColumnResource127">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ጠንካራ ጎን" FieldName="Strong_side" ShowInCustomizationForm="True" VisibleIndex="11" meta:resourcekey="GridViewDataTextColumnResource128">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ደካማ ጎን" FieldName="Weak_side" ShowInCustomizationForm="True" VisibleIndex="12" meta:resourcekey="GridViewDataTextColumnResource129">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="የአፈፃፀም ውጤት" FieldName="Eval_point" ShowInCustomizationForm="True" VisibleIndex="14"
                  meta:resourcekey="GridViewDataTextColumnResource130">
                <CellStyle HorizontalAlign="Center">
                </CellStyle>
                <FooterTemplate>
                    <dx:ASPxLabel ID="lblaverage" runat="server" Width="100%" meta:resourcekey="lblaverageResource1">
                    </dx:ASPxLabel>
                </FooterTemplate>
            </dx:GridViewDataTextColumn>
        </Columns>
        <Settings ShowFooter="True" />
        <Templates>
            <EmptyDataRow>
                &nbsp;መረጃ አልተገኘም
            </EmptyDataRow>
        </Templates>
    </dx:ASPxGridView>
 <asp:HiddenField ID="HidlLanguage" runat="server" />
</div>
