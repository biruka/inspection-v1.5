﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Bussiness;
using DevExpress.Web;
using System.Data;
using CUSTOR;
using CUSTOR.Commen;

public partial class Controls_Delegation : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void DVgvdeligation_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["person"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }

    protected void LoadDelegations()
    {
        Guid person = Guid.Parse(Session["APersonGuid"].ToString());
        SpecialAssignmentBusiness delegation = new SpecialAssignmentBusiness();
        DataTable dtdelegation = delegation.GetRecordByPerson(person);
        DVgvdeligation.DataSource = dtdelegation;
        DVgvdeligation.DataBind();
    }

    protected void DVgvdeligation_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        LoadDelegations();
    }

    protected void DVgvdeligation_OnCustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "Benifit")
        {
            if (Session["OrgLanguage"].ToString() == "Amharic")
            {
                switch (e.Value.ToString())
                {
                    case "True":
                        e.DisplayText = "ያካትታል";
                        break;
                    case "False":
                        e.DisplayText = "አያካትትም";
                        break;
                }
            }
        }
        if (e.Column.FieldName == "from_date")
        {
            e.DisplayText = EthiopicDateTime.GetEthiopicDate(((DateTime)(e.Value)).Day, ((DateTime)(e.Value)).Month, ((DateTime)(e.Value)).Year);
        }
        if (e.Column.FieldName == "to_date")
        {
            e.DisplayText =  EthiopicDateTime.GetEthiopicDate(((DateTime)(e.Value)).Day, ((DateTime)(e.Value)).Month, ((DateTime)(e.Value)).Year);
        }
    }
}