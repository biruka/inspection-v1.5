﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Bussiness;
using CUSTOR.Commen;
using DevExpress.Web;

public partial class Controls_EducationalBackground : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void DvgvCandidate_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        if (e.Column.FieldName == "period_from")
            if ((e.Value).ToString().Length != 0)
            {
                string ethiodate;
                ethiodate = EthiopicDateTime.GetEthiopicDate(((DateTime)(e.Value)).Day, ((DateTime)(e.Value)).Month, ((DateTime)(e.Value)).Year);
                e.DisplayText = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate,p.Organization.LanguageID);
            }
        if (e.Column.FieldName == "period_to")
            if ((e.Value).ToString().Length != 0)
            {
                string ethiodate;
                ethiodate = EthiopicDateTime.GetEthiopicDate(((DateTime)(e.Value)).Day, ((DateTime)(e.Value)).Month, ((DateTime)(e.Value)).Year);
                e.DisplayText = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate, p.Organization.LanguageID);
            }
            else
            {
                { return; }
            }
    }

    protected void DvgvCandidate_BeforePerformDataSelect(object sender, EventArgs e)
    {
         Session["person"] = (sender as ASPxGridView).GetMasterRowKeyValue();

    }

    protected void LoadEducationalBackground()
    {
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        Guid person = Guid.Parse(Session["APersonGuid"].ToString());
        int documentType = Convert.ToInt32(Session["DocumentType"].ToString());
        tblEducationHistoryBussiness educationalBackground = new tblEducationHistoryBussiness();
        DvgvCandidate.DataSource = educationalBackground.GetRecords(person,p.Organization.LanguageID.ToString());
        DvgvCandidate.DataBind();
    }

    protected void DvgvCandidate_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (Convert.ToBoolean(Session["LoadCriterias"]) == true)
        {
            LoadEducationalBackground();
        } 
    }
}