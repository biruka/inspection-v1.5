﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Medical.ascx.cs" Inherits="Controls_Medical" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>

<dx:ASPxGridView ID="Dvgvhealth" runat="server" AutoGenerateColumns="False" ClientInstanceName="Dvgvhealth"
    EnableTheming="True" OnBeforePerformDataSelect="ASPxGridView5_BeforePerformDataSelect" OnCustomCallback="Dvgvhealth_OnCustomCallback"
    Width="100%" OnCustomColumnDisplayText="ASPxGridView5_CustomColumnDisplayText" Theme="Office2010Silver"
     meta:resourcekey="DvgvhealthResource1">
    <Columns>
        <dx:GridViewDataTextColumn FieldName="patient_type" ShowInCustomizationForm="True"
            VisibleIndex="0" Caption="የህመሙ አይነት" meta:resourcekey="GridViewDataTextColumnResource93">
        </dx:GridViewDataTextColumn>  
        <dx:GridViewDataTextColumn FieldName="remark" ShowInCustomizationForm="True" VisibleIndex="7"
            Caption="ማስታወሻ" meta:resourcekey="GridViewDataTextColumnResource94">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="diagnostic_date" ShowInCustomizationForm="True"
            VisibleIndex="1" Caption="የምርመራ ቀን" meta:resourcekey="GridViewDataTextColumnResource87">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="treatment_given" ShowInCustomizationForm="True"
            VisibleIndex="2" Caption="የህክምና ክትትል" meta:resourcekey="GridViewDataTextColumnResource88">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="xray_expense" ShowInCustomizationForm="True"
            VisibleIndex="5" Visible="False" meta:resourcekey="GridViewDataTextColumnResource91">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="medicine_expense" ShowInCustomizationForm="True"
            VisibleIndex="3" Caption="የህክምና ወጪ" meta:resourcekey="GridViewDataTextColumnResource89">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="laboratory_expense" ShowInCustomizationForm="True"
            VisibleIndex="4" Caption="የላብራቶሪ ወጪ" meta:resourcekey="GridViewDataTextColumnResource90">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="other_expense" ShowInCustomizationForm="True"
            VisibleIndex="6" Caption="ሌላ ወጪ" meta:resourcekey="GridViewDataTextColumnResource92">
        </dx:GridViewDataTextColumn>
    </Columns>
    <Templates>
        <EmptyDataRow>
            &nbsp;መረጃ አልተገኘም
        </EmptyDataRow>
    </Templates>
</dx:ASPxGridView>
