﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Response.ascx.cs" Inherits="Controls_Response" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxHtmlEditor" Assembly="DevExpress.Web.ASPxHtmlEditor.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register assembly="DevExpress.Web.ASPxSpellChecker.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxSpellChecker" tagprefix="dx" %>
<script type="text/javascript">

    function Dvgvreanswer_CustomButtonClick(s, e) {
        if (e.buttonID == 'Response') {
            Dvgvreanswer.GetRowValues(e.visibleIndex, "InspectionGUID", OnGetSelectedResponseFieldValues);
        }
    }

    function OnGetSelectedResponseFieldValues(values) {
        cbResponse.PerformCallback('Response' + '|' + values);
    }

    function OnEndCallback(s, e) {

    }

</script>
<div>
   <%-- <dx:ASPxGridView runat="server" ID="test">
        <Columns>

        </Columns>
    </dx:ASPxGridView>--%>

    <dx:ASPxPanel ID="pnlMsg" runat="server" CssClass="messageWarning" EnableViewState="False"
        Height="100%" Visible="False" Width="100%" BackColor="#CCFF33" meta:resourcekey="pnlMsgResource1">
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server" meta:resourcekey="PanelContent2Resource1">
                <asp:Label ID="lblPersonMsg" runat="server" CssClass="Message" Font-Bold="True" ForeColor="Red" meta:resourcekey="lblPersonMsgResource1"></asp:Label>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxPanel>
    <dx:ASPxCallbackPanel ID="cbResponse" OnCallback="cbRespnse_OnCallbackPanel" ClientInstanceName="cbResponse" runat="server" meta:resourcekey="cbResponseResource1">
        <ClientSideEvents EndCallback="OnEndCallback" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent9" runat="server" meta:resourceKey="PanelContent2Resource1" SupportsDisabledAttribute="True">
                <asp:Panel ID="Panel1" runat="server" CssClass="messageWarning" EnableViewState="False"
                    Height="100%" Visible="False" Width="100%" BorderColor="#99CCFF" BackColor="#CCFF33" meta:resourcekey="pnlMsgResource1">
                    <asp:Label ID="Label3" runat="server" CssClass="Message" Font-Bold="True" ForeColor="Red" meta:resourcekey="lblPersonMsgResource1"></asp:Label>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlResponseList" Visible="False" meta:resourcekey="pnlResponseListResource1">
                    <div>
                        <dx:ASPxGridView ID="Dvgvreanswer" ClientInstanceName="Dvgvreanswer" runat="server" AutoGenerateColumns="False" KeyFieldName="InspectionGUID" OnBeforePerformDataSelect="Dvgvreanswer_OnBeforePerformDataSelect"
                            OnCustomColumnDisplayText="Dvgvreanswer_OnCustomColumnDisplayText" OnPageIndexChanged="Dvgvreanswer_PageIndexChanged" Width="100%" OnHtmlRowPrepared="Dvgvreanswer_OnHtmlRowPrepared" Theme="Moderno" meta:resourcekey="DvgvreanswerResource1">
                            <SettingsBehavior AllowSelectByRowClick="True" />
                            <ClientSideEvents CustomButtonClick="Dvgvreanswer_CustomButtonClick" />
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="የውሳኔ አይነት" FieldName="InspectionDecision" ShowInCustomizationForm="True"
                                    VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource124">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="የግምገማ ውሳኔ የተሰጠበት ቀን" ShowInCustomizationForm="True"
                                    VisibleIndex="1" FieldName="AnswerDate" meta:resourcekey="GridViewDataTextColumnResource125">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="3" Width="100px">
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="GridViewCommandColumnCustomButton1"
                                            Text="አሳይ" meta:resourcekey="GridViewCommandColumnCustomButtonResource1">
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                </dx:GridViewCommandColumn>
                                <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="4" Width="100px">
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="Response" Text="ምላሽ" meta:resourcekey="GridViewCommandColumnCustomButtonResource2">
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                </dx:GridViewCommandColumn>
                            </Columns>
                        </dx:ASPxGridView>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlResponseDetail" Visible="False" meta:resourcekey="pnlResponseDetailResource1">
                    <table width="100%">
                        <tr>
                            <td align="left" class="dxflInternalEditorTable_Moderno">
                                <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" HeaderText="ውሳኔ ቃለ ጉባኤ" Width="100%" meta:resourcekey="ASPxRoundPanel1Resource1">
                                    <PanelCollection>
                                        <dx:PanelContent ID="PanelContent12" runat="server" SupportsDisabledAttribute="True" meta:resourcekey="PanelContent4Resource1">
                                            <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text='<%# Eval("InspectionFinding") %>' Width="100%" meta:resourcekey="ASPxLabel3Resource1">
                                            </dx:ASPxLabel>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                </dx:ASPxRoundPanel>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td width="100%">
                                <table width="50%">
                                    <tr>
                                        <td align="center">
                                            <dx:ASPxButton ID="btnSaveResponse" runat="server" Text="አስቀምጥ" CausesValidation="true" Theme="DevEx" meta:resourcekey="btnupdateResource1">
                                                <Image Url="~/Images/Toolbar/save.gif">
                                                </Image>
                                                <ClientSideEvents Click="function(s, e) {  DoSaveResponseClick(); }"></ClientSideEvents>
                                            </dx:ASPxButton>
                                            <dx:ASPxButton ID="btnNewResponse" runat="server" Text="አዲስ" Theme="DevEx" Visible="False" meta:resourcekey="btncancelResource3">
                                                <Image Url="~/Images/Toolbar/new.gif">
                                                </Image>
                                                <ClientSideEvents Click="function(s, e) {  DoNewResponseClick(); }"></ClientSideEvents>
                                            </dx:ASPxButton>
                                        </td>
                                        <td align="center"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td align="left" valign="top" width="125px">
                                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="ጉዳዩ " Width="20px" meta:resourcekey="ASPxLabel4Resource1">
                                </dx:ASPxLabel>
                            </td>
                            <td width="500px">
                                <dx:ASPxTextBox ID="txtsubject" runat="server" Width="170px" meta:resourcekey="txtsubjectResource1">
                                    <ClientSideEvents Validation="OnResponseSubjectValidation" />
                                    <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithText">
                                        <RequiredField IsRequired="True" ErrorText="እባክዎን ጉዳዮን ያስገቡ" />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" class="style10">
                                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="መልስ " Width="20px" meta:resourcekey="ASPxLabel2Resource1">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style10">
                                <dx:ASPxHtmlEditor ID="answerMemo" runat="server" meta:resourcekey="answerMemoResource1">
                                    <SettingsImageUpload UploadImageFolder="~/UploadImages/">
                                        <ValidationSettings AllowedFileExtensions=".jpe,.jpeg,.jpg,.gif,.png" MaxFileSize="500000">
                                        </ValidationSettings>
                                    </SettingsImageUpload>
                                </dx:ASPxHtmlEditor>
                            </td>
                        </tr>
                        <tr>
                            <td class="style10">
                                <asp:HiddenField ID="HiddenField11" runat="server" Value='<%# Eval("InspectionGUID") %>' />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</div>
