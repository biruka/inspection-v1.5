﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Delegation.ascx.cs" Inherits="Controls_Delegation" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>

<div>
    <dx:ASPxGridView ID="DVgvdeligation" runat="server" ClientInstanceName="delegation" OnCustomCallback="DVgvdeligation_OnCustomCallback" OnCustomColumnDisplayText="DVgvdeligation_OnCustomColumnDisplayText" AutoGenerateColumns="False"
        EnableTheming="True" OnBeforePerformDataSelect="DVgvdeligation_BeforePerformDataSelect"
        Width="100%" Theme="Office2010Silver" meta:resourcekey="DVgvdeligationResource1">
        <Columns>
            <dx:GridViewDataTextColumn FieldName="work_performed_str" ShowInCustomizationForm="True"
                VisibleIndex="1" Caption="ስራው" meta:resourcekey="GridViewDataTextColumnResource109">
            </dx:GridViewDataTextColumn>
           <%-- <dx:GridViewDataTextColumn FieldName="is_reference" ShowInCustomizationForm="True"
                VisibleIndex="2" Caption="የወከለው" meta:resourcekey="GridViewDataTextColumnResource110">
            </dx:GridViewDataTextColumn>--%>
            <dx:GridViewDataTextColumn FieldName="Benifit" ShowInCustomizationForm="True"
                VisibleIndex="3" Caption="ጥቅማ ጥቅም" meta:resourcekey="GridViewDataTextColumnResource111">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="from_date" ShowInCustomizationForm="True"
                VisibleIndex="0" Caption="ውክልና የተሰጠበት ቀን" meta:resourcekey="GridViewDataTextColumnResource112">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="from_date" ShowInCustomizationForm="True" VisibleIndex="4"
                Caption="የሚጀምርበት ቀን" meta:resourcekey="GridViewDataTextColumnResource113">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="to_date" ShowInCustomizationForm="True" VisibleIndex="5"
                Caption="የመጨርስበት ቀን" meta:resourcekey="GridViewDataTextColumnResource114">
            </dx:GridViewDataTextColumn>
        </Columns>
        <Templates>
            <EmptyDataRow>
                &nbsp;መረጃ አልተገኘም
            </EmptyDataRow>
        </Templates>
    </dx:ASPxGridView>
</div>
