﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OverTime.ascx.cs" Inherits="Controls_OverTime" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>

<dx:ASPxGridView ID="Dvgvovertime" runat="server" AutoGenerateColumns="False" ClientInstanceName="Dvgvovertime" OnCustomCallback="Dvgvovertime_OnCustomCallback"
    EnableTheming="True" OnBeforePerformDataSelect="Dvgvovertime_OnBeforePerformDataSelect"
    Width="100%" Theme="Office2010Silver" meta:resourcekey="DvgvovertimeResource1">
    <Columns>
        <dx:GridViewDataTextColumn Caption="ምክንያት" FieldName="overtime_reason" ShowInCustomizationForm="True"
            VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource95">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="ዓ.ም" FieldName="overtime_year" ShowInCustomizationForm="True"
            VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource96">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="አይነት" FieldName="overtime_type" ShowInCustomizationForm="True"
            VisibleIndex="1" Visible="False" meta:resourcekey="GridViewDataTextColumnResource97">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="ወር" FieldName="overtime_month" ShowInCustomizationForm="True"
            VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnResource98">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="xray_expense" ShowInCustomizationForm="True"
            VisibleIndex="5" Visible="False" meta:resourcekey="GridViewDataTextColumnResource91">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="ሰዓት" FieldName="worked_hours" ShowInCustomizationForm="True"
            VisibleIndex="4" meta:resourcekey="GridViewDataTextColumnResource99">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="የገንዘብ መጠን" FieldName="overtime_amount" ShowInCustomizationForm="True"
            VisibleIndex="5" meta:resourcekey="GridViewDataTextColumnResource100">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="remark" ShowInCustomizationForm="True" VisibleIndex="7"
            Caption="ማስታወሻ" meta:resourcekey="GridViewDataTextColumnResource94">
        </dx:GridViewDataTextColumn>
    </Columns>
    <Templates>
        <EmptyDataRow>
            &nbsp;መረጃ አልተገኘም
        </EmptyDataRow>
    </Templates>
</dx:ASPxGridView>
