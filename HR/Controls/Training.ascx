﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Training.ascx.cs" Inherits="Controls_Training" %>
<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>

<div>
    <dx:ASPxGridView ID="DvgvExamresult" ClientInstanceName="Training" OnCustomCallback="DvgvExamresult_OnCustomCallback" runat="server" AutoGenerateColumns="False"
        Width="100%" OnBeforePerformDataSelect="DvgvExamresult_BeforePerformDataSelect"
        OnCustomColumnDisplayText="DvgvExamresult_CustomColumnDisplayText" Theme="Office2010Silver" meta:resourcekey="DvgvExamresultResource1">
        <Columns>
            <dx:GridViewDataTextColumn Caption="ተቋም" FieldName="institution" ShowInCustomizationForm="True"
                VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource22">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="የተከታተሉት ትምህርት" FieldName="course_name" ShowInCustomizationForm="True"
                VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource23">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ስልጠና የወሰደው ጊዜ" ShowInCustomizationForm="True"
                VisibleIndex="8" FieldName="duration" meta:resourcekey="GridViewDataTextColumnResource24">
            </dx:GridViewDataTextColumn>
            <dx:GridViewBandColumn Caption="ስልጠና" ShowInCustomizationForm="True" VisibleIndex="2" meta:resourcekey="GridViewBandColumnResource4">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="የስልጠናው ዓይነት" FieldName="training_nature" ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource25">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="ስልጠና የተሰጠበት ሁኔታ" FieldName="training_type" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource26">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="የስልጠናው ውጤት" FieldName="points_scored" ShowInCustomizationForm="True" VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource27">
                    </dx:GridViewDataTextColumn>
                </Columns>
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewBandColumn>
            <dx:GridViewBandColumn Caption="ቀን" ShowInCustomizationForm="True" VisibleIndex="4" meta:resourcekey="GridViewBandColumnResource5">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="ከ" FieldName="period_from" ShowInCustomizationForm="True" VisibleIndex="0" meta:resourcekey="GridViewDataTextColumnResource28">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="እስከ" FieldName="period_to" ShowInCustomizationForm="True" VisibleIndex="1" meta:resourcekey="GridViewDataTextColumnResource29">
                    </dx:GridViewDataTextColumn>
                </Columns>
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewBandColumn>
        </Columns>
        <Templates>
            <EmptyDataRow>
                &nbsp;መረጃ አልተገኘም
            </EmptyDataRow>
        </Templates>
    </dx:ASPxGridView>
</div>
