﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Bussiness;
using CUSTOR.Commen;
using DevExpress.Web;

public partial class Controls_Transfer : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Dvgvtransfer_OnBeforePerformDataSelect(object sender, EventArgs e)
    {

    }

    protected void Dvgvtransfer_OnCustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        if (e.Column.FieldName.Equals("transfer_date"))
        {
            if ((e.Value).ToString().Length != 0)
            {
                string ethiodate;
                DateTime date = Convert.ToDateTime(e.Value);
                ethiodate = EthiopicDateTime.GetEthiopicDate(date.Day, date.Month, date.Year);
                e.DisplayText = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate,
                    Convert.ToInt32(p.Organization.LanguageID));
            }
        }
        if (e.Column.FieldName != "TransferTypeReal")
        {
            if (p.Organization.LanguageID == 1)
            {
                switch (e.Value.ToString())
                {
                    case"0":
                        e.DisplayText = "የውስጥ";
                        break;
                    case"1":
                        e.DisplayText = "ተጠባባቂ";
                        break;
                    case"2":
                        e.DisplayText = "እርስ በርስ";
                        break;
                    case"3":
                        e.DisplayText = "ትውስት";
                        break;
                    case"4":
                        e.DisplayText = "የፕሮጀክት";
                        break;
                    case"5":
                        e.DisplayText = "የውጭ";
                        break;
                }
            }
            else if (p.Organization.LanguageID == 2)
            {
                switch (e.Value.ToString())
                {
                    case"0":
                        e.DisplayText = "Kan Keessaa";
                        break;
                    case"1":
                        e.DisplayText = "Egaataa";
                        break;
                    case"2":
                        e.DisplayText = "Walliin";
                        break;
                    case"3":
                        e.DisplayText = "Ergiisaan";
                        break;
                    case"4":
                        e.DisplayText = "Projeectiidhan";
                        break;
                    case"5":
                        e.DisplayText = "Kan Alaa";
                        break;
                }
            }
        }
    }

    protected void LoadTransfer()
    {
        ProfileCommon p = this.Profile;
        p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
        Guid person = Guid.Parse(Session["APersonGuid"].ToString());
        int documentType = Convert.ToInt32(Session["DocumentType"].ToString());
        tblTransferHistoryBussiness promotions = new tblTransferHistoryBussiness();
        Dvgvtransfer.DataSource = promotions.GetRecords(person,p.Organization.LanguageID);
        Dvgvtransfer.DataBind();
    }

    protected void Dvgvtransfer_OnCustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        LoadTransfer();
    }
}