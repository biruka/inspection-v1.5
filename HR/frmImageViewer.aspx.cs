﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Web.Configuration;
using System.Web.Security;
using DevExpress.Web;
using System.Text;
using System.Diagnostics;
using System.Globalization;
using DevExpress.XtraPrinting;
using System.Drawing;
using System.IO;
using DevExpress.XtraReports.UI;

public partial class Forms_frmImageViewer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DoFindDocumentTestimony();
        }
    }
    protected bool DoFindDocumentTestimony()
    {
        string url = Session["FileURL"].ToString();
        string NavigateUrl = "~/AttachedFile/" + url;

        Response.Redirect(NavigateUrl);
        return true;
    }
}