﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="DocumentType.aspx.cs" Inherits="DocumentType" Culture="auto" meta:resourcekey="PageResource2" UICulture="auto" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>
<%@ Register Src="~/Controls/ConfirmBox.ascx" TagPrefix="uc1" TagName="ConfirmBox" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function DoNewClick(s, e) {
            cbDocumentType.PerformCallback('New');

        }
        function DoSaveClick(s, e) {
            if (ASPxClientEdit.ValidateGroup('DocumentTypeForm')) {
                cbDocumentType.PerformCallback('Save');
            }
        }

        function DoShowClick(s, e) {
            cbDocumentType.PerformCallback('Show');
            documentTypeSearch.SetVisible(true);
            documentTypeList.SetVisible(true);
        }

        function DoSearchClick(s, e) {
            cbDocumentType.PerformCallback('Search');
        }

        function OnEndDocumentTypeCallback(s, e) {
            if (s.cpStatus == "Success") {
                if (s.cpAction == 'registration') {
                    ShowSuccess(s.cpMessage);
                }
                if (s.cpAction == 'update') {
                    ShowSuccess(s.cpMessage);
                }
                if (s.cpAction == 'FormReseted') {
                    documentTypeSearch.SetVisible(false);
                    documentTypeList.SetVisible(false);
                    btnNew.SetEnabled(true);
                }
                if (s.cpAction == 'documentTypeloaded') {
                    documentTypeSearch.SetVisible(false);
                    documentTypeList.SetVisible(false);
                    btnNew.SetEnabled(true);
                }
            }
            else if (s.cpStatus == "ERROR") {
                if (s.cpAction == 'registration') {
                    ShowError(s.cpMessage);
                }
                if (s.cpAction == 'existance') {
                    pnlDocumentTypeForm.SetVisible(true);
                    btnSave.SetEnabled(true);
                    ShowError(s.cpMessage);
                }
            }
        }
        //===========================Grid===============
        var rowVisibleIndex;
        var strID;
        var strGuid;
        function gvwDocumentType_CustomButtonClick(s, e) {
            if (e.buttonID != 'Del' && e.buttonID != 'Edit') return;
            if (e.buttonID == 'Del') {
                rowVisibleIndex = e.visibleIndex;
                strGuid = s.GetRowKey(e.visibleIndex);
                s.GetRowValues(e.visibleIndex, 'MainGuid', ShowPopupDocument);
            }
            if (e.buttonID == 'Edit') {
                gvwDocumentType.GetRowValues(e.visibleIndex, 'MainGuid', OnGetSelectedFieldValuesDocumentType);
            }
        }
        function OnGetSelectedFieldValuesDocumentType(values) {
            cbDocumentType.PerformCallback('Edit' + '|' + values);
        }

        function ShowPopupDocument() {
            PopupDocument.Show();
            btnYes.Focus();
        }

        function btnYes_Click(s, e) {

            ClosePopupDocument(true);

        }

        function btnNo_Click(s, e) {
            ClosePopupDocument(false);
        }

        function ClosePopupDocument(result) {
            PopupDocument.Hide();
            if (result) {
                cbDocumentType.PerformCallback('Delete' + '|' + strGuid);
            }
        }

    </script>
    <style type="text/css">
        .erroStyle {
            font-size: smaller;
        }

        div.hidden {
            display: none
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceHolder" runat="Server">
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
    <uc1:Alert runat="server" ID="Alert" />
    <dx:ASPxCallbackPanel ID="cbDocumentType" ClientInstanceName="cbDocumentType" runat="server" Width="100%" OnCallback="cbDocumentType_Callback1" meta:resourcekey="cbDocumentTypeResource2">
        <ClientSideEvents EndCallback="OnEndDocumentTypeCallback" />
        <PanelCollection>
            <dx:PanelContent meta:resourcekey="PanelContentResource2">
                <table id="tblOuter" width="100%">
                    <tr>
                        <td>
                            <asp:Panel ID="pnlMenu" runat="server" CssClass="RoundPanelToolbar" Width="950px" meta:resourcekey="pnlMenu0Resource1">
                                <table cellpadding="0" cellspacing="0" style="width: 100%; padding: 0px;" width="990px">
                                    <tr>
                                        <td style="width: 20%"></td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <dx:ASPxButton ID="btnNew" runat="server" AutoPostBack="False" ClientInstanceName="btnNewClient" Font-Names="Visual Geez Unicode" Font-Size="Small" HorizontalAlign="Center" Text="አዲስ" Theme="Office2010Silver" VerticalAlign="Middle" Width="70px" meta:resourcekey="btnNewResource1" Enabled="true" Visible="true">
                                                            <ClientSideEvents Click="function(s, e) {
	                                                                    DoNewClick();
                                                                    }" />
                                                            <Image Url="~/Controls/ToolbarImages/new.gif">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxButton ID="btnSave" runat="server" AutoPostBack="False" ClientInstanceName="btnSave" Font-Names="Visual Geez Unicode" Font-Size="Small" HorizontalAlign="Center" Text="አስቀምጥ"
                                                            CausesValidation="true" Theme="Office2010Silver" VerticalAlign="Middle" Width="70px" TabIndex="11" meta:resourcekey="btnSaveResource1">
                                                            <ClientSideEvents Click="function(s, e) {
	                                                                    DoSaveClick();
                                                                    }" />
                                                            <Image Url="~/Controls/ToolbarImages/save.gif">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                    </td>
                                                    <td align="left" class="auto-style10">
                                                        <dx:ASPxButton ID="btnShow" runat="server" Text="አሳይ" Theme="DevEx" Width="70px" AutoPostBack="False" ClientInstanceName="btnShow" CausesValidation="False" meta:resourcekey="btnsaveResource2">
                                                            <ClientSideEvents Click="function(s, e) {  
                                                                        DoShowClick(); 
                                                                        } " />
                                                            <Image Url="~/images/Toolbar/search.gif">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                    </td>

                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 20%">&nbsp;</td>
                                        <td style="width: 10%"></td>

                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxPanel ID="pnlDocumentTypeForm" runat="server" ClientInstanceName="pnlDocumentTypeForm" CssClass="RoundPanelToolbar" Width="950px">
                                <PanelCollection>
                                    <dx:PanelContent>
                                        <table id="tblData" width="100%">
                                            <tr>
                                                <td style="text-align: right">
                                                    <asp:Label ID="lblReulatoryBody" runat="server" Text="ፌዴራል\ክልል ሲቪል ሰርቪስ" meta:resourcekey="lblReulatoryBodyResource2"></asp:Label>
                                                </td>
                                                <td>
                                                    <dx:ASPxComboBox ID="cboReulatoryBody" runat="server" Width="500px" Enabled="False" meta:resourcekey="cboReulatoryBodyResource2">
                                                    </dx:ASPxComboBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    <dx:ASPxLabel ID="lblCode" runat="server" ClientInstanceName="lblCode" Text="መለያ" Visible="false" meta:resourcekey="Label1Resource2">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td>
                                                    <dx:ASPxTextBox ID="txtCode" runat="server" Visible="false" Width="170px" Enabled="False" meta:resourcekey="txtCodeResource2">
                                                    </dx:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    <asp:Label ID="lblDescription" runat="server" Text="የሰው ሃብት ክንውን አይነት በእንግሊዘኛ" AssociatedControlID="txtDescription" meta:resourcekey="lblDescriptionResource2"></asp:Label>
                                                </td>
                                                <td>
                                                    <dx:ASPxTextBox ID="txtDescription" runat="server" Width="500px" meta:resourcekey="txtDescriptionResource2">
                                                        <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="Text" Display="Dynamic" ErrorTextPosition="Bottom" ValidationGroup="DocumentTypeForm">
                                                            <RegularExpression ErrorText="የእንግሊዝኛ ፊደል ይጠቀሙ" ValidationExpression="^[^-\s][a-zA-Z\s \/]+$" />
                                                            <RequiredField ErrorText="ባዶ መሆን አይችልም።" IsRequired="True" />
                                                            <ErrorFrameStyle CssClass="erroStyle"></ErrorFrameStyle>
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                    <asp:Label ID="lblDescriptionAm" runat="server" Text="የሰው ሃብት ክንውን አይነት የክልሉ ቋንቋ" meta:resourcekey="lblDescriptionAmResource2"></asp:Label>
                                                </td>
                                                <td>
                                                    <dx:ASPxTextBox ID="txtDescriptionAm" runat="server" Width="500px" meta:resourcekey="txtDescriptionAmResource2">
                                                        <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="Text" Display="Dynamic" ErrorTextPosition="Bottom" ValidationGroup="DocumentTypeForm">
                                                            <RequiredField ErrorText="ባዶ መሆን አይችልም።" IsRequired="True" />
                                                            <ErrorFrameStyle CssClass="erroStyle"></ErrorFrameStyle>
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxPanel>
                        </td>
                    </tr>

                </table>
                <dx:ASPxPanel ID="documentTypeSearch" runat="server" ClientInstanceName="documentTypeSearch" CssClass="RoundPanelToolbar" Width="950px">
                    <PanelCollection>
                        <dx:PanelContent>
                            <table >
                                <tr >
                                    <td style="align-content:center">
                                        <asp:Label ID="lblDocumentTypeRegionalLanguage" runat="server" Text="የሰው ሃብት ክንውን አይነት የክልሉ ቋንቋ"
                                            meta:resourcekey="lblDescriptionAmResource2"></asp:Label>
                                    </td>
                                    <td style="align-content:center">
                                        <asp:Label ID="lblDocumentTypeEnglish" runat="server" Text="የሰው ሃብት ክንውን አይነት በእንግሊዘኛ"></asp:Label>
                                    </td>
                                </tr>
                                <tr style="align-content:center">
                                    <td>
                                        <dx:ASPxTextBox ID="txtSearchOrgRegionalLanguage" runat="server" Width="250px" TabIndex="14" meta:resourcekey="txtSearchOrgResource1" Visible="True">
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="txtSearchOrgEnglish" runat="server" Width="250px" TabIndex="14" meta:resourcekey="txtSearchOrgResource1" Visible="True">
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td width="125px">
                                        <dx:ASPxButton ID="btnFindOrg" runat="server" AutoPostBack="False" ClientInstanceName="btnFindClient" Font-Names="Visual Geez Unicode" Font-Size="Small" HorizontalAlign="Center" Text="ፈልግ" Theme="Office2010Silver" VerticalAlign="Middle" Width="70px" TabIndex="15" meta:resourcekey="btnFindOrgResource1" Visible="true">
                                            <ClientSideEvents Click="function(s, e) {
	                                        DoSearchClick();
                                        }" />
                                            <Image Url="~/Controls/ToolbarImages/search.gif">
                                            </Image>
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
                <dx:ASPxPanel ID="documentTypeList" runat="server" ClientInstanceName="documentTypeList" CssClass="RoundPanelToolbar" Width="950px">
                    <PanelCollection>
                        <dx:PanelContent>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <dx:ASPxGridView ID="gvwtblDocumentType" ClientInstanceName="gvwDocumentType" runat="server"
                                            OnPageIndexChanged="gvwtblDocumentType_PageIndexChanged" KeyFieldName="MainGuid" AutoGenerateColumns="False"
                                            Width="100%" Theme="Office2010Silver" meta:resourcekey="gvwtblDocumentTypeResource2">
                                            <ClientSideEvents CustomButtonClick="gvwDocumentType_CustomButtonClick" />
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="MainGuid" VisibleIndex="0" Visible="False" meta:resourcekey="GridViewDataTextColumnResource5">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Code" Caption="መለያ ኮድ" VisibleIndex="1" Visible="true" meta:resourcekey="GridViewDataTextColumnResource9">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="Description" VisibleIndex="2" Caption="ክንውን አይነት በእንግሊዘኛ" meta:resourcekey="GridViewDataTextColumnResource6">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="DescriptionAm" VisibleIndex="3" Caption="ክንውን አይነት በክልሉ ቋንቋ" meta:resourcekey="GridViewDataTextColumnResource7">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="strReulatoryBody" VisibleIndex="4" Caption="ፌዴራል\ክልል ሲቪል ሰርቪስ" meta:resourcekey="GridViewDataTextColumnResource8">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewCommandColumn ShowInCustomizationForm="True" Width="100px" VisibleIndex="5">
                                                    <CustomButtons>
                                                        <dx:GridViewCommandColumnCustomButton ID="Edit" Text="አርም" meta:resourcekey="GridViewCommandColumnCustomButtonResource2"></dx:GridViewCommandColumnCustomButton>
                                                        <dx:GridViewCommandColumnCustomButton ID="Del" Text="ሰርዝ" meta:resourcekey="GridViewCommandColumnCustomButtonResource3"></dx:GridViewCommandColumnCustomButton>
                                                    </CustomButtons>
                                                </dx:GridViewCommandColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%--  <asp:GridView ID="gvwtblDocumentType" runat="server" AllowPaging="True"  AutoGenerateColumns="False" >
                             <%--   OnPageIndexChanging="gvwtblDocumentType_Page" 
                                OnRowDeleting="gvwtblDocumentType_RowDeleting" 
                                OnRowEditing="gvwtblDocumentType_RowEditing" 
                                PageSize="10" --%>

                                        <%-- <Columns>
                    <asp:TemplateField HeaderText="MainGuid">
                    <ItemTemplate>
                        <asp:Label ID="lblMainGuid" runat="server" Text='<%# Bind("MainGuid")%>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description")%>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="DescriptionAm">
                    <ItemTemplate>
                        <asp:Label ID="lblDescriptionAm" runat="server" Text='<%# Bind("DescriptionAm")%>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ReulatoryBody">
                    <ItemTemplate>
                        <asp:Label ID="lblReulatoryBody" runat="server" Text='<%# Bind("ReulatoryBody")%>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" Text="አርም"></asp:LinkButton>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" Text="ሰርዝ"></asp:LinkButton>
                    </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                </asp:GridView>--%>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <dx:ASPxPopupControl ID="PopupDocument" runat="server" ClientInstanceName="PopupDocument" Font-Names="Visual Geez Unicode" Font-Size="Small" HeaderText="Delete Confirmation" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" meta:resourcekey="PopupDocumentResource1">
                                            <ContentCollection>
                                                <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" meta:resourcekey="PopupControlContentControl1Resource1">
                                                    <uc1:ConfirmBox runat="server" ID="ConfirmBox" />
                                                </dx:PopupControlContentControl>
                                            </ContentCollection>
                                        </dx:ASPxPopupControl>

                                    </td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>

