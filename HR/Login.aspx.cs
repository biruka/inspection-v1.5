﻿using System;
using System.Configuration;
using System.Net.Mail;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using CUSTOR;
//using CUSTOR.Commen;

public partial class Login : System.Web.UI.Page
{


    private static void UpdateUserInfo(MembershipUser usrInfo)
    {
        Membership.UpdateUser(usrInfo);}

    private System.Web.UI.WebControls.Login GetLogin1()
    {
        System.Web.UI.WebControls.Login Login1 = loginBox.FindControl("Login1") as System.Web.UI.WebControls.Login;
        return Login1;
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            string strPasswordChanged = Request["pwd"];
            if (!string.IsNullOrEmpty(strPasswordChanged))
            {
                //pnlMsg.Visible = true;
                //CMsgBar.ShowMessage("Your password has been changed. <br> Please login with the new password.", pnlMsg,lblMessage);

                pnlInfo.Visible = true;
                lblInfo.Text = "Your password has been changed. <br> Please login with the new password.";
            }else
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated && !Page.IsPostBack)
                {
                    System.Web.UI.WebControls.Login Login1 = GetLogin1();
                    TextBox UserName = Login1.FindControl("UserName") as TextBox;
                    UserName.Focus();
                    //DoLogout();
                }
                lblMessage.Text = string.Empty;
             
                lblExpiryMsg.Text = string.Format("Your password must be changed at least every <strong>{0}</strong> days", GetExpiryDays());
            }
        }
        catch
        {

        }



    }
    
    private int GetExpiryDays()
    {
        int intExpiryDays;
        PasswordSetting passwordSetting = CPasswordManager.GetPasswordSetting();
        try
        {
            //intExpiryDays = Convert.ToInt32(ConfigurationManager.AppSettings["PasswordExpiryDuration"]);
            intExpiryDays = passwordSetting.Duration;
        }
        catch
        {
            intExpiryDays = 15;
        }
        return intExpiryDays;
    }
   
    protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
    {
        try
        {
            System.Web.UI.WebControls.Login Login1 = GetLogin1();
            string loginUsername = Login1.UserName;
            string loginPassword = Login1.Password;


            MembershipUser usr = Membership.GetUser(loginUsername);
             
            string strPWD = usr.GetPassword();
            if (strPWD == "Ch@ngeP@55w0rd")
            {
                Session["CurrentUserName"] = loginUsername;
                Session["IsNewUser"] = true;
                if (Membership.ValidateUser(loginUsername, strPWD)){
                    e.Authenticated = true;
                    return;
                }
                else
                {
                    CMsgBar.ShowError("User name and/or Password are not valid!", pnlMsg, lblError);
                    e.Authenticated = false;
                }
                //Response.Redirect("~/Account/ChangePassword.aspx?U=" + loginUsername);
            }
            else
            {

                if (Membership.ValidateUser(loginUsername, loginPassword)) // "59ce1189-d7c8-47d2-ab83-a55afce26bcd"))
                {
                    e.Authenticated = true;
                }
                else
                {
                    CMsgBar.ShowError("User name and/or Password are not valid!", pnlMsg, lblError);
                    e.Authenticated = false;
                }
            }
            Session["IsNewUser"] = false;
        }
        catch (Exception ex)
        {
            string strMsg = "Error. Please check your user name and password and try again";
            CMsgBar.ShowError(strMsg, pnlMsg, lblError);
        }
    }
    protected void Login1_LoginError(object sender, EventArgs e)
    {
        System.Web.UI.WebControls.Login Login1 = GetLogin1();
        MembershipUser usrInfo = Membership.GetUser(Login1.UserName);
        if (usrInfo == null)
            return;
        int passwordAttemptLockoutDuration = Convert.ToInt32(System.Web.Configuration.WebConfigurationManager.AppSettings["PasswordResetDuration"]);
        string strMsg;
        // if user is locked out
        if (usrInfo.IsLockedOut)
        {
 
             strMsg =
                string.Format(
                    "Your account has been locked out because of too many invalid login attempts. Please wait {0} minutes and try again.",
                    passwordAttemptLockoutDuration);
            CMsgBar.ShowError(strMsg, pnlMsg, lblError);
            return;
        }

        int passwordResetDuration = Convert.ToInt32(System.Web.Configuration.WebConfigurationManager.AppSettings["PasswordResetDuration"]);

        // if password is not expired but the account is not approved
        if (usrInfo.IsApproved || usrInfo.LastPasswordChangedDate.ToUniversalTime().AddDays(passwordResetDuration) <= DateTime.UtcNow)
            return;
        
         strMsg = "Your account has not yet been approved.";
       
        CMsgBar.ShowError(strMsg, pnlMsg, lblError);
    }

    protected void Login1_LoggingIn(object sender, LoginCancelEventArgs e)
    {

        System.Web.UI.WebControls.Login Login1 = GetLogin1();
     
        MembershipUser usrInfo = Membership.GetUser(Login1.UserName, false);
        int passwordAttemptLockoutDuration = Convert.ToInt32(System.Web.Configuration.WebConfigurationManager.AppSettings["PasswordResetDuration"]);
        if (usrInfo != null && usrInfo.IsLockedOut && usrInfo.LastLockoutDate.ToUniversalTime().AddMinutes(passwordAttemptLockoutDuration) < DateTime.UtcNow)
        {
            usrInfo.UnlockUser();
        }


        //diasbale the below feature for now
        return;
        bool passwordResetEnabled = Convert.ToBoolean(System.Web.Configuration.WebConfigurationManager.AppSettings["PasswordResetEnabled"]);
        // if auto password expiration is enabled 
        if (passwordResetEnabled != true)
            return;
        int passwordResetDuration = Convert.ToInt32(System.Web.Configuration.WebConfigurationManager.AppSettings["PasswordResetDuration"]);
        // if last password changed date + reset duration is less than the date now
        if (usrInfo == null || usrInfo.LastPasswordChangedDate.ToUniversalTime().AddDays(passwordResetDuration) >= DateTime.UtcNow)
            return;

        // set user's approval to false so user can't login
        usrInfo.IsApproved = false;
        UpdateUserInfo(usrInfo);

        // Create random GUID for new password 
        Guid randomPasswordGuid = System.Guid.NewGuid();

        // declare variables for user name and password
        string username = Convert.ToString(usrInfo);
        string password = randomPasswordGuid.ToString();

        MembershipUser mu = Membership.Providers["CUSTORMembershipProvider"].GetUser(username, false);

        // change user password to newly generated guid
        mu.ChangePassword(mu.ResetPassword(), password);

        // set user approval to true so user can now login with new guid password
        usrInfo.IsApproved = true;
        UpdateUserInfo(usrInfo);

        Alert1.ShowError(usrInfo + ", As per our user policy, your account password has expired! An e-mail is being sent with your new credentials.");

        SendEmail(mu, usrInfo);
    }
    private void SendEmail(MembershipUser mu, MembershipUser usrInfo)
    {
        try
        {
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();

            // determine the site's URL so we can use it in the email
            string urlBase = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;

            // get user's new password and  account email so we can email it - mu is already declared above
            string ePassword = mu.GetPassword();
            string FromEmailAddress = System.Web.Configuration.WebConfigurationManager.AppSettings["SenderEmail"];
            string eEmail = mu.Email;
            string eFrom = FromEmailAddress;
            string eFromDisplayName = "Administrator";
            string eSubject = "Password Reset! - Your new credentials";

            message.To.Add(eEmail);
            message.From = new MailAddress(eFrom, eFromDisplayName);
            message.Subject = eSubject;
            message.Body = "<p>Hello " + usrInfo + ",</p>" + " <p>Your password for " + urlBase + " have been reset:<br/>User Name: "
                            + usrInfo + " <br />Your New Password: " + ePassword +
                            " <br /> Please use your new password to login to your account and change it to you own.<br/><br/> Thank You!<br/>Administrator.</p>";
            message.IsBodyHtml = true;
            message.Priority = MailPriority.High;

            SmtpClient client = new SmtpClient();
            client.Send(message);
        }
        catch (Exception ex)
        {

            Alert1.ShowError(" ERROR Sending Mail: " + ex.Message);
        }
        finally
        {
            // 
        }
    }
    private void DoLogout()
    {
        /* Create new session ticket that expires immediately */
        FormsAuthenticationTicket ticket =
            new FormsAuthenticationTicket(
                1,
                this.Context.User.Identity.Name,
                DateTime.Now,
                DateTime.Now,
                false,
                Guid.NewGuid().ToString());

        /* Encrypt the ticket */
        string encrypted_ticket = FormsAuthentication.Encrypt(ticket);

        /* Create cookie */
        HttpCookie cookie = new HttpCookie(
            FormsAuthentication.FormsCookieName,
            encrypted_ticket);

        /* Add cookie */
        this.Context.Response.Cookies.Add(cookie);

        /* Abandon session object to destroy all session variables */
        this.Context.Session.Clear();
        this.Context.Session.Abandon();
        //==================
        //Response.Redirect("~/Login.aspx");
    }
}