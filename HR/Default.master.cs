﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using CUSTOR.Commen;
using System.Web.Security;
using CUSTOR.Bussiness;
using CUSTOR.Domain;
using DevExpress.Web;
using System.Configuration;
public partial class _Default : System.Web.UI.MasterPage
    {

        public bool TimeoutControlEnabled
        {
            get { return Timeout1.Enabled; }
            set { Timeout1.Enabled = value; }
        }

        // can be used to access the clientid from content pages
        public string TimeoutControlClientId
        {
            get { return Timeout1.ClientID; }
        }

        protected void Timeout1_RaisingCallbackEvent(object sender, EventArgs e)
        {
            // when the timeout control's callback is fired, this event will fire
            //string x = "";
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["Language"] == null)
            {
                //ProfileCommon P = this.Profile;
                Page.Header.DataBind();
                ProfileCommon p = this.Profile;
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);

                    if (p.Organization.Language != null)
                    {
                        SetLanguge(p.Organization.LanguageID);
                    }
                    Session["Language"]= p.Organization.LanguageID;
                    Session["ProfileOrgGuid"] = p.Organization.GUID;
                    Session["ProfileOrgCatagory"] = p.Organization.Category;
                    Session["ProfileFullname"] = p.Staff.FullName;
                    Session["Zone"] = p.Organization.Zone;
                    Session["ProLanguage"] = p.Organization.LanguageID;
                    Session["UserID"] = p.UserName;// myObject.ProviderUserKey.ToString();
                    Session["PesrsonGuid"] = p.Staff.GUID;
                    Session["IsEthiopic"] = (p.Organization.LanguageID == (int)Language.eLanguage.eAmharic ||
                                             p.Organization.LanguageID == (int)Language.eLanguage.eTigrigna);
                }

                //switch (p.Organization.LanguageID)
                //{
                //    //case 2:
                //    //    mnuMain.DataSource = ASPxSiteMapDataSource3;
                //    //    mnuMain.DataBind();
                //        //SitemapDataSource.ResolveUrl("~/WebOromifa.sitemap");
                //        //SitemapDataSource.Provider.ResourceKey ="~/WebOromifa.sitemap";
                //        break;
                //    case 1:
                //    case 0:
                //        mnuMain.DataSource = ASPxSiteMapDataSource1;
                //        mnuMain.DataBind();
                //        //SitemapDataSource.ResolveUrl("~/Web.sitemap");
                //        break;
                //}
                mnuMain.Orientation = Orientation.Horizontal;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.DataBind();
            if (!Page.IsPostBack)
            {
                if (Session["Language"] != null)
                {



                if (Convert.ToInt32(Session["Language"]) == 2)
                {
                    this.ASPxSiteMapDataSource1.SiteMapFileName = "~/WebOr.sitemap";
                    mnuMain.DataSource = ASPxSiteMapDataSource1;
                    mnuMain.DataBind();
                }
                else
                {
                    mnuMain.DataSource = ASPxSiteMapDataSource1;
                    mnuMain.DataBind();
                }
                    //if (Convert.ToInt32(Session["Language"].ToString()) == 2)
                    //{
                    //    mnuMain.DataSource = ASPxSiteMapDataSource3;
                    //    mnuMain.DataBind();
                    //}
                    //else if (Convert.ToInt32(Session["Language"].ToString()) == 1)
                    //{
                    //    mnuMain.DataSource = ASPxSiteMapDataSource1;
                    //    mnuMain.DataBind();
                    //}
                    lblOrg.Text = @"(c) " + GetOrgName();
                }
                else
                {
                    //mnuMain.DataSource = ASPxSiteMapDataSource1;
                    //mnuMain.DataBind();
                }
            }
        }
        private string GetOrgName()
        {
            try
            {
                return ConfigurationManager.AppSettings["OrgName"];
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        private void SetLanguge(int lngID)
        {
            switch (lngID)
            {
                case 0: SetCulture("en-US", "en-US");//English
                    break;
                case 2: SetCulture("am-ET", "am-ET");//Afan Oromo
                    break;
                case 3: SetCulture("am-ET", "am-ET"); //Tig
                    break;
                case 4: SetCulture("en-GB", "en-GB");//afar
                    break;
                case 5: SetCulture("en-AU", "en-AU");//Somali
                    break;
                default: break;//Amharic
            }

        }

        protected void LoginStatus1_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            if (Request.Cookies["ASP.NET_SessionId"] != null)
            {
                Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            }

            if (Request.Cookies["AuthToken"] != null)
            {
                Response.Cookies["AuthToken"].Value = string.Empty;
                Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
            }

        }
        protected void SetCulture(string name, string locale)
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(name);
            Thread.CurrentThread.CurrentCulture = new CultureInfo(locale);

            Session["CurrentUICulture"] = Thread.CurrentThread.CurrentUICulture;
            Session["CurrentCulture"] = Thread.CurrentThread.CurrentCulture;
        }
        protected void mnuMain_MenuItemClick(object sender, MenuEventArgs e)
        {

        }

        protected void mnuMain_ItemDataBound(object source, DevExpress.Web.MenuItemEventArgs e)
        {
        if (e.Item.Text == "መደበኛ")
        {
            if (CAccessRight.CanAccess(CResource.eResource.einspectordataencoder))
            {
                e.Item.Enabled = false;

            }

        }
        SiteMapNode node = e.Item.DataItem as SiteMapNode;
            if (Int32.Parse(Session["ProLanguage"].ToString()) == 2)
            {
            //if (node != null && node.Title == "Inspection(O)")
            //{
            //    //Check whether the user is part of a team
            //    tblTeamMembersBussiness objTeamMembers = new tblTeamMembersBussiness();
            //    ProfileCommon objProfile = this.Profile;
            //    objProfile = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
            //    List<tblTeamMembers> objTeam = objTeamMembers.GetRecordByPersonGuid(Guid.Parse(objProfile.Staff.GUID.ToString()));
            //    if (objTeam.Count == 0)
            //    {
            //       if (!CAccessRight.CanAccess(CResource.eResource.einspectorofficer) && !CAccessRight.CanAccess(CResource.eResource.einspectordirector) && !CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) && !CAccessRight.CanAccess(CResource.eResource.ehrofficer))
            //        e.Item.Enabled = false;
            //    }
            //    else
            //    {

            //    }
            //}

            //if (node != null && node.Title == "Grievance(Complain) Decision(O)")
            //{
            //    if (!CAccessRight.CanAccess(CResource.eResource.einspectorofficer) && !CAccessRight.CanAccess(CResource.eResource.einspectordirector) && !CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor))
            //        e.Item.Enabled = false;
            //}
            //if (node != null && node.Title == "Grievance(Complain) Registration(O)")
            //{
            //    if (!CAccessRight.CanAccess(CResource.eResource.einspectordataencoder))
            //        e.Item.Enabled = false;
            //}

            //if (node != null && node.Title == "Report(O)")
            //{
            //    if (!CAccessRight.CanAccess(CResource.eResource.einspectorofficer) && !CAccessRight.CanAccess(CResource.eResource.einspectordirector) && !CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor))
            //        e.Item.Enabled = false;
            //}

            if (e.Item.Text == "Gamaaggamaa")
            {
                if (CAccessRight.CanAccess(CResource.eResource.einspectordataencoder))
                {
                    e.Item.Enabled = false;

                }

            }


            if (node != null && node.Title == "Gamaaggamaa ")
            {
                tblTeamMembersBussiness objTeamMembers = new tblTeamMembersBussiness();
                ProfileCommon objProfile = this.Profile;
                objProfile = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
                List<tblTeamMembers> objTeam = objTeamMembers.GetRecordByPersonGuid(Guid.Parse(objProfile.Staff.GUID.ToString()));
                if (objTeam.Count == 0)
                {
                    if (!CAccessRight.CanAccess(CResource.eResource.einspectorofficer) &&
                        !CAccessRight.CanAccess(CResource.eResource.einspectordirector) &&
                        !CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) &&
                        !CAccessRight.CanAccess(CResource.eResource.ehrofficer) &&
                        !CAccessRight.CanAccess(CResource.eResource.eHRSupervisor) &&
                        !CAccessRight.CanAccess(CResource.eResource.einspectordataencoder))
                        e.Item.Enabled = false;



                }
                else
                {

                }
            }
            if (node != null && node.Title == "Gaafii  Keesummeessuu")
            {
                tblTeamMembersBussiness objTeamMembers = new tblTeamMembersBussiness();
                ProfileCommon objProfile = this.Profile;
                objProfile = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
                List<tblTeamMembers> objTeam = objTeamMembers.GetRecordByPersonGuid(Guid.Parse(objProfile.Staff.GUID.ToString()));
                if (objTeam.Count == 0)
                {
                    if (!CAccessRight.CanAccess(CResource.eResource.einspectorofficer) &&
                             !CAccessRight.CanAccess(CResource.eResource.einspectordirector) &&
                             !CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) &&
                             !CAccessRight.CanAccess(CResource.eResource.ehrofficer) &&
                             !CAccessRight.CanAccess(CResource.eResource.eHRSupervisor))
                        e.Item.Enabled = false;



                }
                else
                {

                }
            }
            if (node != null && node.Title == "ቅሬታና ጥቆማ ውሳኔ")
            {
                if (!CAccessRight.CanAccess(CResource.eResource.einspectorofficer) && !CAccessRight.CanAccess(CResource.eResource.einspectordirector) && !CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor))
                    e.Item.Enabled = false;
            }
            if (node != null && node.Title == "ቅሬታና ጥቆማ መመዝገቢያ")
            {
                if (!CAccessRight.CanAccess(CResource.eResource.einspectordataencoder))
                    e.Item.Enabled = false;
            }
            if (node != null && node.Title == "Adda Addaa")
            {
                if (!CAccessRight.CanAccess(CResource.eResource.einspectordirector) && !CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor))
                    e.Item.Enabled = false;
            }
            if (node != null && node.Title == "Caasaa Gare ")
            {
                if (!CAccessRight.CanAccess(CResource.eResource.einspectordirector) && !CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor))
                    e.Item.Enabled = false;
            }

            if (node != null && node.Title == "Report(O)")
            {
                if (!CAccessRight.CanAccess(CResource.eResource.einspectorofficer) && !CAccessRight.CanAccess(CResource.eResource.einspectordirector) && !CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor))
                    e.Item.Enabled = false;
            }
        }
            else if (Int32.Parse(Session["ProLanguage"].ToString()) == 1)
            {
                if (node != null && node.Title == "ግምገማ ")
                {
                    tblTeamMembersBussiness objTeamMembers = new tblTeamMembersBussiness();
                    ProfileCommon objProfile = this.Profile;
                    objProfile = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
                    List<tblTeamMembers> objTeam = objTeamMembers.GetRecordByPersonGuid(Guid.Parse(objProfile.Staff.GUID.ToString()));
                    if (objTeam.Count == 0)
                    {
                        if (!CAccessRight.CanAccess(CResource.eResource.einspectorofficer) &&
                            !CAccessRight.CanAccess(CResource.eResource.einspectordirector) &&
                            !CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) &&
                            !CAccessRight.CanAccess(CResource.eResource.ehrofficer) &&
                            !CAccessRight.CanAccess(CResource.eResource.eHRSupervisor)&&
                            !CAccessRight.CanAccess(CResource.eResource.einspectordataencoder))
                            e.Item.Enabled = false;

                    
               
                }
                    else
                    {
                                                
                    }
                }
            if (node != null && node.Title == "ጥያቄ ማቅረቢያ/ማስተናገጃ")
            {
                tblTeamMembersBussiness objTeamMembers = new tblTeamMembersBussiness();
                ProfileCommon objProfile = this.Profile;
                objProfile = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
                List<tblTeamMembers> objTeam = objTeamMembers.GetRecordByPersonGuid(Guid.Parse(objProfile.Staff.GUID.ToString()));
                if (objTeam.Count == 0)
                {
                    if (!CAccessRight.CanAccess(CResource.eResource.einspectorofficer) &&
                             !CAccessRight.CanAccess(CResource.eResource.einspectordirector) &&
                             !CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) &&
                             !CAccessRight.CanAccess(CResource.eResource.ehrofficer) &&
                             !CAccessRight.CanAccess(CResource.eResource.eHRSupervisor))
                        e.Item.Enabled = false;



                }
                else
                {

                }
            }
            if (node != null && node.Title == "ቅሬታና ጥቆማ ውሳኔ")
                {
                    if (!CAccessRight.CanAccess(CResource.eResource.einspectorofficer) && !CAccessRight.CanAccess(CResource.eResource.einspectordirector) && !CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor))
                        e.Item.Enabled = false;
                }
                if (node != null && node.Title == "ቅሬታና ጥቆማ መመዝገቢያ")
                {
                    if (!CAccessRight.CanAccess(CResource.eResource.einspectordataencoder))
                        e.Item.Enabled = false;
                }
                if (node != null && node.Title == "ልዩ ልዩ")
                {
                    if (!CAccessRight.CanAccess(CResource.eResource.einspectordirector) && !CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor))
                        e.Item.Enabled = false;
                }
                if (node != null && node.Title == "የቡድን አወቃቀር")
                {
                    if (!CAccessRight.CanAccess(CResource.eResource.einspectordirector) && !CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor))
                        e.Item.Enabled = false;
                }
                
                if (node != null && node.Title == "ሪፖርት")
                { 
                    if (!CAccessRight.CanAccess(CResource.eResource.einspectorofficer) && !CAccessRight.CanAccess(CResource.eResource.einspectordirector) && !CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor))
                        e.Item.Enabled = false;
                }
            }
        }
        protected void ImageButton_Click(object sender, ImageClickEventArgs e)
        {
            DoLogout();
        }
        private void DoLogout()
        {
            /* Create new session ticket that expires immediately */
            FormsAuthenticationTicket ticket =
                new FormsAuthenticationTicket(
                    1,
                    this.Context.User.Identity.Name,
                    DateTime.Now,
                    DateTime.Now,
                    false,
                    Guid.NewGuid().ToString());

            /* Encrypt the ticket */
            string encrypted_ticket = FormsAuthentication.Encrypt(ticket);

            /* Create cookie */
            HttpCookie cookie = new HttpCookie(
                FormsAuthentication.FormsCookieName,
                encrypted_ticket);

            /* Add cookie */
            this.Context.Response.Cookies.Add(cookie);

            /* Abandon session object to destroy all session variables */
            this.Context.Session.Clear();
            this.Context.Session.Abandon();
            //==================
            Response.Redirect("~/Login.aspx");
        }
    }
 