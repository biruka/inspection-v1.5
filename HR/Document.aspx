﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="Document.aspx.cs" Inherits="Document" Culture="auto" meta:resourcekey="PageResource2" UICulture="auto" %>

<%@ Register Assembly="DevExpress.Web.v19.2, Version=19.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<%@ Register Src="~/Controls/ConfirmBox.ascx" TagPrefix="uc1" TagName="ConfirmBox" %>
<%@ Register Src="~/Controls/Alert.ascx" TagPrefix="uc1" TagName="Alert" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function OnParentGuidChanged(cboParentGuid) {
            cbDocument.PerformCallback('DocumentDetail' + '|' + cboParentGuid.GetValue().toString());
        }

        function DoNewClick(s, e) {
            cbDocument.PerformCallback('New');
        }
        function DoSaveClick(s, e) {
            alert(ASPxClientEdit.ValidateGroup('DocumentForm'));
            if (ASPxClientEdit.ValidateGroup('DocumentForm')) {
                cbDocument.PerformCallback('Save');
            }
        }

        function DoShowClick(s, e) {
            cbDocument.PerformCallback('Show');
        }

        function DoSearchClick(s, e) {
            cbDocument.PerformCallback('Search');
        }

        function OnEndDocumentCallback(s, e) {

            if (s.cpMessage == '') {
                return;
            }

            if (s.cpStatus == "SUCCESS") {
                if (s.cpMessage == '') {
                    return;
                }
                ShowSuccess(s.cpMessage);
            }
            else if (s.cpStatus == "ERROR") {
                ShowError(s.cpMessage);
            }
        }
        //===========================Grid===============
        var rowVisibleIndex;
        var strID;
        var strGuid;
        function gvwDocument_CustomButtonClick(s, e) {
            if (e.buttonID != 'Del' && e.buttonID != 'Edit') return;
            if (e.buttonID == 'Del') {
                rowVisibleIndex = e.visibleIndex;
                strGuid = s.GetRowKey(e.visibleIndex);
                s.GetRowValues(e.visibleIndex, 'MainGuid', ShowPopupDocument);
            }
            if (e.buttonID == 'Edit') {
                gvwDocument.GetRowValues(e.visibleIndex, 'MainGuid', OnGetSelectedFieldValuesDocument);
            }
        }
        function OnGetSelectedFieldValuesDocument(values) {
            cbDocument.PerformCallback('Edit' + '|' + values);
        }
        function ShowPopupDocument() {
            PopupDocument.Show();
            btnYes.Focus();
        }

        function btnYes_Click(s, e) {

            ClosePopupDocument(true);

        }

        function btnNo_Click(s, e) {
            ClosePopupDocument(false);
        }

        function ClosePopupDocument(result) {
            PopupDocument.Hide();
            if (result) {
                cbDocument.PerformCallback('Delete' + '|' + strGuid);
            }
        }
    </script>
    <style type="text/css">
        .erroStyle {
            font-size: smaller;
        }

        div.hidden {
            display: none
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPlaceHolder" runat="Server">
    <uc1:Alert runat="server" ID="Alert" />
    <div>
        <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
        <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="100%" ShowHeader="False" meta:resourcekey="ASPxRoundPanel1Resource2">

            <PanelCollection>
                <dx:PanelContent runat="server" SupportsDisabledAttribute="True" meta:resourcekey="PanelContentResource4">
                    <dx:ASPxCallbackPanel ID="cbDocument" runat="server" ClientInstanceName="cbDocument" Width="100%" OnCallback="cbDocument_Callback" meta:resourcekey="cbDocumentResource2">
                        <ClientSideEvents EndCallback="OnEndDocumentCallback" />
                        <PanelCollection>
                            <dx:PanelContent meta:resourcekey="PanelContentResource5">
                                <table id="tblOuter" width="100%">
                                    <tr>
                                        <td>
                                            <asp:Panel ID="pnlMenu" runat="server" CssClass="RoundPanelToolbar" Width="950px" meta:resourcekey="pnlMenu0Resource1">
                                                <table cellpadding="0" cellspacing="0" style="width: 100%; padding: 0px;" width="990px">
                                                    <tr>
                                                        <td style="width: 20%"></td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxButton ID="btnNew" runat="server" AutoPostBack="False" CausesValidation="false" ClientInstanceName="btnNewClient" Font-Names="Visual Geez Unicode" Font-Size="Small" HorizontalAlign="Center" Text="አዲስ" Theme="Office2010Silver" VerticalAlign="Middle" Width="70px" meta:resourcekey="btnNewResource1">
                                                                            <ClientSideEvents Click="function(s, e) {
	                                                                    DoNewClick();
                                                                    }" />
                                                                            <Image Url="~/Controls/ToolbarImages/new.gif">
                                                                            </Image>
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxButton ID="btnSave" runat="server" AutoPostBack="False" ClientInstanceName="btnSave" CausesValidation="true" Font-Names="Visual Geez Unicode" Font-Size="Small" HorizontalAlign="Center" Text="አስቀምጥ" Theme="Office2010Silver" VerticalAlign="Middle" Width="70px" TabIndex="11" meta:resourcekey="btnSaveResource1">
                                                                            <ClientSideEvents Click="function(s, e) {
	                                                                    DoSaveClick();
                                                                    }" />
                                                                            <Image Url="~/Controls/ToolbarImages/save.gif">
                                                                            </Image>
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxButton ID="btnShow" runat="server" AutoPostBack="False" CausesValidation="false" ClientInstanceName="btnShow" Font-Names="Visual Geez Unicode" Font-Size="Small" HorizontalAlign="Center" Text="አሳይ" Theme="Office2010Silver" VerticalAlign="Middle" Width="70px" TabIndex="11" meta:resourcekey="btnShowResource1">
                                                                            <ClientSideEvents Click="function(s, e) {
	                                                                            DoShowClick();
                                                                            }" />
                                                                            <Image Url="~/Controls/ToolbarImages/search.gif">
                                                                            </Image>
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                    <td></td>
                                                                    <td>&nbsp;</td>
                                                                    <td>
                                                                        <dx:ASPxComboBox ID="cboHRActivity" runat="server" Width="250px" NullText="ይምረጡ" TabIndex="14" meta:resourcekey="txtSearchOrgResource1" Visible="True">
                                                                        </dx:ASPxComboBox>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxButton ID="btnFindOrg" runat="server" AutoPostBack="False" ClientInstanceName="btnFindClient" Font-Names="Visual Geez Unicode" Font-Size="Small" HorizontalAlign="Center" Text="ፈልግ" Theme="Office2010Silver" VerticalAlign="Middle" Width="70px" TabIndex="15" meta:resourcekey="btnFindOrgResource1" Visible="True">
                                                                            <ClientSideEvents Click="function(s, e) {
	                                                                                 DoSearchClick();
                                                                                 }" />
                                                                            <Image Url="~/Controls/ToolbarImages/search.gif">
                                                                            </Image>
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 20%">&nbsp;</td>
                                                        <td style="width: 10%"></td>

                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:ASPxPanel ID="pnlData" ClientInstanceName="pnlData" runat="server" Width="100%" meta:resourcekey="pnlDataResource2">
                                                <PanelCollection>
                                                    <dx:PanelContent meta:resourcekey="PanelContentResource6">
                                                        <table id="tblData" width="100%">
                                                            <tr>
                                                                <td style="text-align: right">
                                                                    <asp:Label ID="lblParentGuid" runat="server" AssociatedControlID="cboParentGuid" Text="የሰው ሃብት ክንውን አይነት" meta:resourcekey="lblParentGuidResource2"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxComboBox ID="cboParentGuid" runat="server" Width="500px" OnDataBinding="cboParentGuid_DataBinding" NullText="ይምረጡ" meta:resourcekey="cboParentGuidResource2">
                                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                                                            OnParentGuidChanged(s);}" />
                                                                        <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="Text" Display="Dynamic" ErrorTextPosition="Bottom" ValidationGroup="DocumentForm">
                                                                            <RequiredField ErrorText="ባዶ መሆን አይችልም።" IsRequired="True" />
                                                                            <ErrorFrameStyle CssClass="erroStyle"></ErrorFrameStyle>
                                                                        </ValidationSettings>
                                                                    </dx:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align: right">
                                                                    <dx:ASPxLabel ID="lblCode" runat="server" Text="መለያ" ClientInstanceName="lblCode" meta:resourcekey="lblCodeResource2"></dx:ASPxLabel>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxTextBox ID="txtCode" runat="server" ClientInstanceName="txtCode" Width="500px" meta:resourcekey="txtCodeResource2">
                                                                        <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip">
                                                                            <ErrorImage Url="~/Images/ToolbarImages/warn.gif"></ErrorImage>
                                                                        </ValidationSettings>
                                                                    </dx:ASPxTextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align: right">
                                                                    <asp:Label ID="lblDescription" runat="server" Text="የሰነድ አይነት በእንግሊዘኛ" AssociatedControlID="txtDescription" meta:resourcekey="lblDescriptionResource2"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxTextBox ID="txtDescription" runat="server" Width="500px">
                                                                        <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="Text" Display="Dynamic" ErrorTextPosition="Bottom" ValidationGroup="DocumentForm">
                                                                            <RegularExpression ErrorText="የእንግሊዝኛ ፊደል ይጠቀሙ" ValidationExpression="^[^-\s][a-zA-Z\s \/]+$" />
                                                                            <RequiredField ErrorText="ባዶ መሆን አይችልም።" IsRequired="True" />
                                                                            <ErrorFrameStyle CssClass="erroStyle"></ErrorFrameStyle>
                                                                        </ValidationSettings>
                                                                    </dx:ASPxTextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align: right">
                                                                    <asp:Label ID="lblDescriptionAm" runat="server" Text="የሰነድ አይነት በክልሉ ቋንቋ" AssociatedControlID="txtDescriptionAm" meta:resourcekey="lblDescriptionAmResource2"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxTextBox ID="txtDescriptionAm" runat="server" Width="500px">
                                                                        <ValidationSettings SetFocusOnError="True" ErrorDisplayMode="Text" Display="Dynamic" ErrorTextPosition="Bottom" ValidationGroup="DocumentForm">
                                                                            <RequiredField ErrorText="ባዶ መሆን አይችልም።" IsRequired="True" />
                                                                            <ErrorFrameStyle CssClass="erroStyle"></ErrorFrameStyle>
                                                                        </ValidationSettings>
                                                                    </dx:ASPxTextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align: right">&nbsp;</td>
                                                                <td>
                                                                    <dx:ASPxCheckBox ID="ckIsoptional" runat="server" CheckState="Unchecked" Text="የግድ መረጋገጥ ይገባዋል ?" Theme="iOS" Width="500px" meta:resourcekey="ckIsoptionalResource2">
                                                                    </dx:ASPxCheckBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align: right">&nbsp;</td>
                                                                <td>
                                                                    <dx:ASPxGridView ID="gvwtbllookup" ClientInstanceName="gvwtbllookup" runat="server" AutoGenerateColumns="False" Width="500px" Theme="Office2010Silver" OnDataBinding="gvwtbllookup_DataBinding" OnPageIndexChanged="gvwtbllookup_PageIndexChanged" meta:resourcekey="gvwtbllookupResource2">
                                                                        <Columns>
                                                                           <dx:GridViewCommandColumn ShowInCustomizationForm="True" ShowSelectCheckbox="True" VisibleIndex="0" Visible="False">
                                                                            </dx:GridViewCommandColumn>
                                                                            <dx:GridViewDataTextColumn FieldName="chkInclude" ShowInCustomizationForm="True" VisibleIndex="1" Caption=" " meta:resourcekey="GridViewDataTextColumnResource10">
                                                                                <DataItemTemplate>
                                                                                    <dx:ASPxCheckBox ID="chkInclude" runat="server" Theme="Aqua" CheckState="Unchecked" meta:resourcekey="chkIncludeResource2">
                                                                                    </dx:ASPxCheckBox>
                                                                                </DataItemTemplate>
                                                                            </dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn Caption="ሰነዱ የሚያስፈልግበት አይነት" FieldName="amdescription" ShowInCustomizationForm="True" VisibleIndex="2" meta:resourcekey="GridViewDataTextColumnResource11">
                                                                            </dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn FieldName="id" ShowInCustomizationForm="True" Visible="False" VisibleIndex="3" meta:resourcekey="GridViewDataTextColumnResource12">
                                                                            </dx:GridViewDataTextColumn>
                                                                        </Columns>
                                                                        <SettingsBehavior ProcessSelectionChangedOnServer="True" />
                                                                        <SettingsPager PageSize="20">
                                                                        </SettingsPager>
                                                                    </dx:ASPxGridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </dx:PanelContent>
                                                </PanelCollection>
                                            </dx:ASPxPanel>


                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:ASPxGridView ID="gvwtblDocument" ClientInstanceName="gvwDocument" runat="server" AutoGenerateColumns="False" Theme="Office2010Silver" Width="100%" OnCustomColumnDisplayText="gvwtblDocument_OnCustomColumnDisplayText" OnPageIndexChanged="gvwtblDocument_PageIndexChanged" KeyFieldName="MainGuid" meta:resourcekey="gvwtblDocumentResource2">
                                                <ClientSideEvents CustomButtonClick="gvwDocument_CustomButtonClick" />
                                                <Columns>
                                                   
                                                    <dx:GridViewDataTextColumn Caption="ID" FieldName="MainGuid" ShowInCustomizationForm="True" Visible="False" meta:resourcekey="GridViewDataTextColumnResource13">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="HRActivity" ShowInCustomizationForm="True" VisibleIndex="1" Caption="የሰው ሀብት ክንውን ዓይነት" meta:resourcekey="GridViewDataTextColumnResource140">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="code" ShowInCustomizationForm="True" VisibleIndex="2" Caption="መለያ" meta:resourcekey="GridViewDataTextColumnResource14">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="Description" ShowInCustomizationForm="True" VisibleIndex="3" Caption="የሰነድ አይነት በእንግሊዘኛ" meta:resourcekey="GridViewDataTextColumnResource15">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DescriptionAm" ShowInCustomizationForm="True" VisibleIndex="4" Caption="የሰነድ አይነት በክልሉ ቋንቋ" meta:resourcekey="GridViewDataTextColumnResource16">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="ParentGuids" ShowInCustomizationForm="True" VisibleIndex="5" Visible="False" meta:resourcekey="GridViewDataTextColumnResource17">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="Isoptional" ShowInCustomizationForm="True" VisibleIndex="6" Caption="የግድ መረጋገጥ ይገባዋል" meta:resourcekey="GridViewDataTextColumnResource18">
                                                    </dx:GridViewDataTextColumn>
                                                     <dx:GridViewCommandColumn ShowInCustomizationForm="True" Width="100px" VisibleIndex="7">
                                                        <CustomButtons>
                                                            <dx:GridViewCommandColumnCustomButton ID="Edit" Text="አርም" meta:resourcekey="GridViewCommandColumnCustomButtonResource3"></dx:GridViewCommandColumnCustomButton>
                                                            <dx:GridViewCommandColumnCustomButton ID="Del" Text="ሰርዝ" meta:resourcekey="GridViewCommandColumnCustomButtonResource4"></dx:GridViewCommandColumnCustomButton>
                                                        </CustomButtons>
                                                    </dx:GridViewCommandColumn>
                                                </Columns>
                                                <SettingsPager>
                                                    <Summary Text="ገፅ የሰነድ ማረጋገጪያ {0} of {1} ({2} ዝርዝር)" />
                                                </SettingsPager>
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:ASPxPopupControl ID="PopupDocument" runat="server" ClientInstanceName="PopupDocument" Font-Names="Visual Geez Unicode" Font-Size="Small" HeaderText="Delete Confirmation" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" meta:resourcekey="PopupDocumentResource2">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" meta:resourcekey="PopupControlContentControl1Resource2">
                                                        <uc1:ConfirmBox runat="server" ID="ConfirmBox" />
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                            </dx:ASPxPopupControl>

                                        </td>
                                    </tr>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxCallbackPanel>
                </dx:PanelContent>
            </PanelCollection>

        </dx:ASPxRoundPanel>


    </div>
</asp:Content>

