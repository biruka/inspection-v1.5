﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Bussiness;
using CUSTOR.Domain;
using System.Text;
using System.Threading;
using DevExpress.Web;
using CUSTOR.Commen;

public partial class DocumentType : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Session["IsNew"] = false;
            BindGrid();
            pnlDocumentTypeForm.ClientVisible = false;
            btnSave.Enabled = false;
        }
    }

    protected override void InitializeCulture()
    {
        ProfileCommon p = this.Profile;
        if (p.Organization.LanguageID != null)
        {
            String selectedLanguage = string.Empty;
            switch (p.Organization.LanguageID)
            {
                case 0: selectedLanguage = "en-US";//English
                    break;
                case 2: selectedLanguage = "am-ET";//Afan Oromo
                    break;
                case 3: selectedLanguage = "am-ET"; //Tig
                    break;
                case 4: selectedLanguage = "en-GB";//afar
                    break;
                case 5: selectedLanguage = "en-AU";//Somali
                    break;
                default: break;//Amharic
            }
            UICulture = selectedLanguage;
            Culture = selectedLanguage;

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(selectedLanguage);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(selectedLanguage);
        }
        base.InitializeCulture();
    }

    private void BindGrid()
    {
        try
        {
            tblDocumentTypeBussiness objtblDocumentTypeBussiness = new tblDocumentTypeBussiness();
            gvwtblDocumentType.DataSource = objtblDocumentTypeBussiness.GettblDocumentTypes();
            gvwtblDocumentType.DataBind();
        }
        catch
        {
        }
    }

    private void SearchDocumentTypes()
    {
        try 
        {
            if ((txtSearchOrgEnglish.Text != string.Empty) || (txtSearchOrgRegionalLanguage.Text != string.Empty))
            {
                bool isGeez = false;
                string organizationRegionalLanguage = "";
                tblDocumentTypeBussiness objtblDocumentTypeBussiness = new tblDocumentTypeBussiness();
                if(txtSearchOrgRegionalLanguage.Text != string.Empty)
                {
                    ProfileCommon objProfile = this.Profile;
                    objProfile = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
                    if (objProfile.Organization.LanguageID == (int)Language.eLanguage.eAmharic ||
                        objProfile.Organization.LanguageID == (int)Language.eLanguage.eTigrigna)
                    {
                        isGeez = true;
                        CVGeez objGeez = new CVGeez();
                        organizationRegionalLanguage = objGeez.GetSortValueU(txtSearchOrgRegionalLanguage.Text);
                    }
                    else
                    {
                        organizationRegionalLanguage = txtSearchOrgRegionalLanguage.Text;
                    }
                }
                gvwtblDocumentType.DataSource = objtblDocumentTypeBussiness.SearchDocumentTypes(txtSearchOrgEnglish.Text, organizationRegionalLanguage, isGeez);
                gvwtblDocumentType.DataBind();
            }
            else if ((txtSearchOrgEnglish.Text == string.Empty) && (txtSearchOrgRegionalLanguage.Text == string.Empty))
            {
                tblDocumentTypeBussiness objtblDocumentTypeBussiness = new tblDocumentTypeBussiness();
                gvwtblDocumentType.DataSource = objtblDocumentTypeBussiness.GettblDocumentTypes();
                gvwtblDocumentType.DataBind();
            }
        }
        catch
        {
        }
    }

    private void FillRegion(DevExpress.Web.ASPxComboBox cboRegion)
    {
        tblAddressBussiness objAdd = new tblAddressBussiness();
        cboRegion.DataSource = objAdd.GettblAddresss();
        cboRegion.TextField = "Description";
        cboRegion.ValueField = "Region";
        cboRegion.DataBind();

        cboRegion.Items.Insert(0, new ListEditItem("ፌዴራል", "99"));
        cboRegion.Text = "99";
    }

    protected void mnuToolbar_MenuItemClick(object sender, MenuEventArgs e)
    {
        if (e.Item.Value.ToLower() == "new")
        {
            DoNew();
            //mnuToolbar.Items[(int)enumToolbar.eNew].Enabled = false;
            //mnuToolbar.Items[(int)enumToolbar.eSave].Enabled = true;
        }
        else if (e.Item.Value.ToLower() == "save")
        {
            Page.Validate();
            if(!Page.IsValid) return;
            if(DoSave())
            {
                //mnuToolbar.Items[(int)enumToolbar.eNew].Enabled = true;
                //mnuToolbar.Items[(int)enumToolbar.eSave].Enabled = false;
                //mnuToolbar.Items[(int)enumToolbar.eDelete].Enabled = false;
            }
            }
        else if (e.Item.Value.ToLower() == "delete")
        {
            if(DoDelete())
            {
                //mnuToolbar.Items[(int)enumToolbar.eNew].Enabled = true;
                //mnuToolbar.Items[(int)enumToolbar.eSave].Enabled = false;
                //mnuToolbar.Items[(int)enumToolbar.eDelete].Enabled = false;
            }
        }
        else if (e.Item.Value.ToLower() == "refresh")
        {
            ClearForm();
            //mnuToolbar.Items[(int)enumToolbar.eNew].Enabled = true;
            //mnuToolbar.Items[(int)enumToolbar.eSave].Enabled = false;
            //mnuToolbar.Items[(int)enumToolbar.eDelete].Enabled = false;
        }
    }

    private bool IsDocumentCreatedForHRACtivity(Guid mainGuid)
    {
        bool areDocumentsCreated = false;

        tblDocumentBussiness objDocument = new tblDocumentBussiness();
        if (objDocument.GetRecordsByDocumentTypeGuid(mainGuid).Rows.Count > 0)
        {
            areDocumentsCreated = true;
        }

        return areDocumentsCreated;
    }

    protected bool DoDelete()
    {
        bool deleted = false;
        tblDocumentTypeBussiness objtblDocumentTypeBussiness = new tblDocumentTypeBussiness();
        try
        { 
            Guid docGuid = (Guid)Session["ID"];
            //Check whether there are no documents created for the selected HR activity
            if (IsDocumentCreatedForHRACtivity(docGuid))
            {
                ShowError(Resources.Message.MSG_DocumentsCreatedInHRActivity);
            }
            else
            {
                if (objtblDocumentTypeBussiness.Delete(docGuid))
                {
                    deleted = true;
                    Session["IsNew"] = false;
                    pnlDocumentTypeForm.ClientVisible = false;
                    BindGrid();
                    ShowSuccess(Resources.Message.MSG_DocumentDeleted);
                }
            }
        }
        catch(Exception ex)
        {
            deleted = false;
        }
        return deleted;
    }

    protected bool DoFind(Guid ID)
    {
        tblDocumentTypeBussiness objtblDocumentTypeBussiness = new tblDocumentTypeBussiness();

        tblDocumentType objtblDocumentType= new tblDocumentType();
        btnSave.Enabled = true;
        //mnuToolbar.Items[(int)enumToolbar.eSave].Enabled = false;
        //mnuToolbar.Items[(int)enumToolbar.eDelete].Enabled = false;
        try
        {
            objtblDocumentType = objtblDocumentTypeBussiness.GettblDocumentType(ID);
            if (objtblDocumentType == null)
            {
                //ShowMessage("No record was found.");
                return false;
            }
            else
            {
                //Now Record was found;
                Session["IsNew"] = false;
                ClearForm();
                pnlDocumentTypeForm.ClientVisible = true;
                FillRegion(cboReulatoryBody);
                //this.cboMainGuid.SelectedValue=objtblDocumentType.MainGuid.ToString();
                this.txtDescription.Text = objtblDocumentType.Description;
                this.txtDescription.Text = objtblDocumentType.Description.ToString();
                //this.txtDescriptionAm.Text=objtblDocumentType.DescriptionAm;
                this.txtDescriptionAm.Text = objtblDocumentType.DescriptionAm.ToString();
                this.cboReulatoryBody.Value = objtblDocumentType.ReulatoryBody.ToString();
                this.txtCode.Text = objtblDocumentType.Code.ToString();
                lblCode.Visible = true;
                txtCode.Visible = true;
                Session["ID"] = ID;
                cbDocumentType.JSProperties["cpAction"] = "documentTypeloaded";
                cbDocumentType.JSProperties["cpStatus"] = "Success";
                return true;
            }
        }
        catch (Exception ex)
        {
            //ShowError(ex.Message);
            return false;
        }
    }

    protected void ClearForm()
    {
            //this.cboMainGuid.SelectedIndex=0;
            this.txtDescription.Text=string.Empty;
            this.txtDescriptionAm.Text=string.Empty;
            this.cboReulatoryBody.Text=string.Empty;
            this.txtCode.Text = string.Empty;
        }

    protected bool DoNew()
    {
        try
        {
            Session["IsNew"]=true;
            pnlDocumentTypeForm.ClientVisible=true;
            ClearForm();
            FillRegion(cboReulatoryBody);                
            btnSave.Enabled = true;
            cbDocumentType.JSProperties["cpAction"] = "FormReseted";
            cbDocumentType.JSProperties["cpStatus"] = "SUCCESS";
            documentTypeSearch.ClientVisible = false;
            documentTypeList.ClientVisible = false;
            return true;
        }
        catch(Exception ex)
        {
            //ShowError(ex.Message);
            return false;
        }
    }

    protected bool DoSave()
    {
        tblDocumentType objtblDocumentType = new tblDocumentType();
        try
        {
            List<string> errMessages = GetErrorMessage();
            if (errMessages.Count > 0)
            {
                ShowError(GetErrorDisplay(errMessages));
                return false;
            }
            else
            {
                FillRegion(cboReulatoryBody);
                tblDocumentTypeBussiness objtblDocumentTypeBussiness = new tblDocumentTypeBussiness();
                if ((bool)Session["IsNew"])
                {
                    #region 
                    bool isGeez = false;
                    string organizationRegionalLanguage = "";
                    ProfileCommon objProfile = this.Profile;
                    objProfile = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
                    if (objProfile.Organization.LanguageID == (int)Language.eLanguage.eAmharic ||
                        objProfile.Organization.LanguageID == (int)Language.eLanguage.eTigrigna)
                    {
                        isGeez = true;
                        CVGeez objGeez = new CVGeez();
                        organizationRegionalLanguage = objGeez.GetSortValueU(txtDescriptionAm.Text);
                    }
                    else
                    {
                        organizationRegionalLanguage = txtSearchOrgRegionalLanguage.Text;
                    }
                    if (objtblDocumentTypeBussiness.Exists(this.txtDescription.Text, organizationRegionalLanguage, isGeez))
                    {
                        cbDocumentType.JSProperties["cpAction"] = "existance";
                        cbDocumentType.JSProperties["cpMessage"] = Resources.Message.MSG_DocumentTypeExisted;
                        cbDocumentType.JSProperties["cpStatus"] = "ERROR";
                        pnlDocumentTypeForm.ClientVisible = true;
                        documentTypeSearch.ClientVisible = false;
                        documentTypeList.ClientVisible = false;
                        btnSave.ClientEnabled = true;
                    }
                    else
                    {
                        objtblDocumentType.MainGuid = Guid.NewGuid();
                        objtblDocumentType.Code = this.txtCode.Text;
                        objtblDocumentType.Description = this.txtDescription.Text;
                        objtblDocumentType.DescriptionAm = this.txtDescriptionAm.Text;
                        objtblDocumentType.ReulatoryBody = Convert.ToInt32(this.cboReulatoryBody.Value);
                        objProfile = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
                        if (objProfile.Organization.LanguageID == (int)Language.eLanguage.eAmharic ||
                            objProfile.Organization.LanguageID == (int)Language.eLanguage.eTigrigna)
                        {
                            CVGeez objGeez = new CVGeez();
                            objtblDocumentType.DescriptionSortValue = objGeez.GetSortValueU(txtDescriptionAm.Text);
                            objtblDocumentType.DescriptionSoudx = objGeez.GetSoundexValue(txtDescriptionAm.Text);
                        }
                        else
                        {
                            objtblDocumentType.DescriptionSortValue = "";
                            objtblDocumentType.DescriptionSoudx = "";
                        }
                        if (objtblDocumentTypeBussiness.InserttblDocumentType(objtblDocumentType))
                        {
                            cbDocumentType.JSProperties["cpStatus"] = "Success";
                            cbDocumentType.JSProperties["cpAction"] = "registration";
                            cbDocumentType.JSProperties["cpMessage"] = Resources.Message.MSG_DocumentsRegistered;
                        }
                        else
                        {
                            cbDocumentType.JSProperties["cpStatus"] = "Error";
                            cbDocumentType.JSProperties["cpAction"] = "registration";
                            cbDocumentType.JSProperties["cpMessage"] = Resources.Message.MSG_DocumentsNotRegistered;
                        }
                    }

                    #endregion
                }
                else
                {
                    objtblDocumentType.MainGuid = (Guid)Session["ID"];//Attention
                    objtblDocumentType.Description = this.txtDescription.Text;
                    objtblDocumentType.DescriptionAm = this.txtDescriptionAm.Text;
                    if(objtblDocumentTypeBussiness.UpdatetblDocumentType(objtblDocumentType))
                    {
                        cbDocumentType.JSProperties["cpStatus"] = "Success";
                        cbDocumentType.JSProperties["cpAction"] = "update";
                        cbDocumentType.JSProperties["cpMessage"] = Resources.Message.MSG_DocumentsUpdated;
                    }
                }
            }
            Session["IsNew"]=false;
            pnlDocumentTypeForm.ClientVisible = false;
            BindGrid();
            //GETH: Dec 28 2017
            //ShowSuccess(Resources.Message.MSG_SAVED);
            return true;
        }
        catch(Exception ex)
        {
            ShowError(ex.Message);
            return false;
        }
    }

    protected void gvwtblDocumentType_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        tblDocumentType objtblDocumentType = new tblDocumentType();

        try
        {
            int index = e.RowIndex;
            //int intID = Convert.ToInt32(this.gvwtblDocumentType.Rows[index].Cells[0].Text);//to-do check this code
            //Session["ID"]=intID;
            DoDelete();
            BindGrid();
        }
        catch(Exception ex)
        {
            //ShowError(ex.Message);
        }
    }

    protected void gvwtblDocumentType_PageIndexChanging(Object sender, GridViewPageEventArgs e)
    {
        if ((e.NewPageIndex >= 0) & (e.NewPageIndex <= gvwtblDocumentType.PageCount))
        {
            gvwtblDocumentType.PageIndex = e.NewPageIndex;
            BindGrid();
        }
    }

    protected void gvwtblDocumentType_RowEditing(Object sender, GridViewEditEventArgs e)
    {
        try
        {
            //Label lbl = (Label)gvwtblDocumentType.Rows[e.NewEditIndex].FindControl(lblMainGuid);
            //DoFind((Guid)lbl.Text));
            //e.NewEditIndex = -1;
        }
        catch (Exception ex)
        {
            //ShowError(ex.Message);
        }
    }
      
    private void ShowError(string strMsg)
    {
        cbDocumentType.JSProperties["cpMessage"] = strMsg;
        cbDocumentType.JSProperties["cpStatus"] = "ERROR";
    }

    private void ShowSuccess(string strMsg)
    {
        cbDocumentType.JSProperties["cpMessage"] = strMsg;
        cbDocumentType.JSProperties["cpStatus"] = "SUCCESS";
    }

    private void ShowMessage(string strMsg)
    {
        cbDocumentType.JSProperties["cpMessage"] = strMsg;
        cbDocumentType.JSProperties["cpStatus"] = "INFO";
    }

    private string GetErrorDisplay(List<string> strMessages)
    {

        StringBuilder sb = new StringBuilder();
        if (strMessages.Count == 0) return string.Empty;
        sb.Append(Resources.Message.MSG_CVALD);
        sb.Append("<ul>");
        foreach (string s in strMessages)
        {
            sb.Append(String.Format("<li>{0}</li>", s));
        }
        sb.Append("</ul>");

        return sb.ToString();

    }

    private List<string> GetErrorMessage()
    {
        List<string> errMessages = new List<string>();

        if (int.Parse(Session["ProLanguage"].ToString()) == 1)
        {
            if (txtDescription.Text.Trim().Length == 0)
            {
                errMessages.Add("እባክዎ ስሜውን ያስገቡ");

            }
            if (txtDescriptionAm.Text.Trim().Length == 0)
            {
                errMessages.Add("እባክዎ ስሜውን በክልሉ ቋንቋ ያስገቡ");

            }
        }
        else if (int.Parse(Session["ProLanguage"].ToString()) == 0)
        {
            if (txtDescription.Text.Trim().Length == 0)
            {
                errMessages.Add("Please Insert Description");

            }
            if (txtDescriptionAm.Text.Trim().Length == 0)
            {
                errMessages.Add("Please Insert Regional Description");
            }
        }
        return errMessages;
    }
       
    protected void cbDocumentType_Callback1(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {
        cbDocumentType.JSProperties["cpStatus"] = "";
        string strID = string.Empty;
        string[] parameters = e.Parameter.Split('|');
        string strParam = parameters[0].ToString();
        if (e.Parameter.ToString().Contains('|'))
            strID = parameters[1].ToString();

        switch (strParam)
        {
            case "New":
                DoNew();
                break;
            case "Edit":
                DoFind(Guid.Parse(strID));
                Session["ID"] = new Guid(strID);
                break;
            case"Show":
                BindGrid();
                break;
            case "Search":
                SearchDocumentTypes();
                break;
            case "Save":
                DoSave();
                break;
            case "Delete":
                if (strID.Length > 0)
                    Session["ID"] = new Guid(strID);
                DoDelete();
                break;
            case "Refresh":
                //BindGrid();
                //cbData.JSProperties["cpStatus"] = "REFRESH";
                //cbData.JSProperties["cpAction"] = "refresh";
                break;
            case "Attach":
                //DoAttach();
                break;
            default:
                break;

        }
    }

    protected void gvwtblDocumentType_PageIndexChanged(object sender, EventArgs e)
    {
        BindGrid();
    }
}
