﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Configuration;
using System.Globalization;
using CUSTOR.Domain;
using CUSTOR.DataAccess;
using CUSTOR.Commen;
using CUSTORDatePicker;
using CUSTOR.Bussiness;
using DevExpress.Web;
using System.Net;
using System.Threading;
using DevExpress.Web.ASPxHtmlEditor;

namespace UI.Pages
{
    public partial class Complain : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Session["ProLanguage"] == null)
                {
                    return;
                }
                else
                {
                    HidlLanguage.Value = Session["ProLanguage"].ToString();
                    Session["Language"] = Session["ProLanguage"].ToString();
                }
                
                txtDateFrom.SelectedDate = Convert.ToString((EthiopicDateTime.GetEthiopicDate(int.Parse(DateTime.Today.Day.ToString()), int.Parse(DateTime.Today.Month.ToString()), int.Parse(DateTime.Today.Year.ToString()))));
                txtDateTo.SelectedDate = Convert.ToString((EthiopicDateTime.GetEthiopicDate(int.Parse(DateTime.Today.Day.ToString()), int.Parse(DateTime.Today.Month.ToString()), int.Parse(DateTime.Today.Year.ToString()))));
                //txtFindingSummaryDate
                //txtFindingSummaryDate.SelectedDate = Convert.ToString((EthiopicDateTime.GetEthiopicDate(int.Parse(DateTime.Today.Day.ToString()), int.Parse(DateTime.Today.Month.ToString()), int.Parse(DateTime.Today.Year.ToString()))));
                LoadComplainTypes();
                if (Session["UserID"] != null)
                {
                    HidUserid.Value = Session["UserID"].ToString();
                    LoadOrganizations(Guid.Parse(Session["ProfileOrgGuid"].ToString()), Session["ProfileOrgCatagory"].ToString(), int.Parse(HidlLanguage.Value));
                }
                LoadPageProperties();
               // upnlCompGrid.Update();
                pageControl.Visible = false;
                //upnlTabs.Update();
                //
                LoadDescsionType();

            }
        }

        protected override void InitializeCulture()
        {
            ProfileCommon p = this.Profile;
            if (p.Organization.LanguageID != null)
            {
                String selectedLanguage = string.Empty;
                switch (p.Organization.LanguageID)
                {
                    case 0: selectedLanguage = "en-US";//English
                        break;
                    case 2: selectedLanguage = "am-ET";//Afan Oromo
                        break;
                    case 3: selectedLanguage = "am-ET"; //Tig
                        break;
                    case 4: selectedLanguage = "en-GB";//afar
                        break;
                    case 5: selectedLanguage = "en-AU";//Somali
                        break;
                    default: break;//Amharic
                }
                UICulture = selectedLanguage;
                Culture = selectedLanguage;

                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(selectedLanguage);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(selectedLanguage);
            }
            base.InitializeCulture();
        }

        private void BindGrid()
        {
            try
            {
                DateTime dtfrom = txtDateFrom.SelectedGCDate;
                DateTime dtto = txtDateTo.SelectedGCDate;
                DataTable dt = new DataTable();
                ComplaintBusiness objcompliantInformation = new ComplaintBusiness();
                if (AreUserInputsValid())
                {
                    //pnlCompliantList1.Visible = true;
                    ////     ASPxRoundPanel2.Visible = true;
                    //pnlCompliantDetail.Visible = pnlFindingSummary.Visible = false;
                    ComplaintBusiness compliantInformation = new ComplaintBusiness();
                    DateTime reportedDateFrom = (dtfrom.Equals("") ? new DateTime() : dtfrom);
                    DateTime reportedDateTo = (dtto.Equals("") ? new DateTime() : dtto);
                    //Get selected organization
                    if (HidlLanguage.Value == "0")
                    {
                        Guid orgGuid = !cboOrganization.SelectedItem.Text.Equals("--Select--") ? Guid.Parse(cboOrganization.SelectedItem.Value.ToString()) : Guid.Empty;
                        //Get selected complain Type 
                        int complainType = !cboComplainType.SelectedItem.Text.Equals("--Select--") ? Convert.ToInt32(cboComplainType.SelectedItem.Value.ToString()) : -1;
                        //Get selected descision Type 

                        //Check whether page mode type is grievance or not
                        bool isGrievance = (hdfldPageMode.Value.Equals("ቅሬታዎች") ? true : false);
                        //Search compliants
                        DataTable dtCompliant = compliantInformation.SearchCompliant(orgGuid, complainType, reportedDateFrom, reportedDateTo, isGrievance, Convert.ToInt32(HidlLanguage.Value));
                        DvgrdDecision.DataSource = dtCompliant;
                        DvgrdDecision.DataBind();
                    }
                    else if (HidlLanguage.Value == "1")
                    {
                        Guid orgGuid = !cboOrganization.SelectedItem.Text.Equals("ይምረጡ") ? Guid.Parse(cboOrganization.SelectedItem.Value.ToString()) : Guid.Empty;
                        //Get selected complain Type 
                        int complainType = !cboComplainType.Text.Equals("ይምረጡ") ? Convert.ToInt32(cboComplainType.SelectedItem.Value.ToString()) : -1;
                        //Get selected descision Type 
                        //int complainType = -1;
                        //Check whether page mode type is grievance or not
                        bool isGrievance = (hdfldPageMode.Value.Equals("ቅሬታዎች") ? true : false);
                        //Search compliants
                        DataTable dtCompliant = compliantInformation.SearchCompliant(orgGuid, complainType, reportedDateFrom, reportedDateTo, isGrievance, Convert.ToInt32(HidlLanguage.Value));
                        DvgrdDecision.DataSource = dtCompliant;
                        DvgrdDecision.DataBind();
                    }
                    else if (HidlLanguage.Value == "2")
                    {
                        Guid orgGuid = !cboOrganization.SelectedItem.Value.Equals("-1") ? Guid.Parse(cboOrganization.SelectedItem.Value.ToString()) : Guid.Empty;
                        //Get selected complain Type 
                        int complainType = !cboComplainType.Value.Equals("-1") ? Convert.ToInt32(cboComplainType.SelectedItem.Value.ToString()) : -1;
                        //Get selected descision Type 
                        //int complainType = -1;
                        //Check whether page mode type is grievance or not
                        bool isGrievance = (hdfldPageMode.Value.Equals("ቅሬታዎች") ? true : false);
                        //Search compliants
                        DataTable dtCompliant = compliantInformation.SearchCompliant(orgGuid, complainType, reportedDateFrom, reportedDateTo, isGrievance, Convert.ToInt32(HidlLanguage.Value));
                        DvgrdDecision.DataSource = dtCompliant;
                        DvgrdDecision.DataBind();
                    }

                    //DvgrdDecision.Enabled = pnlShowDeleteButton.Visible = pnlCompliantList1.Visible = dtCompliant.Rows.Count > 0;
                    Session["CompliantList"] = (Object)DvgrdDecision;
                }
                //dt = objcompliantInformation.GetComplaintDetail(Convert.ToBoolean(Session["Grievance"]));                   

                //if (dt.Rows.Count > 0)
                //{
                //    DvgrdDecision.DataSource = dt;
                //    DvgrdDecision.DataBind();
                //}

                //else
                //{
                //    DvgrdDecision.Visible = false;
                //}

            }
            catch { }
        }

        protected bool IsValidDecisionRecord()
        {
            List<string> errMessages = GetErrorMessaages();
            if (errMessages.Count > 0)
            {
                MessageBox1.ShowError(errMessages.ToString());
                return false;
            }

            return true;
        }

        protected void LoadPageProperties()
        {
            cmdSearch.ToolTip = "ፈልግ";
            //cmdAddNew.ToolTip = "አዲስ ጨምር";
            //cmdDelete.ToolTip = "አጥፋ";
            switch (Request.QueryString["ModeType"])
            {

                case "Allude":
                    //this.Page.Title = lblComplains.Text = "የጥቆማዎች";
                    if (HidlLanguage.Value == "0")
                    {
                        hdfldPageMode.Value = "Complain";
                        lblComplainType.Text = "Complain Type: ";
                        DvgrdDecision.Columns[0].Caption = "Full Name";
                        DvgrdDecision.Columns[3].Caption = "Complain type";
                    }
                    else if (HidlLanguage.Value == "1")
                    {
                        hdfldPageMode.Value = "ጥቆማዎች";
                        lblComplainType.Text = "ጥቆማ የቀረበበት የሰው ሀብት ክንውን : ";
                        DvgrdDecision.Columns[0].Caption = "ጥቆማ አቅራቢ";
                        DvgrdDecision.Columns[3].Caption = "የጥቆማው አይነት";
                    }
                    else if (HidlLanguage.Value == "2")
                    {
                        hdfldPageMode.Value = "ጥቆማዎች";
                        lblComplainType.Text = "Gosa Saaxilaa: ";
                        DvgrdDecision.Columns[0].Caption = "Heyyata Dhi`eesa";
                        DvgrdDecision.Columns[3].Caption = "Gosa Saaxilaa";
                        DvgrdDecision.Columns[1].Caption = "Dhaabbata";
                        DvgrdDecision.Columns[4].Caption = "Murtee Kename";
                        DvgrdDecision.Columns[2].Caption = "Guyyaa";
                    }

                    break;
                case "Grievance":
                    //this.Page.Title = lblComplains.Text = "የቅሬታዎች";
                    if (HidlLanguage.Value == "0")
                    {
                        hdfldPageMode.Value = "Grievance";
                        lblComplainType.Text = "Grievance Type: ";
                        DvgrdDecision.Columns[0].Caption = "Full Name";
                        DvgrdDecision.Columns[3].Caption = "Grievance type";
                    }
                    else if (HidlLanguage.Value == "1")
                    {
                        hdfldPageMode.Value = "ቅሬታዎች";
                        lblComplainType.Text = "የቅሬታው አይነት: ";
                        DvgrdDecision.Columns[0].Caption = "የቅሬታው አቅራቢ";
                        DvgrdDecision.Columns[3].Caption = "የቅሬታው አይነት";
                    }
                    else if (HidlLanguage.Value == "2")
                    {
                        hdfldPageMode.Value = "ቅሬታዎች";
                        lblComplainType.Text = "Gosa Iyyaata: ";
                        DvgrdDecision.Columns[0].Caption = "Kan Heyyataa Dheyeesee";
                        DvgrdDecision.Columns[3].Caption = "Gosa Heyyanoo";
                        DvgrdDecision.Columns[1].Caption = "Dhaabbata";
                        DvgrdDecision.Columns[4].Caption = "Murtee Kename";
                        DvgrdDecision.Columns[2].Caption = "Guyyaa";
                    }
                    break;
            }
        }

        private void FillCombo(ASPxComboBox ddl, Language.eLanguage eLang, Type t)
        {
            ddl.TextField = "Value";
            ddl.ValueField = "Key";
            ddl.DataSource = EnumHelper.GetEnumDescriptions(t, eLang);
            ddl.DataBind();
            if (HidlLanguage.Value == "1")
            {
                ListEditItem le = new ListEditItem("ይምረጡ", "-1");
                ddl.Items.Insert(0, le);
                ddl.SelectedIndex = 0;
            }
            else if (HidlLanguage.Value == "0")
            {
                ListEditItem le = new ListEditItem("select", "-1");
                ddl.Items.Insert(0, le);
                ddl.SelectedIndex = 0;
            }
            else if (HidlLanguage.Value == "2")
            {
                ListEditItem le = new ListEditItem("Filadha", "-1");
                ddl.Items.Insert(0, le);
                ddl.SelectedIndex = 0;
            }

        }

        protected void LoadComplainTypes()
        {
            if (HidlLanguage.Value == "0")
            {
                FillCombo(cboComplainType, Language.eLanguage.eEnglish, typeof(Enumeration.ComplainTypes));
            }
            else if (HidlLanguage.Value == "1")
            {
                FillCombo(cboComplainType, Language.eLanguage.eAmharic, typeof(Enumeration.ComplainTypes));
            }
            else if (HidlLanguage.Value == "2")
            {
                FillCombo(cboComplainType, Language.eLanguage.eAfanOromo, typeof(Enumeration.ComplainTypes));
            }

        }

        protected void LoadOrganizations()
        {
            if (Session["UserID"] != null)
            {
                HidUserid.Value = Session["UserID"].ToString();
                //LoadOrganizations(HidUserid.Value);

            }
            OrganizationBussiness obj = new OrganizationBussiness();
            cboOrganization.ValueField = "OrgGuid";
            cboOrganization.TextField = "DescriptionAm";
            cboOrganization.DataSource = obj.GetOrganizations();
            cboOrganization.DataBind();
            ListEditItem le = new ListEditItem("ይምረጡ", "-1");
            cboOrganization.Items.Insert(0, le);
            cboOrganization.SelectedIndex = 0;

        }

        protected bool AreUserInputsValid()
        {
            bool userInputsAreValid = true;
            DateTime reportedDateFrom = (txtDateFrom.SelectedDate.Equals("") ? new DateTime() : Convert.ToDateTime(txtDateFrom.SelectedGCDate));
            DateTime reportedDateTo = (txtDateTo.SelectedDate.Equals("") ? new DateTime() : Convert.ToDateTime(txtDateTo.SelectedGCDate));
            if (!txtDateTo.SelectedDate.Equals("") && reportedDateFrom > reportedDateTo)
            {
                MessageBox1.ShowInfo(Resources.Message.MSG_VALIDDATE);
                userInputsAreValid = false;
            }

            return userInputsAreValid;
        }

        protected void GetActiveOrganizations()
        {
            try
            {
                OrganizationBussiness obj = new OrganizationBussiness();
                cboOrganization.ValueField = "OrgGuid";
                cboOrganization.TextField = "DescriptionAm";
                cboOrganization.DataSource = obj.GetActiveOrganizations();
                cboOrganization.DataBind();
            }
            catch (Exception ex)
            {
                MessageBox1.ShowError(ex.ToString());
            }

        }

        protected void LoadOrganizations(Guid OrganizationGuid, string OrganizationCatagory, int OrganizationLanguage)
        {
            //if (HttpContext.Current.User.Identity.IsAuthenticated)
            //{
            //    ProfileCommon objProfile = this.Profile;
            //    objProfile = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
            //    Session["OrgLanguage"] = objProfile.Organization.Language;
            //    if (!HttpContext.Current.User.IsInRole("HR Officer") && !HttpContext.Current.User.IsInRole("Inspection Director") && !HttpContext.Current.User.IsInRole("Inspection Data Encoder"))
            //    {
            //        InspectionFollowupBussiness objOrganizations = new InspectionFollowupBussiness();
            //        DataTable dt = objOrganizations.getEmployeeInspectedOrganizations(Guid.Parse(objProfile.Staff.GUID));
            //        //check whethere the logged in user can view approve button
            //        Session["loggedInUser"] = Guid.Parse(objProfile.Staff.GUID).ToString();
            //        cboOrganization.ValueField = "OrgGuid";
            //        cboOrganization.TextField = "DescriptionAm";
            //        cboOrganization.DataSource = dt;
            //        cboOrganization.DataBind();
            //        if (dt.Rows.Count > 1)
            //        {
            //            if (OrganizationLanguage.ToString() == "0")
            //            {
            //                ListEditItem le = new ListEditItem("--Select--", "-1");
            //                cboOrganization.Items.Insert(0, le);
            //            }
            //            if (OrganizationLanguage.ToString() == "1")
            //            {
            //                ListEditItem le = new ListEditItem("ይምረጡ", "-1");
            //                cboOrganization.Items.Insert(0, le);
            //            }
            //            else if (OrganizationLanguage.ToString() == "2")
            //            {
            //                ListEditItem le = new ListEditItem("--mret--", "-1");
            //                cboOrganization.Items.Insert(0, le);
            //            }
            //            cboOrganization.SelectedIndex = 0;
            //        }
            //        else
            //        {
            //            cboOrganization.DataSource = dt;
            //            cboOrganization.DataBind();
            //            cboOrganization.SelectedIndex = 0;
            //        }
            //    }
            //    else if (CAccessRight.CanAccess(CResource.eResource.ehrofficer))
            //    {
            //        List<Organization> objorg = new List<Organization>();
            //        OrganizationBussiness objorgBusiness = new OrganizationBussiness();
            //        objorg = objorgBusiness.GetListOrg(Guid.Parse(objProfile.Organization.GUID));
            //        cboOrganization.ValueField = "OrgGuid";
            //        cboOrganization.TextField = "DescriptionAm";
            //        if (objorg.Count > 1)
            //        {
            //            cboOrganization.DataSource = objorg;
            //            cboOrganization.DataBind();
            //            if (OrganizationLanguage.ToString() == "0")
            //            {
            //                ListEditItem le = new ListEditItem("--Select--", "-1");
            //                cboOrganization.Items.Insert(0, le);
            //            }
            //            if (OrganizationLanguage.ToString() == "1")
            //            {
            //                ListEditItem le = new ListEditItem("ይምረጡ", "-1");
            //                cboOrganization.Items.Insert(0, le);
            //            }
            //            else if (OrganizationLanguage.ToString() == "2")
            //            {
            //                ListEditItem le = new ListEditItem("--mret--", "-1");
            //                cboOrganization.Items.Insert(0, le);
            //            }
            //            cboOrganization.SelectedIndex = 0;
            //        }
            //        else
            //        {
            //            cboOrganization.SelectedIndex = 0;
            //            cboOrganization.DataSource = objorg;
            //            cboOrganization.DataBind();
            //        }
            //    }

            //    else if (HttpContext.Current.User.IsInRole("Inspection Director"))
            //    {
            //        //Load all list of Organizations
            //        List<Organization> objorg = new List<Organization>();
            //        OrganizationBussiness objorgBusiness = new OrganizationBussiness();
            //        objorg = objorgBusiness.GetActiveOrganizationsForInspectionTeam(Guid.Parse(objProfile.Organization.GUID));
            //        if (objorg.Count > 1)
            //        {
            //            cboOrganization.ValueField = "OrgGuid";
            //            cboOrganization.TextField = "DescriptionAm";
            //            cboOrganization.DataSource = objorg;
            //            cboOrganization.DataBind();
            //            if (OrganizationLanguage.ToString() == "0")
            //            {
            //                ListEditItem le = new ListEditItem("--Select--", "-1");
            //                cboOrganization.Items.Insert(0, le);
            //            }
            //            if (OrganizationLanguage.ToString() == "1")
            //            {
            //                ListEditItem le = new ListEditItem("ይምረጡ", "-1");
            //                cboOrganization.Items.Insert(0, le);
            //            }
            //            else if (OrganizationLanguage.ToString() == "2")
            //            {
            //                ListEditItem le = new ListEditItem("--mret--", "-1");
            //                cboOrganization.Items.Insert(0, le);
            //            }

            //            cboOrganization.SelectedIndex = 0;
            //            cboOrganization.ReadOnly = false;
            //        }
            //        else
            //        {
            //            cboOrganization.DataSource = objorg;
            //            cboOrganization.DataBind();
            //            cboOrganization.SelectedIndex = 0;
            //            cboOrganization.ReadOnly = false;
            //        }
            //    }

            //    else if (HttpContext.Current.User.IsInRole("Inspection Data Encoder"))
            //    {
            //        //Load all list of Organizations
            //        List<Organization> objorg = new List<Organization>();
            //        OrganizationBussiness objorgBusiness = new OrganizationBussiness();
            //        objorg = objorgBusiness.GetActiveOrganizationsForInspectionTeam(Guid.Parse(objProfile.Organization.GUID));
            //        if (objorg.Count > 1)
            //        {
            //            cboOrganization.ValueField = "OrgGuid";
            //            cboOrganization.TextField = "DescriptionAm";
            //            cboOrganization.DataSource = objorg;
            //            cboOrganization.DataBind();
            //            if (OrganizationLanguage.ToString() == "0")
            //            {
            //                ListEditItem le = new ListEditItem("--Select--", "-1");
            //                cboOrganization.Items.Insert(0, le);
            //            }
            //            if (OrganizationLanguage.ToString() == "1")
            //            {
            //                ListEditItem le = new ListEditItem("ይምረጡ", "-1");
            //                cboOrganization.Items.Insert(0, le);
            //            }
            //            else if (OrganizationLanguage.ToString() == "2")
            //            {
            //                ListEditItem le = new ListEditItem("--mret--", "-1");
            //                cboOrganization.Items.Insert(0, le);
            //            }

            //            cboOrganization.SelectedIndex = 0;
            //            cboOrganization.ReadOnly = false;
            //        }
            //        else
            //        {
            //            cboOrganization.ValueField = "OrgGuid";
            //            cboOrganization.TextField = "DescriptionAm";
            //            cboOrganization.DataSource = objorg;
            //            cboOrganization.DataBind();
            //            cboOrganization.SelectedIndex = 0;
            //            cboOrganization.ReadOnly = false;
            //        }
            //    }
            //}


            if (!CAccessRight.CanAccess(CResource.eResource.ehrofficer))
            {
                List<Organization> objorgList = new List<Organization>();
                cboOrganization.ValueField = "OrgGuid";
                cboOrganization.TextField = "DescriptionAm";
              //  cboOrganizationSearch.ValueField = "OrgGuid";
               // cboOrganizationSearch.TextField = "DescriptionAm";
                ProfileCommon p = this.Profile;
                p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
                OrganizationBussiness objorgBusiness = new OrganizationBussiness();
                objorgList = objorgBusiness.GetActiveOrganizationsForInspectionTeam(Guid.Parse(p.Organization.GUID));
                if (objorgList.Count > 0)
                {
                    cboOrganization.DataSource = objorgList;
                    cboOrganization.DataBind();
             //       cboOrganizationSearch.DataSource = objorgList;
                   // cboOrganizationSearch.DataBind();
                    cboOrganization.Value = null;
                //    cboOrganizationSearch.Value = null;
                }
            }
            else
            {
                Session["OrgGuid"] = Session["ProfileOrgGuid"];// "8ca7db60-a5cc-4f1d-8956-5f52a82afae1"; //
                Organization objorg = new Organization();
                OrganizationBussiness objorgBusiness = new OrganizationBussiness();
                if (Session["OrgGuid"] != null)
                {
                    objorg = objorgBusiness.GetOrganization(Guid.Parse(Session["OrgGuid"].ToString()));
                    cboOrganization.Items.Insert(0, new ListEditItem(objorg.DescriptionAm, objorg.OrgGuid));
              //      cboOrganizationSearch.Items.Insert(0, new ListEditItem(objorg.DescriptionAm, objorg.OrgGuid));
                    cboOrganization.SelectedIndex = 0;
             //       cboOrganizationSearch.SelectedIndex = 0;
                    cboOrganization.ClientEnabled = false;
           //         cboOrganizationSearch.ClientEnabled = false;
                    cboOrganization.Value = null;
              //      cboOrganizationSearch.Value = null;
                }
            }

        }

        protected void Search()
        {
            if (AreUserInputsValid())
            {
                ProfileCommon p = this.Profile;
                p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
                DateTime dtfrom = txtDateFrom.SelectedGCDate;
                DateTime dtto = txtDateTo.SelectedGCDate;
                //pnlCompliantList1.Visible = true;
                ///  ASPxRoundPanel2.Visible = true;
                //pnlCompliantDetail.Visible = pnlFindingSummary.Visible = false;
                ComplaintBusiness compliantInformation = new ComplaintBusiness();
                DateTime reportedDateFrom = dtfrom;
                DateTime reportedDateTo = dtto;
                //Get selected organization 
                Guid orgGuid = !cboOrganization.SelectedItem.Text.Equals("ይምረጡ") ? Guid.Parse(cboOrganization.SelectedItem.Value.ToString()) : Guid.Empty;
                //Get selected complain Type 
                int complainType = !cboComplainType.SelectedItem.Text.Equals("ይምረጡ") ? Convert.ToInt32(cboComplainType.SelectedItem.Value.ToString()) : -1;
                //Get selected descision Type 
                //int descisionType = !cboDescsionType.SelectedItem.Text.Equals("ይምረጡ") ? Convert.ToInt32(cboDescsionType.SelectedItem.Value.ToString()) : -1;
                //Check whether page mode type is grievance or not
                bool isGrievance = (hdfldPageMode.Value.Equals("ቅሬታዎች") ? true : false);
                //Search compliants
                DataTable dtCompliant = compliantInformation.SearchCompliant(orgGuid, complainType, reportedDateFrom, reportedDateTo, isGrievance,p.Organization.LanguageID);
                //gvResult.DataSource = dtCompliant;
                //gvResult.DataBind();
                //if (dtCompliant.Rows.Count == 0)
                //{
                //    MessageBox1.ShowInfo(hdfldPageMode.Value + "(ዎች) ዝርዝር የለም።");
                //}
                //gvResult.Enabled = pnlShowDeleteButton.Visible = pnlCompliantList1.Visible = dtCompliant.Rows.Count > 0;
                //Session["CompliantList"] = (Object)gvResult;
            }
        }

        protected void cmdSearch_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void gvResult_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //edit tooltip
                if (CAccessRight.CanMakeComplianDecision(CResource.eResource.eMakeComplianDecision))
                {
                    e.Row.Cells[1].ToolTip = "ውሳኔ አስተላልፍ";
                }
                if (CAccessRight.CanEditComplain(CResource.eResource.eEditDataEntry))
                {
                    e.Row.Cells[1].ToolTip = "አስተካክል";
                }
            }
        }

        protected string GetCompliantMeansOfReport(Enumeration.MeansOfReport meansOfReport)
        {
            ProfileCommon p = this.Profile;
            p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
            string strMeansOfReport = "";
            switch (meansOfReport)
            {
                case Enumeration.MeansOfReport.InPerson:
                    if (p.Organization.LanguageID == 1)
                    {
                        strMeansOfReport =EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionType), Language.eLanguage.eAmharic, Convert.ToInt32(meansOfReport));
                    }
                    if (p.Organization.LanguageID == 2)
                    {
                        strMeansOfReport = EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionType), Language.eLanguage.eAfanOromo, Convert.ToInt32(meansOfReport));
                    }
                    break;
                case Enumeration.MeansOfReport.Web:
                    if (p.Organization.LanguageID == 1)
                    {
                        strMeansOfReport =EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionType), Language.eLanguage.eAmharic, Convert.ToInt32(meansOfReport));
                    }
                    if (p.Organization.LanguageID == 2)
                    {
                        strMeansOfReport = EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionType), Language.eLanguage.eAfanOromo, Convert.ToInt32(meansOfReport));
                    }
                    break;
                case Enumeration.MeansOfReport.Email:
                    if (p.Organization.LanguageID == 1)
                    {
                        strMeansOfReport =EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionType), Language.eLanguage.eAmharic, Convert.ToInt32(meansOfReport));
                    }
                    if (p.Organization.LanguageID == 2)
                    {
                        strMeansOfReport = EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionType), Language.eLanguage.eAfanOromo, Convert.ToInt32(meansOfReport));
                    }
                    break;
                case Enumeration.MeansOfReport.Telephone:
                    if (p.Organization.LanguageID == 1)
                    {
                        strMeansOfReport =EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionType), Language.eLanguage.eAmharic, Convert.ToInt32(meansOfReport));
                    }
                    if (p.Organization.LanguageID == 2)
                    {
                        strMeansOfReport = EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionType), Language.eLanguage.eAfanOromo, Convert.ToInt32(meansOfReport));
                    }
                    break;
                case Enumeration.MeansOfReport.Pobox:
                    if (p.Organization.LanguageID == 1)
                    {
                        strMeansOfReport =EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionType), Language.eLanguage.eAmharic, Convert.ToInt32(meansOfReport));
                    }
                    if (p.Organization.LanguageID == 2)
                    {
                        strMeansOfReport = EnumHelper.GetEnumDescription(typeof(Enumeration.InspectionType), Language.eLanguage.eAfanOromo, Convert.ToInt32(meansOfReport));
                    }
                    break;
            }
            return strMeansOfReport;
        }

        protected void LoadCompliantAttachment(Guid compliantGuid)
        {
            CompliantAttachmentBusiness attachment = new CompliantAttachmentBusiness();
            //int j = Convert.ToInt16(Session["j"]);
            //ASPxPageControl pageControl = DvgrdDecision.FindDetailRowTemplateControl(j, "pageControl") as ASPxPageControl;
            //if (pageControl != null) //True when called from Page_Load
            //{
            //    ASPxGridView gvAttachment = (ASPxGridView)pageControl.TabPages[1].FindControl("gvAttachment");
            //    if (gvAttachment != null)
            //    {
                    gvAttachment.DataSource = attachment.GetAttachmentByCompliantId(compliantGuid);
                    gvAttachment.DataBind();
            //    }
            //}


        }

        protected void LoadCompliantInfo(Guid compliantGuid)
        {
            ComplaintBusiness complian = new ComplaintBusiness();
            //GETH: Sep 7 2018
            DataTable dtComplian = complian.GetComplaint(compliantGuid);
            //string defaultDateFormat = ConfigurationManager.AppSettings.GetValues("DefaultDateFormat")[0];
            string compliantType = "አጠቃላይ";//used to set አጠቃላይ if the compliant type is  ጥቆማ
            if (dtComplian.Rows.Count == 1)
            {
                int j = Convert.ToInt16(Session["j"]);
                
                if (hdfldPageMode.Value.Equals("ጥቆማዎች"))
                {
                    if (HidlLanguage.Value == "1")
                    {
                        lblCompliantNameDecision.Text = "ጥቆማ አቅራቢ ሙሉ ስም: ";
                        lblMeansOfReportDecision.Text = "ጥቆማ የቀረበበት መንገድ: ";
                        lblCompliantTypeDecision.Text = "ጥቆማ አይነት: ";
                    }
                    else if (HidlLanguage.Value == "2")
                    {
                        lblCompliantNameDecision.Text = "Maqaa Gutu Saaxilaa Dhiyeessa : ";
                        lblMeansOfReportDecision.Text = "Haala Saaxilii Itti Dhiyaatte: ";
                        lblCompliantTypeDecision.Text = "Gosa Saaxilaa: ";
                    }
                }
                else
                {
                    if (HidlLanguage.Value == "1")
                    {
                        lblCompliantNameDecision.Text = "ቅሬታ አቅራቢ ሙሉ ስም: ";
                        lblMeansOfReportDecision.Text = "ቅሬታ የቀረበበት መንገድ:  ";
                        lblCompliantTypeDecision.Text = "ቅሬታ አይነት:  ";
                    }
                    if (HidlLanguage.Value == "2")
                    {
                        lblCompliantNameDecision.Text = "Maqaa Gutu Iyyaata Dhiyeessa: ";
                        lblMeansOfReportDecision.Text = "Haala Iyyaatichi Itti Dhiyaatte:  ";
                        lblCompliantTypeDecision.Text = "Gosa Iyyaata:  ";
                    }
                }
                lblCompliantNameDecisionValue.Text = dtComplian.Rows[0]["ReportedBy"].ToString();
                ListEditItem organization = cboOrganization.Items.FindByValue(dtComplian.Rows[0]["OrgGuid"].ToString());
                lblOrganizationDecisionValue.Text = organization.Text;
                lblTelephoneDecisionValue.Text = dtComplian.Rows[0]["ReporterTel"].ToString();
                lblEmailDecisionValue.Text = dtComplian.Rows[0]["ReporterEmail"].ToString();
                string ethiodate = Convert.ToString(dtComplian.Rows[0]["DateReported"].ToString());
                ethiodate = EthiopicDateTime.GetEthiopicDate((Convert.ToDateTime(ethiodate.ToString())).Day, Convert.ToDateTime(ethiodate.ToString()).Month, (Convert.ToDateTime(ethiodate.ToString())).Year);
                string tEthiodate = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate, Convert.ToInt32(HidlLanguage.Value));
                lblDateDecisionValue.Text = tEthiodate; //EthiopicDateTime.TranslateDateMonth(Convert.ToDateTime(dtComplian.Rows[0]["DateReported"]));
                lblMeansOfReportDecisionValue.Text = GetCompliantMeansOfReport((Enumeration.MeansOfReport)Convert.ToInt32(dtComplian.Rows[0]["MeansOfReport"]));
                if (Convert.ToInt32(dtComplian.Rows[0]["ScreeningDecision"]) != 0)
                {
                    //the complaint type is not አጠቃላይ so assign the value form the dropdown list box
                    ListEditItem listItem = cboComplainType.Items.FindByValue(dtComplian.Rows[0]["ScreeningDecision"].ToString());
                    compliantType = (listItem != null ? listItem.Text : "");
                }
                lblCompliantTypeDecisionValue.Text = compliantType;
                txtCompliantReason.Text = dtComplian.Rows[0]["ComplaintDescription"].ToString();
                //txtCompliantRemark.Text = dtComplian.Rows[0]["ComplaintRemark"].ToString();
                //txtCompliantRemark.Enabled = true;
                
                //GETH: Sep 7 2018 : only the if condition is commented
                // if (Convert.ToDateTime(dtComplian.Rows[0]["DateScreened"]) != Convert.ToDateTime(ConfigurationManager.AppSettings["FakeDate"].ToString()))
                //{
                    //cboDescsionTypeDetail.Value = dtComplian.Rows[0]["FindingSummary"].ToString();
                    // string[] findingSummaryDate = dtComplian.Rows[0]["DateScreened"].ToString().Split(new char[] { '/', ' ' });
                    // string findingSummaryReportedDate = EthiopicDateTime.GetEthiopicDate(Convert.ToInt32(findingSummaryDate[1]), Convert.ToInt32(findingSummaryDate[0]), Convert.ToInt32(findingSummaryDate[2]));
                    // txtFindingSummaryDate.SelectedDate = findingSummaryReportedDate;
                    // txtDecisionRemark.Text = dtComplian.Rows[0]["FindingSummaryDecision"].ToString();
                //}
                hdfldCompliantGuid.Value = compliantGuid.ToString();
                 LoadCompliantAttachment(compliantGuid);
                 LoadCompliantDescision();
            }
        }

        protected void gvRowCommand(Guid strGuid)
        {
            //int index = Convert.ToInt32(e.CommandArgument);
            Session["ComplaintGuid"] = "";
            if (CAccessRight.CanEditComplain(CResource.eResource.eEditDataEntry))
            {
                Session["ComplaintGuid"] = strGuid;
                string test = "ComplainDetail.aspx?ModeType=" + Request.QueryString["ModeType"] + "?PageMode=EDIT";
                Response.Redirect(test);
            }
            //pnlCompliantList1.Visible = false;
            ///  ASPxRoundPanel2.Visible = false;
            //FillCombo(cboDescsionTypeDetail, Language.eLanguage.eAmharic, typeof(Enumeration.FindingSummary));
            //pnlCompliantList1.Visible = pnlSearchingCriteria.Visible = false;
            //pnlCompliantDetail.Visible = pnlFindingSummary.Visible = true;
            //LoadCompliantInfo(strGuid);



            //////////////////////////////////////////////////////////////////////////////////////////////////

            //Session["person"] = strGuid;
            //Session["FileType"] = "311";
            //DvdvwComplian.Selection.UnselectAll();
            //DvdvwComplian.PageIndex = 0;
            //DvdvwComplian.DataBind();
            //pnlCallback.JSProperties["cpStatus"] = "Select";
        }

        protected void gvAttachment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //if (CAccessRight.CanDownload(CResource.eResource.eDownLoad))
                //{
                //    e.Row.Cells[2].ToolTip = "አውርድ";
                //}
                //else
                //{
                //    e.Row.Cells[2].Controls[0].Visible = false;
                //}
            }
        }

        protected void cmdDelete_Click(object sender, EventArgs e)
        {
            //if (cbeDelete.ConfirmOnFormSubmit == false)
            //{
            string strMsg = "";
            #region Collect selected compliant rows

            ArrayList selectedRowsList = new ArrayList();
            //Get all rows that are found in the grid
            //foreach (GridViewRow rows in gvResult.Rows)
            //{
            //    //Register selected or checked rows in the grid view on arraylist
            //    if (((CheckBox)rows.Cells[1].FindControl("chkSelect")).Checked)
            //    {
            //        selectedRowsList.Add(gvResult.DataKeys[rows.RowIndex]["ComplaintGUID"].ToString());
            //    }
            //}

            #endregion
            #region Delete compliant rows
            //check whether rows are selected
            if (selectedRowsList.Count > 0)
            {

                #region Deleting Compliant Attachmentes'

                bool deleted = false;
                int deletedCompliantCount = 0;
                ComplaintBusiness compliant = new ComplaintBusiness();
                for (int selectedCompliantindex = 0; selectedCompliantindex < selectedRowsList.Count; selectedCompliantindex++)
                {
                    //Try to delete
                    if (compliant.Delete(selectedRowsList[selectedCompliantindex].ToString()))
                    {
                        deleted = true;
                    }
                    if (deleted)
                    {
                        //count deleted rows
                        deletedCompliantCount++;
                        //Delete compliant attachment
                        CompliantAttachmentBusiness compliantAttachment = new CompliantAttachmentBusiness();
                        DataTable dt = compliantAttachment.GetAttachmentByCompliantId(Guid.Parse(selectedRowsList[selectedCompliantindex].ToString()));
                        if (dt.Rows.Count > 0)
                        {
                            for (int index = 0; index < dt.Rows.Count; index++)
                            {
                                compliantAttachment.Delete(dt.Rows[index]["AttachmentGuid"].ToString());
                            }
                        }
                        //Register compliant deletion 
                        //.....
                    }
                }
                // Report how many company type are successfully deleted
                if (deletedCompliantCount > 0)
                {
                    strMsg = String.Format("{0} " + hdfldPageMode.Value + "በትክክል ተወግደዋል።", deletedCompliantCount);
                    MessageBox1.ShowInfo(strMsg);
                    //Search
                    Search();
                }
                if (selectedRowsList.Count > deletedCompliantCount)
                {
                    strMsg = (selectedRowsList.Count - deletedCompliantCount).ToString() + hdfldPageMode.Value + " አልተወገዱም";
                    MessageBox1.ShowError(strMsg);
                }
                #endregion
            }
            else
            {
                strMsg = "እባክዎን! ማጥፋት የሚፈልጉትን " + hdfldPageMode.Value + " ይምረጡ።";
                MessageBox1.ShowSuccess(strMsg);
            }
            #endregion
            //}
        }

        protected void gvResult_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //gvResult.PageIndex = e.NewPageIndex;
            Search();
        }

        private string GetSortDirection(string column)
        {
            // By default, set the sort direction to ascending.
            string sortDirection = "ASC";

            // Retrieve the last column that was sorted.
            string sortExpression = ViewState["SortExpression"] as string;

            if (sortExpression != null)
            {
                // Check if the same column is being sorted.
                // Otherwise, the default value can be returned.
                if (sortExpression == column)
                {
                    string lastDirection = ViewState["SortDirection"] as string;
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        sortDirection = "DESC";
                    }
                }
            }

            // Save new values in ViewState.
            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return sortDirection;
        }

        protected void gvResult_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dt = Session["CompliantList"] as DataTable;
            if (dt != null)
            {
                //Sort the data.
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                //gvResult.DataSource = Session["CompliantList"];
                //gvResult.DataBind();
            }
        }

        protected void cmdSave_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void cmdBack_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void gvResult_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gvResult_SelectedIndexChanged1(object sender, EventArgs e)
        {

        }

        protected void cmdBack_Click(object sender, EventArgs e)
        {

            /// ASPxRoundPanel2.Visible = true;
        }

        protected void DvdvwComplian_BeforePerformDataSelect(object sender, EventArgs e)
        {

            Session["ComptGuid"] = (sender as ASPxGridView).GetMasterRowKeyValue();
        }

        protected void cmdSearch_Click1(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void pnlCallback_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            string strID = string.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0].ToString();
            if (e.Parameter.ToString().Contains('|'))
                strID = parameters[1].ToString();

            switch (strParam)
            {
                case "New":
                    //DoNew();
                    break;
                case "Select":
                    gvRowCommand(Guid.Parse(strID));
                    break;
            }
        }

        protected void DvgrdDecision_SelectionChanged(object sender, EventArgs e)
        {
            DvgrdDecision.Focus();
            int i = DvgrdDecision.FocusedRowIndex;
            string MainGuid = DvgrdDecision.GetRowValues(i, DvgrdDecision.KeyFieldName).ToString();

            Session["MainGuid"] = MainGuid;
        }

        protected void lkbtn(object sender, EventArgs e)
        {
            gvRowCommand(Guid.Parse(Session["MainGuid"].ToString()));
        }

        protected void DvgrdDecision_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs e)
        {
            //if (e.Column.FieldName == "ReportedDate")
            //    if ((e.Value).ToString().Length != 0)
            //    {
            //        string ethiodate;
            //        ethiodate = EthiopicDateTime.GetEthiopicDate(((DateTime)(e.Value)).Day, ((DateTime)(e.Value)).Month, ((DateTime)(e.Value)).Year);
            //        e.DisplayText = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate);
            //    }
            //    else
            //    {
            //        { return; }
            //    }
        }

        protected void gvAttachment_BeforePerformDataSelect(object sender, EventArgs e)
        {
            Session["CompGuid"] = (sender as ASPxGridView).GetMasterRowKeyValue();
        }

        protected void LoadCompliantDescision()
        {
            ProfileCommon p = this.Profile;
            p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
            ComplaintBusiness compliant = new ComplaintBusiness();
            Complaint objCompliant = new Complaint();
            Guid compliantGuid = Guid.Parse(Session["compliantGuid"].ToString());
            objCompliant = compliant.GettblComplaint(compliantGuid);
            if (objCompliant != null)
            {
                tblTeamMembersBussiness objTeamMembersBusiness = new tblTeamMembersBussiness();
                ProfileCommon objProfile = this.Profile;
                objProfile = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
                if (!(objTeamMembersBusiness.DoesEmployeeTeamLeader(Guid.Parse(objProfile.Staff.GUID.ToString()), Guid.Parse(objCompliant.OrgGuid.ToString()))
                    || HttpContext.Current.User.IsInRole("Inspection Director")))
                {
                    DvcomplainDecision.Columns[0].Visible = true;
                    btnupdate.Enabled = false;
                }
                else
                {
                    DvcomplainDecision.Columns[0].Visible = true;
                 
                    DataTable dtCompliant = new DataTable();
                    dtCompliant = compliant.GetComplianRecord(compliantGuid,p.Organization.LanguageID);
                    if (dtCompliant.Rows.Count > 0)
                    {
                        DvcomplainDecision.DataSource = dtCompliant;
                        DvcomplainDecision.DataBind();
                    }
                }
            }
            //upnlTabs.Update();
        }

        protected void DvgrdDecision_DetailRowExpandedChanged(int index, Guid complainGuid)
        {
            ProfileCommon p = this.Profile;
            p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
            Session["compliantGuid"] = complainGuid;
            Session["j"] = "";
            var temp = index;
            Session["j"] = index;
            ComplaintBusiness compliant = new ComplaintBusiness();
            DataTable dtcompliant = compliant.GetComplianRecord(Guid.Parse(Session["compliantGuid"].ToString()),p.Organization.LanguageID);
            if (dtcompliant.Rows.Count > 0)
            {
                Session["APersonGuid"] = dtcompliant.Rows[0]["EmployeeGUID"].ToString();
                Session["DocumentType"] = dtcompliant.Rows[0]["ScreeningDecision"].ToString();
                //LoadCompliantInfo(Guid.Parse(Session["compliantGuid"].ToString()));
              //  LoadCompliantAttachment(Guid.Parse(Session["compliantGuid"].ToString()));
              //  LoadCompliantDescision();
            }
        }

        protected void ASPxGridView1_BeforePerformDataSelect(object sender, EventArgs e)
        {
          //  Session["ComptGuid"] = (sender as ASPxGridView).GetMasterRowKeyValue();
        }

        protected string GetDecisionText()
        {
                    return cboDescsionTypeDetail.Value.ToString();
           
        }

        protected string GetDecisionMemo()
        {
           
                    return txtDecisionRemark.Html;
         
        }

        protected string GetComplainGUID()
        {
            if ( Session["compliantGuid"]!=null)
                return Session["compliantGuid"].ToString() ;
            else
             return Guid.Empty.ToString();
        }

        protected void LoadDescsionType()
        {
          ProfileCommon p = this.Profile;
          p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
          if (p.Organization.LanguageID == 0)
          {
            FillCombo(cboDescsionTypeDetail, Language.eLanguage.eEnglish, typeof (Enumeration.FindingSummary));
          }
          if (p.Organization.LanguageID == 1)
          {
              FillCombo(cboDescsionTypeDetail, Language.eLanguage.eAmharic, typeof(Enumeration.FindingSummary));
          }
          if (p.Organization.LanguageID == 2)
          {
              FillCombo(cboDescsionTypeDetail, Language.eLanguage.eAfanOromo, typeof(Enumeration.FindingSummary));
          }

        }

        protected void HiddenField1_DataBinding(object sender, EventArgs e)
        {
            LoadDescsionType();
        }

        private List<string> GetErrorMessaages()
        {

            List<string> errMessages = new List<string>();

            if (GetDecisionText() == string.Empty || GetDecisionText() == "-1")
                errMessages.Add("እባክዎ የሚሰጡትን ውሳኔ ያስገብ.");

            return errMessages;
        }

        protected bool SaveDecisionStatus()
        {
            bool saved = false;
            try
            {
                if (GetDecisionText() == "" || GetDecisionText() == "-1")
                {
                    return false;
                }
                else
                {
                    ComplaintBusiness complian = new ComplaintBusiness();
                    Complaint complaintInformation = new Complaint();
                    complaintInformation.ComplaintGUID = Guid.Parse(GetComplainGUID());
                    complaintInformation.FindingSummary = Convert.ToInt32(GetDecisionText());
                    complaintInformation.DateScreened = DateTime.Today;
                    complaintInformation.FindingSummaryDecision = GetDecisionMemo();
                    complaintInformation.ScreenedBy = Convert.ToString(Session["ProfileFullname"].ToString());
                    #region Saving Complain
                    if (complian.MakeCompliantDecision(complaintInformation))
                    {
                        saved = true;
                    }
                    #endregion
                }

            }

            catch (Exception ex)
            {
                MessageBox1.ShowError(ex.ToString());
            }

            return saved;
        }

        protected void btnupdate_Click(object sender, EventArgs e)
        {
            string actionOn = "";
            try
            {
                if (IsValidDecisionRecord())
                {

                    if (SaveDecisionStatus())
                    {
                        if (hdfldPageMode.Value.Equals("ጥቆማዎች"))
                        {
                            actionOn = "ጥቆማ";
                        }
                        else
                        {
                            actionOn = "ቅሬታ";
                        }
                        MessageBox1.ShowSuccess(actionOn + " " + " ውሳኔ  ተሰጦታል።");
                        LoadCompliantDescision();
                    }
                }

                else
                {
                    MessageBox1.ShowError(actionOn + " " + " ውሳኔ  አልተሰጠም።");
                }
            }
            catch (Exception ex)
            {
                MessageBox1.ShowError(ex.ToString());
            }
        }

        protected bool DoFindAttachment()
        {
            try
            {
                CompliantAttachment objAttachment = new CompliantAttachment();
                CompliantAttachmentBusiness objAttachmentBusiness = new CompliantAttachmentBusiness();
                objAttachment = objAttachmentBusiness.GetAttachment(Guid.Parse(Session["AMainGuid"].ToString()));
                string strURL = objAttachment.AttachmentTitle;
                string filePath = "~/AttachedFile/" + strURL;
                Response.AppendHeader("Content-Disposition", "AttachedFile; filename=" + strURL);
                // Write the file to the Response
                Response.TransmitFile(Server.MapPath(filePath));
                Response.End();
                //WebClient req = new WebClient();
                //HttpResponse response = HttpContext.Current.Response;
                //response.Clear();
                //response.ClearContent();
                //response.ClearHeaders();
                //response.Buffer = true;
                //response.AddHeader("Content-Disposition", "AttachedFile;filename=\""+ strURL + "\"");
                //byte[] data = req.DownloadData(Server.MapPath(strURL));
                //response.BinaryWrite(data);
                //response.End();

            }
            catch
            {
                MessageBox1.ShowInfo("ሰነዱ በቦታው አልተገኘም");
            }

            return true;
        }

        protected void btnView3_Click(object sender, EventArgs e)
        {
            //if (Session["AMainGuid"] != null)
            //{
                DoFindAttachment();

            //}
        }

        protected void gvAttachment_SelectionChanged(object sender, EventArgs e)
        {
            if (Session["j"] != null)
            {
                int j = Convert.ToInt16(Session["j"].ToString());
                ASPxPageControl pageControl = DvgrdDecision.FindDetailRowTemplateControl(j, "pageControl") as ASPxPageControl;
                if (pageControl != null) //True when called from Page_Load
                {
                    ASPxGridView GridAtt = (ASPxGridView)pageControl.TabPages[1].FindControl("gvAttachment");
                    if (GridAtt != null)
                    {
                        GridAtt.Focus();
                        int i = GridAtt.FocusedRowIndex;
                        string MainGuid = GridAtt.GetRowValues(i, GridAtt.KeyFieldName).ToString();
                        Session["AMainGuid"] = MainGuid;
                    }
                }
            }
        }

        protected void DvcomplainDecision_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
        {
            ProfileCommon p = this.Profile;
            p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
            if (e.Column.FieldName == "DateScreened")
            {
                if ((e.Value).ToString().Length != 0)
                {
                    DateTime dt = Convert.ToDateTime(e.Value.ToString());

                    if (dt.ToShortDateString() == "6/6/1900")
                        e.DisplayText = "";
                    else
                    {
                        string ethiodate;
                        ethiodate = EthiopicDateTime.GetEthiopicDate(((DateTime) (e.Value)).Day,
                            ((DateTime) (e.Value)).Month, ((DateTime) (e.Value)).Year);
                        e.DisplayText = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate, p.Organization.LanguageID);
                    }
                }
            }
            if (e.Column.FieldName == "FindingSummary")
            {
                if (p.Organization.LanguageID == 1)
                {
                    e.DisplayText = EnumHelper.GetEnumDescription(typeof(Enumeration.FindingSummary), Language.eLanguage.eAmharic, Convert.ToInt32(e.Value));
                }
                if (p.Organization.LanguageID == 2)
                {
                    e.DisplayText = EnumHelper.GetEnumDescription(typeof(Enumeration.FindingSummary), Language.eLanguage.eAfanOromo, Convert.ToInt32(e.Value));
                }
            }
        }

        public string GetDocURL(string strID)
        {
            try
            {
                return strID;
            }
            catch
            {
                return string.Empty;
            }

        }

        protected void gvAttachment_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
        {
            ProfileCommon p = this.Profile;
            p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
            if (e.Column.FieldName == "SentDate")
                if ((e.Value).ToString().Length != 0)
                {
                    string ethiodate;
                    ethiodate = EthiopicDateTime.GetEthiopicDate(((DateTime)(e.Value)).Day, ((DateTime)(e.Value)).Month, ((DateTime)(e.Value)).Year);
                    e.DisplayText = EthiopicDateTime.TranslateEthiopicDateMonth(ethiodate,p.Organization.LanguageID);
                }
                else
                {
                    { return; }
                }
        }

        protected void SearchComplian()
        {
            BindGrid();
        }
        
        protected void cmdSearch_Click2(object sender, EventArgs e)
        {
            Session["DocumentType"] = cboComplainType.Value;
            BindGrid();
       //     upnlCompGrid.Update();
            pageControl.Visible = false;
            //upnlTabs.Update();
        }
      
        protected void DvgrdDecision_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
        {
            ProfileCommon p = this.Profile;
            p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
            pageControl.Visible = true;
            //upnlTabs.Update();
            Session["compliantGuid"] = DvgrdDecision.GetRowValues(e.VisibleIndex, DvgrdDecision.KeyFieldName);
            //DvgrdDecision.key.GetSelectedFieldValues("ComplaintGuid");// DvgrdDecision.GetRowValues(e.VisibleIndex, DvgrdDecision.KeyFieldName).ToString();
            Session["j"] = "";
            var temp = e.VisibleIndex;
            Session["j"] = e.VisibleIndex;
            ComplaintBusiness compliant = new ComplaintBusiness();
            DataTable dtcompliant = compliant.GetComplianRecord(Guid.Parse(Session["compliantGuid"].ToString()),p.Organization.LanguageID);
            if (dtcompliant.Rows.Count > 0)
            {
                Session["APersonGuid"] = dtcompliant.Rows[0]["EmployeeGUID"].ToString();
                Session["DocumentType"] = cboComplainType.Value.ToString();
                Session["emp_status"] = "";
                Session["NormalInspectionType"] = false;
                LoadCompliantInfo(Guid.Parse(Session["compliantGuid"].ToString()));
                switch (Request.QueryString["ModeType"])
                {
                    case "Allude":
                        //pageControl.TabPages[3].Visible = false;
                        break;
                }
            }
        }
       
        protected void Button1_Click(object sender, EventArgs e)
        {
            if (Session["compliantGuid"] != null)
                LoadCompliantInfo(Guid.Parse(Session["compliantGuid"].ToString()));
        }

        protected void DvgrdDecision_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {

        }
 
        protected void DvgrdDecision_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
        {
            string key = e.Result.ToString();
        }
       
        protected void DvcomplainDecision_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
        {
            DvcomplainDecision.Visible = true;
            LoadDescsionType();
        }

        protected void pageControl_ActiveTabChanged(object source, TabControlEventArgs e)
        {
            if (pageControl.TabIndex == 3) ;
              //  DvcomplainDecision.Visible = false;
        }

        protected void DvgrdDecision_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            //var buttonEdit = (ASPxButton)DvgrdDecision.FindRowCellTemplateControl(e.VisibleIndex, null, "lnkSelectParentMessage");


            if (HttpContext.Current.User.IsInRole("Inspection Data Encoder"))
            {
                if (e.Row.Cells.Count == 6)
                    e.Row.Cells[5].Visible = false;
            }
        }
    }
}