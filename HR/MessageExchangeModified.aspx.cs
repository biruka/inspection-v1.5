﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.Data;
using System.Drawing;

using System.Data;
using DevExpress;
using System.IO;
using System.Web.Security;

using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.Configuration;
using System.Web.Security;

using CUSTOR;
using System.Text;
using System.Diagnostics;
using System.Globalization;
using DevExpress.XtraPrinting;
using System.Drawing;
using System.IO;
using DevExpress.XtraReports.UI;
using System.Web.Profile;
using System.Web;
using System.Web.UI;
using System.Reflection;
using System.Windows.Forms;
using CUSTOR.Domain;
using CUSTOR.Bussiness;
using CUSTOR.Business;
using CUSTOR.Commen;
using System.IO;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Threading;
using CUSTOR.Commen;

//namespace Messaging.UI
//{

public partial class MessageExchangeModified : System.Web.UI.Page
{
    const string UploadDirectory = "~/Attached/";
    const int ThumbnailSize = 100;
    protected void Page_Load(object sender, EventArgs e)
    {

        Session["PesrsonGuid"] = Session["UserID"];
        Session["OrgGuid"] = Session["ProfileOrgGuid"];
        if (!IsPostBack)
        {
            ParentMessageBLL ng = new ParentMessageBLL();
            LoadRequestType(ComboDocType);
            if (Session["ProLanguage"] != null)
            {
                HidlLanguage.Value = Session["ProLanguage"].ToString();
            }
            //LoadOrganizations();

            Session["IsNew"] = true;


            cdpFromDate.SelectedDate = EthiopicDateTime.GetEthiopicDate(int.Parse(DateTime.Now.Day.ToString()),
                                                 int.Parse(DateTime.Now.Month.ToString()),
                                                 int.Parse(DateTime.Now.Year.ToString()));
            cdpToDate.SelectedDate = EthiopicDateTime.GetEthiopicDate(int.Parse(DateTime.Now.Day.ToString()),
                                                   int.Parse(DateTime.Now.Month.ToString()),
                                                   int.Parse(DateTime.Now.Year.ToString()));
            if (CAccessRight.CanAccess(CResource.eResource.einspectorofficer) || CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) || HttpContext.Current.User.IsInRole("Inspection Director"))
            {
                lkbtnNew.Visible = false;
            }
            else if (CAccessRight.CanAccess(CResource.eResource.ehrofficer))
            {
                lkbtnNew.Visible = true;
            }

            if (Session["UserID"] != null)
            {
                HidUserid.Value = Session["UserID"].ToString();
                //LoadOrganizations(HidUserid.Value);
                LoadOrganizations(Guid.Parse(Session["ProfileOrgGuid"].ToString()),
                                            Session["ProfileOrgCatagory"].ToString(), int.Parse(HidlLanguage.Value));
            }
            //}


            BindGrid();
            if ((HttpContext.Current.User.IsInRole(("Inspection Director"))))
            {

                btnCompletAnswer.ClientVisible = true;
            }
            else
            {
                btnCompletAnswer.ClientVisible = false;
            }


        }
        if (IsPostBack)
        {
            ParentMessageBLL objMsgBusiness = new ParentMessageBLL();
            if (Session["ID"] != null)
            {

            }
        }
    }

    protected override void InitializeCulture()
    {
        ProfileCommon p = this.Profile;
        if (p.Organization.LanguageID != null)
        {
            String selectedLanguage = string.Empty;
            switch (p.Organization.LanguageID)
            {
                case 0:
                    selectedLanguage = "en-US";//English
                    break;
                case 2:
                    selectedLanguage = "am-ET";//Afan Oromo
                    break;
                case 3:
                    selectedLanguage = "am-ET"; //Tig
                    break;
                case 4:
                    selectedLanguage = "en-GB";//afar
                    break;
                case 5:
                    selectedLanguage = "en-AU";//Somali
                    break;
                default: break;//Amharic
            }
            UICulture = selectedLanguage;
            Culture = selectedLanguage;

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(selectedLanguage);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(selectedLanguage);
        }
        base.InitializeCulture();
    }

    //protected void LoadOrganizations()
    //{

    //    if (!CAccessRight.CanAccess(CResource.eResource.ehrofficer) && !CAccessRight.CanAccess(CResource.eResource.eHRSupervisor))
    //    {
    //        OrganizationBussiness obj = new OrganizationBussiness();
    //        ComboOrg.ValueField = "OrgGuid";
    //        ComboOrg.TextField = "DescriptionAm";
    //        ComboOrg.DataSource = obj.GetActiveOrganizations();
    //        ComboOrg.DataBind();
    //        ListEditItem le = new ListEditItem("ይምረጡ", "-1");
    //        ComboOrg.Items.Insert(0, le);
    //        ComboOrg.SelectedIndex = 0;

    //    }
    //    else
    //    {
    //        Session["OrgGuid"] = Session["ProfileOrgGuid"];// "8ca7db60-a5cc-4f1d-8956-5f52a82afae1"; //
    //        Organization objorg = new Organization();
    //        OrganizationBussiness objorgBusiness = new OrganizationBussiness();
    //        if (Session["OrgGuid"] != null)
    //            objorg = objorgBusiness.GetOrganization(Guid.Parse(Session["OrgGuid"].ToString()));

    //        string DescriptionAm = objorg.DescriptionAm;
    //        ComboOrg.Text = DescriptionAm;
    //        ComboOrg.ReadOnly = false;
    //    }

    //}

    protected void LoadRequestType(ASPxComboBox comboBox)
    {
        tblLookupBussiness objRequestType = new tblLookupBussiness();
        DataTable dtRequestType = objRequestType.GetLookupByLookupType("98");
        if (dtRequestType.Rows.Count > 0)
        {
            if (Session["ProLanguage"].ToString() == "0")
            {
                ListEditItem le = new ListEditItem("--Select--", "-1");
                comboBox.ValueField = "code";
                comboBox.TextField = "description";
                comboBox.DataSource = dtRequestType;
                comboBox.DataBind();
                comboBox.Items.Insert(0, le);
                comboBox.Text = "-1";
            }
            else if (Session["ProLanguage"].ToString() == "1")
            {
                ListEditItem le = new ListEditItem("ይምረጡ", "-1");
                comboBox.ValueField = "code";
                comboBox.TextField = "amdescription";
                comboBox.DataSource = dtRequestType;
                comboBox.DataBind();
                comboBox.Items.Insert(0, le);
                comboBox.Text = "-1";
            }
            else if (Session["ProLanguage"].ToString() == "2")
            {
                ListEditItem le = new ListEditItem("--mret--", "-1");
                comboBox.ValueField = "code";
                comboBox.TextField = "description";
                comboBox.DataSource = dtRequestType;
                comboBox.DataBind();
                comboBox.Items.Insert(0, le);
                comboBox.Text = "-1";
            }
        }
    }

    private bool checkdate()
    {
        try
        {
            DateTime dtfrom = cdpFromDate.SelectedGCDate;
            DateTime dtto = cdpToDate.SelectedGCDate;
            Session["dtfrom"] = dtfrom;
            Session["dtto"] = dtto;
            if (dtfrom != new DateTime())
            {
                if (dtto != new DateTime())
                {
                    if (dtfrom.CompareTo(dtto) > 0)
                    {
                        CMsgBar.ShowError(Resources.Message.MSG_VRDATE, pnlMsg, lblPersonMsg);
                        return false;
                    }
                    else if ((dtfrom == new DateTime()) || (dtto == new DateTime()))
                    {
                        CMsgBar.ShowError(Resources.Message.MSG_VDATE, pnlMsg, lblPersonMsg);
                        return false;
                    }
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            CMsgBar.ShowError(Resources.Message.MSG_VDATE, pnlMsg, lblPersonMsg);
            return false;
        }
    }

    private bool orgcombochk()
    {
        try
        {
            if (ComboOrg.Value.ToString() != null)
            {
                if (ComboOrg.Value.ToString() == "-1")
                {
                    CMsgBar.ShowError(Resources.Message.MSG_SELECTORGANIZATION, pnlMsg, lblPersonMsg);
                    return false;
                }
                else
                {
                    Session["OrgGuid"] = (string)ComboOrg.SelectedItem.Value;
                    return true;
                }
                return true;
            }
            return false;
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    protected void LoadOrganizations(Guid OrganizationGuid, string OrganizationCatagory, int OrganizationLanguage)
    {
        if (HttpContext.Current.User.Identity.IsAuthenticated)
        {
            ProfileCommon objProfile = this.Profile;
            objProfile = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
            Session["OrgLanguage"] = objProfile.Organization.Language;

            //InspectionFollowupBussiness objOrganizations = new InspectionFollowupBussiness();
            //DataTable dt = objOrganizations.getEmployeeInspectedOrganizations(Guid.Parse(objProfile.Staff.GUID));
            ////check whethere the logged in user can view approve button
            //Session["loggedInUser"] = Guid.Parse(objProfile.Staff.GUID).ToString();

            //ComboOrg.ValueField = "OrgGuid";
            //ComboOrg.TextField = "DescriptionAm";
            //if (dt.Rows.Count > 1)
            //{
            //    if (HidlLanguage.Value == "0")
            //    {
            //        ListEditItem le = new ListEditItem("--Select--", "-1");
            //        ComboOrg.Items.Insert(0, le);
            //    }
            //    if (HidlLanguage.Value == "1")
            //    {
            //        ListEditItem le = new ListEditItem("ይምረጡ", "-1");
            //        ComboOrg.Items.Insert(0, le);
            //    }
            //    else if (HidlLanguage.Value == "2")
            //    {
            //        ListEditItem le = new ListEditItem("--mret--", "-1");
            //        ComboOrg.Items.Insert(0, le);
            //    }
            //    ComboOrg.SelectedIndex = 0;
            //    ComboOrg.DataSource = dt;
            //    ComboOrg.DataBind();
            //}
            //else
            //{
            //    ComboOrg.DataSource = dt;
            //    ComboOrg.DataBind();
            //}

            if (CAccessRight.CanAccess(CResource.eResource.ehrofficer) || CAccessRight.CanAccess(CResource.eResource.eHRSupervisor) || HttpContext.Current.User.IsInRole("Inspection officer"))
            {
                List<Organization> objorg = new List<Organization>();
                OrganizationBussiness objorgBusiness = new OrganizationBussiness();
                objorg = objorgBusiness.GetListOrg(Guid.Parse(objProfile.Organization.GUID));
                if (objorg.Count > 0)
                {
                    ComboOrg.ValueField = "OrgGuid";
                    ComboOrg.TextField = "DescriptionAm";
                    ComboOrg.DataSource = objorg;
                    ComboOrg.DataBind();
                    ComboOrg.ReadOnly = false;
                    if (HidlLanguage.Value == "0")
                    {
                        ListEditItem le = new ListEditItem("--Select--", "-1");
                        ComboOrg.Items.Insert(0, le);
                    }
                    if (HidlLanguage.Value == "1")
                    {
                        ListEditItem le = new ListEditItem("ይምረጡ", "-1");
                        ComboOrg.Items.Insert(0, le);
                    }
                    else if (HidlLanguage.Value == "2")
                    {
                        ListEditItem le = new ListEditItem("--mret--", "-1");
                        ComboOrg.Items.Insert(0, le);
                    }
                    ComboOrg.SelectedIndex = 0;
                }
            }
            if (HttpContext.Current.User.IsInRole("Inspection Director") || CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor))
            {
                //Load all list of Organizations
                List<Organization> objorg = new List<Organization>();
                OrganizationBussiness objorgBusiness = new OrganizationBussiness();
                objorg = objorgBusiness.GetActiveOrganizationsForInspection(Guid.Parse(objProfile.Organization.GUID));
                if (objorg.Count > 0)
                {
                    ComboOrg.ValueField = "OrgGuid";
                    ComboOrg.TextField = "DescriptionAm";
                    ComboOrg.DataSource = objorg;
                    ComboOrg.DataBind();
                    ComboOrg.ReadOnly = false;
                    if (HidlLanguage.Value == "0")
                    {
                        ListEditItem le = new ListEditItem("--Select--", "-1");
                        ComboOrg.Items.Insert(0, le);
                    }
                    if (HidlLanguage.Value == "1")
                    {
                        ListEditItem le = new ListEditItem("ይምረጡ", "-1");
                        ComboOrg.Items.Insert(0, le);
                    }
                    else if (HidlLanguage.Value == "2")
                    {
                        ListEditItem le = new ListEditItem("--mret--", "-1");
                        ComboOrg.Items.Insert(0, le);
                    }
                    ComboOrg.SelectedIndex = 0;
                }
            }
        }
    }

    protected void LoadOrganizations(string UserId)
    {

        if (!CAccessRight.CanAccess(CResource.eResource.ehrofficer))
        {

            OrganizationBussiness obj = new OrganizationBussiness();
            ComboOrg.ValueField = "OrgGuid";
            ComboOrg.TextField = "DescriptionAm";
            //ComboOrg.DataSource = obj.GetOrganizationsList(Guid.Parse(UserId.ToString()));
            ComboOrg.DataBind();
            if (HidlLanguage.Value == "0")
            {
                ListEditItem le = new ListEditItem("--Select--", "-1");
                ComboOrg.Items.Insert(0, le);
            }
            else if (HidlLanguage.Value == "1")
            {
                ListEditItem le = new ListEditItem("ይምረጡ", "-1");
                ComboOrg.Items.Insert(0, le);
            }
            else if (HidlLanguage.Value == "2")
            {
                ListEditItem le = new ListEditItem("--mret--", "-1");
                ComboOrg.Items.Insert(0, le);
            }
            else if (HidlLanguage.Value == "3")
            {
                ListEditItem le = new ListEditItem("ምረፅ", "-1");
                ComboOrg.Items.Insert(0, le);
            }
            else if (HidlLanguage.Value == "4")
            {
                ListEditItem le = new ListEditItem("--mret--", "-1");
                ComboOrg.Items.Insert(0, le);
            }
            else if (HidlLanguage.Value == "5")
            {
                ListEditItem le = new ListEditItem("--Select--", "-1");
                ComboOrg.Items.Insert(0, le);
            }
            else
            {
                ListEditItem le = new ListEditItem("--Select--", "-1");
                ComboOrg.Items.Insert(0, le);
            }
            ComboOrg.Text = "-1";
            //Session["OrgGuid"] = "4136794a-f49f-4b68-a237-431a3511eb48";

        }
        else
        {
            //Session["OrgGuid"] = Session["ProfileOrgGuid"];// "8ca7db60-a5cc-4f1d-8956-5f52a82afae1"; //
            //Session["OrgGuid"] = "8ca7db60-a5cc-4f1d-8956-5f52a82afae1";
            Organization objorg = new Organization();
            OrganizationBussiness objorgBusiness = new OrganizationBussiness();
            if (Session["OrgGuid"] != null)
                objorg = objorgBusiness.GetOrganization(Guid.Parse(Session["OrgGuid"].ToString()));

            string DescriptionAm = objorg.DescriptionAm;
            ComboOrg.Text = DescriptionAm;
            ComboOrg.ReadOnly = false;
        }

    }

    public void setSecutiryPageAccess()
    {

        int j = (Session["RowIndex"] != null ? Convert.ToInt16(Session["RowIndex"].ToString()) : 0);

        ASPxPageControl pageControl = gvwParentMessage.FindDetailRowTemplateControl(j, "pagecontrol") as ASPxPageControl;


        if (pageControl != null) //True when called from Page_Load 
        {

            if (CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) || (HttpContext.Current.User.IsInRole(("Inspection Director"))))
            {
                //tabpage access not authorise;
                lkbtnNew.Visible = false;
                pageControl.TabPages[0].Visible = true;
                pageControl.TabPages[1].Visible = true;
                pageControl.TabPages[2].Visible = true;
                gvwParentMessage.Columns[11].Visible = false;
                if ((HttpContext.Current.User.IsInRole(("Inspection Director"))))
                {
                    btnCompletAnswer.Visible = true;
                    chkCompleted.Visible = true;
                    btnSaveMessageExchange.Visible = false;
                }
                if (CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor))
                {
                    btnCompletAnswer.Visible = false;
                    chkCompleted.ClientVisible = false;
                    btnSaveMessageExchange.Visible = true;

                }
            }
            else if (CAccessRight.CanAccess(CResource.eResource.ehrofficer))
            {
                lkbtnNew.Visible = true;
                pageControl.TabPages[0].Visible = false;
                pageControl.TabPages[1].Visible = false;
                pageControl.TabPages[2].Visible = true;
                gvwParentMessage.Columns[11].Visible = true;
                btnCompletAnswer.ClientVisible = false;
            }
      

        }

    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.Unload += new EventHandler(UpdatePanel_Unload);
        //this.UpdatePanel1.Unload += new EventHandler(UpdatePanel_Unload);


    }

    void UpdatePanel_Unload(object sender, EventArgs e)
    {
        this.RegisterUpdatePanel(sender as UpdatePanel);
    }

    public void RegisterUpdatePanel(UpdatePanel panel)
    {
        foreach (MethodInfo methodInfo in typeof(ScriptManager).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance))
        {
            if (methodInfo.Name.Equals("System.Web.UI.IScriptManagerInternal.RegisterUpdatePanel"))
            {
                try
                {
                    //methodInfo.Invoke(ScriptManager.GetCurrent(Page), new object[] { UpdatePanel1 });
                    //methodInfo.Invoke(ScriptManager.GetCurrent(Page), new object[] { upnlGrade });
                    //methodInfo.Invoke(ScriptManager.GetCurrent(Page), new object[] { upnlUnitCodetxtBox });
                }
                catch (Exception ex)
                {

                }

            }
        }
    }

    private void BindAttachmentGrid(Int32 ParentId)
    {
        MessageExchangeBLL objtblInspectionMessageExchangeBussiness = new MessageExchangeBLL();
        DvgvAttachment.DataSource = objtblInspectionMessageExchangeBussiness.BindMessageAttachments(ParentId);
        DvgvAttachment.DataBind();
    }

    //search messages
    private void BindGrid()
    {
        try
        {
            string condition = "";
            ParentMessageBLL objMessageBussiness = new ParentMessageBLL();

            if (CAccessRight.CanAccess(CResource.eResource.einspectorofficer) || CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) || HttpContext.Current.User.IsInRole("Inspection Director"))
            {
                condition = condition + "AND (FlowStatus=2 OR FlowStatus=4)  ";
                Session["parentMessageCondition"] = condition;

                gvwParentMessage.DataSource = objMessageBussiness.GetParentMessages(Session["parentMessageCondition"].ToString());
                gvwParentMessage.DataBind();
            }
            else if (CAccessRight.CanAccess(CResource.eResource.ehrofficer) || CAccessRight.CanAccess(CResource.eResource.eHRSupervisor))
            {
                gvwParentMessage.DataSource = objMessageBussiness.GetParentMessages(" AND Organization.OrgGuid= '" + Session["ProfileOrgGuid"].ToString() + "'");
                gvwParentMessage.DataBind();
                gvwParentMessage.SettingsDetail.ShowDetailButtons = false;
            }





        }
        catch (Exception ex)
        {

        }
    }

    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        //tabs.Visible = true;
        string condition = "";
        if (ComboOrg.Value.ToString() != "-1")//
        {
            Session["ServingOrgGuid"] = ComboOrg.Value.ToString();

            condition = " AND tblInspectionParentMessage.OrgGuid= '" + ComboOrg.Value.ToString() + "'";
        }
        if (cdpFromDate.SelectedDate != "" && cdpToDate.SelectedDate != "")
        {
            condition = condition + "  AND tblInspectionParentMessage.EventDatetime BETWEEN '" + cdpFromDate.SelectedGCDate.ToShortDateString() + "' AND '" + cdpToDate.SelectedGCDate.ToShortDateString() + "' ";
        }

        ParentMessageBLL objMessageBussiness = new ParentMessageBLL();

        if (CAccessRight.CanAccess(CResource.eResource.einspectorofficer) || CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) || HttpContext.Current.User.IsInRole("Inspection Director"))
        {
            condition = condition + "AND (FlowStatus=2 OR FlowStatus=4)  ";
            Session["parentMessageCondition"] = condition;

            gvwParentMessage.DataSource = objMessageBussiness.GetParentMessages(Session["parentMessageCondition"].ToString());
            gvwParentMessage.DataBind();
        }

        setSecutiryPageAccess();
    }

    public void displayParentMessages()
    {
        if (Guid.Parse("4136794a-f49f-4b68-a237-431a3511eb48") != Guid.Parse(Session["ProfileOrgGuid"].ToString()))
        {
            ParentMessageBLL objMsgBLL = new ParentMessageBLL();
            gvwParentMessage.DataSource = objMsgBLL.GetParentMessages("AND Organization.OrgGuid='" + Session["ProfileOrgGuid"].ToString() + "'");
            gvwParentMessage.DataBind();
        }
        else
        {
            ParentMessageBLL objMsgBLL = new ParentMessageBLL();
            gvwParentMessage.DataSource = objMsgBLL.GetParentMessages("");
            gvwParentMessage.DataBind();
        }
    }

    protected void GridViewAttach_SelectedIndexChanged(object sender, EventArgs e)
    {


    }

    protected void ASPxButton2_Click(object sender, EventArgs e)
    {
        ParentMessageBLL objBussines = new ParentMessageBLL();
        AttachmentMessageExchangeBO objAttachment = new AttachmentMessageExchangeBO();
        objAttachment.MainGuid = Guid.NewGuid();
        objAttachment.Url = objAttachment.MainGuid.ToString();
        objAttachment.ID = Int32.Parse(Session["ParentMessageID"].ToString());

        objAttachment.UserName = Session["ProfileFullName"].ToString();
        objAttachment.UpdatedEventDatetime = DateTime.Today;
        objAttachment.EventDatetime = DateTime.Today;
        objAttachment.UpdatedUsername = Session["ProfileFullName"].ToString();

        //attachment

        //objAttachment.Url = objAttachment.MainGuid.ToString() + fuDocument.FileName;
        //objAttachment.txtFileDescription = txtFileDescription.Text;

        //if (FileUpload1.PostedFile != null)
        //{
        //    objAttachment.Url = objAttachment.MainGuid.ToString() + FileUpload1.PostedFile.FileName;
        //    objAttachment.txtFileDescription = txtFileDescription.Text;
        //    // + fuDocument.FileName
        //    FileUpload1.PostedFile.SaveAs(Server.MapPath("~/AttachedFile/") + objAttachment.Url);
        //    objBussines.InserttblMessageExchangeAttachment(objAttachment);
        //    txtFileDescription.Text=string.Empty;
        //    GridViewAttach.DataSource = objBussines.BindMessageAttachments(Int32.Parse(Session["ParentMessageID"].ToString()));
        //    GridViewAttach.DataBind();

        //    Msg.ShowSuccess("ፋይሉ በትክክል ተያይዘዋል!");

        //}
        //else
        //    Msg.ShowInfo("እባክዎ ፋይሉን ይምረጡ!");

    }

    protected void treeList_SelectionChanged(object sender, EventArgs e)
    {
        // Session["ID"]=treeList.KeyFieldName[0];
    }

    protected bool DoDelete()
    {
        MessageExchangeBO ob = new MessageExchangeBO();
        ParentMessageBLL objMsgBusiness = new ParentMessageBLL();
        try
        {
            objMsgBusiness.DeleteMessage(Int32.Parse(Session["ParentMessageID"].ToString()));
            Session["IsNew"] = false;
            Msg.ShowSuccess(Resources.Message.MSG_DELETED);

            return true;
        }
        catch (Exception ex)
        {
            Msg.ShowError(ex.Message);
            return false;
        }
    }

    protected bool DoNew()
    {
        try
        {
            Session["IsNew"] = true;
            ClearForm();
            return true;
        }
        catch (Exception ex)
        {
            Msg.ShowError(ex.Message);
            return false;
        }
    }

    //save new document inspection
    protected bool DoSave()
    {

        List<string> errMessages = GetErrorMessage();
        if (errMessages.Count > 0)
        {

            Msg.ShowError(errMessages.ToString());
            return false;
        }

        MessageExchangeBO objMessageBO = new MessageExchangeBO();
        try
        {
            objMessageBO.OrgGuid = Guid.Parse(Session["OrgGuid"].ToString());
            objMessageBO.Subject = ddlCase.Text;
            objMessageBO.Case = ddlCase.Value.ToString();
            objMessageBO.Text = txtMessage.Text;
            objMessageBO.UserName = Session["ProfileFullName"].ToString();
            objMessageBO.EmployeeName = Session["ProfileFullName"].ToString();
            objMessageBO.UpdatedUsername = Session["ProfileFullName"].ToString();
            objMessageBO.EventDatetime = DateTime.Today;
            objMessageBO.UpdatedEventDatetime = DateTime.Today;
            objMessageBO.FlowStatus = "2";
            ParentMessageBLL objMessageBussiness = new ParentMessageBLL();
            if ((bool)Session["IsMNew"])
            {
                objMessageBussiness.Insert(objMessageBO);
            }
            else
            {
                if (Session["ParentEditID"] != null)
                {
                    objMessageBO.ParentMessageID = Int32.Parse(Session["ParentEditID"].ToString());
                    objMessageBO.UpdatedUsername = Session["ProfileFullName"].ToString();
                    objMessageBO.UpdatedEventDatetime = DateTime.Today;
                    objMessageBussiness.Update(objMessageBO);
                }
                else
                {
                    Msg.ShowError(Resources.Message.MSG_NotSaved);
                }
            }
            //Hidparentid.Value =Session["Attparent"];
            SaveAttachment();
            BindGrid(); //bind data
            Session["IsMNew"] = false;
            Msg.ShowSuccess(Resources.Message.MSG_SAVED);
            return true;
        }
        catch (Exception ex)
        {
            Msg.ShowError(ex.Message);
            return false;
        }
    }

    protected void ClearForm()
    {

        ddlCase.SelectedIndex = 0;
        this.txtMessage.Text = string.Empty;
        this.chkCompleted.Checked = false;
    }

    protected string DoFindTemp(int ID)
    {
        ParentMessageBLL objMsgBusiness = new ParentMessageBLL();
        MessageExchangeBO objMsgBO = new MessageExchangeBO();

        try
        {
            objMsgBO = (MessageExchangeBO)objMsgBusiness.GetParentMessage(ID);
            if (objMsgBO == null)
            {
                Msg.ShowInfo(Resources.Message.MSG_NODATACR);
                return string.Empty;
            }
            //Now Record was found;
            Session["IsNew"] = false;
            ClearForm();

            string msgRecord = objMsgBO.Case + "|";
            msgRecord += objMsgBO.Subject + "|";
            msgRecord += objMsgBO.Text + "|";
            msgRecord += objMsgBO.Status + "|";

            int Toplevel = 0;
            if (objMsgBO.ParentID == 1)
                Toplevel = 1;
            msgRecord += Toplevel;

            Session["ID"] = ID;

            //bind attachment
            //GridViewAttach.DataSource = objMsgBusiness.BindMessageAttachments(ID);
            //GridViewAttach.DataBind();

            return msgRecord;
        }
        catch (Exception ex)
        {
            return string.Empty;
        }
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        //GridViewAttach.Focus();
        //int i = GridViewAttach.FocusedRowIndex;
        //string url = GridViewAttach.GetRowValues(i, "Url").ToString();

        //if (url!=null)
        //{
        //   // string NavigateUrl = "~/AttachedFile/" + url;
        //    Session["FileURL"] = url;

        //}




        Redirect("~/frmImageViewer.aspx", "_blank", "");

    }

    public void Redirect(string url, string target, string windowFeatures)
    {
        HttpContext context = HttpContext.Current;
        if ((String.IsNullOrEmpty(target) || target.Equals("_self", StringComparison.OrdinalIgnoreCase)) && String.IsNullOrEmpty(windowFeatures))
        {
            context.Response.Redirect(url);
        }
        else
        {
            System.Web.UI.Page page = (System.Web.UI.Page)context.Handler;
            if (page == null)
            {
                throw new InvalidOperationException("Cannot redirect to new window outside Page context.");
            }
            url = page.ResolveClientUrl(url);
            string script;
            if (!String.IsNullOrEmpty(windowFeatures))
            {
                script = @"window.open(""{0}"", ""{1}"", ""{2}"");";
            }
            else
            {
                script = @"window.open(""{0}"", ""{1}"");";
            }
            script = String.Format(script, url, target, windowFeatures);
            ScriptManager.RegisterStartupScript(page, typeof(System.Web.UI.Page), "Redirect", script, true);
        }
    }

    protected void LinkButton5_Click(object sender, EventArgs e)
    {

    }

    protected void LinkButton4_Click(object sender, EventArgs e)
    {

    }

    protected void gvwParentMessage_DetailRowExpandedChanged(object sender, DevExpress.Web.ASPxGridViewDetailRowEventArgs e)
    {
        Session["RowIndex"] = "";
        int temp = e.VisibleIndex;
        Session["RowIndex"] = temp;
    }

    public void displayMessageExchange()
    {
        MessageExchangeBLL objMessageBLL = new MessageExchangeBLL();
        if (Session["RowIndex"] != null)
        {
            int rowIndex = (int)Session["RowIndex"];
            ASPxPageControl pageControl = gvwParentMessage.FindDetailRowTemplateControl(rowIndex, "pagecontrol") as ASPxPageControl;
            if (pageControl != null) //True when called from Page_Load 
            {
                ASPxGridView MessageExchange = (ASPxGridView)pageControl.TabPages[2].FindControl("gvwMessageExchange");
                if (MessageExchange != null)
                {
                    objMessageBLL.GetMessages(Int32.Parse(Session["ParentMessageID"].ToString()));
                    MessageExchange.DataSource = objMessageBLL.GetMessages(Int32.Parse(Session["ParentMessageID"].ToString()));
                    MessageExchange.DataBind();
                }
            }

        }
    }

    protected void DoFindParent()
    {
        ParentMessageBLL objMsgBusiness = new ParentMessageBLL();
        MessageExchangeBO objMsgBO = new MessageExchangeBO();

        gvwParentMessage.Focus();
        int i = gvwParentMessage.FocusedRowIndex;
        string ri = gvwParentMessage.GetRowValues(i, "ParentMessageID").ToString();
        Session["RowI"] = i;
        Session["ParentMessageID"] = ri;
        Session["ParentEditID"] = ri;
        Session["IsMNew"] = false;
        try
        {
            objMsgBO = (MessageExchangeBO)objMsgBusiness.GetParentMessage(Int32.Parse(Session["ParentMessageID"].ToString()));
            if (objMsgBO == null)
            {
                Msg.ShowInfo(Resources.Message.MSG_NODATACR);

            }
            //Now Record was found;
            Session["IsMessageNew"] = false;
            ClearForm();
            LoadRequestType(ddlCase);
            ddlCase.Value = objMsgBO.Case;
            txtMessage.Text = objMsgBO.Text;
            //ComboOrg.Text = objMsgBO.OrgGuid.ToString();
            BindAttachmentGrid(Int32.Parse(ri));
            if (objMsgBO.FlowStatus == "2")
            {
                btnApproval.Enabled = true;
                cmdSave.Enabled = true;
                cmdBack.Enabled = true;
            }
            else if (objMsgBO.FlowStatus == "4")
            {
                cmdSave.Enabled = false;
                btnApproval.Enabled = false;
                cmdBack.Enabled = false;

            }

        }
        catch (Exception ex)
        {
            Msg.ShowError("Error " + ex.Message);
        }
        displayGridAttachments();


        string condition = "";
        if (Session["ParentMessageID"] != null)
            condition = " AND ParentMessageID=" + Session["ParentMessageID"].ToString();

    }

    protected void lnkSelectParentMessage_Click(object sender, EventArgs e)
    {
        ParentMessageBLL objMsgBusiness = new ParentMessageBLL();
        MessageExchangeBO objMsgBO = new MessageExchangeBO();

        gvwParentMessage.Focus();
        int i = gvwParentMessage.FocusedRowIndex;
        string ri = gvwParentMessage.GetRowValues(i, "ParentMessageID").ToString();

        Session["ParentMessageID"] = ri;
        try
        {
            objMsgBO = (MessageExchangeBO)objMsgBusiness.GetParentMessage(Int32.Parse(Session["ParentMessageID"].ToString()));
            if (objMsgBO == null)
            {
                Msg.ShowInfo(Resources.Message.MSG_NODATACR);

            }
            else
            {
                LoadRequestType(ddlCase);
                //Now Record was found;
                Session["IsNew"] = false;
                ClearForm();
                ddlCase.SelectedIndex = Convert.ToInt32(objMsgBO.Case);
                txtMessage.Text = objMsgBO.Text;
                ComboOrg.Text = objMsgBO.OrgGuid.ToString();
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error " + ex.Message);
        }
        displayGridAttachments();


        string condition = "";
        if (Session["ParentMessageID"] != null)
            condition = " AND ParentMessageID=" + Session["ParentMessageID"].ToString();

    }

    public void displayGridAttachments()
    {
        ParentMessageBLL objBussines = new ParentMessageBLL();
        //GridViewAttach.DataSource = objBussines.BindMessageAttachments(Int32.Parse(Session["ParentMessageID"].ToString()));
        //GridViewAttach.DataBind();
    }

    protected void gvwParentMessage_BeforePerformDataSelect(object sender, EventArgs e)
    {
        gvwParentMessage.Focus();
    }

    protected void gvwParentMessage_FocusedRowChanged(object sender, EventArgs e)
    {

    }

    protected void gvwParentMessage_SelectionChanged(object sender, EventArgs e)
    {
        gvwParentMessage.Focus();

        int i = gvwParentMessage.FocusedRowIndex;




    }

    protected void gvwMessageExchange_BeforePerformDataSelect(object sender, EventArgs e)
    {
        if (Session["ParentMessageID"] == null)
        {
            Session["ParentMessageID"] = (sender as ASPxGridView).GetMasterRowKeyValue();
            Int32 parentID = Int32.Parse(Session["ParentMessageID"].ToString());
        }
        else
        {
            Int32 parentID = Int32.Parse(Session["ParentMessageID"].ToString());
        }
    }


    protected void gvwParentMessage_DetailRowExpandedChanged1(object sender, ASPxGridViewDetailRowEventArgs e)
    {
        Session["RowIndex"] = "";
        int temp = e.VisibleIndex;
        Session["RowIndex"] = temp;
        //displayMessageExchange();
        setSecutiryPageAccess();
        LoadSupervisorRequestResponse();
    }

    protected void gvwParentMessage_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        string key = e.Result.ToString();// Argument.ToString();

        e.Result = DoFindTemp(Convert.ToInt32(key));

        //tabs.TabPages[0].Visible = true;
    }

    //save message Exchange
    protected void lnkNewMessageExchange_Click(object sender, EventArgs e)
    {
        txtExchangeCase.Text = string.Empty;
        txtExchangeText.Text = string.Empty;
        Session["IsNewMessageExchange"] = true;
        Session["IsMNew"] = true;
        popNewMessageExchange.ShowOnPageLoad = true;
        setSecutiryPageAccess();
        //
        if ((HttpContext.Current.User.IsInRole(("Inspection Director"))))
        {

            btnCompletAnswer.ClientVisible = true;
        }
        else
        {
            btnCompletAnswer.ClientVisible = false;
        }

    }

    public void UpdateParentMessageStatus()
    {
        MessageExchangeBLL objtblMessageExchangeBussiness = new MessageExchangeBLL();
        objtblMessageExchangeBussiness.UpdateParentMessageStatus(Int32.Parse(Session["ParentMessageID"].ToString()));
    }

    protected void LoadSupervisorRequestResponse()
    {
        int rowIndex = (int)Session["RowIndex"];
        ASPxPageControl pageControl =
            gvwParentMessage.FindDetailRowTemplateControl(rowIndex, "pagecontrol") as ASPxPageControl;
        if (pageControl != null) //True when called from Page_Load 
        {
            ASPxGridView MessageExchange = (ASPxGridView)pageControl.TabPages[2].FindControl("gvwMessageExchange");
            MessageExchangeBLL objMessageExchange = new MessageExchangeBLL();
            MessageExchange.DataSource = objMessageExchange.GetMessages(Convert.ToInt32(Session["ParentMessageID"]));
            MessageExchange.DataBind();

        }
    }

    protected bool DoSaveMessageExchange()
    {
        MessageExchangeBO objtblMessageExchange = new MessageExchangeBO();
        try
        {
            objtblMessageExchange.ParentID = Int32.Parse(Session["ParentMessageID"].ToString());
            objtblMessageExchange.OrgGuid = Guid.Parse(Session["ProfileOrgGuid"].ToString());//handle from session
            objtblMessageExchange.EmployeeName = Session["ProfileFullName"].ToString();
            objtblMessageExchange.Subject = this.txtExchangeCase.Text;
            objtblMessageExchange.Text = this.txtExchangeText.Text;
            objtblMessageExchange.UserName = Session["ProfileFullName"].ToString();
            objtblMessageExchange.EventDatetime = DateTime.Today;
            objtblMessageExchange.UpdatedUsername = Session["ProfileFullName"].ToString();
            objtblMessageExchange.UpdatedEventDatetime = DateTime.Today;


            MessageExchangeBussiness objtblMessageExchangeBussiness = new MessageExchangeBussiness();
            if ((bool)Session["IsNewMessageExchange"])
            {
                objtblMessageExchangeBussiness.InserttblMessageExchange(objtblMessageExchange);
            }
            else
            {
                objtblMessageExchange.UpdatedUsername = Session["ProfileFullName"].ToString();
                objtblMessageExchange.UpdatedEventDatetime = DateTime.Today;
                objtblMessageExchange.ID = Int32.Parse(Session["MessageExchangeID"].ToString());
                objtblMessageExchangeBussiness.UpdatetblMessageExchange(objtblMessageExchange);
            }
            Session["IsNewMessageExchange"] = false;
            popNewMessageExchange.ShowOnPageLoad = false;
            int rowIndex = (int)Session["RowIndex"];
            ASPxPageControl pageControl = gvwParentMessage.FindDetailRowTemplateControl(rowIndex, "pagecontrol") as ASPxPageControl;
            if (pageControl != null) //True when called from Page_Load 
            {
                ASPxGridView MessageExchange = (ASPxGridView)pageControl.TabPages[2].FindControl("gvwMessageExchange");
                MessageExchangeBLL objMessageExchange = new MessageExchangeBLL();
                MessageExchange.DataSource = objMessageExchange.GetMessages(Convert.ToInt32(Session["ParentMessageID"]));
                MessageExchange.DataBind();
            }
            Msg.ShowSuccess(Resources.Message.MSG_SAVED);

            return true;
        }
        catch (Exception ex)
        {
            Msg.ShowError("Error: " + ex.Message);
            return false;
        }
    }

    protected void lnkSelectMessageExchange_Click(object sender, EventArgs e)
    {
        //select and display for update
        txtExchangeCase.Text = string.Empty;
        txtExchangeText.Text = string.Empty;
        Session["IsNewMessageExchange"] = false;
        popNewMessageExchange.ShowOnPageLoad = true;
        setSecutiryPageAccess();

        MessageExchangeBussiness objMsgBusiness = new MessageExchangeBussiness();
        MessageExchangeBO objMsgBO = new MessageExchangeBO();
        try
        {
            //int rowIndex = ;
            int rowIndex = (int)Session["RowIndex"];
            ASPxPageControl pageControl = gvwParentMessage.FindDetailRowTemplateControl(rowIndex, "pagecontrol") as ASPxPageControl;
            if (pageControl != null) //True when called from Page_Load 
            {
                ASPxGridView MessageExchange = (ASPxGridView)pageControl.TabPages[2].FindControl("gvwMessageExchange");
                if (MessageExchange != null)
                {
                    MessageExchange.Focus();
                    int i = MessageExchange.FocusedRowIndex;
                    string ri = MessageExchange.GetRowValues(i, "ID").ToString();
                    Session["MessageExchangeID"] = ri;

                    objMsgBO = (MessageExchangeBO)objMsgBusiness.GettblMessageExchange(Int32.Parse(Session["MessageExchangeID"].ToString()));
                    if (objMsgBO == null)
                    {
                        Msg.ShowInfo(Resources.Message.MSG_NORECORD);

                    }
                    txtExchangeCase.Text = objMsgBO.Subject;
                    txtExchangeText.Text = objMsgBO.Text;
                }
            }

        }
        catch (Exception ex)
        {
            Msg.ShowError("Error " + ex.Message);
        }

        popNewMessageExchange.ShowOnPageLoad = true;
    }

    //delete message exchange
    protected void lnkDeleteMessageExchange_Click(object sender, EventArgs e)
    {
        MessageExchangeBussiness objtblMessageExchangeBussiness = new MessageExchangeBussiness();
        try
        {
            int rowIndex = (int)Session["RowIndex"];
            ASPxPageControl pageControl = gvwParentMessage.FindDetailRowTemplateControl(rowIndex, "pagecontrol") as ASPxPageControl;
            if (pageControl != null) //True when called from Page_Load 
            {
                ASPxGridView MessageExchange = (ASPxGridView)pageControl.TabPages[2].FindControl("gvwMessageExchange");
                if (MessageExchange != null)
                {
                    MessageExchange.Focus();
                    int i = MessageExchange.FocusedRowIndex;
                    string ri = MessageExchange.GetRowValues(i, "ID").ToString();
                    Session["MessageExchangeID"] = ri;
                    objtblMessageExchangeBussiness.Delete(Int32.Parse(Session["MessageExchangeID"].ToString()));
                    Session["IsNewMessageExchange"] = false;
                    Msg.ShowSuccess("Record was deleted successfully.");
                }
            }


        }
        catch (Exception ex)
        {
            Msg.ShowError(ex.Message);
        }
    }

    protected void Tree_DataBinding(object sender, EventArgs e)
    {

    }

    protected void gvwMessageExchange_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "EventDateTime")
            if ((e.Value).ToString().Length != 0)
            {
                string ethiodate;
                ethiodate = EthiopicDateTime.GetEthiopicDate(((DateTime)(e.Value)).Day, ((DateTime)(e.Value)).Month, ((DateTime)(e.Value)).Year);
                e.DisplayText = TranslateEthiopicDateMonth(ethiodate);
            }
            else
            {
                { return; }
            }

    }

    public string TranslateEthiopicDateMonth(string ethiopicDate)
    {
        //To display date with the following format "month day, year"
        string[] splitedDate = ethiopicDate.ToString().Split(new char[] { '/', ' ' });
        //Write ethiopic date by the following format "monthName day, year"
        string translatedMonth = "";
        switch (splitedDate[1])
        {
            case "1":
                translatedMonth = "መስከረም";
                break;
            case "2":
                translatedMonth = "ጥቅምት";
                break;
            case "3":
                translatedMonth = "ህዳር";
                break;
            case "4":
                translatedMonth = "ታህሳስ";
                break;
            case "5":
                translatedMonth = "ጥር";
                break;
            case "6":
                translatedMonth = "የካቲት";
                break;
            case "7":
                translatedMonth = "መጋቢት";
                break;
            case "8":
                translatedMonth = "ሚያዚያ";
                break;
            case "9":
                translatedMonth = "ግንቦት";
                break;
            case "10":
                translatedMonth = "ሰኔ";
                break;
            case "11":
                translatedMonth = "ሐምሌ";
                break;
            case "12":
                translatedMonth = "ነሐሴ";
                break;
            case "13":
                translatedMonth = "ጳግሜ";
                break;
        }

        return ethiopicDate = translatedMonth + " " + splitedDate[0] + " , " + splitedDate[2]; ;
    }

    protected void gvwParentMessage_PageIndexChanged(object sender, EventArgs e)
    {
        displayParentMessages();
    }

    protected void btnCompleteMessage_Click(object sender, EventArgs e)
    {
        if (chkParentMessageCompleted.Checked)
        {
            UpdateParentMessageFllowStatus(2);
            popParentMessageCompleted.ShowOnPageLoad = false;

            Msg.ShowSuccess("መልእክቱ በትክክል ተልከዋል!!");

        }
    }

    protected void lnkCompletMessage_Click(object sender, EventArgs e)
    {
        if (Session["ParentMessageID"] != null)
        {
            popParentMessageCompleted.ShowOnPageLoad = true;
            chkParentMessageCompleted.Checked = true;

            lblQuestionedBy.Text = "የጠየቀው ሙሉ ስም: " + Session["ProfileFullName"].ToString();
            lblQuestionedDate.Text = "ቀን: " + EthiopicDateTime.GetEthiopicDate(int.Parse(DateTime.Now.Day.ToString()),
                                                  int.Parse(DateTime.Now.Month.ToString()),
                                                  int.Parse(DateTime.Now.Year.ToString()));
        }
        else
            Msg.ShowSuccess(" እባክዎ መጀመሪያ ዋና ጥያቄውን ይምረጡ!");
    }

    public void UpdateParentMessageFllowStatus(Int32 status)
    {
        MessageExchangeBLL objtblMessageExchangeBussiness = new MessageExchangeBLL();
        String FinalRevisedBy = Session["ProfileFullName"].ToString();//from profile
        if (CAccessRight.CanAccess(CResource.eResource.ehrofficer) || CAccessRight.CanAccess(CResource.eResource.eHRSupervisor))
        {
            objtblMessageExchangeBussiness.UpdateParentMessageFllowStatus(Int32.Parse(Session["ParentEditID"].ToString()), FinalRevisedBy, status);
        }
        else
        {
            if (Session["ParentMessageID"] != null)
                objtblMessageExchangeBussiness.UpdateParentMessageFllowStatus(Int32.Parse(Session["ParentMessageID"].ToString()), FinalRevisedBy, status);
        }

    }

    protected void btnCompleteMessageNo_Click(object sender, EventArgs e)
    {
        popParentMessageCompleted.ShowOnPageLoad = false;
    }

    protected void btnCompletAnswerNo_Click(object sender, EventArgs e)
    {
        popParentMessageAnswerCompleted.ShowOnPageLoad = false;
        setSecutiryPageAccess();
    }

    protected void btnCompletAnswer_Click(object sender, EventArgs e)
    {
        ParentMessageBLL obj = new ParentMessageBLL();
        if (Session["ParentMessageID"] != null)
        {
            bool approved = obj.MessageApproved(Int32.Parse(Session["ParentMessageID"].ToString()));
            if (chkCompleted.Checked)
            {
                if (approved == false)
                {
                    DoSaveMessageExchange();
                    UpdateParentMessageFllowStatus(4);
                    popParentMessageAnswerCompleted.ShowOnPageLoad = false;
                    Msg.ShowSuccess("መልሱ በትክክል ተልከዋል!!");
                }
                else
                {
                    Msg.ShowSuccess("ጥያቄው ከዚህ በፊት መልስ ተሰጥቶታል!!");

                }
            }
            setSecutiryPageAccess();
        }
    }

    protected void lnkCompletAnswer_Click(object sender, EventArgs e)
    {
        if (Session["ParentMessageID"] != null)
        {
            popParentMessageAnswerCompleted.ShowOnPageLoad = true;
            chkParentMessageAnswerCompleted.Checked = true;

            lblAnsweredBy.Text = "ሙሉ ስም: " + Session["ProfileFullName"].ToString();
            lblAnsweredDate.Text = "ቀን: " + EthiopicDateTime.GetEthiopicDate(int.Parse(DateTime.Now.Day.ToString()),
                                                  int.Parse(DateTime.Now.Month.ToString()),
                                                  int.Parse(DateTime.Now.Year.ToString()));
        }
        else
            Msg.ShowSuccess(" እባክዎ መጀመሪያ ዋና ጥያቄውን ይምረጡ!");
    }

    protected void gvwParentMessage_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {

        string ethiodate;
        object status = 0;
        if (e.Column.FieldName == "SentDate")
            if ((e.Value).ToString().Length != 0)
            {
                if (DateTime.Parse(e.Value.ToString()).ToShortDateString() == DateTime.MaxValue.ToShortDateString())
                {
                    e.DisplayText = "";
                }
                else
                {
                    ethiodate = EthiopicDateTime.GetEthiopicDate(((DateTime)(e.Value)).Day, ((DateTime)(e.Value)).Month, ((DateTime)(e.Value)).Year);
                    e.DisplayText = TranslateEthiopicDateMonth(ethiodate);
                }
            }
            else
            {
                { return; }
            }
        if (e.Column.FieldName == "Case")
        {
            tblLookupBussiness lookupBusiness = new tblLookupBussiness();
            tblLookup lookup = lookupBusiness.GettblLookup(Convert.ToInt32(e.Value));
            e.DisplayText = lookup.Amdescription;
        }

        if (e.Column.FieldName == "AnsweredDate")
            if ((e.Value).ToString().Length != 0)
            {

                if (DateTime.Parse(e.Value.ToString()).ToShortDateString() == DateTime.MaxValue.ToShortDateString())
                {
                    e.DisplayText = "";
                }
                else
                {
                    ethiodate = EthiopicDateTime.GetEthiopicDate(((DateTime)(e.Value)).Day, ((DateTime)(e.Value)).Month, ((DateTime)(e.Value)).Year);
                    e.DisplayText = TranslateEthiopicDateMonth(ethiodate);
                }

            }
            else
            {
                { return; }
            }

        if (CAccessRight.CanAccess(CResource.eResource.einspectorofficer) || CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) || HttpContext.Current.User.IsInRole("Inspection Director"))
        {
            if (e.Column.FieldName == "FlowStatus")

                if ((e.Value).ToString().Length != 0)
                {
                    Session["status"] = e.Value;
                    if (e.Value.ToString() == "ተልከዋል")
                    {
                        e.DisplayText = "ጥያቄ ቀርበዋል";
                    }
                }
        }
        else if (CAccessRight.CanAccess(CResource.eResource.ehrofficer) || CAccessRight.CanAccess(CResource.eResource.eHRSupervisor))
        {
            if (e.Column.FieldName == "FlowStatus")
            {
                if ((e.Value).ToString() == "መልስ ተሰጥቶታል")
                {
                    Session["status"] = e.Value;
                }
                else
                {
                    Session["status"] = "";
                }
            }
        }





    }

    protected void fuDocument_DataBinding(object sender, EventArgs e)
    {

    }

    protected void btnFillAttachment_Click(object sender, EventArgs e)
    {
        ParentMessageBLL objBussines = new ParentMessageBLL();
        AttachmentMessageExchangeBO objAttachment = new AttachmentMessageExchangeBO();
        objAttachment.MainGuid = Guid.NewGuid();
        objAttachment.Url = objAttachment.MainGuid.ToString();
        objAttachment.ID = Int32.Parse(Session["ParentMessageID"].ToString());

        objAttachment.UserName = Session["ProfileFullName"].ToString();
        objAttachment.UpdatedEventDatetime = DateTime.Today;
        objAttachment.EventDatetime = DateTime.Today;
        objAttachment.UpdatedUsername = Session["ProfileFullName"].ToString();

        //attachment

        //objAttachment.Url = objAttachment.MainGuid.ToString() + fuDocument.FileName;
        //objAttachment.txtFileDescription = txtFileDescription.Text;

        //if (FileUpload1.PostedFile != null)
        //{
        //    objAttachment.Url = objAttachment.MainGuid.ToString() + FileUpload1.PostedFile.FileName;
        //    objAttachment.txtFileDescription = txtFileDescription.Text;
        //    // + fuDocument.FileName
        //    FileUpload1.PostedFile.SaveAs(Server.MapPath("~/AttachedFile/") + objAttachment.Url);
        //    objBussines.InserttblMessageExchangeAttachment(objAttachment);
        //    txtFileDescription.Text = string.Empty;
        //    GridViewAttach.DataSource = objBussines.BindMessageAttachments(Int32.Parse(Session["ParentMessageID"].ToString()));
        //    GridViewAttach.DataBind();

        //    Msg.ShowSuccess("ፋይሉ በትክክል ተያይዘዋል!");

        //}
        //else
        //    Msg.ShowInfo("እባክዎ ፋይሉን ይምረጡ!");
        setSecutiryPageAccess();

    }

    protected void gvwParentMessage_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        var buttonEdit = (ASPxButton)gvwParentMessage.FindRowCellTemplateControl(e.VisibleIndex, null, "lnkMessageResponse");
       // var buttonNew = (ASPxGridView)gvwMessageExchange.FindRowCellTemplateControl(e.VisibleIndex, null, "lnkNewMessageExchange");

        //      int rowIndex = (int)Session["RowIndex"];
        ASPxPageControl pageControl = gvwParentMessage.FindDetailRowTemplateControl(e.VisibleIndex, "pagecontrol") as ASPxPageControl;
     

        string Status = Convert.ToString(e.GetValue("FStatus"));
        if (CAccessRight.CanAccess(CResource.eResource.einspectorofficer) || CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) || CAccessRight.CanAccess(CResource.eResource.einspectordirector))
        {
            if (Status == "2")
            {
            //    e.Row.BackColor = Color.LightCyan; // Changes The BackColor of ENTIRE ROW
              //  e.Row.ForeColor = Color.DarkRed; // Change the Font Color of ENTIRE ROW
            }
            else if (Status == "4")
            {
                //e.Row.BackColor = Color.LightGray;
            }
        }
        else if (CAccessRight.CanAccess(CResource.eResource.ehrofficer) || CAccessRight.CanAccess(CResource.eResource.eHRSupervisor))
        {
            if (Status == "0")
            {
                e.Row.BackColor = Color.PaleGoldenrod;
            }
            else if (Status == "2")
            {
                e.Row.BackColor = Color.LightCyan; // Changes The BackColor of ENTIRE ROW
                e.Row.ForeColor = Color.DarkRed; // Change the Font Color of ENTIRE ROW
            }
            else if (Status == "4")
            {
                if (pageControl != null) //True when called from Page_Load 
                {
                            // ASPxGridView MessageExchange = (ASPxGridView)pageControl.TabPages[2].FindControl("gvwMessageExchange");
                            //   MessageExchange.Columns[2].Visible = false;
                            //  var buttonNew = (ASPxButton)MessageExchange.FindRowCellTemplateControl(e.VisibleIndex, null, "lnkNewMessageExchange");

                            //buttonNew.Visible = false;
             
                        e.Row.BackColor = Color.LightGray;
                        ASPxGridView MessageExchange = (ASPxGridView)pageControl.TabPages[2].FindControl("gvwMessageExchange");
                        MessageExchange.Columns[2].Visible = false;
             
                }

            }
        }
        //
        if (pageControl != null) //True when called from Page_Load 
        {
            if (HttpContext.Current.User.IsInRole("Inspection officer") || HttpContext.Current.User.IsInRole("Inspection Supervisor"))
            {
                if (Status == "4" || gvwParentMessage.GetRowValues(e.VisibleIndex, "FlowStatus").ToString() == "መልስ ተሰጥቶታል")
                {
                    ASPxGridView MessageExchange = (ASPxGridView)pageControl.TabPages[2].FindControl("gvwMessageExchange");
                    MessageExchange.Columns[2].Visible = false;
                }
            }
        }
            //
            if (HttpContext.Current.User.IsInRole("HR Officer"))
        {
            if (Status == "4" || gvwParentMessage.GetRowValues(e.VisibleIndex, "FlowStatus").ToString() == "መልስ ተሰጥቶታል")
            {

                //      e.Row.BackColor = Color.LightGray;

                if (e.Row.Cells.Count == 10)
                    e.Row.Cells[8].Enabled = false;

            }
        }





        
        if (Status == "0")
        {
            if (e.Row.Cells.Count == 10)
            {
                buttonEdit.Text = "የቀረበ ጥያቄ ";
            }
            //if (e.Row.Cells.Count == 10)
            //    e.Row.Cells[9].Text = "የቀረበ ጥያቄ";
            // gvwParentMessage.Columns[9].IsClickable=true
        }
        if (Status == "2")
        {
            if (e.Row.Cells.Count == 10)
            {
                buttonEdit.Text = "የውሳኔ ሀሳብ "; 
            }
        }
        else if (Status == "4")
        {
            if (e.Row.Cells.Count == 10)
                buttonEdit.Text = "የተሰጠ ውሳኔ ";

        }

        //
        if (Status != "4" && (HttpContext.Current.User.IsInRole("HR Officer") || HttpContext.Current.User.IsInRole("HR Supervisor")))
        {
            if (e.Row.Cells.Count == 10)
                buttonEdit.ClientEnabled= false;
            //if (e.Row.Cells.Count == 10)
            //    e.Row.Cells[9].Enabled = true;
            //if (e.Row.Cells.Count == 10)
            //    e.Row.Cells[9].Text = "የተሰጠ ውሳኔ";

        }
        else
        {
            //if (e.Row.Cells.Count == 10)
            //    e.Row.Cells[9].Enabled = true;
            //if (e.Row.Cells.Count == 10)
            //    e.Row.Cells[9].Text = "የውሳኔ ሀሳብ";
        }

    }

    protected void ASPxButton2_Click1(object sender, EventArgs e)
    {

    }

    protected void ddlCase_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnShowApprovedReport_Click(object sender, EventArgs e)
    {


    }

    protected void btnSaveMessageExchange_Click(object sender, EventArgs e)
    {
        try
        {

            DoSaveMessageExchange();
            //////displayMessageExchange();

            if (chkCompleted.Checked)
            {
                //call update status of parent Message
                UpdateParentMessageStatus();
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError(" እባክዎ መጀመሪያ መልእክቱን ይምረጡ! ");
        }
    }

    protected void btnSaveMessageExchange_Click1(object sender, EventArgs e)
    {
        try
        {
            DoSaveMessageExchange();
            ////displayMessageExchange();

            if (chkCompleted.Checked)
            {
                //call update status of parent Message
                UpdateParentMessageStatus();
            }
        }
        catch (Exception ex)
        {
            Msg.ShowError(" እባክዎ መጀመሪያ መልእክቱን ይምረጡ! ");
        }
    }

    private List<string> GetErrorMessage()
    {
        List<string> errMessages = new List<string>();
        if (ddlCase.Value.ToString() == "" && ddlCase.Value != null)
        {
            errMessages.Add("እባክዎ ጉዳዩን ይምረጡ");
        }

        if (txtMessage.Text == string.Empty || txtMessage.Text == "")
        {
            errMessages.Add("እባክዎ የመልክቱን ይዘት ያስገቡ");
        }

        return errMessages;
    }

    protected void btnsearch_Click(object sender, EventArgs e)
    {
        string condition = "";
        if (orgcombochk())
        {
            Session["ServingOrgGuid"] = ComboOrg.Value.ToString();

            condition = " AND tblInspectionParentMessage.OrgGuid= '" + ComboOrg.Value.ToString() + "'";
        }

        if (checkdate())
        {
            condition = condition + "  AND tblInspectionParentMessage.EventDatetime BETWEEN '" + cdpFromDate.SelectedGCDate.ToShortDateString() + "' AND '" + cdpToDate.SelectedGCDate.ToShortDateString() + "' ";
        }

        ParentMessageBLL objMessageBussiness = new ParentMessageBLL();

        if (CAccessRight.CanAccess(CResource.eResource.einspectorofficer) || CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) || CAccessRight.CanAccess(CResource.eResource.einspectordirector))
        {
            condition = condition + "AND (FlowStatus=2 OR FlowStatus=4)  ";
            Session["parentMessageCondition"] = condition;

            gvwParentMessage.DataSource = objMessageBussiness.GetParentMessages(Session["parentMessageCondition"].ToString());
            gvwParentMessage.DataBind();
        }

        setSecutiryPageAccess();
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        Page.Validate("question");
        if (Page.IsValid)
        {
            DoSave();
            ClearForm();
        }
    }

    protected void ASPxButton3_Click(object sender, EventArgs e)
    {

    }

    protected void UploadControl_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
    {
        try
        {
            e.CallbackData = SavePostedFiles(e.UploadedFile);
        }
        catch (Exception ex)
        {
            e.IsValid = false;
            e.ErrorText = ex.Message;
        }
    }

    DataTable dtAttachment = new DataTable();
    int count = 0;

    string SavePostedFiles(UploadedFile uploadedFile)
    {

        if (!uploadedFile.IsValid)
            return string.Empty;



        FileInfo fileInfo = new FileInfo(uploadedFile.FileName);

        string resFileName = MapPath(UploadDirectory) + fileInfo.Name;
        uploadedFile.SaveAs(resFileName);
        CompliantAttachment objCompliantAttachment = new CompliantAttachment();



        if (count == 0)
        {
            System.Type typeString = System.Type.GetType("System.String");
            System.Type typeLong = System.Type.GetType("System.Int64");
            System.Type typeGuid = System.Type.GetType("System.Guid");
            dtAttachment.Columns.Add("AttachmentGuid", typeString);
            dtAttachment.Columns.Add("FileNumber", typeString);
            dtAttachment.Columns.Add("AttachmentTitle", typeString);
            dtAttachment.Columns.Add("DocumentTitle", typeString);
            dtAttachment.Columns.Add("FileSize", typeString);
            dtAttachment.Columns.Add("FileUrl", typeString);
            dtAttachment.Columns.Add("FileType", typeString);

        }

        count++;
        //DataRow dr;

        dtAttachment.Rows.Add(new Object[] { Guid.Empty, "test1", fileInfo.Name, fileInfo.Name, uploadedFile.ContentLength / 1024, fileInfo.Extension, fileInfo.Extension });
        UploadingUtils.RemoveFileWithDelay(uploadedFile.FileName, resFileName, 5);
        string fileLabel = fileInfo.Name;
        string fileLength = uploadedFile.ContentLength / 1024 + "K";

        fuAttachment.SaveAs(Server.MapPath(Path.Combine("~/AttachedFile/", fileLabel)));

        //Session["attachedFiles"] = dtAttachment;
        Session["attachedFiles"] = dtAttachment;

        return string.Format("{0} ({1})|{2}", fileLabel, fileLength, fileInfo.Name);

    }

    //protected void btnnew_Click(object sender, EventArgs e)
    //{
    //    pnlnewmessage.Visible = true;
    //}
    protected void lkbtnNew_Click(object sender, EventArgs e)
    {
        //btnCompletAnswer.Visible = false;
        pupmessage.ShowOnPageLoad = true;
        Session["IsMNew"] = true;
        LoadRequestType(ddlCase);
    }

    protected bool SaveAttachment()
    {
        bool saved = false;
        MessageExchangeBO objMessageExchangeBO = new MessageExchangeBO();
        ParentMessageBLL objBussines = new ParentMessageBLL();
        objMessageExchangeBO = objBussines.GetMaxParentID();
        DataTable gridViewAttachment = (DataTable)Session["attachedFiles"];
        if (gridViewAttachment != null)
        {
            for (int index = 0; index < gridViewAttachment.Rows.Count; index++)
            {
                if (gridViewAttachment.Rows[index]["AttachmentGuid"].ToString().Equals(Guid.Empty.ToString()))
                {

                    AttachmentMessageExchangeBO objAttachment = new AttachmentMessageExchangeBO();
                    string fileUrl = gridViewAttachment.Rows[index]["AttachmentTitle"].ToString();
                    objAttachment.MainGuid = Guid.NewGuid();
                    objAttachment.Url = fileUrl;
                    objAttachment.ID = Int32.Parse(objMessageExchangeBO.ParentMessageID.ToString());
                    objAttachment.UserName = Session["ProfileFullname"].ToString();
                    objAttachment.UpdatedEventDatetime = DateTime.Today;
                    objAttachment.EventDatetime = DateTime.Today;
                    objAttachment.UpdatedUsername = Session["ProfileFullName"].ToString();


                    if (objBussines.InserttblMessageExchangeAttachment(objAttachment))
                    {
                        saved = true;
                    }
                }
            }
        }


        return saved;
    }

    protected void DvgvEmployment_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["ParentMessageID"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }

    protected void DvgvEmployment_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {

    }

    protected void DvgvEmployment_SelectionChanged(object sender, EventArgs e)
    {

    }

    protected void btnView3_Click(object sender, EventArgs e)
    {
        DoFindAttachment();
    }

    protected bool DoFindAttachment()
    {
        try
        {

            //AttachmentMessageExchangeBO objAttachment = new AttachmentMessageExchangeBO();
            //ParentMessageBLL objAttachmentBusiness = new ParentMessageBLL();
            //objAttachment = objAttachmentBusiness.g(Guid.Parse(Session["AMainGuid"].ToString()));
            DvgvAttachment.Focus();
            int i = DvgvAttachment.FocusedRowIndex;
            string strURL = DvgvAttachment.GetRowValues(i, "Url").ToString();

            string filePath = "~/AttachedFile/" + strURL;
            Response.AppendHeader("Content-Disposition", "AttachedFile; filename=" + strURL);
            // Write the file to the Response
            Response.TransmitFile(Server.MapPath(filePath));
            Response.End();
            //WebClient req = new WebClient();
            //HttpResponse response = HttpContext.Current.Response;
            //response.Clear();
            //response.ClearContent();
            //response.ClearHeaders();
            //response.Buffer = true;
            //response.AddHeader("Content-Disposition", "AttachedFile;filename=\""+ strURL + "\"");
            //byte[] data = req.DownloadData(Server.MapPath(strURL));
            //response.BinaryWrite(data);
            //response.End();

        }
        catch
        {
            //MessageBox1.ShowInfo("ሰነዱ በቦታው አልተገኘም");
        }

        return true;
    }

    protected void DvGQuestion_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["ParentID"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }

    protected void gvwParentMessage_PageIndexChanged1(object sender, EventArgs e)
    {
        try
        {
            string condition = "";
            ParentMessageBLL objMessageBussiness = new ParentMessageBLL();

            if (CAccessRight.CanAccess(CResource.eResource.einspectorofficer) || CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) || HttpContext.Current.User.IsInRole("Inspection Director"))
            {
                condition = condition + "AND (FlowStatus=2 OR FlowStatus=4)  ";
                Session["parentMessageCondition"] = condition;

                gvwParentMessage.DataSource = objMessageBussiness.GetParentMessages(Session["parentMessageCondition"].ToString());
                gvwParentMessage.DataBind();
            }
            else if (CAccessRight.CanAccess(CResource.eResource.ehrofficer))
            {
                gvwParentMessage.DataSource = objMessageBussiness.GetParentMessages(" AND Organization.OrgGuid= '" + Session["ProfileOrgGuid"].ToString() + "'");
                gvwParentMessage.DataBind();

            }
            else
            {

            }




        }
        catch { }
    }

    protected void gvwParentMessage_Load(object sender, EventArgs e)
    {
        if (CAccessRight.CanAccess(CResource.eResource.einspectorofficer) || CAccessRight.CanAccess(CResource.eResource.einspectorsupervisor) || HttpContext.Current.User.IsInRole("Inspection Director"))
        {
            //EDit access not authorise;      

            gvwParentMessage.Columns[11].Visible = false;


        }
        else if (CAccessRight.CanAccess(CResource.eResource.ehrofficer))
        {
            //Edit Access 
            gvwParentMessage.Columns[11].Visible = true;
        }
    }

    protected void cbMessageExchange_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {
        string strID = string.Empty;
        string[] parameters = e.Parameter.Split('|');
        string strParam = parameters[0].ToString();
        if (e.Parameter.ToString().Contains('|'))
            strID = parameters[1].ToString();

        switch (strParam)
        {
            case "New":
                DoNew();
                break;
            case "List":
                DoFind(strID);
                //DevExpress.Web.Internal.PopupWindowClientStateUtils.LoadClientState(PopupWindow window, String state) +288
                //DevExpress.Web.ASPxPopupControl.LoadWindowsState(String state) +133
                //DevExpress.Web.ASPxWebControl.LoadPostDataInternal(NameValueCollection 
                //pupmessage.ShowOnPageLoad = true;
                break;
        }
    }

    protected void DoFind(string value)
    {
        // select and display for update
        txtExchangeCase.Text = string.Empty;
        txtExchangeText.Text = string.Empty;
        Session["IsNewMessageExchange"] = false;
        popNewMessageExchange.ShowOnPageLoad = true;
        setSecutiryPageAccess();

        MessageExchangeBussiness objMsgBusiness = new MessageExchangeBussiness();
        MessageExchangeBO objMsgBO = new MessageExchangeBO();
        try
        {
            //int rowIndex = ;
            int rowIndex = (int)Session["RowIndex"];
            ASPxPageControl pageControl = gvwParentMessage.FindDetailRowTemplateControl(rowIndex, "pagecontrol") as ASPxPageControl;
            if (pageControl != null) //True when called from Page_Load 
            {
                ASPxGridView MessageExchange = (ASPxGridView)pageControl.TabPages[2].FindControl("gvwMessageExchange");
                if (MessageExchange != null)
                {
                    MessageExchange.Focus();

                    int i = int.Parse(hfselectedrowinfo.Text.Trim());

                    //string someValue = MessageExchange.GetRowValues(MessageExchange.FocusedRowIndex, "ID").ToString();


                    //var row = MessageExchange.GetSelectedFieldValues("ID");

                    //// var t = row.Cells[0].Text;
                    ////      var t= MessageExchange.GetRowValues(MessageExchange.FocusedRowIndex, "ID").ToString();
                    //int i = MessageExchange.FocusedRowIndex;
                    string ri = MessageExchange.GetSelectedFieldValues("ID").ToString();
                    Session["MessageExchangeID"] = ri;

                    objMsgBO = (MessageExchangeBO)objMsgBusiness.GettblMessageExchange(Int32.Parse(Session["MessageExchangeID"].ToString()));
                    if (objMsgBO == null)
                    {
                        Msg.ShowInfo(Resources.Message.MSG_NORECORD);

                    }
                    txtExchangeCase.Text = objMsgBO.Subject;
                    txtExchangeText.Text = objMsgBO.Text;
                }
            }

        }
        catch (Exception ex)
        {
            Msg.ShowError("Error " + ex.Message);
        }

        popNewMessageExchange.ShowOnPageLoad = true;
    }

    protected void ASPxButton1_Click1(object sender, EventArgs e)
    {
        pupmessage.ShowOnPageLoad = true;
    }

    protected void EDIT_Click(object sender, EventArgs e)
    {
        if (HttpContext.Current.User.IsInRole(("Inspection Director")))
        {
            //btnCompletAnswer.ClientVisible = true;
        }
        else
        {
            // btnCompletAnswer.ClientVisible = false;
        }
        pupmessage.ShowOnPageLoad = true;
        if (HttpContext.Current.User.IsInRole(("HR Supervisor")))
        {
            btnApproval.Visible = true;
        }
        DoFindParent();
    }

    protected void btnApproval_Click(object sender, EventArgs e)
    {
        UpdateParentMessageFllowStatus(2);

        Msg.ShowSuccess("መልእክቱ በትክክል ተልከዋል!!");
        popParentMessageCompleted.ShowOnPageLoad = false;
        BindGrid();
    }

    protected void btnView2_Click(object sender, EventArgs e)
    {
        try
        {
            int rowIndex = (int)Session["RowIndex"];
            ASPxPageControl pageControl = gvwParentMessage.FindDetailRowTemplateControl(rowIndex, "pagecontrol") as ASPxPageControl;
            if (pageControl != null) //True when called from Page_Load 
            {
                ASPxGridView DvAttachment = (ASPxGridView)pageControl.TabPages[0].FindControl("DvgvEmployment");
                if (DvAttachment != null)
                {
                    DvAttachment.Focus();
                    int i = DvAttachment.FocusedRowIndex;
                    string strURL = DvAttachment.GetRowValues(i, "Url").ToString();
                    string filePath = "~/AttachedFile/" + strURL;
                    Response.AppendHeader("Content-Disposition", "AttachedFile; filename=" + strURL);
                    // Write the file to the Response
                    Response.TransmitFile(Server.MapPath(filePath));
                    Response.End();
                    //DevExpress.Web.ASPxWebControl.RedirectOnCallback("~\\HR\\HR.aspx");       


                }
            }
        }
        catch
        {
            //MessageBox1.ShowInfo("ሰነዱ በቦታው አልተገኘም");
        }

    }

    protected void gvwParentMessage_SelectionChanged1(object sender, EventArgs e)
    {

    }

    protected void ASPxGridView1_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["ParentMessageID"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }

    protected void gvwMessageExchange_OnDataBound(object sender, EventArgs e)
    {
        //if (CAccessRight.CanAccess(CResource.eResource.eHRSupervisor) ||
        //      CAccessRight.CanAccess(CResource.eResource.ehrofficer))
        //{
        //    int rowIndex = (int)Session["RowIndex"];
        //    ASPxPageControl pageControl = gvwParentMessage.FindDetailRowTemplateControl(rowIndex, "pagecontrol") as ASPxPageControl;
        //    if (pageControl != null) //True when called from Page_Load 
        //    {
        //        ASPxGridView MessageExchange = (ASPxGridView)pageControl.TabPages[2].FindControl("gvwMessageExchange");
        //        ASPxHyperLink newLink = MessageExchange.FindHeaderTemplateControl(MessageExchange.Columns[6], "lnkNewMessageExchange") as ASPxHyperLink;
        //        //newLink.Visible = false;
        //        //GridViewDataColumn editButton = MessageExchange.Columns[3];
        //        //ASPxHyperLink selectLink = (ASPxHyperLink)MessageExchange.FindRowCellTemplateControl(e.VisibleIndex, editButton, "lnkSelectMessageExchange");
        //        ////selectLink.Visible = false;
        //        //LinkButton deleteLink = (LinkButton)MessageExchange.FindControl("lnkDeleteMessageExchange");
        //        ////deleteLink.Visible = false;
        //    }
        //}
    }

    protected void gvwParentMessage_HtmlRowCreated(object sender,
        DevExpress.Web.ASPxGridViewTableRowEventArgs e)
    {
        //if (e.RowType == GridViewRowType.Data)
        //{
        //    if (CAccessRight.CanAccess(CResource.eResource.ehrofficer) ||
        //        CAccessRight.CanAccess(CResource.eResource.eHRSupervisor))
        //    {
        //        if (Session["status"].ToString() == "መልስ ተሰጥቶታል")
        //        {
        //            e.Row.Cells[11].TemplateControl.Controls[0].Visible = true;
        //            //LinkButton requestResponse = (LinkButton) e.Row.Cells[11].FindControl("lnkMessageResponse");
        //            //requestResponse.Visible = false;
        //        }
        //    }
        //}
    }
    protected void gvwParentMessage_HtmlCommandCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableCommandCellEventArgs e)
    {
        //if (e.CommandCellType == DevExpress.Web.GridViewTableCommandCellType.Data)
        //{
        //    if (CAccessRight.CanAccess(CResource.eResource.ehrofficer) ||
        //        CAccessRight.CanAccess(CResource.eResource.eHRSupervisor))
        //    {
        //        if (Session["status"].ToString() == "መልስ ተሰጥቶታል")
        //        {
        //            e.CommandColumn..Cell.Controls[0].Visible = true;
        //            //LinkButton requestResponse = (LinkButton) e.Row.Cells[11].FindControl("lnkMessageResponse");
        //            //requestResponse.Visible = false;
        //        }
        //    }
        //}
    }

    protected void gvwParentMessage_OnRowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {

    }

    protected void lnkMessageResponse_OnClick(object sender, EventArgs e)
    {
        gvwParentMessage.Focus();
        int i = gvwParentMessage.FocusedRowIndex;
        string ri = gvwParentMessage.GetRowValues(i, "ParentMessageID").ToString();
        Session["ParentMessageID"] = ri;
        MessageExchangeBLL objMessageExchange = new MessageExchangeBLL();
        DataTable dtMessage = objMessageExchange.GetMessages(Convert.ToInt32(Session["ParentMessageID"].ToString()));
        if (dtMessage.Rows.Count > 0)
        {
            pnlResponse.Visible = true;
            gvwOfficerMessageResponse.DataSource = dtMessage;
            gvwOfficerMessageResponse.DataBind();
        }
    }



    protected void lnkEditmessageexchange_Click(object sender, EventArgs e)
    {

    }

    protected void gvwMessageExchange_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        string strID = string.Empty;
        string[] parameters = e.Parameters.Split('|');
        string strParam = parameters[0];
        if (e.Parameters.Contains('|'))
            strID = parameters[1];

        //
        //if (e.Parameters.Contains('|'))
        //    strID = parameters[1];


        string strAction = e.Parameters;

        if (strParam == "List")
        {
            //select and display for update
            txtExchangeCase.Text = string.Empty;
            txtExchangeText.Text = string.Empty;
            Session["IsNewMessageExchange"] = false;
         //   popNewMessageExchange.ShowOnPageLoad = true;
            setSecutiryPageAccess();

            MessageExchangeBussiness objMsgBusiness = new MessageExchangeBussiness();
            MessageExchangeBO objMsgBO = new MessageExchangeBO();
            try
            {
                //int rowIndex = ;
                int rowIndex = (int)Session["RowIndex"];
                ASPxPageControl pageControl = gvwParentMessage.FindDetailRowTemplateControl(rowIndex, "pagecontrol") as ASPxPageControl;
                if (pageControl != null) //True when called from Page_Load 
                {
                    ASPxGridView MessageExchange = (ASPxGridView)pageControl.TabPages[2].FindControl("gvwMessageExchange");
                    if (MessageExchange != null)
                    {
                        int SelectedIndex = int.Parse(hfselectedrowinfo.Text);
                        MessageExchange.Focus();
                        int i = MessageExchange.Columns[5].Collection.Count;
                        ASPxGridView detain = (ASPxGridView)sender;
                        //  string r8i = detain.GetRowValues(0, "ID").ToString();
                       // string ri = MessageExchange.GetSelectedFieldValues("ID").ToString();

                        string ri = MessageExchange.GetRowValues(SelectedIndex, "ID").ToString();
                        Session["MessageExchangeID"] = ri;

                        objMsgBO = (MessageExchangeBO)objMsgBusiness.GettblMessageExchange(Int32.Parse(Session["MessageExchangeID"].ToString()));
                        if (objMsgBO == null)
                        {
                            Msg.ShowInfo(Resources.Message.MSG_NORECORD);

                        }
                        txtExchangeCase.Text = objMsgBO.Subject;
                        txtExchangeText.Text = objMsgBO.Text;
                    }
                }

            }
            catch (Exception ex)
            {
                Msg.ShowError("Error " + ex.Message);
            }

            popNewMessageExchange.ShowOnPageLoad = true;

        }
    }



    protected void gvwMessageExchange_SelectionChanged(object sender, EventArgs e)
    {
        int rowIndex = (int)Session["RowIndex"];

        ASPxPageControl pageControl = gvwParentMessage.FindDetailRowTemplateControl(rowIndex, "pagecontrol") as ASPxPageControl;
        if (pageControl != null) //True when called from Page_Load 
        {
            ASPxGridView MessageExchange = (ASPxGridView)pageControl.TabPages[2].FindControl("gvwMessageExchange");
            string id;
            if (MessageExchange.Selection.Count > 0)
            {
                int i = int.Parse(hfselectedrowinfo.Text.Trim());
                string ri = MessageExchange.GetRowValues(i, "ID").ToString();
                //   id = MessageExchange.GetSelectedFieldValues("ProductID").ToString();
                //     string ri = MessageExchange.GetRowValues(i, "ID").ToString();
            }

        }
    }



}


