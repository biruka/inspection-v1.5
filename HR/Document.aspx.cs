﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CUSTOR.Bussiness;
using CUSTOR.Domain;
using CUSTOR;
using System.Text;
using System.Threading;
using DevExpress.Web;
using CUSTOR.Commen;

    public partial class Document : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Session["IsNew"] = false;
                BindGrid();

                pnlData.ClientVisible = false;
                btnSave.ClientEnabled = false;
                LoadHRActivity();
                //btnSaveClient.Enable= false;           

            }
        }

        private void BindGrid()
        {
            tblDocumentBussiness objtblDocumentBussiness = new tblDocumentBussiness();
            gvwtblDocument.DataSource = objtblDocumentBussiness.GettblDocuments();
            gvwtblDocument.DataBind();
            cboHRActivity.Value = null;
        }

        protected void LoadHRActivity()
        {
            cboHRActivity.ValueField = "MainGuid";
            cboHRActivity.TextField = "DescriptionAm";  
            tblDocumentTypeBussiness objtblDocumentTypeBussiness = new tblDocumentTypeBussiness();
            cboHRActivity.DataSource = objtblDocumentTypeBussiness.GettblDocumentTypes();
            cboHRActivity.DataBind();
        }

        protected override void InitializeCulture()
        {
            ProfileCommon p = this.Profile;
            if (p.Organization.LanguageID != null)
            {
                String selectedLanguage = string.Empty;
                switch (p.Organization.LanguageID)
                {
                    case 0: selectedLanguage = "en-US";//English
                        break;
                    case 2: selectedLanguage = "am-ET";//Afan Oromo
                        break;
                    case 3: selectedLanguage = "am-ET"; //Tig
                        break;
                    case 4: selectedLanguage = "en-GB";//afar
                        break;
                    case 5: selectedLanguage = "en-AU";//Somali
                        break;
                    default: break;//Amharic
                }
                UICulture = selectedLanguage;
                Culture = selectedLanguage;

                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(selectedLanguage);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(selectedLanguage);
            }
            base.InitializeCulture();
        }

        protected void mnuToolbar_MenuItemClick(object sender, MenuEventArgs e)
        {
            if (e.Item.Value.ToLower() == "new")
            {
                DoNew();
                //mnuToolbar.Items[(int)enumToolbar.eNew].Enabled = false;
                //mnuToolbar.Items[(int)enumToolbar.eSave].Enabled = true;
            }
            else if (e.Item.Value.ToLower() == "save")
            {
                Page.Validate();
                if (!Page.IsValid) return;
                if (DoSave())
                {
                    //mnuToolbar.Items[(int)enumToolbar.eNew].Enabled = true;
                    //mnuToolbar.Items[(int)enumToolbar.eSave].Enabled = false;
                    //mnuToolbar.Items[(int)enumToolbar.eDelete].Enabled = false;
                }
            }
            else if (e.Item.Value.ToLower() == "delete")
            {
                if (DoDelete())
                {
                    //mnuToolbar.Items[(int)enumToolbar.eNew].Enabled = true;
                    //mnuToolbar.Items[(int)enumToolbar.eSave].Enabled = false;
                    //mnuToolbar.Items[(int)enumToolbar.eDelete].Enabled = false;
                }
            }
            else if (e.Item.Value.ToLower() == "refresh")
            {
                ClearForm();
            }
        }

        protected bool IsInspectiondDoneByDocument(Guid docGuid)
        {
            bool canBeDeleted = true;
            InspectionFollowupBussiness objInspectionFollowup = new InspectionFollowupBussiness();
            if (objInspectionFollowup.GetRecordsByDocumentGuid(docGuid).Rows.Count > 0)
            {
                canBeDeleted = false;
            }

            return canBeDeleted; 
        }

        protected bool DoDelete()
        {
            bool deleted = true;
            tblDocumentBussiness objtblDocumentBussiness = new tblDocumentBussiness();
            try
            {
                Guid docGuid = (Guid)Session["ID"];
               //Check whether there are not inspection done by selected Document
                if (IsInspectiondDoneByDocument(docGuid))
                {
                    if (objtblDocumentBussiness.Delete(docGuid))
                    {
                        Session["IsNew"] = false;
                        pnlData.Visible = false;
                        BindGrid();
                        ShowSuccess(Resources.Message.MSG_DocumentDeleted);
                        deleted = true;
                    }
                }
                else
                {
                    ShowError(Resources.Message.MSG_DocumentRegisteredInInspection);
                    deleted = false;
                }
                
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
                
            }
            return deleted;
        }

        protected bool DoFind(string ID)
        {
            cboParentGuid.Value = "";
            tblDocumentBussiness objtblDocumentBussiness = new tblDocumentBussiness();

            tblDocument objtblDocument = new tblDocument();
            pnlData.ClientVisible = true;
            try
            {
                objtblDocument = objtblDocumentBussiness.GettblDocument(Guid.Parse(ID));
                LoadDocumentTypes();
                if (objtblDocument == null)
                {
                    ShowError(Resources.Message.Not_Found);
                    return false;
                }
                //Now Record was found;
                Session["IsNew"] = false;
                ClearForm();
                pnlData.Visible = true;
                this.txtCode.Text = objtblDocument.Code.ToString();
                this.txtDescription.Text = objtblDocument.Description.ToString();
                this.txtDescriptionAm.Text = objtblDocument.DescriptionAm.ToString();
                txtCode.ClientVisible = false;
                lblCode.ClientVisible = false;
                BindLookupGrid(objtblDocument.ParentGuid, 1);
                Grid_DataBinding(Guid.Parse(ID));
                this.ckIsoptional.Checked = objtblDocument.Isoptional;
                this.cboParentGuid.Text = objtblDocument.ParentGuid.ToString();
                Session["ID"] = ID;
                btnSave.ClientEnabled = true;
                return true;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
                return false;
            }
        }

        protected void ClearForm()
        {
            //this.cboMainGuid.SelectedIndex=0;
            BindLookupGrid(Guid.Empty, 1);
            this.txtCode.Text = string.Empty;
            this.txtDescription.Text = string.Empty;
            this.txtDescriptionAm.Text = string.Empty;
            this.cboParentGuid.SelectedIndex = 0;
            this.ckIsoptional.Checked = false;
            //this.txtIsoptional.Text=string.Empty;
        }

        protected bool DoNew()
        {
            try
            {
                Session["IsNew"] = true;
                pnlData.ClientVisible = true;
                LoadDocumentTypes();
                ClearForm();
                btnNew.Enabled = false;
                btnSave.ClientEnabled = true;
                txtCode.ClientVisible = false;
                lblCode.ClientVisible = false;
                cboParentGuid.Value = null;
                return true;
            }
            catch (Exception ex)
            {
                //ShowError(ex.Message);
                return false;
            }
        }

        protected bool DoSave()
        {
            bool saved = false;
            tblDocument objtblDocument = new tblDocument();
            try
            {
                List<string> errMessages = GetErrorMessage();
                if (errMessages.Count > 0)
                {
                    ShowError(GetErrorDisplay(errMessages));
                    saved = false;
                }
                objtblDocument.Description = this.txtDescription.Text;
                objtblDocument.DescriptionAm = this.txtDescriptionAm.Text;
                objtblDocument.ParentGuid = new Guid(this.cboParentGuid.Value.ToString());//Attention
                objtblDocument.Isoptional = this.ckIsoptional.Checked;//Attention

                tblDocumentBussiness objtblDocumentBussiness = new tblDocumentBussiness();
                if ((bool)Session["IsNew"])
                {
                    if (!objtblDocumentBussiness.Exists(Guid.Parse(this.cboParentGuid.Value.ToString()),this.txtCode.Text))
                    {
                        objtblDocument.MainGuid = Guid.NewGuid();
                        if (objtblDocumentBussiness.InserttblDocument(objtblDocument))
                        {
                            DoDetailInsetSave();
                            BindGrid();
                            ShowSuccess(Resources.Message.MSG_SAVED);
                            saved = true;
                        }
                    }
                    else
                    {
                        ShowError(Resources.Message.MSG_DocumentExisted);  
                    }
                }
                else
                {
                    objtblDocument.MainGuid = (Guid)Session["ID"];//Attention
                    objtblDocumentBussiness.UpdatetblDocument(objtblDocument);
                    DoDetailUpdateSave(Guid.Parse(Session["ID"].ToString()));
                }
                Session["IsNew"] = false;
                pnlData.ClientVisible = false;
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
                
            }
            return saved;
        }

        protected void gvwtblDocument_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            tblDocument objtblDocument = new tblDocument();

            try
            {
                int index = e.RowIndex;
                //////int intID = Convert.ToInt32(this.gvwtblDocument.Rows[index].Cells[0].Text);//to-do check this code
                //////Session["ID"]=intID;
                DoDelete();
                BindGrid();
            }
            catch (Exception ex)
            {
                //ShowError(ex.Message);
            }
        }

        protected bool DoDetailInsetSave()
        {

            try
            {

                tblDocument objtblDocument = new tblDocument();
                tblDocumentBussiness objDocDetail = new tblDocumentBussiness();
                objtblDocument = objDocDetail.GetRecordDetail(this.txtCode.Text);
                Guid DocGuid = objtblDocument.MainGuid;
                for (int i = 0; i < gvwtbllookup.VisibleRowCount; i++)
                {
                    GridViewDataColumn col1 = gvwtbllookup.Columns[1] as GridViewDataColumn;
                    ASPxCheckBox chkIsVal = gvwtbllookup.FindRowCellTemplateControl(i, col1, "chkInclude") as ASPxCheckBox;

                    if (chkIsVal.Checked == true)
                    {
                        tblDocumentDetail objtblDocumentDetail = new tblDocumentDetail();

                        objtblDocumentDetail.Id = Convert.ToInt32(gvwtbllookup.GetRowValues(i, "id"));
                        objtblDocumentDetail.DocMainGuid = DocGuid;
                        tblDocumentDetailBussiness objtblDocumentDetailBussiness = new tblDocumentDetailBussiness();
                        objtblDocumentDetailBussiness.InserttblDocumentDetail(objtblDocumentDetail);

                    }

                }
                return true;
            }
            catch
            {

                return false;
            }


        }

        protected bool DoDetailUpdateSave(Guid DocumentGuid)
        {
            try
            {
                tblDocumentDetailBussiness objtblDocumentDetailBussinessd = new tblDocumentDetailBussiness();
                objtblDocumentDetailBussinessd.DeleteDocument(DocumentGuid);
                tblDocument objtblDocument = new tblDocument();
                tblDocumentBussiness objDocDetail = new tblDocumentBussiness();
                objtblDocument = objDocDetail.GetRecordDetail(this.txtCode.Text);
                Guid DocGuid = objtblDocument.MainGuid;
                for (int i = 0; i < gvwtbllookup.VisibleRowCount; i++)
                {
                    GridViewDataColumn col1 = gvwtbllookup.Columns[1] as GridViewDataColumn;
                    ASPxCheckBox chkIsVal = gvwtbllookup.FindRowCellTemplateControl(i, col1, "chkInclude") as ASPxCheckBox;
                    if (chkIsVal.Checked == true)
                    {
                        tblDocumentDetail objtblDocumentDetail = new tblDocumentDetail();
                        objtblDocumentDetail.Id = Convert.ToInt32(gvwtbllookup.GetRowValues(i, "id"));
                        objtblDocumentDetail.DocMainGuid = DocGuid;
                        tblDocumentDetailBussiness objtblDocumentDetailBussiness = new tblDocumentDetailBussiness();
                        objtblDocumentDetailBussiness.InserttblDocumentDetail(objtblDocumentDetail);
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }


        }

        protected void gvwtblDocument_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            if ((e.NewPageIndex >= 0) & (e.NewPageIndex <= gvwtblDocument.PageCount))
            {
                gvwtblDocument.PageIndex = e.NewPageIndex;
                BindGrid();
            }
        }

        protected void DoSearch()
        {
            try
            {                    
                tblDocumentBussiness objtblDocumentBussiness = new tblDocumentBussiness();
                if (cboHRActivity.SelectedIndex != -1)
                {
                    gvwtblDocument.DataSource = objtblDocumentBussiness.SearchDocument(cboHRActivity.Value.ToString());
                    gvwtblDocument.DataBind();
                }
                else
                {
                    gvwtblDocument.DataSource = objtblDocumentBussiness.GettblDocuments();
                    gvwtblDocument.DataBind();
                }
            }
            catch
            {
            }
        }

        protected void gvwtblDocument_RowEditing(Object sender, GridViewEditEventArgs e)
        {
            try
            {
                //Label lbl = (Label)gvwtblDocument.Rows[e.NewEditIndex].FindControl(lblMainGuid);
                //DoFind((Guid)lbl.Text);
                e.NewEditIndex = -1;
            }
            catch (Exception ex)
            {
                //ShowError(ex.Message);
            }
        }

        public enum enumToolbar
        {
            eNew = 0,
            eSave = 2,
            eDelete = 4,
            eRefresh = 6,
            eFind = 8,
            ePrint = 10,
            eEdit = 12
        }

        protected void LoadDocumentTypes()
        {
            tblDocumentTypeBussiness obj = new tblDocumentTypeBussiness();
            if (int.Parse(Session["ProLanguage"].ToString()) == 1)
            {
                cboParentGuid.ValueField = "MainGuid";
                cboParentGuid.TextField = "DescriptionAm";
                ListEditItem le = new ListEditItem("ይምረጡ", "-1");
                cboParentGuid.Items.Insert(0, le);
            }
            else if (int.Parse(Session["ProLanguage"].ToString()) == 0)
            {
                cboParentGuid.ValueField = "MainGuid";
                cboParentGuid.TextField = "Description";
                ListEditItem le = new ListEditItem("--Select--", "-1");
                cboParentGuid.Items.Insert(0, le);
            }
            if (int.Parse(Session["ProLanguage"].ToString()) == 2)
            {
                cboParentGuid.ValueField = "MainGuid";
                cboParentGuid.TextField = "DescriptionAm";
                ListEditItem le = new ListEditItem("Filadha", "-1");
                cboParentGuid.Items.Insert(0, le);
            }
            cboParentGuid.DataSource = obj.GettblDocumentTypes();
            cboParentGuid.DataBind();
           //GETH: Sep 9 2018
            //cboParentGuid.SelectedIndex = 0;
        }

        protected void cbDocument_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {
            cbDocument.JSProperties["cpStatus"] = "";

            string strID = string.Empty;
            string[] parameters = e.Parameter.Split('|');
            string strParam = parameters[0].ToString();
            if (e.Parameter.ToString().Contains('|'))
                strID = parameters[1].ToString();

            switch (strParam)
            {
                case "New":
                    DoNew();
                    break;
                case "Edit":
                    DoFind(strID);
                    Session["ID"] = new Guid(strID);
                    break;
                case "Search":
                    DoSearch();
                    break;
                case "Save":
                    DoSave();
                    break;
                case "Show":
                    BindGrid();
                    break;
                case "Delete":
                    if (strID.Length > 0)
                        Session["ID"] = new Guid(strID);
                    DoDelete();

                    break;
                case "Refresh":
                    //BindGrid();
                    //cbData.JSProperties["cpStatus"] = "REFRESH";
                    //cbData.JSProperties["cpAction"] = "refresh";
                    break;
                case "Attach":
                    //DoAttach();
                    break;
                case "DocumentDetail":
                    BindLookupGrid(Guid.Parse(strID), 1);
                    Session["Lookparent"] = strID;
                    break;
                default:
                    break;

            }
        }

        private void ShowError(string strMsg)
        {
            cbDocument.JSProperties["cpMessage"] = strMsg;
            cbDocument.JSProperties["cpStatus"] = "ERROR";
        }

        private void ShowSuccess(string strMsg)
        {
            cbDocument.JSProperties["cpMessage"] = strMsg;
            cbDocument.JSProperties["cpStatus"] = "SUCCESS";
        }

        private void ShowMessage(string strMsg)
        {
            cbDocument.JSProperties["cpMessage"] = strMsg;
            cbDocument.JSProperties["cpStatus"] = "INFO";
        }

        private string GetErrorDisplay(List<string> strMessages)
        {

            StringBuilder sb = new StringBuilder();
            if (strMessages.Count == 0) return string.Empty;
            sb.Append(Resources.Message.MSG_CVALD);
            sb.Append("<ul>");
            foreach (string s in strMessages)
            {
                sb.Append(String.Format("<li>{0}</li>", s));
            }
            sb.Append("</ul>");

            return sb.ToString();

        }

        private List<string> GetErrorMessage()
        {
            List<string> errMessages = new List<string>();
            if (int.Parse(Session["ProLanguage"].ToString()) == 1)
            {

                if (cboParentGuid.Value.ToString() == "-1")
                {
                    errMessages.Add("እባክዎ የስራ አይነት ያስገቡ");
                }
                if (txtCode.Text.Trim().Length == 0)
                {
                    errMessages.Add("እባክዎ መለያ ቁጥሩን ያስገቡ");

                }
                if (txtDescription.Text.Trim().Length == 0)
                {
                    errMessages.Add("እባክዎ ስሜውን ያስገቡ");

                }
                if (txtDescriptionAm.Text.Trim().Length == 0)
                {
                    errMessages.Add("እባክዎ ስሜውን በክልሉ ቋንቋ ያስገቡ");

                }
                else if (int.Parse(Session["ProLanguage"].ToString()) == 0)
                {
                    if (cboParentGuid.Value.ToString() == "-1")
                    {
                        errMessages.Add("Please Insert Activitiy Type");
                    }
                    if (txtCode.Text.Trim().Length == 0)
                    {
                        errMessages.Add("Please Insert Code Number");

                    }
                    if (txtDescription.Text.Trim().Length == 0)
                    {
                        errMessages.Add("Plese Insert English Description");

                    }
                    if (txtDescriptionAm.Text.Trim().Length == 0)
                    {
                        errMessages.Add("Please Insert Regional Description");
                    }
                }
            }


            return errMessages;
        }

        protected void gvwtblDocument_PageIndexChanged(object sender, EventArgs e)
        {
            if(cboHRActivity.Value !=  null)
            {
                DoSearch();
            }
            else
            {
                BindGrid();
            }
        }

        private void BindLookupGrid(Guid parent, int language)
        {
            LoadDocumentTypes();
            btnSave.ClientEnabled = true;
            tblDocumentType objDocumentTypeD = new tblDocumentType();
            tblDocumentTypeBussiness objdocumentTypeBuss = new tblDocumentTypeBussiness();
            objDocumentTypeD = objdocumentTypeBuss.GettblDocumentType(parent);
            DataTable dt = new DataTable();
            tblLookupBussiness objtbllookupBussiness = new tblLookupBussiness();
            dt = objtbllookupBussiness.GetDocDetailRecords(objDocumentTypeD.Code, language);
            if (dt.Rows.Count > 0)
            {
                gvwtbllookup.DataSource = objtbllookupBussiness.GetDocDetailRecords(objDocumentTypeD.Code, language);
                gvwtbllookup.DataBind();
                gvwtbllookup.ClientVisible = true;
            }
            else
            {
                gvwtbllookup.ClientVisible = false;

            }
            if((bool)Session["IsNew"] == true)
            {
                pnlData.ClientVisible = true;
                lblCode.Visible = false;
                txtCode.Visible = false;
            }
        }

        protected void cboHRActivitytype_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
        {

            //BindLookupGrid("311", 1);
        }

        protected void Grid_DataBinding(Guid parent)
        {

            tblDocumentType objDocumentTypeD = new tblDocumentType();
            tblDocumentTypeBussiness objdocumentTypeBuss = new tblDocumentTypeBussiness();
            if (cboParentGuid.Value != null)
            {
                if (cboParentGuid.Value != "-1")
                {
                    objDocumentTypeD = objdocumentTypeBuss.GettblDocumentType(parent);

                    DataTable dt = new DataTable();
                    tblLookupBussiness objtbllookupBussiness = new tblLookupBussiness();
                    tblDocumentDetailBussiness objtblDocumentDetailBussiness = new tblDocumentDetailBussiness();
                    dt = objtblDocumentDetailBussiness.GetDocRecord(parent);
                    //dt = objtbllookupBussiness.GetDocDetailRecords(objDocumentTypeD.Code, 1);
                    for (int i = 0; i < gvwtbllookup.VisibleRowCount; i++)
                    {
                        GridViewDataColumn col1 = gvwtbllookup.Columns[1] as GridViewDataColumn;
                        ASPxCheckBox chkIsVal = gvwtbllookup.FindRowCellTemplateControl(i, col1, "chkInclude") as ASPxCheckBox;

                        for (int cr = 0; cr < dt.Rows.Count; cr++)
                        {
                            if (Convert.ToInt32(gvwtbllookup.GetRowValues(i, "id")) == Convert.ToInt32(dt.Rows[cr]["id"]))
                            {
                                chkIsVal.Checked = true;
                            }
                        }

                    }
                }

            }

        }

        protected void cboParentGuid_DataBinding(object sender, EventArgs e)
        {

            //tblDocumentType objDocumentTypeD = new tblDocumentType();
            //tblDocumentTypeBussiness objdocumentTypeBuss = new tblDocumentTypeBussiness();
            //if (cboParentGuid.Value != null)
            //{
            //    if (cboParentGuid.Value != "-1")
            //    { 
            //        objDocumentTypeD = objdocumentTypeBuss.GettblDocumentType(Guid.Parse(cboParentGuid.Value.ToString()));

            //        DataTable dt = new DataTable();
            //        tblLookupBussiness objtbllookupBussiness = new tblLookupBussiness();

            //        dt = objtbllookupBussiness.GetDocDetailRecords(objDocumentTypeD.Code,1);
            //        for (int i = 0; i < gvwtbllookup.VisibleRowCount; i++)
            //        {
            //            GridViewDataColumn col1 = gvwtbllookup.Columns[1] as GridViewDataColumn;
            //            ASPxCheckBox chkIsVal = gvwtbllookup.FindRowCellTemplateControl(i, col1, "chkInclude") as ASPxCheckBox;

            //            for (int cr = 0; cr < dt.Rows.Count; cr++)
            //            {
            //                if (Convert.ToInt32(gvwtbllookup.GetRowValues(i, "id")) == Convert.ToInt32(dt.Rows[cr]["id"]))
            //                {
            //                    chkIsVal.Checked = true;
            //                }
            //            }

            //            }
            //        }


            //if (chkIsVal.Checked == true)
            //{
            //    tblDocumentDetail objtblDocumentDetail = new tblDocumentDetail();

            //    objtblDocumentDetail.Id = Convert.ToInt32(gvwtbllookup.GetRowValues(i, "id"));

            //    tblDocumentDetailBussiness objtblDocumentDetailBussiness = new tblDocumentDetailBussiness();
            //    objtblDocumentDetailBussiness.InserttblDocumentDetail(objtblDocumentDetail);

            //}

            //}
            //objInspection = objinspectionbusiness.GetInspection(Guid.Parse(inspguid.ToString()));
            //Crdt = objlookup.GetRecords((string)ComboDocType.SelectedItem.Value, Guid.Parse(inspguid.ToString()), Guid.Parse(Session["APersonGuid"].ToString()), 1);
            //cbl.Items.Clear();
            //for (int cr = 0; cr < Crdt.Rows.Count; cr++)
            //{
            //    cbl.Items.Add(Crdt.Rows[cr]["amdescription"].ToString(), Crdt.Rows[cr]["id"].ToString());
            //    cbl.Items[cr].Selected = (bool)Crdt.Rows[cr]["Isvalid"];
            //}
        }

        protected void gvwtbllookup_DataBinding(object sender, EventArgs e)
        {
            if (cboParentGuid.Value != null)
            {
                if (cboParentGuid.Value != "-1")
                {
                }
            }
        }

        protected void gvwtbllookup_PageIndexChanged(object sender, EventArgs e)
        {
            BindLookupGrid(Guid.Parse(Session["Lookparent"].ToString()), 1);
        }

        protected void gvwtblDocument_OnCustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
        {
            ProfileCommon p = this.Profile;
            p = this.Profile.GetProfile(HttpContext.Current.User.Identity.Name);
            if (e.Column.FieldName == "Isoptional")
            {
                if (p.Organization.LanguageID == 1)
                {
                    if (Convert.ToBoolean(e.Value))
                    {
                        e.DisplayText = "አዎ";
                    }
                    else
                    {
                        e.DisplayText = "አይ";
                    }

                }
                if (p.Organization.LanguageID == 2)
                {
                   if (Convert.ToBoolean(e.Value))
                   {
                       e.DisplayText = "Eeyyee";
                   }
                    else
                   {
                       e.DisplayText = "Miti";
                   }
                }
            }
        }
        
}

